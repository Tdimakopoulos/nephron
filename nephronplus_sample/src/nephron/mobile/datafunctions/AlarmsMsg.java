package nephron.mobile.datafunctions;

 
import java.io.Serializable;

 

public class AlarmsMsg  implements Serializable{

	String msgPatient,msgDoctor,alert,date;
	int idDtabase,status;
	
	public AlarmsMsg(int id){
		
		switch (id){
		case 0: // decTreeMsg_detectedBPsysAbsL
			alert="decTreeMsg_detectedBPsysAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys lower than normal range";
			 break;
			 
		case 1: // decTreeMsg_detectedBPsysAbsH
			alert="decTreeMsg_detectedBPsysAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys higher than normal range";
			 break;
		case 2: // decTreeMsg_detectedBPsysAAbsH
			alert="decTreeMsg_detectedBPsysAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-sys high";
			 break;
		case 3: // decTreeMsg_detectedBPdiaAbsL
			alert="decTreeMsg_detectedBPdiaAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia lower than normal range";
			 break;
		case 4: // decTreeMsg_detectedBPdiaAbsH
			alert="decTreeMsg_detectedBPdiaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia higher than normal range";
			 break;
		case 5: // decTreeMsg_detectedBPdiaAAbsH
			alert="decTreeMsg_detectedBPdiaAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-dia high";
			 break;
		case 6: // dectreeMsg_detectedWghtAbsL
			alert="dectreeMsg_detectedWghtAbsL";
			msgPatient ="";//debug
			msgDoctor  ="";
			 break;
		case 7: // dectreeMsg_detectedWghtAbsH
			alert="dectreeMsg_detectedWghtAbsH";
			msgPatient ="";//debug
			msgDoctor  ="";//12
			 break;
		case 8: // dectreeMsg_detectedWghtTrdT
			alert="dectreeMsg_detectedWghtTrdT";
			msgPatient ="";//12 debug
			msgDoctor  ="";
			 break;
		case 9: // dectreeMsg_detectedpumpBaccDevi
			alert="dectreeMsg_detectedpumpBaccDevi";
			msgPatient ="";//23 debug
			msgDoctor  ="";
			 break;
		case 10: // dectreeMsg_detectedpumpFaccDevi
			alert="dectreeMsg_detectedpumpFaccDevi";
			msgPatient ="";//23 //debug
			msgDoctor  ="";
			 break;
		case 11: // dectreeMsg_detectedBPorFPhigh
			alert="dectreeMsg_detectedBPorFPhigh";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 12: // dectreeMsg_detectedBPorFPlow
			alert="dectreeMsg_detectedBPorFPlow";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 13: // dectreeMsg_detectedBTSoTH
			alert="dectreeMsg_detectedBTSoTH";
			msgPatient ="";
			msgDoctor  ="ALARM: temperature (high) excess at BTSo, all pumps stopped";
			 break;
		case 14: // dectreeMsg_detectedBTSoTL
			alert="dectreeMsg_detectedBTSoTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSo, all pumps stopped";
			 break;
		case 15: // dectreeMsg_detectedBTSiTH
			alert="dectreeMsg_detectedBTSiTH";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (high) excess at BTSi, all pumps stopped";
			 break;
		case 16: // dectreeMsg_detectedBTSiTL
			alert="dectreeMsg_detectedBTSiTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSi, all pumps stopped";
			 break;
		case 17: // dectreeMsg_detectedSorDys
			alert="dectreeMsg_detectedSorDys";
			msgPatient ="please replace the sorbend cartridge"; 
			msgDoctor  ="a sorbend disfunction was detected, patient was instructed to replace the cartridge";
			 break;
		case 18: // dectreeMsg_detectedPhAAbsL
			alert="dectreeMsg_detectedPhAAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH low";
			 break;
		case 19: // dectreeMsg_detectedPhAAbsH
			alert="dectreeMsg_detectedPhAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH high";
			 break;
		case 20: // dectreeMsg_detectedPhAbsLorPhAbsH
			alert="dectreeMsg_detectedPhAbsLorPhAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH out of normal range";
			 break;
		case 21: // dectreeMsg_detectedPhTrdLPsT
			alert="dectreeMsg_detectedPhTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH decrease out of normal range";
			 break;
		case 22: // dectreeMsg_detectedPhTrdHPsT
			alert="dectreeMsg_detectedPhTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH increase out of normal range";
			 break;
		case 23: // dectreeMsg_detectedNaAbsLorNaAbsH
			alert="dectreeMsg_detectedNaAbsLorNaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="Na+ out of normal range";
			 break;
		case 24: // dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT
			alert="dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT";
			msgPatient ="please contact the physician"; 
			msgDoctor  =" A increasing or decreasing fast change in Na+ was detected in thrend analysis";
			 break;
		case 25: // dectreeMsg_detectedKAbsLorKAbsHandSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break;
		case 26: // dectreeMsg_detectedKAbsLorKAbsHnoSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="K+ out of normal range";
			 break;
		case 27: // dectreeMsg_detectedKAAL
			alert="dectreeMsg_detectedKAAL";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 28: // dectreeMsg_detectedKAAHandKincrease
			alert="dectreeMsg_detectedKAAHandKincrease";
			msgPatient ="replace sorbent cartridge and Contact Physician";
			msgDoctor  ="";
			 break;
		case 29: // dectreeMsg_detectedKAAHnoKincrease
			alert="dectreeMsg_detectedKAAHnoKincrease";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 30: // dectreeMsg_detectedKTrdLPsT
			alert="dectreeMsg_detectedKTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 31: //
			msgPatient ="please contact the physician";//dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgDoctor  ="A increasing fast change in K+ trend was detected  in thrend analysis.";
			 break;
		case 32: // dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 33: // dectreeMsg_detectedKTrdofnriandSorDys
			alert="dectreeMsg_detectedKTrdofnriandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 34: // dectreeMsg_detectedPhTrdofnriandSorDys
			alert="dectreeMsg_detectedPhTrdofnriandSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break; 
		case 35: // dectreeMsg_detectedUreaAAbsHandSorDys
			alert="dectreeMsg_detectedUreaAAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 36: // dectreeMsg_detectedUreaAAbsHnoSorDys
			alert="dectreeMsg_detectedUreaAAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea out of normal range (high)";
			 break; 
		case 37: // dectreeMsg_detectedUreaTrdHPsT
			alert="dectreeMsg_detectedUreaTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea increase out of normal range (high)";
			 break; 
			 
			 
			 
		}
	}
	
	
	public AlarmsMsg(int id,int idnew){
		idDtabase=idnew;
		switch (id){
		case 0: // decTreeMsg_detectedBPsysAbsL
			alert="decTreeMsg_detectedBPsysAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys lower than normal range";
			 break;
			 
		case 1: // decTreeMsg_detectedBPsysAbsH
			alert="decTreeMsg_detectedBPsysAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys higher than normal range";
			 break;
		case 2: // decTreeMsg_detectedBPsysAAbsH
			alert="decTreeMsg_detectedBPsysAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-sys high";
			 break;
		case 3: // decTreeMsg_detectedBPdiaAbsL
			alert="decTreeMsg_detectedBPdiaAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia lower than normal range";
			 break;
		case 4: // decTreeMsg_detectedBPdiaAbsH
			alert="decTreeMsg_detectedBPdiaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia higher than normal range";
			 break;
		case 5: // decTreeMsg_detectedBPdiaAAbsH
			alert="decTreeMsg_detectedBPdiaAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-dia high";
			 break;
		case 6: // dectreeMsg_detectedWghtAbsL
			alert="dectreeMsg_detectedWghtAbsL";
			msgPatient ="";//debug
			msgDoctor  ="";
			 break;
		case 7: // dectreeMsg_detectedWghtAbsH
			alert="dectreeMsg_detectedWghtAbsH";
			msgPatient ="";//debug
			msgDoctor  ="";//12
			 break;
		case 8: // dectreeMsg_detectedWghtTrdT
			alert="dectreeMsg_detectedWghtTrdT";
			msgPatient ="";//12 debug
			msgDoctor  ="";
			 break;
		case 9: // dectreeMsg_detectedpumpBaccDevi
			alert="dectreeMsg_detectedpumpBaccDevi";
			msgPatient ="";//23 debug
			msgDoctor  ="";
			 break;
		case 10: // dectreeMsg_detectedpumpFaccDevi
			alert="dectreeMsg_detectedpumpFaccDevi";
			msgPatient ="";//23 //debug
			msgDoctor  ="";
			 break;
		case 11: // dectreeMsg_detectedBPorFPhigh
			alert="dectreeMsg_detectedBPorFPhigh";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 12: // dectreeMsg_detectedBPorFPlow
			alert="dectreeMsg_detectedBPorFPlow";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 13: // dectreeMsg_detectedBTSoTH
			alert="dectreeMsg_detectedBTSoTH";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (high) excess at BTSo, all pumps stopped";
			 break;
		case 14: // dectreeMsg_detectedBTSoTL
			alert="dectreeMsg_detectedBTSoTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSo, all pumps stopped";
			 break;
		case 15: // dectreeMsg_detectedBTSiTH
			alert="dectreeMsg_detectedBTSiTH";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (high) excess at BTSi, all pumps stopped";
			 break;
		case 16: // dectreeMsg_detectedBTSiTL
			alert="dectreeMsg_detectedBTSiTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSi, all pumps stopped";
			 break;
		case 17: // dectreeMsg_detectedSorDys
			alert="dectreeMsg_detectedSorDys";
			msgPatient ="please replace the sorbend cartridge"; 
			msgDoctor  ="a sorbend disfunction was detected, patient was instructed to replace the cartridge";
			 break;
		case 18: // dectreeMsg_detectedPhAAbsL
			alert="dectreeMsg_detectedPhAAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH low";
			 break;
		case 19: // dectreeMsg_detectedPhAAbsH
			alert="dectreeMsg_detectedPhAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH high";
			 break;
		case 20: // dectreeMsg_detectedPhAbsLorPhAbsH
			alert="dectreeMsg_detectedPhAbsLorPhAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH out of normal range";
			 break;
		case 21: // dectreeMsg_detectedPhTrdLPsT
			alert="dectreeMsg_detectedPhTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH decrease out of normal range";
			 break;
		case 22: // dectreeMsg_detectedPhTrdHPsT
			alert="dectreeMsg_detectedPhTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH increase out of normal range";
			 break;
		case 23: // dectreeMsg_detectedNaAbsLorNaAbsH
			alert="dectreeMsg_detectedNaAbsLorNaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="Na+ out of normal range";
			 break;
		case 24: // dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT
			alert="dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT";
			msgPatient ="please contact the physician"; 
			msgDoctor  =" A increasing or decreasing fast change in Na+ was detected in thrend analysis";
			 break;
		case 25: // dectreeMsg_detectedKAbsLorKAbsHandSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break;
		case 26: // dectreeMsg_detectedKAbsLorKAbsHnoSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="K+ out of normal range";
			 break;
		case 27: // dectreeMsg_detectedKAAL
			alert="dectreeMsg_detectedKAAL";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 28: // dectreeMsg_detectedKAAHandKincrease
			alert="dectreeMsg_detectedKAAHandKincrease";
			msgPatient ="replace sorbent cartridge and Contact Physician";
			msgDoctor  ="";
			 break;
		case 29: // dectreeMsg_detectedKAAHnoKincrease
			alert="dectreeMsg_detectedKAAHnoKincrease";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 30: // dectreeMsg_detectedKTrdLPsT
			alert="dectreeMsg_detectedKTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 31: //
			msgPatient ="please contact the physician";//dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgDoctor  ="A increasing fast change in K+ trend was detected  in thrend analysis.";
			 break;
		case 32: // dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 33: // dectreeMsg_detectedKTrdofnriandSorDys
			alert="dectreeMsg_detectedKTrdofnriandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 34: // dectreeMsg_detectedPhTrdofnriandSorDys
			alert="dectreeMsg_detectedPhTrdofnriandSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break; 
		case 35: // dectreeMsg_detectedUreaAAbsHandSorDys
			alert="dectreeMsg_detectedUreaAAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 36: // dectreeMsg_detectedUreaAAbsHnoSorDys
			alert="dectreeMsg_detectedUreaAAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea out of normal range (high)";
			 break; 
		case 37: // dectreeMsg_detectedUreaTrdHPsT
			alert="dectreeMsg_detectedUreaTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea increase out of normal range (high)";
			 break; 
			 
			 
			 
		}
	}
	
	
	
	

	public AlarmsMsg(int idnew,int id,String date,int status){
		idDtabase=idnew;
		this.date=date;
		this.status=status;
		 
		switch (id){
		case 0: // decTreeMsg_detectedBPsysAbsL
			alert="decTreeMsg_detectedBPsysAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys lower than normal range";
			 break;
			 
		case 1: // decTreeMsg_detectedBPsysAbsH
			alert="decTreeMsg_detectedBPsysAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-sys higher than normal range";
			 break;
		case 2: // decTreeMsg_detectedBPsysAAbsH
			alert="decTreeMsg_detectedBPsysAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-sys high";
			 break;
		case 3: // decTreeMsg_detectedBPdiaAbsL
			alert="decTreeMsg_detectedBPdiaAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia lower than normal range";
			 break;
		case 4: // decTreeMsg_detectedBPdiaAbsH
			alert="decTreeMsg_detectedBPdiaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="BP-dia higher than normal range";
			 break;
		case 5: // decTreeMsg_detectedBPdiaAAbsH
			alert="decTreeMsg_detectedBPdiaAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: BP-dia high";
			 break;
		case 6: // dectreeMsg_detectedWghtAbsL
			alert="dectreeMsg_detectedWghtAbsL";
			msgPatient ="";//debug
			msgDoctor  ="";
			 break;
		case 7: // dectreeMsg_detectedWghtAbsH
			alert="dectreeMsg_detectedWghtAbsH";
			msgPatient ="";//debug
			msgDoctor  ="";//12
			 break;
		case 8: // dectreeMsg_detectedWghtTrdT
			alert="dectreeMsg_detectedWghtTrdT";
			msgPatient ="";//12 debug
			msgDoctor  ="";
			 break;
		case 9: // dectreeMsg_detectedpumpBaccDevi
			alert="dectreeMsg_detectedpumpBaccDevi";
			msgPatient ="";//23 debug
			msgDoctor  ="";
			 break;
		case 10: // dectreeMsg_detectedpumpFaccDevi
			alert="dectreeMsg_detectedpumpFaccDevi";
			msgPatient ="";//23 //debug
			msgDoctor  ="";
			 break;
		case 11: // dectreeMsg_detectedBPorFPhigh
			alert="dectreeMsg_detectedBPorFPhigh";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 12: // dectreeMsg_detectedBPorFPlow
			alert="dectreeMsg_detectedBPorFPlow";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
			 break;
		case 13: // dectreeMsg_detectedBTSoTH
			alert="dectreeMsg_detectedBTSoTH";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (high) excess at BTSo, all pumps stopped";
			 break;
		case 14: // dectreeMsg_detectedBTSoTL
			alert="dectreeMsg_detectedBTSoTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSo, all pumps stopped";
			 break;
		case 15: // dectreeMsg_detectedBTSiTH
			alert="dectreeMsg_detectedBTSiTH";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (high) excess at BTSi, all pumps stopped";
			 break;
		case 16: // dectreeMsg_detectedBTSiTL
			alert="dectreeMsg_detectedBTSiTL";
			msgPatient ="Pumps Stopped - Contact Physician";
			msgDoctor  ="ALARM: temperature (low) excess at BTSi, all pumps stopped";
			 break;
		case 17: // dectreeMsg_detectedSorDys
			alert="dectreeMsg_detectedSorDys";
			msgPatient ="please replace the sorbend cartridge"; 
			msgDoctor  ="a sorbend disfunction was detected, patient was instructed to replace the cartridge";
			 break;
		case 18: // dectreeMsg_detectedPhAAbsL
			alert="dectreeMsg_detectedPhAAbsL";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH low";
			 break;
		case 19: // dectreeMsg_detectedPhAAbsH
			alert="dectreeMsg_detectedPhAAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="ALARM: pH high";
			 break;
		case 20: // dectreeMsg_detectedPhAbsLorPhAbsH
			alert="dectreeMsg_detectedPhAbsLorPhAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH out of normal range";
			 break;
		case 21: // dectreeMsg_detectedPhTrdLPsT
			alert="dectreeMsg_detectedPhTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH decrease out of normal range";
			 break;
		case 22: // dectreeMsg_detectedPhTrdHPsT
			alert="dectreeMsg_detectedPhTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="pH increase out of normal range";
			 break;
		case 23: // dectreeMsg_detectedNaAbsLorNaAbsH
			alert="dectreeMsg_detectedNaAbsLorNaAbsH";
			msgPatient ="Contact Physician";
			msgDoctor  ="Na+ out of normal range";
			 break;
		case 24: // dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT
			alert="dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT";
			msgPatient ="please contact the physician"; 
			msgDoctor  =" A increasing or decreasing fast change in Na+ was detected in thrend analysis";
			 break;
		case 25: // dectreeMsg_detectedKAbsLorKAbsHandSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break;
		case 26: // dectreeMsg_detectedKAbsLorKAbsHnoSorDys
			alert="dectreeMsg_detectedKAbsLorKAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="K+ out of normal range";
			 break;
		case 27: // dectreeMsg_detectedKAAL
			alert="dectreeMsg_detectedKAAL";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 28: // dectreeMsg_detectedKAAHandKincrease
			alert="dectreeMsg_detectedKAAHandKincrease";
			msgPatient ="replace sorbent cartridge and Contact Physician";
			msgDoctor  ="";
			 break;
		case 29: // dectreeMsg_detectedKAAHnoKincrease
			alert="dectreeMsg_detectedKAAHnoKincrease";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 30: // dectreeMsg_detectedKTrdLPsT
			alert="dectreeMsg_detectedKTrdLPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break;
		case 31: //
			msgPatient ="please contact the physician";//dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgDoctor  ="A increasing fast change in K+ trend was detected  in thrend analysis.";
			 break;
		case 32: // dectreeMsg_detectedKTrdHPsT
			alert="dectreeMsg_detectedKTrdHPsT";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 33: // dectreeMsg_detectedKTrdofnriandSorDys
			alert="dectreeMsg_detectedKTrdofnriandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 34: // dectreeMsg_detectedPhTrdofnriandSorDys
			alert="dectreeMsg_detectedPhTrdofnriandSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="";
			 break; 
		case 35: // dectreeMsg_detectedUreaAAbsHandSorDys
			alert="dectreeMsg_detectedUreaAAbsHandSorDys";
			msgPatient ="replace cartridge";
			msgDoctor  ="";
			 break; 
		case 36: // dectreeMsg_detectedUreaAAbsHnoSorDys
			alert="dectreeMsg_detectedUreaAAbsHnoSorDys";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea out of normal range (high)";
			 break; 
		case 37: // dectreeMsg_detectedUreaTrdHPsT
			alert="dectreeMsg_detectedUreaTrdHPsT";
			msgPatient ="Contact Physician";
			msgDoctor  ="Urea increase out of normal range (high)";
			 break; 
			 
			 
			 
		}
	}
	public String getmsgPatient() {
		return msgPatient;    
	}
	
	public String getmsgDoctor() {
		return msgDoctor;    
	}
	public String getmsgAlert() {
		return alert;    
	}
	public int getIdDtabase(){
		return idDtabase;
	}
	public String getDate(){
		return date;
	}
	public int getStatus(){
		return status;
	}
	
	 
	 
}  
 