package nephron.mobile.datafunctions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
	public static final String KEY_ROWID = "_id";
	public static final String KEY_ISBN = "isbn";
	public static final String KEY_TITLE = "title";
	public static final String KEY_PUBLISHER = "publisher";
	private static final String TAG = "DBAdapter";
	private static final String DATABASE_NAME = "nephron";
	private static final String DATABASE_TABLE = "act";
	private static final String DATABASE_TABLE2 = "alert";
	private static final String DATABASE_TABLE3 = "scheduledTask";
	private static final int DATABASE_VERSION = 1;

	private static final String ACT_CREATE = "create table act (_id integer primary key autoincrement, "
			+ "ACT_TYPE integer not null, CREATION_DATE text not null, "
			+ "DESCR text,RECORD_STATUS integer,UPDATE_DATE text,ACT_STATUS integer not null,ACTIVTY_DATE text,"
			+ "EFFECTIVE_DATE text,MOOD_CODE integer not null,TEXT text,VALUE numeric,WAKD_MSG text,CREATED_BY numeric,UPDATED_BY numeric);";

	private static final String ALERT_CREATE = "create table alert (_id integer primary key autoincrement, "
			+ "CREATION_DATE text not null, "
			+ "DESCR text,RECORD_STATUS integer,UPDATE_DATE text,ALERT_DATE text,CODE integer not null,SEVERITY integer not null,"
			+ "CREATED_BY integer,UPDATED_BY integer,DEVICE_ID integer);";

	private static final String SCHEDULED_TASK = "create table scheduledTask (_id integer primary key autoincrement, "
			+ "CREATION_DATE text not null, "
			+ "DESCR text,RECORD_STATUS integer,UPDATE_DATE text,OUTCOME integer,"
			+ "CREATED_BY integer,UPDATED_BY integer,ACT_ID integer);";

	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(ACT_CREATE);
			db.execSQL(ALERT_CREATE);
			db.execSQL(SCHEDULED_TASK);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS titles");
			onCreate(db);
		}
	}

	// ---opens the database---
	public DBAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// ---checks if database is open---
	public boolean isOpen() {
		return db.isOpen();

	}

	// ---insert a title into the database---
	/*
	 * public long insertTitle(String isbn, String title, String publisher) {
	 * ContentValues initialValues = new ContentValues();
	 * initialValues.put(KEY_ISBN, isbn); initialValues.put(KEY_TITLE, title);
	 * initialValues.put(KEY_PUBLISHER, publisher); return
	 * db.insert(DATABASE_TABLE, null, initialValues); }
	 */

	public long insertScheduledTask(int ACT_TYPE, String CREATION_DATE, String DESCR,
			int RECORD_STATUS, String UPDATE_DATE, int ACT_STATUS,
			String ACTIVTY_DATE, String EFFECTIVE_DATE, int MOOD_CODE,
			String TEXTt, double VALUE, String WAKD_MSG, int CREATED_BY,
			int UPDATED_BY,String EVENTDATE,String EVENTDESCR){

		long id;
		ContentValues initialValues = new ContentValues();
		initialValues.put("ACT_TYPE", ACT_TYPE);
		initialValues.put("CREATION_DATE", CREATION_DATE);
		initialValues.put("DESCR", DESCR);
		initialValues.put("RECORD_STATUS", RECORD_STATUS);
		initialValues.put("UPDATE_DATE", UPDATE_DATE);
		initialValues.put("ACT_STATUS", ACT_STATUS);
		initialValues.put("ACTIVTY_DATE", ACTIVTY_DATE);
		initialValues.put("EFFECTIVE_DATE", EFFECTIVE_DATE);
		initialValues.put("MOOD_CODE", MOOD_CODE);
		initialValues.put("TEXT", TEXTt);
		initialValues.put("VALUE", VALUE);
		initialValues.put("WAKD_MSG", WAKD_MSG);
		initialValues.put("CREATED_BY", CREATED_BY);
		initialValues.put("UPDATED_BY", UPDATED_BY);

		id =  db.insert(DATABASE_TABLE, null, initialValues);

		if (id != -1) {

			initialValues = new ContentValues();
			initialValues.put("CREATION_DATE", EVENTDATE);
			initialValues.put("DESCR", EVENTDESCR);
			initialValues.put("RECORD_STATUS", 1);
			initialValues.put("UPDATE_DATE", EVENTDATE);
			initialValues.put("OUTCOME", 1); 
			initialValues.put("CREATED_BY", CREATED_BY);
			initialValues.put("UPDATED_BY", UPDATED_BY);
			initialValues.put("ACT_ID", id);
			id = db.insert(DATABASE_TABLE3, null, initialValues);

		}

		return id;
	}

	public long insertAlert(String CREATION_DATE, String DESCR,
			int RECORD_STATUS, String UPDATE_DATE, String ALERT_DATE, int CODE,
			int SEVERITY, int CREATED_BY, int UPDATED_BY, int DEVICE_ID) {

		ContentValues initialValues = new ContentValues();
		initialValues.put("CREATION_DATE", CREATION_DATE);
		initialValues.put("DESCR", DESCR);
		initialValues.put("RECORD_STATUS", RECORD_STATUS);
		initialValues.put("UPDATE_DATE", UPDATE_DATE);
		initialValues.put("ALERT_DATE", ALERT_DATE);
		initialValues.put("CODE", CODE);
		initialValues.put("SEVERITY", SEVERITY);
		initialValues.put("CREATED_BY", CREATED_BY);
		initialValues.put("UPDATED_BY", UPDATED_BY);
		initialValues.put("DEVICE_ID", DEVICE_ID);
		return db.insert(DATABASE_TABLE2, null, initialValues);

	}

	public long insertAct(int ACT_TYPE, String CREATION_DATE, String DESCR,
			int RECORD_STATUS, String UPDATE_DATE, int ACT_STATUS,
			String ACTIVTY_DATE, String EFFECTIVE_DATE, int MOOD_CODE,
			String TEXTt, double VALUE, String WAKD_MSG, int CREATED_BY,
			int UPDATED_BY) {
		ContentValues initialValues = new ContentValues();
		initialValues.put("ACT_TYPE", ACT_TYPE);
		initialValues.put("CREATION_DATE", CREATION_DATE);
		initialValues.put("DESCR", DESCR);
		initialValues.put("RECORD_STATUS", RECORD_STATUS);
		initialValues.put("UPDATE_DATE", UPDATE_DATE);
		initialValues.put("ACT_STATUS", ACT_STATUS);
		initialValues.put("ACTIVTY_DATE", ACTIVTY_DATE);
		initialValues.put("EFFECTIVE_DATE", EFFECTIVE_DATE);
		initialValues.put("MOOD_CODE", MOOD_CODE);
		initialValues.put("TEXT", TEXTt);
		initialValues.put("VALUE", VALUE);
		initialValues.put("WAKD_MSG", WAKD_MSG);
		initialValues.put("CREATED_BY", CREATED_BY);
		initialValues.put("UPDATED_BY", UPDATED_BY);
		return db.insert(DATABASE_TABLE, null, initialValues);
	}

	// ---retrieves CurentState---
	public String getCurentState(int type) throws SQLException {
		Cursor mCursor = db.rawQuery("SELECT DESCR FROM act WHERE ACT_TYPE="
				+ type + " ORDER BY _id DESC", null);
		if (mCursor.moveToNext()) {
			return mCursor.getString(0);
		}
		return null;
	}

	// ---retrieves specific Measurement---
	public Double getMeasurement(int type) throws SQLException {
		Cursor mCursor = db.rawQuery("SELECT VALUE FROM act WHERE ACT_TYPE="
				+ type + " ORDER BY _id DESC", null);
		if (mCursor.moveToNext()) {
			Log.d("i mljia", "einai=" + mCursor.getDouble(0));
			Double measurement = mCursor.getDouble(0);
			mCursor.close();
			return measurement;
		}
		mCursor.close();
		return null;
	}

	// ---retrieves an Measurements of the given name---
	public double getDayMeasurements(String date, int type) throws SQLException {
		Cursor mCursor = db.rawQuery(
				"SELECT AVG(VALUE) FROM act WHERE date(CREATION_DATE) ='"
						+ date + "' AND ACT_TYPE=" + type, null);
		mCursor.moveToNext();
		return mCursor.getDouble(0);
	}

	// ---retrieves Today Measurements Until Now---
	public Cursor getTodayMeasurementsUntilNow(int type) throws SQLException {
		Cursor mCursor = db
				.rawQuery(
						"SELECT VALUE,CREATION_DATE FROM act WHERE ACT_TYPE="
								+ type
								+ " AND CREATION_DATE>(select date()) ORDER BY _id ASC",
						null);
		return mCursor;
	}

	// ---retrieves Weekly Measurements Until Now---
	public Cursor getWeeklyMeasurements(int type) throws SQLException {
		Cursor mCursor = db
				.rawQuery(
						"SELECT AVG(VALUE),strftime('%d/%m', CREATION_DATE),_id FROM ACT WHERE ACT_TYPE="
								+ type
								+ " GROUP BY date(CREATION_DATE) ORDER BY _id DESC LIMIT 7",
						null);
		return mCursor;
	}

	// ---retrieves all the Unchecked Alerts---
	public Cursor getUncheckedAlerts() throws SQLException {
		Cursor mCursor = db.query("alert", new String[] { "_id", "CODE" },
				"RECORD_STATUS=1", null, null, null, null, null);
		return mCursor;
	}

	// ---retrieves all the All Alerts---
	public Cursor getAllAlerts() throws SQLException {
		Cursor mCursor = db
				.rawQuery(
						"SELECT _id,CODE,CREATION_DATE,RECORD_STATUS FROM alert ORDER BY _id DESC",
						null);
		return mCursor;
	}

	// ---retrieves an Alert---
	public int getAlert(String datetime) throws SQLException {
		Cursor mCursor = db.query("alert", new String[] { "_id" },
				"CREATION_DATE='" + datetime + "'", null, null, null, null,
				null);
		mCursor.moveToNext();

		int alertid = mCursor.getInt(0);
		mCursor.close();
		return alertid;
	}

	// ---updates an alert---
	public boolean updateAlertStatus(int rowId, int status) {
		ContentValues args = new ContentValues();
		args.put("RECORD_STATUS", status);
		return db.update("alert", args, "_id=" + rowId, null) > 0;
	}

	
	// --- retrieves all Events of that date ---
		public Cursor getEvents(String date) throws SQLException {
			Cursor mCursor = db
					.rawQuery(
							"SELECT strftime('%H:%M', CREATION_DATE),DESCR FROM scheduledTask WHERE date(CREATION_DATE) = '"+date+"' ORDER BY  time(CREATION_DATE)  ASC",
							null);
			return mCursor;
		}
 
}
