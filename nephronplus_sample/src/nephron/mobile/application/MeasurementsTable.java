package nephron.mobile.application;
 

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Display;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class MeasurementsTable extends Activity {

	private String filter = "nephron.mobile.application.PhysiologicalMeasurementEvent";

	private IncomingReceiver receiver;
	private ListView dailyMeasurementsListView, weeklyMeasurementsListView;
	private TextView dailyTableTextView, weeklyTableTextView;
	ArrayAdapter<String> dailyDataAdapter, weeklyDataAdapter;
	LinkedList<String> dailyMeasurements;
	List<String> weeklyMeasurements;
	LinearLayout dailyLinear, weeklyLinear;
	int code;
	String head1, head2, dailyMeasurement, weeklyMeasurement, unit="";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementstablelayout);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		registerReceiver(receiver, intentFilter);

		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated
		
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		if (code != 931) {
             unit = " mmol/L";
		}

		dailyMeasurementsListView = (ListView) findViewById(R.id.DailyMeasurementsListView);
		weeklyMeasurementsListView = (ListView) findViewById(R.id.WeeklyMeasurementsListView);
		dailyLinear = (LinearLayout) findViewById(R.id.DailyLinear);
		weeklyLinear = (LinearLayout) findViewById(R.id.WeeklyLinear);
		dailyTableTextView = (TextView) findViewById(R.id.DailyTableTextView);
		weeklyTableTextView = (TextView) findViewById(R.id.WeeklyTableTextView);
		dailyTableTextView.setText(head1);
		weeklyTableTextView.setText(head2);

		LinearLayout.LayoutParams dailyparams = (LayoutParams) dailyLinear
				.getLayoutParams();
		dailyparams.height = height / 2;
		dailyLinear.setLayoutParams(dailyparams);

		LinearLayout.LayoutParams weeklyparams = (LayoutParams) weeklyLinear
				.getLayoutParams();
		weeklyparams.height = height / 2;
		weeklyLinear.setLayoutParams(weeklyparams);

		dailyMeasurements = new LinkedList<String>();
		weeklyMeasurements = new ArrayList<String>();

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		if (c.getCount() == 0) {
			dailyMeasurements.addFirst("No Data Yet");
		}
		while (c.moveToNext()) {
			dailyMeasurement = c.getString(1) + "    " +   String.format("%.1f", c.getDouble(0) )  + unit;
			dailyMeasurements.addFirst(dailyMeasurement);
		}

		c.close();

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		while (c2.moveToNext()) {
			weeklyMeasurement = c2.getString(1) + "    " +  String.format("%.1f", c2.getDouble(0) )
					+ unit;
			weeklyMeasurements.add(weeklyMeasurement);
		}
		c2.close();

		dailyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				dailyMeasurements);

		dailyMeasurementsListView.setAdapter(dailyDataAdapter);

		weeklyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				weeklyMeasurements);

		weeklyMeasurementsListView.setAdapter(weeklyDataAdapter);
	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(filter)) {

				if (intent.hasExtra("physiologicalData")) {
					Bundle b = intent.getBundleExtra("physiologicalData");
					String Sodium = b.getString("Sodium");
					String Potassium = b.getString("Potassium");
					String Urea = b.getString("Urea");
					String pH = b.getString("pH");
					switch (code) {
					case 911:
						dailyMeasurement = Sodium;
						break;
					case 921:
						dailyMeasurement = Potassium;
						break;
					case 941:
						dailyMeasurement = Urea;
						break;
					case 931:
						dailyMeasurement = pH;
						break;
					default:
						break;
					}
					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}

			}

		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}
