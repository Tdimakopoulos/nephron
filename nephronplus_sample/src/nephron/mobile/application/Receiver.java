package nephron.mobile.application;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date; 
import java.util.List;
import nephron.mobile.datafunctions.AlarmsMsg;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;  
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle; 
import android.util.Log;

public class Receiver extends BroadcastReceiver {

	String current_State;
	MessageConverter converter ;
	boolean wait_for_update = false;
	public  List<AlarmsMsg> alerts; 
	
	@Override
	public void onReceive(Context context, Intent intent) {
		converter = new MessageConverter();
		String action = intent.getAction(); 
		
		if (action.equals("nephron.mobile.application.DataArrived")) {
			byte[] incomingMessage = intent.getByteArrayExtra("value");
			final SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
					
			
	  	if (incomingMessage != null) {
	  		
//	  		DIALOG JUST FOR TESTING
		/*    Intent testing = new Intent("android.intent.action.MAIN");
		    testing.setClass(context, infoDialog.class);
		    testing.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
		    testing.putExtra("dataArrived",incomingMessage);
    	    context.startActivity(testing);     */ 
  		
	  		    if (unsignedByteToInt(incomingMessage[3]) == 5) {  // ACK
	    	    	Intent i2 = new Intent("nephron.mobile.application.ACK"); 
	    	    	i2.putExtra("messageCount", unsignedByteToInt(incomingMessage[4])); 
	    	    	context.sendBroadcast(i2);
	  		    }
	  		    else if (unsignedByteToInt(incomingMessage[3]) == 4) { // CurrentWakdStatus
					 if(incomingMessage.length == 8){
					// send Ack
					WakdCommandEnum command = WakdCommandEnum.ACK;
					MsgSimple ack = new MsgSimple(incomingMessage[4], command);
					try {
						ConnectionService.u.sendDatatoWAKD(ack.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/*
					current_State = converter.BytesToString(a);
					if (wait_for_update) {
						// Kanoume update to status tou command
						PutMethod method3 = new PutMethod(
								"http://rdocs.exodussa.com:8080/nephron-restful/command/update?commandID="
										+ id_command
										+ "&statusID=2&message="
										+ current_State + "&userID=1");// 01000a0b010404010101
																		// 0400080407020401
						try {
							client.executeMethod(method3);
						} catch (HttpException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Log.d("!!Steilame kai to commadara!!!",
								"!!!Steilame kai to commadara!!!");
						wait_for_update = false;
					}  
					 * else { // ----- stelnoume current State -----
					 * PostMethod method1 = new PostMethod(
					 * "http://rdocs.exodussa.com:8080/nephron-restful/state/create?message="
					 * + current_State + "&userID=1&deviceID=3");
					 * Log.d("!!REEEEEEEEEEEEEEEEEEEEEEEEE!!!",
					 * current_State); try { client.executeMethod(method1);
					 * } catch (HttpException e) { // TODO Auto-generated
					 * catch block e.printStackTrace(); } catch (IOException
					 * e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); } Log.d("!!!!createState!!!",
					 * "!!!createState!!!"); // ----- steilame to current
					 * State -----
					 * 
					 * if (alarm != null) {
					 * 
					 * // PostMethod method2 = new // PostMethod(
					 * "http://192.168.3.19:8080/nephron-restful/command/alert?message="
					 * +alarm+"&userID=1&deviceID=3"); PostMethod method2 =
					 * new PostMethod(
					 * "http://rdocs.exodussa.com:8080/nephron-restful/alert/create?message="
					 * + alarm + "&userID=1&deviceID=3"); try {
					 * client.executeMethod(method2); } catch (HttpException
					 * e) { // TODO Auto-generated catch block
					 * e.printStackTrace(); } catch (IOException e) { //
					 * TODO Auto-generated catch block e.printStackTrace();
					 * }
					 * 
					 * Log.i("!!ALARMH!!", alarm); } }
					 */  
				 
					Date date = new Date();
					// Save WAKD Current State in db
					ConnectionService.db.insertAct(41, dateFormat.format(date), WakControl.wakdState(2, unsignedByteToInt(incomingMessage[6])), 2,
							dateFormat.format(date), 2, null, null, 4,
							null,  unsignedByteToInt(incomingMessage[6]),
							null, 1, 1);   
					
					// Save WAKD Current Operational State in db
					ConnectionService.db.insertAct(42, dateFormat.format(date), WakControl.wakdState(2, unsignedByteToInt(incomingMessage[7])), 2,
							dateFormat.format(date), 2, null, null, 4,
							null, unsignedByteToInt(incomingMessage[7]) ,
							null, 1, 1);    
					
					// Publish event with incoming states
					Bundle b = new Bundle();
    	    	 	b.putInt("wakdStateIs",unsignedByteToInt(incomingMessage[6]));
    	    	 	b.putInt("wakdOpStateIs",unsignedByteToInt(incomingMessage[7])); 
    	    		Intent i2 = new Intent("nephron.mobile.application.WakStatusEvent");
    	    		i2.putExtra("currentWakdState", b); 
    	    		context.sendBroadcast(i2);
	  		        }
				}

				else if (unsignedByteToInt(incomingMessage[3]) == 8) { // ALERT
					if(incomingMessage.length == 8){
					// send Ack
					WakdCommandEnum command = WakdCommandEnum.ACK;
					MsgSimple ack = new MsgSimple(incomingMessage[4], command);
					try {
						ConnectionService.u.sendDatatoWAKD(ack.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					byte[] alrm = new byte[] { incomingMessage[6], incomingMessage[7] };
					BigInteger inlet_sod = new BigInteger(alrm);
					String inlet_alrm = inlet_sod.toString(16);
					AlarmsMsg alarm_msg = new AlarmsMsg(Integer.parseInt(
							inlet_alrm, 16));

					final Date date = new Date();
					// Save Alert in db
					ConnectionService.db.insertAlert(dateFormat.format(date),
							alarm_msg.getmsgAlert(), 1,
							dateFormat.format(date),
							dateFormat.format(date),
							Integer.parseInt(inlet_alrm, 16), 2, 1, 1, 4);

					int alertid = ConnectionService.db.getAlert(dateFormat.format(date));
				 
					//Display Alert
			 	    Intent trIntent = new Intent("android.intent.action.MAIN");
	        		trIntent.setClass(context, MyAlertDialog.class);
	        		trIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
	        		trIntent.putExtra("alertid",Integer.toString(alertid));
	        		trIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
	        	    context.startActivity(trIntent);       
	        	    
					// send dataID_statusRequest
				/*	WakdCommandEnum command2 = WakdCommandEnum.STATUS_REQUEST;
					MsgSimple statusRequest = new MsgSimple(incomingMessage[4] + 1,
							command2);
					try {
						// Thread.sleep(10000); //10secs
						ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
				   }
				} else if (unsignedByteToInt(incomingMessage[3]) == 12) { // Physiological
					if(incomingMessage.length == 70){
					// send Ack
					WakdCommandEnum command = WakdCommandEnum.ACK;
					MsgSimple ack = new MsgSimple(incomingMessage[4], command);
					try { 
						ConnectionService.u.sendDatatoWAKD(ack.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			       
					// Get Measurements from incoming Message 
					byte[] inlet_Sodium = new byte[] { incomingMessage[10], incomingMessage[11] };
					BigInteger inlet_sod = new BigInteger(inlet_Sodium);
					String inlet_s = inlet_sod.toString(16);

					byte[] outlet_Sodium = new byte[] { incomingMessage[40], incomingMessage[41] };
					BigInteger outlet_sod = new BigInteger(outlet_Sodium);
					String outlet_s = outlet_sod.toString(16);

					byte[] inlet_Potassium = new byte[] { incomingMessage[16], incomingMessage[17] };
					BigInteger inlet_pot = new BigInteger(inlet_Potassium);
					String inlet_s1 = inlet_pot.toString(16);

					byte[] outlet_Potassium = new byte[] { incomingMessage[46], incomingMessage[47] };
					BigInteger outlet_pot = new BigInteger(outlet_Potassium);
					String outlet_s1 = outlet_pot.toString(16);

					byte[] inlet_pH = new byte[] { incomingMessage[22], incomingMessage[23] };
					BigInteger inlet_ph = new BigInteger(inlet_pH);
					String inlet_s2 = inlet_ph.toString(16);

					byte[] outlet_pH = new byte[] { incomingMessage[52], incomingMessage[53] };
					BigInteger outlet_ph = new BigInteger(outlet_pH);
					String outlet_s2 = outlet_ph.toString(16);

					byte[] inlet_Urea = new byte[] { incomingMessage[28], incomingMessage[29] };
					BigInteger inlet_ur = new BigInteger(inlet_Urea);
					String inlet_s3 = inlet_ur.toString(16);

					byte[] outlet_Urea = new byte[] { incomingMessage[58], incomingMessage[59] };
					BigInteger outlet_ur = new BigInteger(outlet_Urea);
					String outlet_s3 = outlet_ur.toString(16);

					byte[] inlet_Temperature = new byte[] { incomingMessage[34], incomingMessage[35] };
					BigInteger inlet_temp = new BigInteger(
							inlet_Temperature);
					String inlet_s4 = inlet_temp.toString(16);

					byte[] outlet_Temperature = new byte[] { incomingMessage[66], incomingMessage[65] };
					BigInteger outlet_temp = new BigInteger(
							outlet_Temperature);
					String outlet_s4 = outlet_temp.toString(16);

				

					Date date = new Date();
					
					// Save incoming inlet Sodium
					ConnectionService.db.insertAct(911, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(inlet_s, 16)) / 10, null, 1, 1);
					
					// Save incoming outlet Sodium
					ConnectionService.db.insertAct(912, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(outlet_s, 16)) / 10,
							null, 1, 1);
					
					// Save incoming inlet Potassium
					ConnectionService.db.insertAct(921, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(inlet_s1, 16)) / 10, null, 1, 1);
					
					// Save incoming outlet Potassium
					ConnectionService.db.insertAct( 922, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4, null, ((double) Integer.parseInt(outlet_s1, 16)) / 10,
							null, 1, 1);
					
					// Save incoming inlet pH
					ConnectionService.db.insertAct(931, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(inlet_s2, 16)) / 10, null, 1, 1);
					
					// Save incoming outlet pH
					ConnectionService.db.insertAct( 932, dateFormat.format(date), null, 2, dateFormat.format(date),
							2, null, null, 4, null, ((double) Integer.parseInt(outlet_s2, 16)) / 10, null, 1, 1);
					
					// Save incoming inlet Urea
					ConnectionService.db.insertAct(941, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(inlet_s3, 16)) / 10, null, 1, 1);
					
					// Save incoming outlet Urea
					ConnectionService.db.insertAct( 942, dateFormat.format(date), null, 2, dateFormat.format(date), 2,
							null, null, 4, null, ((double) Integer.parseInt(outlet_s3, 16)) / 10, null, 1, 1);
					
					// Save incoming inlet Temperature
					ConnectionService.db.insertAct(951, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(inlet_s4, 16)) / 10, null, 1, 1);
					
					// Save incoming outlet Temperature
					ConnectionService.db.insertAct( 952, dateFormat.format(date), null, 2, dateFormat.format(date), 2,
							null, null, 4, null, ((double) Integer.parseInt(outlet_s4, 16)) / 10, null, 1, 1);

					//publish Temperature
					Intent i = new Intent("nephron.mobile.application.PhysicalMeasurementEvent");
					i.putExtra("temperature",((double) Integer.parseInt(inlet_s4, 16)) / 10);
    	    		context.sendBroadcast(i);
    	    		
    	    		// publish Sodium,Potassium,Urea,pH
    	    		Bundle b = new Bundle();  
    	    	 	b.putString("Sodium", dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s, 16)) / 10 +" mmol/L");
    	    	 	b.putString("Potassium",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s1, 16)) / 10 +" mmol/L"); 
    	    	 	b.putString("Urea",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s3, 16)) / 10+" mmol/L"); 
    	    	 	b.putString("pH",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s2, 16)) / 10); 
    	    		Intent i2 = new Intent("nephron.mobile.application.PhysiologicalMeasurementEvent");
    	    		i2.putExtra("physiologicalData", b); 
    	    		context.sendBroadcast(i2);

			     	}
    	    		
				} else if (unsignedByteToInt(incomingMessage[3]) == 22) { // Weight
					if(incomingMessage.length == 19){ 
					// send Ack
					WakdCommandEnum command = WakdCommandEnum.ACK;
					MsgSimple ack = new MsgSimple(incomingMessage[4], command);
					try {
						ConnectionService.u.sendDatatoWAKD(ack.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// Get Measurements from incoming Message 
					byte[] Weight = new byte[] { incomingMessage[6], incomingMessage[7] };
					BigInteger bi = new BigInteger(Weight);
					String s = bi.toString(16);

					byte[] WSBatteryLevel = new byte[] { incomingMessage[9], incomingMessage[10] };
					BigInteger bat = new BigInteger(WSBatteryLevel);
					String s1 = bat.toString(16);

					byte[] WSStatus = new byte[] { incomingMessage[11], incomingMessage[12], incomingMessage[13],
							incomingMessage[14] };
					BigInteger status = new BigInteger(WSStatus);
					String s2 = status.toString(16);

	

					Date date = new Date();
					// Save Weight in db
					ConnectionService.db.insertAct(181, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(s, 16)) / 10,
							null, 1, 1);
					
					// Save Body Fat
					ConnectionService.db.insertAct(182, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, ((double) unsignedByteToInt(incomingMessage[8])) ,
							null, 1, 1);
					
					// Save WAKD Battery Level db
					ConnectionService.db.insertAct(183, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(s1, 16)),
							null, 1, 1);
					
					// Save WSStatus in db
					ConnectionService.db.insertAct(184, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, ((double) Integer.parseInt(s2, 16)),
							null, 1, 1);
					
					// publish Battery Level
					Intent i = new Intent("nephron.mobile.application.WakStatusEvent");
					i.putExtra("WSBatteryLevel",(int) Integer.parseInt(s1, 16));
    	    		context.sendBroadcast(i);
     		
    	    		// publish weight,bodyfat
    	    	 	Bundle b = new Bundle();
    	    	 	b.putDouble("weight",((double) Integer.parseInt(s, 16)) / 10);
    	    	 	b.putDouble("bodyfat",((double) unsignedByteToInt(incomingMessage[8]))); 
    	    		Intent i2 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent");
    	    		i2.putExtra("weight_bodyfat", b); 
    	    		context.sendBroadcast(i2);
    	    		
    	    		// publish the hole Physical Measurement Message
    	    		Intent i3 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent1");
					i3.putExtra("message",incomingMessage);
    	    		context.sendBroadcast(i3);
			     	}
				}
				
				else if (unsignedByteToInt(incomingMessage[3]) == 27) { // EcgData
					if(incomingMessage.length == 8){
					// send Ack
					WakdCommandEnum command = WakdCommandEnum.ACK;
					MsgSimple ack = new MsgSimple(incomingMessage[4], command);
					try {
						ConnectionService.u.sendDatatoWAKD(ack.encode());
						Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
					Date date = new Date();
					// Save Pulse
					ConnectionService.db.insertAct(231, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, (unsignedByteToInt(incomingMessage[6])),
							null, 1, 1);
					
					// Save Respiration Rate
					ConnectionService.db.insertAct(232, dateFormat.format(date), null, 2,
							dateFormat.format(date), 2, null, null, 4,
							null, (unsignedByteToInt(incomingMessage[7])),
							null, 1, 1);
					
					// publish Physical Measurements
					Bundle b = new Bundle();
    	    	 	b.putDouble("pulse",((double) unsignedByteToInt(incomingMessage[6])) / 10);
    	    	 	b.putDouble("respiration",((double) unsignedByteToInt(incomingMessage[7])) / 10); 
    	    		Intent i2 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent");
    	    		i2.putExtra("pulse_respiration", b); 
    	    		context.sendBroadcast(i2);
    	    		
				   }
				}

			}

		}

		// if there are unchecked Alerts display them on screen
		if (action.equals("nephron.mobile.application.AlertsReminder")) {
			alerts =   (List<AlarmsMsg>) intent.getExtras().get("value"); 
			for (int i = 0; i < alerts.size(); i++) {
              final int count=i;
 			    Intent trIntent = new Intent("android.intent.action.MAIN");
        		trIntent.setClass(context, MyAlertDialog.class);
        		trIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
        		trIntent.putExtra("alertid",Integer.toString(alerts.get(count).getIdDtabase()));
        		trIntent.putExtra("alertMessage",alerts.get(count).getmsgPatient());
        	    context.startActivity(trIntent);       
			}
	 

		}
		
	}
	
	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

}
