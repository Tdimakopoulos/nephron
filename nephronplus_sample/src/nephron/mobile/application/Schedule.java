package nephron.mobile.application;


import nephron.mobile.calendar.CalendarActivity;
import nephron.mobile.calendar.CalendarView;
import nephron.mobile.calendar.CalendarView.OnCellTouchListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.widget.TextView;

public class Schedule extends Activity
{
	CalendarView mView = null;
	public static final String MIME_TYPE = "vnd.android.cursor.dir/date";
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedulelayout);
				
		mView = (CalendarView)findViewById(R.id.calendar);
	    mView.setOnCellTouchListener((OnCellTouchListener) this);
				
		TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
        monthText.setText(DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
		
        startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
	}
}

