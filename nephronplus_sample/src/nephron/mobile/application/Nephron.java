package nephron.mobile.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;    
import android.app.Activity;
import android.app.AlertDialog; 
import android.content.Intent;   
import android.view.View;  
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler; 
import nephron.mobile.calendar.CalendarActivity;
import nephron.mobile.datafunctions.*;

public class Nephron extends Activity {

	MyDataListener mylistener;
	Thread alerthandler; 
	AlertDialog alert;
	AlertDialog.Builder alertd;
	ConnectionHandler connectionhandler;
	Handler mHandler;
	WakdCommandEnum command;
	MsgSimple statusRequest;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
  	
		 //deleteDatabase("nephron");
		// Start Connection Service with WAKD
        if(!ConnectionService.connection) {
		Intent start = new Intent("nephron.mobile.application.ConnectionService");
		this.startService(start);      
        }
 
	}
	


	protected void onDestroy() {
		super.onDestroy();

		try {
			backupDatabase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.ButtonMeasurements: // Show list of measurements
			Intent openListOfMeasurements = new Intent(Nephron.this,
					MeasurementOption.class);
			startActivity(openListOfMeasurements);
			break;
		case R.id.WAKStatusButton: // Show WAKD status
			Intent openWakStatus = new Intent(Nephron.this, WakStatus.class);
			startActivity(openWakStatus);
			break;
		case R.id.WAKControlButton: // Show WAKD settings
			Intent openWakControl = new Intent(Nephron.this, WakControl.class);
			startActivity(openWakControl);
			break;
		case R.id.ScheduleButton: // Show patient schedules
			startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null,
					CalendarActivity.MIME_TYPE));
			break;
		case R.id.QuestionnairesButton: // Show questionnaires
			Intent openQuestionnairesControl = new Intent(Nephron.this,
					Questionnaire.class);
			startActivity(openQuestionnairesControl);
			break;
		case R.id.SettingsButton: // Show application settings
			Intent openSettingsControl = new Intent(Nephron.this,
					Settings.class);
			startActivity(openSettingsControl);
			break;
		case R.id.HistoryButton: // Show history
			Intent openHistory = new Intent(Nephron.this, History.class);
			startActivity(openHistory);
			break;
		case R.id.ExitImageButton:
			this.finish();
			System.exit(1);
		default:
			break;
		}

	}

	//Create backup of Database on Smartphone
	public static void backupDatabase() throws IOException {
		// Open your local db as the input stream
		String inFileName = "/data/data/nephron.mobile.application/databases/nephron";
		File dbFile = new File(inFileName);
		FileInputStream fis = new FileInputStream(dbFile);

		String outFileName = Environment.getExternalStorageDirectory()
				+ "/nephron";
		// Open the empty db as the output stream
		OutputStream output = new FileOutputStream(outFileName);
		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = fis.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}
		// Close the streams
		output.flush();
		output.close();
		fis.close();
	}
}
