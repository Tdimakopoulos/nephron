package nephron.mobile.application;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import nephron.mobile.datafunctions.DBAdapter;
import nephron.mobile.datafunctions.MsgChangeStateCommand;
import nephron.mobile.datafunctions.MyDataListener;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import de.imst.nephron.bt.BluetoothAbstractionLayer;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

public class ConnectionService extends Service {
	static BluetoothAbstractionLayer u;
	static boolean connection = false;
	AlertDialog alert;
	AlertDialog.Builder alertd;
	WakdCommandEnum command;
	MsgSimple statusRequest;
	MyDataListener mylistener;
	Thread alertHandler,currentStateHandler;
	public static DBAdapter db;
	public static boolean alive;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	// Start Service
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Service Started","Service Started");
		//create db
		db = new DBAdapter(this);
        db.open();
     // delete db
	 //	deleteDatabase("nephron");
        alive = true;
		u = new BluetoothAbstractionLayer();
		currentStateHandler = new CurrentStateHandler(this);
		try {
			u.init(); 
		    connection = true;
			 
		    //if connection established Send STATUS_REQUEST to WAKD
	        command = WakdCommandEnum.STATUS_REQUEST;
		    statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
			try {
				// Thread.sleep(10000); //10secs
				u.sendDatatoWAKD(statusRequest.encode());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		  
   		// ask for Current State Every 10 sec
		// currentStateHandler.start();
 			 
		} catch (IOException e) {
			// TODO Auto-generated catch block

			//if Bluetooth not Activated open it
			if (e.getMessage() == "Bluetooth not Activated!") { 			
				Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
				infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
				infoDialog.putExtra("BluetoothMessage","Your Bluetooth is not Activated!");
	    	    startActivity(infoDialog);    
			} else if (e.getMessage() == "Service discovery failed") {

				// connectionhandler=new ConnectionHandler();
				// connectionhandler.start();
			}

			e.printStackTrace();
		}
 	
		
		// if there is no connection try to connect every 2 sec
		if (!connection) {
			final Timer t = new Timer();
			t.schedule(new TimerTask() {
				public void run() {
					Looper.myLooper().prepare();
					int connectionAttempt = 0;
					while (!connection) {
						Log.d("!!!!NO CONNECTION!!!!!!!!",
								"!!!!!!!!NO CONNECTION!!!!!!!!!");
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							u.init();
							connection = true;
							Intent i = new Intent("nephron.mobile.application.WakStatusEvent");
							i.putExtra("connection","Active");
					    	sendBroadcast(i);
					     
					    	//if connection established Send STATUS_REQUEST to WAKD
					        command = WakdCommandEnum.STATUS_REQUEST;
							statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
							try {
								// Thread.sleep(10000); //10secs
								u.sendDatatoWAKD(statusRequest.encode());
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} 
							
							Log.d("!!!!YESSSSSSSSSSSS!!!!!!!!","!!!!!!!!YESSSSSSSSSSSS!!!!!!!!!");
							//Sow dialog if connection established
							Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
							infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
							infoDialog.putExtra("message","Connection Established with WAKD!!");
				    	    startActivity(infoDialog);      
				    	    
				    		// ask for Current State Every 10 sec 
						 	// currentStateHandler.start();
				    	    
						} catch (IOException e) {
							//Sow dialog if connection not established after 4 attempts
							if(connectionAttempt == 4){
								Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
								infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
								infoDialog.putExtra("message","Connection failed to WAKD!!");
					    	    startActivity(infoDialog);     
							}
						}
						connectionAttempt++;
					}

					// t.cancel();
				}
			}, 2000);
		}else{
			//Sow dialog if connection established
			Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
			infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
			infoDialog.putExtra("message","Connection Established with WAKD!!");
    	    startActivity(infoDialog);      
		}
     
		// Set dataListener from WAKD
		mylistener = new MyDataListener(this);
		u.setDataListener(mylistener);
		
		// Start thread to check for unchecked alerts
		alertHandler = new AlertHandler(this);
		alertHandler.start();
 	
		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("Service stopped","Service stopped");
		alive = false;
		connection = false;
        db.close();
	}

}
