package nephron.mobile.application;

import de.imst.nephron.bt.BluetoothAbstractionLayer;
import android.app.Application;

public class GlobalVar extends Application {

	private int messageCounter = 1;
	private BluetoothAbstractionLayer u;

	public int getMessageCounter() {
		return messageCounter++;
	}

	public BluetoothAbstractionLayer getBluetoothAbstractionLayer() {
		return u;
	}
	
	public void setBluetoothAbstractionLayer(BluetoothAbstractionLayer u) {
		this.u=u;
	}
/*	public void onCreate() {

	}*/

}