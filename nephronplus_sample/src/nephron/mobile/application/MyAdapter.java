package nephron.mobile.application;

import java.util.ArrayList;
import java.util.List;
import nephron.mobile.datafunctions.AlarmsMsg;
import android.app.Activity;
import android.app.AlertDialog;
import android.widget.BaseAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

class MyAdapter extends BaseAdapter {

	private List<AlarmsMsg> items = new ArrayList<AlarmsMsg>();
	public final String tag = "RSSReader";
	private LayoutInflater inflater;
	private Context context;
	int id;
	int len = 0;
	byte[] data1 = new byte[1024];
	AlertDialog alert;
	AlertDialog.Builder alertd;
	TextView alarms;

	public MyAdapter(Context context1) {
		context = context1;
		inflater = LayoutInflater.from(context1);
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		v = inflater.inflate(R.layout.listview, null);

		final AlarmsMsg o = items.get(position);
		if (o != null) {

			alarms = (TextView) v.findViewById(R.id.backtextview);
			alarms.setText(o.getmsgPatient() + "\n" + o.getDate());
			if (o.getStatus() == 1) {
				alarms.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.delete, 0);
				alarms.setTextColor(Color.RED);
				alarms.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						final View v=view;
						alertd = new AlertDialog.Builder(context);
						alertd.setTitle("Acknowledge Alarm");
						alertd.setMessage("Acknowledge Alarm :"+ o.getmsgPatient()+" send on "+o.getDate());
						alertd.setPositiveButton("Acknowledge",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										ConnectionService.db.updateAlertStatus(o.getIdDtabase(),
												2);
 									Intent itemintent = new Intent(v.getContext(),Alarms.class);
					 			       	((Activity) context).startActivityForResult(itemintent, 0);
										((Activity) context).overridePendingTransition(0, 0);
										((Activity) context).finish();
									}
								});
						alertd.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
									}
								});

						alert = alertd.create();
						alert.show();
					}

				});
				
 
			}
		}
		return v;
	}

	public void onClick(View arg0) {
		alarms.setText("My text on click");  
	    }
	
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	public AlarmsMsg getItem(int position) {
		// TODO Auto-generated method stub
		return items.get(position);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addOrder(List<AlarmsMsg> order) {
		this.items = order;
	}

	public void clear() {
		this.items.clear();
	}

}
