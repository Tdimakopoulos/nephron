package nephron.mobile.application;


import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.app.AlertDialog;


public class MyAlertDialog extends Activity {
	AlertDialog alert;
	String alertid, alertMessage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(0, 0);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			alertid = extras.getString("alertid");
			alertMessage = extras.getString("alertMessage");
		}

		AlertDialog.Builder alertd = new AlertDialog.Builder(this);
		alertd.setTitle("ALARM");
		alertd.setMessage(alertMessage);
		alertd.setPositiveButton("Accept",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Log.d("!VALUE!", alertid);
						alert.dismiss();
						ConnectionService.db.updateAlertStatus(Integer.parseInt(alertid),
								2);
						finish();
						overridePendingTransition(0, 0);
					}
				});
		alert = alertd.create();
		alert.show();

		final Timer t = new Timer();
		t.schedule(new TimerTask() {
			public void run() {
				alert.dismiss();
				finish();
				overridePendingTransition(0, 0);
				t.cancel();
			}
		}, 5000);

	}

}
