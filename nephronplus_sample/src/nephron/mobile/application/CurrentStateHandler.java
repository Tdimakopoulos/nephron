package nephron.mobile.application;
 
import java.io.IOException;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Service;

public class CurrentStateHandler extends Thread {
	
	Service service;  
	WakdCommandEnum command;
	MsgSimple statusRequest;

	public CurrentStateHandler(Service y) {
    	this.service = y;
	}
	
	public void run() {
		 WakdCommandEnum command = WakdCommandEnum.CHANGE_STATE_FROM_TO;
		 while (ConnectionService.alive) {
			 
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				    command = WakdCommandEnum.STATUS_REQUEST;
				    statusRequest = new MsgSimple(((GlobalVar) service.getApplication()).getMessageCounter(), command);
					try {
						// Thread.sleep(10000); //10secs
						ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}  
		 }
		
		    
	}

}
