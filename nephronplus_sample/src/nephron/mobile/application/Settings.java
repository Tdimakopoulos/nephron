package nephron.mobile.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.content.SharedPreferences;

public class Settings extends Activity { 

	private RadioGroup radioMeasurementGroup;
	private RadioButton radioMeasurementButton;
	private Button save,cancel;
	SharedPreferences userSettings;
	SharedPreferences.Editor prefsEditor;
	AlertDialog.Builder confirmation;
	TextView myMsg;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingslayout);


		  userSettings = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
          prefsEditor = userSettings.edit();
          String selection = userSettings.getString("MeasurementPresentation", "Graphical");
           
   
        confirmation = new AlertDialog.Builder(this);
        radioMeasurementGroup=(RadioGroup) findViewById(R.id.MeasurementPresentationGroup);
        save = (Button) findViewById(R.id.SaveSettingsButton);
        cancel = (Button) findViewById(R.id.CancelSettingsButton);
        
        if(selection.equals("Graphical")){
        	radioMeasurementGroup.check(R.id.GraphicalRadioButton);
        }else{
        	radioMeasurementGroup.check(R.id.TableRadioButton);
        }
        
        save.setOnClickListener(new OnClickListener() {
        	 
    		@Override
    		public void onClick(View v) { 
      
    		        // get selected radio button from radioGroup
    			int selectedId = radioMeasurementGroup.getCheckedRadioButtonId();
     
    			// find the radiobutton by returned id
    			radioMeasurementButton = (RadioButton) findViewById(selectedId);
    			
        		prefsEditor.putString("MeasurementPresentation", (String) radioMeasurementButton.getText()); 
    	        prefsEditor.commit();
    	       
    	        myMsg = new TextView(v.getContext());
    	        myMsg.setText("Your changes has been successfully saved.");
    	        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
    	        myMsg.setTextSize(17);
    	        myMsg.setHeight(40);
    	        confirmation.setView(myMsg);
    	        confirmation.setPositiveButton("Ok", null).show();
 
    	      /*  confirmation
				.setMessage("Your data has been successfully saved.")
				.setPositiveButton("Ok", null).show();
    	        TextView messageView = (TextView)confirmation.findViewById(R.id.message);
    	        messageView.setGravity(Gravity.CENTER);*/
     
    		}
     
    	});
        
        cancel.setOnClickListener(new OnClickListener() {
        	@Override
      		public void onClick(View v) { 
         	    finish();
      	 	}
       
      	});

	}
}
