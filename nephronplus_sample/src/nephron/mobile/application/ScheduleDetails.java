package nephron.mobile.application;

import java.text.SimpleDateFormat; 
import java.util.Date;
import java.util.LinkedList;  
import nephron.mobile.calendar.CalendarActivity;
import nephron.mobile.calendar.CalendarView; 
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

public class ScheduleDetails extends Activity
{
	CalendarView mView = null;
	AlertDialog.Builder setScheduleEvent,alertDialog;
	private TimePickerDialog timePickDialog = null;
	EditText timeSelection,userDetails;
	String timeSelectionInput,userDetailsInput;
	LayoutInflater li;
	View setsSheduleventView;
    SimpleDateFormat dateFormat;
	private ListView eventsListView;  
	ArrayAdapter<String> eventListAdapter;
	private LinkedList<String> eventsList;
	Cursor c;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scheduledetailslayout);
		
		mView = (CalendarView)findViewById(R.id.calendar);
	    dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    
	    eventsListView = (ListView)findViewById(R.id.EventsListView);
	    eventsList = new LinkedList<String>();
	   
	   
	    c =   ConnectionService.db.getEvents(CalendarActivity.dateForDB);
	     while (c.moveToNext()) { 
 	    	eventsList.add(c.getString(0) + " " + c.getString(1)); 
		}
		c.close();
		
		  eventListAdapter = new ArrayAdapter<String>(this,R.layout.backgroundmeauseremntsrow, R.id.backtextview,eventsList);
		  eventsListView.setAdapter(eventListAdapter);
		
		TextView dateText = (TextView)findViewById(R.id.DateTitleTextBox);
		dateText.setText (CalendarActivity.date); 
		
	    li = LayoutInflater.from(this);  
	    
	    alertDialog =  new AlertDialog.Builder(this);
	    alertDialog.setTitle("Alert").setMessage(" Please Insert Correct data ");
	    alertDialog.setPositiveButton("Ok",null);
	}
	
	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.NewEventImageButton: // New Event
			setsSheduleventView = li.inflate(R.layout.setschedulevent, null);
			timeSelection = (EditText) setsSheduleventView.findViewById(R.id.UserTimeInput);
			userDetails = (EditText) setsSheduleventView.findViewById(R.id.UserDetailsInput);
		  	timeSelection.setOnClickListener(new OnClickListener() {
		 		@Override
			    public void onClick(View v) {
					timePickDialog = new TimePickerDialog(v.getContext(),
							new TimePickHandler(), 8, 00, true);
		 			  timePickDialog.show();
			    }
			});
			setScheduleEvent = new AlertDialog.Builder(this);
			setScheduleEvent.setView(setsSheduleventView);
			setScheduleEvent.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							timeSelectionInput = timeSelection.getText().toString();
							userDetailsInput = userDetails.getText().toString();
							Log.d(" Test!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
							
	 						 if(timeSelectionInput.equals("--:--") || userDetailsInput.length() < 1){
								 alertDialog.show();
							 }else{
									Log.d(" Ok!!!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
									Date date = new Date();
									// Insert Event into the db 
									ConnectionService.db.insertScheduledTask(0, dateFormat.format(date), null, 2,
											dateFormat.format(date), 2, null, null, 4,
											null,  0,
											null, 1, 1, CalendarActivity.dateForDB+" "+timeSelectionInput, userDetailsInput);  
									finish();
									Intent scheduleDetails = new Intent(ScheduleDetails.this, ScheduleDetails.class);
									startActivity(scheduleDetails);
									overridePendingTransition(0, 0);
							 }
	 		 			}
					});
			
			setScheduleEvent.setNegativeButton("No",null);
			setScheduleEvent.show();		
			break;
		case R.id.CancelImageButton: // Cancel
	 
			break;
		default:
			break;
		}
	}
	
    private class TimePickHandler implements OnTimeSetListener {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        	timeSelection.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute) );
            timePickDialog.hide();
            
        }
    }
    
 
}
 