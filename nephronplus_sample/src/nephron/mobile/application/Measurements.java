package nephron.mobile.application;
 

import nephron.mobile.application.R;  
import nephron.mobile.statistics.Statistics;  
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.*;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener; 
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Measurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = { "Sodium",
			"Potassium", "Urea", "pH" };
	SharedPreferences myPrefs;
	String prefName;
	Bundle b;
	Intent openMeasurements;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementslayout);

		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical")) {
			openMeasurements = new Intent(Measurements.this, Statistics.class);
		} else {
			openMeasurements = new Intent(Measurements.this, MeasurementsTable.class);
		}

		_measurementsListView = (ListView) findViewById(R.id.MeasurementsListView);

		_measurementsListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.backgroundlayout, R.id.backtextview,
				_measurementsElementArray));

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// handle the various choices
				try {
					switch (position) {
					case 0: // Sodium
 	 
						b = new Bundle();
						b.putInt("code", 911);
						b.putString("header1", "Daily Sodium Trends");
						b.putString("header2", "Weekly Sodium Trends");
						openMeasurements.putExtra("values", b);
						startActivity(openMeasurements);
						break;

					case 1: // Potassium
 
						b = new Bundle();
						b.putInt("code", 921);
						b.putString("header1", "Daily Potassium Trends");
						b.putString("header2", "Weekly Potassium Trends");
						openMeasurements.putExtra("values", b);
						startActivity(openMeasurements);
						break;

					case 2: // Urea
 
						b = new Bundle();
						b.putInt("code", 941);
						b.putString("header1", "Daily Urea Trends");
						b.putString("header2", "Weekly Urea Trends");
						openMeasurements.putExtra("values", b);
						startActivity(openMeasurements);
						break;

					case 3: // pH
 
						b = new Bundle();
						b.putInt("code", 931);
						b.putString("header1", "Daily pH Trends");
						b.putString("header2", "Weekly pH Trends");
						openMeasurements.putExtra("values", b);
						startActivity(openMeasurements);
						break;
					default:
						break;
					}
				} catch (Exception ex) {
					AlertDialog _dialog = new AlertDialog.Builder(parent
							.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage(ex.getMessage());
					_dialog.setButton("OK",
							new DialogInterface.OnClickListener() {
								// Event of the OK button of the message box
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss(); // close dialog
								}
							});
					_dialog.show(); // show message box
				}
			}
		});

	}
}
