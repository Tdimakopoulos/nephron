package nephron.mobile.application;

import java.util.ArrayList;
import java.util.List;
import nephron.mobile.application.R;
import nephron.mobile.datafunctions.AlarmsMsg;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;

public class Alarms extends Activity 
{
	private ListView _measurementsListView;
	private MyAdapter adapter_listas;
	private List<AlarmsMsg> alarmsList;
	Cursor c;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout); 
		_measurementsListView = (ListView)findViewById(R.id.AlarmsListView);
		alarmsList = new ArrayList<AlarmsMsg>();
		adapter_listas = new MyAdapter(this);
		_measurementsListView.setAdapter(adapter_listas);
		  
		c =   ConnectionService.db.getAllAlerts();

		while (c.moveToNext()) { 
			AlarmsMsg das=new AlarmsMsg(c.getInt(0),c.getInt(1),c.getString(2),c.getInt(3)); 
     		alarmsList.add(das); 
		}
		c.close();
		 
     	ShowOrder((List<AlarmsMsg>)alarmsList);
		
 	}
	
	public void ShowOrder(final List<AlarmsMsg> alarm) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapter_listas.addOrder(alarm);
				adapter_listas.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adapter_listas.clear();
			}
		});
	}

}





