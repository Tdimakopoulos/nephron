package nephron.mobile.application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import nephron.mobile.datafunctions.AlarmsMsg;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

public class AlertHandler extends Thread implements Serializable {

	Context context;
	public static List<AlarmsMsg> alerts;

	public AlertHandler(Context y) {

		this.context = y;

	}

	// gets every 3 secs unchecked Alarms
	public void run() {
		Cursor c;
		AlarmsMsg msg;
		while (ConnectionService.alive) {
			 
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			alerts = new ArrayList<AlarmsMsg>();

			if (ConnectionService.db.isOpen()) {
				c = ConnectionService.db.getUncheckedAlerts();

				while (c.moveToNext()) {
					msg = new AlarmsMsg(c.getInt(1), c.getInt(0));
					alerts.add(msg);
				}

				Intent i = new Intent(
						"nephron.mobile.application.AlertsReminder");

				i.putExtra("value", (ArrayList<AlarmsMsg>) alerts);

				this.context.sendBroadcast(i);
				alerts.clear();
				c.close();
			} else {
			}

		}
	}
}
