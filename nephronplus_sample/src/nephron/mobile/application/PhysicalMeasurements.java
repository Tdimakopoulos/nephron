package nephron.mobile.application;
 
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter; 
import android.os.Bundle; 
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PhysicalMeasurements extends Activity {
	private ListView _wakStatusListView;
	private static final String _wakStatusElementArray[] = { "Blood Pressure: -",
			"Temperature: -", "Pulse Rate: -", "Weight: -", "Body Fat: -", "Respiration Rate: -" };
	
	private String filter = "nephron.mobile.application.PhysicalMeasurementEvent";
	
	private IncomingReceiver receiver;
	ArrayAdapter<String> dataAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakstatuslayout);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		registerReceiver(receiver, intentFilter);
		
		_wakStatusListView = (ListView) findViewById(R.id.WakStatusListView);
		dataAdapter=new ArrayAdapter<String>(this,
				R.layout.backgroundlayoutwithoutarows, R.id.backtextview,
				_wakStatusElementArray);
		_wakStatusListView.setAdapter(dataAdapter);

		Double temperature = ConnectionService.db.getMeasurement(951);
		Double pulse = ConnectionService.db.getMeasurement(231);
		Double weight = ConnectionService.db.getMeasurement(181);
		Double bodyfat = ConnectionService.db.getMeasurement(182);
		Double respiration = ConnectionService.db.getMeasurement(232);

		if (temperature != null) {
			_wakStatusElementArray[1] = "Temperature: " + temperature + "�C";
		}
		if (pulse != null) {
			_wakStatusElementArray[2] = "Pulse Rate: " + pulse + " /Minute";
		}
		if (weight != null) {
			_wakStatusElementArray[3] = "Weight: " + weight + " Kg";
		}
		if (bodyfat != null) {
			_wakStatusElementArray[4] = "Body Fat: " + bodyfat + "%";
		}
		if (respiration != null) {
			_wakStatusElementArray[5] = "Respiration Rate: " + respiration + " /Minute";
		}

	}
	
	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(filter)) {  
				 if(intent.hasExtra("weight_bodyfat")) {   
					Bundle b =  intent.getBundleExtra("weight_bodyfat");
 					double weight  = b.getDouble("weight");
					double bodyfat  = b.getDouble("bodyfat");
 						_wakStatusElementArray[3] = "Weight: " + weight + " Kg";
     					_wakStatusElementArray[4] = "Body Fat: " + bodyfat + "%";
     					dataAdapter.notifyDataSetChanged();
	 			}else if(intent.hasExtra("temperature")){
	 				 double temperature  = intent.getExtras().getDouble("temperature");
	 				_wakStatusElementArray[1] = "Temperature: " + temperature + "�C";
	 				dataAdapter.notifyDataSetChanged();
				} else if(intent.hasExtra("pulse_respiration")){ 
					Bundle b =  intent.getBundleExtra("pulse_respiration");
 					double pulse  = b.getDouble("pulse");
					double respiration  = b.getDouble("respiration");
 						_wakStatusElementArray[2] = "Pulse Rate: " + pulse + " /Minute";
     					_wakStatusElementArray[5] = "Respiration Rate: " + respiration + " /Minute";
     					dataAdapter.notifyDataSetChanged();
				}
 			}
		}

	}
	
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}