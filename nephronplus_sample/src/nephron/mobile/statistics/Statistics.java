package nephron.mobile.statistics;
 

import nephron.mobile.application.ConnectionService; 
import nephron.mobile.application.R; 
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle; 
import android.os.Handler;
import android.util.Log; 
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;

public class Statistics extends Activity {

	private WebView dailyWebView;
	private WebView weeklyWebView;
	private TextView DailyTrendTextView, AverageTrendTextView;
	private ScrollView scrolview;
	private String url = "file:///android_asset/dailygraph.html?",url2="file:///android_asset/weeklygraph.html?";
	private int code, i, count;
	private String head1, head2; 
	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.statisticslayout);
		handler = new Handler(); 
		i = 0;
		count=0;
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		scrolview = (ScrollView) findViewById(R.id.scrollview);
		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);
		DailyTrendTextView.setText(head1);
		AverageTrendTextView.setText(head2);
	
		// Load daily statistics
		dailyWebView = (WebView) findViewById(R.id.DailyWebView);
		dailyWebView.getSettings().setJavaScriptEnabled(true);
		
		// Load weekly statistics
		weeklyWebView = (WebView) findViewById(R.id.WeeklyWebView);
		weeklyWebView.getSettings().setJavaScriptEnabled(true);

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		while (c.moveToNext()) {
			url = url + "value=" +  c.getDouble(0)  + "&" + "time="
					+ c.getString(1) + "&";
		}
		c.close();
		url = url.substring(0, url.length() - 1);
		url = url.replaceAll("-", "/");
		url = url.replaceAll(" ", "%20");
		Log.d("!!PEIMENOUME DATABASE!!", url);

		// Load daily statistics
		dailyWebView.loadUrl(url);

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		while (c2.moveToNext()) {
			url2 = url2 + "value=" + c2.getDouble(0) + "&" + "date="
					+ c2.getString(1) + "&";
		}
		url2 = url2.substring(0, url2.length() - 1);
		c2.close();
		Log.d("!!PEIMENOUME DATABASE!!", url2);
		
		// Load weekly statistics
		weeklyWebView.loadUrl(url2);
		
		handler.post(mUpdate);
	}

	public Runnable mUpdate = new Runnable() {
		public void run() {
			url="file:///android_asset/dailygraph.html?";
			Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
			while (c.moveToNext()) {
				url = url + "value=" + c.getDouble(0) + "&" + "time="
						+ c.getString(1) + "&";
			}
			count = c.getCount();
			Log.d("!!counnttt!!", Integer.toString(count));
			c.close();
			url = url.substring(0, url.length() - 1);
			url = url.replaceAll("-", "/");
			url = url.replaceAll(" ", "%20");
			// Log.d("!!PEIMENOUME DATABASE!!", url);
			if (i < count) {
				Log.d("!!yohhhhhhhoooooooooo!!", "!!yohhhhhhhoooooooooo!!");

	 			dailyWebView.loadUrl(url);

				i = count;
         	}
			Log.d("!!cooooooooooouuuuuuuuuuuuunt!!",  url); 
			// Load weekly statistics
		 
			handler.postDelayed(this, 1000);

		}
	};
	
	protected void onDestroy() {
		super.onDestroy();
		handler.removeCallbacks(mUpdate); 
	}

}