// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   main.h
//! \brief  startup program
//!
//! Initializes devices and variables, executes main loop
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef __MAIN_H
#define __MAIN_H
// -----------------------------------------------------------------------------------
// Exported constants 
// -----------------------------------------------------------------------------------

#define HWBUILD                               // HW BUILD - This enables HW specific code, and disables VP-specific code

#define SDCRCDISABLE                          // CRC check for SD card causes error... disable...

// 2012 12 10
#define RTMCB_FW_96                           // New RTMCB firmware
//#define RTMCB_HANDLES_STATE                   // The RTMCB handles the states now...
// - - - - - 

#define DISABLE_SCC                           // disable the SCC task
#define DISABLE_REMINDER                      // disable the reminder task
//#define DISABLE_RTBPA                         // disable the RTPBA task

//#define DISABLE_FC                            // disable the flowcontrol task
#define DISABLE_DS                            // disable the datastorage task
#define DISABLE_PM1                           // disable the pm1 task
#define DISABLE_PM2                           // disable the pm2 task
//#define DISABLE_RTBINCOMING                   // disable ISR RTB sending messages

#define SIMPLIFIED_DISP                       // Simplified Dispatcher
#define SIMPLIFIED_FC                         // Simplify FC, by commenting out startup sequence

#define ECP_SENSOR_DUMMY_INTEGRATION          //We dont have ECP, modify incoming to dummy vals (in RTBPA task)
//#define ALARM_DUMMY_SEND                      //Dumy alarm sending (from RTMCB, sorry)
//#define DUMMY_PHYSICAL_VALLUES_SENDING        // Dummy sending physical data to DISPATCHER, in the PHYSIOLOGICAL DATA section

#define DISABLE_MSG_TO_SCC
#define DISABLE_SD_CARD_WRITE
#define DISABLE_DATA_REQ_SD

#define INTER_INTEGRATION_201103              // SOME THINGS FOR DEMO SOME INTEGRATION

#define DEMO_BYPASS                           // Disables RTMCB_TASK_NEW_MESSAGE FLAG for DEMO
                                              // Instead: send commandBufferFromRTBAvailable to RTBPA task!
                                              // Disables tasks from mb_tasks to switch RTB Actuators


#define ACTIVATE_SDCARD                     // ENABLES/DISABLES SDCARD
//#define ACCELEROMETER_SELFTEST              // DEFINED: CONFIGURE SELFTEST
                                              // UNDEFINED: NORMAL MODE

//#define MB_GATEWAY_RTMCB_PC                 // 20120504 RTMCB --> USB [LINK TO PC] [PC CONTROLS RTMCB, MB JUST DECO]
                                              // IT CAN BE USED WITH MICROFLUIDICS WINDOW
//#define PC_SPY_MB_RTMCB                       // 20120515: PC IS SPY: MB INITIATES HANDSHAKE AND RTMCB STREAMS
                                              // MB TRANSMITS MB TX --> RTMCB, --> PC
                                              // MB TRANSMITS MB RX <-- RTMCB, --> PC
                                              // IT CAN BE USED WITH RS422 PC[RX] <-- RTMCB[CHECK] WINDOW
#define MB_TASK_CMD_RTMCB                     // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
                                              // IT CAN BE USED WITH MICROFLUIDICS WINDOW [TO CONTROL FROM PC]
                                              // IT CAN BE USED WITH RS422 PC[RX] <-- RTMCB[CHECK] WINDOW [TO SEE CONTROL FROM MB]


#define STM32F_MB                             // DEFINES XTAL CONFIG

//#define USART2_HWFLOWCTRL                   // if defined nRTS/nCTS ENABLED

//#define STM32F_CBCRC                          // MB-CB CRC ENABLED
#define STM32F_SEMAPHORE_INT                  // SEMAPHORE FOR CB LINK 

#define STM32_USART2_LBACK
#define STM32_USART3_LBACK
#define STM32_UART4_LBACK
#define STM32F_UART4_DMA                      // UART4 RX DATA AND FILLS MEMORY BUFFER
#define STM32_UART5_LBACK

//#define STM32F_LED_INT_TICK                 // LED IN SYSTICK
#define RTOS_SYSTICK_LED                      // LED IN RTOS-SYSTICK
//#define STM32F_LED_TIM1                     // LED IN TIM1 INT
//#define STM32F_LED_TIM2                     // LED IN TIM2 INT
//#define STM32F_LED_TASK_MAINTENANCE_PL      // LED TOGGLES IN MAINTENANCE TASK

//#define UIF_SIMULATION                        // comment this line if MB should transmit real numbers to UIF
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
// STM32F Peripheral Library
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "stm32f10x.h"
	#include "stm32f10x_pwr.h"
	#include "stm32f10x_rcc.h"
	#include "stm32f10x_tim.h"
	#include "stm32f10x_exti.h"
	#include "stm32f10x_gpio.h"
	#include "stm32f10x_spi.h"
	#include "stm32f10x_rtc.h"
	#include "stm32f10x_bkp.h"
	#include "stm32f10x_flash.h"
	#include "stm32f10x_systick.h"
	#include "stm32f10x_usart.h"
	#include "stm32f10x_i2c.h"
	#include "stm32f10x_adc.h"
	#include "stm32f10x_dma.h"
	#include "eeprom.h"
#endif

// ----------------------------------------
// SDCARD
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "efsl\\sdcard_spi1.h"
	#include "efsl\\dir.h"
	#include "efsl\\disc.h"
	#include "efsl\\efs.h"
	#include "efsl\\extract.h"
	#include "efsl\\fat.h"
	#include "efsl\\file.h"
	#include "efsl\\fs.h"
	#include "efsl\\ioman.h"
	#include "efsl\\ls.h"
	#include "efsl\\plibc.h"
	#include "efsl\\sd.h"
	#include "efsl\\time.h"

	#include "efsl\\lpc2000_dbg_printf.h"
#endif
// ----------------------------------------
// RS422
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "ptp_rs422\\RS422_cmd_class_acquisition.h"
	#include "ptp_rs422\\RS422_cmd_class_communication.h"
	#include "ptp_rs422\\RS422_cmd_class_memory.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_ctrl.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_data.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_system.h"
	#include "ptp_rs422\\RS422_cmd_class_power.h"
	#include "ptp_rs422\\RS422_cmd_class_system.h"
	#include "ptp_rs422\\RS422_cmd_classes.h"
	#include "ptp_rs422\\RS422_Protocol.h"
#endif
// ----------------------------------------
// MB Tests / Application
// ----------------------------------------
#ifdef LINUX
	#include "data.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "main\\data.h"
#endif
#ifndef VP_SIMULATION
	#include "main\\mb_initialization.h"
	#include "main\\devices_initialization.h"

	#include "main\\math_tool.h"
	#include "main\\tools.h"
	#include "main\\SerialPortTest.h"         // later
	#include "main\\clock_calendar.h"
	#include "main\\supplies.h"
	#include "main\\feedbackadc.h"
	#include "main\\sdcard.h"
	#include "main\\uif_acc_spi0.h"
	//#include "main\\uif_acc_application.h"
	#include "main\\uif_application.h"
  #include "main\\acc_application.h"
  #include "main\\SerialPortRTMCB.h"
	#include "main\\SerialPortCB.h"
	#include "main\\rtmcb_uart4_dma.h"
	#include "main\\mb_tasks.h"
#endif

/* Standard includes. */
#include <string.h>

/* Scheduler includes. */
#define MBFREERTOS
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// ----------------------------------------
// code from CSEM
// ----------------------------------------
// #include "rtmcb_tasks.h"
// ----------------------------------------
// code from OFFIS
// ----------------------------------------
/* NEPHRON API includes */
#ifdef LINUX
	#include "nephron.h"
	#include "getData.h"
	#include "realTimeBoardProtocolAbstraction.h"
	#include "communicationBoardProtocolAbstraction.h"
	#include "dispatcher.h"
	#include "reminder.h"
	#include "dataStorage.h"
	#include "flowControl.h"
	#include "powermanager.h"
	#include "systemCheckCalibration.h"
	#include "watchDog.h"
	#include "userInterface.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "CodeFromOFFIS\\nephron.h"
	#include "CodeFromOFFIS\\DummyCode\\getData.h"
	#include "CodeFromOFFIS\\TaskRealTimeBoardProtocolAbstraction\\realTimeBoardProtocolAbstraction.h"
	#include "CodeFromOFFIS\\TaskCommunicationBoardProtocolAbstraction\\communicationBoardProtocolAbstraction.h"
	#include "CodeFromOFFIS\\TaskDispatcher\\dispatcher.h"
	#include "CodeFromOFFIS\\TaskReminder\\reminder.h"
	#include "CodeFromOFFIS\\TaskDataStorage\\dataStorage.h"
	#include "CodeFromOFFIS\\TaskFlowControl\\flowControl.h"
	#include "CodeFromOFFIS\\TaskPowerManagementBoardProtocolAbstraction\\powermanager.h"
	#include "CodeFromOFFIS\\TaskSystemCheckCalibration\\systemCheckCalibration.h"
	#include "CodeFromOFFIS\\TaskWatchDog\\watchDog.h"
	#include "CodeFromOFFIS\\TaskUserInterface\\userInterface.h"
#endif


// -----------------------------------------------------------------------------------
// Exported functions 
// -----------------------------------------------------------------------------------
extern void Delay(uint32_t nCount);

#endif /* __MAIN_H */
// -----------------------------------------------------------------------------------
// use of RAM TOTAL : 64K
// -----------------------------------------------------------------------------------
// 4K SDBUFFER, 4K SDCACHE

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
