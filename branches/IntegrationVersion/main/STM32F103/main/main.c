// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   main.c
//! \brief  startup program
//!
//! Initializes devices and variables, executes main loop
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"


#define PREFIX "OFFIS FreeRTOS"

// Create the queues for all 12 tasks.
// The data structure needs to be globally available, since the ISRs are also accessing these
// to send out messages to queues in case of certain IRQ event.
tdAllHandles allHandles;

static void setupNephronEmbeddedSW( void );

#ifdef VP_SIMULATION
	// Virtual Platform code here! Must not end up in final version!
	// The following global variable is been used by a dummy "getData" function.
	// Usually data would come in via a serial interface and getData pulls it from
	// the serial interface buffer.
	// For simulation with Simulink there is not serial interface and the dummy getData
	// pulls the values from Simulink it self. To know which data to pull, this global variable is used.
	uint8_t newStaticBufferPhysiologicalData	= 0;
	uint8_t newStaticBufferPhysicalData		= 0;
	uint8_t newStaticBufferStatusData		= 0;
	uint8_t newStaticBufferActuatorData		= 0;
	uint8_t newStaticBufferStatesRTB		= 0;
#endif

// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
portBASE_TYPE xReturn;

/* Define the strings that will be passed in as the task parameters.  These are
defined const and off the stack to ensure they remain valid when the tasks are
executing. */
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
const char *pcTextForTask0 = "Task 0: RTMCB ";
#endif                    // 20120608: MB_TASK_CMD_RTMCB
const char *pcTextForTask1 = "Task 1: CB ";
const char *pcTextForTask2 = "Task 2: SD CARD ";
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  main
//!
//! Main function of the project
//!
//! \param      none
//!
//! \return     exit value
// -----------------------------------------------------------------------------------
int main(void)
{ 
#ifndef VP_SIMULATION
        prvSetupHardware();
#endif
    
    setupNephronEmbeddedSW();

    /* Start the scheduler so our tasks start executing. */
    vTaskStartScheduler();

    for( ;; );


}
// -----------------------------------------------------------------------------------
//! \brief  Delay
//!
//! Inserts a delay time
//!
//! \param[IN]  Count: specifies the delay time length (time base 10 ms).
//!
//! \return     none
// -----------------------------------------------------------------------------------
#ifndef VP_SIMULATION
  void Delay(uint32_t nCount)
  {
     TimingDelay = nCount;
    
    while(TimingDelay != 0)
    {
    }
  }       
#endif //VP_SIM
// -----------------------------------------------------------------------------------
//! \brief  assert_failed
//!
//! Reports the name of the source file and the source line number where 
//! the assert_param error has occurred
//!
//! \param[IN]  file: pointer to the source file name
//! \param[IN]  line: assert_param error line source number
//!
//! \return     none
// -----------------------------------------------------------------------------------
#ifdef USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line)
{ 
  // User can add his own implementation to report the file name and line number,
  // ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) 

  // Infinite loop
  while (1)
  {
  }
}
#endif

static void setupNephronEmbeddedSW( void )
{

	taskMessage("I", PREFIX, "##############################################################");
	taskMessage("I", PREFIX, "#                                                            #");
	taskMessage("I", PREFIX, "#             ######  #####  #####  ####  #####              #");
	taskMessage("I", PREFIX, "#             ##  ##  ##     ##      ##   ###                #");
	taskMessage("I", PREFIX, "#             ##  ##  ####   ####    ##     ###              #");
	taskMessage("I", PREFIX, "#             ######  ##     ##     ####  #####              #");
	taskMessage("I", PREFIX, "#                                                            #");
	taskMessage("I", PREFIX, "#  Dipl.-Inform. Frank Poppen                                #");
	taskMessage("I", PREFIX, "#  Escherweg 2                                               #");
	taskMessage("I", PREFIX, "#  26121 Oldenburg                                           #");
	taskMessage("I", PREFIX, "#  Germany                                                   #");
	taskMessage("I", PREFIX, "#                                                            #");
	taskMessage("I", PREFIX, "# Demonstrating ARM7TDMI virtual platfrom running FreeRTOS.  #");
	taskMessage("I", PREFIX, "#                                                            #");
	taskMessage("I", PREFIX, "# Booting FreeRTOS operating system on HW simulation.        #");
	taskMessage("I", PREFIX, "#                                                            #");
	taskMessage("I", PREFIX, "##############################################################");

	tdAllHandlesPMB	allHandlesPMB0;
	tdAllHandlesPMB	allHandlesPMB1;
	allHandlesPMB0.instance = 0;
	allHandlesPMB0.pAllHandles = &allHandles;
	allHandlesPMB1.instance = 1;
	allHandlesPMB1.pAllHandles = &allHandles;


	// Queue Dispatcher
	allHandles.allQueueHandles.qhDISPin = xQueueCreate(uxQueueLengthDISP, sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhDISPin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Dispatcher!");}
	// Queue Real Time Board Protocol Abstraction
	allHandles.allQueueHandles.qhRTBPAin = xQueueCreate(uxQueueLengthRTBPA, sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhRTBPAin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Real Time Board Protocol Abstraction!");}
	// Queue Flow Control
	allHandles.allQueueHandles.qhFCin = xQueueCreate(uxQueueLengthFC, sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhFCin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Flow Control!");}
	// Queue User Interface
	allHandles.allQueueHandles.qhUIin = xQueueCreate(uxQueueLengthUI,     sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhUIin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for User Interface!");}
	// Queue Data Storage
	allHandles.allQueueHandles.qhDSin = xQueueCreate(uxQueueLengthDS,     sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhDSin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Data Storage!");}
	// Queue System Check and Calibration
	allHandles.allQueueHandles.qhSCCin = xQueueCreate(uxQueueLengthSCC,    sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhSCCin == NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for System Check & Calibration!");}
	// Queue Communication Board Protocol Abstraction
	allHandles.allQueueHandles.qhCBPAin   = xQueueCreate(uxQueueLengthCBPA,   sizeof(tdQtoken));
        if (allHandles.allQueueHandles.qhCBPAin	== NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Communication Board Protocol Abstraction!");}
        // Queue Reminder Abstraction
	allHandles.allQueueHandles.qhREMINDERin   = xQueueCreate(uxQueueLengthREMINDER,   sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhREMINDERin	== NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Reminder!");}
	// Queue Power Management Board Protocol Abstraction 0
	allHandles.allQueueHandles.qhPMBPA0in   = xQueueCreate(uxQueueLengthREMINDER,   sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhPMBPA0in	== NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Power Management Board 0 Task!");}
	// Queue Power Management Board Protocol Abstraction 1
	allHandles.allQueueHandles.qhPMBPA1in   = xQueueCreate(uxQueueLengthREMINDER,   sizeof(tdQtoken));
	if (allHandles.allQueueHandles.qhPMBPA1in	== NULL){taskMessage("F", PREFIX, "Not enough memory to create queue for Power Management Board 1 Task!");}

        

	// Create the Dispatcher task.
	if (pdTRUE != xTaskCreate( tskDISP, ( signed char * ) "Dispatcher", stackSizeDISP, &allHandles, tskDISP_PRIORITY, &(allHandles.allTaskHandles.handleDISP))){
		taskMessage("F", PREFIX, "Not enough memory to create task Dispatcher!");
	}
        
	// Task Communication Board Protocol Abstraction
	if (pdTRUE != xTaskCreate( tskCBPA, ( signed char * ) "CommunicationBoardProtocolAbstraction", stackSizeCBPA, &allHandles, tskCBPA_PRIORITY, &(allHandles.allTaskHandles.handleCBPA))){
		taskMessage("F", PREFIX, "Not enough memory to create task Communication Board Protocol Abstraction!");
	}

        // Task Real Time Board Protocol Abstraction
	if (pdTRUE != xTaskCreate( tskRTBPA, ( signed char * ) "RealTimeBoardProtocolAbstraction", stackSizeRTBPA, &allHandles, tskRTBPA_PRIORITY, &(allHandles.allTaskHandles.handleRTBPA))){
		taskMessage("F", PREFIX, "Not enough memory to create task Real Time Board Protocol Abstraction!");
	}
         

	// Task Reminder
	if (pdTRUE != xTaskCreate( tskReminder, ( signed char * ) "Reminder", stackSizeReminder, &allHandles, tskReminder_PRIORITY, &(allHandles.allTaskHandles.handleReminder))){
		taskMessage("F", PREFIX, "Not enough memory to create task Reminder!");
	}


        // Task User Interface
	if (pdTRUE != xTaskCreate( tskUI, ( signed char * ) "UserInterface", stackSizeUI, &allHandles, tskUI_PRIORITY, &(allHandles.allTaskHandles.handleUI))){
		taskMessage("F", PREFIX, "Not enough memory to create task User Interface!");
	}


        // Task Data Storage
	if (pdTRUE != xTaskCreate( tskDS, ( signed char * ) "DataStorage", stackSizeDS, &allHandles, tskDS_PRIORITY, &(allHandles.allTaskHandles.handleDS))){
		taskMessage("F", PREFIX, "Not enough memory to create task Data Storage!");
	}

/*        // Task System Check & Calibration
	if (pdTRUE != xTaskCreate( tskSCC, ( signed char * ) "SystemCheckCalibration", stackSizeSCC, &allHandles, tskSCC_PRIORITY, &(allHandles.allTaskHandles.handleSCC))){
		taskMessage("F", PREFIX, "Not enough memory to create task System Check and Calibration!");
	}
	
        
        // Task Watch Dog
	if (pdTRUE != xTaskCreate( tskWD, ( signed char * ) "WatchDog", stackSizeWD, &allHandles, tskWD_PRIORITY, &(allHandles.allTaskHandles.handleWD))){
		taskMessage("F", PREFIX, "Not enough memory to create task Watch Dog!");
	}
*/
	// Task Flow Control
	if (pdTRUE != xTaskCreate( tskFC, ( signed char * ) "FlowControl", stackSizeFC, &allHandles, tskFC_PRIORITY, &(allHandles.allTaskHandles.handleFC))){
		taskMessage("F", PREFIX, "Not enough memory to create task Flow Control!");
	}
/*
        // Task Power Management Board 0 Protocol Abstraction
	if (pdTRUE != xTaskCreate( tskPMBPA, ( signed char * ) "PowerManagementBoardProtocolAbstraction0", stackSizePMBPA, &allHandlesPMB0, tskPMBPA_PRIORITY, &(allHandles.allTaskHandles.handlePMBPA0))){
		taskMessage("F", PREFIX, "Not enough memory to create task Power Management Board Protocol Abstraction 0!");
	}
	// Task Power Management Board 1 Protocol Abstraction
	if (pdTRUE != xTaskCreate( tskPMBPA, ( signed char * ) "PowerManagementBoardProtocolAbstraction1", stackSizePMBPA, &allHandlesPMB1, tskPMBPA_PRIORITY, &(allHandles.allTaskHandles.handlePMBPA1))){
		taskMessage("F", PREFIX, "Not enough memory to create task Power Management Board Protocol Abstraction 1!");
	}
*/

}

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
