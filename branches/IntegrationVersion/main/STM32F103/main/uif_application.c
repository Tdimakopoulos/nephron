// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_application.c
//! \brief  UIF application
//!
//! scheduler and complementary routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
// Simulation for testing UIF
#define STATUSMAX     4
uint8_t bts_status_simulator = 0;
uint8_t bps_status_simulator = 1;
uint8_t fps_status_simulator = 2;
uint8_t dcs_status_simulator = 3;
uint8_t vcs_status_simulator = 0;

uint8_t blpump_status_simulator = 0;
uint8_t flpump_status_simulator = 0;
uint8_t valve3_status_simulator = 0;
uint8_t vcpump_status_simulator = 0;

uint8_t hr_sign_simulator = 1;        // 1 (+), 2 (-)
uint8_t br_sign_simulator = 1;
uint8_t age_simulator = 1;
uint8_t weight_simulator = 1;

uint8_t alarms_simulator[4] = {0,0,0,0};
uint8_t errors_simulator[4] = {0,0,0,0};
// -----------------------------------------------------------------------------------
//! \brief  MB_UIF_DATAREALTIME()
//!
//! Connects data incoming from RTMCB and other data sources in RealTime to UIF. 
//! Executes in stm32f10x_it if no FREERTOS, in mb_tasks if FREERTOS
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void MB_UIF_DATAREALTIME(){
    // WAKD STATUS
    // just from ready to not ready....
    MB_WAKD_Status^=WAKD_READY_TO_WORK_SET;
    // WAKD OPERATING MODE
    MB_WAKD_OperatingMode = RTMCB_OPERATING_MODE;

    // BPACK1, BPACK2
    //MB_BPACK_1_Percent = 0;
    //MB_BPACK_2_Percent = 0;
    // WAKD ATTITUDE
    //MB_WAKD_Attitude = 0;
    // BTS
    MB_WAKD_BTS_InletTemperature = staticBufferPhysicalsensorData.TemperatureInOut.TemperatureInlet_Value;
    MB_WAKD_BTS_OutletTemperature = staticBufferPhysicalsensorData.TemperatureInOut.TemperatureOutlet_Value;
    // BPS
    MB_WAKD_BPS_Pressure=staticBufferPhysicalsensorData.PressureBCI.Pressure;
    // FPS
    MB_WAKD_FPS_Pressure=staticBufferPhysicalsensorData.PressureFCI.Pressure;
    // BLPUMP
    MB_WAKD_BLPump_SpeedCode = staticBufferActuatorData.BLPumpData.SpeedReferenceCode;
    // FLPUMP
    MB_WAKD_FLPump_SpeedCode = staticBufferActuatorData.FLPumpData.SpeedReferenceCode;
    // COMM LINKS
    MB_CLINKS_Status= CLINKS_USB;
    // DCS SENSOR
    // @@@@ FCR OR FCQ      
    MB_WAKD_DCS_Conductance  = staticBufferPhysicalsensorData.CSENS1Data.Cond_FCR;
    MB_WAKD_DCS_Susceptance = staticBufferPhysicalsensorData.CSENS1Data.Cond_FCQ;
    MB_WAKD_DCS_Status = staticBufferDevicesStatus.statusDCS;
    // WAKD BLOOD CIRCUIT
    MB_WAKD_BLCircuit_Status = SENSOR_NOINFO;
    // WAKD FILTRATE CIRCUIT
    MB_WAKD_FLCircuit_Status = SENSOR_NOINFO;
    // HFD
    MB_WAKD_HFD_Status = DEVICE_NOINFO;
    //MB_WAKD_HFD_PercentOK++;
    // SU
    MB_WAKD_SU_Status = DEVICE_NOINFO;
    // POLARIZER
    MB_WAKD_POLAR_Voltage=staticBufferActuatorData.PolarizationData.Voltage;
    MB_WAKD_POLAR_Voltage&=0x7FFF;
    if(staticBufferActuatorData.PolarizationData.Direction == SENS_REVERSE)MB_WAKD_POLAR_Voltage |=0x8000;
    MB_WAKD_POLAR_Status = DEVICE_NOINFO;
    // ECPS
    MB_WAKD_ECP1_Status = SENSOR_NOINFO;
    MB_WAKD_ECP2_Status = SENSOR_NOINFO;
    // PS INFO
    bts_status_simulator = DEVICE_NOINFO;
    bps_status_simulator = DEVICE_NOINFO;
    fps_status_simulator = DEVICE_NOINFO;
    dcs_status_simulator = DEVICE_NOINFO;
    vcs_status_simulator = DEVICE_NOINFO;

    MB_WAKD_PS_Status=0;
    MB_WAKD_PS_Status|=(bts_status_simulator&0x0F);
    MB_WAKD_PS_Status|=((bps_status_simulator&0x0F)<<8);
    MB_WAKD_PS_Status|=((fps_status_simulator&0x0F)<<16);
    MB_WAKD_PS_Status|=((dcs_status_simulator&0x0F)<<24);
    MB_WAKD_PS_Status|=((vcs_status_simulator&0x0F)<<28);

    // ACT INFO
    blpump_status_simulator = DEVICE_NOINFO;
    flpump_status_simulator = DEVICE_NOINFO;
    valve3_status_simulator = DEVICE_NOINFO;
    vcpump_status_simulator = DEVICE_NOINFO;

    MB_WAKD_ACT_Status=0;
    MB_WAKD_ACT_Status|=(blpump_status_simulator&0x0F);
    MB_WAKD_ACT_Status|=((flpump_status_simulator&0x0F)<<8);
    MB_WAKD_ACT_Status|=((valve3_status_simulator&0x0F)<<16);
    MB_WAKD_ACT_Status|=((vcpump_status_simulator&0x0F)<<24);

    // ECP1 DATA
    P_DATA_NA_ECP1 = staticBufferPhysiologicalData.ECPDataI.Sodium;
    P_DATA_K_ECP1 = staticBufferPhysiologicalData.ECPDataI.Potassium;
    P_DATA_PH_ECP1 = staticBufferPhysiologicalData.ECPDataI.pH;
    P_DATA_UREA_ECP1 = staticBufferPhysiologicalData.ECPDataI.Urea;
    // ECP2 DATA
    P_DATA_NA_ECP2 = staticBufferPhysiologicalData.ECPDataO.Sodium;
    P_DATA_K_ECP2 = staticBufferPhysiologicalData.ECPDataO.Potassium;
    P_DATA_PH_ECP2 = staticBufferPhysiologicalData.ECPDataO.pH;
    P_DATA_UREA_ECP2 = staticBufferPhysiologicalData.ECPDataO.Urea;

    // P_DATA
    // AGE
    P_DATA_AGE = 45;
    P_DATA_WEIGHT = 52;
    // HEART RATE
    P_DATA_HEARTRATE = 60;
    P_DATA_BREATHINGRATE = 15;
    P_DATA_ACTIVITYCODE = ACTIVITY_RUNNING;
    P_DATA_SISTOLICBP = 12;
    P_DATA_DIASTOLICBP = 8;
    // ALARMS
    MB_WAKD_ALARMS_Status= staticBufferAlarms.Status;
    // ERRORS
    MB_WAKD_ERRORS_Status= 0;

}
// -----------------------------------------------------------------------------------
//! \brief  MB_UIF_DATASIMULATION
//!
//! Generates dummy data to test the UIF. 
//! Executes in stm32f10x_it if no FREERTOS, in mb_tasks if FREERTOS
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void MB_UIF_DATASIMULATION(){
    // WAKD STATUS
    // just from ready to not ready....
    MB_WAKD_Status^=WAKD_READY_TO_WORK_SET;
    // WAKD OPERATING MODE
    MB_WAKD_OperatingMode++;
    if(MB_WAKD_OperatingMode>MICROFLUIDIC_CODE_MAX) MB_WAKD_OperatingMode = MICROFLUIDIC_CODE_MIN;

    // BPACK1, BPACK2
    MB_BPACK_1_Percent++;
    if(MB_BPACK_1_Percent == 100) MB_BPACK_1_Percent = 0;
    MB_BPACK_2_Percent++;
    if(MB_BPACK_2_Percent == 50) MB_BPACK_2_Percent = 0;
    // WAKD ATTITUDE
    switch(MB_WAKD_Attitude){
        case WAKD_ATTITUDE_OK:
            MB_WAKD_Attitude = WAKD_ATTITUDE_KO;
        break;
        case WAKD_ATTITUDE_KO:
            MB_WAKD_Attitude = WAKD_ATTITUDE_NOINFO;
        break;
        case WAKD_ATTITUDE_NOINFO:
        default:
            MB_WAKD_Attitude = WAKD_ATTITUDE_OK;
        break;        
    }
    // BTS
    MB_WAKD_BTS_InletTemperature+=2;
    if(MB_WAKD_BTS_InletTemperature==100) MB_WAKD_BTS_InletTemperature = 0;
    MB_WAKD_BTS_OutletTemperature++;
    if(MB_WAKD_BTS_OutletTemperature==100) MB_WAKD_BTS_OutletTemperature = 0;
    // BPS
    MB_WAKD_BPS_Pressure+=5;
    if(MB_WAKD_BPS_Pressure==500) MB_WAKD_BPS_Pressure=0;
    // FPS
    MB_WAKD_FPS_Pressure+=10;
    if(MB_WAKD_FPS_Pressure==750) MB_WAKD_FPS_Pressure=0;
    // BLPUMP
    if(blpump_up == 0){
        MB_WAKD_BLPump_SpeedCode--;
        if(MB_WAKD_BLPump_SpeedCode==_PWM_CODE_RLEFT_MIN)blpump_up=1;        
    }
    else{
        MB_WAKD_BLPump_SpeedCode++;
        if(MB_WAKD_BLPump_SpeedCode==_PWM_CODE_RRIGHT_MAX)blpump_up=0;        
    }
    // FLPUMP
    if(flpump_up == 0){
        MB_WAKD_FLPump_SpeedCode--;
        if(MB_WAKD_FLPump_SpeedCode==_PWM_CODE_RLEFT_MIN)flpump_up=1;        
    }
    else{
        MB_WAKD_FLPump_SpeedCode++;
        if(MB_WAKD_FLPump_SpeedCode==_PWM_CODE_RRIGHT_MAX)flpump_up=0;        
    }
    // COMM LINKS
    switch(MB_CLINKS_Status){
        case CLINKS_NONE:
            MB_CLINKS_Status = CLINKS_BLUETOOTH;
        break;
        case CLINKS_BLUETOOTH:
            MB_CLINKS_Status = CLINKS_USB;
        break;
        case CLINKS_USB:
            MB_CLINKS_Status = CLINKS_BOTH;
        break;
        case CLINKS_BOTH:
        default:
            MB_CLINKS_Status = CLINKS_NONE;
        break;
    }
    // DCS SENSOR
    MB_WAKD_DCS_Conductance+=25;
    if(MB_WAKD_DCS_Conductance>=1000)MB_WAKD_DCS_Conductance = 0;
    if(MB_WAKD_DCS_Susceptance>=25) MB_WAKD_DCS_Susceptance-=25;
    else MB_WAKD_DCS_Susceptance=1000;
    switch(MB_WAKD_DCS_Status){
        case SENSOR_OK:
            MB_WAKD_DCS_Status = SENSOR_KO;
        break;
        case SENSOR_KO:
        default:
            MB_WAKD_DCS_Status = SENSOR_NOINFO;
        break;
        case SENSOR_NOINFO:
            MB_WAKD_DCS_Status = SENSOR_OK;
        break;
    }
    // WAKD BLOOD CIRCUIT
    switch(MB_WAKD_BLCircuit_Status){
        case SENSOR_OK:
            MB_WAKD_BLCircuit_Status = SENSOR_KO;
        break;
        case SENSOR_KO:
        default:
            MB_WAKD_BLCircuit_Status = SENSOR_NOINFO;
        break;
        case SENSOR_NOINFO:
            MB_WAKD_BLCircuit_Status = SENSOR_OK;
        break;
    }
    // WAKD FILTRATE CIRCUIT
    switch(MB_WAKD_FLCircuit_Status){
        case SENSOR_OK:
            MB_WAKD_FLCircuit_Status = SENSOR_KO;
        break;
        case SENSOR_KO:
        default:
            MB_WAKD_FLCircuit_Status = SENSOR_NOINFO;
        break;
        case SENSOR_NOINFO:
            MB_WAKD_FLCircuit_Status = SENSOR_OK;
        break;
    }
    // HFD
    switch(MB_WAKD_HFD_Status){
        case DEVICE_OK:
            MB_WAKD_HFD_Status = DEVICE_KO;
        break;
        case DEVICE_KO:
        default:
            MB_WAKD_HFD_Status = DEVICE_NOINFO;
        break;
        case DEVICE_NOINFO:
            MB_WAKD_HFD_Status = DEVICE_OK;
        break;
    }    
    MB_WAKD_HFD_PercentOK++;
    if(MB_WAKD_HFD_PercentOK>100)MB_WAKD_HFD_PercentOK=0;
    // SU
    switch(MB_WAKD_SU_Status){
        case DEVICE_OK:
            MB_WAKD_SU_Status = DEVICE_KO;
        break;
        case DEVICE_KO:
        default:
            MB_WAKD_SU_Status = DEVICE_NOINFO;
        break;
        case DEVICE_NOINFO:
            MB_WAKD_SU_Status = DEVICE_OK;
        break;
    }
    // POLARIZER
    if(polar_up == 0){
        if(MB_WAKD_POLAR_Voltage==0){
            polar_up=1;    
            if(polar_sign==1)polar_sign=0;
            else polar_sign=1;
        }
        else MB_WAKD_POLAR_Voltage--;
    }
    else{
        if(MB_WAKD_POLAR_Voltage==500)polar_up=0;        
        else MB_WAKD_POLAR_Voltage++;
    }
    MB_WAKD_POLAR_Voltage&=0x7FFF;
    if(polar_sign==1)MB_WAKD_POLAR_Voltage |=0x8000;

    switch(MB_WAKD_POLAR_Status){
        case DEVICE_OK:
            MB_WAKD_POLAR_Status = DEVICE_KO;
        break;
        case DEVICE_KO:
        default:
            MB_WAKD_POLAR_Status = DEVICE_NOINFO;
        break;
        case DEVICE_NOINFO:
            MB_WAKD_POLAR_Status = DEVICE_OK;
        break;
    }
   
    // ECPS
    switch(MB_WAKD_ECP1_Status){
        case SENSOR_OK:
            MB_WAKD_ECP1_Status = SENSOR_KO;
        break;
        case SENSOR_KO:
        default:
            MB_WAKD_ECP1_Status = SENSOR_NOINFO;
        break;
        case SENSOR_NOINFO:
            MB_WAKD_ECP1_Status = SENSOR_OK;
        break;
    }
    switch(MB_WAKD_ECP2_Status){
        case SENSOR_OK:
            MB_WAKD_ECP2_Status = SENSOR_KO;
        break;
        case SENSOR_KO:
        default:
            MB_WAKD_ECP2_Status = SENSOR_NOINFO;
        break;
        case SENSOR_NOINFO:
            MB_WAKD_ECP2_Status = SENSOR_OK;
        break;
    }
    // PS INFO
    bts_status_simulator++;
    if(bts_status_simulator==STATUSMAX)bts_status_simulator=0;
    bps_status_simulator++;
    if(bps_status_simulator==STATUSMAX)bps_status_simulator=0;
    fps_status_simulator++;
    if(fps_status_simulator==STATUSMAX)fps_status_simulator=0;
    dcs_status_simulator++;
    if(dcs_status_simulator==STATUSMAX)dcs_status_simulator=0;
    vcs_status_simulator++;
    if(vcs_status_simulator==STATUSMAX)vcs_status_simulator=0;

    MB_WAKD_PS_Status=0;
    MB_WAKD_PS_Status|=(bts_status_simulator&0x0F);
    MB_WAKD_PS_Status|=((bps_status_simulator&0x0F)<<8);
    MB_WAKD_PS_Status|=((fps_status_simulator&0x0F)<<16);
    MB_WAKD_PS_Status|=((dcs_status_simulator&0x0F)<<24);
    MB_WAKD_PS_Status|=((vcs_status_simulator&0x0F)<<28);

    // ACT INFO
    blpump_status_simulator++;
    if(blpump_status_simulator==STATUSMAX)blpump_status_simulator=0;
    flpump_status_simulator++;
    if(flpump_status_simulator==STATUSMAX)flpump_status_simulator=0;
    valve3_status_simulator++;
    if(valve3_status_simulator==STATUSMAX)valve3_status_simulator=0;
    vcpump_status_simulator++;
    if(vcpump_status_simulator==STATUSMAX)vcpump_status_simulator=0;

    MB_WAKD_ACT_Status=0;
    MB_WAKD_ACT_Status|=(blpump_status_simulator&0x0F);
    MB_WAKD_ACT_Status|=((flpump_status_simulator&0x0F)<<8);
    MB_WAKD_ACT_Status|=((valve3_status_simulator&0x0F)<<16);
    MB_WAKD_ACT_Status|=((vcpump_status_simulator&0x0F)<<24);

    // ECP1 DATA
    P_DATA_NA_ECP1++; if(P_DATA_NA_ECP1>246) P_DATA_NA_ECP1=123;
    P_DATA_K_ECP1++; if(P_DATA_K_ECP1>44) P_DATA_K_ECP1=22;
    P_DATA_PH_ECP1++; if(P_DATA_PH_ECP1>14) P_DATA_PH_ECP1=7;
    P_DATA_UREA_ECP1++; if(P_DATA_UREA_ECP1>18)P_DATA_UREA_ECP1=9;
    // ECP2 DATA
    P_DATA_NA_ECP2++; if(P_DATA_NA_ECP2>222) P_DATA_NA_ECP2=111;
    P_DATA_K_ECP2++; if(P_DATA_K_ECP2>110) P_DATA_K_ECP2=55;
    P_DATA_PH_ECP2++; if(P_DATA_PH_ECP2>20) P_DATA_PH_ECP2=10;
    P_DATA_UREA_ECP2++; if(P_DATA_UREA_ECP2>166)P_DATA_UREA_ECP2=88;

    // P_DATA
    // AGE
    if (age_simulator == 1){
        if(P_DATA_AGE<100) P_DATA_AGE++;
        else age_simulator = 2;
    }
    else if(age_simulator == 2){
        if(P_DATA_AGE>0) P_DATA_AGE--;
        else age_simulator = 1;
    }
    // WEIGHT
    if (weight_simulator == 1){
        if(P_DATA_WEIGHT<500) P_DATA_WEIGHT++;
        else weight_simulator = 2;
    }
    else if(weight_simulator == 2){
        if(P_DATA_WEIGHT>50) P_DATA_WEIGHT--;
        else weight_simulator = 1;
    }
    // HEART RATE
    if (hr_sign_simulator == 1){
        if(P_DATA_HEARTRATE<240) P_DATA_HEARTRATE++;
        else hr_sign_simulator = 2;
    }
    else if(hr_sign_simulator == 2){
        if(P_DATA_HEARTRATE>10) P_DATA_HEARTRATE--;
        else hr_sign_simulator = 1;
    }
    // BREATHING RATE
    if (br_sign_simulator == 1){
        if(P_DATA_BREATHINGRATE<30) P_DATA_BREATHINGRATE++;
        else br_sign_simulator = 2;
    }
    else if(br_sign_simulator == 2){
        if(P_DATA_BREATHINGRATE>3) P_DATA_BREATHINGRATE--;
        else br_sign_simulator = 1;
    }
    // ACTIVITY
    switch(P_DATA_ACTIVITYCODE){
        case ACTIVITY_STEADY:
            P_DATA_ACTIVITYCODE = ACTIVITY_LAYING;
        break;
        case ACTIVITY_LAYING:
            P_DATA_ACTIVITYCODE = ACTIVITY_WALKING;
        break;
        case ACTIVITY_WALKING:
            P_DATA_ACTIVITYCODE = ACTIVITY_RUNNING;
        break;
        case ACTIVITY_RUNNING:
            P_DATA_ACTIVITYCODE = ACTIVITY_UNKNOWN;
        break;
        case ACTIVITY_UNKNOWN:
            P_DATA_ACTIVITYCODE = ACTIVITY_STEADY;
        break;
    }
    P_DATA_SISTOLICBP++;
    if (P_DATA_SISTOLICBP==25) P_DATA_SISTOLICBP = 10;
    P_DATA_DIASTOLICBP++;
    if(P_DATA_DIASTOLICBP==15) P_DATA_DIASTOLICBP=5;
    
    /*
    // ALARMS
    alarms_simulator[0]+=4;
    alarms_simulator[1]+=3;
    alarms_simulator[2]+=2;
    alarms_simulator[3]+=1;
    MB_WAKD_ALARMS_Status=((alarms_simulator[0]<<24)+(alarms_simulator[1]<<16)+(alarms_simulator[2]<<8)+(alarms_simulator[3])) ;
    // ERRORS
    errors_simulator[0]+=1;
    errors_simulator[1]+=2;
    errors_simulator[2]+=3;
    errors_simulator[3]+=4;
    MB_WAKD_ERRORS_Status=((errors_simulator[0]<<24)+(errors_simulator[1]<<16)+(errors_simulator[2]<<8)+(errors_simulator[3])) ;;
    */
}
// -----------------------------------------------------------------------------------
//! \brief  UIF_ACC_SCHEDULER
//!
//! Sends one command each time to update or to read information
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void UIF_ACC_SCHEDULER(uint16_t uif_acc_index)
{
    uint8_t x =0;
    for(x=0;x<MB_UIF_QTY_CMD_SET;x++) MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[x] = MB_UIF_CMD_CLR;

    switch(uif_acc_index){
        case MB_UIF_SCHNO_VBAT1_INFO:
            Update_VBAT1_INFO ();            
        break;
        case MB_UIF_SCHNO_VBAT2_INFO:
            Update_VBAT2_INFO ();            
        break;
        case MB_UIF_SCHNO_WAKD_STATUS:
            Update_WAKD_STATUS ();         
        break;
        case MB_UIF_SCHNO_WAKD_ATTITUDE:
            Update_WAKD_ATTITUDE ();         
        break;
        case MB_UIF_SCHNO_WAKD_COMMLINK:
            Update_WAKD_COMMLINK ();         
        break;
        case MB_UIF_SCHNO_WAKD_OPMODE:
            Update_WAKD_OPMODE ();         
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS:
            Update_WAKD_BLCIRCUIT_STATUS ();         
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS:
            Update_WAKD_FLCIRCUIT_STATUS ();         
        break;
        case MB_UIF_SCHNO_WAKD_BLPUMP_INFO:
            Update_WAKD_BLPUMP_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_FLPUMP_INFO:
            Update_WAKD_FLPUMP_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_BLTEMPERATURE:
            Update_WAKD_BLTEMPERATURE ();         
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE:
            Update_WAKD_BLCIRCUIT_PRESSURE ();         
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE:
            Update_WAKD_FLCIRCUIT_PRESSURE ();         
        break;
        case MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY:
            Update_WAKD_FLCONDUCTIVITY ();         
        break;
        case MB_UIF_SCHNO_WAKD_HFD_INFO:
            Update_WAKD_HFD_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_SU_INFO:
            Update_WAKD_SU_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_POLAR_INFO:
            Update_WAKD_POLAR_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_ECPS_INFO:
            Update_WAKD_ECPS_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_PS_INFO:
            Update_WAKD_PS_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_ACT_INFO:
            Update_WAKD_ACT_INFO ();         
        break;
        case MB_UIF_SCHNO_WAKD_ACK_EXECUTED:
            Update_WAKD_CMD_EXECUTED();
        break;
        case MB_UIF_SCHNO_ALARMS:
            Update_WAKD_ALARMS ();         
        break;
        case MB_UIF_SCHNO_ERRORS:
            Update_WAKD_ERRORS ();         
        break;
        case MB_UIF_SCHNO_TEST_LINK:
        break;
        case MB_UIF_SCHNO_PDATA_INITIALS:
            Update_PDATA_INITIALS ();         
        break;
        case MB_UIF_SCHNO_PDATA_PATIENTCODE:
            Update_PDATA_PATIENTCODE ();         
        break;
        case MB_UIF_SCHNO_PDATA_GENDERAGE:
            Update_PDATA_GENDERAGE ();         
        break;
        case MB_UIF_SCHNO_PDATA_WEIGHT:
            Update_PDATA_WEIGHT ();         
        break;
        case MB_UIF_SCHNO_PDATA_SEWALL:
            Update_PDATA_SEWALL ();         
        break;
        case MB_UIF_SCHNO_PDATA_BLPRESSURE:
            Update_PDATA_BLPRESSURE ();         
        break;
        case MB_UIF_SCHNO_PDATA_ECP1_A:
            Update_PDATA_ECP1_A ();         
        break;
        case MB_UIF_SCHNO_PDATA_ECP1_B:
            Update_PDATA_ECP1_B ();         
        break;
        case MB_UIF_SCHNO_PDATA_ECP2_A:
            Update_PDATA_ECP2_A ();         
        break;
        case MB_UIF_SCHNO_PDATA_ECP2_B:
            Update_PDATA_ECP2_B ();         
        break;
    }
}

// -----------------------------------------------------------------------------------
//! \brief  Update_VBAT1_INFO
//!
//! Sends BAT1 information
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_VBAT1_INFO (){
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_VBAT1_INFO;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = MB_BPACK_1_Percent;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_BPACK_1_RemainingCapacity);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_VBAT2_INFO
//!
//! Sends BAT2 information
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_VBAT2_INFO (){
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_VBAT2_INFO;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = MB_BPACK_2_Percent;   
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_BPACK_2_RemainingCapacity);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_STATUS
//!
//! Sends WAKD STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_STATUS (){
    // Update information:
    // WAKD STATUS
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_STATUS;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}

// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_ATTITUDE
//!
//! Sends WAKD ATTITUDE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_ATTITUDE(){
    // Update information:
    // WAKD Attitude
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_ATTITUDE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_Attitude);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_COMMLINK
//!
//! Sends WAKD CB LINK STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_COMMLINK (){
    // Update information:
    // CB Status
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_COMMLINK;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_CLINKS_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_OPMODE
//!
//! Sends CURRENT WAKD OPERATING MODE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_OPMODE (){
    // Update information:
    // WAKD Operating Mode
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_OPMODE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_OperatingMode);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_BLCIRCUIT_STATUS
//!
//! Sends WAKD BLOOD CIRCUIT STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_BLCIRCUIT_STATUS (){
    // Update information:
    // WAKD Microfluidics
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_BLCircuit_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_FLCIRCUIT_STATUS
//!
//! Sends WAKD FILTRATE CIRCUIT STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_FLCIRCUIT_STATUS (){
    // Update information:
    // WAKD Microfluidics
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_FLCircuit_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_BLPUMP_INFO
//!
//! Sends WAKD BLOOD PUMP INFO
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_BLPUMP_INFO (){
    // Update information:
#if 0
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_BLPUMP_INFO;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_BLPump_Speed);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_BLPump_DirStatus);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
#else
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_BLPUMP_INFO;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = MB_WAKD_BLPump_SpeedCode;   
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
#endif
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_FLPUMP_INFO
//!
//! Sends WAKD DIALYSATE PUMP INFO
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_FLPUMP_INFO (){
    // Update information:
#if 0
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_FLPUMP_INFO;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_FLPump_Speed);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_FLPump_DirStatus);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
#else
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_FLPUMP_INFO;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = MB_WAKD_FLPump_SpeedCode;   
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
#endif
}

// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_BLTEMPERATURE
//!
//! Sends BLOOD INLET/OUTLET TEMPERATURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_BLTEMPERATURE (){
    // Update information:
    // WAKD Temperature Sensor
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_BLTEMPERATURE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_BTS_InletTemperature);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_BTS_OutletTemperature);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_BLCIRCUIT_PRESSURE
//!
//! Sends BLOOD CIRCUIT PRESSURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_BLCIRCUIT_PRESSURE (){
    // Update information:
    // WAKD Pressure Sensors
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_BPS_Pressure);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_FLCIRCUIT_PRESSURE
//!
//! Sends WAKD FILTRATE CIRCUIT PRESSURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_FLCIRCUIT_PRESSURE (){
    // Update information:
    // WAKD Pressure Sensors
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_FPS_Pressure);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_FLCONDUCTIVITY
//!
//! Sends WAKD FILTRATE CIRCUIT CONDUCTIVITY
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_FLCONDUCTIVITY (){
    // Update information:
    // WAKD Conductivity Sensor
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_FLCONDUCTIVITY;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_DCS_Conductance);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_DCS_Susceptance);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_HFD_INFO
//!
//! Sends WAKD HIGH-FLUX-DIALYSER INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_HFD_INFO (){
    // Update information:
    // WAKD High Flux Dialyser    
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_HFD_INFO;
    SET08(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_HFD_Status);
    SET08(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 2,MB_WAKD_HFD_PercentOK);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_SU_INFO
//!
//! Sends WAKD SORBENT UNIT INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_SU_INFO (){
    // Update information:
    // WAKD Sorbent Unit
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_SU_INFO;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_SU_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_POLAR_INFO
//!
//! Sends WAKD POLARIZER INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_POLAR_INFO (){
    // Update information:
    // WAKD Polarizer
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_POLAR_INFO;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_POLAR_Voltage);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_POLAR_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_ECPS_INFO
//!
//! Sends WAKD ECP1 & ECP2 INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_ECPS_INFO (){
    // Update information:
    // WAKD ECPs
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_ECPS_INFO;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_ECP1_Status);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,MB_WAKD_ECP2_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_PS_INFO
//!
//! Sends WAKD PS INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_PS_INFO (){
    // Update information:
    // WAKD PS
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_PS_INFO;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_PS_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_ACT_INFO
//!
//! Sends WAKD ACT INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_ACT_INFO (){
    // Update information:
    // WAKD ACT
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_ACT_INFO;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_ACT_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_CMD_EXECUTED
//!
//! Sends WAKD ACT INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_CMD_EXECUTED (){
    // Update information:
    // WAKD CMDS
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_WAKD_ACK_COMMANDS;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_USER_COMMAND);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}

// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_ALARMS
//!
//! Sends WAKD ALARMS STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_ALARMS (){
    // Update information:
    // WAKD Alarms
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_ALARMS;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_ALARMS_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_WAKD_ERRORS
//!
//! Sends WAKD ERRORS STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_WAKD_ERRORS (){
    // Update information:
    // WAKD ERRORS
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_ERRORS;
    SET32(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,MB_WAKD_ERRORS_Status);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_FIRSTNAME
//!
//! Sends PATIENT FIRSTNAME
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_INITIALS (){
    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_INITIALS;
    for(i=0;i<4;i++) MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[i+1] = P_DATA_INITIALS[i];
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_LASTNAME
//!
//! Sends PATIENT LASTNAME
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_PATIENTCODE (){
    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_PATIENTCODE;
    for(i=0;i<4;i++) MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[i+1] = P_DATA_PATIENTCODE[i];
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_GENDERAGE
//!
//! Sends PATIENT GENDER AND AGE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_GENDERAGE (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_GENDERAGE;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = P_DATA_GENDER[0];
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = P_DATA_AGE;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_WEIGHT
//!
//! Sends PATIENT WEIGHT
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_WEIGHT (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_WEIGHT;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_WEIGHT);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;   
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_SEWALL
//!
//! Sends PATIENT CURRENT HEART RATE, BREATHING RATE, ACTIVITY INFO (if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_SEWALL (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_SEWALL;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[1] = P_DATA_HEARTRATE;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[2] = P_DATA_BREATHINGRATE;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[3] = P_DATA_ACTIVITYCODE;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[4] = 0;
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_BLPRESSURE
//!
//! Sends PATIENT BLOOD PRESSURE (NIBP MEASUREMENT, if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_BLPRESSURE (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] =  MB_UIF_CMD_PDATA_BLPRESSURE;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_SISTOLICBP);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,P_DATA_DIASTOLICBP);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_ECP1_A
//!
//! Sends ECP1-A VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_ECP1_A (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_ECP1_A;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_NA_ECP1);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,P_DATA_K_ECP1);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_ECP1_B
//!
//! Sends ECP1-B VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_ECP1_B (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_ECP1_B;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_PH_ECP1);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,P_DATA_UREA_ECP1);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_ECP2_A
//!
//! Sends ECP2-A VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_ECP2_A (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_ECP2_A;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_NA_ECP2);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,P_DATA_K_ECP2);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
//! \brief  Update_PDATA_ECP2_B
//!
//! Sends ECP2-B VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void Update_PDATA_ECP2_B (){
    // Update information:
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0] = MB_UIF_CMD_PDATA_ECP2_B;
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 1,P_DATA_PH_ECP2);
    SET16(&MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[0], 3,P_DATA_UREA_ECP2);
    MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[5] = MB_UIF_CMD_END;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// STATE DIAGRAMS
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_TEST_LINK
//!
//! Sends LINK TEST
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_TEST_LINK (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send receive CMD
        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
            SPI_TRIALS_RESET();
        }
        completed = SPI_TRIALS_TIMEOUT();
        if(completed==true){
            UIF_ACC_TEST_TIMEOUT++;
            UIF_ACC_STATEDIAG = UIF_STATE_START;
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // send receive DATA
        uif_acc_din = MB_UIF_MSG_TEST_LINK[UIF_ACC_INDEX];
        if(UIF_ACC_INDEX == 0) UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(UIF_ACC_INDEX>0) uif_data_inpx[UIF_ACC_INDEX-1] = uif_acc_dout;
        UIF_ACC_INDEX++;
        if(UIF_ACC_INDEX>=UIF_ACC_BYTES){
            UIF_ACC_STATEDIAG = UIF_STATE_CHKTX;
            SPI_TRIALS_RESET();
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_CHKTX){
        sb_ok = 1;
        for (spicnt=0; spicnt<(UIF_ACC_INDEX-2);spicnt++){
            if(uif_data_inpx[spicnt]!=MB_UIF_MSG_TEST_LINK[spicnt]){
                sb_ok=0; break;
            }
        }
        if(sb_ok == 1) {
            uif_acc_dout = uif_data_inpx[UIF_ACC_INDEX-2];
            if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_ACKNOWLEDGE;
                UIF_ACC_ACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ok++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                UIF_ACC_NACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else{
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ko++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
        }
        else {
          uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
          uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
          uif_acc_data_ko++;
          UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_WAKD_UIF_GET_COMMANDS
//!
//! Gets Commands from UIF
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_WAKD_UIF_GET_COMMANDS (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send TEST1
        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
            SPI_TRIALS_RESET();
        }
        // 20.02.2012: ELSE SHOULD SET TIME OUT AFTER A WHILE...
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // TX CMD RX TEST1
        uif_acc_din = MB_UIF_MSG_CMD_EXCHANGE_BUFFER[0];
        UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == MB_UIF_CMD_TEST1){
            UIF_ACC_STATEDIAG = UIF_STATE_RXD2;  
            SPI_TRIALS_RESET();
        }
        else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD2){       // TX END RX CMD
        uif_acc_din = MB_UIF_MSG_CMD_EXCHANGE_BUFFER[1];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == UIF_ACC_CMD_ID){
            UIF_ACC_STATEDIAG = UIF_STATE_RXD1;  
            SPI_TRIALS_RESET();
        }
        else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD1){       // TX TEST2 RX D[1]
        uif_acc_din = MB_UIF_CMD_TEST2;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        uif_data_inpx[0] = uif_acc_dout;
        UIF_ACC_STATEDIAG = UIF_STATE_RXD0;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD0){       // TX D[1] RX D[0]
        uif_acc_din = uif_data_inpx[0];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        uif_data_inpx[1] = uif_acc_dout;
        UIF_ACC_STATEDIAG = UIF_STATE_RXACKNK;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXACKNK){    // TX D[0] RX ACK/NACK
        uif_acc_din = uif_data_inpx[1];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        UIF_ACC_STATEDIAG = UIF_STATE_TXACKNK;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXACKNK){    // TX ACK/NACK RX --
        uif_acc_din = uif_acc_dout;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
            UIF_ACC_ACK_NO++;
            uif_acc_data_ok++;
            // 19.04.2012 SEE IF OK
            UIF_CMDS_Register = uif_data_inpx[0]<<8 + uif_data_inpx[1];
            // @@@@
            UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            SPI_TRIALS_RESET();
        }
        else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
            UIF_ACC_NACK_NO++;
            UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            SPI_TRIALS_RESET();
       }
       else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
       }

    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        UIF_ACC_COMMAND_OK++;
        UIF_USER_COMMAND = (uif_data_inpx[0] << 8) | uif_data_inpx[1];
        // I assumed here MB accepts the COMMAND (can be also be resent after execution...)
        MB_USER_COMMAND |= UIF_USER_COMMAND;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_WAKD_DATA_GROUP_1
//!
//! Sends ANY INFO from GROUP 1
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_WAKD_DATA_GROUP_1 (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send receive CMD
        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // send receive DATA
        uif_acc_din = MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[UIF_ACC_INDEX];
        if(UIF_ACC_INDEX == 0)  UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0 (20.02.2012: do I use it?
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(UIF_ACC_INDEX>0) uif_data_inpx[UIF_ACC_INDEX-1] = uif_acc_dout;
        UIF_ACC_INDEX++;
        if(UIF_ACC_INDEX>=UIF_ACC_BYTES){
            UIF_ACC_STATEDIAG = UIF_STATE_CHKTX;
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_CHKTX){
        sb_ok = 1;
        for (spicnt=0; spicnt<(UIF_ACC_INDEX-2);spicnt++){
            if(uif_data_inpx[spicnt]!=MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[spicnt]){
                sb_ok=0; break;
            }
        }
        if(sb_ok == 1) {
            uif_acc_dout = uif_data_inpx[UIF_ACC_INDEX-2];
            if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_ACKNOWLEDGE;
                UIF_ACC_ACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ok++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                UIF_ACC_NACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else{
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ko++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
        }
        else {
          uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
          uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
          uif_acc_data_ko++;
          UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
//! \brief  SPI_TRIALS_TIMEOUT
//!
//! returns TRUE if tried communication UIF_COM_TRIALS times
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t SPI_TRIALS_TIMEOUT(){
    uint8_t completed = false;
    uif_acc_trial_counter++;
    if(uif_acc_trial_counter>=UIF_COM_TRIALS){
        uif_acc_trial_counter = 0;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
//! \brief  SPI_TRIALS_RESET
//!
//! returns TRUE if tried communication UIF_COM_TRIALS times
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void SPI_TRIALS_RESET(){
    uif_acc_trial_counter = 0;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
