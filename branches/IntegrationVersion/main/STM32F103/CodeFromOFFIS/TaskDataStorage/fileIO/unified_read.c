 /* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "main.h"
#include "filenames.h"

#include "unified_read.h"
#include "csvparselib/csvparselib.h"

#ifdef LINUX
	#include "global.h"
	#ifdef __arm__
		#include "nephron.h"
	#endif
	#include "interfaces.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "global.h"	//#include "csvparselib/pstdint.h"
	#ifdef __arm__
		#include "..\\..\\nephron.h"
	#endif
	#include "..\\..\\includes_nephron\\interfaces.h"
#endif


#define SD_BLOCK_READSIZE 512





// for testing purposes
//#ifdef __arm__
//	#include "csvparselib\\pstdint.h"	//#include "csvparselib/pstdint.h"
//	#include "..\\..\\nephron.h"
//	#include "..\\..\\typedefsNephron.h"
//#else
//	#include <stdint.h>
//	//#define taskMessage printf
//	#include "..\interfaces.h"
//	#include "..\typedefsNephron.h"
//#endif

#include <limits.h>

#define PREFIX "OFFIS FreeRTOS task DS - CSV read"
//#define DEBUG_DS_IO


/*
int main()
{
	tdActuators db;
	db.timestamp = 126;
	
	printf("%d\n",unified_read(3,&db));
	printf("test value bloodPump: %f, polarizer: %f",db.bloodPump, db.polarizer);
	
	//tdPatientProfile db;
	//unified_read(4,&db);
	//printf("\nThe results are: name %s, gender %c, kCtlDefRemPerDay %g,",db.name, db.gender, db.kCtlDefRemPerDay);
	
	tdWAKDAllStateConfigure db;
	unified_read(5,&db);
	printf("\nThe result is: db.stateRegen1Config.parametersSCC.KAbsL %f , db.stateMaintenanceConfig.defPol_V %i",db.stateRegen1Config.parametersSCC.KAbsL, db.stateMaintenanceConfig.defPol_V);
	return 0;
}*/

//! Reads in a given file and writes the contained values into \a db. If you don't pass the matching \a datatype, behavior will be undefined.
/*!
  Behaviour is dependent on the passed datatype.
  For a \a datatype with timestamps:
  You need to fill your \a db struct with a valid timestamp before passing it.
  All values which were found will be written to \a db, if a value was not found the member variable will contain
  \a SELECT_NOT_FLT for float values or \a SELECT_NOT_INT for int32 values. If the exact timestamp wasn't found but there is a timestamp greater than
  the timestamp you asked for, it will return those values and the corresponding timestamp. This behaviour is limited onto greater timestamps and therefore does not apply
  at smaller timestamps which were found. WARNING: Due use of fseek etc. which limits to 2147483647 bytes , this WON'T WORK ON FILES > 2GB!
  For a \a datatype without timestamps: 
  Reads in the given data.
  \param datatype defines which struct is contained in \a db
  \param db will be casted to the datatype passed by \a datatype
  \return 1 on errors, 0 otherwise
  \sa unified_read(uint8_t datatype, void* db)
*/
uint8_t unified_read(tdDataId datatype, void* db)
{
	char filename[FILENAME_LENGTH];
	FILE* pFile;
	uint32_t lastLF;
	uint8_t firstLine = 1; // boolean to check if we read in only one line so far
	char dataBuffer [DBUFFER_CSVLINE_SIZE]; // memory for reading in a maximum of 950 chars

        char readBuffer[SD_BLOCK_READSIZE];   //Used to store block (512) first
        uint16_t readctr = 0;                 //Used for parsing, looking for newline
	
	char* ptr = NULL;
	char* end = NULL;

	// both needed for data with timestamps
	void* prev_db = NULL;
	uint32_t timestamp;
	
	/*
		Determine filename and allocate memory. ATM i don't check if pvPortMalloc fails(!)
	*/
	switch(datatype)
	{
		case dataID_physiologicalData:
		{
			strncpy(filename,FILE_PHYSIOLOGICALDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_physiologicalData,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdPhysiologicalData));
		}
		break;
		case dataID_physicalData:
		{
			strncpy(filename,FILE_PHYSICALDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_physicalData,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdPhysicalsensorData));
		}
		break;
		case dataID_actuatorData:
		{
			strncpy(filename,FILE_ACTUATORDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_actuatorData,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdActuatorData));
		}
		break;
		case dataID_weightDataOK:
		{
			strncpy(filename,FILE_WEIGHTDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_weightDataOK,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdWeightMeasure));
		}
		break;
		case dataID_bpData:
		{
			strncpy(filename,FILE_BPDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_bpData,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdBpData));
		}
		break;
		case dataID_EcgData:
		{
			strncpy(filename,FILE_ECGDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_EcgData,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdEcgData));
		}
		break;
		case dataID_statusRTB:
		{
			strncpy(filename,FILE_DEVICESSTATUS,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_statusRTB,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdDevicesStatus));
		}
		break;
		/*case dataID_configureStateDump:
		{
			strncpy(filename,FILE_MSGCONFIGURESTATE,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_configureStateDump,db,0)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			prev_db = pvPortMalloc(sizeof(tdMsgConfigureState));
		}
		break;		*/
		case dataID_PatientProfile:
			strncpy(filename,FILE_PATIENTPROFILE,FILENAME_LENGTH);
		break;
		case dataID_WAKDAllStateConfigure:
			strncpy(filename,FILE_WAKDALLSTATECONFIGURE,FILENAME_LENGTH);
		break;
		case dataID_configureState:
			strncpy(filename,FILE_WAKDALLSTATECONFIGURE,FILENAME_LENGTH);
		break;
		case dataID_AllStatesParametersSCC:
			strncpy(filename,FILE_ALLSTATESPARAMETERSSCC,FILENAME_LENGTH);
		break;
		case dataID_StateParametersSCC:
			strncpy(filename,FILE_ALLSTATESPARAMETERSSCC,FILENAME_LENGTH);
		break;
		default:
		{
			defaultIdHandling(PREFIX, datatype);
			return 1;
		}
		break;
	}	
#ifdef VP_SIMULATION
	pFile = fopen(filename, "rb+");
	if (pFile == NULL)
	{
		taskMessage("F", PREFIX, "Error opening file %s", filename);
		switch(datatype)
		{
			case dataID_physiologicalData:
				sFree((void *)&prev_db);
			break;
			case dataID_physicalData:
				sFree((void *)&prev_db);
			break;
			case dataID_actuatorData:
				sFree((void *)&prev_db);
			break;
			case dataID_weightDataOK:
				sFree((void *)&prev_db);
			break;
			case dataID_bpData:
				sFree((void *)&prev_db);
			break;
			case dataID_EcgData:
				sFree((void *)&prev_db);
			break;
			case dataID_statusRTB:
				sFree((void *)&prev_db);
			break;
			/*case dataID_configureStateDump:
				sFree((void *)&prev_db);
			break;*/
			case dataID_PatientProfile:
				;
			break;
			case dataID_WAKDAllStateConfigure:
				;
			break;
			case dataID_configureState:
				;
			break;
			case dataID_AllStatesParametersSCC:
				;
			case dataID_StateParametersSCC:
				;
			break;
			default:
			{
				defaultIdHandling(PREFIX, datatype);
				return 1;
			}
			break;
		}
		return 1;
	}
	/* 
		File exists and can be opened - proceed
	*/
	else
	{
		/*	hate me for this, just following orders..
			switch/case to determine how and which data should be read in
			and passed out as a void pointer
		*/
		uint8_t erroroccured = 0;
		switch(datatype)
		{
			// these are sensor readout with timestamps and so on
			case dataID_physiologicalData:
			case dataID_physicalData:
			case dataID_actuatorData:
			case dataID_weightDataOK:
			case dataID_bpData:
			case dataID_EcgData:
			case dataID_statusRTB:
			/*case dataID_configureStateDump:*/
			{
				// simple check if memory allocation failed
				if (prev_db==NULL)
				{
					// unable to allocate memory
					taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
					#ifdef __arm__
						errMsg(errmsg_pvPortMallocFailed);
					#endif
					return 1;
				}
				/*
				Save the value of timestamp before it is overwritten.
				prev_timestamp is used to save the previous values
				e.g. the line after the current one in the file.
				*/
				timestamp = extract_timestamp(datatype, db,0);
				fseek (pFile, 0, SEEK_END);	// jump to the end of file
				movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
				while(!seekToLastLF(pFile) || firstLine)
				{
					/*	Seek backwards the begin of the line - and break if we reach the first byte.
						Because we don't have negative timestamps and it will return with "nothing found"
						anyway if timestamp 0 doesn't match, this probably will never be reached. Just
						to make sure that we won't try to parse the header description line, we
						check it.
					*/
					lastLF = ftell(pFile); // remember position of the last line
					fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile); // read in a whole line
					//puts(dataBuffer);
					/*
						ptr is used to tell parseLine where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;	
					/*
						Parse, parse, parse all values. 
					*/	
					ReadInMemberValues(datatype, db, &ptr, end, 0);	
					/*
						no &ptr here because we want to pass by value and use the unchanged ptr
						in case of a CRC error to get to CRC: without seeking again.
					*/
					if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE) && (extract_timestamp(datatype, db,0) != SELECT_NOT_INT))
					{
						taskMessage("F", PREFIX, "CRC error while reading line with timestamp %d, continuing with next line", extract_timestamp(datatype, db,0));
						/* 
							rewind to remembered position, add length of dataBuffer
							and subtract 6 to pass \n;4321 (which would be the faulty CRC value)
						*/
						fseek(pFile,lastLF - ftell(pFile) + strnlen(dataBuffer, DBUFFER_CSVLINE_SIZE) - 6,SEEK_CUR);
						// mark line as faulty by changing crc value to CRC:ERRD
						fprintf(pFile,"ERRD");
						// now we can rewind again and do another while iteration
						fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); 
						movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
						firstLine = 0;
						continue;
					}
					
					#ifdef DEBUG_DS_IO
					taskMessage("I", PREFIX, "timestamp = %d, firstLine = %d", timestamp, firstLine);
					#endif	

					/*
						correct timestamp or we only one timestamp which is bigger than
						the one we're searching for
					*/
					if((extract_timestamp(datatype, db,0) == timestamp)) 
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "(db->timestamp == timestamp)\n");
						#endif	
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					// exact timestamp doesn't exist, maybe a bigger one if we got a next line?
					else if((extract_timestamp(datatype, db,0) < timestamp && !firstLine) || (extract_timestamp(datatype, db,0) == SELECT_NOT_INT && !firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "db->timestamp < timestamp && !firstLine || (db->timestamp == SELECT_NOT_INT && !firstLine)\n");
						#endif	
						/*	This looks a bit cryptic - i'll cast both void pointers
							to the specific struct and deference them to copy by value. Otherwise we would assign the pointers 
							and prev_db will be free()d after the function ends and a dangling pointer would appear.
						*/
						switch(datatype)
						{
							case dataID_physiologicalData:
								*((tdPhysiologicalData *)db) = *((tdPhysiologicalData *)prev_db);
							break;
							case dataID_physicalData:
								*((tdPhysicalsensorData *)db) = *((tdPhysicalsensorData *)prev_db);
							break;
							case dataID_actuatorData:
								*((tdActuatorData *)db) = *((tdActuatorData *)prev_db);
							break;
							case dataID_weightDataOK:
							    *((tdWeightMeasure *)db) = *((tdWeightMeasure *)prev_db);
							break;
							case dataID_bpData:
								*((tdBpData *)db) = *((tdBpData *)prev_db);
							break;
							case dataID_EcgData:
								*((tdEcgData *)db) = *((tdEcgData *)prev_db);
							break;
							case dataID_statusRTB:
								*((tdDevicesStatus *)db) = *((tdDevicesStatus *)prev_db);
							break;
							/*case dataID_configureStateDump:
								*((tdMsgConfigureState *)db) = *((tdMsgConfigureState *)prev_db);
							break;*/
							default: 
								taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
								sFree((void *)&prev_db);
								return 1;
							break;
						}
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					// last check if we really haven't found anything
					else if((extract_timestamp(datatype, db,0) < timestamp && firstLine) || (extract_timestamp(datatype, db,0) == SELECT_NOT_INT && firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "(db->timestamp < timestamp && firstLine) || (timestamp == SELECT_NOT_INT && firstLine)\n");
						#endif	
						/*
							mark all values as not found - otherwise we would return
							the last line which was read in
						*/
						ReadInMemberValues(datatype, db, &ptr, end, 1);
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); // rewind to remembered position
					movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
					firstLine = 0;
					/*
						Save previous values, maybe we'll need them. This looks a bit cryptic - i'll cast both void pointers
						to the specific struct and deference them to copy by value. Otherwise we would assign the pointers 
						and prev_db will be free()d after the function ends and a dangling pointer would appear.
					*/
					switch(datatype)
					{
						case dataID_physiologicalData:
							*((tdPhysiologicalData *)prev_db) = *((tdPhysiologicalData *)db);
						break;
						case dataID_physicalData:
							*((tdPhysicalsensorData *)prev_db) = *((tdPhysicalsensorData *)db);
						break;
						case dataID_actuatorData:
							*((tdActuatorData *)prev_db) = *((tdActuatorData *)db);
						break;
						case dataID_weightDataOK:
							*((tdWeightMeasure *)prev_db) = *((tdWeightMeasure *)db);
						break;
						case dataID_bpData:
							*((tdBpData *)prev_db) = *((tdBpData *)db);
						break;
						case dataID_EcgData:
							*((tdEcgData *)prev_db) = *((tdEcgData *)db);
						break;
						case dataID_statusRTB:
							*((tdDevicesStatus *)prev_db) = *((tdDevicesStatus *)db);
						break;
						/*case dataID_configureStateDump:
							*((tdMsgConfigureState *)prev_db) = *((tdMsgConfigureState *)db);
						break;*/
						default: 
							taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
							sFree((void *)&prev_db);
							return 1;
						break;
					}
				}
				/*
					this usually means either there is no greater timestamp than the value which was searched for
					or the file is corrupt
				*/
				taskMessage("F", PREFIX, "!!!!!Nothing found in %s", filename);
				fclose (pFile);
				sFree((void *)&prev_db);
				return 1;
			}
			break;
			case dataID_PatientProfile:
			{
				//patient data
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				// second line, containing the data
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					fclose(pFile);
					taskMessage("F", PREFIX, "No patient data found while reading File %s", filename);
					return 1;
				}
				ptr = dataBuffer;
			
				erroroccured |= parseString(&ptr,((tdPatientProfile*)db)->name, 50); // if you change it here, change it in the typedef too
				
				((tdPatientProfile*)db)->gender = *ptr;
				ptr += 2; // to pass the gender and the ; after gender
				
				((tdPatientProfile*)db)->measureTimeHours = parseLine(&ptr,end);
				((tdPatientProfile*)db)->measureTimeMinutes = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->wghtCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->wghtCtlLastMeasurement = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->wghtCtlDefRemPerDay = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlLastMeasurement = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlDefRemPerDay = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlLastMeasurement = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlDefRemPerDay = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->minDialPlasFlow = parseLine(&ptr,end);
				((tdPatientProfile*)db)->maxDialPlasFlow = parseLine(&ptr,end);
				((tdPatientProfile*)db)->minDialVoltage = parseLine(&ptr,end);
				((tdPatientProfile*)db)->maxDialVoltage = parseLine(&ptr,end);
				
				/*
				((tdPatientProfile*)db)->minDialPlasFlow = parseLineFloat(&ptr,end);
				((tdPatientProfile*)db)->maxDialPlasFlow = parseLineFloat(&ptr,end);
				((tdPatientProfile*)db)->minDialVoltage = parseLineFloat(&ptr,end);
				((tdPatientProfile*)db)->maxDialVoltage = parseLineFloat(&ptr,end);
				*/
				
				if(erroroccured)
				{
					fclose(pFile);
					#ifdef DEBUG_DS
					taskMessage("I", PREFIX, "File %s contains empty or damaged data", filename);
					#endif	
					return 2; // means file should be okay but this state is empty or CRC wrong
				}		
				if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
				{
					fclose(pFile);
					taskMessage("F", PREFIX, "CRC error on File %s", filename);
					return 1;
				}
				fclose (pFile);
			}
			break;
			case dataID_WAKDAllStateConfigure: 
			{
				// state configuration
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				tdWakdStates state = wakdStates_AllStopped;
				while(1) // will be terminated in the switch case at the end of the loop
				{
					/* 
					* just to make sure that we don't miss a state even if they're in the wrong order
					*/
					fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR); 
					while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
					{
						/*
							ptr is used to tell parseLineFloat where to start 
							and save the current pointer position for every parse.
						*/
						ptr = dataBuffer;
						/*
							double cast to remove warning caused by int32_t
						*/
						current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
						if(current_state == state)
						{
							switch(current_state)
							{
								case wakdStates_AllStopped:{
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateAllStoppedConfig));
								break;
								}
								case wakdStates_Dialysis: {
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateDialysisConfig));
										//taskMessage("I", PREFIX, "switch(current_state) Dialysis duration %i",((tdWAKDAllStateConfigure*)db)->stateDialysisConfig.duration_sec);
/*
										current_values->defSpeedFp_mlPmin = tmp; 
										//taskMessage("I", PREFIX, "BP preset %i", tmp );
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->defPol_V = tmp; 
										taskMessage("I", PREFIX, "FP preset %i", tmp );
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->direction = tmp; 
										taskMessage("I", PREFIX, "defPol_V preset %i", tmp);
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->duration_sec = tmp; 
										taskMessage("I", PREFIX, "duration_sec preset %i", tmp );	
*/								}	
								break;
								case wakdStates_Regen1:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateRegen1Config));
								break;
								case wakdStates_Ultrafiltration:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateUltrafiltrationConfig));
								break;
								case wakdStates_Regen2:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateRegen2Config));
								break;
								case wakdStates_Maintenance:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateMaintenanceConfig));
								break;
								case wakdStates_NoDialysate:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateNoDialysateConfig));
								break;
								default:
									fclose(pFile);
									taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
									return 1;
								break;
							}
							if(erroroccured)
							{
								fclose(pFile);
								#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
								#endif	
								return 2; // means file should be okay but this state is empty or CRC wrong
							}		
							if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
							{
								fclose(pFile);
								taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
								return 1;
							}	
							found = 1; // leave while(fget(......)) if found and either continue to next state or exit
						}
						/* if we reach this, the current read in line isn't the one 
						 * we're searching for and need to continue with the next one
						*/
					}
					if(found == 0)
					{
						// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
						taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, state);
						return 1;
					}
					else if(found == 1)
					{
						found = 0;
					}
					/*
						if you add states, you need to add them here too because we can't iterate though enums in c
					*/
					switch(state)
					{
						case wakdStates_AllStopped:
							state = wakdStates_Dialysis;
						break;
						case wakdStates_Dialysis:
							state = wakdStates_Regen1;
						break;
						case wakdStates_Regen1:
							state = wakdStates_Ultrafiltration;
						break;
						case wakdStates_Ultrafiltration:
							state = wakdStates_Regen2;
						break;
						case wakdStates_Regen2:
							state = wakdStates_Maintenance;
						break;
						case wakdStates_Maintenance:
							state = wakdStates_NoDialysate;
						break;
						case wakdStates_NoDialysate:
							fclose(pFile);
							return 0; // exit because we found every state
						break;
						default:
							fclose(pFile);
							taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
							return 1;
						break;
					}
				}
				taskMessage("E", PREFIX, "This is after a while(1) loop so something must be very wrong");
				fclose(pFile);
				return 2;
			}
			break;
			case dataID_configureState:
			{
				// state configuration
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR);  // move after the header
				while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
				{
					/*
						ptr is used to tell parseLineFloat where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;
					/*
						double cast to remove warning caused by int32_t
					*/
					current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
					if(current_state == ((tdWAKDStateConfigurationParameters*)db)->thisState)
					{
						readWholeState(&ptr, end, &erroroccured, (tdWAKDStateConfigurationParameters*)db);
						if(erroroccured)
						{
							fclose(pFile);
							#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
							#endif	
							return 2; // means file should be okay but this state is empty or CRC wrong
						}		
						if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
						{
							fclose(pFile);
							taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
							return 1;
						}	
						found = 1; // leave while(fget(......)) if found and either continue to next state or exit
					}
					/* if we reach this, the current read in line isn't the one 
					 * we're searching for and need to continue with the next one
					*/
				}
				if(found == 0)
				{
					// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
					fclose(pFile);
					taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, ((tdWAKDStateConfigurationParameters*)db)->thisState);
					return 1;
				}
				else
				{
					fclose(pFile);
					return 0;
				}
			}
			break;			
			case dataID_AllStatesParametersSCC: 
			{
				// state params
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				tdWakdStates state = wakdStates_AllStopped;
				while(1) // will be terminated in the switch case at the end of the loop
				{
					/* 
					* just to make sure that we don't miss a state even if they're in the wrong order
					*/
					fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR); 
					while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
					{
						/*
							ptr is used to tell parseLineFloat where to start 
							and save the current pointer position for every parse.
						*/
						ptr = dataBuffer;
						/*
							double cast to remove warning caused by int32_t
						*/
						current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
						if(current_state == state)
						{
							switch(current_state)
							{
								case wakdStates_AllStopped:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateAllStopped));
								break;
								case wakdStates_Dialysis:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateDialysis));
								break;
								case wakdStates_Regen1:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen1));
								break;
								case wakdStates_Ultrafiltration:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateUltrafiltration));
								break;
								case wakdStates_Regen2:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen2));
								break;
								case wakdStates_Maintenance:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateMaintenance));
								break;
								case wakdStates_NoDialysate:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateNoDialysate));
								break;
								default:
									fclose(pFile);
									taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
									return 1;
								break;
							}
							if(erroroccured)
							{
								fclose(pFile);
								#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
								#endif	
								return 2; // means file should be okay but this state is empty or CRC wrong
							}		
							if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
							{
								fclose(pFile);
								taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
								return 1;
							}	
							found = 1; // leave while(fget(......)) if found and either continue to next state or exit
						}
						/* if we reach this, the current read in line isn't the one 
						 * we're searching for and need to continue with the next one
						*/
					}
					if(found == 0)
					{
						// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
						taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, state);
						return 1;
					}
					else if(found == 1)
					{
						found = 0;
					}
					/*
						if you add states, you need to add them here too because we can't iterate though enums in c
					*/
					switch(state)
					{
						case wakdStates_AllStopped:
							state = wakdStates_Dialysis;
						break;
						case wakdStates_Dialysis:
							state = wakdStates_Regen1;
						break;
						case wakdStates_Regen1:
							state = wakdStates_Ultrafiltration;
						break;
						case wakdStates_Ultrafiltration:
							state = wakdStates_Regen2;
						break;
						case wakdStates_Regen2:
							state = wakdStates_Maintenance;
						break;
						case wakdStates_Maintenance:
							state = wakdStates_NoDialysate;
						break;
						case wakdStates_NoDialysate:
							fclose(pFile);
							return 0; // exit because we found every state
						break;
						default:
							fclose(pFile);
							taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
							return 1;
						break;
					}
				}
				taskMessage("I", PREFIX, "This is after a while(1) loop so something must be very wrong");
				fclose(pFile);
				return 2;
			}
			break;
			case dataID_StateParametersSCC:
			{
				// stateparam configuration
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Starting state in file %s is %i", filename, ((tdParametersSCC*)db)->thisState);
					#endif
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR);  // move after the header
				while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
				{
					/*
						ptr is used to tell parseLineFloat where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;
					/*
						double cast to remove warning caused by int32_t
					*/
					current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
					#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Current state in file %s is %i", filename, current_state);
					#endif	
					if(current_state == ((tdParametersSCC*)db)->thisState)
					{
						#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Current state in file %s matches", filename, current_state);
						#endif	
						readWholeStateParam(&ptr, end, &erroroccured, (tdParametersSCC*)db);
						if(erroroccured)
						{
							fclose(pFile);
							#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "File %s contains empty or damaged state parameter %i", filename, current_state);
							#endif	
							return 2; // means file should be okay but this stateparam is empty or CRC wrong
						}		
						if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
						{
							fclose(pFile);
							taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
							return 1;
						}	
						found = 1; // leave while(fget(......)) if found and either continue to next state or exit
					}
					/* if we reach this, the current read in line isn't the one 
					 * we're searching for and need to continue with the next one
					*/
				}
				if(found == 0)
				{
					// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
					fclose(pFile);
					taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, ((tdParametersSCC*)db)->thisState);
					return 1;
				}
				else
				{
					fclose(pFile);
					return 0;
				}
			}
			break;
			default:
			{	
				fclose(pFile);
				defaultIdHandling(PREFIX, datatype);
				return 1;
			}
			break;
		}
	}
//#endif  // VP_SIMULATION

// ***********************************************************************************
// INTEGRATION 20120930
// Now for integration, we do the same, only with sdcard.c macro functions to read
// ***********************************************************************************
#else

        //pFile = fopen(filename, "rb+");	
        #ifndef DATA_H_
          #warning HELP NO DATA_H IN UNIFIED READ
        #endif

        pFile = &sdcard_filerdx;
        
        if ( file_fopen( pFile, &sdcard_efs.myFs ,filename , 'r' ) != 0 )
        //INTEGRATION TEST: if ( file_fopen( pFile, &sdcard_efs.myFs , "PAT.TXT" , 'r' ) != 0 )
	{
		taskMessage("F", PREFIX, "Error opening file %s", filename);
		switch(datatype)
		{
			case dataID_physiologicalData:
				sFree((void *)&prev_db);
			break;
			case dataID_physicalData:
				sFree((void *)&prev_db);
			break;
			case dataID_actuatorData:
				sFree((void *)&prev_db);
			break;
			case dataID_weightDataOK:
				sFree((void *)&prev_db);
			break;
			case dataID_bpData:
				sFree((void *)&prev_db);
			break;
			case dataID_EcgData:
				sFree((void *)&prev_db);
			break;
			case dataID_statusRTB:
				sFree((void *)&prev_db);
			break;
			/*case dataID_configureStateDump:
				sFree((void *)&prev_db);
			break;*/
			case dataID_PatientProfile:
				;
			break;
			case dataID_WAKDAllStateConfigure:
				;
			break;
			case dataID_configureState:
				;
			break;
			case dataID_AllStatesParametersSCC:
				;
			case dataID_StateParametersSCC:
				;
			break;
			default:
			{
				defaultIdHandling(PREFIX, datatype);
				return 1;
			}
			break;
		}
		return 1;
	}
	/* 
		File exists and can be opened - proceed
	*/
	else
	{
		/*	hate me for this, just following orders..
			switch/case to determine how and which data should be read in
			and passed out as a void pointer
		*/
		uint8_t erroroccured = 0;
		switch(datatype)
		{
			// these are sensor readout with timestamps and so on
			case dataID_physiologicalData:
			case dataID_physicalData:
			case dataID_actuatorData:
			case dataID_weightDataOK:
			case dataID_bpData:
			case dataID_EcgData:
		#if 0
                        case dataID_statusRTB:
			//case dataID_configureStateDump:
			{
				// simple check if memory allocation failed
				if (prev_db==NULL)
				{
					// unable to allocate memory
					taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
					#ifdef __arm__
						errMsg(errmsg_pvPortMallocFailed);
					#endif
					return 1;
				}
				/*
				Save the value of timestamp before it is overwritten.
				prev_timestamp is used to save the previous values
				e.g. the line after the current one in the file.
				*/
				timestamp = extract_timestamp(datatype, db,0);
				fseek (pFile, 0, SEEK_END);	// jump to the end of file
				movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
				while(!seekToLastLF(pFile) || firstLine)
				{
					/*	Seek backwards the begin of the line - and break if we reach the first byte.
						Because we don't have negative timestamps and it will return with "nothing found"
						anyway if timestamp 0 doesn't match, this probably will never be reached. Just
						to make sure that we won't try to parse the header description line, we
						check it.
					*/
					lastLF = ftell(pFile); // remember position of the last line
					fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile); // read in a whole line
					//puts(dataBuffer);
					/*
						ptr is used to tell parseLine where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;	
					/*
						Parse, parse, parse all values. 
					*/	
					ReadInMemberValues(datatype, db, &ptr, end, 0);	
					/*
						no &ptr here because we want to pass by value and use the unchanged ptr
						in case of a CRC error to get to CRC: without seeking again.
					*/
					if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE) && (extract_timestamp(datatype, db,0) != SELECT_NOT_INT))
					{
						taskMessage("F", PREFIX, "CRC error while reading line with timestamp %d, continuing with next line", extract_timestamp(datatype, db,0));
						/* 
							rewind to remembered position, add length of dataBuffer
							and subtract 6 to pass \n;4321 (which would be the faulty CRC value)
						*/
						fseek(pFile,lastLF - ftell(pFile) + strnlen(dataBuffer, DBUFFER_CSVLINE_SIZE) - 6,SEEK_CUR);
						// mark line as faulty by changing crc value to CRC:ERRD
						fprintf(pFile,"ERRD");
						// now we can rewind again and do another while iteration
						fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); 
						movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
						firstLine = 0;
						continue;
					}
					
					#ifdef DEBUG_DS_IO
					taskMessage("I", PREFIX, "timestamp = %d, firstLine = %d", timestamp, firstLine);
					#endif	

					/*
						correct timestamp or we only one timestamp which is bigger than
						the one we're searching for
					*/
					if((extract_timestamp(datatype, db,0) == timestamp)) 
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "(db->timestamp == timestamp)\n");
						#endif	
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					// exact timestamp doesn't exist, maybe a bigger one if we got a next line?
					else if((extract_timestamp(datatype, db,0) < timestamp && !firstLine) || (extract_timestamp(datatype, db,0) == SELECT_NOT_INT && !firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "db->timestamp < timestamp && !firstLine || (db->timestamp == SELECT_NOT_INT && !firstLine)\n");
						#endif	
						/*	This looks a bit cryptic - i'll cast both void pointers
							to the specific struct and deference them to copy by value. Otherwise we would assign the pointers 
							and prev_db will be free()d after the function ends and a dangling pointer would appear.
						*/
						switch(datatype)
						{
							case dataID_physiologicalData:
								*((tdPhysiologicalData *)db) = *((tdPhysiologicalData *)prev_db);
							break;
							case dataID_physicalData:
								*((tdPhysicalsensorData *)db) = *((tdPhysicalsensorData *)prev_db);
							break;
							case dataID_actuatorData:
								*((tdActuatorData *)db) = *((tdActuatorData *)prev_db);
							break;
							case dataID_weightDataOK:
							    *((tdWeightMeasure *)db) = *((tdWeightMeasure *)prev_db);
							break;
							case dataID_bpData:
								*((tdBpData *)db) = *((tdBpData *)prev_db);
							break;
							case dataID_EcgData:
								*((tdEcgData *)db) = *((tdEcgData *)prev_db);
							break;
							case dataID_statusRTB:
								*((tdDevicesStatus *)db) = *((tdDevicesStatus *)prev_db);
							break;
							/*case dataID_configureStateDump:
								*((tdMsgConfigureState *)db) = *((tdMsgConfigureState *)prev_db);
							break;*/
							default: 
								taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
								sFree((void *)&prev_db);
								return 1;
							break;
						}
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					// last check if we really haven't found anything
					else if((extract_timestamp(datatype, db,0) < timestamp && firstLine) || (extract_timestamp(datatype, db,0) == SELECT_NOT_INT && firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "(db->timestamp < timestamp && firstLine) || (timestamp == SELECT_NOT_INT && firstLine)\n");
						#endif	
						/*
							mark all values as not found - otherwise we would return
							the last line which was read in
						*/
						ReadInMemberValues(datatype, db, &ptr, end, 1);
						fclose (pFile);
						sFree((void *)&prev_db);
						return 0;
					}
					fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); // rewind to remembered position
					movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
					firstLine = 0;
					/*
						Save previous values, maybe we'll need them. This looks a bit cryptic - i'll cast both void pointers
						to the specific struct and deference them to copy by value. Otherwise we would assign the pointers 
						and prev_db will be free()d after the function ends and a dangling pointer would appear.
					*/
					switch(datatype)
					{
						case dataID_physiologicalData:
							*((tdPhysiologicalData *)prev_db) = *((tdPhysiologicalData *)db);
						break;
						case dataID_physicalData:
							*((tdPhysicalsensorData *)prev_db) = *((tdPhysicalsensorData *)db);
						break;
						case dataID_actuatorData:
							*((tdActuatorData *)prev_db) = *((tdActuatorData *)db);
						break;
						case dataID_weightDataOK:
							*((tdWeightMeasure *)prev_db) = *((tdWeightMeasure *)db);
						break;
						case dataID_bpData:
							*((tdBpData *)prev_db) = *((tdBpData *)db);
						break;
						case dataID_EcgData:
							*((tdEcgData *)prev_db) = *((tdEcgData *)db);
						break;
						case dataID_statusRTB:
							*((tdDevicesStatus *)prev_db) = *((tdDevicesStatus *)db);
						break;
						/*case dataID_configureStateDump:
							*((tdMsgConfigureState *)prev_db) = *((tdMsgConfigureState *)db);
						break;*/
						default: 
							taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
							sFree((void *)&prev_db);
							return 1;
						break;
					}
				}
				/*
					this usually means either there is no greater timestamp than the value which was searched for
					or the file is corrupt
				*/
				taskMessage("F", PREFIX, "!!!!!Nothing found in %s", filename);
				fclose (pFile);
				sFree((void *)&prev_db);
				return 1;
			}
                #endif // 0 statusRTB / put before the break, look above
			break;

                  
                   
			case dataID_PatientProfile:
			{
				
                                //patient data
				// read in the first line, which is the header
				/*
                                if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				// second line, containing the data
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					fclose(pFile);
					taskMessage("F", PREFIX, "No patient data found while reading File %s", filename);
					return 1;
				}*/

                                // INTEGRATION:
                                // Reading only 512 bytes here; patientprofile file should be small
                                // However, this is pretty ugly... 
  
                                uint16_t i,j, bytesread = 0;
                                
                                // Read first Block 
                                bytesread = file_read( pFile, SD_BLOCK_READSIZE , dataBuffer );
                                if(bytesread == 0 ){
					file_fclose( pFile );
					taskMessage("F", PREFIX, "No patient data found while reading File %s", filename);
					return 1;
				}                                                                
				
                                //Skip through the first line... search for newline = 0x0a;
                                for(i=0; i<bytesread; i++){
                                  if( (uint8_t) dataBuffer[i] == 0x0a ) break; 
                                }
                                if(i == bytesread){
					file_fclose( pFile );
					taskMessage("F", PREFIX, "No newline found in first 512 bytes header of file %s", filename);
					return 1;
				}  
                                i++;
                                // Pushing dataBuffer left
                                for(j=0; i<bytesread; j++){
                                   dataBuffer[j] = dataBuffer[i++];
                                }

                                ptr = dataBuffer;


                                // This is the next line
				erroroccured |= parseString(&ptr,((tdPatientProfile*)db)->name, 50); // if you change it here, change it in the typedef too
				
				((tdPatientProfile*)db)->gender = *ptr;
				ptr += 2; // to pass the gender and the ; after gender
				
				((tdPatientProfile*)db)->measureTimeHours = parseLine(&ptr,end);
				((tdPatientProfile*)db)->measureTimeMinutes = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->wghtCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->wghtCtlLastMeasurement = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->wghtCtlDefRemPerDay = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlLastMeasurement = parseLine(&ptr,end);
				((tdPatientProfile*)db)->kCtlDefRemPerDay = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlTarget = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlLastMeasurement = parseLine(&ptr,end);
				((tdPatientProfile*)db)->urCtlDefRemPerDay = parseLine(&ptr,end);
				
				((tdPatientProfile*)db)->minDialPlasFlow = parseLine(&ptr,end);
				((tdPatientProfile*)db)->maxDialPlasFlow = parseLine(&ptr,end);
				((tdPatientProfile*)db)->minDialVoltage = parseLine(&ptr,end);
				((tdPatientProfile*)db)->maxDialVoltage = parseLine(&ptr,end);
				
								
				if(erroroccured)
				{
					//fclose(pFile);
					file_fclose( pFile );
                                        #ifdef DEBUG_DS
					taskMessage("I", PREFIX, "File %s contains empty or damaged data", filename);
					#endif	
					return 2; // means file should be okay but this state is empty or CRC wrong
				}		
                                //Integration CRC CHECK HW
                                // Dont know if this is gonna work for HW:
				
                                #ifndef SDCRCDISABLE
                                if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
				{
					                                       
                                        //fclose(pFile);
					file_fclose( pFile );
                                        taskMessage("F", PREFIX, "CRC error on File %s", filename);
					return 1;
				}
                                #endif
                                
				//fclose (pFile);
                                file_fclose( pFile );

			}
			break;
            #if 0
                        case dataID_WAKDAllStateConfigure: 
			{
				// state configuration
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				tdWakdStates state = wakdStates_AllStopped;
				while(1) // will be terminated in the switch case at the end of the loop
				{
					/* 
					* just to make sure that we don't miss a state even if they're in the wrong order
					*/
					fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR); 
					while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
					{
						/*
							ptr is used to tell parseLineFloat where to start 
							and save the current pointer position for every parse.
						*/
						ptr = dataBuffer;
						/*
							double cast to remove warning caused by int32_t
						*/
						current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
						if(current_state == state)
						{
							switch(current_state)
							{
								case wakdStates_AllStopped:{
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateAllStoppedConfig));
								break;
								}
								case wakdStates_Dialysis: {
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateDialysisConfig));
										//taskMessage("I", PREFIX, "switch(current_state) Dialysis duration %i",((tdWAKDAllStateConfigure*)db)->stateDialysisConfig.duration_sec);
/*
										current_values->defSpeedFp_mlPmin = tmp; 
										//taskMessage("I", PREFIX, "BP preset %i", tmp );
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->defPol_V = tmp; 
										taskMessage("I", PREFIX, "FP preset %i", tmp );
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->direction = tmp; 
										taskMessage("I", PREFIX, "defPol_V preset %i", tmp);
										
										*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
										current_values->duration_sec = tmp; 
										taskMessage("I", PREFIX, "duration_sec preset %i", tmp );	
*/								}	
								break;
								case wakdStates_Regen1:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateRegen1Config));
								break;
								case wakdStates_Ultrafiltration:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateUltrafiltrationConfig));
								break;
								case wakdStates_Regen2:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateRegen2Config));
								break;
								case wakdStates_Maintenance:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateMaintenanceConfig));
								break;
								case wakdStates_NoDialysate:
									readWholeState(&ptr, end, &erroroccured, &(((tdWAKDAllStateConfigure*)db)->stateNoDialysateConfig));
								break;
								default:
									fclose(pFile);
									taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
									return 1;
								break;
							}
							if(erroroccured)
							{
								fclose(pFile);
								#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
								#endif	
								return 2; // means file should be okay but this state is empty or CRC wrong
							}		
							if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
							{
								fclose(pFile);
								taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
								return 1;
							}	
							found = 1; // leave while(fget(......)) if found and either continue to next state or exit
						}
						/* if we reach this, the current read in line isn't the one 
						 * we're searching for and need to continue with the next one
						*/
					}
					if(found == 0)
					{
						// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
						taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, state);
						return 1;
					}
					else if(found == 1)
					{
						found = 0;
					}
					/*
						if you add states, you need to add them here too because we can't iterate though enums in c
					*/
					switch(state)
					{
						case wakdStates_AllStopped:
							state = wakdStates_Dialysis;
						break;
						case wakdStates_Dialysis:
							state = wakdStates_Regen1;
						break;
						case wakdStates_Regen1:
							state = wakdStates_Ultrafiltration;
						break;
						case wakdStates_Ultrafiltration:
							state = wakdStates_Regen2;
						break;
						case wakdStates_Regen2:
							state = wakdStates_Maintenance;
						break;
						case wakdStates_Maintenance:
							state = wakdStates_NoDialysate;
						break;
						case wakdStates_NoDialysate:
							fclose(pFile);
							return 0; // exit because we found every state
						break;
						default:
							fclose(pFile);
							taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
							return 1;
						break;
					}
				}
				taskMessage("E", PREFIX, "This is after a while(1) loop so something must be very wrong");
				fclose(pFile);
				return 2;
			}
			break;
                 #endif
			case dataID_configureState:
			{
				// state configuration
				// read in the first line, which is the header
				/*
                                if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR);  // move after the header
				*/
                                
                                uint8_t found = 0;
                                uint16_t i,j,k, bytesread = 0;
                                tdWakdStates current_state;

                                // Read first Block 
                                bytesread = file_read( pFile, SD_BLOCK_READSIZE , dataBuffer );
                                if(bytesread == 0 ){
					file_fclose( pFile );
					taskMessage("F", PREFIX, "No data found while reading File %s", filename);
					return 1;
				}                                                                
				
                                //Skip through the first line... search for newline = 0x0a;
                                for(i=0; i<bytesread; i++){
                                  if( (uint8_t) dataBuffer[i] == 0x0a ) break; 
                                }
                                if(i == bytesread){
					file_fclose( pFile );
					taskMessage("F", PREFIX, "No newline found in first 512 bytes header of file %s", filename);
					return 1;
				}  
                                i++;
                                // Pushing dataBuffer left, we keep i for bytesparsed!
                                for(j=0; (i+j)<bytesread; j++){
                                   dataBuffer[j] = dataBuffer[i+j];
                                }
                               
                                
                                //INTEGRATION:
                                //State_c.csv is about 260 bytes, so reading one 512 block is enough
                                //We remove fgets, because we dont have that available in LIB efsl.
                                //Instead at the end of the loop, we shift the buffer left. 

                                //while(fgets (dataBuffer,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
				while( (i<bytesread) && !found)
				{
					/*
						ptr is used to tell parseLineFloat where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;
					/*
						double cast to remove warning caused by int32_t
					*/
					current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
					if(current_state == ((tdWAKDStateConfigurationParameters*)db)->thisState)
					{
						readWholeState(&ptr, end, &erroroccured, (tdWAKDStateConfigurationParameters*)db);
						if(erroroccured)
						{
							fclose(pFile);
							#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
							#endif	
							return 2; // means file should be okay but this state is empty or CRC wrong
						}	
                                                #ifndef SDCRCDISABLE
						if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
						{
							fclose(pFile);
							taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
							return 1;
						}
                                                #endif
						found = 1; // leave while(fget(......)) if found and either continue to next state or exit
					}
					/* if we reach this, the current read in line isn't the one 
					 * we're searching for and need to continue with the next one
					*/

                                        //INTEGRATION, look for end line char, then shift left.
                                        for(j=0; i<bytesread; i++){
                                           if( (uint8_t) dataBuffer[j++] == 0x0a ) break;                                           
                                        }
                                        i++;
                                        
                                        for(k=0; (i+k)<bytesread; k++){
                                           dataBuffer[k] = dataBuffer[j+k];
                                        }
				}
                                
				if(found == 0)
				{
					// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
					fclose(pFile);
					taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, ((tdWAKDStateConfigurationParameters*)db)->thisState);
					return 1;
				}
				else
				{
					fclose(pFile);
					return 0;
				}
                                

			}
			break;		
                  #if 0
			case dataID_AllStatesParametersSCC: 
			{
				// state params
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				tdWakdStates state = wakdStates_AllStopped;
				while(1) // will be terminated in the switch case at the end of the loop
				{
					/* 
					* just to make sure that we don't miss a state even if they're in the wrong order
					*/
					fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR); 
					while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
					{
						/*
							ptr is used to tell parseLineFloat where to start 
							and save the current pointer position for every parse.
						*/
						ptr = dataBuffer;
						/*
							double cast to remove warning caused by int32_t
						*/
						current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
						if(current_state == state)
						{
							switch(current_state)
							{
								case wakdStates_AllStopped:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateAllStopped));
								break;
								case wakdStates_Dialysis:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateDialysis));
								break;
								case wakdStates_Regen1:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen1));
								break;
								case wakdStates_Ultrafiltration:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateUltrafiltration));
								break;
								case wakdStates_Regen2:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen2));
								break;
								case wakdStates_Maintenance:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateMaintenance));
								break;
								case wakdStates_NoDialysate:
									readWholeStateParam(&ptr, end, &erroroccured, &(((tdAllStatesParametersSCC*)db)->parametersSCC_stateNoDialysate));
								break;
								default:
									fclose(pFile);
									taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
									return 1;
								break;
							}
							if(erroroccured)
							{
								fclose(pFile);
								#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "File %s contains empty or damaged state %i", filename, current_state);
								#endif	
								return 2; // means file should be okay but this state is empty or CRC wrong
							}		
							if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
							{
								fclose(pFile);
								taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
								return 1;
							}	
							found = 1; // leave while(fget(......)) if found and either continue to next state or exit
						}
						/* if we reach this, the current read in line isn't the one 
						 * we're searching for and need to continue with the next one
						*/
					}
					if(found == 0)
					{
						// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
						taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, state);
						return 1;
					}
					else if(found == 1)
					{
						found = 0;
					}
					/*
						if you add states, you need to add them here too because we can't iterate though enums in c
					*/
					switch(state)
					{
						case wakdStates_AllStopped:
							state = wakdStates_Dialysis;
						break;
						case wakdStates_Dialysis:
							state = wakdStates_Regen1;
						break;
						case wakdStates_Regen1:
							state = wakdStates_Ultrafiltration;
						break;
						case wakdStates_Ultrafiltration:
							state = wakdStates_Regen2;
						break;
						case wakdStates_Regen2:
							state = wakdStates_Maintenance;
						break;
						case wakdStates_Maintenance:
							state = wakdStates_NoDialysate;
						break;
						case wakdStates_NoDialysate:
							fclose(pFile);
							return 0; // exit because we found every state
						break;
						default:
							fclose(pFile);
							taskMessage("F", PREFIX, "Unknown state recognized while reading File %s", filename);
							return 1;
						break;
					}
				}
				taskMessage("I", PREFIX, "This is after a while(1) loop so something must be very wrong");
				fclose(pFile);
				return 2;
			}
			break;
			case dataID_StateParametersSCC:
			{
				// stateparam configuration
				// read in the first line, which is the header
				if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
				{
					taskMessage("F", PREFIX, "fgets() on File %s failed while trying to read the header.", filename);
					fclose(pFile);
					return 1;
				}
				#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Starting state in file %s is %i", filename, ((tdParametersSCC*)db)->thisState);
					#endif
				int found = 0;
				int32_t afterHeader = ftell(pFile);
				tdWakdStates current_state;
				fseek(pFile,afterHeader - ftell(pFile),SEEK_CUR);  // move after the header
				while(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) != NULL && !found)
				{
					/*
						ptr is used to tell parseLineFloat where to start 
						and save the current pointer position for every parse.
					*/
					ptr = dataBuffer;
					/*
						double cast to remove warning caused by int32_t
					*/
					current_state = (tdWakdStates)((int)parseLine(&ptr,end)); 
					#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Current state in file %s is %i", filename, current_state);
					#endif	
					if(current_state == ((tdParametersSCC*)db)->thisState)
					{
						#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Current state in file %s matches", filename, current_state);
						#endif	
						readWholeStateParam(&ptr, end, &erroroccured, (tdParametersSCC*)db);
						if(erroroccured)
						{
							fclose(pFile);
							#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "File %s contains empty or damaged state parameter %i", filename, current_state);
							#endif	
							return 2; // means file should be okay but this stateparam is empty or CRC wrong
						}		
						if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
						{
							fclose(pFile);
							taskMessage("F", PREFIX, "CRC error on File %s while trying to read state %i.", filename, current_state);
							return 1;
						}	
						found = 1; // leave while(fget(......)) if found and either continue to next state or exit
					}
					/* if we reach this, the current read in line isn't the one 
					 * we're searching for and need to continue with the next one
					*/
				}
				if(found == 0)
				{
					// while(fget(.....)) ran to an EOL, otherwise found would be 1 so no state found
					fclose(pFile);
					taskMessage("E", PREFIX, "File %s doesn't contain the needed state %i", filename, ((tdParametersSCC*)db)->thisState);
					return 1;
				}
				else
				{
					fclose(pFile);
					return 0;
				}
			}
			break;
               #endif
			default:
			{	
				//fclose(pFile);
                                file_fclose( pFile );
				defaultIdHandling(PREFIX, datatype);
				return 1;
			}
			break;
		}
	}




#endif

        return 0;
}

//! Wrapper function to extract the timestamp out of \a db.
/*!
  Because at runtime we don't know which struct is contained in \a db we need to cast it to use it in comparisons.
  \param datatype defines which struct is contained in \a db
  \param db will be casted to the datatype passed by \a datatype
  \return 1 on errors, 0 otherwise
  \sa unified_read(tdDataId datatype, void* db)
*/
uint32_t extract_timestamp(tdDataId datatype, void* db, tdUnixTimeType bufferedTime)
{
	switch(datatype)
	{
		case dataID_physiologicalData:
			return ((tdPhysiologicalData *)db)->TimeStamp;
		break;
		case dataID_physicalData:
			return ((tdPhysicalsensorData *)db)->TimeStamp;
		break;
		case dataID_actuatorData:{
			return ((tdActuatorData *)db)->TimeStamp;
		break;
		}
		case dataID_weightDataOK:
			return ((tdWeightMeasure *)db)->TimeStamp;
		break;
		case dataID_bpData:
			return ((tdBpData *)db)->TimeStamp;
		break;
		case dataID_EcgData:
			return ((tdEcgData *)db)->TimeStamp;
		break;
		case dataID_statusRTB:
			return ((tdDevicesStatus *)db)->TimeStamp;
		break;
		case dataID_configureStateDump:
			/*	this struct got no timestamp, but if we want to write
				it, we need one which is consistent for the whole duration
				of writing. Therefore we use this buffered time.
			*/
			return bufferedTime; 
		break;		
		default: 
			taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
			return SELECT_NOT_INT;
		break;
	}
	taskMessage("F", PREFIX, "If you can read this, something special(in a wrong way) has happened.");
	return SELECT_NOT_INT; // won't be reached, but hey nice to have
}

//! Parses and reads in the values provided by \a db. To avoid code reusage, you can use this function too to mark all values as not found by setting \a nothing_found to 1.
/*!
  \param datatype defines which struct is contained in \a db
  \param db will be casted to the datatype passed by \a datatype
  \param ptr Important: This is a pointer to a pointer because we want to use ptr afterwards by verifyCRC.
  \param end see \a parseLineFloat
  \param nothing_found will set all members of the struct to SELECT_NOT_FLT if this parameter was set.
  \return 1 on errors, 0 otherwise
*/
uint8_t ReadInMemberValues(tdDataId datatype, void* db, char** ptr, char* end, uint8_t nothing_found)
{
	switch(datatype)
	{
		case dataID_physiologicalData:
		{
			/*	write timestamp into the struct in which we found the value.
				If nothing was found, we need to skip this.
			*/
			if(nothing_found == 0)
			{
				((tdPhysiologicalData *)db)->TimeStamp = parseLine(ptr, end);
			}			
			((tdPhysiologicalData *)db)->currentState =  	  (tdWakdStates)(nothing_found == 1 ? SELECT_NOT_FLT : parseLine(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.Sodium = 					(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.msOffset_Sodium = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.Sodium = 					(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.msOffset_Sodium = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.Potassium = 				(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.msOffset_Potassium = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.Potassium = 				(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.msOffset_Potassium = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.Urea = 					(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.msOffset_Urea = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.Urea = 					(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.msOffset_Urea = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.pH = 						(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.msOffset_pH = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.pH = 						(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.msOffset_pH = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.Temperature = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataI.msOffset_Temperature = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.Temperature = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysiologicalData *)db)->ECPDataO.msOffset_Temperature = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			// ((tdPhysiologicalData *)db)->ECPDataI.Creatinine = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			// ((tdPhysiologicalData *)db)->ECPDataO.Creatinine = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			// ((tdPhysiologicalData *)db)->ECPDataI.Phosphate = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			// ((tdPhysiologicalData *)db)->ECPDataO.Phosphate = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_physicalData:
		{
			if(nothing_found == 0)
			{
				((tdPhysicalsensorData *)db)->TimeStamp = parseLine(ptr, end);
			}	
//			((tdPhysicalsensorData *)db)->statusECPI = 						(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
//			((tdPhysicalsensorData *)db)->statusECPO = 						(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureFCI.Pressure = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureFCI.msOffset_Pressure = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureFCO.Pressure = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureFCO.msOffset_Pressure = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureBCI.Pressure = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureBCI.msOffset_Pressure = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureBCO.Pressure = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->PressureBCO.msOffset_Pressure = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->TemperatureInOut.TemperatureInlet_Value = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->TemperatureInOut.TemperatureOutlet_Value = 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->TemperatureInOut.msOffset_Temperature = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
//			((tdPhysicalsensorData *)db)->TemperatureInOut.Status = 					(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
//			((tdPhysicalsensorData *)db)->TemperatureInOut.msOffset_Status = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->CSENS1Data.Cond_FCR = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->CSENS1Data.Cond_FCQ = 			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdPhysicalsensorData *)db)->CSENS1Data.Cond_PT1000 =			(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));		
			((tdPhysicalsensorData *)db)->CSENS1Data.msOffset_Cond = 		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
//			((tdPhysicalsensorData *)db)->CSENS1Data.Status = 				(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
//			((tdPhysicalsensorData *)db)->CSENS1Data.msOffset_Status =		(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_actuatorData:
		{
			if(nothing_found == 0)
			{
				((tdActuatorData *)db)->TimeStamp = parseLine(ptr, end);
								tdActuatorData *pActDta = NULL;
				pActDta = (tdActuatorData *) db;
			}
			((tdActuatorData *)db)->MultiSwitchBIData.SwitchONnOFF			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBIData.Position				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBIData.msOffset_Position		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBOData.SwitchONnOFF			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBOData.Position				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBOData.msOffset_Position		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBUData.SwitchONnOFF			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBUData.Position				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->MultiSwitchBUData.msOffset_Position		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.SwitchONnOFF					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.Direction					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.msOffset_Direction			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.Speed			 		 	 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.msOffset_Speed				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.FlowReference				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.msOffset_FlowReference		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.Flow			 				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.msOffset_Flow			 	 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.Current						 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->BLPumpData.msOffset_Current				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.SwitchONnOFF			 		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.Direction					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.msOffset_Direction			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.Speed						 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.msOffset_Speed				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.FlowReference				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.msOffset_FlowReference		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.Flow							 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.msOffset_Flow				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.Current			 			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->FLPumpData.msOffset_Current			 	 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.SwitchONnOFF			 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.Direction				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.msOffset_Direction 	 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.VoltageReference		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.msOffset_VoltageReference	= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.Voltage				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdActuatorData *)db)->PolarizationData.msOffset_Voltage		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_weightDataOK:
		{
			if(nothing_found == 0)
			{
				((tdWeightMeasure *)db)->TimeStamp = parseLine(ptr, end);
			}
			((tdWeightMeasure *)db)->Weight					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdWeightMeasure *)db)->BodyFat					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdWeightMeasure *)db)->WSBatteryLevel					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdWeightMeasure *)db)->WSStatus					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_bpData:
		{
			if(nothing_found == 0)
			{
				((tdBpData *)db)->TimeStamp = parseLine(ptr, end);
			}
			((tdBpData *)db)->systolic					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdBpData *)db)->diastolic					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdBpData *)db)->heartRate					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_EcgData:
		{
			if(nothing_found == 0)
			{
				((tdEcgData *)db)->TimeStamp = parseLine(ptr, end);
			}
			((tdEcgData *)db)->heartRate					 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdEcgData *)db)->respirationRate				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdEcgData *)db)->actuvityLevel				 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_statusRTB:
		{
			if(nothing_found == 0)
			{
				((tdDevicesStatus *)db)->TimeStamp = parseLine(ptr, end);
			}
			((tdDevicesStatus *)db)->statusECPI		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusECPO		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusBPSI		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end)); 
			((tdDevicesStatus *)db)->statusBPSO		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusFPSI		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusFPSO		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusBTS		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusDCS		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusBLPUMP		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusFLPUMP		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusMFSI		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusMFSO		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusMFSBL		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusPOLAR		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			((tdDevicesStatus *)db)->statusRTMCB		 = (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}
		break;
		case dataID_configureStateDump:
		{	
			if(nothing_found == 0)
			{
				//((tdMsgConfigureState *)db)->TimeStamp = parseLine(ptr, end);
			}
			tdMsgConfigureState *msgConfigureState = NULL;
			msgConfigureState = (tdMsgConfigureState *) db;
			msgConfigureState->stateToConfigure									= 	(nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));	
			msgConfigureState->actCtrl.BLPumpCtrl.direction				= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.BLPumpCtrl.flowReference			= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.FLPumpCtrl.direction				= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.FLPumpCtrl.flowReference			= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.PolarizationCtrl.direction			= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.PolarizationCtrl.voltageReference	= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			msgConfigureState->actCtrl.msOffset_ActCtrl					= (nothing_found == 1 ? SELECT_NOT_FLT : parseLineFloat(ptr, end));
			return 0;
		}		
		break;
		default: 
			taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
			return 1;
		break;
	}
}

//! Used to read in states
void readWholeState(char** ptr, char* end, uint8_t* erroroccured, tdWAKDStateConfigurationParameters* current_values)
{
	/* parse values. tmp is needed to check if parseLine returns an incorrect value,
		after converting to int16 it can't be checked anymore
	*/
	uint32_t tmp;
	*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
	current_values->defSpeedBp_mlPmin = tmp;
//	taskMessage("I", PREFIX, "BP preset %i",tmp );
	
	*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
	current_values->defSpeedFp_mlPmin = tmp; 
//	taskMessage("I", PREFIX, "FP preset %i", tmp );
	//taskMessage("I", PREFIX, "FP preset %i", current_values->defSpeedFp_mlPmin );
	
	*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
	current_values->defPol_V = tmp; 
//	taskMessage("I", PREFIX, "FP preset %i", tmp );
	
	*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
	current_values->direction = tmp; 
//	taskMessage("I", PREFIX, "defPol_V preset %i", tmp);
	
	*erroroccured |= ((tmp = parseLine(ptr,end)) != SELECT_NOT_INT ? 0 : 1); 
	current_values->duration_sec = tmp; 
//	taskMessage("I", PREFIX, "duration_sec preset %i", current_values->duration_sec );
}

//! Used to read in state params
void readWholeStateParam(char** ptr, char* end, uint8_t* erroroccured, tdParametersSCC* current_values)
{
	// parse values
	*erroroccured |= (current_values->NaAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->NaAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->NaTrdLT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->NaTrdHT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->NaTrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->NaTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KAAL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KAAH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KTrdT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->KTrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->KTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaAAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaTrdLT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->CaTrdHT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->CaTrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->UreaAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->UreaTrdT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->UreaTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CreaAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CreaAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CreaTrdT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CreaTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhosAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhosAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhosAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhosTrdLT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->PhosTrdHT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->PhosTrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhosTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3AAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3AbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3AbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3TrdLT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->HCO3TrdHT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->HCO3TrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3TrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhAAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhTrdLT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->PhTrdHT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1;
	*erroroccured |= (current_values->PhTrdLPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->PhTrdHPsT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPsysAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPsysAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPsysAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPdiaAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPdiaAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPdiaAAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->pumpFaccDevi = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->pumpBaccDevi = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->VertDeflecitonT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->WghtAbsL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->WghtAbsH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->WghtTrdT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FDpTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FDpTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPSoTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPSoTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPSiTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BPSiTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FPSoTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FPSoTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FPSTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->FPSTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BTSoTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BTSoTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BTSiTH = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BTSiTL = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->BatStatT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->f_K = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->SCAP_K = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->P_K = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->Fref_K = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->cap_K = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->f_Ph = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->SCAP_Ph = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->P_Ph = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->Fref_Ph = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->cap_Ph = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->A_dm2 = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CaAdr = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->CreaAdr = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->HCO3Adr = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtNa = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtK = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtCa = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtUr = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtCrea = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtPhos = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->wgtHCO3 = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
	*erroroccured |= (current_values->mspT = parseLineFloat(ptr,end)) != SELECT_NOT_FLT ? 0 : 1; 
}

