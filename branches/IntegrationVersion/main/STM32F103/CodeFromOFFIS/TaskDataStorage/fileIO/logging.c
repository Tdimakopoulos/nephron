/* -----------------------------------------------------------------------
 * Copyright (c) 2012     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
#include <stdio.h>
#include <string.h>
#include "csvparselib/csvparselib.h"
 
#ifdef LINUX
	#include "nephron.h"
	#include "global.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\..\\nephron.h"
	#include "..\\..\\..\\includes_nephron\\global.h"
#endif

#include "filenames.h"
#include "logging.h"

#define PREFIX "OFFIS FreeRTOS task DS - Logging library"

uint8_t writeToLog(tdUnixTimeType timestamp, char* message, ...)
 {
	va_list vargzeiger;
	va_start(vargzeiger, message);
	char filename[FILENAME_LENGTH];
	strncpy(filename,FILE_LOG,FILENAME_LENGTH);
	int error = 0;
	changeFileName(filename,timestamp); // file splitting into one file per hour
	FILE* pFile;
	pFile = fopen(filename, "ab");
	if(pFile == NULL)
	{
		taskMessage("F", PREFIX, "Error while opening file %s", filename);
		return 1;
	}
	error |= fprintf(pFile, "<%lu> ", timestamp) < 0 ? 1 : 0;
	error |= vfprintf(pFile, message, vargzeiger) < 0 ? 1 : 0;
	error |= fprintf(pFile, "\n") < 0 ? 1 : 0;
	va_end(vargzeiger);
	if(error < 0)
	{
		taskMessage("F", PREFIX, "Error while writing to file %s", filename);
		fclose (pFile);
		return 1;
	}
	fclose (pFile);
	return 0;
}

