 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
#ifndef UNIFIED_READ_H
#define UNIFIED_READ_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include "main.h"

#ifdef LINUX
	#include "interfaces.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\..\\includes_nephron\\interfaces.h"
#endif

// for testing purposes
//#ifdef __arm__ 
//	#include "csvparselib/pstdint.h"
//	#include "..\\..\\includes_nephron\\enums.h"
//	#include "..\\..\\typedefsNephron.h"
//#else
//	#include <stdint.h>
//	#include "..\enums.h"
//	#include "..\typedefsNephron.h"
//#endif

#include "unified_write.h"

uint8_t unified_read(tdDataId datatype, void* db);
uint32_t extract_timestamp(tdDataId datatype, void* db, tdUnixTimeType bufferedTime);
uint8_t ReadInMemberValues(tdDataId datatype, void* db, char** ptr, char* end, uint8_t nothing_found);
void readWholeState(char** ptr, char* end, uint8_t* erroroccured, tdWAKDStateConfigurationParameters* current_values);
void readWholeStateParam(char** ptr, char* end, uint8_t* erroroccured, tdParametersSCC* current_values);
void initMyFixedBuffer(char* dataBufferFixed);
#endif
