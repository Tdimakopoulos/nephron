/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "writeConfigureParametersToSD.h"
#include "informConfigurationChanged.h"

uint8_t writeConfigureParametersToSD(void *pMsg, xQueueHandle *queue, char *PREFIX)
{
	uint8_t err = 0;
	uint8_t changedPatientProfileData = 0;
	uint8_t changedWAKDStateConfigurationParameters = 0;
	uint8_t changedParametersSCC = 0;


	uint8_t  *pData 	= (uint8_t *) pMsg;
	uint16_t dataSize	= (((tdMsgOnly *)(pMsg))->header.msgSize)+(sizeof(tdMsgOnly)-6); // correct number bytes due to allignemnt and padding
//
//	printf("The message to process is ...\n");
//	int j = 0;
//	for (j=0;j<dataSize; j++)
//	{
//		printf("Byte[%d] is %x\n", j, *(pData+j));
//	}

	// Right after the header comes the byte defining the state the values count for.
	uint8_t *pWakdState = pData+sizeof(tdMsgOnly);	// repointed to directly after the header. This byte is the state definition

	// ############################################################################################################
	// ## Step 1: Process patient profile data
	// ############################################################################################################

	// Read values from the patient profile of SD card. Then change the relevant values to new values
	tdMsgPatientProfileData					*pMsgPatientProfileData					= NULL;
	tdMsgWAKDStateConfigurationParameters	*pMsgWAKDStateConfigurationParameters	= NULL;
	tdParametersSCC 						*pParametersSCC							= NULL;
	pMsgPatientProfileData					= (tdMsgPatientProfileData *)				pvPortMalloc(sizeof(tdMsgPatientProfileData));
	pMsgWAKDStateConfigurationParameters	= (tdMsgWAKDStateConfigurationParameters *)	pvPortMalloc(sizeof(tdMsgWAKDStateConfigurationParameters));
	pParametersSCC							= (tdParametersSCC *)						pvPortMalloc(sizeof(tdParametersSCC));
	if ( (pMsgPatientProfileData == NULL) || (pMsgWAKDStateConfigurationParameters == NULL) || (pParametersSCC == NULL))
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory for configuration of WAKD. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		// The function "unified_read" will take from the given data structure the state to read from
		// So we need to initialize that.
		pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState	= *pWakdState;
		pParametersSCC->thisState															= *pWakdState;

		if(		(unified_read(dataID_PatientProfile,	&(pMsgPatientProfileData->patientProfile)))									||
				(unified_read(dataID_configureState, 	&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters)))	||
				(unified_read(dataID_StateParametersSCC,pParametersSCC))															)
		{
			taskMessage("E", PREFIX, "Reading WAKD configuration from SD has failed!");
			errMsg(errmsg_ReadingSDcardFailed);
			err = 1;
		} else {
			#if defined DEBUG_DS || defined SEQ13
				taskMessage("I", PREFIX, "Reading patient profile of '%s' complete.", pMsgPatientProfileData->patientProfile.name);
				taskMessage("I", PREFIX, "Reading configuration parameters for WAKD state %s complete.", stateIdToString(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState));
				taskMessage("I", PREFIX, "Reading System Check configuration parameters for WAKD state %s complete.", stateIdToString(pParametersSCC->thisState));
			#endif

			// Run through the received data to search for the name value pairs. Start right after state information.
			int i = sizeof(tdMsgOnly)+1;	// One byte extra which is the state information after header.
			while (i<dataSize)
			{	// pData+i points to a parameter name.
				tdParameter parameter = (tdParameter) (*(pData+i));
				switch (parameter)
				{

					// ############################################################################################################
					// ## Step 1: Process patient profile data
					// ############################################################################################################

					case parameter_name: {
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						if (i<dataSize)
						{	// Still within received data. Proceed
							if (sizeof(pMsgPatientProfileData->patientProfile.name) < *(pData+i))
							{	// String is too large to fit!
								taskMessage("E", PREFIX, "The length of 'parameter_name' exceeds the maximum of %d bytes!", sizeof(pMsgPatientProfileData->patientProfile.name));
								err = 1; i = dataSize; // terminate the while. We cannot continue.
							} else {
								#if defined SEQ13
									taskMessage("I", PREFIX, "New parameter_name, number of characters: %d", *(pData+i));
								#endif
								int strEnd = i+*(pData+i)+1;
								i++;
								// Make perfectly sure that we are always staying within the data package we received
								// So reading the string characters should be within the dataSize from beginning to its end.
								if ((i<dataSize) && strEnd<=dataSize)
								{	// Still within received data. Proceed
									int bytecount = 0;
									while (i<strEnd)
									{
										*((uint8_t *)(&(pMsgPatientProfileData->patientProfile.name))+bytecount) = *(pData+i);
										i++;
										bytecount++;
									}
									// Terminate the string with the end character "\0"
									*((uint8_t *)(&(pMsgPatientProfileData->patientProfile.name))+bytecount) = 0x0;
									#if defined SEQ13
										taskMessage("I", PREFIX, "Parameter '%s' is now %s.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.name);
									#endif
								} else {
									// Processing the parameters' name value pairs proceeded behind the limit of received data
									// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
									err = 1; i = dataSize; // terminate the while. We cannot continue.
									taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
								}
							}
						} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_gender:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.gender))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.gender);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.gender)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %c.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.gender);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_measureTimeHours:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.measureTimeHours))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.measureTimeHours);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.measureTimeHours)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeHours);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_measureTimeMinutes:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.measureTimeMinutes))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.measureTimeMinutes);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.measureTimeMinutes)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeMinutes);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wghtCtlTarget:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.wghtCtlTarget))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlTarget);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlTarget);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wghtCtlLastMeasurement:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wghtCtlDefRemPerDay:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_kCtlTarget:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.kCtlTarget))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlTarget);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlTarget);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_kCtlLastMeasurement:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_kCtlDefRemPerDay:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_urCtlTarget:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.urCtlTarget))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlTarget);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlTarget);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_urCtlLastMeasurement:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_urCtlDefRemPerDay:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_minDialPlasFlow:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.minDialPlasFlow))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.minDialPlasFlow);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialPlasFlow)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialPlasFlow);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_maxDialPlasFlow:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.maxDialPlasFlow))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.maxDialPlasFlow);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialPlasFlow)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialPlasFlow);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_minDialVoltage:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.minDialVoltage))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.minDialVoltage);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialVoltage)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialVoltage);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_maxDialVoltage:{
						// patient profile data was touched. Remember this and inform WAKD system about this later.
						changedPatientProfileData = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgPatientProfileData->patientProfile.maxDialVoltage))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.maxDialVoltage);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialVoltage)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialVoltage);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}

					// ############################################################################################################
					// ## Step 2: Process WAKDStateConfigurationParameters
					// ############################################################################################################

					case parameter_defSpeedBp_mlPmin:{
						// WAKD state configuration was touched. Remember this and inform WAKD system about this later.
						changedWAKDStateConfigurationParameters = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_defSpeedFp_mlPmin:{
						// WAKD state configuration was touched. Remember this and inform WAKD system about this later.
						changedWAKDStateConfigurationParameters = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_defPol_V:{
						// WAKD state configuration was touched. Remember this and inform WAKD system about this later.
						changedWAKDStateConfigurationParameters = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_direction:{
						// WAKD state configuration was touched. Remember this and inform WAKD system about this later.
						changedWAKDStateConfigurationParameters = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_duration_sec:{
						// WAKD state configuration was touched. Remember this and inform WAKD system about this later.
						changedWAKDStateConfigurationParameters = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}

					// ############################################################################################################
					// ## Step 3:
					// ############################################################################################################

					case parameter_NaAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_NaAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_NaTrdLT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaTrdLT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaTrdLT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaTrdLT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaTrdLT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_NaTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaTrdHT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaTrdHT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaTrdHT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaTrdHT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_NaTrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaTrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaTrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaTrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaTrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_NaTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->NaTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->NaTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->NaTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->NaTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KAAL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KAAL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KAAL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KAAL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KAAL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KAAH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KAAH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KAAH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KAAH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KAAH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KTrdT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KTrdT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KTrdT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KTrdT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KTrdT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KTrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KTrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KTrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KTrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KTrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_KTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->KTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->KTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->KTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->KTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaAAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaAAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaAAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaAAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaAAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaTrdLT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaTrdLT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaTrdLT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaTrdLT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaTrdLT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaTrdHT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaTrdHT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaTrdHT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaTrdHT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaTrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaTrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaTrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaTrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaTrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_UreaAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->UreaAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->UreaAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->UreaAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->UreaAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_UreaTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->UreaTrdT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->UreaTrdT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->UreaTrdT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->UreaTrdT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_UreaTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->UreaTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->UreaTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->UreaTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->UreaTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CreaAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CreaAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CreaAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CreaAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CreaAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CreaAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CreaAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CreaAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CreaAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CreaAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CreaTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CreaTrdT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CreaTrdT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CreaTrdT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CreaTrdT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CreaTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CreaTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CreaTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CreaTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CreaTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosTrdLT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosTrdLT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosTrdLT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosTrdLT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosTrdLT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosTrdHT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosTrdHT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosTrdHT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosTrdHT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosTrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosTrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosTrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosTrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosTrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhosTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhosTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhosTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhosTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhosTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3AAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3AAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3AAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3AAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3AAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3AbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3AbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3AbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3AbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3AbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3AbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3AbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3AbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3AbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3AbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3TrdLT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3TrdLT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3TrdLT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3TrdLT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3TrdLT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3TrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3TrdHT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3TrdHT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3TrdHT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3TrdHT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3TrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3TrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3TrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3TrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3TrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3TrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3TrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3TrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3TrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3TrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhAAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhAAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhAAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhAAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhAAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhTrdLT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhTrdLT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhTrdLT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhTrdLT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhTrdLT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhTrdHT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhTrdHT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhTrdHT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhTrdHT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhTrdHT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhTrdLPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhTrdLPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhTrdLPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhTrdLPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhTrdLPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_PhTrdHPsT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->PhTrdHPsT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->PhTrdHPsT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->PhTrdHPsT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->PhTrdHPsT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPsysAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPsysAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPsysAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPsysAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPsysAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPsysAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPsysAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPsysAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPsysAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPsysAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPsysAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPsysAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPsysAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPsysAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPsysAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPdiaAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPdiaAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPdiaAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPdiaAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPdiaAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPdiaAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPdiaAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPdiaAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPdiaAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPdiaAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPdiaAAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPdiaAAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPdiaAAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPdiaAAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPdiaAAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_pumpFaccDevi:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->pumpFaccDevi))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->pumpFaccDevi);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->pumpFaccDevi)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->pumpFaccDevi);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_pumpBaccDevi:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->pumpBaccDevi))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->pumpBaccDevi);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->pumpBaccDevi)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->pumpBaccDevi);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_VertDeflecitonT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->VertDeflecitonT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->VertDeflecitonT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->VertDeflecitonT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->VertDeflecitonT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_WghtAbsL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->WghtAbsL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->WghtAbsL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->WghtAbsL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->WghtAbsL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_WghtAbsH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->WghtAbsH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->WghtAbsH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->WghtAbsH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->WghtAbsH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_WghtTrdT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->WghtTrdT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->WghtTrdT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->WghtTrdT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->WghtTrdT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FDpTL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FDpTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FDpTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FDpTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FDpTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FDpTH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FDpTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FDpTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FDpTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FDpTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPSoTH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPSoTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPSoTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPSoTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPSoTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPSoTL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPSoTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPSoTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPSoTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPSoTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPSiTH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPSiTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPSiTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPSiTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPSiTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BPSiTL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BPSiTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BPSiTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BPSiTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BPSiTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FPS1TH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FPSoTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FPSoTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FPSoTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FPSoTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FPS1TL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FPSoTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FPSoTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FPSoTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FPSoTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FPS2TH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FPSTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FPSTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FPSTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FPSTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_FPS2TL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->FPSTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->FPSTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->FPSTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->FPSTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BTSoTH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BTSoTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BTSoTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BTSoTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BTSoTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BTSoTL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BTSoTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BTSoTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BTSoTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BTSoTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BTSiTH:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BTSiTH))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BTSiTH);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BTSiTH)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BTSiTH);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BTSiTL:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BTSiTL))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BTSiTL);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BTSiTL)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BTSiTL);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_BatStatT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->BatStatT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->BatStatT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->BatStatT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->BatStatT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_f_K:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->f_K))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->f_K);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->f_K)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->f_K);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_SCAP_K:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->SCAP_K))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->SCAP_K);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->SCAP_K)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->SCAP_K);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_P_K:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->P_K))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->P_K);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->P_K)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->P_K);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_Fref_K:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->Fref_K))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->Fref_K);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->Fref_K)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->Fref_K);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_cap_K:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->cap_K))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->cap_K);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->cap_K)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->cap_K);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_f_Ph:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->f_Ph))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->f_Ph);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->f_Ph)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->f_Ph);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_SCAP_Ph:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->SCAP_Ph))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->SCAP_Ph);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->SCAP_Ph)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->SCAP_Ph);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_P_Ph:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->P_Ph))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->P_Ph);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->P_Ph)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->P_Ph);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_Fref_Ph:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->Fref_Ph))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->Fref_Ph);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->Fref_Ph)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->Fref_Ph);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_cap_Ph:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->cap_Ph))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->cap_Ph);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->cap_Ph)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->cap_Ph);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_A_dm2:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->A_dm2))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->A_dm2);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->A_dm2)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->A_dm2);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CaAdr:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CaAdr))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CaAdr);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CaAdr)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CaAdr);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_CreaAdr:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->CreaAdr))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->CreaAdr);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->CreaAdr)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->CreaAdr);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_HCO3Adr:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->HCO3Adr))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->HCO3Adr);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->HCO3Adr)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->HCO3Adr);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtNa:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtNa))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtNa);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtNa)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtNa);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtK:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtK))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtK);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtK)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtK);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtCa:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtCa))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtCa);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtCa)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtCa);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtUr:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtUr))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtUr);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtUr)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtUr);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtCrea:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtCrea))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtCrea);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtCrea)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtCrea);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtPhos:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtPhos))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtPhos);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtPhos)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtPhos);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_wgtHCO3:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->wgtHCO3))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->wgtHCO3);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->wgtHCO3)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->wgtHCO3);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					case parameter_mspT:{
						//  SCC parameter was touched. Remember this and inform WAKD system about this later.
						changedParametersSCC = 1;
						i++;
						// Check if data received is sufficient to completely fill in new values
						if ( (i<dataSize) && ((i+sizeof(pParametersSCC->mspT))<=dataSize) )
						{	// Still within received data. Proceed:
							uint8_t j = 0;
							for (j = 0; j<sizeof(pParametersSCC->mspT);j++)
							{	// copy byte by byte
								*(((uint8_t *)(&(pParametersSCC->mspT)))+j) = *(pData+i);
								i++;
							}
							#if defined SEQ13
								taskMessage("I", PREFIX, "Parameter '%s' is now %d.", parameterToString(parameter), pParametersSCC->mspT);
							#endif

					} else {
							// Processing the parameters' name value pairs proceeded behind the limit of received data
							// This is an error, likely because the phone assumed incorrect data types are or wrong message size.
							err = 1; i = dataSize; // terminate the while. We cannot continue.
							taskMessage("E", PREFIX, "Structure of parameters' name-value-pairs is corrupt!");
						}
						break;
					}
					default: {
						// Unknown parameter. Cannot continue.
						err = 1; i = dataSize; // terminate the while. We cannot continue.
						taskMessage("E", PREFIX, "Unknown name-value-pair => structure of parameters' name-value-pairs is corrupt!");
						break;
					}
				}
			} // end while processing through the list of new parameters' name value pairs.
			if (!err)
			{	// All new values have been stored from message into patient profile.
				// now write to SD card.
				if (changedPatientProfileData)
				{	// Data of patient profile was touched. Write and inform system about this.
					#if defined SEQ13
						taskMessage("I", PREFIX, "Storing new patient data on SD card.");
					#endif
					unified_write(dataID_PatientProfile, 	&(pMsgPatientProfileData->patientProfile));
					err = err + informConfigurationChanged(dataID_PatientProfile, queue, PREFIX);
				}
				if (changedWAKDStateConfigurationParameters)
				{	// WAKD state configuration was touched. Write and inform system about this.
					#if defined SEQ13
						taskMessage("I", PREFIX, "Storing new WAKD configuration data on SD card.");
					#endif
					unified_write(dataID_configureState, 	&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters));
					err = err + informConfigurationChanged(dataID_configureState, queue, PREFIX);
				}
				if (changedParametersSCC)
				{	// SCC parameter was touched. Write and inform system about this.
					#if defined SEQ13
						taskMessage("I", PREFIX, "Storing new SCC parameter data on SD card.");
					#endif
					unified_write(dataID_StateParametersSCC,pParametersSCC);
					err = err + informConfigurationChanged(dataID_StateParametersSCC, queue, PREFIX);
				}
			}
		}
		sFree((void *)(&pMsgPatientProfileData));
		sFree((void *)(&pMsgWAKDStateConfigurationParameters));
		sFree((void *)(&pParametersSCC));
	}
	return(err);
}

