/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef FCHELPERFUNC_H_
#define FCHELPERFUNC_H_

#ifdef LINUX
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
#endif

#ifdef VP_SIMULATION
	// Virtual Platform code here! Must not end up in final version!
	// The following global variable is been used by a dummy "getData" fuction.
	// Usually data would come in via a serial interface and getData pulls it from
	// the serial interface buffer.
	// For simulation with Simulink there is not serial interface and the dummy getData
	// pulls the values from simulink it self. To know which data to pull, this global variable is used.
	extern tdWakdStates				dummy_CurrentState;
	extern tdWakdOperationalState	dummy_CurrentOpState;
#endif

uint8_t changeIntoStateRequest(tdWakdStates newState, tdWakdOperationalState newOpState, tdMsgCurrentWAKDstateIs *pCurrentState, xQueueHandle *pQueueHandle);

// void sendCurrentState(tdMsgCurrentWAKDstateIs *pCurrentState, tdQtoken *pQtoken, xQueueHandle *pQueueHandle);

uint8_t sendWakdStateConfig(tdWakdStates stateToConfig, tdWAKDStateConfigurationParameters *pWAKDStateConfigure, xQueueHandle *pQueueHandle);

uint8_t sendACK(tdMsgOnly * pMsgToNack, xQueueHandle *pQueueHandle);

uint8_t sendNACK(tdMsgOnly * pMsgToNack, xQueueHandle *pQueueHandle);

uint8_t forceChangeState(tdWakdStates newState, xQueueHandle *pQueueHandleDISPin);

uint8_t interpretSCCmessage (tdInfoMsg sccMsg, xQueueHandle *pQueueHandleDISPin);

#endif /* FCHELPERFUNC_H_ */
