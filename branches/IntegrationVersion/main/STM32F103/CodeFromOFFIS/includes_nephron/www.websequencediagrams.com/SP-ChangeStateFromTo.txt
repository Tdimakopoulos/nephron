note left of SP
	SP knows WAKD state
	(most of the time)
	Sends message with FROM-state
	to reach the requested TO-STATE
end note

SP -> CB: dataID_changeWAKDStateFromTo

note over CB
	looks into header for
	... size information
end note
CB -> MB: bytewise forward

alt fromWakdState == state of MB/RTB
	
	MB -> CB: dataID_ack
	note over CB
		looks into header for
		... id information
		... size information
	end note
	CB -> SP: bytewise forward
	
	MB -> RTB: putData();
	note over RTB MB
		dataID_changeWAKDStateFromTo
	end note
	RTB -> MB: decodeData();
	note over MB
		current state now
		in static variable
	end note
	
	MB -> CB: dataID_currentWAKDstateIs
	note over CB
		looks into header for
		... id information
		... size information
	end note
	CB -> SP: bytewise forward
	
	SP -> CB: dataID_ack
	note over CB
		looks into header for
		... size information
	end note
	CB -> MB: bytewise forward
	
	
else fromWakdState != state of MB
	note over MB
		MB discards the command
	end note
	MB -> CB: dataID_nack
	note over CB
		looks into header for
		... id information
		... size information
	end note
	CB -> SP: bytewise forward
end