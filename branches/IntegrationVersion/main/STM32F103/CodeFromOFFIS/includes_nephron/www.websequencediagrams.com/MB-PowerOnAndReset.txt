participant SP
participant CB

alt Warm Start (SW Reset) from WAKD LCD
	note over MB
		User operastes buttons on WAKD
		to select "reset" in WAKD
		menu tree.
	end note
	note over MB
		WAKD LCD shows message:
		"Are you sure to turn off?"
		OK, "Really?" OK, "Really really?"
		OK
	end note
	MB->CB: dataID_reset
	CB->MB: dataID_ack
	MB->RTB: dataID_reset
	note over RTB
		RTB will automatically move
		into state "AllStopped"
	end note
	MB->PMB1: dataID_reset
	MB->PMB2: dataID_reset
	note over MB
		FreeRTOS tasks terminate themselves.
		SCC terminates itself last after which
		watchdog timer will run out causing a
		HW reset.
	end note	
	
else Cold Start (Power On/HW Reset)
	note over MB
		Power is established and
		MB gets reset from HW.
	end note
end 

note over MB
	SW	"main.c" starts and boots
	FreeRTOS. All Nephron+ application
	tasks are started and communication
	queues are installed.
end note

note over MB
	MB configures RTB.
end note

MB -> RTB: dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_AllStopped
end note
MB -> RTB: dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_Maintenance
end note
MB -> RTB: dataID_configureState
note over RTB  MB
	SEQ9
	wakdStates_NoDialysate
end note
MB -> RTB: dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_Dialysis
end note
MB -> RTB: dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_Regen1
end note
MB -> RTB:  dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_Ultrafiltration
end note
MB -> RTB:  dataID_configureState
note over RTB MB
	SEQ9
	wakdStates_Regen2
end note

note over MB
	Have RTB in state
	"AllStopped".
end note

MB -> RTB: dataID_statusRequest
note over RTB MB
	SEQ10
end note
MB -> RTB:  dataID_changeWAKDStateFromTo
note over RTB MB
	SEQ11
	wakdStates_AllStopped
	wakdStatesOS_Automatic
end note
MB -> RTB: dataID_statusRequest
note over RTB MB
	SEQ10
end note

MB -> CB: dataID_currentWAKDstateIs
CB -> SP: bytewise forward
SP -> CB: dataID_ack
CB -> MB: bytewise forward

note over MB
	READY
end note
