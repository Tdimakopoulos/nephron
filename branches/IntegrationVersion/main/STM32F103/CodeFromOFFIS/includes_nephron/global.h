// -----------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   global.h
//! \brief  General declaration
//!
//! Declaration of useful macros, constants
//! Definition of user types
//!
//!  
//! \author  Kaeser Ch.
//! \date    09.06.2009
//! \version 1.0
//!  
//! \author  Porchet J-A
//! \date    03.08.2009
//! \version 1.1
//! \remarks Modification of the typedef
// -----------------------------------------------------------------------------------

#ifndef GLOBAL_H
#define GLOBAL_H

#include <limits.h>

// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported definitions
// -----------------------------------------------------------------------------------
#define MASK_BIT0       (0x0001)
#define MASK_BIT1       (0x0002)
#define MASK_BIT2       (0x0004)
#define MASK_BIT3       (0x0008)
#define MASK_BIT4       (0x0010)
#define MASK_BIT5       (0x0020)
#define MASK_BIT6       (0x0040)
#define MASK_BIT7       (0x0080)
#define MASK_BIT8       (0x0100)
#define MASK_BIT9       (0x0200)
#define MASK_BITA       (0x0400)
#define MASK_BITB       (0x0800)
#define MASK_BITC       (0x1000)
#define MASK_BITD       (0x2000)
#define MASK_BITE       (0x4000)
#define MASK_BITF       (0x8000)

#define MASK_BIT10      MASK_BITA
#define MASK_BIT11      MASK_BITB
#define MASK_BIT12      MASK_BITC
#define MASK_BIT13      MASK_BITD
#define MASK_BIT14      MASK_BITE
#define MASK_BIT15      MASK_BITF

#define true   (0 == 0)
#define false  (0 == 1)

#ifndef NULL
#define NULL  ((void *) 0)
#endif

#define REG32 (volatile unsigned int*)

// -----------------------------------------------------------------------------------
// Exported macros
// -----------------------------------------------------------------------------------
#define  MAX(x, y)  	   ((x) > (y) ? (x) : (y))
//#define  MIN(x, y)   	   ((x) < (y) ? (x) : (y))
#define  ABS(x)			   (((x) < 0) ? -(x) : (x))
#define  NB_ELEM(Array)	   (sizeof (Array) / sizeof (*(Array)))
#define  LOBYTE(x)  	   ((uint8_t) ((x) & 0xFF))
#define  HIBYTE(x)  	   ((uint8_t) (((x) & 0xFF00) >> 8))

#define  SETBIT(a,b)    ((a) |= (1 << (b)))
#define  CLRBIT(a,b)    ((a) &= ~(1 << (b)))
#define  TSTBIT(a,b)    ((a) & (1 << (b)))

#define  SETMASK(data,set)          ((data) |= (set))
#define  CLRMASK(data,set)          ((data) &= ~(set))
#define  CLRSETMASK(data,clr,set)   ((data) = ((data) & ~(clr)) | (set))

// -----------------------------------------------------------------------------------
// Exported type
// -----------------------------------------------------------------------------------
typedef  unsigned char       boolean_t; //!< boolean quantity
// defined below: typedef  unsigned char       uint8_t;   //!< Unsigned 8 bit quantity
// defined below: typedef  signed   char       int8_t;	//!< Signed 8 bit quantity
// defined below: typedef  unsigned short      uint16_t;  //!< Unsigned 16 bit quantity
// defined below: typedef  signed   short      int16_t;   //!< Signed 16 bit quantity
// defined below: typedef  unsigned long       uint32_t;  //!< Unsigned 32 bit quantity
// defined below: typedef  signed   long       int32_t;   //!< Signed 32 bit quantity
// defined below: typedef  signed long long    int64_t;   //!< Signed 64 bit quantity
// defined below: typedef  unsigned long long  uint64_t;  //!< Unsigned 64 bit quantity

#ifndef UINT8_MAX
# define UINT8_MAX 0xff
#endif
#ifndef uint8_t
# if (UCHAR_MAX == UINT8_MAX) || defined (S_SPLINT_S)
    typedef unsigned char uint8_t;
#   define UINT8_C(v) ((uint8_t) v)
# else
#   error "Platform not supported"
# endif
#endif

#ifndef INT8_MAX
# define INT8_MAX 0x7f
#endif
#ifndef INT8_MIN
# define INT8_MIN INT8_C(0x80)
#endif
#ifndef int8_t
# if (SCHAR_MAX == INT8_MAX) || defined (S_SPLINT_S)
    typedef signed char int8_t;
#   define INT8_C(v) ((int8_t) v)
# else
#   error "Platform not supported"
# endif
#endif

#ifndef UINT16_MAX
# define UINT16_MAX 0xffff
#endif
#ifndef uint16_t
#if (UINT_MAX == UINT16_MAX) || defined (S_SPLINT_S)
  typedef unsigned int uint16_t;
# ifndef PRINTF_INT16_MODIFIER
#  define PRINTF_INT16_MODIFIER ""
# endif
# define UINT16_C(v) ((uint16_t) (v))
#elif (USHRT_MAX == UINT16_MAX)
  typedef unsigned short uint16_t;
# define UINT16_C(v) ((uint16_t) (v))
# ifndef PRINTF_INT16_MODIFIER
#  define PRINTF_INT16_MODIFIER "h"
# endif
#else
#error "Platform not supported"
#endif
#endif

#ifndef INT16_MAX
# define INT16_MAX 0x7fff
#endif
#ifndef INT16_MIN
# define INT16_MIN INT16_C(0x8000)
#endif
#ifndef int16_t
#if (INT_MAX == INT16_MAX) || defined (S_SPLINT_S)
  typedef signed int int16_t;
# define INT16_C(v) ((int16_t) (v))
# ifndef PRINTF_INT16_MODIFIER
#  define PRINTF_INT16_MODIFIER ""
# endif
#elif (SHRT_MAX == INT16_MAX)
  typedef signed short int16_t;
# define INT16_C(v) ((int16_t) (v))
# ifndef PRINTF_INT16_MODIFIER
#  define PRINTF_INT16_MODIFIER "h"
# endif
#else
#error "Platform not supported"
#endif
#endif

#ifndef UINT32_MAX
# define UINT32_MAX (0xffffffffUL)
#endif
#ifndef uint32_t
#if (ULONG_MAX == UINT32_MAX) || defined (S_SPLINT_S)
  typedef unsigned long uint32_t;
# define UINT32_C(v) v ## UL
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER "l"
# endif
#elif (UINT_MAX == UINT32_MAX)
  typedef unsigned int uint32_t;
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER ""
# endif
# define UINT32_C(v) v ## U
#elif (USHRT_MAX == UINT32_MAX)
  typedef unsigned short uint32_t;
# define UINT32_C(v) ((unsigned short) (v))
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER ""
# endif
#else
#error "Platform not supported"
#endif
#endif

#ifndef INT32_MAX
# define INT32_MAX (0x7fffffffL)
#endif
#ifndef INT32_MIN
# define INT32_MIN INT32_C(0x80000000)
#endif
#ifndef int32_t
#if (LONG_MAX == INT32_MAX) || defined (S_SPLINT_S)
  typedef signed long int32_t;
# define INT32_C(v) v ## L
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER "l"
# endif
#elif (INT_MAX == INT32_MAX)
  typedef signed int int32_t;
# define INT32_C(v) v
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER ""
# endif
#elif (SHRT_MAX == INT32_MAX)
  typedef signed short int32_t;
# define INT32_C(v) ((short) (v))
# ifndef PRINTF_INT32_MODIFIER
#  define PRINTF_INT32_MODIFIER ""
# endif
#else
#error "Platform not supported"
#endif
#endif

#undef stdint_int64_defined
#if (defined(__STDC__) && defined(__STDC_VERSION__)) || defined (S_SPLINT_S)
# if (__STDC__ && __STDC_VERSION__ >= 199901L) || defined (S_SPLINT_S)
#  define stdint_int64_defined
   typedef long long int64_t;
   typedef unsigned long long uint64_t;
#  define UINT64_C(v) v ## ULL
#  define  INT64_C(v) v ## LL
#  ifndef PRINTF_INT64_MODIFIER
#   define PRINTF_INT64_MODIFIER "ll"
#  endif
# endif
#endif

#if !defined (stdint_int64_defined)
# if defined(__GNUC__)
#  define stdint_int64_defined
   __extension__ typedef long long int64_t;
   __extension__ typedef unsigned long long uint64_t;
#  define UINT64_C(v) v ## ULL
#  define  INT64_C(v) v ## LL
#  ifndef PRINTF_INT64_MODIFIER
#   define PRINTF_INT64_MODIFIER "ll"
#  endif
# elif defined(__MWERKS__) || defined (__SUNPRO_C) || defined (__SUNPRO_CC) || defined (__APPLE_CC__) || defined (_LONG_LONG) || defined (_CRAYC) || defined (S_SPLINT_S)
#  define stdint_int64_defined
   typedef long long int64_t;
   typedef unsigned long long uint64_t;
#  define UINT64_C(v) v ## ULL
#  define  INT64_C(v) v ## LL
#  ifndef PRINTF_INT64_MODIFIER
#   define PRINTF_INT64_MODIFIER "ll"
#  endif
# elif (defined(__WATCOMC__) && defined(__WATCOM_INT64__)) || (defined(_MSC_VER) && _INTEGRAL_MAX_BITS >= 64) || (defined (__BORLANDC__) && __BORLANDC__ > 0x460) || defined (__alpha) || defined (__DECC)
#  define stdint_int64_defined
   typedef __int64 int64_t;
   typedef unsigned __int64 uint64_t;
#  define UINT64_C(v) v ## UI64
#  define  INT64_C(v) v ## I64
#  ifndef PRINTF_INT64_MODIFIER
#   define PRINTF_INT64_MODIFIER "I64"
#  endif
# endif
#endif

#if !defined (LONG_LONG_MAX) && defined (INT64_C)
# define LONG_LONG_MAX INT64_C (9223372036854775807)
#endif
#ifndef ULONG_LONG_MAX
# define ULONG_LONG_MAX UINT64_C (18446744073709551615)
#endif

#if !defined (INT64_MAX) && defined (INT64_C)
# define INT64_MAX INT64_C (9223372036854775807)
#endif
#if !defined (INT64_MIN) && defined (INT64_C)
# define INT64_MIN INT64_C (-9223372036854775808)
#endif
#if !defined (UINT64_MAX) && defined (INT64_C)
# define UINT64_MAX UINT64_C (18446744073709551615)
#endif

// -----------------------------------------------------------------------------------
// Exported structures
// -----------------------------------------------------------------------------------
//! Bits structure
typedef struct
{
   unsigned b0   : 1;
   unsigned b1   : 1;
   unsigned b2   : 1;
   unsigned b3   : 1;
   unsigned b4   : 1;
   unsigned b5   : 1;
   unsigned b6   : 1;
   unsigned b7   : 1;
} Bit_t;

//! Byte structure
typedef  union
{
  Bit_t     bits;   //!< Bit field access
  uint8_t   byte;   //!< Byte access
} BitUnionByte_t;   

// -----------------------------------------------------------------------------------
// Exported constants
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Exported inline function
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Prototypes of exported functions
// -----------------------------------------------------------------------------------


#endif  // GLOBAL_H //



