/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "powermanager.h"

void tskPMBPA( void *pvParameters )
{
	char *pPREFIX = NULL;
	char *pPREFIX0 = "OFFIS FreeRTOS task PMBPA0";
	char *pPREFIX1 = "OFFIS FreeRTOS task PMBPA1";
	xQueueHandle	*qhPMBPAin	= NULL;
	xTaskHandle		*pthPMBPA	= NULL;
	xTaskHandle		*thPMBPA0	= &((tdAllHandlesPMB *) pvParameters)->pAllHandles->allTaskHandles.handlePMBPA0;
	xTaskHandle		*thPMBPA1	= &((tdAllHandlesPMB *) pvParameters)->pAllHandles->allTaskHandles.handlePMBPA1;
	if (((tdAllHandlesPMB *) pvParameters)->instance == 0)
	{	// This is the task handling Power Board 0
		pPREFIX		= pPREFIX0;
		qhPMBPAin	= ((tdAllHandlesPMB *) pvParameters)->pAllHandles->allQueueHandles.qhPMBPA0in;
		pthPMBPA	= thPMBPA0;
	} else { // This is the task handling Power Board 1
		pPREFIX		= pPREFIX1;
		qhPMBPAin	= ((tdAllHandlesPMB *) pvParameters)->pAllHandles->allQueueHandles.qhPMBPA1in;
		pthPMBPA	= thPMBPA1;
	}

	taskMessage("I", pPREFIX, "Starting Power Management Protocol Abstraction task!");

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", pPREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

 	tdQtoken Qtoken;
 	Qtoken.command	= command_DefaultError;
 	Qtoken.pData	= NULL;

	for( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every CBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		//heartBeatPMBPA++;
	// WARNING: Currently the PMBPA tasks are not being monitored by the watch dog task.
	// So Should thesen tasks somehow crash this would remain unnoticed in the current implementation.
	// You should enable the "heartBeatPMBPA" and have it chacked by taskWD.

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", pPREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", pPREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		if ( xQueueReceive( qhPMBPAin, &Qtoken, PMBPA_PERIOD) == pdPASS ) {

                        taskMessage("I", pPREFIX, "Message in queue Power Management Protocol Abstraction task!");

			switch (Qtoken.command)
			{
				case command_UseAtachedMsgHeader: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_shutdown): // fall through, continue with reset.
						case (dataID_reset):{
						#if defined DEBUG_CBPA || defined SEQ0
							taskMessage("I", pPREFIX, "Shutdown of task.");
						#endif
						// Do here whatever needs to be done for shutting down.
						// ((tdAllHandles *) pvParameters)->allTaskHandles.handleCBPA = NULL;
						*pthPMBPA = NULL;
						vTaskDelete(NULL);	// Shut down and wait forever.
						vTaskSuspend(NULL); // Never run again (until after reset)
						}
						default:{
							defaultIdHandling(pPREFIX, dataId);
							break;
						}
					}
					break;
				}
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(pPREFIX, Qtoken.command);
					break;
				}
			}
		}
	}
}

