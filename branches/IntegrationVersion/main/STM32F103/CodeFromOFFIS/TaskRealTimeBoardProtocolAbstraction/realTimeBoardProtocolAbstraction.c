/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "main.h"
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\main\\main.h"
	#include "..\\nephron.h"
#endif
#include "realTimeBoardProtocolAbstraction.h"

#define PREFIX "OFFIS FreeRTOS task RTBPA"

#ifdef VP_SIMULATION
	// Dummy functionality for functions getData and putData. The dummies operate with the
	// simlink interface instead of a real serial interface buffer!
	#include "getData.h"
#endif

	/*-----------------------------------------------------------*/

// allocating static DATA Buffer for real time board
extern tdPhysiologicalData  	staticBufferPhysiologicalData;
extern tdDevicesStatus		staticBufferDevicesStatusData;
extern tdPhysicalsensorData 	staticBufferPhysicalsensorData;
extern tdActuatorData		staticBufferActuatorData;
extern tdWakdStates		staticBufferStateRTB;
extern tdWakdOperationalState	staticBufferOperationalStateRTB;

uint8_t heartBeatRTBPA = 0;
uint8_t SetActuator_Beat = 0;
uint16_t StackRTB = 0;

// MFS: Integration 20120910 - Only valid for Integration 2012 - Should go
// RealtimeBoardProtocolAbstraction Task controls the States and ActuatorSettings.
// Therefore the following variables have been declared, using the stateId-enums as array-index

tdPumpCtrl BPCtrlIntegration[8];
tdPumpCtrl FPCtrlIntegration[8];
tdPolarizationCtrl PolCtrlIntegration[8];
tdMultiSwitchData SwitchCtrlIntegration[8];

// RTMCB StateNumbering differs from MB StateNumbering
// So we remember changeToState
uint8_t changeToState = 0;
uint8_t StateConversion[8];


void tskRTBPA( void *pvParameters )
{
        taskMessage("I", PREFIX, "Starting Real Time Board Protocol Abstraction task!");

        xQueueHandle *qhDISPin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
        xQueueHandle *qhRTBPAin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhRTBPAin;


        #ifdef HWBUILD

        StateConversion[0] = wakdStates_UndefinedInitializing;
        StateConversion[1] = wakdStates_AllStopped;
        StateConversion[2] = wakdStates_Dialysis;
        StateConversion[3] = wakdStates_Regen1;
        StateConversion[4] = wakdStates_Ultrafiltration;
        StateConversion[5] = wakdStates_Regen2;
        StateConversion[6] = wakdStates_Maintenance;
        StateConversion[7] = wakdStates_NoDialysate;


        BPCtrlIntegration[wakdStates_AllStopped].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_AllStopped].flowReference = _SPEED_CODE_STOP;
        FPCtrlIntegration[wakdStates_AllStopped].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_AllStopped].flowReference = _SPEED_CODE_STOP;
        PolCtrlIntegration[wakdStates_AllStopped].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_AllStopped].voltageReference = VOLTAGE_MIN;
        
        BPCtrlIntegration[wakdStates_Dialysis].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_Dialysis].flowReference = _SPEED_CODE_0575R;
        FPCtrlIntegration[wakdStates_Dialysis].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_Dialysis].flowReference = _SPEED_CODE_0575R;
        PolCtrlIntegration[wakdStates_Dialysis].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_Dialysis].voltageReference = VOLTAGE_MIN;

        BPCtrlIntegration[wakdStates_Regen1].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_Regen1].flowReference = _SPEED_CODE_0575R;
        FPCtrlIntegration[wakdStates_Regen1].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_Regen1].flowReference = _SPEED_CODE_0575R;
        PolCtrlIntegration[wakdStates_Regen1].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_Regen1].voltageReference = VOLTAGE_MIN;

        BPCtrlIntegration[wakdStates_Ultrafiltration].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_Ultrafiltration].flowReference = _SPEED_CODE_STOP;
        FPCtrlIntegration[wakdStates_Ultrafiltration].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_Ultrafiltration].flowReference = _SPEED_CODE_STOP;
        PolCtrlIntegration[wakdStates_Ultrafiltration].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_Ultrafiltration].voltageReference = VOLTAGE_MIN;

        BPCtrlIntegration[wakdStates_Regen2].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_Regen2].flowReference = _SPEED_CODE_0575R;
        FPCtrlIntegration[wakdStates_Regen2].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_Regen2].flowReference = _SPEED_CODE_0575R;
        PolCtrlIntegration[wakdStates_Regen2].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_Regen2].voltageReference = VOLTAGE_MIN;

        BPCtrlIntegration[wakdStates_NoDialysate].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_NoDialysate].flowReference = _SPEED_CODE_0575R;
        FPCtrlIntegration[wakdStates_NoDialysate].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_NoDialysate].flowReference = _SPEED_CODE_STOP;
        PolCtrlIntegration[wakdStates_NoDialysate].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_NoDialysate].voltageReference = VOLTAGE_MIN;

        BPCtrlIntegration[wakdStates_Maintenance].direction = SENS_DIRECT;
        BPCtrlIntegration[wakdStates_Maintenance].flowReference = _SPEED_CODE_STOP;
        FPCtrlIntegration[wakdStates_Maintenance].direction = SENS_DIRECT;
        FPCtrlIntegration[wakdStates_Maintenance].flowReference = _SPEED_CODE_STOP;
        PolCtrlIntegration[wakdStates_Maintenance].direction = SENS_DIRECT;
        PolCtrlIntegration[wakdStates_Maintenance].voltageReference = VOLTAGE_MIN;       

        staticBufferStateRTB = wakdStates_AllStopped;
        staticBufferOperationalStateRTB = wakdStatesOS_Automatic;

        #endif

        // TO ANALYZE RTMCB STATUS - CSEM DEMO 44
        uint32_t channelsFlags = 0;

 	tdQtoken Qtoken;
	Qtoken.command = command_DefaultError;
	Qtoken.pData   = NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
                StackRTB = stackHighWaterMark;
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	for( ;; ) { 
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every RTBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatRTBPA++;

                #ifdef HWBUILD
                        // This is demo functionality to test changing of WAKD states
                        /*
                        if(heartBeatRTBPA == 30) {
                            staticBufferStateRTB = wakdStates_Regen1;
                            //setActuatorsHW(wakdStates_Regen1);
                                tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
                                pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
                                if ( pMsgCurrentWAKDstateIs == NULL )
                                {	// unable to allocate memory
                                        taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                                        errMsg(errmsg_pvPortMallocFailed);
                                        break;
                                }
                                pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
                                pMsgCurrentWAKDstateIs->header.issuedBy	= whoId_RTB;
                                pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
                                pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
                                pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
                                
                                // send token to Dispatcher task
                                #if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
                                        taskMessage("I", PREFIX, "HackSending 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
                                #endif

                                Qtoken.command	= command_UseAtachedMsgHeader;
                                Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
                                // Send token to DISP
                                if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                                {	// DISP queue did not accept data. Free message and send error.
                                        taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
                                        errMsg(errmsg_QueueOfDispatcherTaskFull);
                                        sFree((void *)&pMsgCurrentWAKDstateIs);
                                }
                        }
                        
                        if(heartBeatRTBPA == 80) {
                            staticBufferStateRTB = wakdStates_AllStopped;
                            setActuatorsHW(wakdStates_AllStopped);
                        }

                        // Integration test for new ChangeWAKDStateTo command
                        // Executed at 50, 100,150 = lil over 10 seconds
                        
                        if(heartBeatRTBPA%400 ==100) {
                       
                            staticBufferStateRTB = wakdStates_Regen1;
                            setActuatorsHW(staticBufferStateRTB);
                        }
                       //*/ 

                    
                    #ifndef RTMCB_FW_96

                        // INTEGRATION 20121030 
                        // Here we check if the command for the RTMCB has been acknowledged.
                        // Coming directly from CSEM DEMO 44

                     
                     if( RTMCB_TASK_NEW_MESSAGE == SIGNAL_ON ){

                        // RTMCB Command check
                        // Is this a RTMCB OPMODES Data Packet?
                        // if(RTMCB_MBTASK_GETDATA_WHICHGROUP == RS422_ACK_RECV_OPMODES ){
                            // Do we expect a response? 
                            if(RTMCB_MBTASK_PUTDATA_CMD == OPMODES_CONFIGURE){
                              //Is the acknowledge matching the request?
                              if(MicrofluidicWorkingMode_cmdFback == MicrofluidicWorkingMode_CMD){
                                  if( (MicrofluidicWorkingMode_cmdFback > 0) && (MicrofluidicWorkingMode_cmdFback < 8) ){
                                    
                                    RTMCB_OPERATING_MODE = MicrofluidicWorkingMode_cmdFback;
                                    
                                    /*
                                    switch(MicrofluidicWorkingMode_cmdFback){
                                      case 1:   staticBufferStateRTB = wakdStates_AllStopped;
                                                break;
                                      case 2:   staticBufferStateRTB = wakdStates_Dialysis;
                                                break;
                                      case 3:   staticBufferStateRTB = wakdStates_Regen1;
                                                break;
                                      case 4:   staticBufferStateRTB = wakdStates_Ultrafiltration;
                                                break;
                                      case 5:   staticBufferStateRTB = wakdStates_Regen2;
                                                break;
                                      case 6:   staticBufferStateRTB = wakdStates_Maintenance;
                                                break;                              
                                      default:  staticBufferStateRTB = wakdStates_UndefinedInitializing;
                                                break;
                                    }*/

                                    MicrofluidicWorkingMode_cmdFback = 0;
                                    MicrofluidicWorkingMode_CMD = 0;   
                                  }
                              }
                            
                                                   
                              // ----------------------------------------
                              // RTMCB_STATUS
                              channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_RTMCB_STATUS;
                              if(channelsFlags!=0){
                                  RTMCB_channelsFlag_02 &= READOK_RTMCB_STATUS;
                                  RTMCB_MBTASK_CMD_EXECUTED = OperatingModeEXCfromRTMCBInfo(staticBufferDevicesStatus.statusRTMCB);
                                  // clears this part of the information
                                  staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_EXC;
    
                                  // [1] --> [0]
                                  // if not SIGNAL_OFF --> OPERATING MODE EXECUTED
                                  if(RTMCB_MBTASK_CMD_EXECUTED != SIGNAL_OFF){  
                                      RTMCB_MBTASK_PUTDATA_CMD = 0;   // DONE!
                                  }
                              }
                            } // close.. PUTDATA_CMD == OPMODES_CONFIGURE
                        //}

                       // We dont do this, this is used below in bufferfromrtb availlable
                       //RTMCB_MBTASK_GETDATA_WHICHGROUP = RS422_NOGROUP;
                       //RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_undefined;
                       RTMCB_TASK_NEW_MESSAGE = SIGNAL_OFF;
                      }

                                           
                      // Are we free to send a command to the RTMCB?
                      if(RTMCB_MBTASK_PUTDATA_CMD!=OPMODES_CONFIGURE){
                         
                          
                          if( StateConversion[RTMCB_OPERATING_MODE] != (uint8_t) staticBufferStateRTB  ){
                          
                            //RTMCB_MBTASK_PUTDATA_CMD = OPMODES_CONFIGURE;
                            setActuatorsHW(staticBufferStateRTB);
                            //staticBufferStateRTB++;
                          
                            //if(staticBufferStateRTB > 6) staticBufferStateRTB = 1;
                          
                            // ------------------------------------------ //
                            // MB SENDS NEW COMMAND TO RTMCB
                            //    RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                            // TIMEOUT COUNTER = 0;
                            //   RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                            // ------------------------------------------ //  
                          }

                         
                      }
                      else {  // Kind of Timeout counter, after a while tries again...
                          RTMCB_MBTASK_PUTDATA_COUNTER++;
                          if(RTMCB_MBTASK_PUTDATA_COUNTER == 16){
                              RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                              RTMCB_MBTASK_PUTDATA_CMD = 0;             // SETS TO 0
                              setActuatorsHW(staticBufferStateRTB);
                          }
                      }

                 #else
                  // INTEGRATION 20121210 - RTMCB-FW-V00R96
                  
                        if(heartBeatRTBPA == 40) {
                                  //staticBufferStateRTB = wakdStates_Dialysis;
                                  //setActuatorsHW(staticBufferStateRTB);
                          }
      
                          // Integration test for new ChangeWAKDStateTo command
                          // Executed at 50, 100,150 = lil over 10 seconds
                          
                        if(heartBeatRTBPA == 120) {
                       
                            //staticBufferStateRTB = wakdStates_Regen1;
                            //setActuatorsHW(staticBufferStateRTB);
                        }

                  // If equal, it means that the command has been started but not executed yet....
                   
                  if(RTMCB_MBTASK_PUTDATA_CMD!=OPMODES_CONFIGURE){
                      
                      /*
                      MicrofluidicWorkingMode_Status = MICROFLUIDIC_NULL;           // NULL, ACK, EXC 
                      MicrofulidicWorkingMode_Error = MICROFLUIDIC_EXECUTE_NOERROR;
                      MicrofluidicWorkingMode_cmdFback = 0;
                      // clears this part of the information
                      staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_ACK;
                      staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_EXC;
                      */
                      
                      // If RTMCB operating mode != requested State, send command to change state 
                      
                      //  setActuatorsHW(staticBufferStateRTB);
                      
                      // TEST
                      
                      if(heartBeatRTBPA == 30) {
                        MicrofluidicWorkingMode_Status = MICROFLUIDIC_NULL;           // NULL, ACK, EXC 
                        MicrofulidicWorkingMode_Error = MICROFLUIDIC_EXECUTE_NOERROR;
                        MicrofluidicWorkingMode_cmdFback = 0;
                        // clears this part of the information
                        staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_ACK;
                        staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_EXC;
                        RTMCB_MBTASK_PUTDATA_CMD = OPMODES_CONFIGURE;
                        //RTMCB_OPERATING_MODE = 3;
                        RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
                        MicrofluidicWorkingMode_CMD = MICROFLUIDIC_REGENERATION_1;
                       
                        // ------------------------------------------ //
                        // MB SENDS NEW COMMAND TO RTMCB, FLAG ON
                        RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                        // TIMEOUT COUNTER = 0
                        RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                        // ------------------------------------------ //
                      }
                      if(heartBeatRTBPA == 100) {
                        MicrofluidicWorkingMode_Status = MICROFLUIDIC_NULL;           // NULL, ACK, EXC 
                        MicrofulidicWorkingMode_Error = MICROFLUIDIC_EXECUTE_NOERROR;
                        MicrofluidicWorkingMode_cmdFback = 0;
                        // clears this part of the information
                        staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_ACK;
                        staticBufferDevicesStatus.statusRTMCB &= MICROFLUIDIC_OPMODE_CLR_EXC;
                        RTMCB_MBTASK_PUTDATA_CMD = OPMODES_CONFIGURE;
                        //RTMCB_OPERATING_MODE = 7;
                        RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
                        MicrofluidicWorkingMode_CMD = MICROFLUIDIC_DIALYSIS;
                       
                        // ------------------------------------------ //
                        // MB SENDS NEW COMMAND TO RTMCB, FLAG ON
                        RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                        // TIMEOUT COUNTER = 0
                        RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                        // ------------------------------------------ //
                      }
                      /*

                      
                      if( StateConversion[RTMCB_OPERATING_MODE] != (uint8_t) staticBufferStateRTB  ){
                        setActuatorsHW(staticBufferStateRTB);
                      }
                      
                      /*
                      if(heartBeatRTBPA%16 == 0){
                        RTMCB_MBTASK_PUTDATA_CMD = OPMODES_CONFIGURE;
                        vRTMCB_CMDS_EXAMPLE(OPMODES_CONFIGURE); 
                        // ------------------------------------------ //
                        // MB SENDS NEW COMMAND TO RTMCB, FLAG ON
                        RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                        // TIMEOUT COUNTER = 0
                        RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                        // ------------------------------------------ //
                     }*/

                      
                   }
                  
                  

                  if(RTMCB_TASK_NEW_MESSAGE == SIGNAL_ON){      // GOT DATA
                      // ------------------------------------------ //
                      // READ RTMCB_STATUS
                      // ------------------------------------------ //
                      // - DEVICE_NOTREADY: AT THE BEGINNING, NO COMMUNICATION WITH RTMCB YET
                      // - DEVICE_READY: RTMCB SENT 'READY' OR MB ASKED AND RTMCB ANSWER 'READY'
                      // - NONE: IT WAS RECEIVING CMD OR DATA BUT IT COULDN'T DECODE ANY PACKET
                      // - COMMAND: AT LEAST ONE STRUCTURED PACKET RECEIVED OK
                      // - INTERVAL: AT LEAST ONE INTERVAL DATA RECEIVED OK
                      // ------------------------------------------ //
                      // READ RTMCB_MBTASK_GETDATA_WHICHGROUP
                      // ------------------------------------------ //
                      // RS422_NOGROUP: at start-up, before analyzing message at the end of message processing
                      // RS422_ACK_RTMCB_READY: RTMCB indicates that it is ready to receive commands 
                      // RS422_SIMULATIONVSRT: RTMCB ACKS that it received simulation/real time mode (realtime)
                      // RS422_STSTSTREAMING: RTMCB ACKS that it received start/stop streaming command
                      // RS422_ACK_RECV_DATA: RTMCB ACKS that it received at least one new data (of any type)
                      // ------------------------------------------ //
                      // READ RTMCB_MBTASK_PUTDATA_CMD
                      // ------------------------------------------ //
                      // If the command executed was OPMODES_CONFIGURE, 
                      // and the feedback = the wished operating mode, updates the global variable
                    
                      if(RTMCB_MBTASK_PUTDATA_CMD == OPMODES_CONFIGURE){
                          if((MicrofluidicWorkingMode_cmdFback == MicrofluidicWorkingMode_CMD)&&(MicrofluidicWorkingMode_Status == MICROFLUIDIC_NULL)){
                              // The first acknowledge that the command has been received by the RTMCB
                              MicrofluidicWorkingMode_Status = MICROFLUIDIC_ACKNOWLEDGE1;
                              RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                          }
                          else if ((MicrofluidicWorkingMode_cmdFback != MicrofluidicWorkingMode_CMD)&&(MicrofluidicWorkingMode_Status == MICROFLUIDIC_NULL)){
                              // Kind of Timeout counter, after a while tries again...
                              // TIME OUT COUNTER UPDATE
                              RTMCB_MBTASK_PUTDATA_COUNTER++;
                              if(RTMCB_MBTASK_PUTDATA_COUNTER >= MICROFLUIDIC_PUTDATA_TIMEOUT){
                                  RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                                  // TIMEOUT, CAN SEND THE SAME COMMAND AGAIN
                                  RTMCB_MBTASK_PUTDATA_CMD = 0;             // SETS TO 0
                              }
                          }
                          // ----------------------------------------
                          // RTMCB_STATUS
                          // ----------------------------------------
                          // If RTMCB status was updated, see if OPMODES_CONFIGURE has been successfully executed
                          channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_RTMCB_STATUS;
                          if(channelsFlags!=0){
                              RTMCB_channelsFlag_02 &= READOK_RTMCB_STATUS;
                              if( OperatingModeACKfromRTMCBInfo(staticBufferDevicesStatus.statusRTMCB) == MicrofluidicWorkingMode_CMD){
                                  MicrofluidicWorkingMode_Status = MICROFLUIDIC_ACKNOWLEDGE2;
                              }
                              RTMCB_MBTASK_CMD_EXECUTED = OperatingModeEXCfromRTMCBInfo(staticBufferDevicesStatus.statusRTMCB);
                              if(RTMCB_MBTASK_CMD_EXECUTED == MICROFLUIDIC_EXECUTE_ERROR) MicrofluidicWorkingMode_Status = MICROFLUIDIC_EXECUTE_ERROR;
                              // [1] --> [0]
                              // if not SIGNAL_OFF --> OPERATING MODE EXECUTED
                              if (RTMCB_MBTASK_CMD_EXECUTED == MicrofluidicWorkingMode_CMD){
                                  RTMCB_OPERATING_MODE = MicrofluidicWorkingMode_CMD;
                                  MicrofluidicWorkingMode_Status = MICROFLUIDIC_EXECUTED;   // $$$$ 03.12.2012 $$$$

                                   // INTEGRATION: 
                                   // We should set the new RTMCB status here...
                                   #ifdef RTMCB_HANDLES_STATE
                                      tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
                                      pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
                                      if ( pMsgCurrentWAKDstateIs == NULL )
                                      {	// unable to allocate memory
                                              taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                                              errMsg(errmsg_pvPortMallocFailed);
                                              break;
                                      }
                                      pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
                                      pMsgCurrentWAKDstateIs->header.issuedBy	= whoId_RTB;
                                      pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
                                      pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
                                      pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
                                      
                                      // send token to Dispatcher task
                                      #if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
                                              taskMessage("I", PREFIX, "HackSending 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
                                      #endif
                                      Qtoken.command	= command_UseAtachedMsgHeader;
                                      Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
                                      // Send token to DISP
                                      if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                                      {	// DISP queue did not accept data. Free message and send error.
                                              taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
                                              errMsg(errmsg_QueueOfDispatcherTaskFull);
                                              sFree((void *)&pMsgCurrentWAKDstateIs);
                                      }
                                  #endif
                                  // command finished, ready to start again
                                  RTMCB_MBTASK_PUTDATA_CMD = 0;   // DONE!
                              }
                              else {
                                  // TIME OUT COUNTER UPDATE
                                  RTMCB_MBTASK_PUTDATA_COUNTER++;
                                  if(RTMCB_MBTASK_PUTDATA_COUNTER >= MICROFLUIDIC_EXECUTION_TIMEOUT){
                                      RTMCB_MBTASK_PUTDATA_COUNTER = 0;
                                      // TIMEOUT, CAN SEND THE SAME COMMAND AGAIN
                                      RTMCB_MBTASK_PUTDATA_CMD = 0;             // SETS TO 0
                                  }
                              }
                          }
                      }
                      // ------------------------------------------ //
                      // READ RTMCB_MBTASK_GETDATA_WHICHSUBGROUP (= RTMCB_message)
                      // ------------------------------------------ //
                      // dataID_undefined
                      // dataID_physicalData
                      // dataID_physiologicalData
                      // dataID_actuatorData
                      // dataID_alarmRTB
                      // dataID_statusRTB
                      // ------------------------------------------ //
                      // CLEAR ALL
                      // ------------------------------------------ //
                      RTMCB_MBTASK_GETDATA_WHICHGROUP = RS422_NOGROUP;
                      RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_undefined;
                      RTMCB_TASK_NEW_MESSAGE = SIGNAL_OFF;
                      // ------------------------------------------ //
                  } // RTMCB_TASK_NEW_MESSAGE = SIGNAL_ON (GOT DATA)
                  #endif

                  // INTEGRATION this is very nasty and should go.. (just for a test)
                  #ifdef ALARM_DUMMY_SEND
                    if(heartBeatRTBPA == 180){
                        sendSystemInfo(PREFIX, qhDISPin, dectreeMsg_detectedSorDys);
                    }
                    if(heartBeatRTBPA == 255){
                        sendSystemInfo(PREFIX, qhDISPin, dectreeMsg_detectedBPorFPhigh);
                    }
                  #endif


                  /*
                  // Pretend to periodically send current status to the Dispatcher
                  if(heartBeatRTBPA % 200 == 150 ){
                    // Now return the new status
                    // MFS 20120910 Integration: Pretend to be RTB and answer with the new current state:
    
                    tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
                    pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
                    if ( pMsgCurrentWAKDstateIs == NULL )
                    {	// unable to allocate memory
                            taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                            errMsg(errmsg_pvPortMallocFailed);
                            break;
                    }
                    pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
                    pMsgCurrentWAKDstateIs->header.issuedBy	= whoId_RTB;
                    pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
                    pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
                    pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
                    
                    // send token to Dispatcher task
                    #if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
                            taskMessage("I", PREFIX, "HackSending 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
                    #endif
                    Qtoken.command	= command_UseAtachedMsgHeader;
                    Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
                    // Send token to DISP
                    if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                    {	// DISP queue did not accept data. Free message and send error.
                            taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
                            errMsg(errmsg_QueueOfDispatcherTaskFull);
                            sFree((void *)&pMsgCurrentWAKDstateIs);
                    }
                }*/

                                             
                #endif

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
                        StackRTB = stackHighWaterMarkNew;
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		/*
		 * Watch Task's input queue for commands to come in.
		 * If nothing happens in queue, the task will wake up anyway
		 * after period to give note that it is unusually quiet!
		 */
		if ( xQueueReceive( qhRTBPAin, &Qtoken, RTBPA_PERIOD) == pdPASS )
		{	// We did find something in the queue of RTBPA
                        #ifdef DEBUG_RTBPA
			    taskMessage("I", PREFIX, "command: %d in RTBPA queue!", Qtoken.command);
			#endif

			
                        
                        switch (Qtoken.command) {
				case command_UseAtachedMsgHeader: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						
                                                case (dataID_configureState):{
							#if defined DEBUG_RTBPA || defined SEQ9 || defined SEQ0
								taskMessage("I", PREFIX, "Received 'dataID_configureState' for state '%s'.", stateIdToString(((tdMsgConfigureState *) (Qtoken.pData))->stateToConfigure));
							#endif
							#ifdef HWBUILD
                                                           // MFS 20120910 Integeration We do not use putData, RTBPA task handles configuration
                                                           // write values to the StateConfiguration integration variables
    
                                                           tdMsgConfigureState *msgConfigureState = (tdMsgConfigureState *) (Qtoken.pData);
                                                           //switch(msgConfigureState->stateToConfigure) 
                                                           
                                                           BPCtrlIntegration[msgConfigureState->stateToConfigure].direction = msgConfigureState->actCtrl.BLPumpCtrl.direction;
                                                           BPCtrlIntegration[msgConfigureState->stateToConfigure].flowReference = msgConfigureState->actCtrl.BLPumpCtrl.flowReference;
                                                           FPCtrlIntegration[msgConfigureState->stateToConfigure].direction = msgConfigureState->actCtrl.FLPumpCtrl.direction;
                                                           FPCtrlIntegration[msgConfigureState->stateToConfigure].flowReference = msgConfigureState->actCtrl.FLPumpCtrl.flowReference;
                                                           PolCtrlIntegration[msgConfigureState->stateToConfigure].direction = msgConfigureState->actCtrl.PolarizationCtrl.direction;
                                                           PolCtrlIntegration[msgConfigureState->stateToConfigure].voltageReference = msgConfigureState->actCtrl.PolarizationCtrl.voltageReference;
                                                           // The MicroFluidicSwitches don`t have to be configured now, predefined
                                                            
                                                        #else
                                                          if (putData(Qtoken.pData)) {
                                                                  errMsg(errmsg_PutDataFunctionFailed);
                                                          }
                                                        #endif
							sFree((void *)&Qtoken.pData);
							break;
						} 
                                                
						case (dataID_shutdown): // fall through, continue with reset.
						case (dataID_reset):{
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Received 'dataID_reset'.");
							#endif

                                                        #ifndef HWBUILD
							// sending msg to real time board via UART
							uint8_t err = putData(Qtoken.pData);
							if (err) {
								errMsg(errmsg_PutDataFunctionFailed);
							}
                                                        #else
                                                          // Reset requested! STOP
                                                          uint8_t err = 10;
                                                        #endif
							sFree((void *)&Qtoken.pData);
							// System is going down because of reset. Nothing more to do. Terminate this task
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Shutting down task.");
							#endif
							((tdAllHandles *) pvParameters)->allTaskHandles.handleRTBPA = NULL;
							vTaskDelete(NULL);	// Shut down and wait forever.
							vTaskSuspend(NULL); // Never run again (until after reset)
							break;
						}
                                                
						case (dataID_statusRequest):{
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Sending 'dataID_statusRequest' to RTB.");
							#endif

                                                        // Integration:
                                                        // Staterequest should be handled by RTB (use putdata)
                                                        // For now, RTBPA intercepts and repsonds
                                                        #ifndef HWBUILD
                                                          if (putData(Qtoken.pData)) {
                                                                  errMsg(errmsg_PutDataFunctionFailed);
                                                          }
                                                          sFree((void *)&Qtoken.pData);
                                                          break;
                                                        #else
                                                          // MS (NANO): Integration 20120906
                                                          // Here we intercept statusRequest Message to the RTB, 
                                                          // we pretend to be the RTB, and return the currentStatus to the Dispatcher
                                                          // This is actually a copy from the procedure below, BufferFromRTBAvailable -> currentWAKDStateIs
                                                          
                                                          // Clean up the request first
                                                          sFree((void *)&Qtoken.pData);
                                                          
                                                          tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
                                                          pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
                                                          if ( pMsgCurrentWAKDstateIs == NULL )
                                                          {	// unable to allocate memory
                                                                  taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                                                                  errMsg(errmsg_pvPortMallocFailed);
                                                                  break;
                                                          }
                                                          pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
                                                          pMsgCurrentWAKDstateIs->header.issuedBy	= whoId_RTB;
                                                          pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
                                                          pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
                                                          pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
                                                          
                                                          // send token to Dispatcher task
                                                          #if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
                                                                  taskMessage("I", PREFIX, "HackSending 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
                                                          #endif

                                                          Qtoken.command	= command_UseAtachedMsgHeader;
                                                          Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
                                                          // Send token to DISP
                                                          if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                                                          {	// DISP queue did not accept data. Free message and send error.
                                                                  taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
                                                                  errMsg(errmsg_QueueOfDispatcherTaskFull);
                                                                  sFree((void *)&pMsgCurrentWAKDstateIs);
                                                          }
                                                          break;		
                                                        #endif //HWBuild                                                  
                                                      
						}
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
                                
				case command_RtbChangeIntoStateRequest: {
					// Forward this message to RTBPA
					#if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
						taskMessage("I", PREFIX, "Sending 'command_RtbChangeIntoStateRequest' to RTB.");
					#endif

                                        // Integration:
                                        // State change should be handled by RTB (use putdata to forward)
                                        // But for now (sept 2012), RTBPA handles states and responds
                                        // the term HWBUILD for this define is not good, should be smt like RTB_HANDLES_STATES
                                        
                                        #ifndef HWBUILD
                                          if (putData(Qtoken.pData)) {
                                                  errMsg(errmsg_PutDataFunctionFailed);
                                          }
                                          sFree((void *)&Qtoken.pData);
                                          break;
                                        
                                        #else                                         
                                           // MFS: 20120906 Integration 
                                           // State handling is now done in this task
                                           // Using the global static buffers to keep track of the state.
                                        
                                          // In the final version the RTB will change into the new state without any condition.
                                          // RTB will answer this with its current status. Just as if it had received: dataID_statusRequest
                                          
                                          tdMsgWakdStateFromTo *pMsgWakdStateFromTo = (tdMsgWakdStateFromTo*) (Qtoken.pData);
                                          
                                          // RTB State
                                          staticBufferStateRTB = pMsgWakdStateFromTo->toWakdState;
                                          if(!staticBufferStateRTB) staticBufferStateRTB = wakdStates_AllStopped;
                                          // RTB Operational State
                                          staticBufferOperationalStateRTB = pMsgWakdStateFromTo->toWakdOpState;
                                          if(!staticBufferOperationalStateRTB) staticBufferOperationalStateRTB = wakdStatesOS_Automatic;
                                          
                                          // Activate State by Updating Actuators
                                          // This is for now only (sept 2012), should be done by RTB later                                          
                                          // We skip this, this moved to the top
                                          // setActuatorsHW(staticBufferStateRTB);  
  
                                          #ifdef VP_SIMULATION
                                                  // For debugging tracing we protocol each change in state in log file
                                                  FILE *pFile = fopen("stateTransition.dump", "a");
                                                  if(pFile!=NULL) {
                                                          fprintf(pFile,"*** Time: %lu ms: Changing into state ", xTaskGetTickCount());
                                                          fprintf(pFile,"'%s'|'%s'.\n", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
                                                          fclose(pFile);
                                                  }
                                          #endif
          
                                          #if defined SEQ0 || defined SEQ4
                                                  taskMessage("I", "putData()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
                                          #endif
                                          
                                          #ifndef HWBUILD
                                            newStaticBufferStatesRTB = 1;	// for decode() to know what data has just arrived. (CSEM DEMO)
                                          #endif
        
                                          // Free the incoming message
                                          sFree((void *)&Qtoken.pData);
                                          

                                            // INTEGRATION 20121210 - This moved to the top (RTMCB acks state change)
                                          #ifndef RTMCB_HANDLES_STATE
                                            // Now return the new status
                                            // MFS 20120910 Integration: Pretend to be RTB and answer with the new current state:
    
                                            tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
                                            pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
                                            if ( pMsgCurrentWAKDstateIs == NULL )
                                            {	// unable to allocate memory
                                                    taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                                                    errMsg(errmsg_pvPortMallocFailed);
                                                    break;
                                            }
                                            pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
                                            pMsgCurrentWAKDstateIs->header.issuedBy	= whoId_RTB;
                                            pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
                                            pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
                                            pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
                                            
                                            // send token to Dispatcher task
                                            #if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
                                                    taskMessage("I", PREFIX, "HackSending 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
                                            #endif
                                            Qtoken.command	= command_UseAtachedMsgHeader;
                                            Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
                                            // Send token to DISP
                                            if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                                            {	// DISP queue did not accept data. Free message and send error.
                                                    taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
                                                    errMsg(errmsg_QueueOfDispatcherTaskFull);
                                                    sFree((void *)&pMsgCurrentWAKDstateIs);
                                            }
                                          #endif // RTMCB_HANDLES_STATE
                                          break;

                                        #endif // HWBUILD
				}
                                
				case command_BufferFromRTBavailable: {
                                        
                                        // Integration 20120918:
                                        // decodeMsg is the way to go originally: it should decode the message in the buffer (from RTB)
                                        // This is now done instantly in the interrupt handler which is not ideal and should go.
                                        // However for now (Test sept. 2012) we use this already decoded message
                                       
                                        
					#ifndef HWBUILD
                                          tdDataId dataId = decodeMsg();
                                        #else                               
                                          tdDataId dataId = RTMCB_MBTASK_GETDATA_WHICHSUBGROUP;
                                        #endif

					#if defined DEBUG_RTBPA
						taskMessage("I", PREFIX, "Decoded id: '%s'.", dataIdToString(dataId));
					#endif
                                        
                                                             


                                        // FOR INTEGRATION 
                                        // Free the incoming message
                                        // sFree((void *)&Qtoken.pData);

					switch (dataId) {
                                              
						case dataID_physiologicalData: {
							// SEQ6: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgPhysiologicalData *pMsgPhysiologicalData = NULL;
							pMsgPhysiologicalData = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
							if ( pMsgPhysiologicalData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ6
								taskMessage("I", PREFIX, "Copy static buffer from UART: physiologicalData.");
							#endif

                                                        #ifdef ECP_SENSOR_DUMMY_INTEGRATION
                                                          if(heartBeatRTBPA < 128){
                                                            staticBufferPhysiologicalData.ECPDataI.Sodium = 1470;
                                                            staticBufferPhysiologicalData.ECPDataO.Sodium = 1370;
                                                            staticBufferPhysiologicalData.ECPDataI.Potassium = 34;
                                                            staticBufferPhysiologicalData.ECPDataO.Potassium = 15;
                                                            staticBufferPhysiologicalData.ECPDataI.Urea = 30;
                                                            staticBufferPhysiologicalData.ECPDataO.Urea = 10;
                                                            staticBufferPhysiologicalData.ECPDataI.pH = 74;
                                                            staticBufferPhysiologicalData.ECPDataO.pH = 13;
                                                            staticBufferPhysiologicalData.ECPDataI.Temperature = 365;
                                                            staticBufferPhysiologicalData.ECPDataO.Temperature = 380;
                                                           }
                                                          else{
                                                           staticBufferPhysiologicalData.ECPDataI.Sodium = 1480;
                                                            staticBufferPhysiologicalData.ECPDataO.Sodium = 1380;
                                                            staticBufferPhysiologicalData.ECPDataI.Potassium = 35;
                                                            staticBufferPhysiologicalData.ECPDataO.Potassium = 16;
                                                            staticBufferPhysiologicalData.ECPDataI.Urea = 32;
                                                            staticBufferPhysiologicalData.ECPDataO.Urea = 14;
                                                            staticBufferPhysiologicalData.ECPDataI.pH = 77;
                                                            staticBufferPhysiologicalData.ECPDataO.pH = 18;
                                                            staticBufferPhysiologicalData.ECPDataI.Temperature = 371;
                                                            staticBufferPhysiologicalData.ECPDataO.Temperature = 384;
                                                          }
                                                        #endif

                                                        

							memcpy(&(pMsgPhysiologicalData->physiologicalData), &staticBufferPhysiologicalData, sizeof(tdPhysiologicalData));
							pMsgPhysiologicalData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ6: http://is.gd/IJSmbK)
							// Ack is being skipped.

                                //Integration: we skip step 3 and free the incoming message
                                                        //sFree((void *)&Qtoken.pData);
                                                        //sFree((void *)&pMsgPhysiologicalData);

                                                        
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.							
                                                        
                                                        #if defined DEBUG_RTBPA || defined SEQ6
								taskMessage("I", PREFIX, "Forwarding 'dataID_physiologicalData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgPhysiologicalData;
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physiologicalData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgPhysiologicalData);
							}

                                                        #ifdef DUMMY_PHYSICAL_VALLUES_SENDING

                                                                    tdMsgPhysicalsensorData *pMsgPhysicalsensorData = NULL;
                                                                    pMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
                                                                    if ( pMsgPhysicalsensorData == NULL )
                                                                    {	// unable to allocate memory
                                                                            taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
                                                                            errMsg(errmsg_pvPortMallocFailed);
                                                                            break;
                                                                    }
                                                                    #if defined DEBUG_RTBPA || defined SEQ7
                                                                            taskMessage("I", PREFIX, "Copy static buffer from UART: PhysicalsensorData.");
                                                                    #endif
                                                                    memcpy(&(pMsgPhysicalsensorData->physicalsensorData), &staticBufferPhysicalsensorData, sizeof(tdPhysicalsensorData));
                                                                    pMsgPhysicalsensorData->header.dataId = dataID_physicalData;
                                            // ** STEP  2: Ack Buffer
                                                                    // CSEM did not see the need to implement a sequence with Ack (compare with SEQ7: http://is.gd/IJSmbK)
                                                                    // Ack is being skipped.
                                            
                                            //Integration: we skip step 3 and free the incoming message
                                                                    //sFree((void *)&Qtoken.pData);
                                                                    //sFree((void *)&pMsgPhysicalsensorData);
                                                                    
                                            // ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
                                                                    #if defined DEBUG_RTBPA || defined SEQ7
                                                                            taskMessage("I", PREFIX, "Forwarding 'dataID_physicalData' to Dispatcher Task.");
                                                                    #endif
                                                                    Qtoken.command	= command_UseAtachedMsgHeader;
                                                                    Qtoken.pData	= (void *) pMsgPhysicalsensorData;
                                                                    // Send token to DISP
                                                                    if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
                                                                    {	// DISP queue did not accept data. Free message and send error.
                                                                            taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physicalData'!");
                                                                            errMsg(errmsg_QueueOfDispatcherTaskFull);
                                                                            sFree((void *)&pMsgPhysicalsensorData);
                                                                    }
                                                        #endif
                                                        
							break;
						}
						case dataID_physicalData: {
							// SEQ7: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgPhysicalsensorData *pMsgPhysicalsensorData = NULL;
							pMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
							if ( pMsgPhysicalsensorData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ7
								taskMessage("I", PREFIX, "Copy static buffer from UART: PhysicalsensorData.");
							#endif
							memcpy(&(pMsgPhysicalsensorData->physicalsensorData), &staticBufferPhysicalsensorData, sizeof(tdPhysicalsensorData));
							pMsgPhysicalsensorData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ7: http://is.gd/IJSmbK)
							// Ack is being skipped.
                                
                                                        //Integration: we skip step 3 and free the incoming message
                                                        //sFree((void *)&Qtoken.pData);
                                                        //sFree((void *)&pMsgPhysicalsensorData);
                                                        
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA || defined SEQ7
								taskMessage("I", PREFIX, "Forwarding 'dataID_physicalData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgPhysicalsensorData;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physicalData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgPhysicalsensorData);
							}
							break;
						}
                                                
						case dataID_actuatorData: {
							// SEQ8: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgActuatorData *pMsgActuatorData = NULL;
							pMsgActuatorData = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
							if ( pMsgActuatorData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ8
								taskMessage("I", PREFIX, "Copy static buffer from UART: ActuatorData.");
							#endif
							memcpy(&(pMsgActuatorData->actuatorData), &staticBufferActuatorData, sizeof(tdActuatorData));
							pMsgActuatorData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.

                                //Integration: we skip step 3 and free the incoming message
                                                        sFree((void *)&Qtoken.pData);
                                                        sFree((void *)&pMsgActuatorData);
                                                        /*
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA || defined SEQ8
								taskMessage("I", PREFIX, "Forwarding 'dataID_actuatorData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgActuatorData;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_actuatorData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgActuatorData);
							}*/
							break;
						}
						case dataID_statusRTB: {				
							// SEQ????: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgDeviceStatus *pMsgDevicesStatus = NULL;
							pMsgDevicesStatus = (tdMsgDeviceStatus *) pvPortMalloc(sizeof(tdMsgDeviceStatus)); // Will be freed by receiver task!
							if ( pMsgDevicesStatus == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Copy static buffer from UART: StatusData.");
							#endif
#ifndef HWBUILD
							memcpy(&(pMsgDevicesStatus->devicesStatus), &staticBufferDevicesStatusData, sizeof(tdDevicesStatus));
#else
 							memcpy(&(pMsgDevicesStatus->devicesStatus), &staticBufferDevicesStatus, sizeof(tdDevicesStatus));
#endif
                                                        pMsgDevicesStatus->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.

                                //Integration: we skip step 3 and free the incoming message
                                                        sFree((void *)&Qtoken.pData);
                                                        sFree((void *)&pMsgDevicesStatus);
                                                        /*
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Forwarding 'dataID_statusData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgDevicesStatus;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_statusData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgDevicesStatus);
							}*/
							break;
						}
						case dataID_alarmRTB: {
							// SEQ????: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgAlarms *pMsgAlarms = NULL;
							pMsgAlarms = (tdMsgAlarms *) pvPortMalloc(sizeof(tdMsgAlarms)); // Will be freed by receiver task!
							if ( pMsgAlarms == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Copy static buffer from UART: AlarmData.");
							#endif
							memcpy(&(pMsgAlarms->alarms), &staticBufferAlarms, sizeof(tdAlarms));
							pMsgAlarms->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.

                                //Integration: we skip step 3 and free the incoming message
                                                        sFree((void *)&Qtoken.pData);
                                                        sFree((void *)&pMsgAlarms);
                                                        
                                                        /*
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Forwarding 'dataID_alarmRTB' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgAlarms;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_alarmRTB'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgAlarms);
							}*/
							break;
						}
						case dataID_currentWAKDstateIs:{
							tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
							pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
							if ( pMsgCurrentWAKDstateIs == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
							pMsgCurrentWAKDstateIs->header.issuedBy		= whoId_RTB;
							pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
							pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
							pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
							// send token to Dispatcher task
							#if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
								taskMessage("I", PREFIX, "Forwarding 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
							#endif

                                            //Integration: we skip step 3 and free the incoming message
                                                        //sFree((void *)&Qtoken.pData);
                                                        //sFree((void *)&pMsgCurrentWAKDstateIs);

                                                        
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgCurrentWAKDstateIs);
							}
							break;
						}
						default : {
							defaultIdHandling(PREFIX, dataId);
                                                        //Integration: do we cover a possible leak?
                                                        sFree((void *)&Qtoken.pData);
							break;
						} 
					}
					break;
				}
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
                               

		} else {
			// We now waited for "RTBPA_PERIOD" and nothing arrived in input queue.
			// But the RTBPA should be rather busy. Issue an error.
			taskMessage("W", PREFIX, "No message for very long time. This doesn't look good!!!!");
			// Make an entry in the protocol of data storage. This is an unusual event.
			// errMsg(errmsg_TimeoutOnRtbpaQueue);
		}
	}
}


//void setActuators(tdActCtrl *pActuators)
//{
//	setActuatorInSimulink (ACTUATOR_BLOODPUMP,    FIXPOINTSHIFT_BLOODPUMP,    pActuators->BLPumpCtrl.flowReference);
//	setActuatorInSimulink (ACTUATOR_FLUIDPUMP,    FIXPOINTSHIFT_FLUIDPUMP,    pActuators->FLPumpCtrl.flowReference);
//	setActuatorInSimulink (ACTUATOR_POLARIZATION, FIXPOINTSHIFT_POLARIZATION, pActuators->PolarizationCtrl.voltageReference);
//	// setActuatorInSimulink (ACTUATOR_MFSO,   	  FIXPOINTSHIFT_MFSO,	      pActuators->mfso);
//	// setActuatorInSimulink (ACTUATOR_MFSU, 	      FIXPOINTSHIFT_MFSU,		  pActuators->mfsu);
//	// setActuatorInSimulink (ACTUATOR_MFSI, 	      FIXPOINTSHIFT_MFSI,		  pActuators->mfsi);
//}

//----------------------------------------------------------------------------------------------
// 20120910 Integration - NANO 
// setActuatorsHW() is used for the first prototype, it controls actuators separately, depending on current state
// Configuration (pumpspeeds and switchsettings) of these states is handled by rtbpa-task now, stored in local `integration`-variables, should be in RTB finally
//----------------------------------------------------------------------------------------------

#ifdef HWBUILD

void setActuatorsHW(tdWakdStates stateId)
{
       if( RTMCB_TASK_NEW_MB_CMD == SIGNAL_ON ) {

        // Message in the queue
        RTMCB_TASK_NEW_MB_CMD = RTMCB_TASK_NEW_MB_CMD;
       }

        RTMCB_MBTASK_PUTDATA_CMD = OPMODES_CONFIGURE;

        // Setting variables for this new command
        // ------------------------------------------ //
        // OPERATING MODES
        // ------------------------------------------ //
   
        // POLARIZER: DIRECTION
        polarizer_direction = PolCtrlIntegration[(uint8_t)stateId].direction;

        // POLARIZER VALUE: VOLTAGE IN mV          
        // polarizer_voltage = VOLTAGE_MIN;
        polarizer_voltage = PolCtrlIntegration[(uint8_t)stateId].voltageReference;

        // BLPUMP: SPEED CODE
        blpump_speedcode = BPCtrlIntegration[(uint8_t)stateId].flowReference;

        // FLPUMP: SPEED CODE
        flpump_speedcode = FPCtrlIntegration[(uint8_t)stateId].flowReference;

        // SETTING THE OPERATING MODE               
        switch(stateId){
                  
                   case wakdStates_NoDialysate:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_DIALYSATE_NOFLOW;
                          break;
                   case wakdStates_Dialysis:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_DIALYSIS;
                          break;
                   case wakdStates_Regen1:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_REGENERATION_1;
                          break;
                   case wakdStates_Ultrafiltration:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_ULTRAFILTRATION;
                          break;
                   case wakdStates_Regen2:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_REGENERATION_2;
                          break;
                   case wakdStates_Maintenance:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_MAINTENANCE;
                          break;
                   case wakdStates_AllStopped:
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_STOP;
                          break;
                   default: 
                          MicrofluidicWorkingMode_CMD = MICROFLUIDIC_STOP;
                          break;
        }
                  

        // READY TO SEND THE COMMAND...
        RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
        // MB SENDS NEW COMMAND TO RTMCB
        RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
        // TIMEOUT COUNTER = 0;
        RTMCB_MBTASK_PUTDATA_COUNTER = 0;

        SetActuator_Beat++;               
}


// -----------------------------------------------------------------------------------
//! \brief OperatingModeACKfromRTMCBInfo
//!
//! Has the OP MODE CMD been acknowledged?
//!
//! \param[IN]  rtmcb_status
//!
//! \return     rtmcb_mf_acknowledge or 0
// -----------------------------------------------------------------------------------
uint8_t OperatingModeACKfromRTMCBInfo(uint32_t rtmcb_status){
    uint8_t rtmcb_mf_acknowledge = (uint8_t)((uint8_t)((rtmcb_status >> 20)) & 0x07);   // nibble 5 (7..0)
    if ((rtmcb_mf_acknowledge > 0) && (rtmcb_mf_acknowledge < 7)) return rtmcb_mf_acknowledge;
    else return 0;
}
// -----------------------------------------------------------------------------------
//! \brief OperatingModeEXCfromRTMCBInfo
//!
//! Has the OP MODE CMD been executed?
//!
//! \param[IN]  rtmcb_status
//!
//! \return     rtmcb_mf_executed or 0
// -----------------------------------------------------------------------------------
uint8_t OperatingModeEXCfromRTMCBInfo(uint32_t rtmcb_status)
{
    uint8_t rtmcb_mf_executed = (uint8_t)((uint8_t)((rtmcb_status >> 24)) & 0x07);       // nibble 6 (7..0)
    if ((rtmcb_mf_executed > 0) && (rtmcb_mf_executed < 7)) return rtmcb_mf_executed;
    else return 0;
}


#endif
