/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model.relationships;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class MedicalPartActRelationship implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long medicanEntityID;
    private Long participationID;
    private Long actID;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalPartActRelationship)) {
            return false;
        }
        MedicalPartActRelationship other = (MedicalPartActRelationship) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.model.relationships.MedicalPartActRelationship[ id=" + id + " ]";
    }

    /**
     * @return the medicanEntityID
     */
    public Long getMedicanEntityID() {
        return medicanEntityID;
    }

    /**
     * @param medicanEntityID the medicanEntityID to set
     */
    public void setMedicanEntityID(Long medicanEntityID) {
        this.medicanEntityID = medicanEntityID;
    }

    /**
     * @return the participationID
     */
    public Long getParticipationID() {
        return participationID;
    }

    /**
     * @param participationID the participationID to set
     */
    public void setParticipationID(Long participationID) {
        this.participationID = participationID;
    }

    /**
     * @return the actID
     */
    public Long getActID() {
        return actID;
    }

    /**
     * @param actID the actID to set
     */
    public void setActID(Long actID) {
        this.actID = actID;
    }
    
}
