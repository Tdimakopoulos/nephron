/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.ScheduledActFacade;
import eu.nephron.model.act.ScheduledAct;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ScheduledActManager")
public class ScheduledActManager {
    @EJB
    private ScheduledActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "ScheduledActcreate")
    @Oneway
    public void ScheduledActcreate(@WebParam(name = "entity") ScheduledAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "ScheduledActedit")
    @Oneway
    public void ScheduledActedit(@WebParam(name = "entity") ScheduledAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "ScheduledActremove")
    @Oneway
    public void ScheduledActremove(@WebParam(name = "entity") ScheduledAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "ScheduledActfind")
    public ScheduledAct ScheduledActfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "ScheduledActfindAll")
    public List<ScheduledAct> ScheduledActfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "ScheduledActfindRange")
    public List<ScheduledAct> ScheduledActfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "ScheduledActcount")
    public int ScheduledActcount() {
        return ejbRef.count();
    }
    
}
