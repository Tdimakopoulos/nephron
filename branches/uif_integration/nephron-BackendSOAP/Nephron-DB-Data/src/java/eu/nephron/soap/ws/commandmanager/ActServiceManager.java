/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws.commandmanager;

import eu.nephron.beans.Children.MedicalPartActRelationshipFacade;
import eu.nephron.beans.Children.MedicalPartActRelationshipFacadeLocal;
import eu.nephron.beans.ParticipationFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.model.act.Act;
import eu.nephron.model.act.ActStatusEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.act.AssociationAct;
import eu.nephron.model.act.MoodCodeEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.entity.MedicalEntity;
import eu.nephron.model.entity.Person;
import eu.nephron.model.relationships.MedicalPartActRelationship;
import eu.nephron.model.role.Patient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ActServiceManager")
public class ActServiceManager {

    @EJB
    private CrudService crudService;
    @EJB
    private ParticipationFacade ejbRefparticipation;
    //ManagerAssociationsMPA pAsso=new ManagerAssociationsMPA();
    @EJB
    private MedicalPartActRelationshipFacadeLocal mrela;

    private void AddRelationship(Long actId, Long medicalEntityID, Long perparticipationId) {
        MedicalPartActRelationship pEntity = new MedicalPartActRelationship();
        pEntity.setActID(actId);
        pEntity.setMedicanEntityID(medicalEntityID);
        pEntity.setParticipationID(perparticipationId);
        mrela.create(pEntity);
    }

    @WebMethod(operationName = "CreateAssociationAct")
    public void createAssociationAct(List<Long> medicalEntityIDs, String publickey) {
        AssociationAct act = new AssociationAct();

        act.setActStatus(2);//ActStatusEnum.SUCCESSFUL
        act.setMoodCode(4);//MoodCodeEnum.EVENT
        crudService.create(act);
        for (Long medicalEntityID : medicalEntityIDs) {
            MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
            Participation participation = new Participation();

            Participation perparticipation = (Participation) crudService.create(participation);
            AddRelationship(act.getId(), entity.getId(), perparticipation.getId());
        }
    }

    @WebMethod(operationName = "UpdateAssociationAct")
    public void updateAssociationAct(Long actID, List<Long> medicalEntityIDs, String publickey) {
        AssociationAct act = (AssociationAct) crudService.find(AssociationAct.class, actID);
        for (Long medicalEntityID : medicalEntityIDs) {
            MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
            Participation participation = new Participation();

            Participation perparticipation = (Participation) crudService.create(participation);
            AddRelationship(act.getId(), entity.getId(), perparticipation.getId());
        }
    }

    private <T> boolean CheckForInstance(final T[] array, final T v) {
        for (final T e : array) {
            if (e == v || v != null && v.equals(e)) {
                return true;
            }
        }

        return false;
    }

    @WebMethod(operationName = "findConnections")
    public List<Person> findConnections(Long DoctorID) {
        List<Person> doctorrecord = new ArrayList();
        List<Participation> preturn = ejbRefparticipation.findAll();
        List<MedicalPartActRelationship> pmrelalist = mrela.findAll();

        List<Long> Actid = new ArrayList();
        Long iActID=0L;
        for (int i=0;i<pmrelalist.size();i++)
        {
            if (pmrelalist.get(i).getMedicanEntityID()==DoctorID)
            {
                iActID=pmrelalist.get(i).getActID();
            }
        }
//        for (int i = 0; i < preturn.size(); i++) {
//            if (preturn.get(i).getMedicalEntity().getId() == DoctorID) {
//                Actid.add( preturn.get(i).getAct().getId());
//                
//            }
//        }
//        for (int i = 0; i < preturn.size(); i++) {
//            
//            //if (Actid == preturn.get(i).getAct().getId()) {
//            if(CheckForInstance(Actid.toArray(),preturn.get(i).getAct().getId())){
//                if (preturn.get(i).getMedicalEntity().getId() == DoctorID) {
//                } else {
//                    Person ppatient = new Person();
//                    ppatient.setId(preturn.get(i).getMedicalEntity().getId());
//                    ppatient.setCode(preturn.get(i).getMedicalEntity().getCode());
//                    doctorrecord.add(ppatient);
//                }
//            }
//            
//        }


        return doctorrecord;
    }


    public Person FindPersonWithID(Long ID) {
        Person entity = (Person) crudService.find(MedicalEntity.class, ID);
        return entity;
    }
}
