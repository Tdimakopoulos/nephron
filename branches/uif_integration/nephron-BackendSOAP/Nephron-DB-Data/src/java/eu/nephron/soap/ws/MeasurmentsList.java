package eu.nephron.soap.ws;


import java.util.Date;


public class MeasurmentsList{
	
	private static final long serialVersionUID = 1692777844218154602L;
	
	private Long id;
	
	private int typeCode;
        
	
	private String text;
	
	
	private Date activityDate;
	
        
	private Long pID;
        
	
	private Long value;
        
        
	private int ivalue;
        
        
	private Float fvalue;
        
        
	private String svalue;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the typeCode
     */
    public int getTypeCode() {
        return typeCode;
    }

    /**
     * @param typeCode the typeCode to set
     */
    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the activityDate
     */
    public Date getActivityDate() {
        return activityDate;
    }

    /**
     * @param activityDate the activityDate to set
     */
    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    /**
     * @return the pID
     */
    public Long getpID() {
        return pID;
    }

    /**
     * @param pID the pID to set
     */
    public void setpID(Long pID) {
        this.pID = pID;
    }

    /**
     * @return the value
     */
    public Long getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Long value) {
        this.value = value;
    }

    /**
     * @return the ivalue
     */
    public int getIvalue() {
        return ivalue;
    }

    /**
     * @param ivalue the ivalue to set
     */
    public void setIvalue(int ivalue) {
        this.ivalue = ivalue;
    }

    /**
     * @return the fvalue
     */
    public Float getFvalue() {
        return fvalue;
    }

    /**
     * @param fvalue the fvalue to set
     */
    public void setFvalue(Float fvalue) {
        this.fvalue = fvalue;
    }

    /**
     * @return the svalue
     */
    public String getSvalue() {
        return svalue;
    }

    /**
     * @param svalue the svalue to set
     */
    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }

	
	
}
