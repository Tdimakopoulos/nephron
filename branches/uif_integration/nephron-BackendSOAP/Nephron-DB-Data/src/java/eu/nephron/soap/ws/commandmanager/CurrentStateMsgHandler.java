/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws.commandmanager;

import eu.nephron.beans.WakdStateFacade;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.WakdState;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;

public class CurrentStateMsgHandler extends IncomingWakdMsgHandler {

    @EJB
    private WakdStateFacade wakdstateejbRef;

    @Override
    protected boolean canHandle(IncomingWakdMsg msg) {
        return CurrentStateMsg.class.isInstance(msg);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean doHandle(IncomingWakdMsg msg, Device device) {

        CurrentStateMsg stateMsg = (CurrentStateMsg) msg;
        WakdState currentState = new WakdState();
        currentState.setDate(new Date());
        currentState.setDevice(device);
        currentState.setState(new Integer(stateMsg.getCurrentState().getIdentifier()));
        currentState.setOpState(new Integer(stateMsg.getCurrentOpState().getIdentifier()));

        List<WakdState> wakdp =wakdstateejbRef.FindParents(device);
        currentState.setParent(wakdp.get(0));
        wakdstateejbRef.create(currentState);
        return true;
    }
}
