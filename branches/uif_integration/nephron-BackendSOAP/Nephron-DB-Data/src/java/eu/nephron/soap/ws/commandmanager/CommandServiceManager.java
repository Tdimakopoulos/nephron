///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package eu.nephron.soap.ws.commandmanager;
//
//import eu.nephron.beans.Children.MedicalPartActRelationshipFacadeLocal;
//import eu.nephron.crudservice.CrudService;
//import eu.nephron.model.act.Act;
//import eu.nephron.model.act.ActRelationship;
//import eu.nephron.model.act.ActStatusEnum;
//import eu.nephron.model.act.CommandAct;
//import eu.nephron.model.act.Participation;
//import eu.nephron.model.entity.Device;
//import eu.nephron.model.entity.Person;
//import eu.nephron.model.relationships.MedicalPartActRelationship;
//import eu.nephron.wakd.api.IncomingWakdMsg;
//import eu.nephron.wakd.api.OutgoingWakdMsg;
//import java.util.HashMap;
//import java.util.Map;
//import javax.ejb.EJB;
//import javax.jws.WebService;
//import javax.jws.WebMethod;
//import javax.jws.WebParam;
//
///**
// *
// * @author tdim
// */
//@WebService(serviceName = "CommandServiceManager")
//public class CommandServiceManager {
//    @EJB
//    private CrudService crudService;
//    
//    @EJB
//     private MedicalPartActRelationshipFacadeLocal mrela;
//    
//    public void AddRelationship(Long actId, Long medicalEntityID, Long perparticipationId) {
//        MedicalPartActRelationship pEntity = new MedicalPartActRelationship();
//        pEntity.setActID(actId);
//        pEntity.setMedicanEntityID(medicalEntityID);
//        pEntity.setParticipationID(perparticipationId);
//        mrela.create(pEntity);
//    }
////    @WebMethod(operationName = "hello")
////    public String hello(@WebParam(name = "name") String txt) {
////        return "Hello " + txt + " !";
////    }
//    
//    @WebMethod(operationName = "ReadCommand")
//	public ActRelationship readCommand(Long deviceID, String publickey) {
//		
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("deviceID", deviceID);
//		ActRelationship actRelationship = 
//				(ActRelationship)crudService.findSingleWithNamedQuery("ActRelationship.findCommandBySmartphoneID", params);
//		if (null != actRelationship) {
//			Act inboundAct = actRelationship.getInboundAct();
//			inboundAct.setActStatus(2);//ActStatusEnum.SUCCESSFUL
//			crudService.update(inboundAct);
//		}
//		return actRelationship;
//            
//	}
//
//	@WebMethod(operationName = "CreateCommand")
//	public void createCommand(OutgoingWakdMsg message, Long doctorID, Long smartPhoneID, String publickey) {
//
//               // ManagerAssociationsMPA pAsso=new ManagerAssociationsMPA();
//            
//		Person doctor = (Person)crudService.find(Person.class, doctorID);
//		Device smartPhone = (Device)this.crudService.find(Device.class, smartPhoneID);
//		
//		CommandAct request = new CommandAct();
//		request.setWakdMsg(message.encode());
//		request.setMoodCode(3);//MoodCodeEnum.REQUEST);
//		request.setActStatus(1);//ActStatusEnum.PENDING);
//		crudService.create(request);
//		
//		Participation doctorRP = new Participation();
//		//doctorRP.setMedicalEntity(doctor);
//		//doctorRP.setAct(request);
//
//		Participation perdoctorRP =(Participation) crudService.create(doctorRP);
//                
//                AddRelationship(request.getId(), doctorID, perdoctorRP.getId());
//		
//		Participation smartPhoneRP = new Participation();
//		//smartPhoneRP.setMedicalEntity(smartPhone);
//		//smartPhoneRP.setAct(request);
//		Participation persmartPhoneRP =(Participation) crudService.create(smartPhoneRP);
//		
//                AddRelationship(request.getId(), smartPhoneID, persmartPhoneRP.getId());
//                
//		CommandAct event = new CommandAct();
//		event.setMoodCode(4);//MoodCodeEnum.EVENT);
//		event.setActStatus(1);//ActStatusEnum.PENDING);
//		crudService.create(event);
//		
//		Participation smartPhoneEP = new Participation();
//		//smartPhoneEP.setMedicalEntity(smartPhone);
//		//smartPhoneEP.setAct(event);
//		Participation persmartPhoneEP =(Participation) crudService.create(smartPhoneEP);
//		
//                AddRelationship(event.getId(), smartPhoneID, persmartPhoneEP.getId());
//                
//		Participation wakdEP = new Participation();
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("deviceID", smartPhoneID);
//		Device wakd = (Device)crudService.findSingleWithNamedQuery("Device.findAssociatedDevice", params);
//		//wakdEP.setMedicalEntity(wakd);
//		//wakdEP.setAct(event);
//		Device perwakd =(Device) crudService.create(wakdEP);
//		
//                AddRelationship(event.getId(), wakd.getId(), new Long(-1));
//                
//		ActRelationship relationShip = new ActRelationship();
//		relationShip.setInboundAct(request);
//		relationShip.setOutboundAct(event);
//		crudService.create(relationShip);
//	}
//	
//	@WebMethod(operationName = "UpdateCommand")
//	public void updateCommand(IncomingWakdMsg msg, Long commandID, Integer statusID, String publickey) {
//		
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("commandID", commandID);
//		Device device = 
//				(Device)crudService.findSingleWithNamedQuery("Device.findWakdByCommandID", params);
//		IncomingWakdMsgHandler.handleIncomingWakdMsg(msg, device);
//		
//		CommandAct act = (CommandAct)crudService.find(CommandAct.class, commandID);
//		act.setWakdMsg(msg.encode());
//		act.setActStatus(statusID);
//		crudService.update(act);
//	}
//	
//	@WebMethod(operationName = "UpdateCommandStatus")
//	public void updateCommandStatus(Long commandID, Integer statusID, String publickey) {
//		
//		CommandAct act = (CommandAct)crudService.find(CommandAct.class, commandID);
//		act.setActStatus(statusID);
//		crudService.update(act);
//	}
//}
