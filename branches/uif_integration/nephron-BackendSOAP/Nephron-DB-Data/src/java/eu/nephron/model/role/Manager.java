package eu.nephron.model.role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("5")
public class Manager extends Employee {

	private static final long serialVersionUID = 7762808700738543916L;
	
}
