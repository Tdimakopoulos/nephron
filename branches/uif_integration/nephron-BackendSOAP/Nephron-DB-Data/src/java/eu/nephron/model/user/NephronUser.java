//package eu.nephron.model.user;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//
//
//
//@Entity
//@Table(name="NEPHRON_USER")
//@NamedQueries({
//	@NamedQuery(name="NephronUser.findByUserName", query="select u from NephronUser u where u.userName=:userName"),
//	@NamedQuery(name="NephronUser.findByUserNameAndPassword", query="select u from NephronUser u where u.userName=:userName and u.password=:password")
//})
//public class NephronUser{
//
//	private static final long serialVersionUID = 7213034380629090950L;
//
//	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
//	@Column(name="NEPHRON_USER_ID")
//	private Long id;
//
//	@Column(name="USER_NAME", nullable=false, unique=true)
//	private String userName;
//	
//	@Column(name="PASSWD", nullable=false)
//	private String password;
//	
//	@Column(name="FULL_NAME")
//	private String fullName;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	public String getFullName() {
//		return fullName;
//	}
//
//	public void setFullName(String fullName) {
//		this.fullName = fullName;
//	}
//
//}
