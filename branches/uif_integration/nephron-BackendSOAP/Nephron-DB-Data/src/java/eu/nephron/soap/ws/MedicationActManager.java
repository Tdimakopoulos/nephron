/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.MedicationActFacade;
import eu.nephron.model.act.MedicationAct;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "MedicationActManager")
public class MedicationActManager {
    @EJB
    private MedicationActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "entity") MedicationAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "entity") MedicationAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "entity") MedicationAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "find")
    public MedicationAct find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<MedicationAct> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<MedicationAct> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
