/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.act.AssociationAct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class AssociationActFacade extends AbstractFacade<AssociationAct> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AssociationActFacade() {
        super(AssociationAct.class);
    }
    
}
