package eu.nephron.model.role;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class Patient extends Role {
	
	private static final long serialVersionUID = -1764970655524852350L;

	@Column(name="CONF_CODE")
	private String confidentialityCode;
	
	@Column(name="VIP_CODE")
	private String vipCode;

	public String getConfidentialityCode() {
		return confidentialityCode;
	}

	public void setConfidentialityCode(String confidentialityCode) {
		this.confidentialityCode = confidentialityCode;
	}

	public String getVipCode() {
		return vipCode;
	}

	public void setVipCode(String vipCode) {
		this.vipCode = vipCode;
	}
	
}
