package eu.nephron.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class Food extends MedicalEntity {

	private static final long serialVersionUID = 5849393902716691508L;

	@Column(name="FAT_QUANTITY")
	private BigDecimal fatQuantity;
	
	@Column(name="PROTEIN_QUANTITY")
	private BigDecimal proteinQuantity;
	
	@Column(name="CARBOHYDRATE_QUANTITY")
	private BigDecimal carbohydrateQuantity;

	public BigDecimal getFatQuantity() {
		return fatQuantity;
	}

	public void setFatQuantity(BigDecimal fatQuantity) {
		this.fatQuantity = fatQuantity;
	}

	public BigDecimal getProteinQuantity() {
		return proteinQuantity;
	}

	public void setProteinQuantity(BigDecimal proteinQuantity) {
		this.proteinQuantity = proteinQuantity;
	}

	public BigDecimal getCarbohydrateQuantity() {
		return carbohydrateQuantity;
	}

	public void setCarbohydrateQuantity(BigDecimal carbohydrateQuantity) {
		this.carbohydrateQuantity = carbohydrateQuantity;
	}
	
}
