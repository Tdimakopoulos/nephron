package eu.nephron.model.role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="ROLE_LINK")
public class RoleRelationship {

	private static final long serialVersionUID = 7656271504539418159L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ROLE_LINK_ID")
	protected Long id;
	
	@ManyToOne
	@JoinColumn(name="SCOPER_ID")
	protected Role scoper;
	
	@ManyToOne
	@JoinColumn(name="PLAYER_ID")
	protected Role player;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Role getScoper() {
		return scoper;
	}

	public void setScoper(Role scoper) {
		this.scoper = scoper;
	}

	public Role getPlayer() {
		return player;
	}

	public void setPlayer(Role player) {
		this.player = player;
	}

}
