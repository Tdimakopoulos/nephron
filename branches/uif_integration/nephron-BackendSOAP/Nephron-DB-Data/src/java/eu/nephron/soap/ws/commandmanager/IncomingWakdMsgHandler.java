/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws.commandmanager;

import javax.naming.NamingException;


import eu.nephron.model.entity.Device;
import eu.nephron.wakd.api.IncomingWakdMsg;

public abstract class IncomingWakdMsgHandler {
	
	private static CurrentStateMsgHandler stateMsgHandler = new CurrentStateMsgHandler();
	
	public static boolean handleIncomingWakdMsg(IncomingWakdMsg msg, Device device) {
		return stateMsgHandler.handle(msg, device);
	}

	protected IncomingWakdMsgHandler successor;
	
	public IncomingWakdMsgHandler getSuccessor() {
		return successor;
	}

	public void setSuccessor(IncomingWakdMsgHandler successor) {
		this.successor = successor;
	}
	
	public boolean handle(IncomingWakdMsg msg, Device device) {
		if (canHandle(msg)) {
			return this.doHandle(msg, device);
		} else {
			return null != successor ? successor.handle(msg, device) : false;
		}
	}
	


	protected abstract boolean canHandle(IncomingWakdMsg msg);
	
	protected abstract boolean doHandle(IncomingWakdMsg msg, Device device);
}
