/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans.Children;

import eu.nephron.model.relationships.MedicalPartActRelationship;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class MedicalPartActRelationshipFacade extends AbstractFacade<MedicalPartActRelationship> implements MedicalPartActRelationshipFacadeLocal {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MedicalPartActRelationshipFacade() {
        super(MedicalPartActRelationship.class);
    }
    
}
