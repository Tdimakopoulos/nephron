/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.Children.MedicalPartActRelationshipFacade;
import eu.nephron.beans.Children.MedicalPartActRelationshipFacadeLocal;
import eu.nephron.beans.Children.PersonFacade;
import eu.nephron.beans.Children.SmartPhoneFacade;
import eu.nephron.beans.Children.WAKDFacade;
import eu.nephron.beans.DeviceFacade;
//import eu.nephron.beans.NephronUserFacade;
import eu.nephron.beans.RoleFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.model.act.AssociationAct;
import eu.nephron.model.act.Participation;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.MedicalEntity;
import eu.nephron.model.entity.Person;
import eu.nephron.model.relationships.MedicalPartActRelationship;
import eu.nephron.model.role.Doctor;
import eu.nephron.model.role.Patient;
import eu.nephron.model.role.Role;
import eu.nephron.model.role.SmartPhone;
import eu.nephron.model.role.WAKD;
//import eu.nephron.model.user.NephronUser;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "TestWS")
public class TestWS {

    @EJB
    private RoleFacade pRole;
    @EJB
    private SmartPhoneFacade pSmartPhone;
    @EJB
    private WAKDFacade pwakd;
    @EJB
    private DeviceFacade pdevice;
    @EJB
    private PersonFacade pPerson;

     @EJB
    private CrudService crudService;
     
     @EJB
     private MedicalPartActRelationshipFacadeLocal mrela;
     
    @WebMethod(operationName = "CreateAll")
    public void CreateAll()
    {
        CreateBasics();
        CreateDevices();
        CreateDoctorPatient();
        createAssociationActPAndDTest();
        createAssociationActPAndSandWTest();
    }
     
    @WebMethod(operationName = "CreateRoles")
    public String CreateBasics() {
        CreateRoleDoctor("Doctor");
        CreateRoleWAKD("Wearable Artificial Kidney");
        CreateRoleSmartPhone("SMARTPHONE");
        CreateRolePatient("Patient");
        return "Created Doctor,Smartphone,Patient and WAKD roles" ;
    }

    
    
    @WebMethod(operationName = "CreateAssociationActPAndDTest")
    public void createAssociationActPAndDTest() {
        List<Long> medicalEntityIDs= new ArrayList();
        medicalEntityIDs.add(new Long(3));
        medicalEntityIDs.add(new Long(4));
		AssociationAct act = new AssociationAct();
		
		act.setActStatus(2);//ActStatusEnum.SUCCESSFUL
		act.setMoodCode(4);//MoodCodeEnum.EVENT
		crudService.create(act);
		for (Long medicalEntityID : medicalEntityIDs) {
			MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
			Participation participation = new Participation();
                        Participation perparticipation=(Participation) crudService.create(participation);
			
                        MedicalPartActRelationship pEntity= new MedicalPartActRelationship();
                        pEntity.setActID(act.getId());
                        pEntity.setMedicanEntityID(medicalEntityID);
                        pEntity.setParticipationID(perparticipation.getId());
                        mrela.create(pEntity);

			
		}
    }
    
    @WebMethod(operationName = "CreateAssociationActPAndSandWTest")
    public void createAssociationActPAndSandWTest() {
        List<Long> medicalEntityIDs= new ArrayList();
        medicalEntityIDs.add(new Long(3));
        medicalEntityIDs.add(new Long(1));
        medicalEntityIDs.add(new Long(2));
        
		AssociationAct act = new AssociationAct();
		
		act.setActStatus(2);//ActStatusEnum.SUCCESSFUL
		act.setMoodCode(4);//MoodCodeEnum.EVENT
		crudService.create(act);
		for (Long medicalEntityID : medicalEntityIDs) {
			MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
                        
                        Participation participation = new Participation();
                        Participation perparticipation=(Participation) crudService.create(participation);
			
                        MedicalPartActRelationship pEntity= new MedicalPartActRelationship();
                        pEntity.setActID(act.getId());
                        pEntity.setMedicanEntityID(medicalEntityID);
                        pEntity.setParticipationID(perparticipation.getId());
                        mrela.create(pEntity);
		}
    }
    
    @WebMethod(operationName = "CreateDevices")
    public String CreateDevices() {
        CreateSmartPhone();
        CreateWakd();
        return "Created smartphone and wakd devices";
    }

    @WebMethod(operationName = "CreateDoctorPatient")
    public String CreateDoctorPatient() {
        createPatient();
        createDoctor();
        return "Created doctor and patient records";
    }

    private void createPatient() {
        Person patient = new Person();
        patient.setCode("P");
        patient.setName("Patient");
        patient.setEffectiveDate(new Date());
        List<Role> rFind;
        Role rFindone = null;
        rFind = pRole.findAll();
        for (int i = 0; i < rFind.size(); i++) {

            if (rFind.get(i).getName().equalsIgnoreCase("Patient")) {

                rFindone = rFind.get(i);
            }

        }
        patient.setAddress(new Long(1));
        patient.setContactInfo(new Long(1));
        patient.setPersonalInfo(new Long(1));
        patient.setRole(rFindone);
        pPerson.create(patient);
    }
    
    private void createDoctor() {
        Person patient = new Person();
        patient.setCode("DOC");
        patient.setName("Doctor");
        patient.setEffectiveDate(new Date());
        List<Role> rFind;
        Role rFindone = null;
        rFind = pRole.findAll();
        for (int i = 0; i < rFind.size(); i++) {

            if (rFind.get(i).getName().equalsIgnoreCase("Doctor")) {

                rFindone = rFind.get(i);
            }

        }
        patient.setAddress(new Long(2));
        patient.setContactInfo(new Long(2));
        patient.setPersonalInfo(new Long(2));
        patient.setRole(rFindone);
        pPerson.create(patient);
    }


    private void CreateWakd() {



        Device device = new Device();
        device.setCode("WAKD");
        device.setName("WAKD DEVICE");
        device.setQuantity(10);
        device.setSerialNumber("WAKD-000-000-001");
        device.setEffectiveDate(new Date());
        device.setExpirationDate(new Date());
        device.setManufacturedDate(new Date());

        List<Role> rFind;
        Role rFindone = null;
        rFind = pRole.findAll();
        for (int i = 0; i < rFind.size(); i++) {

            if (rFind.get(i).getName().equalsIgnoreCase("Wearable Artificial Kidney")) {

                rFindone = rFind.get(i);
            }

        }
        device.setRole(rFindone);
        pdevice.create(device);

    }

    private void CreateSmartPhone() {
        Device device = new Device();
        device.setCode("SP");
        device.setName("Nexus SmartPhone");
        device.setQuantity(10);
        device.setSerialNumber("SP-000-000-001");
        device.setEffectiveDate(new Date());
        device.setExpirationDate(new Date());
        device.setManufacturedDate(new Date());

        List<Role> rFind;
        Role rFindone = null;
        rFind = pRole.findAll();
        for (int i = 0; i < rFind.size(); i++) {

            if (rFind.get(i).getName().equalsIgnoreCase("SMARTPHONE")) {

                rFindone = rFind.get(i);
            }

        }
        device.setRole(rFindone);
        pdevice.create(device);

    }

    private void CreateRoleDoctor(String szName) {
        Role role = new Doctor();
        role.setCode("DOC");
        role.setName(szName);
        pRole.create(role);
    }

    private void CreateRoleWAKD(String szName) {
        Role role = new WAKD();
        role.setCode("WAKD");
        role.setName(szName);
        pRole.create(role);
    }

    private void CreateRoleSmartPhone(String szName) {
        Role role = new SmartPhone();
        role.setCode("SP");
        role.setName(szName);
        pRole.create(role);
    }

    private void CreateRolePatient(String szName) {
        Role role = new Patient();
        role.setCode("P");
        role.setName(szName);
        pRole.create(role);
    }
}
