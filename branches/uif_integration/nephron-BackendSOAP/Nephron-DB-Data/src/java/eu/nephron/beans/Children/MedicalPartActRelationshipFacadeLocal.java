/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans.Children;

import eu.nephron.model.relationships.MedicalPartActRelationship;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface MedicalPartActRelationshipFacadeLocal {

    void create(MedicalPartActRelationship medicalPartActRelationship);

    void edit(MedicalPartActRelationship medicalPartActRelationship);

    void remove(MedicalPartActRelationship medicalPartActRelationship);

    MedicalPartActRelationship find(Object id);

    List<MedicalPartActRelationship> findAll();

    List<MedicalPartActRelationship> findRange(int[] range);

    int count();
    
}
