/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.AlertFacade;
import eu.nephron.model.alert.Alert;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "AlertManager")
public class AlertManager {
    @EJB
    private AlertFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createalert")
    @Oneway
    public void createalert(@WebParam(name = "entity") Alert entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editalert")
    @Oneway
    public void editalert(@WebParam(name = "entity") Alert entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removealert")
    @Oneway
    public void removealert(@WebParam(name = "entity") Alert entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findalert")
    public Alert findalert(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllalert")
    public List<Alert> findAllalert() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<Alert> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
