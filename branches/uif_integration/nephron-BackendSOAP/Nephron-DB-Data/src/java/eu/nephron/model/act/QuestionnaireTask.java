package eu.nephron.model.act;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="QUESTIONNAIRE_TASK")
public class QuestionnaireTask  {

	private static final long serialVersionUID = 4353238063063544200L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="QUESTIONNAIRE_TASK_ID")
	private Long id;
	
	@Column(name="ANSWER")
	private String answer;
	
	@ManyToOne
	@JoinColumn(name="ACT_ID", nullable=false)
	private QuestionnaireAct act;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public QuestionnaireAct getAct() {
		return act;
	}

	public void setAct(QuestionnaireAct act) {
		this.act = act;
	}
	
}
