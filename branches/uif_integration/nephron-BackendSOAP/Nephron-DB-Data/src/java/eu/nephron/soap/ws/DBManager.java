/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.ObservationActFacade;
import eu.nephron.model.act.ObservationAct;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DBManager")
public class DBManager {
    
    @EJB
    private ObservationActFacade ejbRef;

    @WebMethod(operationName = "create")
    @Oneway
    public void createMeasurment(@WebParam(name = "entity") ObservationAct entity) {
        ejbRef.create(entity);
    }
    
    @WebMethod(operationName = "findAllMeasurements")
    public List<ObservationAct> findAllMeasurements() {
        return ejbRef.findAll();
        
    }
}
