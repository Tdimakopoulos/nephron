package eu.nephron.model.role;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Employee extends Role {

	private static final long serialVersionUID = -6902390342449171116L;

	@Column(name="JOB_CODE")
	protected String jobCode;
	
	@Column(name="VIP_CODE")
	protected String jobName;

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
}
