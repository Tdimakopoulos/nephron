package eu.nephron.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

//import org.hibernate.annotations.Parameter;
//import org.hibernate.annotations.Type;

@Entity
@Table(name="PERSONAL_INFO")
@NamedQueries({
	@NamedQuery(name="PersonalInfo.findbyID", query="select a from PersonalInfo a where a.id=:id")
})
public class PersonalInfo{
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PERSONAL_INFO_ID")
	protected Long id;

	@Column(name="TITLE")
	private String title;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="FATHER_NAME")
	private String fatherName;
	
	@Column(name="MARITAL_STATUS")
	private int maritalStatus;
	
	@Column(name="EDUCATIONAL_LEVEL")
	private int educationalLevel;

        public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
        
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public int getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(int maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public int getEducationalLevel() {
		return educationalLevel;
	}

	public void setEducationalLevel(int educationalLevel) {
		this.educationalLevel = educationalLevel;
	}

}
