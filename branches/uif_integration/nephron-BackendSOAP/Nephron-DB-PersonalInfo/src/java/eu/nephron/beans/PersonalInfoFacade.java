/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.entity.PersonalInfo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class PersonalInfoFacade extends AbstractFacade<PersonalInfo> {
    @PersistenceContext(unitName = "nephron-personal")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonalInfoFacade() {
        super(PersonalInfo.class);
    }
    
}
