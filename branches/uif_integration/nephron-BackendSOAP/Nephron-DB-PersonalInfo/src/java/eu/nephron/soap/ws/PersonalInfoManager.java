/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.PersonalInfoFacade;
import eu.nephron.model.entity.ContactInfo;
import eu.nephron.model.entity.PersonalInfo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "PersonalInfoManager")
public class PersonalInfoManager {

    @EJB
    private PersonalInfoFacade personalInfoFacade;

    @PersistenceContext 
    private EntityManager em;
    
    @WebMethod(operationName = "CreatePersonalInfo")
    public String CreatePersonalInfo(String szTitle, String szFirstname, String szLastname, String szFathername, String szMiddlename, int EducationLevel) {
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setTitle(szTitle);
        personalInfo.setFirstName(szFirstname);
        personalInfo.setLastName(szLastname);
        personalInfo.setFatherName(szFathername);
        personalInfo.setMiddleName(szMiddlename);
        personalInfo.setEducationalLevel(EducationLevel);
        personalInfoFacade.create(personalInfo);
        return personalInfo.getId().toString();
    }
    
    public List<PersonalInfo> FindPersonalInfoAll()
    {
        return personalInfoFacade.findAll();
    }
    
    public List<PersonalInfo> FindPersonalInfoByID(Long iID)
    {
        return em.createNamedQuery("PersonalInfo.findbyID").setParameter("id",iID).getResultList();
    }
    public void EditPersonalInfo(PersonalInfo Entity)
    {
        personalInfoFacade.edit(Entity);
    }
    
    public void RemovePersonalInfo(PersonalInfo Entity)
    {
        personalInfoFacade.remove(Entity);
    }
}
