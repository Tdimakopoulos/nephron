/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.AddressFacade;
import eu.nephron.beans.ContactInfoFacade;
import eu.nephron.beans.PersonalInfoFacade;
import eu.nephron.model.address.Address;
import eu.nephron.model.entity.ContactInfo;
import eu.nephron.model.entity.PersonalInfo;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "TestPDBWS")
public class TestPDBWS {
    @EJB
    private ContactInfoFacade contactInfoFacade;
    @EJB
    private AddressFacade addressFacade;
    @EJB
    private PersonalInfoFacade personalInfoFacade;

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "CreateTestDataPatientDB")
    public String CreateTestDataPDB() {
      return testCreatePatient();    
    }
    
    @WebMethod(operationName = "CreateTestDataDoctorDB")
    public String CreateTestDatadDB() {
      return testCreateDoctor();    
    }
    
    
    
   private String testCreatePatient() {
		
		PersonalInfo personalInfo = new PersonalInfo();
		personalInfo.setTitle("Mr");
		personalInfo.setFirstName("Klein");
		personalInfo.setLastName("Mein");
		personalInfo.setFatherName("Christmas");
		personalInfo.setMiddleName("NY");
		personalInfo.setEducationalLevel(1);
                personalInfoFacade.create(personalInfo);
                
		
		Address address = new Address();
		address.setCode("GR");
		address.setCity("Athens");
		address.setStreet("Kritis");
		address.setStreetNo("15");
		address.setPostCode("15121");
                address.setAdministrativeArea("A Area");
                address.setFloor("Floor 1");
                address.setName("Name");
                addressFacade.create(address);

		
		ContactInfo contactInfo =  new ContactInfo();
		contactInfo.setPhone("+302106520578");
		contactInfo.setMobile("+302106974320804");
		contactInfo.setEmail("test1@email.com");
                contactInfo.setCompanyPhone("+302106520578");
		contactInfo.setCompanyMobile("+302106974320804");
		contactInfo.setCompanyEmail("test1@email.com");
                contactInfoFacade.create(contactInfo);
		return "C "+contactInfo.getId().toString()+" - A "+address.getId().toString()+" - P "+personalInfo.getId().toString();
		
	}
	
	private String testCreateDoctor() {
		
		PersonalInfo personalInfo = new PersonalInfo();
		personalInfo.setTitle("Mrs");
		personalInfo.setFirstName("Georgia");
		personalInfo.setLastName("Kouloukianou");
		personalInfo.setFatherName("Christmas");
		personalInfo.setMiddleName("GR");
		personalInfo.setEducationalLevel(1);
                personalInfoFacade.create(personalInfo);
                
		
		Address address = new Address();
		address.setCode("GR");
		address.setCity("Athens");
		address.setStreet("Kritis");
		address.setStreetNo("15");
		address.setPostCode("15121");
                address.setAdministrativeArea("A Area");
                address.setFloor("Floor 1");
                address.setName("Name");
                addressFacade.create(address);

		
		ContactInfo contactInfo =  new ContactInfo();
		contactInfo.setPhone("+302106520578");
		contactInfo.setMobile("+302106974320804");
		contactInfo.setEmail("test1@email.com");
                contactInfo.setCompanyPhone("+302106520578");
		contactInfo.setCompanyMobile("+302106974320804");
		contactInfo.setCompanyEmail("test1@email.com");
                contactInfoFacade.create(contactInfo);
		return "C "+contactInfo.getId().toString()+" - A "+address.getId().toString()+" - P "+personalInfo.getId().toString();
		
	}
	
}
