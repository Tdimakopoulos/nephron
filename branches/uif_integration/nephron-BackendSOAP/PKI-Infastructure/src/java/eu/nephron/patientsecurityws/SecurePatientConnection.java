/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientsecurityws;

import eu.nephron.entity.patientconnections;
import eu.nephron.role.bean.patientconnectionsFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "SecurePatientConnection")
public class SecurePatientConnection {
    @EJB
    private patientconnectionsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "patientconnections") patientconnections patientconnections) {
        ejbRef.create(patientconnections);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "patientconnections") patientconnections patientconnections) {
        ejbRef.edit(patientconnections);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "patientconnections") patientconnections patientconnections) {
        ejbRef.remove(patientconnections);
    }

    @WebMethod(operationName = "find")
    public patientconnections find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<patientconnections> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<patientconnections> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
