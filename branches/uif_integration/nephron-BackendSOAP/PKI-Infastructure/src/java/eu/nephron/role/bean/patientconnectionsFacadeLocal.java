/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.role.bean;

import eu.nephron.entity.patientconnections;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface patientconnectionsFacadeLocal {

    void create(patientconnections patientconnections);

    void edit(patientconnections patientconnections);

    void remove(patientconnections patientconnections);

    patientconnections find(Object id);

    List<patientconnections> findAll();

    List<patientconnections> findRange(int[] range);

    int count();
    
}
