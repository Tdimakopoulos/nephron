/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.role.bean;

import eu.nephron.entity.RoleDB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class RoleDBFacade extends AbstractFacade<RoleDB> implements RoleDBFacadeLocal {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RoleDBFacade() {
        super(RoleDB.class);
    }
    
}
