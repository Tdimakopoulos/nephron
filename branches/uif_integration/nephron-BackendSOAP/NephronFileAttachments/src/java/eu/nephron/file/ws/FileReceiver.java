/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.file.ws;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebServiceRef;


/**
 *
 * @author tdim
 */
@WebService(serviceName = "NephronFileReceiverWS")
public class FileReceiver {

    

    /**
     * Web service operation
     * 
     *     Sample client code for WS
     *       File pdfFile=null;  
     *       javax.activation.FileDataSource fds=new javax.activation.FileDataSource(pdfFile);  
     *       javax.activation.DataHandler dh=new javax.activation.DataHandler(fds);  
     *       ws.call.ReceiveFile(handler,"file.pdf"); 
     */
    @WebMethod(operationName = "ReceiveFile")
    public String ReceiveFile(@WebParam(name = "fileobject") DataHandler fileobject,@WebParam(name = "filename") String filename) throws ClassNotFoundException {
        java.io.InputStream io = null;
        try {
            javax.activation.DataSource ds=fileobject.getDataSource();
            io = ds.getInputStream();
            java.io.ObjectInputStream ois=new java.io.ObjectInputStream(io);
            java.io.FileOutputStream fos = new java.io.FileOutputStream(filename);
            java.io.ObjectOutputStream oos = new java.io.ObjectOutputStream(fos);
            oos.writeObject(ois.readObject());
            return "File Saved.";
        } catch (IOException ex) {
            return ex.getMessage();
        } finally {
            try {
                io.close();
            } catch (IOException ex) {
                return ex.getMessage();
            }
        }
    }

  
    
    
}
