package eu.nephron.wakd.api.enums;

public enum WakdCommandEnum {
	
	// Generic commands/data relevant for all Who.
	UNDEFINED((byte)-1),
	UNDEFINED_COMMAND((byte)0),
	RESET((byte)1),			// Recipient should reset itself no data attached (refer to strMessageOnly)
	SHUTDOWN((byte)2),
	SET_TIME((byte)3),		// Recipient should set Unix time according to attached data (refer to tdsetTime)
	CURRENT_STATE((byte)4),	// Recipient is informed about the current state of WAKD
	ACK((byte)5),
	NACK((byte)6),
	STATUS_REQUEST((byte)7),
	SYSTEM_INFO((byte)8),
	
	// Commands/data relevant for RTB.
	// Configures actuator values for the states implemented in the RTB
	CONFIGURE_STATE((byte)9),					// For RTB only: set WAKD actuators in state accroding to attached data (refer to tdActCtrl)
	CONFIGURE_PERIOD_POLARIZER_TOGGLE((byte)10),	// For RTB only: tdMsgPeriodPolarizerToggle
		
	// Command id that triggers to switch into another state. The RTB will automatically apply
	// the preconfigured actuator values (see above) whenever it changes into another state.
	CHANGE_STATE_FROM_TO((byte)11),		// Request a state change from State A to B. (refer to tdWakdStateFromTo)
	
	// Command ids relevant for exchange of sensor data from RTB
	PHYSIOLOGICAL_DATA((byte)12),			// Recipient gets all physiological data in one set. (refer to tdPhysiologicalData)
	PHYSICAL_DATA((byte)13),				// Recipient gets all physical data in one set. (refer to tdPhysicalData)
	ALARM_RTB((byte)14),					// Recipient is informed about alarm status from RTB. (refer to tdAlarms)
	
	// commands/data relevant for CB
	WEIGHT_REQUEST((byte)15),		// MessageOnly command to ask CB to connect to weight scale and receive new value.
	WEIGHT_DATA((byte)16),		// Recipient gets weight measurement. (refer to tdWeightData)
	BP_REQUEST((byte)17),  		// SHOULD BE OIN ONE BLOCK
	BP_DATA((byte)18),
	ECG_REQUEST((byte)19),
	ECG_DATA((byte)20);
	
	public static WakdCommandEnum getWakdCommandEnum(byte value) {
		switch (value) {
		case (byte)0:
			return UNDEFINED_COMMAND;
		case (byte)1:
			return RESET;
		case (byte)2:
			return SHUTDOWN;
		case (byte)3:
			return SET_TIME;
		case (byte)4:
			return CURRENT_STATE;
		case (byte)5:
			return ACK;
		case (byte)6:
			return NACK;
		case (byte)7:
			return STATUS_REQUEST;
		case (byte)8:
			return SYSTEM_INFO;
		case (byte)9:
			return CONFIGURE_STATE;
		case (byte)10:
			return CONFIGURE_PERIOD_POLARIZER_TOGGLE;
		case (byte)11:
			return CHANGE_STATE_FROM_TO;
		case (byte)12:
			return PHYSIOLOGICAL_DATA;
		case (byte)13:
			return PHYSICAL_DATA;
		case (byte)14:
			return ALARM_RTB;
		case (byte)15:
			return WEIGHT_REQUEST;
		case (byte)16:
			return WEIGHT_DATA;
		case (byte)17:
			return BP_REQUEST;
		case (byte)18:
			return BP_DATA;
		case (byte)19:
			return ECG_REQUEST;
		case (byte)20:
			return ECG_DATA;
		default:
			return UNDEFINED;
		}
	}
	
	private byte value;

	private WakdCommandEnum(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
	
}
