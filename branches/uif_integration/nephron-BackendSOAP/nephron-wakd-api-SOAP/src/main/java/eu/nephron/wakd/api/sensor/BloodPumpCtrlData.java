package eu.nephron.wakd.api.sensor;


public class BloodPumpCtrlData extends PumpCtrlData {

	private static final long serialVersionUID = 2466740579129572433L;

	public BloodPumpCtrlData(PumpDirectionEnum direction, int flowReference) {
		super(direction, flowReference);
	}
}
