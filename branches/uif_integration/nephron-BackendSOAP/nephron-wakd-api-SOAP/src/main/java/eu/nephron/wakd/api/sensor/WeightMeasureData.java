package eu.nephron.wakd.api.sensor;

import eu.nephron.utils.ByteUtils;

public class WeightMeasureData extends SensorData {

	private static final long serialVersionUID = -8687074508844048925L;

	private int weight;
	
	private int bodyFat;
	
	private int batteryLevel;
	
	private int status;

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getBodyFat() {
		return bodyFat;
	}

	public void setBodyFat(int bodyFat) {
		this.bodyFat = bodyFat;
	}

	public int getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(int batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = new byte[9];
		System.arraycopy(ByteUtils.intToByteArray(this.weight, 2), 0, bytes, 0, 2);
		bytes[2] = ByteUtils.intToByte(this.bodyFat);
		System.arraycopy(ByteUtils.intToByteArray(this.batteryLevel, 2), 0, bytes, 3, 2);
		System.arraycopy(ByteUtils.intToByteArray(this.status, 4), 0, bytes, 5, 4);
		return bytes;
	}
	
}
