package eu.nephron.wakd.api.outgoing;

import java.util.Arrays;

import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;


public class PeriodPolarizerToggleMsg extends OutgoingWakdMsg {
	
	private static final long serialVersionUID = -7095232249637492065L;
	
	private int togglePeriod;
	
	public PeriodPolarizerToggleMsg(int id, int togglePeriod) {
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.CONFIGURE_PERIOD_POLARIZER_TOGGLE);
		this.header.setSize(7);
		this.togglePeriod = togglePeriod;		
	}

	public int getTogglePeriod() {
		return togglePeriod;
	}

	public void setTogglePeriod(int togglePeriod) {
		this.togglePeriod = togglePeriod;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+1);
		bytes[6] = ByteUtils.intToByte(this.togglePeriod);
		return bytes;
	}
	
}
