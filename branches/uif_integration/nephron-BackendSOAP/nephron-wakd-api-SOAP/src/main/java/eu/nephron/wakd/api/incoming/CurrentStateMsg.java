package eu.nephron.wakd.api.incoming;

import java.util.Arrays;

import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;


public class CurrentStateMsg extends IncomingWakdMsg {

	private static final long serialVersionUID = -8522286386866209503L;
	
	public CurrentStateMsg() {
		super();
	}
	
	public CurrentStateMsg(int id, WakdStateEnum currentState, WakdOpStateEnum currentOpState) {
		super();
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.CURRENT_STATE);
		this.header.setSize(8);
		this.currentState = currentState;
		this.currentOpState = currentOpState;
	}

	private WakdStateEnum currentState;
	
	private WakdOpStateEnum currentOpState;

	public WakdStateEnum getCurrentState() {
		return currentState;
	}

	public void setCurrentState(WakdStateEnum currentState) {
		this.currentState = currentState;
	}

	public WakdOpStateEnum getCurrentOpState() {
		return currentOpState;
	}

	public void setCurrentOpState(WakdOpStateEnum currentOpState) {
		this.currentOpState = currentOpState;
	}
	
	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+2);
		bytes[6] = currentState.getIdentifier();
		bytes[7] = currentOpState.getIdentifier();
		return bytes;
	}

	@Override
	public void decode(byte[] bytes) {
		this.header.decode(bytes);
		this.currentState = WakdStateEnum.getWakdStateEnum(bytes[6]);
		this.currentOpState = WakdOpStateEnum.getWakdOpStateEnum(bytes[7]);
	}

}
