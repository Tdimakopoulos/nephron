/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.localimp;

import eu.nephron.dss.db.DSSRuleResults;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface DSSRuleResultsFacadeLocal {

    void create(DSSRuleResults dSSRuleResults);

    void edit(DSSRuleResults dSSRuleResults);

    void remove(DSSRuleResults dSSRuleResults);

    DSSRuleResults find(Object id);

    List<DSSRuleResults> findAll();

    List<DSSRuleResults> findRange(int[] range);

    int count();
    
}
