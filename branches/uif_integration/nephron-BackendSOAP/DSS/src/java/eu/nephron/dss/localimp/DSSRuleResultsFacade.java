/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.localimp;

import eu.nephron.dss.db.DSSRuleResults;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class DSSRuleResultsFacade extends AbstractFacade<DSSRuleResults> implements DSSRuleResultsFacadeLocal {
    @PersistenceContext(unitName = "DSSPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DSSRuleResultsFacade() {
        super(DSSRuleResults.class);
    }
    
}
