package eu.nephron.df.rules.profile;

import eu.nephron.df.eventhandler.bean.DSSEventHandlerBean.RuleEngineType;

public class ProfileManager {

	public ProfileData GetProfileNameForWAKDMeasurments() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("WAKDMeasurments");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);

		return pReturn;
	}
	
	public String GetPackageNameForNephron() {
		
		return "Nephron";
	}
}
