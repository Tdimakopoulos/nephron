/**
 * DroolSettings
 * 
 * This Java class store all settings need by the project to access the drool repository
 * and identify the temporary local repository using a file path 
 *
 * @Project   esponder
 * @package   Datafusion
 */
package eu.nephron.df.ruleengine.settings;

import java.io.File;

import javax.naming.NamingException;



/**
 * @author tdim
 * 
 */
public class DroolSettings {

	private String szinitialPath = "c:\\tmprepo\\";
	
	// Variable to store the local repository (file path)
	private String szTmpRepositoryDirectoryPath = "c:\\tmprepo\\";
        
	// Drool server URL
	private String szDroolApplicationServerURL = "http://localhost:8181/";
	
        // Drool deployment name
	private String szDroolWarDeploymentPath = "guvnor";
        
	// Service entry point
	private String szDroolServiceEntryPoint = "rest";
        
	// Drool username
	private String szDroolUsername = "guest";
        
	// Drool password
	private String szDroolPassword = "";
        
	// Reset repo
	private String szDroolRepo = "local";

	public DroolSettings() {
	
	}

	public DroolSettings(boolean bLoad) {
		if (bLoad) {
			
		}
	}

	public void LoadAllOptions()  {
		//load settings from DB Soap now

	}

	private boolean isUnix() {
		if (File.separatorChar == '/')
			return true;
		else
			return false;
	}

    /**
     * @return the szinitialPath
     */
    public String getSzinitialPath() {
        return szinitialPath;
    }

    /**
     * @param szinitialPath the szinitialPath to set
     */
    public void setSzinitialPath(String szinitialPath) {
        this.szinitialPath = szinitialPath;
    }

    /**
     * @return the szTmpRepositoryDirectoryPath
     */
    public String getSzTmpRepositoryDirectoryPath() {
        return szTmpRepositoryDirectoryPath;
    }

    /**
     * @param szTmpRepositoryDirectoryPath the szTmpRepositoryDirectoryPath to set
     */
    public void setSzTmpRepositoryDirectoryPath(String szTmpRepositoryDirectoryPath) {
        this.szTmpRepositoryDirectoryPath = szTmpRepositoryDirectoryPath;
    }

    /**
     * @return the szDroolApplicationServerURL
     */
    public String getSzDroolApplicationServerURL() {
        return szDroolApplicationServerURL;
    }

    /**
     * @param szDroolApplicationServerURL the szDroolApplicationServerURL to set
     */
    public void setSzDroolApplicationServerURL(String szDroolApplicationServerURL) {
        this.szDroolApplicationServerURL = szDroolApplicationServerURL;
    }

    /**
     * @return the szDroolWarDeploymentPath
     */
    public String getSzDroolWarDeploymentPath() {
        return szDroolWarDeploymentPath;
    }

    /**
     * @param szDroolWarDeploymentPath the szDroolWarDeploymentPath to set
     */
    public void setSzDroolWarDeploymentPath(String szDroolWarDeploymentPath) {
        this.szDroolWarDeploymentPath = szDroolWarDeploymentPath;
    }

    /**
     * @return the szDroolServiceEntryPoint
     */
    public String getSzDroolServiceEntryPoint() {
        return szDroolServiceEntryPoint;
    }

    /**
     * @param szDroolServiceEntryPoint the szDroolServiceEntryPoint to set
     */
    public void setSzDroolServiceEntryPoint(String szDroolServiceEntryPoint) {
        this.szDroolServiceEntryPoint = szDroolServiceEntryPoint;
    }

    /**
     * @return the szDroolUsername
     */
    public String getSzDroolUsername() {
        return szDroolUsername;
    }

    /**
     * @param szDroolUsername the szDroolUsername to set
     */
    public void setSzDroolUsername(String szDroolUsername) {
        this.szDroolUsername = szDroolUsername;
    }

    /**
     * @return the szDroolPassword
     */
    public String getSzDroolPassword() {
        return szDroolPassword;
    }

    /**
     * @param szDroolPassword the szDroolPassword to set
     */
    public void setSzDroolPassword(String szDroolPassword) {
        this.szDroolPassword = szDroolPassword;
    }

    /**
     * @return the szDroolRepo
     */
    public String getSzDroolRepo() {
        return szDroolRepo;
    }

    /**
     * @param szDroolRepo the szDroolRepo to set
     */
    public void setSzDroolRepo(String szDroolRepo) {
        this.szDroolRepo = szDroolRepo;
    }
	
	
}
