package eu.nephron.df.eventhandler.bean;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import eu.nephron.df.ruleengine.core.DomainSpecificKnowledgeParser;
import eu.nephron.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.nephron.df.ruleengine.utilities.RuleStatistics;
import eu.nephron.df.ruleengine.utilities.RuleUtilities;





public class DSSEventHandlerBean  {

	private String szFlowName=null;
	private String szRulePackageName=null;
	private String szDSLName=null;
	@SuppressWarnings("unused")
	private String szDSLRName=null;
	private RuleEngineType dInferenceType=RuleEngineType.NONE;
	
	
	public enum RuleEngineType {
		NONE,DRL_RULES, DSL_RULES, FLOW_RULES, ALL_PACKAGE, MIX
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Rule Engine Calls and Variables
	private RuleEngineGuvnorAssets dEngine = new RuleEngineGuvnorAssets();
	private DomainSpecificKnowledgeParser ddsl = new DomainSpecificKnowledgeParser();

	public void ReinitializeEngine()
	{
		dEngine = new RuleEngineGuvnorAssets();
		ddsl = new DomainSpecificKnowledgeParser();
	}
	public void AddDTOObjects(Object fact) {
		//System.out.println("Adding DTO Objetc");
		dEngine.InferenceEngineAddDTOObject(fact);
		ddsl.AddDTOObject(fact);
	}

	public void AddObjects(Object fact) {
		dEngine.InferenceEngineAddObject(fact);
		ddsl.AddObject(fact);
	}

	private void AddKnowledgeByCategory(String szCategory) {
		try {
			dEngine.InferenceEngineAddKnowledgeForCategory(szCategory);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Category");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForCategory(szCategory);
	}

	private void AddKnowledgeByPackage(String szPackage) {
		try {
			dEngine.InferenceEngineAddKnowledgeForPackage(szPackage);
		} catch (Exception e) {
			System.out.println("Error on add knowledge by Package");
			e.printStackTrace();
		}
		ddsl.AddKnowledgeForPackage(szPackage);
	}

	private void ExecuteRules() {
		// run simple drl rules
		dEngine.InferenceEngineRunAssets();

	}

	private void ExecuteFlow() {
		// run all processes
		ddsl.ExecuteFlow();

		// run dslr rules based on dsl and
		ddsl.RunRules();

		// close session only if all processes has finished
		ddsl.Dispose();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void LoadKnowledge()
	{
		//System.out.println("Load Knowledge Start -- Type :"+RuleEngineType.ALL_PACKAGE+" Name : "+szRulePackageName);
		if(this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			//System.out.println("Load Package : "+szRulePackageName);
			AddKnowledgeByPackage(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			//System.out.println("Load Category : "+szRulePackageName);
			AddKnowledgeByCategory(szRulePackageName);
		}

		if(this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			AddKnowledgeByCategory(szFlowName);
		}

		if(this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			AddKnowledgeByCategory(szDSLName);
		}


	}

	public void ProcessRules()
	{
		////////////////////////////////////////////////////////////////////////
		//Load Default Objects into drools
		// RuleUtils
		// MathUtils
		RuleStatistics pStatistics = new RuleStatistics();
		RuleUtilities pUtils = new RuleUtilities();
		
		AddObjects(pStatistics);
		AddObjects(pUtils);
		////////////////////////////////////////////////////////////////////////

		//Execute Rules using package, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.ALL_PACKAGE)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using Mix, DRL,DSL,DSLR,Flow
		if (this.dInferenceType==RuleEngineType.MIX)
		{
			ExecuteFlow();
			ExecuteRules();
		}

		//Execute Rules using  DRL
		if (this.dInferenceType==RuleEngineType.DRL_RULES)
		{
			ExecuteRules();
		}

		//Execute Rules using  DSL
		if (this.dInferenceType==RuleEngineType.DSL_RULES)
		{
			ExecuteFlow();
		}

		//Execute Rules using  Flow or/and DRSR
		if (this.dInferenceType==RuleEngineType.FLOW_RULES)
		{
			ExecuteFlow();
		}
	}

	

	



	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName)
	{
		SetRuleType(dType,szPackageNameOrFlowName,null,null);
	}

	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName)
	{
		SetRuleType(dType,null,szDSLName,szDSLRName);
	}

	private void SetRuleType(RuleEngineType dType,String szPackageNameOrFlowName,String szDSLNamep,String szDSLRNamep)
	{
		if (dType==RuleEngineType.DRL_RULES)
		{
			szRulePackageName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if (dType==RuleEngineType.ALL_PACKAGE)
		{
			szRulePackageName=szPackageNameOrFlowName;
			dInferenceType=RuleEngineType.ALL_PACKAGE;
		}
		if (dType==RuleEngineType.FLOW_RULES)
		{
			szFlowName=szPackageNameOrFlowName;
			SetInferenceType(dType);
		}
		if(dType==RuleEngineType.DSL_RULES)
		{
			szDSLName=szDSLNamep;
			szDSLRName=szDSLRNamep;
			SetInferenceType(dType);
		}
	}

	private void SetInferenceType(RuleEngineType dType)
	{
		if (dInferenceType==RuleEngineType.ALL_PACKAGE)
		{}else{
			if (dInferenceType==RuleEngineType.NONE)
				dInferenceType=dType;
			else
				dInferenceType=RuleEngineType.MIX;
		}
	}
}

