/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.ws;

import eu.nephron.dss.db.DSSRuleResults;
import eu.nephron.dss.localimp.DSSRuleResultsFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DSSWSManager")
public class DSSWSManager {
    @EJB
    private DSSRuleResultsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "dSSRuleResults") DSSRuleResults dSSRuleResults) {
        ejbRef.create(dSSRuleResults);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "dSSRuleResults") DSSRuleResults dSSRuleResults) {
        ejbRef.edit(dSSRuleResults);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "dSSRuleResults") DSSRuleResults dSSRuleResults) {
        ejbRef.remove(dSSRuleResults);
    }

    @WebMethod(operationName = "find")
    public DSSRuleResults find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<DSSRuleResults> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<DSSRuleResults> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
