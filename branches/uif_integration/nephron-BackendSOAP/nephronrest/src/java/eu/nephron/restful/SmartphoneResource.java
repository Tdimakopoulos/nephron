/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.restful;

import eu.nephron.phonemanager.soap.ManageShutdownMessages;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author tdim
 */
@Path("smartphone")
public class SmartphoneResource {

    /**
     * Creates a new instance of SmartphoneResource
     */
    public SmartphoneResource() {
    }

    /**
     * Retrieves representation of an instance of
     * eu.nephron.restful.SmartphoneResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("Checkshutdown")
    public String checkshutdown(@QueryParam("imei") String imei) {
        java.util.List<eu.nephron.phonemanager.soap.ManageShutdownMessages> pfind = spSfindAll();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getSzTextfield1() != null) {
                if (pfind.get(i).getSzTextfield1().equalsIgnoreCase(imei)) {
                    if (pfind.get(i).getIcommandID() == 0) {
                        if (pfind.get(i).getIACK() == 0) {
                            return "YES";
                        }
                    }
                }
            }
        }
        return "NO";
    }
    
    @GET
    @Path("Updateshutdownack")
    public String Updateshutdownack(@QueryParam("imei") String imei) {
        
        spsUpdateShutdownACK(imei);
        return "UPDATED";
    }

    private static java.util.List<eu.nephron.phonemanager.soap.ManageShutdownMessages> spSfindAll() {
        eu.nephron.phonemanager.soap.ManagePhoneMessages_Service service = new eu.nephron.phonemanager.soap.ManagePhoneMessages_Service();
        eu.nephron.phonemanager.soap.ManagePhoneMessages port = service.getManagePhoneMessagesPort();
        return port.spSfindAll();
    }

    private static void spsUpdateShutdownACK(java.lang.String imei) {
        eu.nephron.phonemanager.soap.ManagePhoneMessages_Service service = new eu.nephron.phonemanager.soap.ManagePhoneMessages_Service();
        eu.nephron.phonemanager.soap.ManagePhoneMessages port = service.getManagePhoneMessagesPort();
        port.spsUpdateShutdownACK(imei);
    }
}
