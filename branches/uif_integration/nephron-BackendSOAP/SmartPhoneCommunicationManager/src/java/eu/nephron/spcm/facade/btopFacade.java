/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.facade;

import eu.nephron.spcm.db.btop;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class btopFacade extends AbstractFacade<btop> implements btopFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public btopFacade() {
        super(btop.class);
    }
    
}
