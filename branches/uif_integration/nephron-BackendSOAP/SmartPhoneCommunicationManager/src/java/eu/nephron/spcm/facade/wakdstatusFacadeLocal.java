/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.facade;

import eu.nephron.spcm.db.wakdstatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface wakdstatusFacadeLocal {

    void create(wakdstatus wakdstatus);

    void edit(wakdstatus wakdstatus);

    void remove(wakdstatus wakdstatus);

    wakdstatus find(Object id);

    List<wakdstatus> findAll();

    List<wakdstatus> findRange(int[] range);

    int count();
    
}
