/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.webservices;

import eu.nephron.spcm.db.btop;
import eu.nephron.spcm.db.ptob;
import eu.nephron.spcm.db.wakdstatus;
import eu.nephron.spcm.facade.btopFacadeLocal;
import eu.nephron.spcm.facade.ptobFacadeLocal;
import eu.nephron.spcm.facade.wakdstatusFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "CommunicationManager")
public class CommunicationManager {

    @EJB
    private btopFacadeLocal ejbbtop;
    @EJB
    private ptobFacadeLocal ejbptob;
    @EJB
    private wakdstatusFacadeLocal ejbwakds;

    public void StoreWAKD(String IMEI, String WAKDStatus, Long Dater, String txt1, String txt2, String txt3, String txt4) {
        wakdstatus pEntity = new wakdstatus();
        List<wakdstatus> pList = ejbwakds.findAll();

        //if we have entry for IMEI update
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getSzIMEI().equalsIgnoreCase(IMEI)) {
                pEntity = pList.get(i);
                pEntity.setDdatereceive(Dater);
                pEntity.setSzWAKDStatus(WAKDStatus);
                ejbwakds.edit(pEntity);
                return;
            }
        }
        
        //No Entry find then add
        pEntity.setDdatereceive(Dater);
        pEntity.setSzIMEI(IMEI);
        pEntity.setSzWAKDStatus(WAKDStatus);
        pEntity.setSztextfield1(txt1);
        pEntity.setSztextfield2(txt2);
        pEntity.setSztextfield3(txt3);
        pEntity.setSztextfield4(txt4);
        ejbwakds.create(pEntity);
    }

    public void StoreCommand(String IMEI, String Command, Long Dater, String txt1, String txt2, String txt3, String txt4) {
        ptob pEntity = new ptob();
        pEntity.setDatereceive(Dater);
        pEntity.setIMEI(IMEI);
        pEntity.setCommand(Command);
        pEntity.setTextfield1(txt1);
        pEntity.setTextfield2(txt2);
        pEntity.setTextfield3(txt3);
        pEntity.setTextfield4(txt4);
        ejbptob.create(pEntity);
    }

    public String RetrieveCommand(String IMEI) {
        String Command = "-1:No Command";
        List<btop> pList = ejbbtop.findAll();
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getIMEI().equalsIgnoreCase(IMEI)) {
                if (!pList.get(i).isACK()) {
                    Command = pList.get(i).getId() + ":" + pList.get(i).getCommand();
                    return Command;
                }
            }
        }
        return Command;
    }

    public void MarkACK(Long CommandID) {
        List<btop> pList = ejbbtop.findAll();
        System.out.println("Look for : "+CommandID);
        for (int i = 0; i < pList.size(); i++) {
            System.out.println("ID:"+pList.get(i).getId());
            if (pList.get(i).getId().toString().equalsIgnoreCase(CommandID.toString()) ) {
                System.out.println("Found do update");
                btop pElement = pList.get(i);
                pElement.setACK(true);
                ejbbtop.edit(pElement);
                return;
            }
        }
    }
}
