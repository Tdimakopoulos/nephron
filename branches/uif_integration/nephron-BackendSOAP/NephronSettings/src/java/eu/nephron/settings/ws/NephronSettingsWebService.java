/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.settings.ws;

import eu.nephron.settings.beans.nephronsettingsFacadeLocal;
import eu.nephron.settings.entity.nephronsettings;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "NephronSettings")
@Stateless
public class NephronSettingsWebService {
    @EJB
    private nephronsettingsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "nephronsettingsentity") nephronsettings nephronsettings) {
        ejbRef.create(nephronsettings);
    }

    @WebMethod(operationName = "createValues")
    @Oneway
    public void createValues(String group,String name,String value) {
        nephronsettings Nephronsettings=new nephronsettings();
        Nephronsettings.setSzgroup(group);
        Nephronsettings.setSzname(name);
        Nephronsettings.setValue(value);
        ejbRef.create(Nephronsettings);
    }
    
    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "nephronsettingsedit") nephronsettings nephronsettings) {
        ejbRef.edit(nephronsettings);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "nephronsettingsremove") nephronsettings nephronsettings) {
        ejbRef.remove(nephronsettings);
    }

    @WebMethod(operationName = "find")
    public nephronsettings find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<nephronsettings> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<nephronsettings> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
