/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.settings.beans;

import eu.nephron.settings.entity.nephronsettings;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface nephronsettingsFacadeLocal {

    void create(nephronsettings nephronsettings);

    void edit(nephronsettings nephronsettings);

    void remove(nephronsettings nephronsettings);

    nephronsettings find(Object id);

    List<nephronsettings> findAll();

    List<nephronsettings> findRange(int[] range);

    int count();
    
}
