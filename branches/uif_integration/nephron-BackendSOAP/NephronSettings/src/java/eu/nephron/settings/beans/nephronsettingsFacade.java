/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.settings.beans;

import eu.nephron.settings.entity.nephronsettings;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class nephronsettingsFacade extends AbstractFacade<nephronsettings> implements nephronsettingsFacadeLocal {
    @PersistenceContext(unitName = "NephronSettingsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public nephronsettingsFacade() {
        super(nephronsettings.class);
    }
    
}
