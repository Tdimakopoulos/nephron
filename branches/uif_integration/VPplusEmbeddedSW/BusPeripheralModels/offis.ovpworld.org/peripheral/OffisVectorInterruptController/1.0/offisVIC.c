/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "offisVIC.h"

////////////////////// Declaration of peripheral wide data structures ///////////////////////////////

OFFIS_VIC_RegisterT OFFIS_VIC_SlavePort; // structure with all public slave port registers
handles_type handles;                    // structure of handles to ports and nets





//////////////////////////////// Bus Slave Ports ///////////////////////////////

// for the LPC210x the VIC reaches from 0xffff f000 to 0xffff f23f
static void installSlavePorts(void) {
  handles.OFFIS_VIC_SlavePort = ppmCreateSlaveBusPort("sp", 576);
}

/////////////////////////////////// Net Ports //////////////////////////////////

static void installNetPorts(void) {
// To write to this net, use ppmWriteNet(handles.IRQ*, value);
    handles.IRQo  = ppmOpenNetPort("IRQo");
    handles.IRQi0 = ppmOpenNetPort("IRQi0");
    handles.IRQi1 = ppmOpenNetPort("IRQi1");
    handles.IRQi2 = ppmOpenNetPort("IRQi2");
    handles.IRQi3 = ppmOpenNetPort("IRQi3");
    handles.IRQi4 = ppmOpenNetPort("IRQi4");
    handles.IRQi5 = ppmOpenNetPort("IRQi5");
}

// WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// This is a quick and dirty implementation of the IRQ Vector Controller!
// In the previous version it was possible that IRQs get lost if the come
// very close together. Ruffly that should have been fixed now. Still I cannot
// guarantee that this VIC behaves as you would expect it from a "regular" VIC.

Uns32 QueueVectAddr[6] = {0,0,0,0,0,0};

static void IRQi0EventHandler(ppmNetValue IRQi0, void *user) {
	if (IRQi0 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// This would be the Tick-Scheduler-IRQ in our Nephron+ VP
			// bhmMessage("I", PREFIX, "Sending IRQ0 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr0.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ0.");
			QueueVectAddr[0] = OFFIS_VIC_SlavePort.VICVectAddr0.value;
		}
	}
}
static void IRQi1EventHandler(ppmNetValue IRQi1, void *user) {
	if (IRQi1 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// bhmMessage("I", PREFIX, "Sending IRQ1 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr1.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ1.");
			QueueVectAddr[1] = OFFIS_VIC_SlavePort.VICVectAddr1.value;
		}
	}
}
static void IRQi2EventHandler(ppmNetValue IRQi2, void *user) {
	if (IRQi2 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// bhmMessage("I", PREFIX, "Sending IRQ2 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr2.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ2.");
			QueueVectAddr[2] = OFFIS_VIC_SlavePort.VICVectAddr2.value;
		}
	}
}
static void IRQi3EventHandler(ppmNetValue IRQi3, void *user) {
	if (IRQi3 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// bhmMessage("I", PREFIX, "Sending IRQ3 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr3.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ03.");
			QueueVectAddr[3] = OFFIS_VIC_SlavePort.VICVectAddr3.value;
		}
	}
}
static void IRQi4EventHandler(ppmNetValue IRQi4, void *user) {
	if (IRQi4 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// bhmMessage("I", PREFIX, "Sending IRQ4 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr4.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ4.");
			QueueVectAddr[4] = OFFIS_VIC_SlavePort.VICVectAddr4.value;
		}
	}
}
static void IRQi5EventHandler(ppmNetValue IRQi5, void *user) {
	if (IRQi5 == 1) {
		if (ppmReadNet(handles.IRQo) == 0) {
			// No other IRQ active. But this one right through.
			// bhmMessage("I", PREFIX, "Sending IRQ4 to uC.");
			ppmWriteNet(handles.IRQo, 1);
			OFFIS_VIC_SlavePort.VICVectAddr.value = OFFIS_VIC_SlavePort.VICVectAddr5.value;
		} else {
			// Another IRQ is already active. Remember this one for later.
			bhmMessage("I", PREFIX, "Other IRQ active. Queueing IRQ5.");
			QueueVectAddr[5] = OFFIS_VIC_SlavePort.VICVectAddr5.value;
		}
	}
}

///////////////////////////// MMR Generic callbacks ////////////////////////////

static PPM_VIEW_CB(view32) {  *(Uns32*)data = *(Uns32*)user; }

PPM_REG_WRITE_CB(wcb) {
  *(Uns32*)user = data;
}

PPM_REG_WRITE_CB(wcb_VICVectAddr) {
	// Writing to this register does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to update
	// the priority hardware.
	// OFFIS: right now we just implemented on IRQi, so no other IRQ possible.
	if (QueueVectAddr[0] != 0) {
		bhmMessage("I", PREFIX, "IRQ0 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[0];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[0] = 0;
	} else if (QueueVectAddr[1] != 0) {
		bhmMessage("I", PREFIX, "IRQ1 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[1];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[1] = 0;
	} else if (QueueVectAddr[2] != 0) {
		bhmMessage("I", PREFIX, "IRQ2 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[2];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[2] = 0;
	} else if (QueueVectAddr[3] != 0) {
		bhmMessage("I", PREFIX, "IRQ3 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[3];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[3] = 0;
	} else if (QueueVectAddr[4] != 0) {
		bhmMessage("I", PREFIX, "IRQ4 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[4];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[4] = 0;
	} else if (QueueVectAddr[5] != 0) {
		bhmMessage("I", PREFIX, "IRQ5 was pending, now putting it through.");
		OFFIS_VIC_SlavePort.VICVectAddr.value = QueueVectAddr[5];
		ppmWriteNet(handles.IRQo, 1);
		// Delete from queue.
		QueueVectAddr[5] = 0;
	} else {  
		ppmWriteNet(handles.IRQo, 0);
		*(Uns32*)user = data;
	}
}

// PPM_REG_WRITE_CB(wcb_control_register) {
//   *(Uns32*)user = data;
//   OFFIS_VIC_SlavePort.count.value++;
// }

PPM_REG_READ_CB(rcb) {
    // YOUR CODE HERE (regRd32)
    return *(Uns32*)user;
}

PPM_CONSTRUCTOR_CB(periphConstructor);

PPM_CONSTRUCTOR_CB(constructor) {
  // YOUR CODE HERE (pre constructor)
  periphConstructor();
  // YOUR CODE HERE (post constructor)
}

PPM_DESTRUCTOR_CB(destructor) {
    // YOUR CODE HERE (destructor)
}





//////////////////////////// Memory mapped registers ///////////////////////////

static void installRegisters(void) {

  ppmCreateRegister("VICIRQStatus",                            // name
		    "VICIRQStatus",                            // description
		    handles.OFFIS_VIC_SlavePort,               // WINDOW base
		    0,                                         // port offset in bytes
		    4,                                         // register size in bytes
		    rcb,                                       // Read call back function
		    wcb,                                       // write call back function
		    view32,                                    // debug view ????
		    &(OFFIS_VIC_SlavePort.VICIRQStatus.value), // register in my data structure
		    True
		    );
  ppmCreateRegister("VICFIQStatus",
		    "VICFIQStatus",
		    handles.OFFIS_VIC_SlavePort,
		    4,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICFIQStatus.value),
		    True
		    );
  ppmCreateRegister("VICRawIntr",
		    "VICRawIntr",
		    handles.OFFIS_VIC_SlavePort,
		    8,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICRawIntr.value),
		    True
		    );
  ppmCreateRegister("VICIntSelect",
		    "VICIntSelect",
		    handles.OFFIS_VIC_SlavePort,
		    12,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICIntSelect.value),
		    True
		    );
  ppmCreateRegister("VICIntEnable",
		    "VICIntEnable",
		    handles.OFFIS_VIC_SlavePort,
		    16,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICIntEnable.value),
		    True
		    );
  ppmCreateRegister("VICIntEnClr",
		    "VICIntEnClr",
		    handles.OFFIS_VIC_SlavePort,
		    20,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICIntEnClr.value),
		    True
		    );
  ppmCreateRegister("VICSoftInt",
		    "VICSoftInt",
		    handles.OFFIS_VIC_SlavePort,
		    24,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICSoftInt.value),
		    True
		    );
  ppmCreateRegister("VICSoftIntClear",
		    "VICSoftIntClear",
		    handles.OFFIS_VIC_SlavePort,
		    28,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICSoftIntClear.value),
		    True
		    );
  ppmCreateRegister("VICProtection",
		    "VICProtection",
		    handles.OFFIS_VIC_SlavePort,
		    32,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICProtection.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr",
		    "VICVectAddr",
		    handles.OFFIS_VIC_SlavePort,
		    48,
		    4,
		    rcb,
		    wcb_VICVectAddr,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr.value),
		    True
		    );
  ppmCreateRegister("VICDefVectAddr",
		    "VICDefVectAddr",
		    handles.OFFIS_VIC_SlavePort,
		    52,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICDefVectAddr.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr0",
		    "VICVectAddr0",
		    handles.OFFIS_VIC_SlavePort,
		    256,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr0.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr1",
		    "VICVectAddr1",
		    handles.OFFIS_VIC_SlavePort,
		    260,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr1.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr2",
		    "VICVectAddr2",
		    handles.OFFIS_VIC_SlavePort,
		    264,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr2.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr3",
		    "VICVectAddr3",
		    handles.OFFIS_VIC_SlavePort,
		    268,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr3.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr4",
		    "VICVectAddr4",
		    handles.OFFIS_VIC_SlavePort,
		    272,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr4.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr5",
		    "VICVectAddr5",
		    handles.OFFIS_VIC_SlavePort,
		    276,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr5.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr6",
		    "VICVectAddr6",
		    handles.OFFIS_VIC_SlavePort,
		    280,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr6.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr7",
		    "VICVectAddr7",
		    handles.OFFIS_VIC_SlavePort,
		    284,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr7.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr8",
		    "VICVectAddr8",
		    handles.OFFIS_VIC_SlavePort,
		    288,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr8.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr9",
		    "VICVectAddr9",
		    handles.OFFIS_VIC_SlavePort,
		    292,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr9.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr10",
		    "VICVectAddr10",
		    handles.OFFIS_VIC_SlavePort,
		    296,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr10.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr11",
		    "VICVectAddr11",
		    handles.OFFIS_VIC_SlavePort,
		    300,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr11.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr12",
		    "VICVectAddr12",
		    handles.OFFIS_VIC_SlavePort,
		    304,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr12.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr13",
		    "VICVectAddr13",
		    handles.OFFIS_VIC_SlavePort,
		    308,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr13.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr14",
		    "VICVectAddr14",
		    handles.OFFIS_VIC_SlavePort,
		    312,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr14.value),
		    True
		    );
  ppmCreateRegister("VICVectAddr15",
		    "VICVectAddr15",
		    handles.OFFIS_VIC_SlavePort,
		    316,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectAddr15.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl0",
		    "VICVectCntl0",
		    handles.OFFIS_VIC_SlavePort,
		    512,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl0.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl1",
		    "VICVectCntl1",
		    handles.OFFIS_VIC_SlavePort,
		    516,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl1.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl2",
		    "VICVectCntl2",
		    handles.OFFIS_VIC_SlavePort,
		    520,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl2.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl3",
		    "VICVectCntl3",
		    handles.OFFIS_VIC_SlavePort,
		    524,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl3.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl4",
		    "VICVectCntl4",
		    handles.OFFIS_VIC_SlavePort,
		    528,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl4.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl5",
		    "VICVectCntl5",
		    handles.OFFIS_VIC_SlavePort,
		    532,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl5.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl6",
		    "VICVectCntl6",
		    handles.OFFIS_VIC_SlavePort,
		    536,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl6.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl7",
		    "VICVectCntl7",
		    handles.OFFIS_VIC_SlavePort,
		    540,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl7.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl8",
		    "VICVectCntl8",
		    handles.OFFIS_VIC_SlavePort,
		    544,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl8.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl9",
		    "VICVectCntl9",
		    handles.OFFIS_VIC_SlavePort,
		    548,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl9.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl10",
		    "VICVectCntl10",
		    handles.OFFIS_VIC_SlavePort,
		    552,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl10.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl11",
		    "VICVectCntl11",
		    handles.OFFIS_VIC_SlavePort,
		    556,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl11.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl12",
		    "VICVectCntl12",
		    handles.OFFIS_VIC_SlavePort,
		    560,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl12.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl13",
		    "VICVectCntl13",
		    handles.OFFIS_VIC_SlavePort,
		    564,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl13.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl14",
		    "VICVectCntl14",
		    handles.OFFIS_VIC_SlavePort,
		    568,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl14.value),
		    True
		    );
  ppmCreateRegister("VICVectCntl15",
		    "VICVectCntl15",
		    handles.OFFIS_VIC_SlavePort,
		    572,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_VIC_SlavePort.VICVectCntl15.value),
		    True
		    );
}

////////////////////////////////// Constructor /////////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor) {
  installSlavePorts();
  installRegisters();
  installNetPorts();
}

////////////////////////////////// Create Threads////////////////////////////////
// define size (32*1024)
// 
// har stackA[size]; 
// 
// oid myThread(void *user)
// 
//  // struct myThreadcontext *p = user;
//  while(1) {
//    bhmWaitDelay(1000);  // wait for 1 ms
//    bhmPrintf("OFFIS HW TimerCounter: tick\n");
//    ppmWriteNet(handles.IRQ0, 0);
//    // bhmWaitDelay(500*1000);  // wait for half a second
//    bhmWaitDelay(1); // wait for 1 us
//    bhmPrintf("OFFIS HW TimerCounter: tock\n");
//    ppmWriteNet(handles.IRQ0, 1);
//  }
// 

void userInit(void)
{
  // struct myThreadcontext { Uns32 myThreadData1; Uns32 myThreadData2; } contextA;
  // bhmCreateThread(myThread, &contextA, "threadA", &stackA[size]);
  //  ppmInstallNetCallback(handles.IRQi, IRQiEventHandler, 0);
  
 ppmInstallNetCallback(handles.IRQi0, IRQi0EventHandler, 0);
 ppmInstallNetCallback(handles.IRQi1, IRQi1EventHandler, 0);
 ppmInstallNetCallback(handles.IRQi2, IRQi2EventHandler, 0);
 ppmInstallNetCallback(handles.IRQi3, IRQi3EventHandler, 0);
 ppmInstallNetCallback(handles.IRQi4, IRQi4EventHandler, 0);
 ppmInstallNetCallback(handles.IRQi5, IRQi5EventHandler, 0);
}

///////////////////////////////////// Main /////////////////////////////////////

int main(int argc, char *argv[]) {
  constructor();
  userInit();
  bhmWaitEvent(bhmGetSystemEvent(BHM_SE_END_OF_SIMULATION));
  destructor();
  return 0;
}
