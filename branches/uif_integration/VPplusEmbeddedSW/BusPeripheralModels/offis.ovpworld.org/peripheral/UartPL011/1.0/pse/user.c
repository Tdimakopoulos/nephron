/*
 * QEMU Uart Emulator.
 *
 * Copyright (c) 2003 Fabrice Bellard
 * Copyright (c) 2006 Openedhand Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Copyright (c) 2005-2011 Imperas Software Ltd., www.imperas.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <string.h>

#include "pse.igen.h"

#define BPORT1 0
#include "pse.macro.igen.h"
#include "psesockets.h"

//////////////////////////////// Callback stubs ////////////////////////////////

typedef enum uart_typeE {
    PL011_ARM,
    PL011_LUMINARY
} uart_type;

// Internal state
uart_type    uType;
static Uns32 read_fifo[16];
static Int32 read_pos;
static Int32 read_count;
static Int32 read_trigger;
static Int32 channel;        // serial socket/log channel
static Uns32 portnum;
static bhmEventHandle charReceived, charSent;

#define DEFAULT_RX_DELAY 20000
// map generated names to shorthand
#define flags          bport1_ab_data.flags.value
#define int_level      bport1_ab_data.int_level.value
#define int_enabled    bport1_ab_data.int_enabled.value
#define dmacr          bport1_ab_data.dmacr.value
#define lcr            bport1_ab_data.lcr.value
#define cr             bport1_ab_data.cr.value
#define ifl            bport1_ab_data.ifl.value

#define RXFF           BPORT1_AB_ECR_RXFF
#define RXFE           BPORT1_AB_ECR_RXFE
#define TXFF           BPORT1_AB_ECR_TXFF
#define TXFE           BPORT1_AB_ECR_TXFE

#define TX             BPORT1_AB_INT_LEVEL_TX
#define RX             BPORT1_AB_INT_LEVEL_RX

// #define DIAG_LOW       (diagnosticLevel>= 1)
#define DIAG_LOW       1

//
// ID
//
static const unsigned char id[2][8] = {
  { 0x11, 0x10, 0x14, 0x00, 0x0d, 0xf0, 0x05, 0xb1 }, /* PL011_ARM */
  { 0x11, 0x00, 0x18, 0x01, 0x0d, 0xf0, 0x05, 0xb1 }, /* PL011_LUMINARY */
};

static void update(void)
{
    Bool f = (int_level & int_enabled) && 1;
    static Bool current = 0;
    if (f != current) {
        current = f;
        ppmWriteNet(handles.irq, current);
		// bhmMessage("I", "AT IRQ", "I'm here doing IRQss");
    }
	
}

static Bool can_receive(void) {
    if (lcr & 0x10)
        return read_count < 16;
    else
        return read_count < 1;
}

static void chr_write(Uns8 *data, Uns32 bytes){
    serWrite(channel, data, bytes);
    bhmTriggerEvent(charSent);
}

static void receiveByte(Uns32 value)
{
    Int32 slot = read_pos + read_count;
    if (slot >= 16)
        slot -= 16;
    read_fifo[slot] = value;
    read_count++;
    flags &= ~RXFE;
    if (cr & 0x10 || read_count == 16) {
        flags |= RXFF;
    }
    if (read_count == read_trigger) {
        int_level |= RX;
        update();
    }
}

PPM_REG_READ_CB(readDR) {
    flags &= ~TXFE;
    Uns32 c = read_fifo[read_pos];
    if (read_count > 0) {
        read_count--;
        if (++read_pos == 16)
            read_pos = 0;
    }
    if (read_count == 0) {
        flags |= RXFE;
    }
    if (read_count == read_trigger - 1)
        int_level &= ~RX;
    update();
    return c;
}

PPM_REG_READ_CB(readECR) {
    // const 0
    return 0;
}

PPM_REG_READ_CB(readID) {
    Uns32 offset = ((Uns32)user - (Uns32)&bport1_ab_data.id0.value)/sizeof(bport1_ab_data.id0.value);
    offset &= 7;
    return id[uType][offset];
}

PPM_REG_READ_CB(readMIS) {
    return int_level & int_enabled;
}

PPM_REG_WRITE_CB(writeDMA) {
    *(Uns32*)user = data;
    if (dmacr & 3)
        bhmMessage("E", "PL011_DNS", "DMA mode not supported");
}

PPM_REG_WRITE_CB(writeDR) {
    /* ??? Check if transmitter is enabled.  */
     Uns8 ch = data;
     chr_write(&ch, 1);

     int_level |= BPORT1_AB_INT_LEVEL_TX;
     update();
}

PPM_REG_WRITE_CB(writeICR) {
    int_level &= (~data) & 0xFF;
    update();
}

PPM_REG_WRITE_CB(writeIMSC) {
    int_enabled = data;
    update();
}

PPM_REG_WRITE_CB(writeIFL) {
    ifl = data;
    read_trigger = 1;
}

PPM_REG_WRITE_CB(writeLCR) {
    lcr = data;
    read_trigger = 1;
}

#define TOP (256 *2048)
Uns8 stack1[TOP];

//
// The polling process that gets characters
//
static void getChars(void *user)
{
    while(1) {
        double d = DEFAULT_RX_DELAY;

        bhmWaitDelay(d);
        if (can_receive()) {
            Uns8 c;
            if (serRead(channel, &c, 1)) {
                bhmTriggerEvent(charReceived);
                receiveByte(c);
            }
        }
    }
}

/*
static void myMessageReceived(void *user)
{
  while(1){
    bhmWaitEvent(charReceived);
	bhmMessage("I", "PL011_INV", "We did receive a char! Juhu!");
  }	
}	

static void myMessageSent(void *user)
{
  while(1){
    bhmWaitEvent(charSent);
	bhmMessage("I", "PL011_INV", "We did sent a char! Juhu!");
  }	
}	
*/

static void openFiles(void)
{
    channel  = 0;
    portnum  = 0;

    char outPath[1024];   // serial output file
    char srcPath[1024];   // serial input file
    Uns32 log = 0;

    Bool portSet = bhmIntegerAttribute("portnum", &portnum);
    Bool outSet  = bhmStringAttribute("outfile",  outPath, sizeof(outPath));
    Bool srcSet  = bhmStringAttribute("infile",   srcPath,  sizeof(srcPath));
    bhmIntegerAttribute("log", &log);

    if (portSet && srcSet) {
        bhmMessage("F", "PL011", "Please specify only one means of input");
    }

    if(portSet && DIAG_LOW) {
        bhmMessage("I", "PL011", "Attempting to Open Socket on port:%d\n", portnum);
    }

    Uns32 *portp = portSet ? &portnum : NULL;
    char  *src   = srcSet  ?  srcPath : NULL;
    char  *out   = outSet  ?  outPath : NULL;

    //  if portSet, this will block the simulator
    channel = serOpenBlocking(portp, out, src, log);
    if(channel < 0) {
        bhmMessage("F", "PL011", "Failed open a serial channel");
    }
}


PPM_CONSTRUCTOR_CB(constructor) {
    uType = PL011_ARM;

    char variant[128];

    if (bhmStringAttribute("variant", variant, sizeof(variant))) {
        if (strcmp(variant, "ARM") == 0) {
            uType = PL011_ARM;
        } else if (strcmp(variant, "LUMINARY") == 0) {
            uType = PL011_LUMINARY;
        } else {
            bhmMessage("F", "PL011_INV", "Invalid variant '%s'. Expected ARM or LUMINARY", variant);
        }
    }
    periphConstructor();
    openFiles();
    read_trigger = 1;
    bhmCreateThread(getChars, NULL, "getchars", &stack1[TOP]);
    // bhmCreateThread(myMessageReceived, NULL, "mymessagereceived", &stack1[TOP]);
    // bhmCreateThread(myMessageSent, NULL, "mymessagesent", &stack1[TOP]);
    charReceived = bhmCreateNamedEvent("Rx", "Character received");
    charSent     = bhmCreateNamedEvent("Tx", "Character sent");
}

PPM_DESTRUCTOR_CB(closeDown) {
    if(channel >= 0) {
        serClose(channel);
    }
}

