
////////////////////////////////////////////////////////////////////////////////
//
//                W R I T T E N   B Y   I M P E R A S   I G E N
//
//                          Fri Jan 14 10:19:13 2011
//
////////////////////////////////////////////////////////////////////////////////

// MODEL IO:
//    Slave Port bport1
//    Net output  irq;

#ifndef ARM_OVPWORLD_ORG_PERIPHERAL_UARTPL011_1_0
#define ARM_OVPWORLD_ORG_PERIPHERAL_UARTPL011_1_0
#include "ovpworld.org/modelSupport/tlmPeripheral/1.0/tlm2.0/peripheral.hpp"
using namespace sc_core;

class UartPL011 : public icmPeripheral
{
  private:
    const char *getModel() {
        return icmGetVlnvString (NULL, "arm.ovpworld.org", "peripheral", "UartPL011", "1.0", "pse");
    }

    const char *getSHL() {
        return icmGetVlnvString (NULL, "arm.ovpworld.org", "peripheral", "UartPL011", "1.0", "model");
    }

  public:
    icmSlavePort        bport1;
    icmOutputNetPort irq;

    UartPL011(sc_module_name name, icmAttrListObject *initialAttrs = 0 )
        : icmPeripheral(name, getModel(), getSHL(), initialAttrs)
        , bport1(this, "bport1", 0x1000) // static
        , irq(this, "irq")
    {
    }

}; /* class UartPL011 */

#endif
