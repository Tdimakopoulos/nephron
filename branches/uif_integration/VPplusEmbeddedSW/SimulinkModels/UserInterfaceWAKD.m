function varargout = UserInterfaceWAKD(varargin)
% USERINTERFACEWAKD MATLAB code for UserInterfaceWAKD.fig
%      USERINTERFACEWAKD, by itself, creates a new USERINTERFACEWAKD or raises the existing
%      singleton*.
%
%      H = USERINTERFACEWAKD returns the handle to a new USERINTERFACEWAKD or the handle to
%      the existing singleton*.
%
%      USERINTERFACEWAKD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in USERINTERFACEWAKD.M with the given input arguments.
%
%      USERINTERFACEWAKD('Property','Value',...) creates a new USERINTERFACEWAKD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before UserInterfaceWAKD_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to UserInterfaceWAKD_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help UserInterfaceWAKD

% Last Modified by GUIDE v2.5 03-Sep-2012 17:41:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @UserInterfaceWAKD_OpeningFcn, ...
                   'gui_OutputFcn',  @UserInterfaceWAKD_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before UserInterfaceWAKD is made visible.
function UserInterfaceWAKD_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to UserInterfaceWAKD (see VARARGIN)

% Choose default command line output for UserInterfaceWAKD
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes UserInterfaceWAKD wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = UserInterfaceWAKD_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%Get still open IRQ from matlab workspace
function irqVectorValue = getIRQvector(irqName)
eml.extrinsic('evalin');
irqVectorValue = 0.0;
irqVectorValue = evalin('base', irqName);


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,1);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in Left.
function Left_Callback(hObject, eventdata, handles)
% hObject    handle to Left (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,2);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in Right.
function Right_Callback(hObject, eventdata, handles)
% hObject    handle to Right (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,3);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in Up.
function Up_Callback(hObject, eventdata, handles)
% hObject    handle to Up (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,4);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in Down.
function Down_Callback(hObject, eventdata, handles)
% hObject    handle to Down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,5);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in ResetWAKD.
function ResetWAKD_Callback(hObject, eventdata, handles)
% hObject    handle to ResetWAKD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq1VectAddr');
irqVectorValue = bitset(irqVectorValue,6);
assignin('base', 'irq1VectAddr', irqVectorValue);


% --- Executes on button press in BubbleDetect.
function BubbleDetect_Callback(hObject, eventdata, handles)
% hObject    handle to BubbleDetect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assignin');
irqVectorValue = getIRQvector('irq0VectAddr');
irqVectorValue = bitset(irqVectorValue,1);
assignin('base', 'irq0VectAddr', irqVectorValue);



function LCD_Callback(hObject, eventdata, handles)
% hObject    handle to LCD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LCD as text
%        str2double(get(hObject,'String')) returns contents of LCD as a double


% --- Executes during object creation, after setting all properties.
function LCD_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RefreshLCD.
function RefreshLCD_Callback(hObject, eventdata, handles)
% hObject    handle to RefreshLCD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('evalin');
messageUI = 0.0;
messageUI = evalin('base', 'Actuator9');
switch messageUI
    case 0.0
        set(handles.LCD,'String', 'WAKD is initializing. WAIT!')
    case 1.0
        set(handles.LCD,'String', 'NoFlowOfBloodAndDialysate: press OK to go to Configuration.')
    case 2.0
        set(handles.LCD,'String', 'Configuration: OK starts flow of Blood.')
    case 3.0
        set(handles.LCD,'String', 'Maintenance: ???')
    case 4.0
        set(handles.LCD,'String', 'NoFlowOfDialysateBloodFlowOn: press OK to dialyze.')
    case 5.0
        set(handles.LCD,'String', 'Dialysis: Doing dialysis!')
    case 9.0
        set(handles.LCD,'String', 'RegenerationReversedPolarization: ')
    case 12.0
        set(handles.LCD,'String', 'Ultrafiltration: ')
    case 15.0
        set(handles.LCD,'String', 'RegenerationNoPolarization: ')
    otherwise
        set(handles.LCD,'String', 'unknown message!')
end

% -----------------------------------------------------------------
% --- Communication Board
% -----------------------------------------------------------------

% --- Executes on button press in WeightMeasureArrivesOnAir.
function WeightMeasureArrivesOnAir_Callback(hObject, eventdata, handles)
% hObject    handle to WeightMeasureArrivesOnAir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 8;
assignin('base', 'irq2VectAddr', irqVectorValue);


% --- Executes on button press in WeightRequest.
function WeightRequest_Callback(hObject, eventdata, handles)
% hObject    handle to WeightRequest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 9;
assignin('base', 'irq2VectAddr', irqVectorValue);


% --- Executes on button press in PatientACKsWeight.
function PatientACKsWeight_Callback(hObject, eventdata, handles)
% hObject    handle to PatientACKsWeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 10;
assignin('base', 'irq2VectAddr', irqVectorValue);


% --- Executes on slider movement.
function WeightScaleMeasure_Callback(hObject, eventdata, handles)
% hObject    handle to WeightScaleMeasure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function WeightScaleMeasure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WeightScaleMeasure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function WeightSlider_Callback(hObject, eventdata, handles)
% hObject    handle to WeightSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
eml.extrinsic('assign');
WeightFromScale = get(hObject,'Value')*140;
assignin('base', 'Sensor53', WeightFromScale*10);
set(handles.LCDweight,'string', WeightFromScale);



% --- Executes during object creation, after setting all properties.
function WeightSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WeightSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function LCDweight_Callback(hObject, eventdata, handles)
% hObject    handle to LCDweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LCDweight as text
%        str2double(get(hObject,'String')) returns contents of LCDweight as a double


% --- Executes during object creation, after setting all properties.
function LCDweight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCDweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in AllStoppedFrom.
function AllStoppedFrom_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 1*get(hObject, 'Value'));
% set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in AllStoppedTo.
function AllStoppedTo_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedTo
eml.extrinsic('assign');
assignin('base', 'Sensor55', 1*get(hObject, 'Value'));
%set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in NoDialysateTo.
function NoDialysateTo_Callback(hObject, eventdata, handles)
% hObject    handle to NoDialysateTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NoDialysateTo
eml.extrinsic('assign');
assignin('base', 'Sensor55', 3*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
%set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in AllStoppedFrom.
function DialysisFrom_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 4*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
%set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in AllStoppedFrom.
function Regenerate1From_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 5*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
%set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in AllStoppedFrom.
function UltrafiltrationFrom_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 6*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
%set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in AllStoppedFrom.
function Regeneration2From_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 7*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
%set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in AllStoppedFrom.
function MaintenanceFrom_Callback(hObject, eventdata, handles)
% hObject    handle to AllStoppedFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AllStoppedFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 2*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
%set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in DialysisTo.
function DialysisTo_Callback(hObject, eventdata, handles)
% hObject    handle to DialysisTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of DialysisTo
eml.extrinsic('assign');
assignin('base', 'Sensor55', 4*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
%set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in Regenerate1To.
function Regenerate1To_Callback(hObject, eventdata, handles)
% hObject    handle to Regenerate1To (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Regenerate1To
eml.extrinsic('assign');
assignin('base', 'Sensor55', 5*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
%set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in UltrafiltrationTo.
function UltrafiltrationTo_Callback(hObject, eventdata, handles)
% hObject    handle to UltrafiltrationTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of UltrafiltrationTo
eml.extrinsic('assign');
assignin('base', 'Sensor55', 6*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
%set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in Regeneration2To.
function Regeneration2To_Callback(hObject, eventdata, handles)
% hObject    handle to Regeneration2To (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Regeneration2To
eml.extrinsic('assign');
assignin('base', 'Sensor55', 7*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
%set(handles.Regeneration2To,'Value',0);
set(handles.MaintenanceTo,'Value',0);


% --- Executes on button press in MaintenanceTo.
function MaintenanceTo_Callback(hObject, eventdata, handles)
% hObject    handle to MaintenanceTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of MaintenanceTo
eml.extrinsic('assign');
assignin('base', 'Sensor55', 2*get(hObject, 'Value'));
set(handles.AllStoppedTo,'Value',0);
set(handles.NoDialysateTo,'Value',0);
set(handles.DialysisTo,'Value',0);
set(handles.Regenerate1To,'Value',0);
set(handles.UltrafiltrationTo,'Value',0);
set(handles.Regeneration2To,'Value',0);
%set(handles.MaintenanceTo,'Value',0);



% --- Executes on button press in NoDialysateFrom.
function NoDialysateFrom_Callback(hObject, eventdata, handles)
% hObject    handle to NoDialysateFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of NoDialysateFrom
eml.extrinsic('assign');
assignin('base', 'Sensor54', 3*get(hObject, 'Value'));
set(handles.AllStoppedFrom,'Value',0);
%set(handles.NoDialysateFrom,'Value',0);
set(handles.DialysisFrom,'Value',0);
set(handles.Regenerate1From,'Value',0);
set(handles.UltrafiltrationFrom,'Value',0);
set(handles.Regeneration2From,'Value',0);
set(handles.MaintenanceFrom,'Value',0);


% --- Executes on button press in SemiAutomaticFrom.
function SemiAutomaticFrom_Callback(hObject, eventdata, handles)
% hObject    handle to SemiAutomaticFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SemiAutomaticFrom
eml.extrinsic('assign');
assignin('base', 'Sensor56', 2*get(hObject, 'Value'));
set(handles.AutomaticFrom,'Value',0);


% --- Executes on button press in SemiAutomaticTo.
function SemiAutomaticTo_Callback(hObject, eventdata, handles)
% hObject    handle to SemiAutomaticTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SemiAutomaticTo
eml.extrinsic('assign');
assignin('base', 'Sensor57', 2*get(hObject, 'Value'));
set(handles.AutomaticTo,'Value',0);


% --- Executes on button press in AutomaticFrom.
function AutomaticFrom_Callback(hObject, eventdata, handles)
% hObject    handle to AutomaticFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AutomaticFrom
eml.extrinsic('assign');
assignin('base', 'Sensor56', 1*get(hObject, 'Value'));
set(handles.SemiAutomaticFrom,'Value',0);


% --- Executes on button press in AutomaticTo.
function AutomaticTo_Callback(hObject, eventdata, handles)
% hObject    handle to AutomaticTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AutomaticTo
eml.extrinsic('assign');
assignin('base', 'Sensor57', 1*get(hObject, 'Value'));
set(handles.SemiAutomaticTo,'Value',0);


% --- Executes on button press in ChangeStateOK.
function ChangeStateOK_Callback(hObject, eventdata, handles)
% hObject    handle to ChangeStateOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 12;
assignin('base', 'irq2VectAddr', irqVectorValue);


% --- Executes on button press in StatRequest.
function StatRequest_Callback(hObject, eventdata, handles)
% hObject    handle to StatRequest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 11;
assignin('base', 'irq2VectAddr', irqVectorValue);


% --- Executes on button press in BPrequest.
function BPrequest_Callback(hObject, eventdata, handles)
% hObject    handle to BPrequest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
eml.extrinsic('assign');
irqVectorValue = 13;
assignin('base', 'irq2VectAddr', irqVectorValue);

% --- Executes on button press in BPmeasurement.
function BPmeasurement_Callback(hObject, eventdata, handles)
% hObject    handle to BPmeasurement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
BPsysValue = 100; %% using fixed dummy values here!
BPdiaValue = 80; %% using fixed dummy values here!
BPhrValue = 55; %% using fixed dummy values here!
assignin('base', 'Sensor58', BPsysValue*1);
assignin('base', 'Sensor59', BPdiaValue*1);
assignin('base', 'Sensor60', BPhrValue*1);
%set(handles.LCDweight,'string', WeightFromScale);
irqVectorValue = 14;
assignin('base', 'irq2VectAddr', irqVectorValue);
set(handles.LCDsys,'string', BPsysValue);
set(handles.LCDdia,'string', BPdiaValue);
set(handles.LCDhr,'string', BPhrValue);



function LCDsys_Callback(hObject, eventdata, handles)
% hObject    handle to LCDsys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LCDsys as text
%        str2double(get(hObject,'String')) returns contents of LCDsys as a double


% --- Executes during object creation, after setting all properties.
function LCDsys_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCDsys (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LCDdia_Callback(hObject, eventdata, handles)
% hObject    handle to LCDdia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LCDdia as text
%        str2double(get(hObject,'String')) returns contents of LCDdia as a double


% --- Executes during object creation, after setting all properties.
function LCDdia_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCDdia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LCDhr_Callback(hObject, eventdata, handles)
% hObject    handle to LCDhr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LCDhr as text
%        str2double(get(hObject,'String')) returns contents of LCDhr as a double


% --- Executes during object creation, after setting all properties.
function LCDhr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LCDhr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
