note left of SP
	User operates SmartPhone UI
	to change configurtion values
	and requests sending.
end note

SP -> CB: dataID_changeConfigurationParameters

note over CB
	looks into header for
	... size information
end note
CB -> UartMB: bytewise forward

UartMB -> TaskCBPA: command_BufferFromCBavailable
note over TaskCBPA
	Looks into UART buffer ...
	... finds ID: dataID_changeConfigurationParameters
	... finds size: xyz
	... allocates mem for xyz
end note
TaskCBPA -> UartMB: getData();

TaskCBPA -> UartMB: putData(dataID_ack); 

UartMB -> CB: dataID_ack
note over CB
	looks into header for
	... id information
	... size information
end note
CB -> SP: bytewise forward
	
TaskCBPA -> TaskDisp: dataID_changeConfigurationParameters

TaskDisp -> TaskDS: dataID_changeConfigurationParameters
note over TaskDS
	1) Read params from SD-card
	2) Applies changes
	3) Writes params to SD-card
	4) frees allocated mem
end note

TaskDS -> TaskDisp: command_ConfigurationChanged




alt change considers FC parameters
    TaskDisp -> TaskFC: command_ConfigurationChanged.
note over TaskFC
Task alloc memory for new configuration set
end note
TaskFC-> TaskDisp: command_InitializeFC

TaskDisp -> TaskDS: command_InitializeFC
note over TaskDS
	1) Read param from SD-crd
	2) Store in FC param struct
end note

TaskDS -> TaskFC: command_RequestDatafromDSReady

note over TaskFC
Task takes over read out parameters and frees previously alloc'ed memory.
end note

note over TaskFC
FC checks first (just to some extend) if the new parameters are fine
end note

TaskFC -> TaskRTBPA: dataID_configureState.




else change considers SCC parameters
TaskDisp -> TaskSCC: command_ConfigurationChanged
note over TaskSCC
Task alloc memory for new configuration set
end note
TaskSCC -> TaskDisp: command_InitializeSCC
TaskDisp -> TaskDS: command_InitializeSCC
note over TaskDS
	1) Read param from SD-crd
	2) Store in SCC param struct
end note
TaskDS -> TaskSCC: command_RequestDatafromDSReady

note over TaskSCC
Task takes over read out parameters and frees previously alloc'ed memory.
end note

end


