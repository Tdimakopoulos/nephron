// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   rtmcb_uart4_dma.h
//! \brief  uart4 incoming data to memory by dma
//!
//! Routines for reading the UART4 and store automatically using DMA 
//!
//! \author  Dudnik G.S.
//! \date    28.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
#ifndef RTMCB_UART4_DMA_H_
#define RTMCB_UART4_DMA_H_
// -----------------------------------------------------------------------------------
// Exported constants & Macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void RTMCB_UART4_DMA_Config(void);
extern void RTMCB_UART4_DMA_NVIC_Config(void);
extern void RTMCB_UART4_DMA_EraseData(void);
extern void RTMCB_UART4_DMA_ReadData (void);
// -----------------------------------------------------------------------------------
#endif /* RTMCB_UART4_DMA_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
