// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   feedbackadc.h
//! \brief  VBAT & VPOLAR Bridge values Measurement
//!
//! Routines for reading the ADC and store automatically using DMA 
//!
//! \author  Dudnik G.S.
//! \date    10.07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private Definitions
// -----------------------------------------------------------------------------------
#define ADC1_DR_Address ((uint32_t)0x4001244C)


// -----------------------------------------------------------------------------------
//! \brief  ....
//!
//! ....
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
//! \brief Initialization of ADC and DMA for pressure recording
//!
//! This function initializes ADC and DMA1 Channel1 for pressure recording
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
//! ADC12_IN10				AB&C: RBAT_CHARGE
//! ADC12_IN11				BRIDGE: VBRIDGE_PWR_OK
//! ADC12_IN12				BRIDGE: VBRIDGE_VOUT1
//! ADC12_IN13				BRIDGE: VBRIDGE_VOUT2
// -----------------------------------------------------------------------------------
void FEEDBACK_ADCDMAConfig(void)
{
    ADC_InitTypeDef ADC_InitStructure;
    DMA_InitTypeDef DMA_InitStructure;

    // ADC1 configuration --------------------------------------------------------
    //----------------------------------------------------------------------------
   
    // - ADC1 CLOCK ENABLE
    RCC_APB2PeriphClockCmd( RCC_APB2ENR_ADC1EN , ENABLE);

    ADC_DeInit(ADC1);
    // In this mode the dual ADC synchronization is bypassed and each ADC interfaces works independently.
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;	
    ADC_InitStructure.ADC_ScanConvMode = ENABLE; // DISABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfChannel = 4; // 1;
    ADC_Init(ADC1, &ADC_InitStructure);
    // ADC1 regular channel10 configuration 
    ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_55Cycles5);
    ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_55Cycles5);
    // ADC1 analog watchdog configuration
    // ADC_AnalogWatchdogCmd(ADC1, ADC_AnalogWatchdog_SingleRegEnable);
    // ADC_AnalogWatchdogThresholdsConfig(ADC1, THRESHOLD_HIGH, THRESHOLD_LOW);
    // ADC_AnalogWatchdogSingleChannelConfig(ADC1, ADC_Channel_0);
    // ADC_ITConfig(ADC1, ADC_IT_AWD, ENABLE); 
    // Enable analog watchdog interrupt
    // Enable ADC1 DMA 
    ADC_DMACmd(ADC1, ENABLE);
    // Enable ADC1 
    ADC_Cmd(ADC1, ENABLE);
    // Enable ADC1 reset calibration register 
    ADC_ResetCalibration(ADC1);
    // Check the end of ADC1 reset calibration register 
    while(ADC_GetResetCalibrationStatus(ADC1));
    // Start ADC1 calibration 
    ADC_StartCalibration(ADC1);
    // Check the end of ADC1 calibration 
    while(ADC_GetCalibrationStatus(ADC1));
    
    /* DMA1 channel1 configuration ----------------------------------------------*/
    //----------------------------------------------------------------------------

    // - DMA1 CLOCK ENABLE
    RCC_AHBPeriphClockCmd(RCC_AHBENR_DMA1EN, ENABLE);

    DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_DR_Address;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&ADC_FeedBack_ReadValue[0]; // ADC_RBATCharge_ReadValue;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = 4;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);
    /* Enable DMA1 channel1 */
    DMA_Cmd(DMA1_Channel1, ENABLE);
    /* Enable DMA1 channel1 Interrupt*/
    DMA_ITConfig(DMA1_Channel1, DMA_CCR1_TCIE, ENABLE);
    // Erase reception Buffer
    FEEDBACK_ADCDMA_EraseData();
}


// -----------------------------------------------------------------------------------
//! \brief FEEDBACK_ADCDMA_NVIC_Config
//!
//! DMA Interrupt Enable for ADC EOC
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
// DMA Channel 1 (event ADC1)
void FEEDBACK_ADCDMA_NVIC_Config(void){
    DEVICE_IRQ_Config(DMA1_Channel1_IRQn, 12, 0);
}

// -----------------------------------------------------------------------------------
//! \brief FEEDBACK_ADCDMA_EraseData
//!
//! Clear Reception Buffer in Memory
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
void FEEDBACK_ADCDMA_EraseData (void){
    uint8_t k=0;
    for(k=0;k<4;k++) {
      ADC_FeedBack_ReadValue[k] = 0;
      ADC_FeedBack_EchoValue[k] = 0;
    }
}
// -----------------------------------------------------------------------------------
//! \brief Feedback_ADCs_ReadData
//!
//! Read Recently acquired data 
//!
//! \param void
//! \return void
//!
//! K=0: RBAT_CHARGE
//! K=1: VBRIDGE_PWR_OK
//! K=2: VBRIDGE_VOUT1
//! K=3: VBRIDGE_VOUT2 
// -----------------------------------------------------------------------------------
void FEEDBACK_ADCDMA_ReadData (void){
  uint8_t k=0;
  for(k=0;k<4;k++) {
    ADC_FeedBack_EchoValue[k] = ADC_FeedBack_ReadValue[k];
  }
   
}

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
