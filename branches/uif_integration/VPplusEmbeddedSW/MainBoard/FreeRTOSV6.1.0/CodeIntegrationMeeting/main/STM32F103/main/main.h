// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   main.h
//! \brief  startup program
//!
//! Initializes devices and variables, executes main loop
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef __MAIN_H
#define __MAIN_H
// -----------------------------------------------------------------------------------
// Exported constants 
// -----------------------------------------------------------------------------------

#define HWBUILD                               // Integration: build for actual HW, not for VP
#define DEMO_BYPASS                           // BYPASSING DEMO FUNCTIONS RTMCB FROM CSEM

#define INTER_INTEGRATION_201103              // SOME THINGS FOR DEMO SOME INTEGRATION

#define PC_SPY_MB_RTMCB                       // PCViewer for messages flowing from MB to RTMCB 

#define STM32F_MB_FREERTOS
//#define STM32F_LED_TASK1
//#define STM32F_LED_TASK_MAINTENANCE_TG
//#define STM32F_LED_TASK_MAINTENANCE_PL
//#define STM32F_SEMAPHORE_TASK
#define STM32F_SEMAPHORE_INT

#define TEST_SDCARD                           // ENABLES/DISABLES SDCARD TEST

#define STM32F_MB
#define STM32F_LED_INT_TICK

#define MB_TASK_CMD_RTMCB

//#define RTOS_SYSTICK_LED
//#define STM32F_LED_MAIN
//#define STM32F_LED_TIM1
//#define STM32F_LED_TIM2
#define STM32F_SPORTS_STINT
//#define STM32_USART1_LBACK
#define STM32_USART2_LBACK
#define STM32_USART3_LBACK
#define STM32_UART4_LBACK
#define STM32F_UART4_DMA
#define STM32_UART5_LBACK

#define STM32F_UIF_ACC_SCHEDULER
#define UIF_SIMULATION
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
// STM32F Peripheral Library
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "stm32f10x.h"
	#include "stm32f10x_pwr.h"
	#include "stm32f10x_rcc.h"
	#include "stm32f10x_tim.h"
	#include "stm32f10x_exti.h"
	#include "stm32f10x_gpio.h"
	#include "stm32f10x_spi.h"
	#include "stm32f10x_rtc.h"
	#include "stm32f10x_bkp.h"
	#include "stm32f10x_flash.h"
	#include "stm32f10x_systick.h"
	#include "stm32f10x_usart.h"
	#include "stm32f10x_i2c.h"
	#include "stm32f10x_adc.h"
	#include "stm32f10x_dma.h"
	#include "eeprom.h"
#endif

// ----------------------------------------
// SDCARD
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "efsl\\sdcard_spi1.h"
	#include "efsl\\dir.h"
	#include "efsl\\disc.h"
	#include "efsl\\efs.h"
	#include "efsl\\extract.h"
	#include "efsl\\fat.h"
	#include "efsl\\file.h"
	#include "efsl\\fs.h"
	#include "efsl\\ioman.h"
	#include "efsl\\ls.h"
	#include "efsl\\plibc.h"
	#include "efsl\\sd.h"
	#include "efsl\\time.h"

	#include "efsl\\lpc2000_dbg_printf.h"
#endif

// ----------------------------------------
// RS422
// ----------------------------------------
#ifndef VP_SIMULATION
	#include "ptp_rs422\\RS422_cmd_class_acquisition.h"
	#include "ptp_rs422\\RS422_cmd_class_communication.h"
	#include "ptp_rs422\\RS422_cmd_class_memory.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_ctrl.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_data.h"
	#include "ptp_rs422\\RS422_cmd_class_nephronplus_system.h"
	#include "ptp_rs422\\RS422_cmd_class_power.h"
	#include "ptp_rs422\\RS422_cmd_class_system.h"
	#include "ptp_rs422\\RS422_cmd_classes.h"
	#include "ptp_rs422\\RS422_Protocol.h"
#endif
// ----------------------------------------
// MB Tests / Application
// ----------------------------------------
#ifdef LINUX
	#include "data.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "main\\data.h"
#endif
#ifndef VP_SIMULATION
	#include "main\\mb_initialization.h"
	#include "main\\devices_initialization.h"
	#include "main\\math_tool.h"
	#include "main\\tools.h"
	#include "main\\SerialPortTest.h"         // later
	#include "main\\clock_calendar.h"
	#include "main\\supplies.h"
	#include "main\\feedbackadc.h"
	#include "main\\sdcard.h"
	#include "main\\uif_acc_spi0.h"
	//#include "main\\uif_acc_application.h"  // Disable according to files found in 042-Demo
	#include "main\\uif_application.h"
        #include "main\\acc_application.h"
        #include "main\\SerialPortRTMCB.h"
	#include "main\\SerialPortCB.h"
	#include "main\\rtmcb_uart4_dma.h"
	#include "main\\mb_tasks.h"
#endif

/* Standard includes. */
#include <string.h>

/* Scheduler includes. */
#define MBFREERTOS
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// ----------------------------------------
// code from CSEM
// ----------------------------------------
// #include "rtmcb_tasks.h"
// ----------------------------------------
// code from OFFIS
// ----------------------------------------
/* NEPHRON API includes */
#ifdef LINUX
	#include "nephron.h"
	#include "getData.h"
	#include "realTimeBoardProtocolAbstraction.h"
	#include "communicationBoardProtocolAbstraction.h"
	#include "dispatcher.h"
	#include "reminder.h"
	#include "dataStorage.h"
	#include "flowControl.h"
	#include "powermanager.h"
	#include "systemCheckCalibration.h"
	#include "watchDog.h"
	#include "userInterface.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "CodeFromOFFIS\\nephron.h"
	#include "CodeFromOFFIS\\DummyCode\\getData.h"
	#include "CodeFromOFFIS\\TaskRealTimeBoardProtocolAbstraction\\realTimeBoardProtocolAbstraction.h"
	#include "CodeFromOFFIS\\TaskCommunicationBoardProtocolAbstraction\\communicationBoardProtocolAbstraction.h"
	#include "CodeFromOFFIS\\TaskDispatcher\\dispatcher.h"
	#include "CodeFromOFFIS\\TaskReminder\\reminder.h"
	#include "CodeFromOFFIS\\TaskDataStorage\\dataStorage.h"
	#include "CodeFromOFFIS\\TaskFlowControl\\flowControl.h"
	#include "CodeFromOFFIS\\TaskPowerManagementBoardProtocolAbstraction\\powermanager.h"
	#include "CodeFromOFFIS\\TaskSystemCheckCalibration\\systemCheckCalibration.h"
	#include "CodeFromOFFIS\\TaskWatchDog\\watchDog.h"
	#include "CodeFromOFFIS\\TaskUserInterface\\userInterface.h"
#endif

// -----------------------------------------------------------------------------------
// Exported functions 
// -----------------------------------------------------------------------------------
extern void Delay(uint32_t nCount);

#endif /* __MAIN_H */
// -----------------------------------------------------------------------------------
// use of RAM TOTAL : 64K
// -----------------------------------------------------------------------------------
// 4K SDBUFFER, 4K SDCACHE

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
