// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   tools.h
//! \brief  useful routines
//!
//! mainly data conversion routines
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef __TOOLS_H_
#define __TOOLS_H_

#ifdef HWBUILD
  #include "..\\CodeFromOFFIS\\includes_nephron\\global.h"
#else
  #include "..\\main\\global.h"
#endif

extern int8_t UINT32_TO_CH_ASCII(uint32_t, char *, int);
extern int UINT16_TO_CH_ASCII(uint16_t, char *, int);
extern int BYTE_TO_CH_ASCII(uint8_t, char*, int);
extern uint8_t GET08(uint8_t*, uint16_t);
extern uint16_t GET16(uint8_t *, uint16_t);
extern uint32_t GET32(uint8_t *, uint16_t);
extern void SET32(uint8_t *, uint16_t, uint32_t);
extern void SET16(uint8_t *, uint16_t, uint16_t);
extern void SET08(uint8_t *, uint16_t, uint8_t);
extern uint32_t SET8_PV(uint8_t *txbuffer, uint16_t, uint8_t, uint32_t);
extern uint32_t SET16_PV(uint8_t *, uint16_t, uint8_t, uint32_t);
extern uint32_t SET32_PV(uint8_t *, uint16_t, uint8_t, uint32_t);
extern uint32_t SET_PV(uint8_t *, uint16_t, uint8_t, const uint8_t*, uint16_t);
extern int ComparePrefixes(char *, char *, uint8_t);
extern void DelayBySoft (uint32_t DELAYLOOP);


#endif