// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   acc_application.c
//! \brief  ACCELEROMETER application
//!
//! accelerometer routines
//!
//! \author  Dudnik G.S.
//! \date    20.05.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  ACC_Config
//!
//! Configure ACCELEROMETER
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t ACC_Config(){
    acc_whoami = ACC_Read(ACC_WHO_AM_I);
    if(acc_whoami != LIS331DHL_ID) return 0;
    // CTRL_REG1
    //ACC_Write(ACC_CTRL_REG1, ACC_NORMAL_ON | ACC_ZYX_ENABLE);
    ACC_Write(ACC_CTRL_REG1, ACC_LOWPWR_10HZ | ACC_ZYX_ENABLE);    
    // CTRL_REG2
    ACC_Write(ACC_CTRL_REG2, 0);
    // CTRL_REG3
    ACC_Write(ACC_CTRL_REG3, ACC_LIR2_LATCH | ACC_DATAREADY);

    // CTRL_REG4
#ifdef ACCELEROMETER_SELFTEST
    ACC_Write(ACC_CTRL_REG4, ACC_DATANOUPD|ACC_MSBATLADD|ACC_FS_2GR|ACC_SELFTESTE); // ACC***SELFTEST
    //ACC_Write(ACC_CTRL_REG4, ACC_DATANOUPD|ACC_MSBATLADD|ACC_FS_8GR|ACC_SELFTESTE); // ACC***SELFTEST
#else 
    ACC_Write(ACC_CTRL_REG4, ACC_DATANOUPD|ACC_MSBATLADD|ACC_FS_2GR);               // ACC: NORMAL
    //ACC_Write(ACC_CTRL_REG4, ACC_DATANOUPD|ACC_MSBATLADD|ACC_FS_8GR);               // ACC: NORMAL
#endif    
    // CTRL_REG5 
    ACC_Write(ACC_CTRL_REG5, 0);
    // INT2 THRESHOLD 1g 
    ACC_Write(INT2_THS, 0x40);
    // INT2 DURATION = 20ms (but theoretically it will be latched)
    ACC_Write(INT2_DURATION, 0x01);
    // INT2 CONFIG = 6D DIRECTION | ENABLE ALL XH, YL, YH, YL, ZH, ZL
    ACC_Write(INT2_CFG, ACC_6D_DIRECTION|ACC_ENABLE_X_Y_Z);
    return 1;
}
// -----------------------------------------------------------------------------------
//! \brief  ACC_DataRead
//!
//! Configure ACCELEROMETER
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t ACC_DataRead(){
    uint16_t ACC_X = 0;
    uint16_t ACC_Y = 0;
    uint16_t ACC_Z = 0;
    uint8_t OVR_TEST = 0;
    uint8_t ACC_IDX = 0;

    ACCELEROMETER_UPDATING = 1;
    // STATUS
    acc_status[ACCELEROMETER_INDEX][1] = ACC_Read(STATUS_REG);
    if((acc_status[ACCELEROMETER_INDEX][1] & ACC_STATUS_NEW)==0) {
        ACCELEROMETER_UPDATING = 0;
        return 2;  // NO DATA
    }    
    // OUTX, OUTY, OUTZ
    ACC_X = (uint16_t) ACC_Read(OUT_X_L);
    ACC_X = ACC_X << 4;
    ACC_X = ACC_X | (((uint16_t) ACC_Read(OUT_X_H))>>4);
    if((ACC_X & 0x0800)!=0)ACC_X |= 0xF000;
    ACC_Y = (uint16_t) ACC_Read(OUT_Y_L);
    ACC_Y = ACC_Y << 4;
    ACC_Y = ACC_Y | (((uint16_t) ACC_Read(OUT_Y_H))>>4);
    if((ACC_Y & 0x0800)!=0)ACC_Y |= 0xF000;
    ACC_Z = (uint16_t) ACC_Read(OUT_Z_L);
    ACC_Z = ACC_Z << 4;
    ACC_Z = ACC_Z | (((uint16_t) ACC_Read(OUT_Z_H))>>4);
    if((ACC_Z & 0x0800)!=0)ACC_Z |= 0xF000;

    //return (int16_t)Data;
    // APPARENTLY IT IS 12-BITS LEFT JUSTIFIED
    // EX. 350mg = 015E   --> H:15 L:E0
    // EX. -350mg = FEA2  --> H:EA L:20
    // ACCX 
    acc_out_x[ACCELEROMETER_INDEX][1]  = (int16_t) ACC_X;
    // ACCY 
    acc_out_y[ACCELEROMETER_INDEX][1]  = (int16_t) ACC_Y;
    // ACCZ
    acc_out_z[ACCELEROMETER_INDEX][1]  = (int16_t) ACC_Z;
    
    acc_TimeStamp[ACCELEROMETER_INDEX][1] = globalTimestamp;
    
    OVR_TEST = (acc_status[ACCELEROMETER_INDEX][1] & ACC_STATUS_OVR);
    if(OVR_TEST == 0){
        OVR_TEST = 0;
    }

    ACCELEROMETER_INDEX++;
    if(ACCELEROMETER_INDEX == ACCELEROMETER_SIZE){
        ACCELEROMETER_INDEX = 0;
        if(ACCELEROMETER_BFULL == SIGNAL_OFF){
            for(ACC_IDX=0; ACC_IDX<ACCELEROMETER_SIZE; ACC_IDX++){
                acc_out_x[ACC_IDX][0] = acc_out_x[ACC_IDX][1];
                acc_out_y[ACC_IDX][0] = acc_out_y[ACC_IDX][1];
                acc_out_z[ACC_IDX][0] = acc_out_z[ACC_IDX][1];
                acc_TimeStamp[ACC_IDX][0] = acc_TimeStamp[ACC_IDX][1];
                acc_status[ACC_IDX][0] = acc_status[ACC_IDX][1];
            }
            ACCELEROMETER_BFULL = SIGNAL_ON;
        }
    }

    ACCELEROMETER_UPDATING = 0;

    if(OVR_TEST!=0) return 1;
    return 0; // 1: OVERRUN, 0: NEW DATA OK
}
//! -----------------------------------------------------------------------------------
//! \brief  Function to read full accelerometer data in TIM2 interrupt
//!
//! \note   ....
//! \note   ....
//!
//! \param [in] nothing
//!-----------------------------------------------------------------------------------
void ACC_CFG_READ_XYZ(){
    UIF_ACC_SPIConfig(DEV_ACC);  
    if (accelerometer_ok == SIGNAL_ON)  ACC_DataRead();                
    else  accelerometer_ok = ACC_Config();                
    UIF_ACC_SPIConfig(DEV_UIF);  
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
