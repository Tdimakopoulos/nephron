note over WS MB 
	user interface asks patient
	to step on scale. He does so
	and ackknowledges by pressing OK.
end note

MB -> CB: dataId_WeightRequest
note over MB CB
	tdMsgOnly varMsg;
	varMsg.header.recipientId = whoId_CB;
	varMsg.header.msgSize  = sizeof(tdMsgOnly);
	varMsg.header.dataId   = dataID_weightRequest;
	varMsg.header.msgCount = 7;
	varMsg.header.issuedBy = whoId_MB;
	putData(&varMsg);
end note

note over CB
	CB now listens for
	weight scale for
	at least 60 sec
end note 

alt weight scale is available

	WS -> CB: weight scale BT visible

	CB -> MB: dataId_Ack
	note over CB MB
		tdMsgOnly varMsg;
		varMsg.header.recipientId = whoId_MB;
		varMsg.header.msgSize  = sizeof(tdMsgOnly);
		varMsg.header.dataId   = dataID_ack;
		varMsg.header.msgCount = 7; // copy from msg to ack!!
		varMsg.header.issuedBy = whoId_CB;
		putData(&varMsg);
	end note
	
	note over MB    
		expect a weightData msg.
	end note  

	WS -> CB: weight data

	note over CB
		rewrap data payload
	end note

	CB -> MB: dataId_weightData
	note over CB MB
		tdMsgWeightData varMsg;
		varMsg.header.recipientId = whoId_MB;
		varMsg.header.msgSize  = sizeof(tdMsgWeightData);
		varMsg.header.dataId   = dataID_weightData;
		varMsg.header.msgCount = 12;
		varMsg.header.issuedBy = whoId_CB;
		varMsg.weightData.Weight = 860; // 0.1kg
		varMsg.weightData.[...]
		putData(&varMsg);
	end note
	
	note over MB
		process weight data
	end note

	alt OK
		MB -> CB: dataId_ack
		note over CB MB
			tdMsgOnly varMsg;
			varMsg.header.recipientId = whoId_CB;
			varMsg.header.msgSize  = sizeof(tdMsgOnly);
			varMsg.header.dataId   = dataID_ack;
			varMsg.header.msgCount = 12; // copy from msg to ack!!
			varMsg.header.issuedBy = whoId_MB;
			putData(&varMsg);
		end note
		
	else Problem with UART or Operating System
		MB->CB: dataId_nack
		note over CB MB
			tdMsgOnly varMsg;
			varMsg.header.recipientId = whoId_CB;
			varMsg.header.msgSize  = sizeof(tdMsgOnly);
			varMsg.header.dataId   = dataID_nack;
			varMsg.header.msgCount = 12; // copy from msg to nack!!
			varMsg.header.issuedBy = whoId_MB;
			putData(&varMsg);
		end note

		note left of MB
			handle case SW/HW error?
		end note
	end

else weight scale is not available
	CB -> MB: dataId_nack
	note over CB MB
		tdMsgOnly varMsg;
		varMsg.header.recipientId = whoId_CB;
		varMsg.header.msgSize  = sizeof(tdMsgOnly);
		varMsg.header.dataId   = dataID_ack;
		varMsg.header.msgCount = 7; // copy from msg to nack!!
		varMsg.header.issuedBy = whoId_MB;
		putData(&varMsg);
	end note
	
	note left of MB
		handle case weight scale not available
	end note
end