note left of MB
	WAKD is requested to change its state
	Reason can either be msg from Backend
	or from user operating SP GUI
	or internal reason (Flow Control Task)
end note

MB -> RTB: dataID_changeWAKDStateFromTo
note over MB RTB
  // WAKD knows its WAKD state
  tdWakdStates currentState;
  currentState = wakdStates_Dialysis;
  
  // craete msg to send
  tdMsgWakdStateFromTo myMsg;
  myMsg.header.recipientId = whoId_RTB;
  myMsg.header.msgSize = sizeof(tdMsgWakdStateFromTo);
  myMsg.header.dataId = dataID_changeWAKDStateFromTo;
  myMsg.header.msgCount = 123; 
  myMsg.header.issuedBy = whoId_MB;
  myMsg.fromWakdState = currentState;
  myMsg.toWakdState = wakdStates_Regen1;
  putData(&varMsg);
end note

alt fromWakdState == current state of RTB
	note over RTB
		RTB moves to new state "toWakdState".
		RTB applies stored actuator values.
		(Blood Pump, Fluidic Pump, Pola Volt, Fluidic Switches)
	end note
	
	RTB -> MB: dataId_ack
	note over RTB MB
		tdMsgOnly varMsg;
		varMsg.header.recipientId = whoId_MB;
		varMsg.header.msgSize  = sizeof(tdMsgOnly);
		varMsg.header.dataId   = dataID_ack;
		varMsg.header.msgCount = 123; // copy from msg to ack!!
		varMsg.header.issuedBy = whoId_RTB;
		putData(&varMsg);
	end note
	
	note over MB
		// update change of state
		currentState = wakdStates_Regen1
	end note
	
	note over MB
		/* Inform remainder of system (UI, SP, FlowControl, ...) about new state */
	end note
	
else fromWakdState != state of RTB
	note over RTB
		RTB has different state than known by MB
		Possible reason: Safety-event caused RTB
		to change state autonomously.
	end note
	
	RTB -> MB: dataId_nack
	note over RTB MB
		tdMsgOnly varMsg;
		varMsg.header.recipientId = whoId_MB;
		varMsg.header.msgSize  = sizeof(tdMsgOnly);
		varMsg.header.dataId   = dataID_nack;
		varMsg.header.msgCount = 123; // copy from msg to nack!!
		varMsg.header.issuedBy = whoId_RTB;
		putData(&varMsg);
	end note
	
	note over MB
		Request failed, react accordingly.
		e.g. ask RTB about its current state
		using "dataID_statusRequest"
	end note
	
end
  