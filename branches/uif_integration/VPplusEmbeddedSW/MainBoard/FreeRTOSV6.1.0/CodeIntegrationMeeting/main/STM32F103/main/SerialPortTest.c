// ---------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// ---------------------------------------------------------------------------------------------------------------------
//
//! \file   SPORT_TEST.c
//! \brief  Test of USART1, USART2, USART3, UART4, UART5
//!
//! Communication Tests
//!
//! \author  Dudnik G.
//! \date    05.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// Includes
// ---------------------------------------------------------------------------------------------------------------------
#include "main.h"

#include <stdint.h>
#include <string.h>

// ---------------------------------------------------------------------------------------------------------------------
// Constant definitions
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
// Function definitions
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CLR_OUT_COMM_BUFFERS
//!         
//!
//! Routine to be used in USART_Rx int or main
//!
//! \param[in,out]  none
//! \return         nothing
// ---------------------------------------------------------------------------------------------------------------------
void CLR_OUT_COMM_BUFFERS(void){
    uint16_t ix = 0;
    for(ix=0;ix<TXRX_MAXVALUE;ix++){
        ztx_out_str[ix]=0;
        zrx_out_str[ix]=0;
    }
    out_reclen = 0;
}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  RS485_Transaction. Reads Data and analyzes when the message is ready. Sends answer.
//!         
//!
//! Routine to be used in USART_Rx int or main.
//! Reads Data and analyzes when the message is ready. Sends answer.
//!
//! \param[in,out]  none
//! \return         nothing
// ---------------------------------------------------------------------------------------------------------------------
uint16_t SerialPort_Transaction (USART_TypeDef* USARTx){
    uint16_t iq = 0;
    uint8_t net_address = 0;
    uint8_t zrx_function[2];

    uint16_t usart_rxdata = 0xFFFF;
    uint16_t usart_rxchck = 0x0000;

    if (MB_SYSTEM_READY==DEVICE_NOTREADY) return usart_rxchck;

    usart_rxdata =  USART_READ_CHECKERRORS (USARTx);
    usart_rxchck =  usart_rxdata & 0xF000;
    if(usart_rxchck!=0) return usart_rxchck;

    
    if(usart_rxdata == ':') {		
        out_reclen = 0;
        OUT_COMM_FLAG = 0x80;
    }
    if (OUT_COMM_FLAG == 0x80){		
        if(out_reclen<TXRX_MAXVALUE){
            zrx_out_str[out_reclen] = usart_rxdata;
            if (zrx_out_str[out_reclen] == '\n') {
                OUT_COMM_FLAG = 0xFF;
            }	
            out_reclen ++;
        }
    }
    if(OUT_COMM_FLAG == 0xFF) {
if (USARTx == USART1){
#ifndef MB_GATEWAY_RTMCB_PC // redirect data to USB
        // ---------------------------------------------------
        // LoopBack Function
        // ---------------------------------------------------
        OUT_STR_TX_LEN = Make_LoopBack_Packet(out_reclen, zrx_out_str, &ztx_out_str[0]);
        // ---------------------------------------------------
        for(iq=0;iq<OUT_STR_TX_LEN;iq++) {    
            USART_SendData(USARTx, ztx_out_str[iq]);
            while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
        }
#endif
}
else{
        // ---------------------------------------------------
        // LoopBack Function
        // ---------------------------------------------------
        OUT_STR_TX_LEN = Make_LoopBack_Packet(out_reclen, zrx_out_str, &ztx_out_str[0]);
        // ---------------------------------------------------
        for(iq=0;iq<OUT_STR_TX_LEN;iq++) {    
            USART_SendData(USARTx, ztx_out_str[iq]);
            while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
        }
}
        // ---------------------------------------------------
        CLR_OUT_COMM_BUFFERS();	
    }
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  SerialPort_SendPacket
//!         
//!
//! Prepares a Message and Sends through USARTx port
//!
//! \param[in]      USARTx
//! \return         no of bytes sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t SerialPort_SendPacket (USART_TypeDef* USARTx){
    uint16_t iq = 0;
    // ---------------------------------------------------
    // LoopBack Function
    // ---------------------------------------------------
    OUT_STR_TX_LEN = Make_Testing_Packet(&ztx_out_str[0]);
    // ---------------------------------------------------
    for(iq=0;iq<OUT_STR_TX_LEN;iq++) {    
        USART_SendData(USARTx, ztx_out_str[iq]);
        while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
    }
    // ---------------------------------------------------
    return OUT_STR_TX_LEN;
}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Make_LoopBack_Packet
//!         
//!
//! Prepares a return packet with the data received
//!
//! \param[in]      quantity of data received
//! \param[in]      pointer to the reception buffer (by value)
//! \param[in]      pointer to the transmission buffer (by reference)
//! \return         quantity of data to be sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t Make_LoopBack_Packet(uint16_t rxpacketlen, uint8_t * rx_inp_str, uint8_t * tx_out_str){	

    uint16_t ix = 0;
    for(ix=0; ix <= rxpacketlen; ix++)			tx_out_str[ix] = rx_inp_str[ix];
    for(ix= (rxpacketlen+1); ix < TXRX_MAXVALUE; ix++)	tx_out_str[ix] = '\0';
    
    return (rxpacketlen+1);
}

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Make_Testing_Packet
//!         
//!
//! Prepares a testing packet 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
//!
//! \param[in]      pointer to the transmission buffer (by reference)

//! \return         quantity of data to be sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t Make_Testing_Packet(uint8_t * tx_out_str){	

    uint16_t ix = 0;
    for(ix=0; ix <26; ix++)                 tx_out_str[ix] = 'A'+ix;
    for(ix= 26; ix < TXRX_MAXVALUE; ix++)   tx_out_str[ix] = '\0';
    
    return 27;
}
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  RS485_Transaction. Reads Data and analyzes when the message is ready. Sends answer.
//!         
//!
//! Routine to be used in USART_Rx int or main.
//! Reads Data and analyzes when the message is ready. Sends answer.
//!
//! \param[in,out]  none
//! \return         nothing
// ---------------------------------------------------------------------------------------------------------------------
uint16_t USB_Transaction (){
    uint16_t iq = 0;
    uint8_t net_address = 0;
    uint8_t zrx_function[2];
    uint16_t usart_rxdata = 0xFFFF;
    uint16_t usart_rxchck = 0x0000;

    if (MB_SYSTEM_READY==DEVICE_NOTREADY) return usart_rxchck;

    usart_rxdata =  USART_READ_CHECKERRORS (USART1);
    usart_rxchck =  usart_rxdata & 0xF000;
    if(usart_rxchck!=0) return usart_rxchck;

    if((usart_rxdata == '\0')&&(INP_COMM_USB_FLAG==0)) return usart_rxchck;

    if((usart_rxdata !=0)&&(INP_COMM_USB_FLAG==0)) {		
        out_usb_reclen_0 = 0;
        INP_COMM_USB_FLAG = 0x80;
    }
    if (INP_COMM_USB_FLAG == 0x80){		
        if(out_usb_reclen_0 <(TXRX_RS422_USB_MAXVALUE-24)){
            if (usart_rxdata == '\0') {
                INP_COMM_USB_FLAG = 0x99; 
            }	
            else{
                zrx_out_usb_str_0[out_usb_reclen_0] = usart_rxdata;
                out_usb_reclen_0 ++;
            }
        }
        else{
            INP_COMM_USB_FLAG = 0;
            out_usb_reclen_0 = 0;
        }
    }
    if(INP_COMM_USB_FLAG == 0x99) {
        // 20120504 USB --> RTMCB [LINK TO RTMCB]
#ifdef MB_GATEWAY_RTMCB_PC // redirect data to RTMCB
        for(iq=0;iq<=out_usb_reclen_0;iq++) {    
            USART_SendData(UART4, zrx_out_usb_str_0[iq]);
            while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET) {}
            zrx_out_usb_str_0[iq] = 0;
        } // end send
        // Generates INTERRUPT to let RTMCB Know about the new Data
        Generate_RTMCB_Interrupt(100);   // 100=20uS (tEXTIpw = 10ns)
#endif
        out_usb_reclen_0 = 0;
        INP_COMM_USB_FLAG = 0x00;
    }        
    return usart_rxchck;   
}

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
