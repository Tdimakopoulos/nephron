note left of SP
	1/3 possibilities.
	SP is communicating with CB
end note	
note left of SP 
	User interface on SP asks patient
	to step on scale. He does so
	activating the weight scale) and
	ackknowledges by pressing OK.
end note
SP -> CB: dataId_WeightRequest
note over CB
	looks into header for
	... size information
end note
CB -> MB: bytewise forward
note over MB
	looks into header for
	... recipient: CB
	... size info
end note
MB -> CB: bytewise forward
note over CB
	looks into header for
	... id information
end note
note over CB
	CB copy sender ID (SP)
	to recipient of Ack
end note
CB -> MB: dataId_Ack
note over MB
	looks into header for
	... recipient: SP
	... size info
end note
MB -> CB: bytewise forward
note over CB
	looks into header for
	... id information
	... size information
end note
CB -> SP: bytewise forward