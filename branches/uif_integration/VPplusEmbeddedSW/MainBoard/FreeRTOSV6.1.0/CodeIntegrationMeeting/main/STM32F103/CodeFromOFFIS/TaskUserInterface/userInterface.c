/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "nephron.h"
	#include "userInterface.h"
	#include "fcHelperFunc.h"
	#include "ioHelperFunc.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
	#include "userInterface.h"
	#include "..\\TaskFlowControl\\fcHelperFunc.h"
    #include "..\\ioHelperFunc.h"
#endif

#ifndef VP_SIMULATION
#include "..\\..\\main\\uif_application.h"
#include "..\\..\\main\\main.h"
#endif /* VP_SIMULATION */


#define PREFIX "OFFIS FreeRTOS task UI"

#define msgPatient "OFFIS FreeRTOS task UI: send 2 PATIENT"
#define msgDoctor "OFFIS FreeRTOS task UI: send 2 DOCTOR"
#define msgDebug "OFFIS FreeRTOS task UI: the device should:"

uint8_t alarm_ack = 1;

void tskUI( void *pvParameters ) {
    heartBeatUI++;
    taskMessage("I", PREFIX, "Starting User Interface task!");

    xQueueHandle *qhDISPin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
    xQueueHandle *qhUIin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhUIin;

    tdQtoken Qtoken;
    Qtoken.command = command_DefaultError;
    Qtoken.pData = NULL;

    //uint8_t alarm_received = 0;
    uint8_t alarm_sent = 0;

#ifdef DEBUG_STACK
    unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
    taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
#endif

    // Here we have a very crude HACK to have a rudimentary communication with a user GUI
    // that is implemented in Simulink. We are using an integer value to call predefined messages
    // in that GUI. We are sending this value as an actor value. THIS IS A HACK.
    // tdWakdStates stateOfWakdAsKnownByUI = wakdStates_UndefinedInitializing;
    // setActuatorInSimulink (ACTUATOR_MESSAGEUI, 1, stateOfWakdAsKnownByUI);
	
        uint16_t user_command = 0;
    for ( ;; ) {
	
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every CBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatUI++;

#ifdef DEBUG_STACK
        // Summary
        // Each task maintains its own stack, the total size of which is specified when the task is created.
        // uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
        // space allocated to it. This value is called the stack 'high water mark'.
        // Return Values
        // The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
        // The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
        // value of zero indicates that the task has actually overflowed its stack already.
        unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
        if (stackHighWaterMark != stackHighWaterMarkNew) {
            stackHighWaterMark = stackHighWaterMarkNew;
            if (stackHighWaterMark == 0) {
                taskMessage("E", PREFIX, "OUT OFF STACK!!");
            } else if (stackHighWaterMark < 32) {
                taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
            }
        }
#endif /* DEBUG_STACK */

               // handling commands sent by the UIF
               user_command = MB_USER_COMMAND;
               if ((user_command & MB_COMMAND_SHUTDOWN) == MB_COMMAND_SHUTDOWN) {
                 sendMessage(PREFIX, qhDISPin, dataID_shutdown);
                 user_command &= ~MB_COMMAND_SHUTDOWN;
                 MB_USER_COMMAND &= ~MB_COMMAND_SHUTDOWN;
               }
               if ((user_command & MB_COMMAND_CHANGE_OPMODE) == MB_COMMAND_CHANGE_OPMODE) {
                  if ((user_command & MB_COMMAND_GOTO_DIALYSIS) == MB_COMMAND_GOTO_DIALYSIS) {
                      forceChangeState(wakdStates_Dialysis, qhDISPin);
                      user_command &= ~MB_COMMAND_GOTO_DIALYSIS;
                      MB_USER_COMMAND &= ~MB_COMMAND_GOTO_DIALYSIS;
                  } else if ((user_command & MB_COMMAND_GOTO_REGEN1) == MB_COMMAND_GOTO_REGEN1) {
                      forceChangeState(wakdStates_Regen1, qhDISPin);
                      user_command &= ~MB_COMMAND_GOTO_REGEN1;
                      MB_USER_COMMAND &= ~MB_COMMAND_GOTO_REGEN1;
                  } else if ((user_command & MB_COMMAND_GOTO_ULTRA) == MB_COMMAND_GOTO_ULTRA) {
                      forceChangeState(wakdStates_Ultrafiltration, qhDISPin);
                      user_command &= ~MB_COMMAND_GOTO_ULTRA;
                      MB_USER_COMMAND &= ~MB_COMMAND_GOTO_ULTRA;
                  } else if ((user_command & MB_COMMAND_GOTO_REGEN2) == MB_COMMAND_GOTO_REGEN2) {
                      forceChangeState(wakdStates_Regen2, qhDISPin);
                      user_command &= ~MB_COMMAND_GOTO_REGEN2;
                      MB_USER_COMMAND &= ~MB_COMMAND_GOTO_REGEN2;
                  }
                  user_command &= ~MB_COMMAND_CHANGE_OPMODE;
                  MB_USER_COMMAND &= ~MB_COMMAND_CHANGE_OPMODE;
              }
              if ((user_command & MB_COMMAND_START_OPERATION) == MB_COMMAND_START_OPERATION) {
                  forceChangeState(wakdStates_NoDialysate, qhDISPin);
                  user_command &= ~MB_COMMAND_START_OPERATION;
                  MB_USER_COMMAND &= ~MB_COMMAND_START_OPERATION;
              }
              if ((user_command & MB_COMMAND_GET_WEIGHT) == MB_COMMAND_GET_WEIGHT) {
                  sendMessage(PREFIX, qhDISPin, dataID_weightRequest);
                  user_command &= ~MB_COMMAND_GET_WEIGHT;
                  MB_USER_COMMAND &= ~MB_COMMAND_GET_WEIGHT;
              }
              if ((user_command & MB_COMMAND_START_SEWSTREAM) == MB_COMMAND_START_SEWSTREAM) {
                  user_command &= ~MB_COMMAND_START_SEWSTREAM;
                  MB_USER_COMMAND &= ~MB_COMMAND_START_SEWSTREAM;
              }
              if ((user_command & MB_COMMAND_STOP_SEWSTREAM) == MB_COMMAND_STOP_SEWSTREAM) {
                  user_command &= ~MB_COMMAND_STOP_SEWSTREAM;
                  MB_USER_COMMAND &= ~MB_COMMAND_STOP_SEWSTREAM;
              }
              if ((user_command & MB_COMMAND_START_NIBP) == MB_COMMAND_START_NIBP) {
                  user_command &= ~MB_COMMAND_START_NIBP;
                  MB_USER_COMMAND &= ~MB_COMMAND_START_NIBP;
              }

        if ( xQueueReceive( qhUIin, &Qtoken, UI_PERIOD) == pdPASS ) {
            // We did found something in the queue of UI: User Interface
#ifdef DEBUG_UI
            taskMessage("I", PREFIX, "Data in UI queue!");
#endif
            switch (Qtoken.command) {
            	case command_ButtonEvent: {
            		switch ((tdButtons)Qtoken.pData) {
            			case btn_up: {
            				taskMessage("I", PREFIX, "Pressed UP button");
            				break;
            			}
            			case btn_down: {
            				taskMessage("I", PREFIX, "Pressed DOWN button");
            				break;
            			}
            			case btn_left: {
            				taskMessage("I", PREFIX, "Pressed LEFT button");
            				break;
            			}
            			case btn_right: {
            				taskMessage("I", PREFIX, "Pressed RIGHT button");
            				break;
            			}
            			case btn_sel: {
                                        taskMessage("I", PREFIX, "Pressed SELECT button");
            				break;
            			}
            			case btn_reset: {
                                        taskMessage("I", PREFIX, "Pressed RESET button");
            				break;
            			}
            			default:
            				taskMessage("E", PREFIX, "Unknown button pressed!");
            		}
            		break;
            	}
                case command_DefaultError: {
                  tdErrMsg error = (tdErrMsg) Qtoken.pData;
                  switch(error) {
                    case errmsg_NoError:
                      //do nothing
                    break;
                    case errmsg_TimeoutOnDispQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnDispQueue;
                      break;
                    case errmsg_TimeoutOnDsQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnDsQueue;
                      break;
                    case errmsg_TimeoutOnFcQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnFcQueue;
                      break;
                    case errmsg_TimeoutOnRtbpaQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnRtbpaQueue;
                      break;
                    case errmsg_TimeoutOnCbpaQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnCbpaQueue;
                      break;
                    case errmsg_TimeoutOnSccQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnSccQueue;
                      break;
                    case errmsg_TimeoutOnUiQueue:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_TimeoutOnUiQueue;
                      break;
                    case errmsg_QueueOfDispatcherTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfDispatcherTaskFull;
                      break;
                    case errmsg_QueueOfDataStorageTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfDataStorageTaskFull;
                      break;
                    case errmsg_QueueOfSystemCheckCalibrationTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfSystemCheckCalibrationTaskFull;
                      break;
                    case errmsg_QueueOfCommunicationBoardAbstractionTaskFull: // #10
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfCommunicationBoardAbstractionTaskFull;
                      break;
                    case errmsg_QueueOfRealtimeBoardAbstractionTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfRealtimeBoardAbstractionTaskFull;
                      break;
                    case errmsg_QueueOfFlowControlTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfFlowControlTaskFull;
                      break;
                    case errmsg_QueueOfReminderTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfReminderTaskFull;
                      break;
                    case errmsg_QueueOfUserInterfaceTaskFull:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_QueueOfUserInterfaceTaskFull;
                      break;
                    case errmsg_nackFromCB:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_nackFromCB;
                      break;
                    case errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask;
                      break;
                    case errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask;
                      break;
                    case errmsg_IsrMbButtonEventLost:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_IsrMbButtonEventLost;
                      break;
                    case errmsg_DispDataWeightSensorReadEventLost:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispDataWeightSensorReadEventLost;
                      break;
                    case errmsg_DispDataPatientProfielWriteEventLost: // #20
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispDataPatientProfielWriteEventLost;
                      break;
                    case errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask;
                      break;
                    case errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask;
                      break;
                    case errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask;
                      break;
                    case errmsg_DispChangeWAKDStateFromToEventLost:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_DispChangeWAKDStateFromToEventLost;
                      break;
                    case errmsg_pvPortMallocFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_pvPortMallocFailed;
                      break;
                    case errmsg_getDataFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_getDataFailed;
                      break;
                    case errmsg_putDataFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_putDataFailed;
                      break;
                    case errmsg_ConfiguringWakdStateFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ConfiguringWakdStateFailed;
                      break;
                    case errmsg_ConfiguredStateTimesMismatch:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ConfiguredStateTimesMismatch;
                      break;
                    case errmsg_ErrFlowControl: // #30
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ErrFlowControl;
                      break;
                    case errmsg_ErrSCC:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ErrSCC;
                      break;
                    case errmsg_ErrSendNack:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ErrSendNack;
                      break;
                    case errmsg_ErrSendAck:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ErrSendAck;
                      break;
                    case errmsg_command_WakeupCallEventLost:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_command_WakeupCallEventLost;
                      break;
                    case errmsg_CommunicationBoardUnreachable:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_CommunicationBoardUnreachable;
                      break;
                    case errmsg_NotInListForAckOnMsgCBPA:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_NotInListForAckOnMsgCBPA;
                      break;
                    case errmsg_PutDataFunctionFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_PutDataFunctionFailed;
                      break;
                    case errmsg_UnknownRecipient:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_UnknownRecipient;
                      break;
                    case errmsg_SDcardError:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_SDcardError;
                      break;
                    case errmsg_FlowControlStateIsUnknown: // #40
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_FlowControlStateIsUnknown;
                      break;
                    case errmsg_IsrCbReceivedFaultySlipMsg:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_IsrCbReceivedFaultySlipMsg;
                      break;
                    case errmsg_SmartphoneUnreachable:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_SmartphoneUnreachable;
                      break;
                    case errmsg_ReadingSDcardFailed:
                      MB_WAKD_ERRORS_Status = (MB_WAKD_ERRORS_Status << 8) + errmsg_ReadingSDcardFailed;
                      break;
                    default:
                      break;
                  }
                  break;
                }
                case command_InformGuiConnectBag: {
                    // Here we would usually ask the patient to do something and press OK.
                    // For this dummy simulation we pretend that the patient has done so.
                    // Attached to the incoming command is the information to what state
                    // we should change.
                    taskMessage("I", PREFIX, "Patient acknowledges connecting bag by pressing 'OK' on WAKD UI.");
                    forceChangeState((tdWakdStates)Qtoken.pData, qhDISPin);
                    break;
                }
                case command_InformGuiDisconnectBag: {
                    // Here we would usually ask the patient to do something and press OK.
                    // For this dummy simulation we pretend that the patient has done so.
                    // The GUI has to account for it!
                    taskMessage("I", PREFIX, "Patient acknowledges Disconnecting bag by pressing 'OK' on WAKD UI.");
                    sFree((void *)&(Qtoken.pData));
                    break;
                }				
                case command_UseAtachedMsgHeader: {
                    // Look at header for further information.
                    tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
                    switch (dataId) {
                        case (dataID_physiologicalData): {
#ifdef DEBUG_UI
                            printPhysiologicalData(PREFIX, ((tdPhysiologicalData *)(Qtoken.pData)));
#endif
#ifndef VP_SIMULATION
                            P_DATA_NA_ECP1 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataI.Sodium;
                            P_DATA_K_ECP1 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataI.Potassium;
                            P_DATA_PH_ECP1 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataI.pH;
                            P_DATA_UREA_ECP1 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataI.Urea;
                            P_DATA_NA_ECP2 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataO.Sodium;
                            P_DATA_K_ECP2 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataO.Potassium;
                            P_DATA_PH_ECP2 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataO.pH;
                            P_DATA_UREA_ECP2 = ((tdPhysiologicalData *)(Qtoken.pData))->ECPDataO.Urea;
                            MB_WAKD_Status = ((tdPhysiologicalData *)(Qtoken.pData))->currentState;
#endif /* VP_SIMULATION */
                            sFree((void *)&(Qtoken.pData));
                            break;
                        }
                        case (dataID_physicalData): {
#ifdef DEBUG_UI
                        	printPhysicalData(PREFIX, ((tdPhysicalsensorData *)(Qtoken.pData)));
#endif
#ifndef VP_SIMULATION
                            MB_WAKD_FPS_Pressure = ((tdPhysicalsensorData *)(Qtoken.pData))->PressureFCI.Pressure;
                            MB_WAKD_BPS_Pressure = ((tdPhysicalsensorData *)(Qtoken.pData))->PressureBCI.Pressure;
                            MB_WAKD_BTS_InletTemperature = ((tdPhysicalsensorData *)(Qtoken.pData))->TemperatureInOut.TemperatureInlet_Value;
                            MB_WAKD_BTS_OutletTemperature = ((tdPhysicalsensorData *)(Qtoken.pData))->TemperatureInOut.TemperatureOutlet_Value;
                            MB_WAKD_DCS_Conductance = ((tdConductivitySensorData *)(Qtoken.pData))->Cond_FCQ;
                            MB_WAKD_DCS_Susceptance = ((tdConductivitySensorData *)(Qtoken.pData))->Cond_FCR;
                            MB_WAKD_DCS_Status = ((tdDevicesStatus *)(Qtoken.pData))->statusDCS;
#endif /* VP_SIMULATION */
                            sFree((void *)&(Qtoken.pData));
                            break;
                        }
                        case (dataID_weightData): {
                            // This weight measure needs to be checked by the patient.
                            // This same measure has also already been forwarded to the communication board to forward
                            // to the smartphone GUI. So the checking by the patient can come back either from task UI or SP.
#if defined DEBUG_UI || defined SEQ2
                            taskMessage("I", PREFIX, "Please verify correctness of weight measure: %f", ((double)(((tdMsgWeightData *)(Qtoken.pData))->weightData.Weight))/FIXPOINTSHIFT_WEIGHTSCALE);
#endif
#ifndef VP_SIMULATION
                            double weight = ((double)(((tdMsgWeightData *)(Qtoken.pData))->weightData.Weight))/FIXPOINTSHIFT_WEIGHTSCALE;
                            P_DATA_WEIGHT = (uint16_t) weight;
                            taskMessage("I", PREFIX, "Check weight: %u", P_DATA_WEIGHT);
#endif
                            sFree((void *)&(Qtoken.pData));
                            break;
                        }
                        case dataID_systemInfo: {
                            // Look into the message to know which error this is
#if defined DEBUG_UI
                            taskMessage("I", PREFIX, "Received message '%s'from SCC.", infoMsgToString(((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum));
#endif
                            switch (((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum) {
                                case dectreeMsg_detectedSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDoctor, "Dysfunction detected. Plasma pump stopped, blood pump still running");
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDebug, "stop Plasma pump and keep blood pump running");
#endif
#ifndef VP_SIMULATION
                                    MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedSorDys;
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBPorFPhigh: {
#if defined DEBUG_UI_MSG || defined SEQ11
                                    taskMessage("I", msgDoctor,  "ALARM: pressure (high) excess, all pumps stopped");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDebug,  "pressure too high, stop device here");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBPorFPhigh;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBPorFPlow: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDoctor,  "ALARM: pressure (low) excess, all pumps stopped");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDebug,  "pressure too low, stop device here");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBPorFPlow;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBTSoTH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "temperature (high) excess at BTSo, stop device here");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSo, all pumps stopped");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBTSoTH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBTSoTL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "temperature (low) excess at BTSo, stop device here");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSo, all pumps stopped");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBTSoTL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBTSiTH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "temperature (high) excess at BTSi, stop device here");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSi, all pumps stopped");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBTSiTH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedBTSiTL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "temperature (low) excess at BTSi, stop device here");
                                    taskMessage("I", msgPatient, "pumps stopped - contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSi, all pumps stopped");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedBTSiTL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedpumpBaccDevi: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "Blood Pump Flow deviation - ");
                                    taskMessage("E", msgDebug,  "... something should happen now, but still undefined what");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedpumpBaccDevi;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedpumpFaccDevi: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "Fluidic Pump Flow deviation");
                                    taskMessage("E", msgDebug,  "... something should happen now, but still undefined what");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedpumpFaccDevi;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhAAbsL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: pH low");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhAAbsL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhAAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: pH high");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhAAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhAbsLorPhAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "pH out of normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhAbsLorPhAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedNaAbsLorNaAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "Na+ out of normal range. Plasma pump stopped, blood pump still running.");
                                    taskMessage("I", msgDebug,   "Stop plasma pump.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedNaAbsLorNaAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "Na+ trend out of normal range. No change in device function initiated.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }								
                                case dectreeMsg_detectedKAbsLorKAbsHandSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "replace cartridge");
                                    taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again)");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKAbsLorKAbsHandSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKAbsLorKAbsHnoSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "K+ out of normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKAbsLorKAbsHnoSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKAAL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,   "K+ Alarm(low). Stopped plasma pump. Blood pump still running");
                                    taskMessage("I", msgDebug,   "K+ Alarm(low). Stop plasma pump.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKAAL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKAAHandKincrease: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,   "K+ Alarm(high) < Kmeasafter");
                                    taskMessage("I", msgPatient, "replace sorbent cartridge and contact physician");
                                    taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again)");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKAAHandKincrease;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKAAHnoKincrease: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,   "K+ Alarm(high) and increasing behing the sorbend filter. Stopped plasma pump. Blood pump still runnning");
                                    taskMessage("I", msgDebug,   "K+ Alarm(high) >= Kmeasafter. Stop plasma pump.");
                                    taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient what to do (stop device.. do corrective actions - 2 be specified.. turn on device again)");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKAAHnoKincrease;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedUreaAAbsHandSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "replace cartridge");
                                    taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again)");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedUreaAAbsHandSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedUreaAAbsHnoSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "Urea out of normal range (high)");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedUreaAAbsHnoSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKTrdLPsT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "K+ Trend is decreasing too fast");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKTrdLPsT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedKTrdofnriandSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "replace cartridge");
                                    taskMessage("I", msgDoctor,  "K+ Trend is decreasing too fast and sorbend dysfunction detected!");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedKTrdofnriandSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhTrdLPsT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "pH decrease out of normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhTrdLPsT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhTrdHPsT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "pH increase out of normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhTrdHPsT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedPhTrdofnriandSorDys: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDebug,   "FC is controlling pumprate to reach target range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedPhTrdofnriandSorDys;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedUreaTrdHPsT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "Urea increase out of normal range (high)");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedUreaTrdHPsT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedWghtAbsL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDebug,  "Weight out of normal range(low)");
                                    taskMessage("I", msgDebug,   "FC is controlling adsorp to reach target range");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedWghtAbsL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedWghtAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDoctor, "probably a notification to doctor needs to be specified");
                                    taskMessage("I", msgDebug,  "Weight out of normal range(high)");
                                    taskMessage("I", msgDebug,  "... Flow Control is adapting Ultrafiltration anyway,");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react immediately.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedWghtAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case dectreeMsg_detectedWghtTrdT: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgDoctor, "probably a notification to doctor needs to be specified");
                                    taskMessage("I", msgDebug,  "weight trend exceeds normal range");
                                    taskMessage("I", msgDebug,  "... Flow Control is adapting Ultrafiltration anyway,");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react immediately.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + dectreeMsg_detectedWghtTrdT;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case decTreeMsg_detectedBPsysAbsL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "BP-sys lower than normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPsysAbsL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case decTreeMsg_detectedBPsysAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "BP-sys higher than normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPsysAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case decTreeMsg_detectedBPsysAAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: BP-sys high");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPsysAAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case decTreeMsg_detectedBPdiaAbsL: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "BP-dia lower than normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPdiaAbsL;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                case decTreeMsg_detectedBPdiaAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "BP-dia higher than normal range");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPdiaAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                   break;
                                }
                                case decTreeMsg_detectedBPdiaAAbsH: {
#if defined DEBUG_UI_MSG
                                    taskMessage("I", msgPatient, "contact physician");
                                    taskMessage("I", msgDoctor,  "ALARM: BP-dia high");
                                    taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way.");
#endif
#ifndef VP_SIMULATION
                                    	MB_WAKD_ALARMS_Status = (MB_WAKD_ALARMS_Status << 8) + decTreeMsg_detectedBPdiaAAbsH;
                                    	 
#endif /* VP_SIMULATION */
                                    break;
                                }
                                default:{
                                    taskMessage("E", PREFIX, "Default in interpretSCCmessage() / switch (sccMsg). This should not have happened!");
                                    break;
                                }
                            } // of switch (((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum)
                            sFree((void *)&(Qtoken.pData));
                            break;
                        } // of case command_ProcessSCCmessage:
                        default:{
                            defaultIdHandling(PREFIX, dataId);
                            break;
                        }
                    }
                    break;
                }
                default: {
                	if ((tdCommand)Qtoken.command != command_DefaultError) {
                		defaultCommandHandling(PREFIX, Qtoken.command);
                		taskMessage("I", PREFIX, "Received command with ID:%d", (tdCommand)Qtoken.command);
                	}
                    break;
                }
            }	
        } else {
            // We now waited for "xDelayPeriod" and nothing arrived in input queue.
            // Doing nothing and keep waiting
#ifndef VP_SIMULATION
	#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
#endif
        }
    }
}
