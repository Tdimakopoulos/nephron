// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   mb_tasks.c
//! \brief  Tasks for main
//!
//! Init and Hooks
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
#include "sdcard.h"
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
#define LED_PERIOD 1000 // 100
#define SPHORE_PERIOD 100
uint32_t x=0;
uint16_t led_counter = 0;
uint16_t sphore_counter = 0;
uint32_t X1 = 0;
uint32_t X2 = 0;
uint32_t X5 = 0;
uint32_t XSD_TK = 0;
uint32_t XSD_WR = 0;
uint32_t XRTCMB = 0;
uint32_t Taken_OK = 0;
uint32_t GivenISR_OK = 0;
uint32_t CBTaken_OK = 0;
uint32_t Taken_KO = 0;
uint32_t GivenISR_KO = 0;
uint32_t CBTaken_KO = 0;
uint8_t X_CB_Task = 0;
uint8_t RTMCB_PROCEDURE_COUNTER = 1;

uint8_t NewState = 1;
uint8_t NewOPState = 0;
uint8_t StateQueue = 0;
uint8_t SPState = 100;
uint8_t QueueMs = 0;
uint8_t msgcount = 7;

uint8_t WeightFlag = 0;
uint8_t PatientWeight = 65;

uint8_t MsgSendDataId[8];
uint8_t MsgSendNr[8];
uint8_t MsgSendAck[8];
uint8_t MsgSendState[8];


portBASE_TYPE mTaken = pdFAIL;
portBASE_TYPE mGivenISR = pdFAIL;
portBASE_TYPE mCBTaken = pdFAIL;
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
extern xSemaphoreHandle xCBMBSemaphore;

/* A variable that is incremented by the idle task hook function. */
uint32_t ulIdleCycleCount = 0UL;

// -----------------------------------------------------------------------------------
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
// -----------------------------------------------------------------------------------
#define BLPUMP_SPEEDCODE    1
#define FLPUMP_SPEEDCODE    2
#define POLARIZER_SWITCHON  3
#define POLARIZER_VOLTAGE   4
#define VALVES3_POSITION    5
#define GETDATA_BY_GROUP    6
#define GETRTMCB_RTC        7
#define SETRTMCB_RTC        8
#define OPMODES_CONFIGURE   9
#define RTMCB_CMDS_MAX      10

#define BLPUMP_STOP         11
#define FLPUMP_STOP         12
#define BLPUMP_RUN          13
#define FLPUMP_RUN          14

#define VALVE_NORM          15
#define VALVE_REG           16

#define MICROFLUIDIC_OM_MIN 1
#define MICROFLUIDIC_OM_MAX 6

tdMsgAlarms ma;

// ---------------------------------------------- //
// RTMCB TASK TESTING
// BLOOD PUMP
uint8_t blpump_leftToRight = 1;                   // Creates a ramp up or down (pwm%), 1 l-t-r, 2 r-t-l
uint8_t blpump_direction = SENS_DIRECT;
uint16_t blpump_speedcode = _SPEED_CODE_STOP;  
// FILTRATE PUMP
uint8_t flpump_leftToRight = 1;
uint8_t flpump_direction = SENS_REVERSE;
uint16_t flpump_speedcode = _SPEED_CODE_STOP;    // Creates a ramp up or down (pwm%) 
// POLARIZER
uint8_t polarizer_downToUp = 1;
uint8_t polarizer_direction = SENS_DIRECT;
uint16_t polarizer_voltage = VOLTAGE_MIN;         // Up to VOLTAGE_MAX, steps: VOLTAGE_FACTOR
// 3-VALVES
uint8_t valves3_sequence = 1;
// OPERATING MODES
uint8_t opmodes_counter = 1;
// -----------------------------------------------------------------------------------
#endif                    // 20120608: MB_TASK_CMD_RTMCB  
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  prvSetupHardware
//!
//! Hardware & Variables Initialization
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void prvSetupHardware( void )
{
    uint32_t x=0;
    __disable_irq();
    // -----------------------------
    // do not allow communication...
    MB_SYSTEM_READY = 0;
    // -----------------------------
    DelayBySoft (10); 
    MB_DATA_INIT();           // Data Initialization: TBD 
    // DISABLES RTMCB RESET
    RTMCB_RESET_CMD = 0;
  
    DelayBySoft (10); 
    MB_HW_INIT();             // Hardware Initialization: ALL 

    SetDate(9, 7, 2011);
    SetTime(15,11,7);
    SetAlarmTime(15,11,15);

    ReadDate();
    x = CalculateTimeG();
    ReadTime();

    DelayBySoft (100);
    
    SUPPLIES_SWITCH_ALL_ON();   // Switch ALL ON SUPPLYES PROGRESSIVELY
    DelayBySoft (100);

    CONFIGURE_SYSTICK();        // ENABLES AFTER SUPPLIES

    // ENABLES INTS AFTER SYSTICK  
    USART_RX_INT_ENABLE(USART1);
    USART_RX_INT_ENABLE(USART2);
    USART_RX_INT_ENABLE(USART3);
#ifndef STM32F_UART4_DMA
    USART_RX_INT_ENABLE(UART4);
#endif
    USART_RX_INT_ENABLE(UART5);

    // UART4 by DMA
    RTMCB_UART4_DMA_Config();    

    // ENABLES INCOMING MESSAGES  
    ENABLE_USART1_RX_TX();       
    ENABLE_USART2_RX_TX();       
    ENABLE_USART3_RX_TX();       
    ENABLE_UART4_RX_TX();         
    ENABLE_UART5_RX_TX();         

    // ENABLES RTMCB RESET (DONE IN SYSTICK)
    RTMCB_RESET_CMD = 1;

    // Enables EXTERNAL INTS at the end...
    MB_EXTI_Config();               

    // Configure TIM2 update on overflow & Interrupt
    TIM2_Config();               

    __enable_irq();
   
    // ALLOW CRITICAL ACTIVITIES
    MB_SYSTEM_READY = DEVICE_READY;

    // SWITCH LED ON
    STM32F_GPIOOff(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        

}
// -----------------------------------------------------------------------------------
//! \brief vApplicationStackOverflowHook
//!
//! Stack Overflow debug
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
	for( ;; );
}
// -----------------------------------------------------------------------------------
//! \brief vApplicationIdleHook
//!
//! Idle hook functions MUST be called vApplicationIdleHook(), take no parameters, and return void
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationIdleHook( void )
{
	/* This hook function does nothing but increment a counter. */
	ulIdleCycleCount++;
}
// -----------------------------------------------------------------------------------
//! \brief vApplicationTickHook
//!
//! Here you build your own Systick
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationTickHook ( void ){

// This function decrements a counter to create delays.
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
  // MB TimeStamp
  globalTimestamp++;


  if (RTMCB_RESET_CMD == 1){
      switch (RTMCB_PROCEDURE_COUNTER){
          case 1:
          // DONE ALREADY AT I/O INITIALIZATION
          STM32F_GPIOOn(RTMCBuC_nRST_GPIO_PORT, RTMCBuC_nRST_GPIO_PIN);        
          RTMCB_PROCEDURE_COUNTER++;
          break;
          case 2:
          // RESET STATE
          STM32F_GPIOOff(RTMCBuC_nRST_GPIO_PORT, RTMCBuC_nRST_GPIO_PIN);        
          RTMCB_PROCEDURE_COUNTER++;
          break;
          // RESET STATE (CONT)
          case 3: case 4: case 5: 
          RTMCB_PROCEDURE_COUNTER++;
          break;
          case 16:
          default:
          // RTMCB RUN STATE
          STM32F_GPIOOn(RTMCBuC_nRST_GPIO_PORT, RTMCBuC_nRST_GPIO_PIN);        
          RTMCB_PROCEDURE_COUNTER=1;
          RTMCB_RESET_CMD = 0; // RESET OK!
          break;
      }
  }

#if 0
    ADCDMACounter++;
    if(ADCDMACounter == ADCDMA_SAMPLEPERIOD){
        ADCDMACounter = 0;
        /* Start ADC1 Software Conversion */ 
        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    }
#endif

#ifdef RTOS_SYSTICK_LED
    led_counter++;
    if(led_counter == LED_PERIOD){
        led_counter = 0;
        STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
    }
#endif

    uif_acc_data_counter++;
    if(uif_acc_data_counter == UIF_ACC_DATA_SIMUL){
        uif_acc_data_counter = 0;
#ifdef UIF_SIMULATION
        MB_UIF_DATASIMULATION();
#else
        MB_UIF_DATAREALTIME();
#endif
    }

#if 0
    uif_acc_cmd_counter++;
    if(uif_acc_cmd_counter == 5000){
        uif_acc_cmd_counter = 0;
        MB_USER_COMMAND = 0;
        // erase the User Activated commands, as they were accepted....
        if((MB_USER_COMMAND & MB_COMMAND_SHUTDOWN)!=0) MB_USER_COMMAND &= !MB_COMMAND_SHUTDOWN;
        if((MB_USER_COMMAND & MB_COMMAND_START_OPERATION)!=0) MB_USER_COMMAND &= !MB_COMMAND_START_OPERATION;
        if((MB_USER_COMMAND & MB_COMMAND_CHANGE_OPMODE)!=0) MB_USER_COMMAND &= !MB_COMMAND_CHANGE_OPMODE;
        if((MB_USER_COMMAND & MB_COMMAND_GET_WEIGHT)!=0) MB_USER_COMMAND &= !MB_COMMAND_GET_WEIGHT;
        if((MB_USER_COMMAND & MB_COMMAND_START_SEWSTREAM)!=0) MB_USER_COMMAND &= !MB_COMMAND_START_SEWSTREAM;
        if((MB_USER_COMMAND & MB_COMMAND_STOP_SEWSTREAM)!=0) MB_USER_COMMAND &= !MB_COMMAND_STOP_SEWSTREAM;
        if((MB_USER_COMMAND & MB_COMMAND_START_NIBP)!=0) MB_USER_COMMAND &= !MB_COMMAND_START_NIBP;
    }
#endif

}
// -----------------------------------------------------------------------------------
//! \brief vTaskCB
//!
//! Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskCB( void *pvParameters )
{

        char *pcTaskName  = ( char * ) pvParameters;
        portSHORT sError = pdFALSE;
        uint16_t CB_DATALEN = 0;
        uint16_t CB_DATALENO = 0;

        uint16_t w=0;
        uint8_t test_flag = 0;     
        uint8_t k = 0;

        vSemaphoreCreateBinary( xCBMBSemaphore );

        for(k=0; k<8; k++){
           MsgSendDataId[k] = 0;
         }

	/* As per most tasks, this task is implemented in an infinite loop. */
        if(xCBMBSemaphore !=  NULL ){
            X_CB_Task++;   // the task was created 

            for( ;; )
            {
                X_CB_Task++;
                /* Try to obtain the semaphore. */
                // But does not wait ... if there is something to process, ok
                // otherwise continue... [0.... 115ms[x/portTICK_RATE_MS]....portMAX_DELAY(wait forever]
                
                mCBTaken = xSemaphoreTake(xCBMBSemaphore, 115/portTICK_RATE_MS );                                    
                if(mCBTaken == pdPASS) CBTaken_OK++;
                else CBTaken_KO++;
                if( mCBTaken == pdPASS ) { // semaphore taken successfully
                        // ---------------------------------------------------
                        // LoopBack Function
                        // ---------------------------------------------------
                        // SLIP_RX_CB -->[FROM SLIP]--> uartBufferCBin
                        CB_DATALEN = CB_ConvertSliptorawRX(SLIP_RX_CB, out_cb_reclen_0,&uartBufferCBin[0]);
                        if(out_cb_reclen_0>0){
                        // VERIFY CRC
#ifdef STM32F_CBCRC
                            crc16_cb_ccrx = crc16_compute(uartBufferCBin, CB_DATALEN - 2);
                            crc16_cb_recv = (uartBufferCBin[CB_DATALEN - 2] << 8) + uartBufferCBin[CB_DATALEN- 1];
                            if(crc16_cb_ccrx!=crc16_cb_recv) CB_DATALEN = 0;
                            else CB_DATALEN-=2;
#endif
                            if(CB_DATALEN>0){
                                if((uartBufferCBin[0]==0x01)&&(uartBufferCBin[1]==0x00)&&(uartBufferCBin[2]==0x06)&&(uartBufferCBin[3]==0x05)&&(uartBufferCBin[5]==0x04)){
                                    // DO NOT RESPOND TO ACK FROM SP
                                    /*uartBufferCBout[0] = 0x01;    // Dest: MB
                                    uartBufferCBout[1] = 0x00;    // Size MSB
                                    uartBufferCBout[2] = 0x06;    // Size lSB
                                    uartBufferCBout[3] = 0x05;    // ACK
                                    uartBufferCBout[4] = uartBufferCBin[4];    // ACK
                                    uartBufferCBout[5] = 0x04;    // Sender: MB*/
                                    CB_DATALENO = 0;
                                }
                                else if((uartBufferCBin[0]==1)&&(uartBufferCBin[1]==0x00)&&(uartBufferCBin[2]==0x50)&&(uartBufferCBin[3]==0x0B)&&(uartBufferCBin[5]==0x04)){
                                    uartBufferCBout[0] = 0x04;    // Dest: SP
                                    uartBufferCBout[1] = 0x00;    // Size MSB
                                    uartBufferCBout[2] = 0x50;    // Size lSB
                                    uartBufferCBout[3] = 0x0B;    // ACK
                                    uartBufferCBout[4] = uartBufferCBin[4];    // ACK
                                    uartBufferCBout[5] = 0x01;    // Sender: MB
                                    for(w=6;w<80;w++) uartBufferCBout[w] = uartBufferCBin[w];
                                    CB_DATALEN = 80;
                                }

                                // Check for ChangeState Here
                                else if(
                                      (uartBufferCBin[0]==0x01) &&
                                      (uartBufferCBin[1]==0x00) &&
                                      (uartBufferCBin[2]==0x0A) &&
                                      (uartBufferCBin[3]==0x0B) &&
                                      (uartBufferCBin[5]==0x04)
                                      ){
                                        QueueMs = uartBufferCBin[4]; 
                                        NewState = uartBufferCBin[8];
                                        NewOPState = uartBufferCBin[9];
                                        StateQueue = 4;

                                        // ChangeState ACK
                                        if(NewState == 1 || NewState == 3 || NewState == 4 || NewState == 5 ){
                                        uartBufferCBout[0] = 0x04;    // Dest: SP
                                        uartBufferCBout[1] = 0x00;    // Size MSB
                                        uartBufferCBout[2] = 0x06;    // Size lSB
                                        uartBufferCBout[3] = 0x05;    // ACK
                                        uartBufferCBout[4] = QueueMs;    // ACK
                                        uartBufferCBout[5] = 0x01;    // Sender: MB
                                        CB_DATALENO = 6;
                                        }
                                        else { // NACK
                                        uartBufferCBout[0] = 0x04;    // Dest: SP
                                        uartBufferCBout[1] = 0x00;    // Size MSB
                                        uartBufferCBout[2] = 0x06;    // Size lSB
                                        uartBufferCBout[3] = 0x06;    // NACK
                                        uartBufferCBout[4] = QueueMs;    
                                        uartBufferCBout[5] = 0x01;    // Sender: MB
                                        CB_DATALENO = 6;
                                        }
                                }
                                // Check for Shutdown
                                else if(
                                      (uartBufferCBin[0]==0x01) &&
                                      (uartBufferCBin[1]==0x00) &&
                                      (uartBufferCBin[2]==0x06) &&
                                      (uartBufferCBin[3]==0x02) &&
                                      (uartBufferCBin[5]==0x04)
                                      ){   
                                        StateQueue = 4;   
                                        QueueMs = uartBufferCBin[4]; 
                                        NewState = 1;
                                        NewOPState = 1;
                                     
                                        // Shutdown ACK
                                        uartBufferCBout[0] = 0x04;    // Dest: SP
                                        uartBufferCBout[1] = 0x00;    // Size MSB
                                        uartBufferCBout[2] = 0x06;    // Size lSB
                                        uartBufferCBout[3] = 0x05;    // ACK
                                        uartBufferCBout[4] = QueueMs;    // MSG NR
                                        uartBufferCBout[5] = 0x01;    // Sender: MB
                                        CB_DATALENO = 6;
                                     
                                }
                                // Get Weight command?
                                else if(
                                      (uartBufferCBin[0]==0x01) &&
                                      (uartBufferCBin[1]==0x00) &&
                                      (uartBufferCBin[2]==0x06) &&
                                      (uartBufferCBin[3]==0x15) &&
                                      (uartBufferCBin[5]==0x04)
                                      ){   
                                         
                                        QueueMs = uartBufferCBin[4]; 
                                        WeightFlag = 1;
                                      
                                        // ACK WeightRequest
                                        uartBufferCBout[0] = 0x04;    // Dest: SP
                                        uartBufferCBout[1] = 0x00;    // Size MSB
                                        uartBufferCBout[2] = 0x06;    // Size lSB
                                        uartBufferCBout[3] = 0x05;    // ACK
                                        uartBufferCBout[4] = QueueMs;    // MSG NR
                                        uartBufferCBout[5] = 0x01;    // Sender: MB
                                        CB_DATALENO = 6;                                     
                                }

                                else{
                                    
                                    // DO NOTHING..
                                    // uartBufferCBin --> uartBufferCBout
                                     //CB_DATALEN = CB_Make_LoopBack_Packet(uartBufferCBin, CB_DATALEN, &uartBufferCBout[0]);
                                    //
                                    //CB_DATALENO = 0;
                                   
                                }
                                // ADD CRC
#ifdef STM32F_CBCRC
                                crc16_cb_cctx = crc16_compute(uartBufferCBout, CB_DATALENO);
                                uartBufferCBout[CB_DATALENO] = crc16_cb_cctx>>8;
                                uartBufferCBout[CB_DATALENO+1] = crc16_cb_cctx & 0x00FF;
                                CB_DATALENO+=2;
#endif
                                // uartBufferCBout -->[TO SLIP]--> SLIP_TX_CB
                                if(CB_DATALENO){
                                  CB_DATALENO = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALENO, &SLIP_TX_CB[0]);        
                                }
                                // ---------------------------------------------------
                                for(w=0;w<CB_DATALENO;w++) {                                                                  
                                    USART_SendData(USART2, SLIP_TX_CB[w]);
                                    while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {}
                                }
                                CB_DATALENO = 0;

                                test_flag = 1;                                

                            }
                        }
                        // END OF CB INPUT

                      
                        /*    
                        REGULAR STATUS UPDATES

                            if( (SPState != NewState) && ( (X_CB_Task % 200) == 0 )  ) {

                                   //uartBufferCBout[0] = 0xC0;    // Dest: SP
                                   
                                   uartBufferCBout[0] = 0x04;    // Dest: SP
                                   uartBufferCBout[1] = 0x00;    // Size MSB
                                   uartBufferCBout[2] = 0x08;    // Size lSB
                                   uartBufferCBout[3] = 0x04;    // CurrentWAKDStateIs
                                   uartBufferCBout[4] = msgcount; 
                                   uartBufferCBout[5] = 0x02;    // Sender: RTB
                                   uartBufferCBout[6] = NewState; //0x02;    // Sender: RTB
                                   uartBufferCBout[7] = NewOPState;    // OperationalState                                 
                           
                                   CB_DATALEN = 8;
                                   msgcount++;
                                   //SPState = NewState;
            
                                   // ADD CRC
#ifdef STM32F_CBCRC
                                   crc16_cb_cctx = crc16_compute(uartBufferCBout, CB_DATALEN);
                                   uartBufferCBout[CB_DATALEN] = crc16_cb_cctx>>8;
                                   uartBufferCBout[CB_DATALEN+1] = crc16_cb_cctx & 0x00FF;
                                   CB_DATALEN+=2;
#endif
                                   // uartBufferCBout -->[TO SLIP]--> SLIP_TX_CB
                                   CB_DATALEN = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALEN, &SLIP_TX_CB[0]);        
                                   // ---------------------------------------------------
                                   for(w=0;w<CB_DATALEN;w++) {    
                                        USART_SendData(USART2, SLIP_TX_CB[w]);
                                        while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {}
                                   }
                                   //test_flag = 1;
                                  // CB_CLR_OUT_COM_BUFFERS();	            
                            }
                        */

                        // ---------------------------------------------------
                        if(test_flag !=0){
                            test_flag = 0;
                        }
                        CB_CLR_OUT_COM_BUFFERS();	
                    /* If we have a block time then we are running at a priority higher                        
                    than the idle priority.  This task takes a long time to complete                        
                    a cycle (deliberately so to test the guarding) so will be starving                        
                    out lower priority tasks.  Block for some time to allow give lower                        
                    priority tasks some processor time. */                        
                    vTaskDelay( 115 / portTICK_RATE_MS );                
                }
                //else 

                else  { // pdFALSE, semaphore not available
                    if( (115 / portTICK_RATE_MS) == ( portTickType ) 0 )                        
                    {        
                        /* We have not got the semaphore yet, so no point using the processor.  
                        We are not blocking when attempting to obtain the semaphore. */                                
                        taskYIELD();                        
                    }                
                }     

                mCBTaken = pdFAIL; 

                // Are there messages to send?
                if( (MsgSendDataId[0] > 0)  || (WeightFlag) ){
                                     
                                       if(MsgSendDataId[0] == 5){
                                           uartBufferCBout[0] = 0x04;    // Dest: SP
                                           uartBufferCBout[1] = 0x00;    // Size MSB
                                           uartBufferCBout[2] = 0x06;    // Size lSB
                                           uartBufferCBout[3] = 0x05;    // ACK
                                           uartBufferCBout[4] = MsgSendNr[0]; 
                                           uartBufferCBout[5] = 0x01; //MsgSendState[0];    // Sender: RTB                                         
                                   
                                           CB_DATALENO = 6;
  
                                       }else if(MsgSendDataId[0] == 4){
                                            
                                           uartBufferCBout[0] = 0x04;    // Dest: SP
                                           uartBufferCBout[1] = 0x00;    // Size MSB
                                           uartBufferCBout[2] = 0x08;    // Size lSB
                                           uartBufferCBout[3] = 0x04;    // CurrentWAKDStateIs
                                           uartBufferCBout[4] = QueueMs; //msgcount++; 
                                           uartBufferCBout[5] = 0x01;    // Sender: MB
                                           uartBufferCBout[6] = MsgSendState[0]; //0x02;    // Sender: RTB
                                           uartBufferCBout[7] = 0x01;    // OpState
                                   
                                           CB_DATALENO = 8;  
                                       } else if (WeightFlag){

                                         
                                             uartBufferCBout[0] = 0x04;    // Dest: SP
                                             uartBufferCBout[1] = 0x00;    // Size MSB
                                             uartBufferCBout[2] = 0x13;    // Size lSB
                                             uartBufferCBout[3] = 0x16;    // CurrentWAKDStateIs
                                             uartBufferCBout[4] = msgcount++; 
                                             uartBufferCBout[5] = 0x01;    // Sender: MB
                                             uartBufferCBout[6] = 0x03;
                                             uartBufferCBout[7] = 0x0E;   
                                             uartBufferCBout[8] = 0x10;
                                             uartBufferCBout[9] = 0x00;
                                             uartBufferCBout[10] = 0x52;
                                             uartBufferCBout[11] = 0x00;
                                             uartBufferCBout[12] = 0x00;
                                             uartBufferCBout[13] = 0x00;
                                             uartBufferCBout[14] = 0x00;
                                             uartBufferCBout[15] = 0x07;
                                             uartBufferCBout[16] = 0x5B;
                                             uartBufferCBout[17] = 0xCD;
                                             uartBufferCBout[18] = 0x15;
                                             CB_DATALENO = 19; 
                                           
                                           WeightFlag=0;

                                       }
                                          
                                     
                                       for(k=0; k<7; k++){
                                         MsgSendDataId[k] = MsgSendDataId[k+1];
                                         MsgSendNr[k] = MsgSendNr[k+1];
                                         MsgSendState[k] = MsgSendState[k+1];
                                       }
                                       MsgSendDataId[7] = 0;

                                CB_DATALENO = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALENO, &SLIP_TX_CB[0]);        
                                // ---------------------------------------------------
                                for(w=0;w<CB_DATALENO;w++) {    
                                    USART_SendData(USART2, SLIP_TX_CB[w]);
                                    while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {}
                                }
                                test_flag = 2;
                                
                                CB_DATALENO = 0;

                                
                                if(test_flag !=0){
                                    test_flag = 0;
                                }
                                CB_CLR_OUT_COM_BUFFERS();
                }

            }
        }
}
// -----------------------------------------------------------------------------------
//! \brief vTaskSDCARD
//!
//! SD CARD Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskSDCARD( void *pvParameters )
{
    /* The string to print out is passed in via the parameter.  
    Cast this to a character pointer. */
    
    char *pcTaskName  = ( char * ) pvParameters;
    //uint8_t sdlib_initialized = SIGNAL_OFF;
    
#ifdef TEST_SDCARD
    LOCK_CRITICAL_INTS = 1;
    // efsl library initialization
    SDCARD_PRESENT = SDCARD_NO_THERE;
    while(SDCARD_PRESENT != SDCARD_INSERTED){
        Update_SDCARD_Presence();
    }
    if(SDCARD_PRESENT == SDCARD_INSERTED){
        sdlib_initialised = 0x55;
        while(sdlib_initialised!=0){                
            SD_libInit();
        }
        sdfilecounter = 1;
        //sdlib_initialized = SIGNAL_ON;
    }
    abc = 0;
    LOCK_CRITICAL_INTS = 0;
#endif
    
    /* As per most tasks, this task is implemented in an infinite loop. */
    for( ;; )
    {
        XSD_TK++;
        if(SDCARD_PRESENT == SDCARD_INSERTED) {
#ifndef RTOS_SYSTICK_LED
            STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif
#ifdef TEST_SDCARD
            LOCK_CRITICAL_INTS = 1;
            //for(abc=0;abc<FILESTOWRITE;abc++){ // 250 --> 50
            Update_SDCARD_Presence();
            //if (SDCARD_PRESENT == SDCARD_INSERTED) sdfilecounter = SDCARD_WRITE_TEST(sdfilecounter);
            if (SDCARD_PRESENT == SDCARD_INSERTED) {
                sdfilecounter = SDCARD_WRITE_TEST_ACCELEROMETER(sdfilecounter);
                XSD_WR++;
            }
            abc++;
            //}
            Update_SDCARD_Presence();
            if (SDCARD_PRESENT == SDCARD_INSERTED)SDCARD_READ_TEST(15); // OK: FILE 33 READS DATA, FILE 333 READS '\0'
            fs_umount( &sdcard_efs.myFs );
            LOCK_CRITICAL_INTS = 0;
#endif
        }
        vTaskDelay( (115*2) / portTICK_RATE_MS );      
    }
}



// -----------------------------------------------------------------------------------
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
// -----------------------------------------------------------------------------------
//! \brief vRTMCB_CMDS_EXAMPLE
//!
//! Full Example: Here every command to RTMCB is tested
//!
//! \param[IN]  scheduler index 
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vRTMCB_CMDS_EXAMPLE(uint8_t scheduler_index){
    switch(scheduler_index){
        // ------------------------------------------ //
        // DEVICE: BLPUMP SETPOINT
        // ------------------------------------------ //
        case BLPUMP_SPEEDCODE:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
            // DIRECTION
            blpump_direction = SENS_DIRECT;
            RTMCB_WHICHDIRECTION = blpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING
            if(blpump_leftToRight==1){
                blpump_speedcode++;
                if(blpump_speedcode>_SPEED_CODE_0575R) blpump_leftToRight = 2;
            }
            else if(blpump_leftToRight==2){
                blpump_speedcode--;
                if(blpump_speedcode<_SPEED_CODE_STOP) blpump_leftToRight = 1;
            }
            RTMCB_ACT_REFERENCE = blpump_speedcode;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: BLPUMP STOP
        // ------------------------------------------ //
        case BLPUMP_STOP:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
            // DIRECTION
            blpump_direction = SENS_DIRECT;
            RTMCB_WHICHDIRECTION = blpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING
           RTMCB_ACT_REFERENCE = 13;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: BLPUMP RUN
        // ------------------------------------------ //
        case BLPUMP_RUN:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
            // DIRECTION
            blpump_direction = SENS_DIRECT;
            RTMCB_WHICHDIRECTION = blpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING
           RTMCB_ACT_REFERENCE = 20;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: FLPUMP SETPOINT
        // ------------------------------------------ //
        case FLPUMP_SPEEDCODE:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // FL PUMP DRIVEN BY PWM %
            // DIRECTION
            flpump_direction = SENS_REVERSE;
            RTMCB_WHICHDIRECTION = flpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING
            if(flpump_leftToRight==1){
                flpump_speedcode++;
                if(flpump_speedcode>_SPEED_CODE_1000R) flpump_leftToRight = 2;
            }
            else if(flpump_leftToRight==2){
                flpump_speedcode--;
                if(flpump_speedcode<14/*_SPEED_CODE_0065R*/) flpump_leftToRight = 1;
            }
            RTMCB_ACT_REFERENCE = flpump_speedcode;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
         // ------------------------------------------ //
        // DEVICE: FLPUMP STOP
        // ------------------------------------------ //
        case FLPUMP_STOP:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // FL PUMP DRIVEN BY PWM %
            // DIRECTION
            flpump_direction = SENS_REVERSE;
            RTMCB_WHICHDIRECTION = flpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING           
            RTMCB_ACT_REFERENCE = 13;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
         // ------------------------------------------ //
        // DEVICE: FLPUMP RUN
        // ------------------------------------------ //
        case FLPUMP_RUN:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // FL PUMP DRIVEN BY PWM %
            // DIRECTION
            flpump_direction = SENS_REVERSE;
            RTMCB_WHICHDIRECTION = flpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
            // VALUE: PWM% SETTING           
            RTMCB_ACT_REFERENCE = 20;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: POLARIZER VOLTAGE
        // ------------------------------------------ //
        case POLARIZER_SWITCHON:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
            // ON_OFF
            RTMCB_ONNOFF = SIGNAL_ON;                     // SWITCH DEVICE ON
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_ONOFF;
        break;
        case POLARIZER_VOLTAGE:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
            // DIRECTION
            if(polarizer_direction == SENS_DIRECT) polarizer_direction = SENS_REVERSE;
            else polarizer_direction = SENS_DIRECT;
            RTMCB_WHICHDIRECTION = polarizer_direction;
            // VALUE: VOLTAGE IN mV          
            if(polarizer_downToUp==1){
                polarizer_voltage+= VOLTAGE_FACTOR;           // Up to VOLTAGE_MAX, steps: VOLTAGE_FACTOR
                if(polarizer_voltage>=VOLTAGE_MAX) polarizer_downToUp = 2;                
            }
            else{
                polarizer_voltage-= VOLTAGE_FACTOR;           // Up to VOLTAGE_MAX, steps: VOLTAGE_FACTOR
                if(polarizer_voltage<=VOLTAGE_MIN) polarizer_downToUp = 1;                
            }
            RTMCB_ACT_REFERENCE = polarizer_voltage;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES
        // ------------------------------------------ //
        case VALVES3_POSITION:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            switch(valves3_sequence){
                case 1:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                break;
                case 2:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                break;
                case 3:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
                break;
                case 4:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W2;
                break;
                case 5:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                break;
                case 6:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                break;
                case 7:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
                break;
                case 8:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W2;
                break;
            }
            valves3_sequence++;
            if(valves3_sequence==9)valves3_sequence=1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ NORMAL POSITION
        // ------------------------------------------ //
        case VALVE_NORM:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ REG 1 POSITION
        // ------------------------------------------ //
        case VALVE_REG:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
          
        break;

        // ------------------------------------------ //
        // GETTING PARTICULAR BLOCKS OF DATA
        // ACTUATOR DATA SHOULD BE ANALYZED 
        // AFTER HAVE MODIFIED SOME SETTINGS
        // ------------------------------------------ //
        case GETDATA_BY_GROUP:
            // CHOOSE A GROUP OF DATA AMONG THE AVAILABLE
            RTMCB_WHICHGROUP = RS422_ACT_DATA;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETGROUPDATA;
        break;
        // ------------------------------------------ //
        // GET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case GETRTMCB_RTC:
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETCLOCK;
        break;
        // ------------------------------------------ //
        // SET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case SETRTMCB_RTC:
            // PRESET RTMCB_RTC VALUES
            RTMCB_YEAR = 2012;
            RTMCB_MONTH = 6;
            RTMCB_DAY = 9;
            RTMCB_HOURS = 9;
            RTMCB_MINUTES = 29;
            RTMCB_SECONDS = 19;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETCLOCK;
        break;
        // ------------------------------------------ //
        // OPERATING MODES
        // 20120609: NOT YET AVAILABLE AS AUTOMATIC IN RTMCB
        // ------------------------------------------ //
        case OPMODES_CONFIGURE:
            // SETTING THE OPERATING MODE
            RTMCB_OPERATING_MODE = opmodes_counter;
            opmodes_counter++; 
            if(opmodes_counter>MICROFLUIDIC_OM_MAX) opmodes_counter = MICROFLUIDIC_OM_MIN;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
        break;
    }

}
// -----------------------------------------------------------------------------------
//! \brief vTaskRTMCB
//!
//! Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskRTMCB( void *pvParameters )
{
	/* The string to print out is passed in via the parameter.  
        Cast this to a character pointer. */
        
        char *pcTaskName  = ( char * ) pvParameters;

        // EXECUTES EACH TIME ONE DIFFERENT COMMAND
        uint8_t example_testing_scheduler = 1;
        uint16_t CB_DATALEN = 0;
        uint16_t w=0;
        uint8_t y=0;
        
	/* As per most tasks, this task is implemented in an infinite loop. */

	for( ;; )
	{
                // ------------------------------------------ //
                // DEBUGGING COUNTER
                // ------------------------------------------ //
                XRTCMB++;
                // ------------------------------------------ //
                // USE OF: 
                // getData 
                // ------------------------------------------ //
                // ------------------------------------------ //
                if(RTMCB_TASK_NEW_MESSAGE == SIGNAL_ON){
                      // ------------------------------------------ //
                      // READ RTMCB_STATUS
                      // ------------------------------------------ //
                      // - DEVICE_NOTREADY: AT THE BEGINNING, NO COMMUNICATION WITH RTMCB YET
                      // - DEVICE_READY: RTMCB SENT 'READY' OR MB ASKED AND RTMCB ANSWER 'READY'
                      // - NONE: IT WAS RECEIVING CMD OR DATA BUT IT COULDN'T DECODE ANY PACKET
                      // - COMMAND: AT LEAST ONE STRUCTURED PACKET RECEIVED OK
                      // - INTERVAL: AT LEAST ONE INTERVAL DATA RECEIVED OK
                      // ------------------------------------------ //
                      // READ RTMCB_MBTASK_GETDATA_WHICHGROUP
                      // ------------------------------------------ //
                      // RS422_NOGROUP: at start-up, before analyzing message at the end of message processing
                      // RS422_ACK_RTMCB_READY: RTMCB indicates that it is ready to receive commands 
                      // RS422_SIMULATIONVSRT: RTMCB ACKS that it received simulation/real time mode (realtime)
                      // RS422_STSTSTREAMING: RTMCB ACKS that it received start/stop streaming command
                      // RS422_ACK_RECV_DATA: RTMCB ACKS that it received at least one new data (of any type)
                      // ------------------------------------------ //
                      // READ RTMCB_MBTASK_GETDATA_WHICHSUBGROUP (= RTMCB_message)
                      // ------------------------------------------ //
                      // dataID_undefined
                      // dataID_physicalData
                      // dataID_physiologicalData
                      // dataID_actuatorData
                      // dataID_alarmRTB
                      // dataID_statusRTB
                      // ------------------------------------------ //
                      // CLEAR ALL
                      // ------------------------------------------ //
                      if( (RTMCB_MBTASK_GETDATA_WHICHGROUP != RS422_NOGROUP) ) {
                        
                        //memcpy(&ma, &RAW_Buffer_RTMCB[0], sizeof(tdMsgAlarms));
                        //if(ma.alarms.Status == 0x000001){
                        //NewState = NewState;
                        //}
                        //RTMCB_message
                        //RTMCB_NEW_MESSAGE

                      }
                     

                      RTMCB_MBTASK_GETDATA_WHICHGROUP = RS422_NOGROUP;
                      RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_undefined;
                      RTMCB_TASK_NEW_MESSAGE = SIGNAL_OFF;
                      // ------------------------------------------ //
                }
                // ------------------------------------------ //
                // EXAMPLE: 
                // putData (one call of each command)
                // we also read the values to see if cmd takes effect
                // ------------------------------------------ //
                //example_testing_scheduler = BLPUMP_SPEEDCODE;
                //example_testing_scheduler = FLPUMP_SPEEDCODE;
                //example_testing_scheduler = POLARIZER_SWITCHON;
                //example_testing_scheduler = POLARIZER_VOLTAGE;
                //example_testing_scheduler = VALVES3_POSITION;
                //example_testing_scheduler = GETDATA_BY_GROUP;
                //example_testing_scheduler = GETRTMCB_RTC;
                //example_testing_scheduler = SETRTMCB_RTC;             // TO BE IMPLEMENTED IN V2
                //example_testing_scheduler = OPMODES_CONFIGURE;        // TO BE IMPLEMENTED IN V2

                //vRTMCB_CMDS_EXAMPLE( FLPUMP_SPEEDCODE ); //example_testing_scheduler);
                
                //example_testing_scheduler++;

                if(example_testing_scheduler > RTMCB_CMDS_MAX) example_testing_scheduler = BLPUMP_SPEEDCODE;
        
                 example_testing_scheduler = BLPUMP_SPEEDCODE;



                if(RTMCB_TASK_NEW_MB_CMD == SIGNAL_OFF){
                                          
                        //vRTMCB_CMDS_EXAMPLE(example_testing_scheduler);
                        //RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                        
               }
               
               // ------------------------------------------ //
               // MB SENDS NEW COMMAND TO RTMCB
               //RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
               // ------------------------------------------ //

               // ------------------------------------------ //
               // LEAVES THIS TIME TO ALLOW OTHER TASKS WORK
                  vTaskDelay( (115*4*4) / portTICK_RATE_MS );  
               // ------------------------------------------ //
	}
}
// -----------------------------------------------------------------------------------
#endif                    // 20120608: MB_TASK_CMD_RTMCB
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
