// ---------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// ---------------------------------------------------------------------------------------------------------------------
//
//! \file   SerialPortCB.h
//! \brief  Test of USART1, USART2, USART3, UART4, UART5
//!
//! Communication Tests
//!
//! \author  Dudnik G.
//! \date    05.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// ---------------------------------------------------------------------------------------------------------------------
#ifndef SERIALPORT_CB_H_
#define SERIALPORT_CB_H_
// ---------------------------------------------------------------------------------------------------------------------
// Exported constant & macro definitions
// ---------------------------------------------------------------------------------------------------------------------
// The following C language functions send and receive SLIP packets.
// They depend on two functions, send_char() and recv_char(), 
// which send and receive a single character over the serial line.
// SLIP special character codes 
#define SLIP_END            0xC0		// (octal) 0300 (decimal) 192	indicates end of packet 
#define SLIP_ESC            0xDB		// (octal) 0333 (decimal) 219	indicates byte stuffing 
#define SLIP_ESC_END        0xDC		// (octal) 0334	(decimal) 220	ESC ESC_END means END data byte 
#define SLIP_ESC_ESC        0xDD		// (octal) 0335 (decimal) 221	ESC ESC_ESC means ESC data byte 
// ---------------------------------------------------------------------------------------------------------------------
// Constant values
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------------------------------------------------
extern uint16_t CB_Transaction (USART_TypeDef* USARTx);
extern void CB_CLR_OUT_COM_BUFFERS(void);
extern uint16_t CB_Make_LoopBack_Packet(uint8_t *, uint16_t, uint8_t *);
extern uint16_t CB_SendPacket (uint8_t *slip_tx_str, uint16_t cbdatalen);
extern uint16_t CB_ConvertToSlipTX(uint8_t *raw_tx_str, uint16_t rawtxlen, uint8_t *slip_tx_str);
extern uint16_t CB_ConvertSliptorawRX(uint8_t *slip_rx_str, uint16_t sliprxlen,uint8_t *raw_rx_str);
// ---------------------------------------------------------------------------------------------------------------------
#endif /*SPORT_CB_H_*/
// ---------------------------------------------------------------------------------------------------------------------
