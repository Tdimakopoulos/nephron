/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef DEFINESACTUATORVALUES_H
#define DEFINESACTUATORVALUES_H

#define ACTUATOR_WAKD_STATE 				ACTUATOR(10)
#define ACTUATOR_WAKD_OPERATIONAL_STATE		ACTUATOR(11)

// BP = Blood Pump
// FP = Fluidic Pump
// PZ = Polarization Voltage
#define ACTUATOR_BP_DIRECTION_ALLSTOPPED	ACTUATOR(12)
#define ACTUATOR_BP_FLOWREF_ALLSTOPPED		ACTUATOR(13)
#define ACTUATOR_FP_DIRECTION_ALLSTOPPED	ACTUATOR(14)
#define ACTUATOR_FP_FLOWREF_ALLSTOPPED		ACTUATOR(15)
#define ACTUATOR_PZ_DIRECTION_ALLSTOPPED	ACTUATOR(16)
#define ACTUATOR_PZ_VOLTREF_ALLSTOPPED		ACTUATOR(17)

#define ACTUATOR_BP_DIRECTION_MAINTENANCE	ACTUATOR(18)
#define ACTUATOR_BP_FLOWREF_MAINTENANCE		ACTUATOR(19)
#define ACTUATOR_FP_DIRECTION_MAINTENANCE	ACTUATOR(20)
#define ACTUATOR_FP_FLOWREF_MAINTENANCE		ACTUATOR(21)
#define ACTUATOR_PZ_DIRECTION_MAINTENANCE	ACTUATOR(22)
#define ACTUATOR_PZ_VOLTREF_MAINTENANCE		ACTUATOR(23)

#define ACTUATOR_BP_DIRECTION_NODIALYSATE	ACTUATOR(24)
#define ACTUATOR_BP_FLOWREF_NODIALYSATE		ACTUATOR(25)
#define ACTUATOR_FP_DIRECTION_NODIALYSATE	ACTUATOR(26)
#define ACTUATOR_FP_FLOWREF_NODIALYSATE		ACTUATOR(27)
#define ACTUATOR_PZ_DIRECTION_NODIALYSATE	ACTUATOR(28)
#define ACTUATOR_PZ_VOLTREF_NODIALYSATE		ACTUATOR(29)

#define ACTUATOR_BP_DIRECTION_DIALYSIS		ACTUATOR(30)
#define ACTUATOR_BP_FLOWREF_DIALYSIS		ACTUATOR(31)
#define ACTUATOR_FP_DIRECTION_DIALYSIS		ACTUATOR(32)
#define ACTUATOR_FP_FLOWREF_DIALYSIS		ACTUATOR(33)
#define ACTUATOR_PZ_DIRECTION_DIALYSIS		ACTUATOR(34)
#define ACTUATOR_PZ_VOLTREF_DIALYSIS		ACTUATOR(35)

#define ACTUATOR_BP_DIRECTION_REGEN1		ACTUATOR(36)
#define ACTUATOR_BP_FLOWREF_REGEN1			ACTUATOR(37)
#define ACTUATOR_FP_DIRECTION_REGEN1		ACTUATOR(38)
#define ACTUATOR_FP_FLOWREF_REGEN1			ACTUATOR(39)
#define ACTUATOR_PZ_DIRECTION_REGEN1		ACTUATOR(40)
#define ACTUATOR_PZ_VOLTREF_REGEN1			ACTUATOR(41)

#define ACTUATOR_BP_DIRECTION_ULTRAFILT		ACTUATOR(42)
#define ACTUATOR_BP_FLOWREF_ULTRAFILT		ACTUATOR(43)
#define ACTUATOR_FP_DIRECTION_ULTRAFILT		ACTUATOR(44)
#define ACTUATOR_FP_FLOWREF_ULTRAFILT		ACTUATOR(45)
#define ACTUATOR_PZ_DIRECTION_ULTRAFILT		ACTUATOR(46)
#define ACTUATOR_PZ_VOLTREF_ULTRAFILT		ACTUATOR(47)

#define ACTUATOR_BP_DIRECTION_REGEN2		ACTUATOR(48)
#define ACTUATOR_BP_FLOWREF_REGEN2			ACTUATOR(49)
#define ACTUATOR_FP_DIRECTION_REGEN2		ACTUATOR(50)
#define ACTUATOR_FP_FLOWREF_REGEN2			ACTUATOR(51)
#define ACTUATOR_PZ_DIRECTION_REGEN2		ACTUATOR(52)
#define ACTUATOR_PZ_VOLTREF_REGEN2			ACTUATOR(53)

#define ACTUATOR_DEBUG_UI					ACTUATOR(55)

#endif
