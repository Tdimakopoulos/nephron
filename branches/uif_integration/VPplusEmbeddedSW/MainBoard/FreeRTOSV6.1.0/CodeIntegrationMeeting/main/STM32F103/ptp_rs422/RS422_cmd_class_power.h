// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   RS422_cmd_class_power.h
//! \brief  power class opcodes and parameters
//!
//! ...
//!
//! \author  Dudnik G.
//! \date    17.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// -----------------------------------------------------------------------------------
#ifndef DRV_CMD_CLASS_POWER_H_
#define DRV_CMD_CLASS_POWER_H_

enum communication_opcode_power_e {		
	COMMUNICATION_OPCODE_POWER_GETSTATUS =1,                        // 1
	COMMUNICATION_OPCODE_POWER_SWITCH_ONNOFF,                       // 2
	COMMUNICATION_OPCODE_POWER_GO_SLEEP,                            // 3
	COMMUNICATION_OPCODE_POWER_INFOSTATUS,                          // 4
	COMMUNICATION_OPCODE_POWER_LAST                                 // 5
};		
		
/* Parameters */		
enum communication_parameterid_power_e {		
	COMMUNICATION_PARAMETERID_POWER_ISCHARGING      =1,             // 1
	COMMUNICATION_PARAMETERID_POWER_ISEXTERNALYPOWERED,             // 2
	COMMUNICATION_PARAMETERID_POWER_BATTERYREMAININGPERCENT,	// 3
	COMMUNICATION_PARAMETERID_POWER_BATTERYREMAINING_ISRELIABLE,	// 4
	COMMUNICATION_PARAMETERID_POWER_BATTERYVOLTAGE,                 // 5
	COMMUNICATION_PARAMETERID_POWER_BATTERYSTATUSSTRUCTURE,         // 6
	COMMUNICATION_PARAMETERID_POWER_WHICHDEVICE,                    // 7
	COMMUNICATION_PARAMETERID_POWER_STATE_ONNOFF,                   // 8
	COMMUNICATION_PARAMETERID_POWER_PM_BATTERY_REMAINING_PERCENT,	// 9
	COMMUNICATION_PARAMETERID_POWER_PM_BATTERY_VOLTAGE,             // 10
	COMMUNICATION_PARAMETERID_POWER_MAIN_BATTERY_REMAINING_PERCENT,	// 11
	COMMUNICATION_PARAMETERID_POWER_MAIN_BATTERY_VOLTAGE,           // 12
	COMMUNICATION_PARAMETERID_POWER_RTMC_BATTERY_REMAINING_PERCENT,	// 13
	COMMUNICATION_PARAMETERID_POWER_RTMC_BATTERY_VOLTAGE,           // 14
	COMMUNICATION_PARAMETERID_POWER__LAST                           // 15
};		

#endif	//DRV_CMD_CLASS_POWER_H
