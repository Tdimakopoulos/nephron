/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

// standard string array length
#define SLEN 1024
// PREFIX being used for all messages of main platform code
#define PREFIX  "OFFIS HW Platform"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifndef __linux__
	#include <Windows.h>
#endif

#include "icm/icmCpuManager.h"

#define INCLUDEFROMVP
#include "../../BusPeripheralModels/offis.ovpworld.org/peripheral/OffisSimLink/1.0/offisSimLink.h"

// Helper functions used by platform to get simulation configuration from environment variables
#include "envIO.h"



int main(int argc, char **argv, char *envp[]) {

	Bool   promptAtExit = False;
	char   anyKey[64];

	////////////////////////////////////////////////////////////////////////////
	// Get application .elf file and other simulation configuration infos.
	// Most practical way is to use environtment variables.
	// The following function come from "envIO.h"
	////////////////////////////////////////////////////////////////////////////
  
	char   appName[SLEN]; getPathToApplicationToRunOnPlatform(appName, SLEN);
	int    simend    = getSimulationEndTime();
	int    simstep   = getSimulationStepFeedback(simend);
	int    simdetail = getSimulationDebugDetail();
	int    simdebug  = getSimulationDebug();

	char	comPortToBT[6]; getComPortToBT(comPortToBT);
	
	int    cosimEnabled = 0;      //getSimulinkModel() will set to 1 if it is successfull
	char   simulinkModel[SLEN]; getSimulinkModel(simulinkModel, SLEN, &cosimEnabled);
	char   simulinkModelName[SLEN]; 
	char   simulinkModelPath[SLEN];
	int    simSyncInterval = 0;
  
	if (cosimEnabled) {
		getSimulinkModelName(simulinkModelName, SLEN, simulinkModel);
		getSimulinkModelPath(simulinkModelPath, SLEN, simulinkModel);
		simSyncInterval = getSimulinkSyncIntervall();
	} else {
		// Environment does not specify a Simulink Model to be used. 
		icmMessage("W", PREFIX, "OVP-Simulink-cosimulation disabled");
		icmMessage("W", PREFIX, "SimuLink peripheral will generate dummy values!");
		icmMessage("W", PREFIX, "Should you have a Matlab/Simulink license it is");
		icmMessage("W", PREFIX, "a good idea to specify a path to your model using");
		icmMessage("W", PREFIX, "the environment variable e.g.");
		icmMessage("W", PREFIX, "export OFFIS_SIMULINKMODEL=../../SimulinkModels/testbenchWarnings.mdl");
		strcpy(simulinkModelName, "DUMMY");
		strcpy(simulinkModelPath, "DUMMY");
		simSyncInterval = 1000; // Default is one second.
	}
  
	// initialize OVPsim, enabling verbose mode to get statistics at end of execution
	if (simdebug == 1) {
		icmInit(True, "localhost", 12345);
	} else {
		icmInit(ICM_VERBOSE|ICM_STOP_ON_CTRLC, 0, 0);
	}
  
	////////////////////////////////////////////////////////////////////////////
	// Memory Mapping
	// Vector IRQ Controler reaches from 0xffff f000 to 0xffff f23f
	// OFFIF UART BT        reaches from 0x8002 0000 to 0x8002 0011
	// SimLink              reaches from 0x8001 0000 to depends on Numer A/S pirs
	// Watchdog             reaches from 0x8000 4100 to 0x8000 413f
	// TC                   reaches from 0x8000 4000 to 0x8000 403f
	// On Chip Static Ram   reaches from 0x4000 0000 to 0x4001 7fff ( 96K)
	// On Chip Non Volatile reaches from 0x0000 0000 to 0x000f ffff (1024K)
	////////////////////////////////////////////////////////////////////////////

	// BE CAREFULL: The address range of SimLink depends on the defined preprocessor
	// variable NumberActuatorSensor! The value of NumberActuatorSensor defines the number of
	// registers to be used to link to Simulink. e.g. NumberActuatorSensor 10 means 10 Sensor regs
	// plus 10 Actuator regs = 20 regs (each reg 4 Bytes!).
  
	// create the processor bus
	icmBusP bus = icmNewBus("busMain", 32);

	// create the two memory regions for Flash and Static Ram and connect to bus
	icmMemoryP OnChipNonVolatile = icmNewMemory("OnChipNonVolatile", ICM_PRIV_RX,  0x000fffff);
	icmMemoryP OnChipStaticRam   = icmNewMemory("OnChipStaticRam",   ICM_PRIV_RWX, 0x00017fff); // (96K)
	icmConnectMemoryToBus(bus, "sp", OnChipNonVolatile, 0x00000000);
	icmConnectMemoryToBus(bus, "sp", OnChipStaticRam,   0x40000000);

	////////////////////////////////////////////////////////////////////////////
	// create nets and net monitors
	////////////////////////////////////////////////////////////////////////////

	icmNetP irq_tc2vic			= icmNewNet("irq_tc2vic");
	icmNetP irq_simRTB2vic		= icmNewNet("irq_simRTB2vic");
	icmNetP irq_simMB2vic		= icmNewNet("irq_simMB2vic");
	icmNetP irq_simCB2vic		= icmNewNet("irq_simCB2vic");
	icmNetP irq_simPB2vic		= icmNewNet("irq_simPB2vic");
	icmNetP irq_uartphone2vic	= icmNewNet("irq_uartphone2vic");
	icmNetP irq_vic2arm			= icmNewNet("irq_vic2arm");
	icmNetP reset_wd2arm		= icmNewNet("reset_wd2arm");

	NET_WRITE_FN(intNetWritten) { 
		icmPrintf("Event on net!\n"); 
	}

	// icmAddNetCallback(irq_simMB2vic, intNetWritten, NULL);


	////////////////////////////////////////////////////////////////////////////
	// create a processor instance
	////////////////////////////////////////////////////////////////////////////
	
	const char *vlnvRoot     = NULL; //When NULL use default library 
	const char *arm7Model    = icmGetVlnvString(vlnvRoot,		\
												"arm.ovpworld.org",	\
												"processor",		\
												"arm",			\
												"1.0",			\
												"model");
												/*
	const char *arm7Semihost = icmGetVlnvString(vlnvRoot,			\
												"arm.ovpworld.org",	\
												"semihosting",		\
												"armNewlib",		\
												"1.0",			\
												// "model");
												*/
	const char *arm7Semihost = 
	#ifndef __linux__
		"model/model.dll";
	#else
		"model/model.so";
	#endif

	
	icmAttrListP icmAttr = icmNewAttrList();
	icmAddStringAttr(icmAttr, "variant", "ARM7TDMI");
	icmAddStringAttr(icmAttr, "compatibility", "ISA");
	icmAddStringAttr(icmAttr, "showHiddenRegs", "0");
	icmAddStringAttr(icmAttr, "UAL", "0");
	icmAddStringAttr(icmAttr, "endian", "little");

	// enable relaxed scheduling for maximum performance
	// #define SIM_ATTRS (ICM_ATTR_RELAXED_SCHED)
	// #define SIM_ATTRS (ICM_ATTR_TRACE)
	// #define SIM_ATTRS (ICM_ATTR_TRACE_REGS_AFTER)

	int SIM_ATTRS = 0;
	switch (simdetail) {
		case 1:
			SIM_ATTRS = ICM_ATTR_TRACE;
			break;
		case 2:
			SIM_ATTRS = ICM_ATTR_TRACE_ICOUNT;
			break;
		case 3:
			SIM_ATTRS = ICM_ATTR_TRACE_REGS_BEFORE;
			break;
		case 4:
			SIM_ATTRS = ICM_ATTR_TRACE_BUFFER;
			break;
		default:
			SIM_ATTRS = ICM_ATTR_DEFAULT;      
			break;
    }

	icmProcessorP processor = icmNewProcessor(
					    "CPU1",             // CPU name
					    "arm",              // CPU type
					    0,                  // CPU cpuId
					    0,                  // CPU model flags
					    32,                 // address bits
					    arm7Model   ,       // model file
					    "modelAttrs",       // morpher attributes
					    SIM_ATTRS,          // attributes
					    icmAttr,            // user-defined attributes
					    arm7Semihost,       // semi-hosting file
					    "modelAttrs"        // semi-hosting attributes
					    );
  
	// connect the processor
	icmConnectProcessorBusses(processor, bus, bus);
	icmConnectProcessorNet(processor, irq_vic2arm, "irq", ICM_INPUT);
	icmConnectProcessorNet(processor, reset_wd2arm, "reset", ICM_INPUT);

	// load the application executable file into processor memory space
	if(!icmLoadProcessorMemory(processor, appName, True, False, True)) {
		return -1;
	}

	////////////////////////////////////////////////////////////////////////////
	// OFFIS SimLink
	// for co-simulation of OVP with matlab/simulink
	////////////////////////////////////////////////////////////////////////////  
 
	icmAttrListP icmSimLinkAttr = icmNewAttrList();
    icmAddUns64Attr(icmSimLinkAttr, "strlenSimulinkModelPath", strlen(simulinkModelPath)+1 );
    icmAddStringAttr(icmSimLinkAttr, "simulinkModelPath", simulinkModelPath);
    icmAddUns64Attr(icmSimLinkAttr, "strlenSimulinkModelName", strlen(simulinkModelName)+1 );
    icmAddStringAttr(icmSimLinkAttr, "simulinkModelName", simulinkModelName);
    icmAddUns64Attr(icmSimLinkAttr, "simSyncInterval", simSyncInterval);
    
    const char *path_OFFIS_simLink = icmGetVlnvString("../../BusPeripheralModels",	
						      "offis.ovpworld.org", 
						      "peripheral",	
						      "OffisSimLink",	
						      "1.0", "pse");
	icmPseP OFFIS_simLink;
	if ((strcmp(simulinkModelName, "DUMMY")==0) && (strcmp(simulinkModelPath, "DUMMY")==0)) {
		// No simulink available. Make sure not to load any simulink.dll (being loaded by model.dll)
		OFFIS_simLink = icmNewPSE("OFFIS_simLink",	
						path_OFFIS_simLink,	
						icmSimLinkAttr,                   
						NULL, 
						NULL);
	} else {
		OFFIS_simLink = icmNewPSE("OFFIS_simLink",	
						path_OFFIS_simLink,	
						icmSimLinkAttr,                   
						#ifndef __linux__
						"..\\..\\BusPeripheralModels\\offis.ovpworld.org\\peripheral\\OffisSimLink\\1.0\\semihost\\model.dll"
						#else
						"../../BusPeripheralModels/offis.ovpworld.org/peripheral/OffisSimLink/1.0/semihost/model.so"
						#endif
						, 
						"modelAttrs");
	}

    // WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING //
    //
    // for the function "icmConnectPSEBus" make sure not to use "any" name for the port
    // but the one as defined in the PSE model. If we name it different then "sp"
    // we wont see a direct error message, but reading/writing from the address space 
    // throws a processor exception without any further hint what went wrong!
    // icmConnectPSEBus(OFFIS_simLink, bus, "sp", False, 0x80010000, 0x8001004f);
	//printf("Range: %x\n", 0x80010000+(2*NumberActuatorSensor*4)-1);
    icmConnectPSEBus(OFFIS_simLink, bus, "sp", False, 0x80010000, (0x80010000+(2*NumberActuatorSensor*4)-1+(4*4))); //+4*4 because of 4 IRQ-address
	icmConnectPSENet(OFFIS_simLink, irq_simRTB2vic,	"IRQo0", ICM_OUTPUT); // IRQ from real Time Board
	icmConnectPSENet(OFFIS_simLink, irq_simMB2vic,	"IRQo1", ICM_OUTPUT); // IRQ from Main Board e.g. User Interface
	icmConnectPSENet(OFFIS_simLink, irq_simCB2vic,	"IRQo2", ICM_OUTPUT); // IRQ from Communication Board
	icmConnectPSENet(OFFIS_simLink, irq_simPB2vic,	"IRQo3", ICM_OUTPUT); // IRQ from Power Board

	////////////////////////////////////////////////////////////////////////////
	// OFFIS Vector Interrupt Controller
	// Only rudimentarily implemented
	////////////////////////////////////////////////////////////////////////////  

	const char *path_OFFIS_VIC = icmGetVlnvString("../../BusPeripheralModels",	
							   "offis.ovpworld.org", 
							   "peripheral",	
							   "OffisVectorInterruptController", 
							   "1.0", "pse");

	icmPseP OFFIS_VIC = icmNewPSE("OFFIS_VIC",	
					   path_OFFIS_VIC,
					   NULL,			
					   NULL,			
					   NULL);
	icmConnectPSEBus(OFFIS_VIC, bus, "sp", False, 0xfffff000, 0xfffff23f);
	icmConnectPSENet(OFFIS_VIC, irq_vic2arm,	   "IRQo",  ICM_OUTPUT);
	icmConnectPSENet(OFFIS_VIC, irq_tc2vic,		   "IRQi0", ICM_INPUT);
	icmConnectPSENet(OFFIS_VIC, irq_simRTB2vic,	   "IRQi1", ICM_INPUT);
	icmConnectPSENet(OFFIS_VIC, irq_simMB2vic,	   "IRQi2", ICM_INPUT);
	icmConnectPSENet(OFFIS_VIC, irq_simCB2vic,	   "IRQi3", ICM_INPUT);
	icmConnectPSENet(OFFIS_VIC, irq_simPB2vic,	   "IRQi4", ICM_INPUT);
	icmConnectPSENet(OFFIS_VIC, irq_uartphone2vic, "IRQi5", ICM_INPUT);

	////////////////////////////////////////////////////////////////////////////
	// OFFIS Watchdog Peripheral
	// Rudimentarily implemented Watchdog. Fixed "barking" at 10s !!!!!
	////////////////////////////////////////////////////////////////////////////
	const char *path_OFFIS_wd = icmGetVlnvString("../../BusPeripheralModels",
						   "offis.ovpworld.org",
						   "peripheral",
						   "OffisWatchDog",
						   "1.0",
						   "pse");
	icmPseP OFFIS_wd = icmNewPSE("OffisWatchDog",
				   path_OFFIS_wd,
				   NULL,
				   NULL,
				   NULL);
	icmConnectPSEBus(OFFIS_wd, bus, "sp", False, 0x80004100, 0x8000413f);
	icmConnectPSENet(OFFIS_wd, reset_wd2arm, "reset", ICM_OUTPUT);

	////////////////////////////////////////////////////////////////////////////
	// OFFIS Timer Counter Peripheral
	// Rudimentarily implemented counter. Fixed frequency of 10 ms
	////////////////////////////////////////////////////////////////////////////
	const char *path_OFFIS_tc = icmGetVlnvString("../../BusPeripheralModels",
						   "offis.ovpworld.org",
						   "peripheral",
						   "TimerCounter",
						   "1.0",
						   "pse");
	icmPseP OFFIS_tc = icmNewPSE("OFFISTimerCounter",
				   path_OFFIS_tc,
				   NULL,
				   NULL,
				   NULL);
	icmConnectPSEBus(OFFIS_tc, bus, "sp", False, 0x80004000, 0x8000403f);
	icmConnectPSENet(OFFIS_tc, irq_tc2vic, "IRQ0", ICM_OUTPUT);

	////////////////////////////////////////////////////////////////////////////
	// OFFIS UART to BlueTooth to Phone
	////////////////////////////////////////////////////////////////////////////  
	if ( strcmp(comPortToBT,"OFF"))
	// Instantiate UART only if environment variable is defined.
	{
		icmAttrListP icmUartBtToPhoneAttr = icmNewAttrList();
		icmAddUns64Attr(icmUartBtToPhoneAttr, "strlenComPort", strlen(comPortToBT)+1 );
		icmAddStringAttr(icmUartBtToPhoneAttr, "comPort", comPortToBT);
		
		const char *path_OFFIS_UartBtToPhone = icmGetVlnvString("../../BusPeripheralModels",	
								  "offis.ovpworld.org", 
								  "peripheral",	
								  "OffisUartBtToPhone",	
								  "1.0", "pse");
		icmPseP OFFIS_UartBtToPhone;
		OFFIS_UartBtToPhone = icmNewPSE("OFFIS_UartBtToPhone",	
						path_OFFIS_UartBtToPhone,	
						icmUartBtToPhoneAttr,                   
						"..\\..\\BusPeripheralModels\\offis.ovpworld.org\\peripheral\\OffisUartBtToPhone\\1.0\\semihost\\model.dll", 
						"modelAttrs");


		// WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING //
		//
		// for the function "icmConnectPSEBus" make sure not to use "any" name for the port
		// but the one as defined in the PSE model. If we name it different then "sp"
		// we wont see a direct error message, but reading/writing from the address space 
		// throws a processor exception without any further hint what went wrong!
		// icmConnectPSEBus(OFFIS_simLink, bus, "sp", False, 0x80010000, 0x8001004f);
		// printf("Range: %x\n", (0x80020000+11));
		icmConnectPSEBus(OFFIS_UartBtToPhone, bus, "sp", False, 0x80020000, (0x80020000+11));
		icmConnectPSENet(OFFIS_UartBtToPhone, irq_uartphone2vic, "IRQo0", ICM_OUTPUT);
	}

  ////////////////////////////////////////////////////////////////////////////
  // Simulation of Platform
  ////////////////////////////////////////////////////////////////////////////
  icmMessage("I", PREFIX, "##############################################################");
  icmMessage("I", PREFIX, "#                                                            #");
  icmMessage("I", PREFIX, "#             ######  #####  #####  ####  #####              #");
  icmMessage("I", PREFIX, "#             ##  ##  ##     ##      ##   ###                #");
  icmMessage("I", PREFIX, "#             ##  ##  ####   ####    ##     ###              #");
  icmMessage("I", PREFIX, "#             ######  ##     ##     ####  #####              #");
  icmMessage("I", PREFIX, "#                                                            #");
  icmMessage("I", PREFIX, "#  Dipl.-Inform. Frank Poppen                                #");
  icmMessage("I", PREFIX, "#  Escherweg 2                                               #");
  icmMessage("I", PREFIX, "#  26121 Oldenburg                                           #");
  icmMessage("I", PREFIX, "#  Germany                                                   #");
  icmMessage("I", PREFIX, "#                                                            #");
  icmMessage("I", PREFIX, "# Nephron+ mainboard simulation using Open Virtual Platform. #");
  icmMessage("I", PREFIX, "#                                                            #");
  icmMessage("I", PREFIX, "# Starting HW Platform simulation.                           #");
  icmMessage("I", PREFIX, "#                                                            #");
  icmMessage("I", PREFIX, "##############################################################");

  icmProcessorP final;
  
  double  i;
  
	// int reset = 1;
	// icmWriteNet(reset_wd2arm, 1);
	
  for (i=simstep; i<=simend; i=i+simstep){
    icmSetSimulationStopTime(i);
    final = icmSimulatePlatform();
    icmMessage("I", PREFIX, "OFFIS HW Platform: Simulated %f seconds!", i);
  // icmWriteNet(reset_wd2arm, 0);
	// icmWriteNet(irq_open2vic, 1); 
	// icmWriteNet(irq_open2vic, 0); 
	// if (reset){
		// reset = 1;
	// } else {
		// reset = 1;
	// }
  }

  // was simulation interrupted or did it complete
  if(final && (icmGetStopReason(final)==ICM_SR_INTERRUPT)) {
    icmMessage("W", PREFIX,"OFFIS HW Platform: *** simulation interrupted!");
  }
  
  // free the processor
  icmFreeProcessor(processor);
  
  // wait for key press before terminating
  if(promptAtExit) {
    icmPrintf("OFFIS HW Platform: Press enter to exit demo > ");
    fgets(anyKey, sizeof(anyKey), stdin);
  }
  
  icmTerminate();
  return 0;
}
