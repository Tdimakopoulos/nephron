package eu.nephron.df.eventhandler;


import javax.ejb.Local;

import eu.nephron.df.eventhandler.bean.EventHandlerBean.RuleEngineType;


@Local
public interface EventHandlerService {

	public void AddDTOObjects(Object fact);
	public void AddObjects(Object fact);
	public void LoadKnowledge();
	public void ProcessRules();
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName);
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName);

}


