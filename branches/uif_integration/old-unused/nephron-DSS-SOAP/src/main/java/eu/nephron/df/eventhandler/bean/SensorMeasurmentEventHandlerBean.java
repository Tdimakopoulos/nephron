package eu.nephron.df.eventhandler.bean;

import eu.nephron.df.eventhandler.SensorMeasurmentEventHandlerRemoteService;
import eu.nephron.df.eventhandler.SensorMeasurmentEventHandlerService;


public class SensorMeasurmentEventHandlerBean extends EventHandlerBean
		implements SensorMeasurmentEventHandlerService,
		SensorMeasurmentEventHandlerRemoteService {

	public void ProcessEvent(/*ESponderEvent<?> pEvent*/)
	{
		//SensorSnapshotDTO pSensorSnapshot=CreateSensorSnapshot(pEvent);
		
		//Set rule profile for sensor measurment
		SetRuleEngineType(RuleEngineType.DRL_RULES, "Sensors");
		//Load Rules
		LoadKnowledge();
		//Add Measurements object list
		//AddDTOObjects(/*pSensorSnapshot*/);
		//Run Rules
		ProcessRules();
	}

   
}
