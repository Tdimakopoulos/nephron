package eu.nephron.df.eventhandler;



import javax.ejb.Remote;

import eu.nephron.df.eventhandler.bean.EventHandlerBean.RuleEngineType;


@Remote
public interface EventHandlerRemoteService extends EventHandlerService{

    
	public void AddDTOObjects(Object fact);
	public void AddObjects(Object fact);
	public void LoadKnowledge();
	public void ProcessRules();
	public void SetRuleEngineType(RuleEngineType dType,String szPackageNameOrFlowName);
	public void SetRuleEngineType(RuleEngineType dType,String szDSLName,String szDSLRName);

}
