/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

//import eu.nephron.client.alertws.AlertManager_Service;
//import eu.nephron.client.alertws.WakdState;
import eu.nephron.securitycheck.SecurityCheckPKI;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import eu.nephron.wakd.api.incoming.AlertMsg;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.ws.WebServiceRef;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
/**
 *
 * @author tdim
 */
@WebService(serviceName = "AlertWS")
@Stateless()
public class AlertWS {
//    @WebServiceRef(wsdlLocation = "META-INF/wsdl/localhost_8787/Nephron-DB-Data/AlertManager.wsdl")
//    private AlertManager_Service service;

     
    
    public void Update(Long lDeviceID,String hexStr,Long lUserID)
    {
        char[] characters = hexStr.toCharArray();
        AlertMsg msg=new AlertMsg();
        try {
            msg.decode(Hex.decodeHex(characters));
        } catch (DecoderException ex) {
            Logger.getLogger(AlertWS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    @POST
//	@Path("/create")
//	public void update(
//			@QueryParam("deviceID") Long deviceID,
//			@QueryParam("message") String hexStr,
//			@QueryParam("userID") Long userID) {
//		
//		AlertMsg msg = new AlertMsg();
//		msg.decode(Hex.decodeHex(hexStr));
//		this.getMedicalEntityService().createAlert(deviceID, msg.getAlert(), userID);
//		System.out.println("ALERT DETECTED: " + msg.getAlert());
//	}
//	
//	private MedicalEntityService getMedicalEntityService() {
//		try {
//			return ServiceLocator.getResource("nephron/MedicalEntityServiceImpl/local");
//		} catch (NamingException e) {
//			throw new RuntimeException(e);
//		}
//	}

    public String update(Long deviceID,String hexStr,String publickey) throws DecoderException
    {
        SecurityCheckPKI pki = new SecurityCheckPKI();
        if(!pki.CheckUser(publickey))
        {
            return "User is not Authorized";
        }
        AlertMsg msg = new AlertMsg();
	msg.decode(Hex.decodeHex(hexStr.toCharArray()));
       // eu.nephron.client.alertws.WakdState entity= new eu.nephron.client.alertws.WakdState();
        //entity.setDevice(deviceID);
        ///entity.s
        return null;
    }
    
//    private void create(eu.nephron.client.alertws.WakdState entity) {
//        eu.nephron.client.alertws.AlertManager port = service.getAlertManagerPort();
//        
//        port.create(entity);
//    }
}
