/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.themes;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class TabBean {

    private String effect="slide";
    /**
     * Creates a new instance of TabBean
     */
    public TabBean() {
    }

    /**
     * @return the effect
     */
    public String getEffect() {
        return effect;
    }

    /**
     * @param effect the effect to set
     */
    public void setEffect(String effect) {
        this.effect = effect;
    }
}
