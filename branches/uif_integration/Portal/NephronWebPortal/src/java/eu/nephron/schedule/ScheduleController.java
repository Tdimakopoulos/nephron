/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.schedule;

import eu.nephron.soap.ws.ScheduledAct;
import eu.nephron.soap.ws.ScheduledActManager_Service;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.WebServiceRef;

import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.ScheduleEntrySelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@SessionScoped
public class ScheduleController {

    private ScheduleModel eventModel;
    private ScheduleEvent event = new DefaultScheduleEvent();

    public ScheduleController() {
        eventModel = new DefaultScheduleModel();
        java.util.List<eu.nephron.soap.ws.ScheduledAct> pfind = scheduledActfindAll();
        for (int i = 0; i < pfind.size(); i++) {
            ScheduleEvent eventtoadd = new DefaultScheduleEvent(pfind.get(i).getText(), normalizeDate(pfind.get(i).getActivityDate().toGregorianCalendar().getTime()), normalizeDate(pfind.get(i).getEffectiveDate().toGregorianCalendar().getTime()));
            eventModel.addEvent(eventtoadd);
        }

    }

    public Date getRandomDate(Date base) {
        Calendar date = Calendar.getInstance();
        date.setTime(base);
        date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1);        //set random day of month

        return date.getTime();
    }

    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar;
    }

    private Date previousDay8Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 8);

        return t.getTime();
    }

    private Date previousDay11Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date today1Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 1);

        return t.getTime();
    }

    private Date theDayAfter3Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 2);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    private Date today6Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 6);

        return t.getTime();
    }

    private Date nextDay9Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 9);

        return t.getTime();
    }

    private Date nextDay11Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date fourDaysLater3pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 4);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    private Date normalizeDate(Date dinit) {
        Calendar t = Calendar.getInstance();
        t.setTime(dinit);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE));


        return t.getTime();
    }

    public void addEvent(ActionEvent actionEvent) {
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ScheduleEvent eventtoadd = new DefaultScheduleEvent(event.getTitle(), normalizeDate(event.getStartDate()), normalizeDate(event.getEndDate()));
        if (event.getId() == null) {
            eu.nephron.soap.ws.ScheduledAct entity= new ScheduledAct();
            entity.setText(event.getTitle());
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(event.getStartDate().getTime());
            entity.setActivityDate(df.newXMLGregorianCalendar(gc));
            gc.setTimeInMillis(event.getEndDate().getTime());
            entity.setEffectiveDate(df.newXMLGregorianCalendar(gc));
            scheduledActcreate(entity);
            eventModel.addEvent(eventtoadd);
        } else {
            eventModel.deleteEvent(event);
            eventModel.addEvent(eventtoadd);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(ScheduleEntrySelectEvent selectEvent) {
        event = selectEvent.getScheduleEvent();
    }

    public void onDateSelect(DateSelectEvent selectEvent) {

        event = new DefaultScheduleEvent("", selectEvent.getDate(), selectEvent.getDate());

    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    private java.util.List<eu.nephron.soap.ws.ScheduledAct> scheduledActfindAll() {
        ScheduledActManager_Service service = new ScheduledActManager_Service();
        eu.nephron.soap.ws.ScheduledActManager port = service.getScheduledActManagerPort();
        return port.scheduledActfindAll();
    }

    private void scheduledActcreate(eu.nephron.soap.ws.ScheduledAct entity) {
        ScheduledActManager_Service service = new ScheduledActManager_Service();
        eu.nephron.soap.ws.ScheduledActManager port = service.getScheduledActManagerPort();
        port.scheduledActcreate(entity);
    }
}
