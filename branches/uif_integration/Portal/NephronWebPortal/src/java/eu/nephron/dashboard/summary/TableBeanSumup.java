/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dashboard.summary;

import eu.nephron.dss.ws.DSSWSManager_Service;
import eu.nephron.soap.ws.AlertManager_Service;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Patient;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.WebServiceRef;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.Dispatch;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Service;
import java.io.StringReader;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class TableBeanSumup {
    private List<DSSResults> sumups;
    private DSSResults selectedsumup;
    private DSSResults[] selectedsumups;

    /**
     * Creates a new instance of TableBeanSumup
     */
    public TableBeanSumup() {
        sumups = new ArrayList<DSSResults>();
        populatesumups(sumups);
    }

    private void populatesumups(List<DSSResults> list) {

        java.util.List<eu.nephron.dss.ws.DssRuleResults> pResults = findAll();
        for (int i = 0; i < pResults.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(pResults.get(i).getId());
            pNew.setRulename(pResults.get(i).getRulename());
            pNew.setRuleresults(pResults.get(i).getRuleresults());
            pNew.setRuleresults1(pResults.get(i).getRuleresults1());
            list.add(pNew);
        }

        java.util.List<eu.nephron.soap.ws.Alert> palerts = findAllalert();
        for (int i = 0; i < palerts.size(); i++) {
            String title = palerts.get(i).getCode();
            DSSResults pNew = new DSSResults();
            pNew.setId(palerts.get(i).getId());
            pNew.setRulename("Alert");
            pNew.setRuleresults(palerts.get(i).getAlertDate().toString());
            pNew.setRuleresults1(title);
            list.add(pNew);
        }

    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the selectedsumup
     */
    public DSSResults getSelectedsumup() {
        return selectedsumup;
    }

    /**
     * @param selectedsumup the selectedsumup to set
     */
    public void setSelectedsumup(DSSResults selectedsumup) {
        this.selectedsumup = selectedsumup;
    }

    /**
     * @return the selectedsumups
     */
    public DSSResults[] getSelectedsumups() {
        return selectedsumups;
    }

    /**
     * @param selectedsumups the selectedsumups to set
     */
    public void setSelectedsumups(DSSResults[] selectedsumups) {
        this.selectedsumups = selectedsumups;
    }

    private java.util.List<eu.nephron.dss.ws.DssRuleResults> findAll() {
        eu.nephron.dss.ws.DSSWSManager_Service service = new eu.nephron.dss.ws.DSSWSManager_Service();
        eu.nephron.dss.ws.DSSWSManager port = service.getDSSWSManagerPort();
        return port.findAll();
    }

    private java.util.List<eu.nephron.soap.ws.Alert> findAllalert() {
        AlertManager_Service service = new AlertManager_Service();
        eu.nephron.soap.ws.AlertManager port = service.getAlertManagerPort();
        return port.findAllalert();
    }
}
