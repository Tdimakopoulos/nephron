/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientview;

import eu.nephron.soap.ws.AlertManager_Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.timeline.DefaultTimeLine;
import org.primefaces.extensions.model.timeline.DefaultTimelineEvent;
import org.primefaces.extensions.model.timeline.Timeline;
import org.primefaces.extensions.model.timeline.TimelineEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class CustomEventsController implements Serializable {

    private List<Timeline> timelines;
    private TimelineEvent selectedEvent;

    public CustomEventsController() {
        timelines = new ArrayList<Timeline>();
        Calendar cal = Calendar.getInstance();
        Date ddate = new Date();
        cal.setTime(ddate);
        Timeline timeline = new DefaultTimeLine("customEventTimeline", "Nephron Timeline");

        java.util.List<eu.nephron.soap.ws.Alert> palerts = findAllalert();
        for (int i = 0; i < palerts.size(); i++) {
            XMLGregorianCalendar pcal = palerts.get(i).getAlertDate();
            String title = palerts.get(i).getCode();
            timeline.addEvent(new DefaultTimelineEvent(title, pcal.toGregorianCalendar().getTime()));
        }

        timelines.add(timeline);
    }

    public void onEventSelect(SelectEvent event) {
        selectedEvent = (TimelineEvent) event.getObject();
    }

    public List<Timeline> getTimelines() {
        return timelines;
    }

    public TimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    private java.util.List<eu.nephron.soap.ws.Alert> findAllalert() {
        AlertManager_Service service = new AlertManager_Service();
        eu.nephron.soap.ws.AlertManager port = service.getAlertManagerPort();
        return port.findAllalert();
    }
}
