/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.charts;

import eu.nephron.model.Sensors;
import eu.nephron.soap.ws.DBManager_Service;
import eu.nephron.soap.ws.Manager;
import eu.nephron.soap.ws.ObservationAct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.WebServiceRef;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class ChartBean implements Serializable {

    private CartesianChartModel linearModel;
    private CartesianChartModel linearModelsodium;
    private CartesianChartModel linearModelpotasium;
    private CartesianChartModel linearModelurea;
    private CartesianChartModel linearModelph;
    private List<Sensors> sensors;
    private CartesianChartModel categoryModel;

    public ChartBean() {
        sensors = new ArrayList<Sensors>();
        createLinearModel();
        createCategoryModel();
    }

    public CartesianChartModel getLinearModel() {
        return linearModel;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void updatereadings() {
        createLinearModel();
        createCategoryModel();
    }

    private void createLinearModel() {
        linearModel = new CartesianChartModel();
        linearModelsodium = new CartesianChartModel();
        linearModelpotasium = new CartesianChartModel();
        linearModelurea = new CartesianChartModel();
        linearModelph = new CartesianChartModel();

        LineChartSeries sodium = new LineChartSeries();
        LineChartSeries potasium = new LineChartSeries();
        LineChartSeries urea = new LineChartSeries();
        LineChartSeries ph = new LineChartSeries();
        sodium.setLabel("Sodium NA+");
        potasium.setLabel("Potasium K+");
        urea.setLabel("Urea");
        ph.setLabel("PH");
        java.util.List<eu.nephron.soap.ws.ObservationAct> plist = findAllMeasurements();
        for (int i = 0; i < plist.size(); i++) {
            Sensors psensor = new Sensors();
            if (plist.get(i).getTypeCode() == 1) {
                psensor.setTime(new Date());
                psensor.setType("Sodium");
                psensor.setValue(plist.get(i).getFvalue());
                sodium.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 2) {
                psensor.setTime(new Date());
                psensor.setType("Potasium");
                psensor.setValue(plist.get(i).getFvalue());
                potasium.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 3) {
                psensor.setTime(new Date());
                psensor.setType("Urea");
                psensor.setValue(plist.get(i).getFvalue());
                urea.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 4) {
                psensor.setTime(new Date());
                psensor.setType("PH");
                psensor.setValue(plist.get(i).getFvalue());
                ph.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            sensors.add(psensor);
        }
        linearModel.addSeries(sodium);
        linearModel.addSeries(potasium);
        linearModel.addSeries(urea);
        linearModel.addSeries(ph);
        linearModelsodium.addSeries(sodium);
        linearModelpotasium.addSeries(potasium);
        linearModelurea.addSeries(urea);
        linearModelph.addSeries(ph);
    }

    private void createCategoryModel() {
        categoryModel = new CartesianChartModel();
        ChartSeries sodium = new ChartSeries();
        ChartSeries potasium = new ChartSeries();
        ChartSeries urea = new ChartSeries();
        ChartSeries ph = new ChartSeries();

        sodium.setLabel("Sodium NA+");
        potasium.setLabel("Potasium K+");
        urea.setLabel("Urea");
        ph.setLabel("PH");
        java.util.List<eu.nephron.soap.ws.ObservationAct> plist = findAllMeasurements();
        for (int i = 0; i < plist.size(); i++) {
            //System.out.println(plist.get(i).getText());
            if (plist.get(i).getTypeCode() == 1) {
                sodium.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 2) {
                potasium.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 3) {
                urea.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
            if (plist.get(i).getTypeCode() == 4) {
                ph.set(plist.get(i).getFvalue().toString(), plist.get(i).getFvalue());
            }
        }

        categoryModel.addSeries(sodium);
        categoryModel.addSeries(potasium);
        categoryModel.addSeries(urea);
        categoryModel.addSeries(ph);
    }

    private java.util.List<eu.nephron.soap.ws.ObservationAct> findAllMeasurements() {
        DBManager_Service service = new DBManager_Service();
        eu.nephron.soap.ws.DBManager port = service.getDBManagerPort();
        return port.findAllMeasurements();
    }

    /**
     * @return the linearModelsodium
     */
    public CartesianChartModel getLinearModelsodium() {
        return linearModelsodium;
    }

    /**
     * @param linearModelsodium the linearModelsodium to set
     */
    public void setLinearModelsodium(CartesianChartModel linearModelsodium) {
        this.linearModelsodium = linearModelsodium;
    }

    /**
     * @return the linearModelpotasium
     */
    public CartesianChartModel getLinearModelpotasium() {
        return linearModelpotasium;
    }

    /**
     * @param linearModelpotasium the linearModelpotasium to set
     */
    public void setLinearModelpotasium(CartesianChartModel linearModelpotasium) {
        this.linearModelpotasium = linearModelpotasium;
    }

    /**
     * @return the linearModelurea
     */
    public CartesianChartModel getLinearModelurea() {
        return linearModelurea;
    }

    /**
     * @param linearModelurea the linearModelurea to set
     */
    public void setLinearModelurea(CartesianChartModel linearModelurea) {
        this.linearModelurea = linearModelurea;
    }

    /**
     * @return the linearModelph
     */
    public CartesianChartModel getLinearModelph() {
        return linearModelph;
    }

    /**
     * @param linearModelph the linearModelph to set
     */
    public void setLinearModelph(CartesianChartModel linearModelph) {
        this.linearModelph = linearModelph;
    }

    /**
     * @return the sensors
     */
    public List<Sensors> getSensors() {
        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }
}
