/*
 *
 *  HL7Element.java : An interface for HL7 Message data item classes.
 *
 */

package eu.nephron.hl7.message;

/**
 *
 * @author tomd
 */
interface HL7Element {
   String toHL7String(HL7Encoding encoders);
   void set(String msgText, HL7Encoding encoders);
   HL7Element getElement(int index);
   boolean hasContent();
} // HL7Element
