/*
 *
 *
 *  GenericHL7Element.java : An class for generic HL7 message elements,
 *  providing structured access to message data content, and constituent items.
 *
 *
 */

package eu.nephron.hl7.message;

/**
 * Note: This class is currently unused.
 * @author tomd
 */
class GenericHL7Element extends AbstractHL7Element implements HL7Element {

   public GenericHL7Element(HL7ElementLevel level) {
      super(level.get());
   } // GenericHL7Element constructor

} // GenericHL7Element
