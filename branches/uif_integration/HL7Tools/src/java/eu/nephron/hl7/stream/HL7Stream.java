/*
 *  HL7Stream.java : A HL7 message stream interface.
 *
*/

package eu.nephron.hl7.stream;

import eu.nephron.hl7.message.HL7Message;




/**
 * An interface specification for HL7 message stream classes.
 * @author tomd      
 */
public interface HL7Stream {
   /**
    * No type specified.
    */
   final int      NO_TYPE              = 0;
   /**
    * Uninitialized - type is not set.
    */
   final int      UNINITIALIZED        = 0;
   /**
    * The linefeed character value (10).
    */
   final int      LF                   = 0x0a;
   /**
    * The carriage return character value (13).
    */
   final int      CR                   = 0x0d;
   /**
    * Reader type
    */
   final int      READER               = 1;
   /**
    * Writer type
    */
   final int      WRITER               = 2;
   /**
    * Appender type
    */
   final int      APPENDER             = 3;
   /**
    * Closed status
    */
   final int      CLOSED               = 0xffff;
   /**
    * Open status
    */
   final int      OPEN                 = 201;
   /**
    * File media type
    */
   final int      FILE_TYPE            = 101;
   /**
    * Socket media type
    */
   final int      SOCKET_TYPE          = 103;
   /**
    * Secured socket media type.
    */
   final int      SECURE_SOCKET_TYPE   = 105;


   /**
    * Opens the context stream.
    * @return true if the operation succeeded, otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean open() throws HL7IOException;

   /**
    * Closes the context stream.
    * @return true if the operation succeeded, otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean close() throws HL7IOException;

   /**
    * Reads a HL7 message from the context stream.
    * @return the message read as a HL7Message object.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public HL7Message read() throws HL7IOException;

   /**
    * Write a HL7 message to the context stream.
    * @param msg the message to send as a HL7Message object.
    * @return true if the operation succeeded, otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean write(HL7Message msg) throws HL7IOException;

   /**
    * Access to the current status of the context stream.
    * @return the current status value of the context stream.
    */
   public int status();

   /**
    * Determines whether the context stream is closed.
    * @return true if the context stream is closed, otherwise false.
    */
   public boolean isClosed();

   /**
    * Determines whether the context stream is open.
    * @return true if the context stream is open, otherwise false.
    */
   public boolean isOpen();

   /**
    * Determines whether the context stream is a server.
    * @return true if the context stream is a server, otherwise false.
    */
   public boolean isServer();

   /**
    * Creates a description of the current state of the context stream.
    * @return the description as a string.
    */
   public String description();

   /**
    * Extracts the HL7MessageHandler from the context stream, if it is valid.
    * @return the HL7MessageHandler, or null if no HL7MessageHandler is available.
    */
   public HL7MessageHandler dispatchHandler();

} // HL7Stream
