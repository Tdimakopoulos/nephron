/*
 *
 *  HL7MessageHandler.java : An HL7 message dispatcher interface.
 *
 *
 */

package eu.nephron.hl7.stream;

import eu.nephron.hl7.message.HL7Message;



/**
 * A interface for HL7Message dispatch handlers.

 */
public interface HL7MessageHandler {
   /**
    * The only required method is dispatch, which may utilize the argument
    * HL7Message object in any way.
    * @param msg a reference to the argument HL7Message object.
    * @return a count of 0 or more dispatch occurrences related to the context
    * dispatch invocation.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public int dispatch(HL7Message msg) throws HL7IOException;
} // HL7MessageHandler
