/*
 *
 *  HL7SegmentMap.java : Provides keyed access to parsed HL7 message segment data.
 *
 */

package eu.nephron.hl7.message;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A hashed multi map class for indexing HL7 segments by ID.
 * @author tomd
 */
class HL7SegmentMap {
   private HashMap segmentHash;

   HL7SegmentMap() {
      segmentHash = new HashMap();
   } // HL7SegmentMap


   /**
    * Retrieves the entry corresponding to the argument string.
    * @param argStr A segment ID string
    * @return the entry corresponding to the argument string.
    */
   ArrayList<HL7Segment> get(String argStr) {
      return (ArrayList<HL7Segment>)segmentHash.get(argStr.substring(0, 3));
   } // get


   /**
    * Indexes the argument HL7Segment in the map.
    * @param segment The HL7Segment object to be indexed.
    */
   void put(HL7Segment segment) {
      if (segment == null) return; 

      String idStr = segment.getID();
      ArrayList<HL7Segment> segments = get(idStr);
      if (segments != null && !segments.isEmpty()) {
         segments.add(segment);
         return;
      } // if

      segments = new ArrayList<HL7Segment>();
      segments.add(segment);
      segmentHash.put(idStr, segments);
   } // put


   /**
    * Determines whether the map contains one or more entries corresponding to
    * the argument key.
    * @param key The segment ID key.
    * @return true if the map contains one or more entries corresponding to the
    * argument key. Otherwise false.
    */
   boolean has(String key) {
      return segmentHash.containsKey(key);
   } // has


   /**
    * Determines whether the map has any entries.
    * @return true if the map contains one or more entries. Otherwise false.
    */
   boolean isEmpty() {
      return segmentHash.isEmpty();
   } // isEmpty

} // HL7SegmentMap
