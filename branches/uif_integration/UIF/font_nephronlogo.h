// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   font_nephronlogo.h
//! \brief  nephronplus logo generated with GLCD Font Creator
//!
//! logo inspired from original (EXODUS: 25.04.2010) and adapted to this LCD
//!
//! \author  DUDNIK G.S.
//! \date    20.12.2011
//! \version 1.0 First version (gdu)
// -----------------------------------------------------------------------------------
#ifndef NEPHRONLOGO_H
#define NEPHRONLOGO_H
// -----------------------------------------------------------------------------------
//GLCD FontName : nephronplus17x15
//GLCD FontSize : 17 x 15
// -----------------------------------------------------------------------------------
#define NEPHRONPLUS_COLS	    (17*2)
#define NEPHRONPLUS_CHARS_PER_COL   2
#define NEPHRONPLUS_START_X         6
#define NEPHRONPLUS_START_Y         19
#define NEPHRONPLUS_FONT_ROWS       17
// -----------------------------------------------------------------------------------
const unsigned short nephronplus17x15[] = {
        0x1C, 0x1C, 0x22, 0x22, 0x41, 0x41, 0xC1, 0x40, 0xC1, 0x40, 0xC1, 0x40, 0xC2, 0x40, 0xF2, 0x23, 0x1C, 0x1E, 0x1C, 0x1E, 0x12, 0x22, 0x12, 0x42, 0xF1, 0x43, 0x01, 0x40, 0x81, 0x41, 0x42, 0x42, 0x3C, 0x3C   // Code for char
        };
// -----------------------------------------------------------------------------------
#endif  // __NEPHRONLOGO_H_
// -----------------------------------------------------------------------------------
