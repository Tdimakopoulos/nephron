// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifMbProtocol.c
//! \brief  Derivated from PM (ltms3) where PM is Master, UIF will be Slave
//!
//! Data Exchange routines between UIF and MB
//!
//! \author  DUDNIK G.S.
//! \date    27.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------
#ifndef UIFMBPROTOCOL_H
#define UIFMBPROTOCOL_H

// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
#define TEST_LINK_OK                      0x12345678
#define TEST_LINK_KO                      0x87654321


#define MB_UIF_TIME_MULTIPLIER	            10	    // x10 = 0.5'' (1=50ms)
#define	MB_UIF_EXCHANGE_COUNTS	            60	    // x60 = 10'

#define MB_UIF_MAXLEN                       64
#define MB_UIF_TXLEN                        64      // 16

#define MESSAGE_NEW                         0
#define MESSAGE_ADV                         1

#define MB_SETINFO                          1
#define MB_GETCMDS                          2
#define MB_UNKNOWN                          0

// -----------------------------------------------------------------------------------
// PM DL Protocol Command CODES
// -----------------------------------------------------------------------------------
// MB UIF Protocol Command                  CODES	Rate(1/f) [s]
	
// GROUP 1: INFO
#define MB_UIF_CMD_VBAT1_INFO	            0x20	// 60
#define MB_UIF_CMD_VBAT2_INFO	            0x21	// 60
#define MB_UIF_CMD_WAKD_STATUS	            0x23	// 60
#define MB_UIF_CMD_WAKD_ATTITUDE	    0x24	// 5
#define MB_UIF_CMD_WAKD_COMMLINK	    0x25	// 60
#define MB_UIF_CMD_WAKD_OPMODE	            0x26	// 60
#define MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS    0x27	// 60
#define MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS    0x28	// 60
#define MB_UIF_CMD_WAKD_BLPUMP_INFO	    0x2A	// 60
#define MB_UIF_CMD_WAKD_FLPUMP_INFO	    0x2B	// 60
#define MB_UIF_CMD_WAKD_BLTEMPERATURE	    0x2C	// 1
#define MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE  0x2D	// 1
#define MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE  0x2E	// 1
#define MB_UIF_CMD_WAKD_FLCONDUCTIVITY	    0x2F	// 1

#define MB_UIF_CMD_WAKD_HFD_INFO	    0x30	// 60
#define MB_UIF_CMD_WAKD_SU_INFO	            0x31	// 60
#define MB_UIF_CMD_WAKD_POLAR_INFO	    0x32	// 60
#define MB_UIF_CMD_WAKD_ECPS_INFO	    0x33	// 60
#define MB_UIF_CMD_WAKD_PS_INFO	            0x34	// 1
#define MB_UIF_CMD_WAKD_ACT_INFO	    0x35        // 1
#define MB_UIF_CMD_WAKD_ACK_COMMANDS        0x36        // 1

#define MB_UIF_CMD_PDATA_INITIALS	    0x40	// (once, atb)
#define MB_UIF_CMD_PDATA_PATIENTCODE	    0x41	// (once, atb)
#define MB_UIF_CMD_PDATA_GENDERAGE	    0x42	// (once, atb)
#define MB_UIF_CMD_PDATA_WEIGHT	            0x43	// once, when measured
//#define MB_UIF_CMD_PDATA_HEARTRATE	    0x44	// when measured, 5
//#define MB_UIF_CMD_PDATA_BREATHRATE	    0x45	// when measured, 5
//#define MB_UIF_CMD_PDATA_ACTIVITY	    0x46	// when measured, 5
#define MB_UIF_CMD_PDATA_SEWALL	            0x47	// when measured, 5
#define MB_UIF_CMD_PDATA_BLPRESSURE	    0x48	// once, when measured
#define MB_UIF_CMD_PDATA_ECP1_A	            0x49	// 30
#define MB_UIF_CMD_PDATA_ECP1_B	            0x4A	// 30
#define MB_UIF_CMD_PDATA_ECP2_A	            0x4B	// 30
#define MB_UIF_CMD_PDATA_ECP2_B	            0x4C	// 30

#define MB_UIF_CMD_ALARMS	            0x50	// 1
#define MB_UIF_CMD_ERRORS	            0x51	// 1

// GROUP 0: TEST LINK
#define MB_UIF_CMD_TEST_LINK	            0x52	// (once, atb)

// GROUP 2: CMDS
#define MB_UIF_CMD_WAKD_SHUTDOWN	    0x60	// 1
#define MB_UIF_CMD_WAKD_START_OPERATION	    0x61	// 1
#define MB_UIF_CMD_WAKD_MODE_OPERATION	    0x62	// 1
#define MB_UIF_CMD_SCALE_GET_WEIGHT	    0x63	// 1
#define MB_UIF_CMD_SEW_START_STREAMING	    0x64	// 1
#define MB_UIF_CMD_SEW_STOP_STREAMING	    0x65	// 1
#define MB_UIF_CMD_NIBP_START_MEASUREMENT   0x66	// 1		

// KEY VALUES
#define MB_UIF_CMD_ACKNOWLEDGE	            0x77	
#define MB_UIF_CMD_NOT_ACKNOWLEDGE	    0x99	
#define MB_UIF_CMD_FORBIDDEN	            0xFF	
#define MB_UIF_CMD_END                      0x88
#define MB_UIF_CMD_TEST1	            0xAB	
#define MB_UIF_CMD_TEST2	            0xCD	
#define MB_UIF_CMD_CLR	                    0x00	

#define MB_UIF_QTY_CMD_SET                  6    
#define MB_UIF_QTY_CMD_GET                  2   

#if 1
// QUANTITY OF BYTES TO BE RECEIVED
#define MB_UIF_QTY_VBAT1_INFO	            8     //  1
#define MB_UIF_QTY_VBAT2_INFO	            8     //  2
#define MB_UIF_QTY_VBATS_INFO	            12    //  3 
#define MB_UIF_QTY_WAKD_STATUS	            8     //  4
#define MB_UIF_QTY_WAKD_ATTITUDE	    6     //  5
#define MB_UIF_QTY_WAKD_COMMLINK	    6     //  6
#define MB_UIF_QTY_WAKD_OPMODE	            6     //  7
#define MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS    8     //  8
#define MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS    8     //  9
#define MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS    12    // 10
#define MB_UIF_QTY_WAKD_BLPUMP_INFO	    8     // 11
#define MB_UIF_QTY_WAKD_FLPUMP_INFO	    8     // 12
#define MB_UIF_QTY_WAKD_BLTEMPERATURE	    10    // 13
#define MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE  8     // 14
#define MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE  8     // 15
#define MB_UIF_QTY_WAKD_FLCONDUCTIVITY	    10    // 16
#define MB_UIF_QTY_WAKD_HFD_INFO	    6     // 17
#define MB_UIF_QTY_WAKD_SU_INFO	            6     // 18
#define MB_UIF_QTY_WAKD_POLAR_INFO	    8     // 19
#define MB_UIF_QTY_WAKD_ECP1_INFO	    6     // 20
#define MB_UIF_QTY_WAKD_ECP2_INFO	    6     // 21
#define MB_UIF_QTY_WAKD_ECPS_INFO	    8     // 22
#define MB_UIF_QTY_PDATA_FIRSTNAME	    18    // 23  
#define MB_UIF_QTY_PDATA_LASTNAME	    18    // 24 
#define MB_UIF_QTY_PDATA_GENDERAGE	    6     // 25
#define MB_UIF_QTY_PDATA_WEIGHT	            6     // 26
#define MB_UIF_QTY_PDATA_HEARTRATE	    5     // 27
#define MB_UIF_QTY_PDATA_BREATHRATE	    5     // 28
#define MB_UIF_QTY_PDATA_ACTIVITY	    5     // 29
#define MB_UIF_QTY_PDATA_SEWALL	            7     // 30
#define MB_UIF_QTY_PDATA_BLPRESSURE	    8     // 31 
#define MB_UIF_QTY_PDATA_ECP1	            12    // 32
#define MB_UIF_QTY_PDATA_ECP2	            12    // 33
#define MB_UIF_QTY_ALARMS	            8     // 34
#define MB_UIF_QTY_ERRORS	            8     // 35
#define MB_UIF_QTY_TEST_LINK	            6     // 36
#define MB_UIF_QTY_WAKD_SHUTDOWN	    4     //
#define MB_UIF_QTY_WAKD_START_OPERATION	    4     //
#define MB_UIF_QTY_WAKD_MODE_OPERATION	    4     //
#define MB_UIF_QTY_SCALE_GET_WEIGHT	    4     //
#define MB_UIF_QTY_SEW_START_STREAMING	    4     //
#define MB_UIF_QTY_SEW_STOP_STREAMING	    4     //
#define MB_UIF_QTY_NIBP_START_MEASUREMENT   4     //
#define MB_UIF_QTY_ANSWER_1                 5
#define MB_UIF_QTY_ANSWER_2                 3
#endif

#define CMDS 1
#define OPMS 2
#define MSGS 3
// -----------------------------------------------------------------------------------
// Variables
// -----------------------------------------------------------------------------------
extern uint16_t SlaveRcv;
extern uint8_t pmdl_counter;                      // counter of data being received
extern uint8_t pmdl_cmd_rx;
extern uint8_t pmdl_command;
extern uint8_t pmdl_cmd_rx;
extern uint8_t pmdl_command;                      // current command
extern uint8_t pmdl_cmd_end;                      // flag, 1 if full cmd received
extern uint8_t pmdl_command_len;                  // qty of cmd to receive
extern uint8_t pmdl_datacounter;                  // counter of data being sent
extern uint8_t pmdl_data_tosend;                  // flag, 1 if there is data to send
extern uint8_t pmdl_data_len;                     // qty of data to send
extern uint8_t pmdl_full_received;
extern uint8_t pmdl_rx_buffer[MB_UIF_MAXLEN];     // reception buffer
extern uint8_t pmdl_tx_buffer[MB_UIF_TXLEN];      // transmission buffer

extern uint8_t mbuif_pbuffer[MB_UIF_MAXLEN];      // simulates the command received
extern uint8_t index;
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
extern uint8_t PM_DL_Data08Exchange (uint8_t data_out);
extern uint8_t PM_DL_Data08ExchangeX (uint8_t data_out);
extern uint16_t GET16(uint8_t *rxbuffer, uint8_t rxindex);
extern uint32_t GET32(uint8_t *rxbuffer, uint8_t rxindex);
extern void SET32(uint8_t *txbuffer, uint32_t txindex, uint32_t data_out);
extern void SET16(uint8_t *txbuffer, uint32_t txindex, uint16_t data_out);
extern void uif_ApplicationInitData(void);
extern void PM_DL_ClearExchangeData(void);

extern void MB_UIF_CMD_Execute();
extern uint8_t MB_UIF_CMD_SET_PROCESS(uint8_t *rx_spi_data, uint8_t rx_spi_len);
extern uint8_t MB_UIF_CMD_GET_PROCESS(uint8_t *rx_spi_data, uint8_t rx_spi_len);
extern uint8_t MB_UIF_CMD_TYPE(uint8_t mb_uif_cmd);
extern uint8_t MB_UIF_CMDS_GET(uint8_t mb_uif_cmd, uint8_t index);              // (from D[1]:D[0])
extern void UIF_MB_CLR_CMD();
// -----------------------------------------------------------------------------------
// END uifmbprotocol.h
// -----------------------------------------------------------------------------------

#endif