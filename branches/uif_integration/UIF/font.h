// -----------------------------------------------------------------------------------
// Font for printing ASCII characters on a graphics display

// The first character has the ASCII code 0x20, the last one 0x7F.
// Adresses can be calculated like this:
// (where &data is the beginning adress of the data and asc the ascii code)
// addr = &data + 7 * (asc - 0x20);

// Each byte of a character is a column of it.
// The least significant bit is the top most pixel, the most sig. the bottom.
// I.e. 0x01 is a pixel at the top most row, where 0x80 is one at the bottom.

// An empty column is indicated by 0xAA and is ignored while drawing text.
// If no space indicater is set, or if all characters have the same count
// of space indicators, the font is monospace.
// -----------------------------------------------------------------------------------
#ifndef _font_H_
#define _font_H_
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
extern const uint8_t font_Standard[96][7];
extern const uint8_t font_Arial6[92][7];
extern const uint8_t Terminal6Patterns[96][7];
// -----------------------------------------------------------------------------------
#endif // _font_H_ //
// -----------------------------------------------------------------------------------
