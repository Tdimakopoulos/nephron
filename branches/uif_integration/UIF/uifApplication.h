// -----------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifapplication.h
//! \brief  Nephron+: UIF Application
//!
//! Application Routines
//!
//! \author  Gabriela Dudnik
//! \date    26.12.2011
//! \version 1.0 (gdu)
//! \version 2.0 (nephron+)
// -----------------------------------------------------------------------------------
#ifndef UIFAPPLICATION_H
#define UIFAPPLICATION_H

// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
#define UB0_LEN                 64
#define USCIB0_SB               '*'
#define USCIB0_EB               '+'


// Alarms Control
#define FLASHLIGHT_ON                   1
#define FLASHLIGHT_OFF                  0

#define BUZZER_ON                       1
#define BUZZER_OFF                      0

#define LEDS_TOGGLE                     2
#define LEDS_ON                         1
#define LEDS_OFF                        0

#define IOTEST_ON                       1
#define IOTEST_OFF                      0

// Patient Activity
#define ACTIVITY_STEADY                 0
#define ACTIVITY_LAYING                 1
#define ACTIVITY_WALKING                2
#define ACTIVITY_RUNNING                3
#define ACTIVITY_UNKNOWN                4

// Application
#define WAKD_ATTITUDE_OK                1
#define WAKD_ATTITUDE_KO                2
#define WAKD_ATTITUDE_NOINFO            3

// Communication links
#define COMMLINKS_NONE                  0
#define COMMLINKS_BT                    1
#define COMMLINKS_USB                   2
#define COMMLINKS_BOTH                  3

// WAKD GLOBAL INFO
#define WAKD_READY_TO_WORK_SET          0x00000001
#define WAKD_READY_TO_WORK_RST          0xFFFFFFFE

// Operating modes
#define MICROFLUIDIC_STOP               1
#define MICROFLUIDIC_DIALYSIS           2
#define MICROFLUIDIC_REGENERATION_1     3
#define MICROFLUIDIC_ULTRAFILTRATION    4
#define MICROFLUIDIC_REGENERATION_2     5
#define MICROFLUIDIC_MAINTENANCE        6
#define MICROFLUIDIC_NODIALYSATE        7
#define MICROFLUIDIC_CONFIGURATION      8

// Microfluidic circuits status
#define MFCIRCUIT_OK                    0
#define MFCIRCUIT_ERROR                 1
#define MFCIRCUIT_UNKNOWN               2

// Sensors status
#define SENSOR_OK                       1
#define SENSOR_KO                       2
#define SENSOR_NOINFO                   3

// Deivecs status
#define DEVICE_OK                       1
#define DEVICE_KO                       2
#define DEVICE_NOINFO                   3

// Pumps status
#define PUMP_RUNNING                    0
#define PUMP_STOPPED                    1
#define PUMP_ERROR                      2
#define PUMP_ST_UNKNOWN                 3

/* Alex: Old commands
// COMMANDS ASSIGMENT  (as listed in the display) ------------------------------------
// If we change the list in lcdnephronplus.c, the defines should also change
#define UIF_COMMAND_SHUTDOWN        0
#define UIF_COMMAND_START_OPERATION 1
#define UIF_COMMAND_CHANGE_OPMODE   2
#define UIF_COMMAND_GET_WEIGHT      3
#define UIF_COMMAND_START_SEWSTREAM 4
#define UIF_COMMAND_STOP_SEWSTREAM  5
#define UIF_COMMAND_START_NIBP      6
#define UIF_COMMAND_GOTO_DIALYSIS   7
#define UIF_COMMAND_GOTO_REGEN1     8
#define UIF_COMMAND_GOTO_ULTRA      9
#define UIF_COMMAND_GOTO_REGEN2     10
*/

// COMMANDS ASSIGMENT  (as listed in the display) ------------------------------------
// If we change the list in lcdnephronplus.c, the defines should also change
#define UIF_COMMAND_SHUTDOWN        0
#define UIF_COMMAND_START_OPERATION 1
#define UIF_COMMAND_MANUAL_DIALYSIS 2
#define UIF_COMMAND_GET_WEIGHT      3
#define UIF_COMMAND_STOP_OPERATION  4
#define UIF_COMMAND_STOP_DIALYSIS   5
#define UIF_COMMAND_START_NIBP      6
#define UIF_COMMAND_GOTO_DIALYSIS   7
#define UIF_COMMAND_GOTO_REGEN1     8
#define UIF_COMMAND_GOTO_ULTRA      9
#define UIF_COMMAND_GOTO_REGEN2     10
#define UIF_COMMAND_AUTO_DIALYSIS   11

extern uint8_t operating_state;


// COMMANDS ASSIGMENT  (in a 16-bit word for MB) -------------------------------------
#define MB_COMMAND_SHUTDOWN         0x0001
#define MB_COMMAND_START_OPERATION  0x0002
#define MB_COMMAND_MANUAL_DIALYSIS  0x0004
#define MB_COMMAND_GET_WEIGHT       0x0008
#define MB_COMMAND_STOP_OPERATION   0x0010
#define MB_COMMAND_STOP_DIALYSIS    0x0020
#define MB_COMMAND_AUTO_DIALYSIS    0x0030
#define MB_COMMAND_START_NIBP       0x0040
#define MB_COMMAND_GOTO_DIALYSIS    0x2000
#define MB_COMMAND_GOTO_REGEN1      0x3000
#define MB_COMMAND_GOTO_ULTRA       0x4000
#define MB_COMMAND_GOTO_REGEN2      0x5000

#define MB_CMD_CLR_SHUTDOWN         0xFFFE
#define MB_CMD_CLR_START_OPERATION  0xFFFD
#define MB_CMD_CLR_CHANGE_OPMODE    0xFFFB
#define MB_CMD_CLR_GET_WEIGHT       0xFFF7
#define MB_CMD_CLR_START_SEWSTREAM  0xFFEF
#define MB_CMD_CLR_STOP_SEWSTREAM   0xFFDF
#define MB_CMD_CLR_START_NIBP       0xFFBF

// ----------------------------------------------------------------------------------
// Polarizer Status
#define POLAR_OFF                       0
#define POLAR_DIRECT                    1
#define POLAR_REVERSE                   2
#define POLAR_ERROR                     3
#define POLAR_UNKNOWN                   4

// conductivity status
#define COND_OK                         0
#define COND_KO                         1
#define COND_UNKNOWN                    2

// ecp status
#define ECP_OK                         0
#define ECP_ERR                        1
#define ECP_UNKNOWN                    2

// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// ALARMS CONTROL --------------------------------------------------------------------
extern uint8_t WAKD_FLASHLIGHT_ONOFF;
extern uint8_t WAKD_BUZZER_ONOFF;
extern uint8_t WAKD_LED_BLUE_CONTROL;
extern uint8_t WAKD_LED_GREEN_CONTROL;
extern uint8_t WAKD_LED_RED_CONTROL;
extern uint8_t WAKD_UIF_IO_TEST;

// UIF PATIENT INFO ----------------------------------------------------------------------
extern uint8_t UIF_P_DATA_INITIALS[4];
extern uint8_t UIF_P_DATA_PATIENTCODE[4];
extern uint8_t UIF_P_DATA_GENDER[2];
extern uint8_t UIF_P_DATA_AGE;
extern uint16_t UIF_P_DATA_WEIGHT;
extern uint8_t UIF_P_DATA_HEARTRATE;
extern uint8_t UIF_P_DATA_BREATHINGRATE;
extern uint8_t UIF_P_DATA_ACTIVITYCODE;
extern uint16_t UIF_P_DATA_SISTOLICBP;
extern uint16_t UIF_P_DATA_DIASTOLICBP;

extern uint16_t UIF_P_DATA_NA_ECP1;
extern uint16_t UIF_P_DATA_K_ECP1;
extern uint16_t UIF_P_DATA_PH_ECP1;
extern uint16_t UIF_P_DATA_UREA_ECP1;
extern uint16_t UIF_P_DATA_NA_ECP2;
extern uint16_t UIF_P_DATA_K_ECP2;
extern uint16_t UIF_P_DATA_PH_ECP2;
extern uint16_t UIF_P_DATA_UREA_ECP2;

// WAKD INFO ------------------------------------------------------------------------
extern int16_t  WAKDAttitude;                   // in degrees

// [UIF] DATA -----------------------------------------------------------------------
// [UIF] WAKD PM1 & PM2
extern uint8_t UIF_BPACK_1_Percent;
extern uint8_t UIF_BPACK_2_Percent;
// [UIF] WAKD Status
extern uint32_t UIF_WAKD_Status;
extern uint8_t WAKD_FLAG_READY_TO_WORK;
// 
// [UIF] MB Communication Links Status
extern uint16_t UIF_MB_CLINKS_Status;
// [UIF] WAKD Attitude 
extern uint8_t UIF_WAKD_Attitude;
// [UIF] WAKD Operating Mode
extern uint8_t UIF_WAKD_OperatingMode;
// [UIF] WAKD Microfluidics
extern uint32_t UIF_WAKD_BLCircuit_Status;
extern uint32_t UIF_WAKD_FLCircuit_Status;
// [UIF] WAKD Temperature Sensor
extern uint16_t UIF_BTS_OutletTemperature;       // C
extern uint16_t UIF_BTS_InletTemperature;        // C
// WAKD Conductivity Sensor
extern uint16_t UIF_DCS_Conductance;
extern uint16_t UIF_DCS_Susceptance;
extern uint32_t UIF_DCS_Status;

// [UIF] WAKD Pressure Sensors
extern uint16_t UIF_BPS_Pressure;               // mbar
extern uint16_t UIF_FPS_Pressure;               // mbar

// [UIF] WAKD Pumps Info
extern uint8_t UIF_BLPump_SpeedCode;
extern uint8_t UIF_FLPump_SpeedCode;

// [UIF] WAKD High Flux Dialyser
extern uint8_t UIF_WAKD_HFD_Status;
extern uint8_t UIF_WAKD_HFD_PercentOK;

// [UIF] WAKD Sorbent Unit
extern uint16_t UIF_WAKD_SU_Status;

// [UIF] WAKD Polarizer
extern uint16_t UIF_WAKD_POLAR_Voltage;
extern uint16_t UIF_WAKD_POLAR_Status;

// [UIF] WAKD ECP1, ECP2
extern uint16_t UIF_WAKD_ECP1_Status;
extern uint16_t UIF_WAKD_ECP2_Status;

// [UIF] WAKD PS
extern uint32_t UIF_WAKD_PS_Status;

// [UIF] WAKD ACT
extern uint32_t UIF_WAKD_ACT_Status;

// [UIF] WAKD ALARMS/ERRORS
extern uint8_t UIF_WAKD_ALARMS_Status[4];
extern boolean_t alarms_received[39];
extern boolean_t alarms_ack[39];
extern uint8_t alarm_raised;
extern uint8_t error_raised;
extern boolean_t errors_received[44];
extern boolean_t errors_ack[45];

extern uint8_t UIF_WAKD_ERRORS_Status[4];

// MB PATIENT INFO ----------------------------------------------------------------------
extern uint8_t MB_P_DATA_INITIALS[4];
extern uint8_t MB_P_DATA_PATIENTCODE[4];
extern uint8_t MB_P_DATA_GENDER[2];
extern uint8_t MB_P_DATA_AGE;
extern uint16_t MB_P_DATA_WEIGHT;
extern uint8_t MB_P_DATA_HEARTRATE;
extern uint8_t MB_P_DATA_BREATHINGRATE;
extern uint8_t MB_P_DATA_ACTIVITYCODE;
extern uint16_t MB_P_DATA_SISTOLICBP;
extern uint16_t MB_P_DATA_DIASTOLICBP;

extern uint16_t MB_P_DATA_NA_ECP1;
extern uint16_t MB_P_DATA_K_ECP1;
extern uint16_t MB_P_DATA_PH_ECP1;
extern uint16_t MB_P_DATA_UREA_ECP1;
extern uint16_t MB_P_DATA_NA_ECP2;
extern uint16_t MB_P_DATA_K_ECP2;
extern uint16_t MB_P_DATA_PH_ECP2;
extern uint16_t MB_P_DATA_UREA_ECP2;

// Battery Packs Variables -----------------------------------------------------------
// Battery Pack 1
extern uint8_t MB_BPACK_1_Percent;
extern uint16_t MB_BPACK_1_RemainingCapacity;
// Battery Pack 2
extern uint8_t MB_BPACK_2_Percent;
extern uint16_t MB_BPACK_2_RemainingCapacity;
// WAKD Status 
extern uint32_t MB_WAKD_Status;
// WAKD Attitude
extern uint16_t MB_WAKD_Attitude;
// CB Status
extern uint16_t MB_CLINKS_Status;
// WAKD Operating Mode
extern uint16_t MB_WAKD_OperatingMode;
// WAKD Microfluidics
extern uint32_t MB_WAKD_BLCircuit_Status;
extern uint32_t MB_WAKD_FLCircuit_Status;
// WAKD Pumps
extern uint16_t MB_WAKD_BLPump_Speed;
extern uint16_t MB_WAKD_BLPump_DirStatus;
extern uint8_t MB_WAKD_BLPump_SpeedCode;
extern uint16_t MB_WAKD_FLPump_Speed;
extern uint16_t MB_WAKD_FLPump_DirStatus;
extern uint8_t MB_WAKD_FLPump_SpeedCode;
// WAKD Temperature Sensor
extern uint16_t MB_WAKD_BTS_InletTemperature;
extern uint16_t MB_WAKD_BTS_OutletTemperature;
// WAKD Pressure Sensors
extern uint16_t MB_WAKD_BPS_Pressure;
extern uint16_t MB_WAKD_FPS_Pressure;
// WAKD Conductivity Sensor
extern uint16_t MB_WAKD_DCS_Conductance;
extern uint16_t MB_WAKD_DCS_Susceptance;
// WAKD High Flux Dialyser
extern uint8_t MB_WAKD_HFD_Status;
extern uint8_t MB_WAKD_HFD_PercentOK;
// WAKD Sorbent Unit
extern uint16_t MB_WAKD_SU_Status;
// WAKD Polarizer
extern uint16_t MB_WAKD_POLAR_Voltage;
extern uint16_t MB_WAKD_POLAR_Status;
// WAKD DEVICES
extern uint16_t MB_WAKD_ECP1_Status;
extern uint16_t MB_WAKD_ECP2_Status;
extern uint32_t MB_WAKD_PS_Status;
extern uint32_t MB_WAKD_ACT_Status;
// WAKD FEEDBACK FROM UIF COMMANDS
extern uint16_t MB_CMDS_Register;

// WAKD Alarms, Errors, Link Test
extern uint8_t MB_WAKD_ALARMS_Status[4];
extern uint8_t MB_WAKD_ERRORS_Status[4];
extern uint16_t MB_WAKD_UIF_LinkTest;
// UIF Information
extern uint16_t UIF_CMDS_Register;
extern uint16_t UIF_OPMS_Register;
extern uint16_t UIF_MSGS_Register;

// data received flags
extern boolean_t spi_bp1_voltage_ok;
extern boolean_t spi_bp2_voltage_ok;
extern boolean_t spi_bp1_remainingcapacity_ok;
extern boolean_t spi_bp2_remainingcapacity_ok;
extern boolean_t spi_wakd_status_ok;
extern boolean_t spi_wakd_attitude_ok;
extern boolean_t spi_clinks_status_ok;
extern boolean_t spi_wakd_operatingmode_ok;
extern boolean_t spi_wakd_blcircuit_status_ok;
extern boolean_t spi_wakd_flcircuit_status_ok;
extern boolean_t spi_wakd_blpump_info_ok;
extern boolean_t spi_wakd_flpump_info_ok;
extern boolean_t spi_wakd_bts_data_ok;
extern boolean_t spi_wakd_bps_data_ok;
extern boolean_t spi_wakd_fps_data_ok;
extern boolean_t spi_wakd_dcs_data_ok;
extern boolean_t spi_wakd_hfd_status_ok;
extern boolean_t spi_wakd_su_status_ok;
extern boolean_t spi_wakd_polar_data_ok;
extern boolean_t spi_wakd_ecps_status_ok;
extern boolean_t spi_wakd_ps_status_ok;
extern boolean_t spi_wakd_act_status_ok;
extern boolean_t spi_wakd_cmds_acknowledge_ok;
extern boolean_t spi_wakd_pdata_firstname_ok;
extern boolean_t spi_wakd_pdata_lastname_ok;
extern boolean_t spi_wakd_pdata_genderage_ok;
extern boolean_t spi_wakd_pdata_weight_ok;
extern boolean_t spi_wakd_pdata_hr_ok;
extern boolean_t spi_wakd_pdata_br_ok;
extern boolean_t spi_wakd_pdata_activity_ok;
extern boolean_t spi_wakd_pdata_bloodpressure_ok;
extern boolean_t spi_nibp_data_ok;
extern boolean_t spi_wakd_ecp1_a_data_ok;
extern boolean_t spi_wakd_ecp1_b_data_ok;
extern boolean_t spi_wakd_ecp2_a_data_ok;
extern boolean_t spi_wakd_ecp2_b_data_ok;
extern boolean_t spi_wakd_alarms_status_ok;
extern boolean_t spi_wakd_errors_status_ok;
#if 0
extern boolean_t spi_wakd_cmd_shutdown_ok;
extern boolean_t spi_wakd_cmd_start_op_ok;
extern boolean_t spi_wakd_cmd_mode_op_ok;
extern boolean_t spi_wakd_cmd_getweight_ok;
extern boolean_t spi_wakd_cmd_start_sew_ok;
extern boolean_t spi_wakd_cmd_stop_sew_ok;
extern boolean_t spi_wakd_cmd_start_nibp_ok;
#endif

// SOURCE: MSP (to be revised) -------------------------------------------------------
extern unsigned int aux_calculation;

extern uint8_t bpack1_percent;
extern uint8_t bpack2_percent;
extern uint8_t bpack1_autonomy;
extern uint8_t bpack2_autonomy;

// USCI B0 USCI B1 TESTS -------------------------------------------------------------
extern uint8_t *UIFTxData;                           // Pointer to TX data
extern uint8_t MST_Data;
extern uint8_t start_rx_ok;
extern uint8_t stop_rx_ok;
extern uint8_t usci_b0_counter;
extern uint8_t usci_b0_received;
extern uint8_t usci_b0_data_rx[UB0_LEN];
extern uint8_t usci_b0_data_tx[UB0_LEN];

// Status & Error Variables ----------------------------------------------------------

// SOURCE: ARM (to be revised) -------------------------------------------------------

// individual flags to be used by the display ----------------------------------------

// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
extern void usci_b0_ClearData(void);
extern void buzzer_Test(void);
extern uint8_t Calculate_WAKDAttitude_Code (uint8_t);
extern uint8_t Calculate_WAKDOperatingMode_Code (uint8_t);
extern uint8_t Calculate_MBCLINKS_Code (uint8_t);
extern uint8_t Calculate_WAKDCSensorStatus_Code (uint8_t);
extern uint8_t Calculate_MFCircuit_Code (uint8_t);
extern uint8_t Calculate_HFDstatus_Code (uint8_t);
extern uint8_t Calculate_SUstatus_Code (uint8_t);
extern uint8_t Calculate_POLARstatus_Code (uint8_t);
extern uint8_t Calculate_ECPSstatus_Code (uint8_t);
extern void LCD_UPDATE_VARIABLES(void);
extern void MB_SET_COMMAND_FLAG(uint16_t);                                       // SET FLAG --> COMMAND
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// END UIFAPPLICATION.H
// -----------------------------------------------------------------------------------
