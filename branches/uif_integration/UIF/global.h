// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   global.h
//! \brief  General declaration
//!
//! Declaration of useful macros, constants
//! Definition of user types
//!
//! \author  Kaeser Ch.
//! \date    09.06.2009
//! \version 1.0 First version (CKs)
//! \version 1.1 Modification of the typedef (JAP)
//! \version 2.0 Adaptation to the programming guidelines (JAP)
//! \version 2.1 Adaptation to the programming guidelines (CKs)
// -----------------------------------------------------------------------------------
#ifndef _GLOBAL_H_
#define _GLOBAL_H_
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
#define MASK_BIT0       (0x0001)
#define MASK_BIT1       (0x0002)
#define MASK_BIT2       (0x0004)
#define MASK_BIT3       (0x0008)
#define MASK_BIT4       (0x0010)
#define MASK_BIT5       (0x0020)
#define MASK_BIT6       (0x0040)
#define MASK_BIT7       (0x0080)
#define MASK_BIT8       (0x0100)
#define MASK_BIT9       (0x0200)
#define MASK_BITA       (0x0400)
#define MASK_BITB       (0x0800)
#define MASK_BITC       (0x1000)
#define MASK_BITD       (0x2000)
#define MASK_BITE       (0x4000)
#define MASK_BITF       (0x8000)

#define MASK_BIT10      MASK_BITA
#define MASK_BIT11      MASK_BITB
#define MASK_BIT12      MASK_BITC
#define MASK_BIT13      MASK_BITD
#define MASK_BIT14      MASK_BITE
#define MASK_BIT15      MASK_BITF

#define TRUE   (0 == 0)
#define FALSE  (0 == 1)

#ifndef NULL
#define NULL  ((void *) 0)
#endif

#define  MAX(x, y)  	((x) > (y) ? (x) : (y))
#define  MIN(x, y)   	((x) < (y) ? (x) : (y))
#define  ABS(x)         (((x) < 0) ? -(x) : (x))
#define  NB_ELEM(Array) (sizeof (Array) / sizeof (*(Array)))
#define  LOW_BYTE(x)  	((uint8_t) ((x) & 0xFF))
#define  HIGH_BYTE(x)  	((uint8_t) (((x) & 0xFF00) >> 8))

#define  SET_BIT(a,b)   ((a) |= (1 << (b)))
#define  CLR_BIT(a,b)   ((a) &= ~(1 << (b)))
#define  TST_BIT(a,b)   ((a) & (1 << (b)))

#define  SET_MASK(data,set)          ((data) |= (set))
#define  CLR_MASK(data,set)          ((data) &= ~(set))
#define  CLR_SET_MASK(data,clr,set)  ((data) = ((data) & ~(clr)) | (set))


// -----------------------------------------------------------------------------------
// Exported type definitions
// -----------------------------------------------------------------------------------
typedef  unsigned char       boolean_t;  //!< Boolean quantity
typedef  unsigned char       uint8_t;    //!< Unsigned 8 bit quantity
typedef  signed   char       int8_t;	 //!< Signed 8 bit quantity
typedef  unsigned short      uint16_t;   //!< Unsigned 16 bit quantity
typedef  signed   short      int16_t;    //!< Signed 16 bit quantity
typedef  unsigned long       uint32_t;   //!< Unsigned 32 bit quantity
typedef  signed   long       int32_t;    //!< Signed 32 bit quantity
typedef  signed long long    int64_t;    //!< Signed 64 bit quantity
typedef  unsigned long long  uint64_t;   //!< Unsigned 64 bit quantity

//! Bits structure
typedef struct
{
   unsigned b0   : 1;
   unsigned b1   : 1;
   unsigned b2   : 1;
   unsigned b3   : 1;
   unsigned b4   : 1;
   unsigned b5   : 1;
   unsigned b6   : 1;
   unsigned b7   : 1;
} Bit_ts;

//! Byte structure
typedef  union
{
  Bit_ts    bits;    //!< Bit field access
  uint8_t   byte;    //!< Byte access
} BitUnionByte_tu;

// -----------------------------------------------------------------------------------
#endif // _GLOBAL_H_ //
// -----------------------------------------------------------------------------------



