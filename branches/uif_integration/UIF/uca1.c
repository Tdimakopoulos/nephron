// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   UCA1.c
//! \brief  UCA1 module functions
//!
//! Creation of the function of initialization and communication for UCA1 in spi mode.
//!
//! \author  Porchet J-A
//! \date    17.01.2010
//! \version 1.0
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "uca1.h"
#include "io.h"
// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the UCA1 in spi mode
//!
//! This function initializes the UCA1 hardware to operate in spi mode.
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
void uca1InitJ(void)
{
    UCA1IE = 0;                     // Disable the interruption
    UCA1CTL1 = UCSWRST;             //  Put state machine in reset
    
    // Clock polarity high, MSB first, 8-bit SPI master, 3-pin
    UCA1CTL0 = UCCKPH|UCMSB|UCMST|UCMODE_0|UCSYNC;  
    
    UCA1CTL1 |= UCSSEL_2;           // SMCLK
    UCA1BR0 = 0x08;                 // UCA1CLK = SMCLK/8 = 1MHz
    UCA1BR1 = 0x00;
    
    UCA1STAT = 0;                   //  No listen mode, reset flags

    CLR_MASK(UCA1IFG,UCTXIFG);      // Clear the receive interrupt flag
}

// -----------------------------------------------------------------------------------
//! \brief  Initialization of the UCA1 in SPI MASTER
//!
//! This function initializes the UCA1 hardware to operate in SPI MASTER.
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
void uca1InitG(void)
{
  UCA1CTL1 |= UCSWRST;

  UCA1CTL0 |= UCMST+UCSYNC+UCCKPL+UCMSB;    
                                    // b0: sync, b2,b1: 3-pin,
	                            // b3: SPI master, b4: 8-bit, b5: msb 1st
	                            // b6:clk inactive high
	                            // b7:data changes 1st, latches 2nd
   // @@@@ 03-12-2009 = 4-MHZ?
  UCA1CTL1 |= UCSSEL_2;             // SMCLK
  UCA1BR0 = 0x02;                   // /2
  UCA1BR1 = 0;			    // prescaler: UCB0BR0 + UCB0BR1x256	
  UCA1CTL1 &= ~UCSWRST;	            // **Initialize USCI state machine**
  //UCA1IE |= UCRXIE;	            // Enable USCI_A1 RX interrupt

}  
// -----------------------------------------------------------------------------------
//! \brief  Transmission of a byte by the UCB3
//!
//! Sends a byte to the chip SC16IS760, waits the transmition end
//!
//! \param   Byte : value which will be sent by the UCB3
//! \return  void
// -----------------------------------------------------------------------------------
void uca1_Xmit1Byte(uint8_t Byte)
{
    IO_UI_CS1B_LOW;
    
    UCA1TXBUF = Byte;           // Send a byte
    while(!(UCA1IFG&UCRXIFG));  // Wait till character received
    CLR_MASK(UCA1IFG,UCRXIFG);  // Clear the receive interrupt flag
    
    IO_UI_CS1B_HIGH;
}
// -----------------------------------------------------------------------------------
//! \brief  Transmission of bytes by the UCB3
//!
//! This function transmits a fixed number of bytes by the UCB3 to the chip SC16IS760
//! waits the transmition end for each byte
//!
//! \param   BytePointer : pointer on the bytes which will be sent
//! \param   BytesNbre : bytes number to send
//! \return  void
// -----------------------------------------------------------------------------------
void uca1_XmitXByte(uint8_t *BytePointer, uint8_t BytesNbre)
{
    uint8_t i;

    IO_UI_CS1B_LOW;  
    
    for(i=0; i<BytesNbre; i++)
    {
        UCA1TXBUF = *BytePointer++; // Send a byte
        while(!(UCA1IFG&UCRXIFG));  // Wait till character received
        CLR_MASK(UCA1IFG,UCRXIFG);  // Clear the receive interrupt flag
    }
    
    IO_UI_CS1B_HIGH;
}
