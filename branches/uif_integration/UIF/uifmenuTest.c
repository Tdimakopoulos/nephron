// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifmenuTest.h
//! \brief  Nephron+: UIF Processor
//!
//! Menu Test: Simulates the three behaviours
//!
//! \author  Gabriela Dudnik
//! \date    07.01.2012
//! \version 1.0 (gdu)
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include <msp430.h>
#include "io.h"
#include "clock.h"
#include "uifmbprotocol.h"
#include "uifApplication.h"
#include "uifmenutest.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
#define KeysDelay 750 //250

// simulated messages from MB
#if 0
const uint8_t MB_UIF_MSG_VBAT1_INFO[MB_UIF_QTY_VBAT1_INFO] = {MB_UIF_CMD_VBAT1_INFO,0x04,0xD2,0x10,0xE1,0xA5,0x5A,0x55};                
const uint8_t MB_UIF_MSG_VBAT2_INFO[MB_UIF_QTY_VBAT2_INFO] = {MB_UIF_CMD_VBAT2_INFO,0x09,0x29,0x15,0x38,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_STATUS[MB_UIF_QTY_WAKD_STATUS] = {MB_UIF_CMD_WAKD_STATUS,0x49,0x96,0x02,0xD2,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_ATTITUDE[MB_UIF_QTY_WAKD_ATTITUDE] = {MB_UIF_CMD_WAKD_ATTITUDE,0x0D,0x05,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_COMMLINK[MB_UIF_QTY_WAKD_COMMLINK] = {MB_UIF_CMD_WAKD_COMMLINK,0x11,0x5C,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_OPMODE[MB_UIF_QTY_WAKD_OPMODE] = {MB_UIF_CMD_WAKD_OPMODE,0x15,0xB3,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS] = {MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS,0x3A,0xDE,0x68,0xB1,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS] = {MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS,0x3A,0xDE,0x68,0xB1,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_BLPUMP_INFO[MB_UIF_QTY_WAKD_BLPUMP_INFO] = {MB_UIF_CMD_WAKD_BLPUMP_INFO,0x27,0x0F,0x22,0xB8,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_FLPUMP_INFO[MB_UIF_QTY_WAKD_FLPUMP_INFO] = {MB_UIF_CMD_WAKD_FLPUMP_INFO,0x1E,0x61,0x1A,0x0A,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_BLTEMPERATURE[MB_UIF_QTY_WAKD_BLTEMPERATURE] = {MB_UIF_CMD_WAKD_BLTEMPERATURE,0x08,0xAE,0x11,0x5C,0x1A,0x0A,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE] = {MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE,0x00,0x7B,0x01,0x41,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE] = {MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE,0x01,0xC8,0x02,0x8E,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_FLCONDUCTIVITY[MB_UIF_QTY_WAKD_FLCONDUCTIVITY] = {MB_UIF_CMD_WAKD_FLCONDUCTIVITY,0x08,0xAE,0x11,0x5C,0x1A,0x0A,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_HFD_INFO[MB_UIF_QTY_WAKD_HFD_INFO] = {MB_UIF_CMD_WAKD_HFD_INFO,0x15,0xB3,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_SU_INFO[MB_UIF_QTY_WAKD_SU_INFO] = {MB_UIF_CMD_WAKD_SU_INFO,0x09,0xDD,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_POLAR_INFO[MB_UIF_QTY_WAKD_POLAR_INFO] = {MB_UIF_CMD_WAKD_POLAR_INFO,0x03,0xDB,0x03,0x15,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_ECPS_INFO[MB_UIF_QTY_WAKD_ECP1_INFO] = {MB_UIF_CMD_WAKD_ECPS_INFO,0x15,0x4E,0x11,0xC1,0x55};
const uint8_t MB_UIF_MSG_PDATA_FIRSTNAME[MB_UIF_QTY_PDATA_FIRSTNAME] = {MB_UIF_CMD_PDATA_INITIALS,'C','O','S','T','A','S',' ',' ',' ',' ',' ',' ',' ','\0',0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_LASTNAME[MB_UIF_QTY_PDATA_LASTNAME] = {MB_UIF_CMD_PDATA_PATIENTCODE,'K','O','T','S','O','K','A','L','I','S',' ',' ',' ','\0',0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_GENDERAGE[MB_UIF_QTY_PDATA_GENDERAGE] = {MB_UIF_CMD_PDATA_GENDERAGE,'M',37,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_WEIGHT[MB_UIF_QTY_PDATA_WEIGHT] = {MB_UIF_CMD_PDATA_WEIGHT,0x00,0x4B,0xA5,0x5A,0x55};
//const uint8_t MB_UIF_MSG_PDATA_HEARTRATE[MB_UIF_QTY_PDATA_HEARTRATE] = {MB_UIF_CMD_PDATA_HEARTRATE,0x45,0xA5,0x5A,0x55};
//const uint8_t MB_UIF_MSG_PDATA_BREATHRATE[MB_UIF_QTY_PDATA_BREATHRATE] = {MB_UIF_CMD_PDATA_BREATHRATE,0x0F,0xA5,0x5A,0x55};
//const uint8_t MB_UIF_MSG_PDATA_ACTIVITY[MB_UIF_QTY_PDATA_ACTIVITY] = {MB_UIF_CMD_PDATA_ACTIVITY,ACTIVITY_RUNNING,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_SEWALL[MB_UIF_QTY_PDATA_SEWALL] = {MB_UIF_CMD_PDATA_SEWALL,0x45,0x0F,ACTIVITY_WALKING,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_BLPRESSURE[MB_UIF_QTY_PDATA_BLPRESSURE] = {MB_UIF_CMD_PDATA_BLPRESSURE,0x01,0xC8,0x03,0x15,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_PDATA_ECP1_A[MB_UIF_QTY_CMD_SET] = {MB_UIF_CMD_PDATA_ECP1_A,0x00,0x6F,0x00,0xDE,MB_UIF_CMD_END};
const uint8_t MB_UIF_MSG_PDATA_ECP1_B[MB_UIF_QTY_CMD_SET] = {MB_UIF_CMD_PDATA_ECP1_B,0x01,0x4D,0x01,0xBC,MB_UIF_CMD_END};
const uint8_t MB_UIF_MSG_PDATA_ECP2_A[MB_UIF_QTY_CMD_SET] = {MB_UIF_CMD_PDATA_ECP2_A,0x02,0x2B,0x02,0x9A,MB_UIF_CMD_END};
const uint8_t MB_UIF_MSG_PDATA_ECP2_B[MB_UIF_QTY_CMD_SET] = {MB_UIF_CMD_PDATA_ECP2_B,0x03,0x09,0x03,0x78,MB_UIF_CMD_END};
const uint8_t MB_UIF_MSG_ALARMS[MB_UIF_QTY_ALARMS] = {MB_UIF_CMD_ALARMS,0x00,0x00,0x0D,0x05,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_ERRORS[MB_UIF_QTY_ERRORS] = {MB_UIF_CMD_ERRORS,0x00,0x00,0x1A,0x0A,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_TEST_LINK[MB_UIF_QTY_TEST_LINK] = {MB_UIF_CMD_TEST_LINK,0x27,0x0F,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_SHUTDOWN[MB_UIF_QTY_WAKD_SHUTDOWN] = {MB_UIF_CMD_WAKD_SHUTDOWN,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_START_OPERATION[MB_UIF_QTY_WAKD_START_OPERATION] = {MB_UIF_CMD_WAKD_START_OPERATION,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_WAKD_MODE_OPERATION[MB_UIF_QTY_WAKD_MODE_OPERATION] = {MB_UIF_CMD_WAKD_MODE_OPERATION,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_SCALE_GET_WEIGHT[MB_UIF_QTY_SCALE_GET_WEIGHT] = {MB_UIF_CMD_SCALE_GET_WEIGHT,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_SEW_START_STREAMING[MB_UIF_QTY_SEW_START_STREAMING] = {MB_UIF_CMD_SEW_START_STREAMING,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_SEW_STOP_STREAMING[MB_UIF_QTY_SEW_STOP_STREAMING] = {MB_UIF_CMD_SEW_STOP_STREAMING,0xA5,0x5A,0x55};
const uint8_t MB_UIF_MSG_NIBP_START_MEASUREMENT[MB_UIF_QTY_NIBP_START_MEASUREMENT] = {MB_UIF_CMD_NIBP_START_MEASUREMENT,0xA5,0x5A,0x55};
#endif
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  MB_UIF_CMD_TEST
//!         ...
//!
//! Actions if pressing keys on Presentation
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
#if 0
void MB_UIF_CMD_TEST(){
    uint8_t w=0;
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBAT1_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBAT1_INFO[w];
    for(w=MB_UIF_QTY_VBAT1_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBAT1_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBAT2_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBAT2_INFO[w];
    for(w=MB_UIF_QTY_VBAT2_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBAT2_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBATS_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBATS_INFO[w];
    for(w=MB_UIF_QTY_VBATS_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBATS_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_STATUS;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ATTITUDE;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ATTITUDE[w];
    for(w=MB_UIF_QTY_WAKD_ATTITUDE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ATTITUDE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_COMMLINK;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_COMMLINK[w];
    for(w=MB_UIF_QTY_WAKD_COMMLINK;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_COMMLINK){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }  
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_OPMODE;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_OPMODE[w];
    for(w=MB_UIF_QTY_WAKD_OPMODE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_OPMODE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }

    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_MFCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLPUMP_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLPUMP_INFO[w];
    for(w=MB_UIF_QTY_WAKD_BLPUMP_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLPUMP_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLPUMP_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLPUMP_INFO[w];
    for(w=MB_UIF_QTY_WAKD_FLPUMP_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLPUMP_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLTEMPERATURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLTEMPERATURE[w];
    for(w=MB_UIF_QTY_WAKD_BLTEMPERATURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLTEMPERATURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[w];
    for(w=MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[w];
    for(w=MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCONDUCTIVITY;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCONDUCTIVITY[w];
    for(w=MB_UIF_QTY_WAKD_FLCONDUCTIVITY;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCONDUCTIVITY){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_HFD_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_HFD_INFO[w];
    for(w=MB_UIF_QTY_WAKD_HFD_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_HFD_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_SU_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_SU_INFO[w];
    for(w=MB_UIF_QTY_WAKD_SU_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_SU_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }   
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_POLAR_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_POLAR_INFO[w];
    for(w=MB_UIF_QTY_WAKD_POLAR_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_POLAR_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECP1_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECP1_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECP1_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECP1_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECP2_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECP2_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECP2_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECP2_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECPS_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECPS_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECPS_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECPS_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_FIRSTNAME;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_FIRSTNAME[w];
    for(w=MB_UIF_QTY_PDATA_FIRSTNAME;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_FIRSTNAME){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_LASTNAME;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_LASTNAME[w];
    for(w=MB_UIF_QTY_PDATA_LASTNAME;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_LASTNAME){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_GENDERAGE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_GENDERAGE[w];
    for(w=MB_UIF_QTY_PDATA_GENDERAGE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_GENDERAGE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_WEIGHT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_WEIGHT[w];
    for(w=MB_UIF_QTY_PDATA_WEIGHT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_WEIGHT){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_HEARTRATE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_HEARTRATE[w];
    for(w=MB_UIF_QTY_PDATA_HEARTRATE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_HEARTRATE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_BREATHRATE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_BREATHRATE[w];
    for(w=MB_UIF_QTY_PDATA_BREATHRATE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_BREATHRATE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ACTIVITY;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ACTIVITY[w];
    for(w=MB_UIF_QTY_PDATA_ACTIVITY;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ACTIVITY){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_SEWALL;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_SEWALL[w];
    for(w=MB_UIF_QTY_PDATA_SEWALL;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_SEWALL){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_BLPRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_BLPRESSURE[w];
    for(w=MB_UIF_QTY_PDATA_BLPRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_BLPRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ECP1;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ECP1[w];
    for(w=MB_UIF_QTY_PDATA_ECP1;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ECP1){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ECP2;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ECP2[w];
    for(w=MB_UIF_QTY_PDATA_ECP2;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ECP2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_ALARMS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_ALARMS[w];
    for(w=MB_UIF_QTY_ALARMS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_ALARMS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_ERRORS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_ERRORS[w];
    for(w=MB_UIF_QTY_ERRORS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_ERRORS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_TEST_LINK;w++) mbuif_pbuffer[w] = MB_UIF_MSG_TEST_LINK[w];
    for(w=MB_UIF_QTY_TEST_LINK;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_TEST_LINK+MB_UIF_QTY_ANSWER_2-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_SHUTDOWN;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_SHUTDOWN[w];
    for(w=MB_UIF_QTY_WAKD_SHUTDOWN;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_SHUTDOWN+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_START_OPERATION;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_START_OPERATION[w];
    for(w=MB_UIF_QTY_WAKD_START_OPERATION;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_START_OPERATION+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_MODE_OPERATION;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_MODE_OPERATION[w];
    for(w=MB_UIF_QTY_WAKD_MODE_OPERATION;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_MODE_OPERATION+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SCALE_GET_WEIGHT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SCALE_GET_WEIGHT[w];
    for(w=MB_UIF_QTY_SCALE_GET_WEIGHT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SCALE_GET_WEIGHT+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SEW_START_STREAMING;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SEW_START_STREAMING[w];
    for(w=MB_UIF_QTY_SEW_START_STREAMING;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SEW_START_STREAMING+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SEW_STOP_STREAMING;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SEW_STOP_STREAMING[w];
    for(w=MB_UIF_QTY_SEW_STOP_STREAMING;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SEW_STOP_STREAMING+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------  
    for(w=0;w<MB_UIF_QTY_NIBP_START_MEASUREMENT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_NIBP_START_MEASUREMENT[w];
    for(w=MB_UIF_QTY_NIBP_START_MEASUREMENT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_NIBP_START_MEASUREMENT+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
}
#endif
// -----------------------------------------------------------------------------------
//! \brief  UIF_MenuTest
//!         ...
//!
//! Actions if pressing keys on Presentation
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void UIF_MenuTest(){

#if 1 // PATIENT DATA
    // TEST OK; PRESENTATION --> MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST OK: MENU --> PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST BOTTOM: DOWN TO PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN TO PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN TO PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN TO PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN TO PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST TOP: UP IN PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN PATIENT DATA SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
    // TEST LEFT: BACK TO MAIN MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
    // TEST LEFT: BACK TO MAIN MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
#endif    
#if 1 // WAKD STATUS
    // TEST OK; PRESENTATION --> MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST BOTTOM: DOWN TO WAKD STATUS
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST OK: MENU --> WAKD STATUS
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST BOTTOM: DOWN IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST TOP: UP IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN WAKD STATUS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
    // TEST LEFT: BACK TO MAIN MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
    // TEST LEFT: BACK TO MAIN MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
#endif    
#if 1 // COMMAND
    // TEST OK; PRESENTATION --> MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST BOTTOM: DOWN TO WAKD STATUS
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN TO WAKD COMMANDS
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST OK: MENU --> WAKD COMMANDS (SHUTDOWN WAKD)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (START OPERATION)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (STOP OPERATION)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (GET WEIGHT)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (START SEW STREAM)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (STOP SEW STREAM)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU (START NIBP)
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
#if 0    
#endif    
    // TEST OK: ITEM --> SELECT COMMAND
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST OK: ITEM --> OK COMMAND
    DELAY_MS(KeysDelay);
    DELAY_MS(KeysDelay);
    DELAY_MS(KeysDelay);
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_1_OK;
    // TEST LEFT: BACK TO WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
#if 1    
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST BOTTOM: DOWN IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_3_BOTTOM;
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;
    // TEST TOP: UP IN WAKD COMMANDS SUBMENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_2_TOP;   
#endif
    
    // TEST LEFT: BACK TO MAIN MENU
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
    // TEST LEFT: BACK TO PRESENTATION SCREEN
    DELAY_MS(KeysDelay);
    P2IFG |= KEY_5_LEFT;
    
#endif
}
// -----------------------------------------------------------------------------------
// END UIFMENUTEST.C
// -----------------------------------------------------------------------------------
