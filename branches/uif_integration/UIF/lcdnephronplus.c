// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   lcdnephronplus.c
//! \brief  specific functions for NEPHRONPLUS LCD
//!
//! NEPHRONPLUS LCD Application routines
//!
//! \author  DUDNIK G.S.
//! \date    20.12.2011
//! \version 1.0 First version (gdu)
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include "io.h"
#include "clock.h"
#include "lcd.h"
#include "lcdnephronplus.h"
#include "uifmenu.h"
#include "uifApplication.h"

#include "font_arialroundedmtbold_26x29_csem.h"
#include "font_arialregular20_numbers.h"
#include "font_symbols.h"
#include "font_nephronlogo.h"

#include "mathtool.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
// Display Text Messages -------------------------------------------------------------
const uint8_t P_DATA_TEXT[15] = {'P','A','T','I','E','N','T',' ','D','A','T','A',' ',' ','\0'};
const uint8_t STATUS_TEXT[15] = {'W','A','K','D',' ','S','T','A','T','U','S',' ',' ',' ','\0'};
const uint8_t CONFIG_TEXT[15] = {'W','A','K','D',' ','C','O','M','M','A','N','D','S',' ','\0'};

const uint8_t YES_NO[2][4]= {
        {'Y','E','S','\0'},
        {'N','O',' ','\0'}
};

const uint8_t CMD_ACCEPTED[2][CMD_ACCEPTED_MAX]= {
        {' ','C','O','M','M','A','N','D',' ','\0'},
        {'A','C','C','E','P','T','E','D','!','\0'},
};

const uint8_t CMD_REJECTED[2][CMD_REJECTED_MAX]= {
        {' ','C','O','M','M','A','N','D',' ','\0'},
        {'R','E','J','E','C','T','E','D','!','\0'},
};

const uint8_t P_DATA_TITLE[P_DATA_QTY][15] = {
	{'P','A','T','I','E','N','T',':','\0','\0','\0','\0','\0','\0'},
	{'I','D','.','C','O','D','E',':','\0','\0','\0','\0','\0','\0'},
	{'M','/','F',':','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
	{'A','G','E',':','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},

        {'W','E','I','G','H','T',':','\0','\0','\0','\0','\0','\0','\0'},
        {'H','R',':','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'B','R',':','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
	{'A','C','T','I','V','.',':','\0','\0','\0','\0','\0','\0','\0'},

        {'N','a','-','1',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'K','+','-','1',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'p','H','-','1',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'U','R','-','1',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},

        {'N','a','-','2',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'K','+','-','2',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'p','H','-','2',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'U','R','-','2',':','\0','\0','\0','\0','\0','\0','\0','\0','\0'},

        {'S','I','S','.','P','R',':','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'D','I','A','.','P','R',':','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},

        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
};

const uint8_t P_DATA_ACTIVITY[5][8]= {
        {'S','T','E','A','D','Y','\0'},
        {'L','A','Y','I','N','G','\0'},
        {'W','A','L','K','I','N','G','\0'},
        {'R','U','N','N','I','N','G','\0'},
        {'U','N','K','N','O','W','N','\0'},
};

const uint8_t ATTITUDE_STATUS[4][4]= {
        {'O','K','!','\0'},
        {'B','A','D','\0'},
        {'?','?','?','\0'},
        {'\0','\0','\0','\0'}      
};


const uint8_t COMM_LINKS[4][7]= {
        {'N','O','N','E','\0','\0','\0'},
        {'B','T','O','O','T','H','\0'},
        {'U','S','B','\0','\0','\0','\0'},
        {'B','T','-','U','S','B','\0'}
};

const uint8_t OPERATING_MODES[8][5]= {
        {'S','T','O','P','\0'},
        {'D','I','A','L','\0'},
        {'R','E','G','1','\0'},
        {'U','F','L','T','\0'},
        {'R','E','G','2','\0'},
        {'M','A','I','N','\0'},
        {'N','O','D','I','\0'},
        {'C','F','G',' ','\0'}
};

const uint8_t MF_CIRCUIT[3][4]= {
        {'O','K','!','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};

const uint8_t PUMP_STATUS[4][4]= {
        {'R','U','N', '\0'},
        {'O','F','F', '\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}     
};
const uint8_t PUMP_SPEEDCODE[26][7]= {
{'1','0','0','%','L','\0','\0'},  // 1
{'8','7','.','5','%','L','\0'},
{'7','5','%','L','\0','\0','\0'},
{'6','2','.','5','%','L','\0'},
{'5','0','%','L','\0','\0','\0'},
{'3','7','.','5','%','L','\0'},
{'2','5','%','L','\0','\0','\0'},
{'1','2','.','5','%','L','\0'},  // 8
{'O','F','F','\0','\0','\0','\0'},  // 9
{'O','F','F','\0','\0','\0','\0'},  // 10
{'O','F','F','\0','\0','\0','\0'},  // 11
{'O','F','F','\0','\0','\0','\0'},  // 12
{'O','F','F','\0','\0','\0','\0'},  // 13
{'O','F','F','\0','\0','\0','\0'},  // 14
{'O','F','F','\0','\0','\0','\0'},  // 15
{'O','F','F','\0','\0','\0','\0'},  // 16
{'O','F','F','\0','\0','\0','\0'},  // 17
{'1','2','.','5','%','R','\0'},  // 18
{'2','5','%','R','\0','\0','\0'},
{'3','7','.','5','%','R','\0'},
{'5','0','%','R','\0','\0','\0'},
{'6','2','.','5','%','R','\0'},
{'7','5','%','R','\0','\0','\0'},
{'8','7','.','5','%','R','\0'},
{'1','0','0','%','L','\0','\0'},  // 25
{'?','?','?','?','?','?','\0'},  // 26 
};

const uint8_t HFD_STATUS[3][4]= {
        {'O','K','!','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};


const uint8_t SU_STATUS[3][4]= {
        {'O','K','!','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};


const uint8_t POLAR_STATUS[3][4]= {
        {'O','K','!','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};

#if 0
const uint8_t POLAR_VAL[5][4]= {
        {'O','F','F','\0'},
        {'(','+',')','\0'},
        {'(','-',')','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};
#endif

const uint8_t CONDUCTIVITY_STATUS[3][7]= {
        {'C','S','.','O','K','!','\0'},
        {'C','S','.','K','O',' ','\0'},
        {'N','O','I','N','F','O','\0'}
};

const uint8_t ECP_STATUS[3][4]= {
        {'O','K','!','\0'},
        {'E','R','R','\0'},
        {'?','?',' ','\0'}
};


const uint8_t STATUS_TITLE[STATUS_QTY][15] = {
	{'V','B','A','T','1',':','\0','\0','\0','\0','\0','\0','\0','\0'},      // BATTERY PACK1 REMAINING AUTONOMY %
	{'V','B','A','T','2',':','\0','\0','\0','\0','\0','\0','\0','\0'},      // BATTERY PACK2 REMAINING AUTONOMY %
	{'W','A','K','D',' ','O','K','?',':','\0','\0','\0','\0','\0'},         // WAKD STATUS
	{'A','T','T','I','T','U','D','E',':','\0','\0','\0','\0','\0'},         // WAKD ATTITUDE

        {'C','O','M','.','L','I','N','K',':','\0','\0','\0','\0','\0'},         // COMMUNICATION LINK STATUS
        {'O','P','.',' ','M','O','D','E',':','\0','\0','\0','\0','\0'},         // OPERATING MODE
	{'B','L','D','.','C','I','R','C',':','\0','\0','\0','\0','\0'},         // BLOOD CIRCUIT STATUS
	{'F','I','L','.','C','I','R','C',':','\0','\0','\0','\0','\0'},         // FILTRATE CIRCUIT STATUS

        {'B','L','D','.','P','U','M','P',':','\0','\0','\0','\0','\0'},         // BLOOD PUMP [DIR(+/-)]SPEED %
        {'B','L','.','T','-','O','U','T',':','\0','\0','\0','\0','\0'},         // BLOOD TEMPERATURE OUTLET
        {'B','L','.','T','-','I','N','P',':','\0','\0','\0','\0','\0'},         // BLOOD TEMPERATURE INLET
        {'B','L','.','P','R','E','S','.',':','\0','\0','\0','\0','\0'},         // BLOOD CIRCUIT PRESSURE

        {'F','I','L',' ','P','U','M','P',':','\0','\0','\0','\0','\0'},         // FILTRATE PUMP [DIR(+/-)]SPEED %
        {'F','I','.','P','R','E','S','.',':','\0','\0','\0','\0','\0'},         // FILTRATE CIRCUIT PRESSURE 
	{'H','F','D', '[','%','O','K',']',' ',':','\0','\0','\0','\0'},         // HFD %OK
	{'H','F','D',' ','S','T','.',' ',':','\0','\0','\0','\0','\0'},         // HFD STATUS
	
	{'C','O','N','D','.','S','T','.',':','\0','\0','\0','\0','\0'},         // DCS STATUS
	{'C','O','N','D','U','C','T','.',':','\0','\0','\0','\0','\0'},         // CONDUCTANCE
	{'S','U','S','C','E','P','T','.',':','\0','\0','\0','\0','\0'},         // SUSCEPTANCE
        {'S','U',' ','S','T','A','T','.',':','\0','\0','\0','\0','\0'},         // SORBENT UNIT STATUS

        {'P','O','L','A','R',' ','S','T',':','\0','\0','\0','\0','\0'},         // POLARIZER STATUS
        {'P','O','L','A','R','.','V','.',':','\0','\0','\0','\0','\0'},         // POLARIZER VOLTAGE
	{'E','C','P','1',' ','S','T','.',':','\0','\0','\0','\0','\0'},         // ECP1 STATUS
	{'E','C','P','2',' ','S','T','.',':','\0','\0','\0','\0','\0'},         // ECP2 STATUS

        {'E','R','R','O','R','S',':','\0','\0','\0','\0','\0','\0','\0','\0'},          // ERRORS TITLE
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ERROR 1
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ERROR 2
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ERROR 3
        
        {'A','L','A','R','M','S',':','\0','\0','\0','\0','\0','\0','\0','\0'},          // ALARMS TITLE    
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ALARM 1
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ALARM 2
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},   // ALARM 3
};

// Holds the type of the alarm
const uint8_t ALARM_TYPE[36][18] = {
  // blood pressure alarms
  {'B', 'P', 's', 'y', 's', ' ', 'L', 'O', 'W', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},          // BP-sys low --> decTreeMsg_detectedBPsysAbsL
  {'B', 'P', 's', 'y', 's', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},           // BP-sys high --> decTreeMsg_detectedBPsysAbsH
  {'B', 'P', 's', 'y', 's', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},           // BP-sys high --> decTreeMsg_detectedBPsysAAbsH
  {'B', 'P', 'd', 'i', 'a', ' ', 'L', 'O', 'W', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},          // BP-dia low --> decTreeMsg_detectedBPdiaAbsL
  {'B', 'P', 'd', 'i', 'a', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},           // BP-dia high --> decTreeMsg_detectedBPdiaAbsH
  {'B', 'P', 'd', 'i', 'a', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},           // BP-dia high --> decTreeMsg_detectedBPdiaAAbsH
  // weight alarms
  {'W', 'E', 'I', 'G', 'H', 'T', ' ', 'L', 'O', 'W', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},           // weight low --> dectreeMsg_detectedWghtAbsL
  {'W', 'E', 'I', 'G', 'H', 'T', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0'},            // weight high --> dectreeMsg_detectedWghtAbsH
  {'W', '.', 'T', 'R', 'E', 'N', 'D', ' ', 'N', 'o', 't', 'N', 'O', 'R', 'M', 'A', 'L'},                  // Weight trend not normal --> dectreeMsg_detectedWghtTrdT
  // actuator data alarms
  {'B', 'L', 'O', 'O', 'D', ' ', 'P', 'U', 'M', 'P', ' ', 'D', 'E', 'V', 'I', 'A', 'T'},                  // Blood pump flow deviation --> dectreeMsg_detectedpumpBaccDevi
  {'F', 'L', 'U', 'I', 'D', ' ', 'P', 'U', 'M', 'P', ' ', 'D', 'E', 'V', 'I', 'A', 'T'},                  // Fluidic pump flow deviation --> dectreeMsg_detectedpumpFaccDevi
  // physical alarms
  {'B', 'L', 'O', 'O', 'D', ' ', 'P', 'R', 'E', 'S', 'U', 'R', 'E', ' ', 'H', 'I', 'G', 'H'},             // Blood pressure high --> dectreeMsg_detectedBPorFPhigh
  {'B', 'L', 'O', 'O', 'D', ' ', 'P', 'R', 'E', 'S', 'U', 'R', 'E', ' ', 'L', 'O', 'W', '\0'},            // Blood pressure low --> dectreeMsg_detectedBPorFPlow
  {'T', 'E', 'M', 'P', '.', ' ', 'H', 'I', 'G', 'H',' ', '-', ' ', 'B', 'T', 'S', 'o', '\0'},             // Temperature high at BTSo --> dectreeMsg_detectedBTSoTH
  {'T', 'E', 'M', 'P', '.', ' ', 'L', 'O', 'W', ' ', '-', ' ', 'B', 'T', 'S', 'o', '\0', '\0'},           // Temperature low at BTSo --> dectreeMsg_detectedBTSoTL
  {'T', 'E', 'M', 'P', '.', ' ', 'H', 'I', 'G', 'H', ' ', '-', ' ', 'B', 'T', 'S', 'i', '\0'},            // Temperature high at BTSi--> dectreeMsg_detectedBTSiTH
  {'T', 'E', 'M', 'P', '.', ' ', 'L', 'O', 'W', ' ', '-', ' ', 'B', 'T', 'S', 'i', '\0', '\0'},           // Temperature low at BTSi--> dectreeMsg_detectedBTSiTL
  // physiological alarms
  {'S', 'O', 'R', 'B', 'E', 'N', 'T', ' ', 'D', 'Y', 'S', 'F', 'U', 'N', 'C', 'T', '\0', '\0'},           // Sorbent dysfunction --> dectreeMsg_detectedSorDys
  {'P', 'H', ' ', 'L', '0', 'W', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}, // pH low --> dectreeMsg_detectedPhAAbsL
  {'P', 'H', ' ', 'H', 'I', 'G', 'H', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'},  // pH high --> dectreeMsg_detectedPhAAbsH
  {'P', 'H', ' ', 'O', 'U', 'T', ' ', 'O', 'F', ' ', 'R', 'A', 'N', 'G', 'E', '\0', '\0', '\0'},          // pH out of normal range --> dectreeMsg_detectedPhAbsLorPhAbsH
  {'P', 'H', ' ', 'D', 'E', 'C', 'R', ' ', 'N', 'O', 'T', ' ', 'N', 'O', 'R', 'M', 'A', 'L'},             // pH decrease out of normal range --> dectreeMsg_detectedPhTrdLPsT
  {'P', 'H', ' ', 'I', 'N', 'C', 'R', ' ', 'N', 'O', 'T', ' ', 'N', 'O', 'R', 'M', 'A', 'L'},             // pH INcrease out of normal range --> dectreeMsg_detectedPhTrdHPsT
  {'N', 'a', '+', ' ', 'O', 'U', 'T', ' ', 'O', 'F', ' ', 'R', 'A', 'N', 'G', 'E', '\0', '\0'},           // Na+ out of normal range --> dectreeMsg_detectedNaAbsLorNaAbsH
  {'N', 'a', '+', ' ', 'T', 'R', 'E', 'N', 'B', ' ', 'N', 'O', 'T', ' ', 'N', 'O', 'R', 'M'},             // Na+ trend not normal --> dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT
  {'R', 'E', 'P', 'L', 'A', 'C', 'E', ' ', 'C', 'A', 'R', 'T', 'R', 'I', 'D', 'G', 'E', '\0'},            // Replace cartridge --> dectreeMsg_detectedKAbsLorKAbsHandSorDys
  {'K', '+', ' ', 'O', 'U', 'T', ' ', 'O', 'F', ' ', 'R', 'A', 'N', 'G', 'E', '\0', '\0', '\0'},          // K+ out of normal range --> dectreeMsg_detectedKAbsLorKAbsHnoSorDys
  {'K', '+', ' ', 'A', 'L', 'A', 'R', 'M', ' ', '(', 'H', 'I', 'G', 'H', ')', '\0', '\0', '\0'},          // K+ Alarm(high) < Kmeasafter --> dectreeMsg_detectedKAAHandKincrease
  {'K', '+', ' ', 'A', 'L', 'A', 'R', 'M', ' ', '(', 'H', 'I', 'G', 'H', ')', '\0', '\0', '\0'},          // K+ Alarm(high) >= Kmeasafter --> dectreeMsg_detectedKAAHnoKincrease
  {'K', '+', ' ', 'T', 'R', 'E', 'N', 'D', ' ', 'D', 'E', 'C', 'R', ' ', 'F', 'A', 'S', 'T'},             // K+ trend is decreasing too fast --> dectreeMsg_detectedKTrdLPsT
  {'C', 'O', 'N', 'T', 'A', 'C', 'T', ' ', 'P', 'H', 'Y', 'S', 'I', 'C', 'I', 'A', 'N', '\0'},            // --> dectreeMsg_detectedKTrdHPsT
  {'K', '+', ' ', 'T', 'R', 'E', 'N', 'D', ' ', 'D', 'E', 'C', 'R', ' ', 'F', 'A', 'S', 'T'},             // K+ trend is decreasing too fast and sorbent dysfunction detected --> dectreeMsg_detectedKTrdofnriandSorDys
  {'C', 'O', 'N', 'T', 'A', 'C', 'T', ' ', 'P', 'H', 'Y', 'S', 'I', 'C', 'I', 'A', 'N', '\0'},            // --> dectreeMsg_detectedPhTrdofnriandSorDys
  {'R', 'E', 'P', 'L', 'A', 'C', 'E', ' ', 'S', 'O', 'R', 'B', ' ', 'C', 'A', 'R', 'T', 'R'},             // Replace sorbent cartridge --> dectreeMsg_detectedUreaAAbsHandSorDys
  {'U', 'R', 'E', 'A', ' ', 'N', 'O', 'T', ' ', 'N', 'O', 'R', 'M', 'A', 'L', '\0', '\0', '\0'},          // Urea out of normal range --> dectreeMsg_detectedUreaAAbsHnoSorDys
  {'U', 'R', 'E', 'A', ' ', 'I', 'N', ' ', 'N', 'O', 'T', ' ', 'N', 'O', 'R', 'M', 'A', 'L'}              // Urea increase (high) out of normal range --> dectreeMsg_detectedUreaTrdHPsT
};


const uint8_t ALARM_MESSAGES[37][37] = {
  {'C', 'o', 'n', 't', 'a', 'c', 't', ' ', 'P', 'h', 'y', 's', 'i', 'c', 'i', 'a', 'n', ' ',
   'a','n','d',' ','p','r','e','s','s',' ','O','K',' ',' ',' ',' ',' ',' ','\0'}
};

/* Alex: Old commands
// If we change the list, the defines should also change (in uifApplication.h)
const uint8_t COMMAND_TITLE[COMMAND_QTY][18] = {
        {'S','H','U','T','D','O','W','N',' ','W','A','K','D','\0','\0','\0','\0','\0'},	
	{'S','T','A','R','T',' ','O','P','E','R','A','T','I','O','N','\0','\0'},	
	{'M','O','D','E',' ','O','P','E','R','A','T','I','O','N','\0','\0','\0'},	
        {'G','E','T',' ','W','E','I','G','H','T','\0','\0','\0','\0','\0','\0','\0'},
	{'S','T','A','R','T',' ','S','E','W',' ','S','T','R','E','A','M','\0'},	
	{'S','T','O','P',' ','S','E','W',' ','S','T','R','E','A','M','\0','\0'},	
	{'S','T','A','R','T',' ','N','I','B','P','\0','\0','\0','\0','\0','\0'},	       
//        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
//        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
//        {'(','.','.','.',')','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'}
};
*/

// If we change the list, the defines should also change (in uifApplication.h)
const uint8_t COMMAND_TITLE[COMMAND_QTY][18] = {
  {'S','T','A','R','T',' ','O','P','E','R','A','T','I','O','N','\0','\0','\0'},
  {'A','U','T','O',' ','D','I','A','L','Y','S','I','S','\0','\0','\0','\0','\0'},
  {'M','A','N','U','A','L',' ','D','I','A','L','Y','S','I','S','\0','\0','\0'},
  {'S','T','O','P',' ','D','I','A','L','Y','S','I','S','\0','\0','\0','\0','\0'},
  {'S','T','O','P',' ','O','P','E','R','A','T','I','O','N','\0','\0','\0','\0'},
  {'G','E','T',' ','W','E','I','G','H','T','\0','\0','\0','\0','\0','\0','\0'},
  {'G','E','T',' ','B','L','O','O','D',' ','P','R','E','S','S','U','R'},
  {'S','H','U','T','D','O','W','N',' ','W','A','K','D','\0','\0','\0','\0','\0'}
};

const uint8_t MODE_TITLE[MODE_QTY][16] = {
  {'D','I','A','L','Y','S','I','S','\0','\0','\0','\0','\0','\0','\0','\0'},
  {'R','E','G','E','N','1','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
  {'U','L','T','R','A','F','I','L','T','R','A','T','I','O','N','\0'},
  {'R','E','G','E','N','2','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'}
};

const uint8_t COMMAND_INFO[COMMAND_QTY][37] = {
	{'P','r','e','s','s',' ','O','K',' ','t','o',' ','s','t','a','r','t',' ',
	 'S','h','u','t','D','o','w','n',' ','p','r','o','c','e','d','u','r','e','\0'},
        {'C','o','n','n','e','c','t',' ','f','u','l','l',' ','W','A','K','D',' ',
	 'a','n','d',' ','p','r','e','s','s',' ','O','K',' ',' ',' ',' ',' ',' ','\0'},
	{'S','e','l','e','c','t',' ','M','O','D','E',' ','O','P','E','R','.',',',
	 'a','n','d',' ','p','r','e','s','s',' ','O','K',' ',' ',' ',' ',' ',' ','\0'},
        {'S','w','i','t','c','h',' ','O','N',' ','S','c','a','l','e',',',' ',' ',
	 's','t','a','n','d',' ','o','n',',','p','r','e','s','s',' ','O','K',' ','\0'},
        {'P','u','t',' ','o','n',' ','t','-','s','h','i','r','t',',','s','e','t',
	 'S','E','W',' ','O','N',',','p','r','e','s','s',' ','O','K',' ',' ',' ','\0'},
        {'S','w','i','t','c','h',' ','O','F','F',' ','S','E','W',',',' ',' ',' ',
	 'p','r','e','s','s',' ','O','K',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'},
        {'A','d','j','u','s','t',' ','N','I','B','P',' ','c','u','f','f',',',' ',
	 's','w','i','t','c','h',' ','O','N',',','p','r','e','s','s',' ','O','K','\0'},
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0',
	 '\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'},
        {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0',
	 '\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'}
};

const uint8_t INSTANT_MESSAGES[INSTMSG_QTY][37] = {
	{'T','H','E',' ','D','A','T','A','L','O','G','G','E','R',' ','I','S',' ',
	 'B','E','I','N','G',' ','I','N','I','T','I','A','L','I','Z','E','D',' ','\0'}, // 0
	{'T','H','E',' ','D','A','T','A','L','O','G','G','E','R',' ','I','S',' ',
	 'R','E','A','D','Y',' ','T','O',' ','W','O','R','K',' ',' ',' ',' ',' ','\0'}, // 1
	{'B','A','T','T','E','R','Y',' ','L','E','V','E','L',' ','O','F',' ',' ',
	 'L','E','S','S',' ','T','H','A','N',' ','7','5','%',' ',' ',' ',' ',' ','\0'}, // 2
	{'B','A','T','T','E','R','Y',' ','L','E','V','E','L',' ','O','F',' ',' ',
	 'L','E','S','S',' ','T','H','A','N',' ','5','0','%',' ',' ',' ',' ',' ','\0'}, // 3
	{'B','A','T','T','E','R','Y',' ','L','E','V','E','L',' ','O','F',' ',' ',
	 'L','E','S','S',' ','T','H','A','N',' ','2','5','%',' ',' ',' ',' ',' ','\0'}, // 4
	{'T','H','E',' ','B','A','T','T','E','R','Y',' ','I','S',' ',' ',' ',' ',
	 'B','E','I','N','G',' ','C','H','A','R','G','E','D',' ',' ',' ',' ',' ','\0'}, // 5
        {'B','A','T','T','E','R','Y',' ','D','I','S','C','H','A','R','G','E','D',
	 'S','Y','S','T','E','M',' ','W','I','L','L',' ','G','O',' ','O','F','F','\0'}, // 6
         {'S','E','A','R','C','H','I','N','G',' ','F','O','R',' ',' ',' ',' ',' ',
	 'P','E','R','I','P','H','E','R','A','L','S','.','.','.',' ',' ',' ',' ','\0'}, // 7
};

const uint8_t nephronplus[2][12] = {
	{'N','E','P','H','R','O','N','P','L','U','S','\0'}, // 0
        {'n','e','p','h','r','o','n','p','l','u','s','\0'}, // 0
};
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
uint8_t vcc_source = 1;               // 1: VBAT_1, 2:VBAT2, 3:NONE
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
void DisplayOperatingState(void);
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  LCD_Set_BytePixels_V_BotToTop
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
short LCD_Set_BytePixels_V_BotToTop (uint8_t xlogoline, uint8_t xoff_x, short xnrows, uint8_t xoff_y, short xbigcols){
        if((xlogoline & 0x80)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x40)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x20)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x10)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x08)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x04)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x02)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
        if((xlogoline & 0x01)!=0) SetPixel(xoff_x+ xbigcols, xoff_y+xnrows, 1);
        xnrows--;
  return xnrows;
}

// -----------------------------------------------------------------------------------
//! \brief  lcd_LoadSymbol
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void lcd_LoadSymbol (uint8_t which_symbol, uint8_t sub_symbol, int8_t XSTART, int8_t YSTART){
  uint8_t off_x, off_y;
  unsigned short nrows, bigcols;
  uint8_t symbol_line1, symbol_line2;
  off_x=0;off_y=0; nrows=0, bigcols=0;
  switch(which_symbol){
    case SYMBOL_BATSM:    // battery SMALL
      off_x = BATSM_START_X;
      off_y = 0;
      for(bigcols=0;bigcols<BATSM_FONT_ROWS;bigcols++){
          nrows = BATSM_START_Y;
          symbol_line1= BATTERY_8x8[sub_symbol][0+(bigcols*BATSM_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
      }
    break;
    case SYMBOL_BATBG:
      off_x = BATBG_START_X;
      off_y = 0;
      for(bigcols=0;bigcols<BATBG_FONT_ROWS;bigcols++){
          nrows = BATBG_START_Y;
          symbol_line1= BATTERY_16x8[sub_symbol][0+(bigcols*BATBG_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
      }
    break;
    case SYMBOL_BTSM:   // bluetooth
      off_x = BT_SM_START_X;
      off_y = 0;
       for(bigcols=0;bigcols<BT_SM_FONT_ROWS;bigcols++){
          nrows = BT_SM_START_Y;
          symbol_line1= BT_8x12[1+(bigcols*BT_SM_CHARS_PER_COL)];
          symbol_line2= BT_8x12[0+(bigcols*BT_SM_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
    break;
    case SYMBOL_OK:     // ok button
      off_x = OK_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<OK_FONT_ROWS;bigcols++){
          nrows = OK_START_Y;
          symbol_line1= BUTTONS13x9[1+(bigcols*OK_CHARS_PER_COL)];
          symbol_line2= BUTTONS13x9[0+(bigcols*OK_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
    break;
    case SYMBOL_KEYS:   // KEYS UP, DOWN, LEFT, RIGHT
    off_x = XSTART;
      off_y = 0;
      for(bigcols=0;bigcols<KEYS_FONT_ROWS;bigcols++){
          nrows = YSTART;
          symbol_line1= KEYS_8x8[sub_symbol][0+(bigcols*KEYS_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
      }
    break;	
    case SYMBOL_USB20A:	// USB SYMBOL
      off_x = USB20A_START_X;
      off_y = 0;
      for(bigcols=0; bigcols<USB20A_FONT_ROWS;bigcols++){
      	  nrows = USB20A_START_Y;
      	  symbol_line1 = USB20A16x8[bigcols*USB20A_CHARS_PER_COL];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
      }
    break;
    case SYMBOL_DIALYSIS:     // DIALYSIS button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= Dialysis13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= Dialysis13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
     break;
     case SYMBOL_NO_DIALYSATE:     // NO DIALYSATE button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= NoDialysate13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= NoDialysate13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
     break;
     case SYMBOL_ALL_STOPPED:     // NO DIALYSATE button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= AllStopped13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= AllStopped13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
      break;
      case SYMBOL_REGEN_1:     // NO DIALYSATE button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= Regen1_13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= Regen1_13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
      break;
      case SYMBOL_ULTRAFILTRATION:     // NO DIALYSATE button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= Ultrafiltration13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= Ultrafiltration13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
      break;
      case SYMBOL_REGEN_2:     // NO DIALYSATE button
      off_x = INDICATION_START_X+XSTART;
      off_y = YSTART;
       for(bigcols=0;bigcols<INDICATION_FONT_ROWS;bigcols++){
          nrows = INDICATION_START_Y;
          symbol_line1= Regen2_13x9[1+(bigcols*INDICATION_CHARS_PER_COL)];
          symbol_line2= Regen2_13x9[0+(bigcols*INDICATION_CHARS_PER_COL)];
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
          nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
       }
      break;
  }
}

// -----------------------------------------------------------------------------------
//! \brief  lcd_LoadNephronPlus
//!         ...
//!
//! ...
//!
//! \param[in,out]  nothing
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_LoadNephronPlus (){
    uint8_t off_x = NEPHRONPLUS_START_X, off_y = 0;
    unsigned short nrows = 0, bigcols = 0;
    uint8_t symbol_line1, symbol_line2;
    for(bigcols=0; bigcols<NEPHRONPLUS_FONT_ROWS;bigcols++){
        nrows = NEPHRONPLUS_START_Y;
        symbol_line1 = nephronplus17x15[1+(bigcols*NEPHRONPLUS_CHARS_PER_COL)];
        symbol_line2 = nephronplus17x15[0+(bigcols*NEPHRONPLUS_CHARS_PER_COL)];
        nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line1, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (symbol_line2, off_x, nrows, off_y, bigcols);
    }
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_NEPHRONPLUS
//!         PRESENTATION SYMBOL+TEXT NEPHRONPLUS + OK
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_NEPHRONPLUS(){
  lcd_ClearLocalFrame();
  lcd_DrawRect(0, 0, 132, 32);
  lcd_LoadNephronPlus ();
  DrawArialRStringW(30,13, nephronplus[1], 128, 1);	
  lcd_LoadSymbol (SYMBOL_OK,0,4,-3);
  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_Display_CSEM_CHAR
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_Display_CSEM_CHAR (short lines, uint8_t off_x){
    uint8_t logoline1,logoline2,logoline3,logoline4;
    short nrows = START_LOGO_Y;
    uint8_t off_y = 0;
    short bigcols=0;
    for(bigcols=0;bigcols<FONT_ROWS;bigcols++){
      	nrows = START_LOGO_Y;
     	logoline1=csem_logo_ARMTB2629_LE[lines][2+(bigcols*CHARS_PER_COL)];
        logoline2=csem_logo_ARMTB2629_LE[lines][1+(bigcols*CHARS_PER_COL)];
        logoline3=csem_logo_ARMTB2629_LE[lines][0+(bigcols*CHARS_PER_COL)];
        logoline4=csem_logo_ARMTB2629_LE[lines][3+(bigcols*CHARS_PER_COL)];
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline1, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline2, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline3, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline4, off_x, nrows, off_y, bigcols);
    }
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_PRESENTATION_EV
//!         EVENT DEMO: PRESENTATION "CSEM"
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_PRESENTATION_EV(){
  short lines=0;
  uint8_t off_x = START_LOGO_X;
  lcd_ClearLocalFrame();
  lcd_DrawRect(0, 0, 132, 32);
  for(lines=0;lines<ARMTB2629_LINES;lines++){
    LCD_Display_CSEM_CHAR (lines, off_x);
    if(lines==1)  off_x += CHAR_DISTANCE-2;
    else          off_x += CHAR_DISTANCE;
  }
   lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_Display_BIGNUMBERS
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_Display_BIGNUMBERS (short lines, uint8_t off_x){
    uint8_t logoline1,logoline2,logoline3,logoline4;
    short nrows = START_AR20_Y;
    uint8_t off_y = 0;
    short bigcols=0;
    for(bigcols=0;bigcols<ARIALR20_FONT_ROWS;bigcols++){
      	nrows = START_AR20_Y;
     	logoline1=ArialRegular20_Numbers[lines][2+(bigcols*ARIALR20_CHARS_PER_COL)];
        logoline2=ArialRegular20_Numbers[lines][1+(bigcols*ARIALR20_CHARS_PER_COL)];
        logoline3=ArialRegular20_Numbers[lines][0+(bigcols*ARIALR20_CHARS_PER_COL)];
        logoline4=ArialRegular20_Numbers[lines][3+(bigcols*ARIALR20_CHARS_PER_COL)];
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline1, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline2, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline3, off_x, nrows, off_y, bigcols);
        nrows = LCD_Set_BytePixels_V_BotToTop (logoline4, off_x, nrows, off_y, bigcols);
    }
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_P_DATA_CODE_EV
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_P_DATA_CODE_EV (short counter){
  uint8_t counter_str[8];	
  uint8_t idx=0;
  uint8_t off_x = START_AR20_X;
  lcd_ClearLocalFrame();

  lcd_DrawRect(0, 0, 132, 32);
  // nro --> index to buffer strs
  for(idx=0;idx<8;idx++) counter_str[idx]=0;
  decword_to_indexstr (counter, counter_str,6,10000);
  for (idx=0; idx<6; idx++){
    LCD_Display_BIGNUMBERS (counter_str[idx], off_x);
    off_x += ARIALR20_CHAR_DISTANCE;
  }
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,4,28);
  lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_MAIN_MENU
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_MAIN_MENU(uint8_t which_item){
  uint8_t offx = 18;
  uint8_t offy = 1;
  uint8_t deltay = 8;
  
  lcd_ClearLocalFrame();
  lcd_DrawRect(126, 0, 6, 32);
  lcd_LoadSymbol (SYMBOL_OK,0,0,-1);
  DisplayOperatingState();
  

  lcd_FillRect(127, 0+8*(which_item-1), 4, 8);
  lcd_FillRect(0, 0+8*(which_item-1), 125, 8);

  switch (which_item){
    case 1:
      DrawArialRString(offx, offy, P_DATA_TEXT, 128,0);
      DrawArialRString(offx, offy+deltay,STATUS_TEXT, 128,1);	
      DrawArialRString(offx, offy+(deltay*2),CONFIG_TEXT, 128,1);	
      break;
    case 2:
      DrawArialRString(offx, offy,P_DATA_TEXT, 128,1);	
      DrawArialRString(offx, offy+deltay,STATUS_TEXT, 128,0);	
      DrawArialRString(offx, offy+(deltay*2),CONFIG_TEXT, 128,1);	
    break;
    case 3:
      DrawArialRString(offx, offy,P_DATA_TEXT, 128,1);	
      DrawArialRString(offx, offy+deltay,STATUS_TEXT, 128,1);	
      DrawArialRString(offx, offy+(deltay*2),CONFIG_TEXT, 128,0);	
    break;
  }
  lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_P_DATA_SUBMENU
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_P_DATA_SUBMENU(uint8_t page, uint8_t max_index){
  uint8_t offx = 12;
  uint8_t offx2 = 62;
  uint8_t offy = 1;
  uint8_t deltay = 8;
  uint8_t yindex = 0;
  uint8_t idx = 0;
  uint8_t strdata[10];
  uint8_t dmax = 10;
  uint8_t unit_index = 0;

  uint8_t square_heigth = 5;
  //short
  if(page>=P_DATA_QTY) return;
  
  // 17.04.2012: here I will also update patient data... 
  LCD_UPDATE_VARIABLES();
  
  
  lcd_ClearLocalFrame();
  lcd_DrawRect(126, 0, 6, 32);

  lcd_LoadSymbol (SYMBOL_OK,0,0,-1);
  DisplayOperatingState();
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,0,32);

  for(idx=0;idx<4;idx++){
  	if((page+yindex+idx)>(max_index-1))break;
  	DrawArialRString(offx, offy+(deltay*idx),P_DATA_TITLE[page+yindex+idx], 128,1);
  }
  
  // add values and symbols...
  switch (page){
        // page 1
  	case 0:	// firstname, lastname, m/f, age
            // firstname
            DrawArialRString(offx2, offy+(deltay*0),UIF_P_DATA_INITIALS, 128,1);	
            // lastname
            DrawArialRString(offx2, offy+(deltay*1),UIF_P_DATA_PATIENTCODE, 128,1);	
            // M/F
            DrawArialRString(offx2, offy+(deltay*2),UIF_P_DATA_GENDER, 128,1);
            // age 
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            decword_to_charstr(UIF_P_DATA_AGE, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*3),strdata, 128,1);
            // small square
            lcd_FillRect(127, (square_heigth*0)+1 , 4, square_heigth);
            break;

            // page 2
        case (1*STATUS_ITEMS):	// weight, hr, br, activity
            // weight
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_WEIGHT, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            strdata[unit_index-1] = 'k';
            strdata[unit_index] = 'g';
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // hr
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_HEARTRATE, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // br
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_BREATHINGRATE, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*2),strdata, 128,1);	
            // activity
            DrawArialRString(offx2, offy+(deltay*3),P_DATA_ACTIVITY[UIF_P_DATA_ACTIVITYCODE], 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*1)+1 , 4, square_heigth);
            break;
        case (2*STATUS_ITEMS):	// na1, k1, ph1, ur1
            // na1
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_NA_ECP1, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // k1
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_K_ECP1, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // ph1
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_PH_ECP1, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*2),strdata, 128,1);	
            // urea1
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_UREA_ECP1, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*3),strdata, 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*2)+1 , 4, square_heigth);
            break;
        // page 4
        case (3*STATUS_ITEMS):	// na2, k2, ph2, urea2
            // na2
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_NA_ECP2, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // k2
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_K_ECP2, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // ph2
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_PH_ECP2, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*2),strdata, 128,1);	
            // urea2
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_UREA_ECP2, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];                      
            DrawArialRString(offx2, offy+(deltay*3),strdata, 128,1);	            
            // small square
            lcd_FillRect(127, (square_heigth*3)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 5
            break;
        case (4*STATUS_ITEMS):	// sistolic pressure, diastolic pressure
            // sistolic pressure
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_SISTOLICBP, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // diastolic pressure
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_P_DATA_DIASTOLICBP, strdata, 4, 100);
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            if(strdata[0] == ' ') for(idx=0;idx<(dmax-1);idx++)strdata[idx]=strdata[idx+1];           
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            break;
  }
  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_STATUS_SUBMENU
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_STATUS_SUBMENU(uint8_t page, uint8_t max_index){
  uint8_t offx = 12;
  uint8_t offx2 = 72;
  uint8_t offx3 = 72;
  uint8_t offy = 1;
  uint8_t deltay = 8;
  uint8_t yindex = 0;
  uint8_t idx = 0;
  uint8_t strdata[10];
  uint8_t dmax = 10;
  uint8_t unit_index = 0;
  uint8_t square_heigth = 5;
  uint8_t wakd_attitude_code = 0;
  uint8_t wakd_operatingmode_code = 0;
  uint8_t mb_commlinks_code = 0;
  uint8_t wakd_conductivitystatus_code = 0;
  uint8_t wakd_mfblcircuit_code = 0;
  uint8_t wakd_mfflcircuit_code = 0;
  uint8_t wakd_hfdstatus_code = 0;
  uint8_t wakd_sustatus_code = 0;
  uint8_t wakd_polarstatus_code = 0;
  uint8_t wakd_ecpsstatus_code = 0;  
  uint8_t wakd_polarvoltage_sign = 0;
  uint16_t wakd_polarvoltage_abs = 0;

  if(page>=STATUS_QTY) return;
  
  LCD_UPDATE_VARIABLES();
  
  lcd_ClearLocalFrame();
  lcd_DrawRect(126, 0, 6, 32);

  lcd_LoadSymbol (SYMBOL_OK,0,0,-1);
  DisplayOperatingState();
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,0,32);

  for(idx=0;idx<4;idx++){
  	if((page+yindex+idx)>(max_index-1))break;
  	DrawArialRString(offx, offy+(deltay*idx),STATUS_TITLE[page+yindex+idx], 128,1);
  }

  // add values and symbols...
  switch (page){
        // page 1
  	case 0:	// bpack1, bpack2, wakd ready, wakd attitude
            // bpack1_percent
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_BPACK_1_Percent, strdata, 4, 100); // max 100 [0..2] 3 = '\0'
            strdata[unit_index-1] = '%';
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // bpack2_percent
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_BPACK_2_Percent, strdata, 4, 100); // max 100 [0..2] 3 = '\0'
            strdata[unit_index-1] = '%';
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // WAKD Ready TO WORK (AFTER START-UP)           
            if(WAKD_FLAG_READY_TO_WORK == 1) DrawArialRString(offx2, offy+(deltay*2),YES_NO[0], 128,1);
            else DrawArialRString(offx2, offy+(deltay*2),YES_NO[1], 128,1);
            // WAKD Attitude 
            wakd_attitude_code = Calculate_WAKDAttitude_Code (UIF_WAKD_Attitude);
            DrawArialRString(offx2, offy+(deltay*3),ATTITUDE_STATUS[wakd_attitude_code], 128,1);
            // small square
            lcd_FillRect(127, (square_heigth*0)+1 , 4, square_heigth);
            break;
        // page 2
        case (1*STATUS_ITEMS):	// comm. link, operating mode, blood circuit, dialysate circuit
            // communication link
            mb_commlinks_code = Calculate_MBCLINKS_Code (UIF_MB_CLINKS_Status);
            DrawArialRString(offx3, offy+(deltay*0),COMM_LINKS[mb_commlinks_code], 128,1);	
            // operating modes
            wakd_operatingmode_code = Calculate_WAKDOperatingMode_Code (UIF_WAKD_OperatingMode);
            DrawArialRString(offx3, offy+(deltay*1),OPERATING_MODES[wakd_operatingmode_code], 128,1);	
            // blood circuit
            wakd_mfblcircuit_code = Calculate_MFCircuit_Code (UIF_WAKD_BLCircuit_Status);
            DrawArialRString(offx3, offy+(deltay*2),MF_CIRCUIT[wakd_mfblcircuit_code], 128,1);	
            // dialysate circuit
            wakd_mfflcircuit_code = Calculate_MFCircuit_Code (UIF_WAKD_FLCircuit_Status);
            DrawArialRString(offx3, offy+(deltay*3),MF_CIRCUIT[wakd_mfflcircuit_code], 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*1)+1 , 4, square_heigth);
            break;
        // page 3
        case (2*STATUS_ITEMS):	// blood pump speed %, blood temp out, blood temp inp, blood circ. pressure
            // blood pump speed %
            if(UIF_BLPump_SpeedCode>25)UIF_BLPump_SpeedCode=25;
            DrawArialRString(offx3, offy+(deltay*0),PUMP_SPEEDCODE[UIF_BLPump_SpeedCode], 128,1);	
            // blood outgoing temperature
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_BTS_OutletTemperature, strdata, 4, 100); 
            strdata[unit_index-1] = 'C';
            DrawArialRString(offx3, offy+(deltay*1),strdata, 128,1);	
            // blood incoming temperature
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_BTS_InletTemperature, strdata, 4, 100); 
            strdata[unit_index-1] = 'C';
            DrawArialRString(offx3, offy+(deltay*2),strdata, 128,1);	
            // blood circuit pressure
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_BPS_Pressure, strdata, 4, 100); 
            strdata[unit_index-1] = 'm';    // mbar
            strdata[unit_index] = 'b';  // mbar
            DrawArialRString(offx3, offy+(deltay*3),strdata, 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*2)+1 , 4, square_heigth);
            break;
        // page 4
        case (3*STATUS_ITEMS):	// filtrate pump speed %, fps, hfd % OK, hfd status, 
            // filtrate pump speed %
            if(UIF_FLPump_SpeedCode>25)UIF_FLPump_SpeedCode=25;
            DrawArialRString(offx3, offy+(deltay*0),PUMP_SPEEDCODE[UIF_FLPump_SpeedCode], 128,1);	
            // dialysate circ. pressure 0-500mbar
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_FPS_Pressure, strdata, 4, 100); 
            strdata[unit_index-1] = 'm';    // mbar
            strdata[unit_index] = 'b';  // mbar
            DrawArialRString(offx3, offy+(deltay*1),strdata, 128,1);	
            // HFD %OK
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(MB_WAKD_HFD_PercentOK, strdata, 4, 100); // max 100 [0..2] 3 = '\0'
            strdata[unit_index-1] = '%';
            DrawArialRString(offx3, offy+(deltay*2),strdata, 128,1);	
            // hfd status
            wakd_hfdstatus_code = Calculate_HFDstatus_Code (UIF_WAKD_HFD_Status);
            DrawArialRString(offx3, offy+(deltay*3),HFD_STATUS[wakd_hfdstatus_code], 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*3)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 5

            break;
  	// page 5
        case (4*STATUS_ITEMS):	// dialysate circ. pressure, conductivity, su status, 
            // conductivity sensor status
            wakd_conductivitystatus_code = Calculate_WAKDCSensorStatus_Code (UIF_DCS_Status);
            DrawArialRString(offx3, offy+(deltay*0),CONDUCTIVITY_STATUS[wakd_conductivitystatus_code], 128,1);	
            // Conductance [S(siemens)]
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_DCS_Conductance, strdata, 4, 100); 
            strdata[unit_index-1] = 'S';
            DrawArialRString(offx3, offy+(deltay*1),strdata, 128,1);	
            // Susceptance [S(siemens)]
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            unit_index = decword_to_charstr(UIF_DCS_Susceptance, strdata, 4, 100); 
            strdata[unit_index-1] = 'S';
            DrawArialRString(offx3, offy+(deltay*2),strdata, 128,1);	
            // sorbent unit status
            wakd_sustatus_code = Calculate_SUstatus_Code (UIF_WAKD_SU_Status);
            DrawArialRString(offx3, offy+(deltay*3),SU_STATUS[wakd_sustatus_code], 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*4)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 5
            break;
        // page 6
	case (5*STATUS_ITEMS):	
            // polarizer status, polarizer val, ecp 1, ecp 2
            wakd_polarstatus_code = Calculate_POLARstatus_Code (UIF_WAKD_POLAR_Status);
            DrawArialRString(offx3, offy+(deltay*0),POLAR_STATUS[wakd_polarstatus_code], 128,1);	
            // polarizer voltage
            for(idx=0;idx<dmax;idx++)strdata[idx]=0;
            wakd_polarvoltage_abs = UIF_WAKD_POLAR_Voltage & 0x7FFF;
            if((UIF_WAKD_POLAR_Voltage & 0x8000)!=0) wakd_polarvoltage_sign=1;
            unit_index = decword_to_charstr(wakd_polarvoltage_abs, strdata, 5, 1000); // max 1000 [0..2] 3 = '\0'
            strdata[5] = 'V';
            strdata[4] = strdata[3];
            strdata[3] = strdata[2];
            strdata[2] = '.';
            strdata[1] = strdata[1];
            if(wakd_polarvoltage_abs<100)strdata[1]='0';
            if(wakd_polarvoltage_sign==1)strdata[0] = '-';
            else strdata[0] = '+';
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // ecp1 
            wakd_ecpsstatus_code = Calculate_ECPSstatus_Code (UIF_WAKD_ECP1_Status);
            DrawArialRString(offx3, offy+(deltay*2),ECP_STATUS[wakd_ecpsstatus_code], 128,1);	
            // ecp2
            wakd_ecpsstatus_code = Calculate_ECPSstatus_Code (UIF_WAKD_ECP2_Status);
            DrawArialRString(offx3, offy+(deltay*3),ECP_STATUS[wakd_ecpsstatus_code], 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*5)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 6
            break;
        case (6*STATUS_ITEMS):
            // error 1
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ERRORS_Status[0], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // error 2
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ERRORS_Status[1], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // error 3
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ERRORS_Status[2], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*2),strdata, 128,1);	
            // error 4
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ERRORS_Status[3], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*3),strdata, 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*5)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 6
        break;
        case (7*STATUS_ITEMS):
            // alarm 1
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ALARMS_Status[0], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*0),strdata, 128,1);	
            // alarm 2
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ALARMS_Status[1], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*1),strdata, 128,1);	
            // alarm 3
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ALARMS_Status[2], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*2),strdata, 128,1);	
            // alarm 4
            for(idx=0;idx<dmax;idx++)strdata[idx]=' ';
            unit_index = decword_to_charstr(UIF_WAKD_ALARMS_Status[3], strdata, 4, 100); // max 255 
            DrawArialRString(offx2, offy+(deltay*3),strdata, 128,1);	
            // small square
            lcd_FillRect(127, (square_heigth*5)+1 , 4, square_heigth);   // estimated length for STATUS PAGES = 6
            break;
  }	
  lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_COMMAND_SUBMENU
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_COMMAND_SUBMENU(uint8_t index, uint8_t which_item, uint8_t max_index){
  uint8_t offx = 22;
  uint8_t offy = 1;
  uint8_t deltay = 8;
  lcd_ClearLocalFrame();
  lcd_DrawRect(126, 0, 6, 32);
  lcd_LoadSymbol (SYMBOL_OK,0,0,-1);
  DisplayOperatingState();
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,0,32);

  lcd_FillRect(127, 0+8*(which_item-1), 4, 8);
  lcd_FillRect(0, 0+8*(which_item-1), 125, 8);

  switch (which_item){    
    case 1:
      DrawArialRString(offx, offy, COMMAND_TITLE[index], 128,0);	
      DrawArialRString(offx, offy+deltay,COMMAND_TITLE[index+1], 128,1);	
      DrawArialRString(offx, offy+(deltay*2),COMMAND_TITLE[index+2], 128,1);	
    break;
  case 2:
      DrawArialRString(offx, offy, COMMAND_TITLE[index], 128,1);	
      DrawArialRString(offx, offy+deltay,COMMAND_TITLE[index+1], 128,0);	
      DrawArialRString(offx, offy+(deltay*2),COMMAND_TITLE[index+2], 128,1);	
    break;
  case 3:
      DrawArialRString(offx, offy, COMMAND_TITLE[index], 128,1);	
      DrawArialRString(offx, offy+deltay,COMMAND_TITLE[index+1], 128,1);	
      DrawArialRString(offx, offy+(deltay*2),COMMAND_TITLE[index+2], 128,0);	
    break;
  }
  lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_COMMAND_LIST
//!         Displays the meaning of the command
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_COMMAND_LIST (uint8_t index){

  //uint8_t offx = 4;
  uint8_t offy = 4;
  uint8_t deltay = 8;
  uint8_t info[MAX_ARIALR];
  uint8_t idx = 0;
  if(index>=COMMAND_QTY) return;
  lcd_ClearLocalFrame();
  lcd_DrawRect(2,0, 128,22);

  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = COMMAND_INFO[index][idx];
  info[MAX_ARIALR-1] = '\0';
  //DrawArialRString(offx, offy, info, 128,1);
  lcd_DrawStringH(offy, info);
  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = COMMAND_INFO[index][idx+MAX_ARIALR-1];
  info[MAX_ARIALR-1] = '\0';
  //DrawArialRString(offx, offy+deltay,info, 128,1);
  lcd_DrawStringH(offy+deltay, info);
  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = COMMAND_INFO[index][idx+2*(MAX_ARIALR-1)];
  info[MAX_ARIALR-1] = '\0';
//  DrawArialRString(offx, offy+(deltay*2),info, 128,1);	
  lcd_DrawStringH(offy+(deltay*2), info);

  lcd_LoadSymbol (SYMBOL_OK,0,5,-1);
  DisplayOperatingState();
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,2,31);
  //DrawArialRString(12,25, RETURN_TEXT, 128, 1);	
  //DrawArialRString(82,25, SAVE_TEXT, 128, 1);	

  lcd_SendDisplayFrame();
}

void LCD_DISPLAY_MODE_SUBMENU(uint8_t index, uint8_t which_item, uint8_t max_index){
  uint8_t offx = 22;
  uint8_t offy = 1;
  uint8_t deltay = 8;
  lcd_ClearLocalFrame();
  lcd_DrawRect(126, 0, 6, 32);
  lcd_LoadSymbol (SYMBOL_OK,0,0,-1);
  DisplayOperatingState();
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,0,32);

  lcd_FillRect(127, 0+8*(which_item-1), 4, 8);
  lcd_FillRect(0, 0+8*(which_item-1), 125, 8);

  switch (which_item){    
    case 1:
      DrawArialRString(offx, offy, MODE_TITLE[index], 128,0);	
      DrawArialRString(offx, offy+deltay, MODE_TITLE[index+1], 128,1);	
      DrawArialRString(offx, offy+(deltay*2), MODE_TITLE[index+2], 128,1);	
    break;
  case 2:
      DrawArialRString(offx, offy, MODE_TITLE[index], 128,1);	
      DrawArialRString(offx, offy+deltay, MODE_TITLE[index+1], 128,0);	
      DrawArialRString(offx, offy+(deltay*2), MODE_TITLE[index+2], 128,1);	
    break;
  case 3:
      DrawArialRString(offx, offy, MODE_TITLE[index], 128,1);	
      DrawArialRString(offx, offy+deltay, MODE_TITLE[index+1], 128,1);	
      DrawArialRString(offx, offy+(deltay*2), MODE_TITLE[index+2], 128,0);	
    break;
  }
  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_COMMAND_ACCEPTED
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_COMMAND_ACCEPTED (uint8_t index){

  uint8_t offx = 40;
  uint8_t offy = 8;
  uint8_t deltay = 8;

  lcd_ClearLocalFrame();
  lcd_DrawRoundedRect(36, 5, DISP_WIDTH-70, 21);
  DrawArialRString(offx, offy, CMD_ACCEPTED[0], 128,1);	
  DrawArialRString(offx, offy+deltay,CMD_ACCEPTED[1], 128,1);
  
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,4,31);
  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_COMMAND_ACCEPTED
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_COMMAND_REJECTED (uint8_t index){

  uint8_t offx = 40;
  uint8_t offy = 8;
  uint8_t deltay = 8;

  lcd_ClearLocalFrame();
  lcd_DrawRoundedRect(36, 5, DISP_WIDTH-70, 21);
  DrawArialRString(offx, offy, CMD_REJECTED[0], 128,1);	
  DrawArialRString(offx, offy+deltay,CMD_REJECTED[1], 128,1);
  
  lcd_LoadSymbol (SYMBOL_KEYS,SYMBOL_KEYS_LEFT,4,31);
  lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_Show_KEY
//!         ...
//!
//! Shows in text the key pressed
//!
//! \param[in]      Arg_1 : key_pressed
//! \return         none
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_Show_KEY(uint8_t key_pressed){
  const uint8_t key_0[20] = {'K','E','Y',':','0','-','R','E','S','E','R','V','E','D',' ',' ',' ',' ',' ','\0'};
  const uint8_t key_1[20] = {'K','E','Y',':','1','-','O','K',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  const uint8_t key_2[20] = {'K','E','Y',':','2','-','T','O','P',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  const uint8_t key_3[20] = {'K','E','Y',':','3','-','B','O','T','T','O','M',' ',' ',' ',' ',' ',' ',' ','\0'};
  const uint8_t key_4[20] = {'K','E','Y',':','4','-','R','I','G','H','T',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  const uint8_t key_5[20] = {'K','E','Y',':','5','-','L','E','F','T',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
  const uint8_t defmsg[20] = {'U','N','K','N','O','W','N',' ','I','N','T',' ','E','V','E','N','T',' ',' ','\0'};
  lcd_ClearLocalFrame();
  switch(key_pressed){
      case (KEYCODE_0_RESERVED):
          DrawArialRString(1, 13, key_0, 128,1);
      break;
      case (KEYCODE_1_OK):
          DrawArialRString(1, 13, key_1, 128,1);
      break;
      case (KEYCODE_2_TOP):
          DrawArialRString(1, 13, key_2, 128,1);	
      break;
      case (KEYCODE_3_BOTTOM):
          DrawArialRString(1, 13, key_3, 128,1);	
      break;
      case (KEYCODE_4_RIGHT):
          DrawArialRString(1, 13, key_4, 128,1);	
      break;
      case (KEYCODE_5_LEFT):
          DrawArialRString(1, 13, key_5, 128,1);	
      break;
      default:
          DrawArialRString(1, 13, defmsg, 128,1);	  	
      break;
  }
  lcd_SendDisplayFrame();	
}
// -----------------------------------------------------------------------------------
//! \brief  LCD_DISPLAY_INSTANT_MESSAGES
//!         ...
//!
//! Displays Instant Messages
//!
//! \param[in,out,in-out] nothing
//! \return               nothing
// -----------------------------------------------------------------------------------
void LCD_DISPLAY_INSTANT_MESSAGES (uint8_t index){

  uint8_t offx = 1;
  uint8_t offy = 9;
  uint8_t deltay = 8;
  uint8_t info[MAX_ARIALR];
  uint8_t idx = 0;
  if(index>=INSTMSG_QTY) return;
  
  lcd_ClearLocalFrame();

  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = INSTANT_MESSAGES[index][idx];
  info[MAX_ARIALR-1] = '\0';
  DrawArialRString(offx, offy, info, 128,1);	
  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = INSTANT_MESSAGES[index][idx+MAX_ARIALR-1];
  info[MAX_ARIALR-1] = '\0';
  DrawArialRString(offx, offy+deltay,info, 128,1);
  for(idx=0; idx<MAX_ARIALR-1;idx++)info[idx] = INSTANT_MESSAGES[index][idx+2*(MAX_ARIALR-1)];
  info[MAX_ARIALR-1] = '\0';
  DrawArialRString(offx, offy+(deltay*2),info, 128,1);	

  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  lcd_JackTest
//!         ...
//!
//! writes a Hello World message
//!
//! \param[in,out,in-out] nothing
//! \return               nothing
// -----------------------------------------------------------------------------------
void lcd_JackTest(){
    lcd_ClearLocalFrame();
    lcd_DrawStringH(8, "Check the LCD");
    lcd_DrawStringH(20, "CSEM");
    lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------------
//! \brief  lcd_DisplayOFF
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void lcd_DisplayOFF(){

    lcd_DisplayClear();
    DELAY_MS(5);
    
    main_menu_item_index = 0;
    main_menu_item_list_index = 0;
    p_data_submenu_page_index = 0;
    status_submenu_page_index = 0;
    command_submenu_item_index = 0;
    command_submenu_item_list_index  = 0;
    mode_submenu_item_index = 0;
    mode_submenu_item_list_index  = 0;
}
// -----------------------------------------------------------------------------------
//! \brief  DisplayMessage
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void DisplayMessage(uint8_t mode){

    if(mode==1){
        Display_Menu_Status = DISPLAY_STATUS_PRESENTATION;
        //next_message = SelectInstantMessage();
        //LCD_DISPLAY_INSTANT_MESSAGES (next_message);
        // 07.10.2010
        //display_timeout_counter = 0;
    }
    else{
        Display_Menu_Status = Display_Initialize_MainMenu();
    }

    main_menu_item_index = 0;
    main_menu_item_list_index = 0;
    p_data_submenu_page_index = 0;
    status_submenu_page_index = 0;
    command_submenu_item_index = 0;
    command_submenu_item_list_index  = 0;
    mode_submenu_item_index = 0;
    mode_submenu_item_list_index  = 0;
}

// -----------------------------------------------------------------------------
/// \brief DisplayAlarm
/// Functionality:
/// - Writes on the LCD screen the alarm messages
/// - Disables all the keys except from the OK button
/// - Saves the previous state
/// - Waits for a predefined mount of time for the user to acknowledge the 
///   message
/// - 
// -----------------------------------------------------------------------------
void DisplayAlarm(uint32_t alarmID) {
  uint8_t offx2 = 8;
  uint8_t offy = 8;
  uint8_t strdata[10] = {' ', ' ', ' ', ' ',' ', ' ',' ', ' ',' ', '\0'};
  char alarm_string[9] = "ALARM   ";
  
  lcd_ClearLocalFrame();
  DELAY_MS(10);
  lcd_LoadSymbol(SYMBOL_OK, 0, 0, -1);
  alarm_string[6] = (char) (alarmID / 10 + 48);
  alarm_string[7] = (char) (alarmID - ((alarmID/10) * 10) + 48);
  lcd_DrawStringH(0, (uint8_t const*)alarm_string);
  DrawArialRString(offx2, offy, strdata, 128, 1);
  if (alarmID == 26 || alarmID == 29 || alarmID == 33 || alarmID == 35 ) { // dectreeMsg_detectedKAbsLorKAbsHandSorDys, dectreeMsg_detectedKAAHandKincrease, dectreeMsg_detectedUreaAAbsHandSorDys
    lcd_DrawStringH(8, "Please replace");
    lcd_DrawStringH(16, "sorbend cartridge");
  } else if (alarmID >= 12 && alarmID <= 17) { // for physical alarms dectreeMsg_detectedBPorFPhigh - dectreeMsg_detectedBTSiTL
    lcd_DrawStringH(8, "Pumps stopped");
    lcd_DrawStringH(16, "Contact physician");
  }
  else {
    lcd_DrawStringH(8, "Please contact");
    lcd_DrawStringH(16, "physician");
  }
  lcd_SendDisplayFrame();
}

// -----------------------------------------------------------------------------
/// \brief DispalyError
/// Functionality:
/// - Writes on the LCD screen the alarm messages
/// - Disables all the keys except from the OK button
/// - Saves the previous state
/// - Waits for a predefined mount of time for the user to acknowledge the 
///   message
/// - 
// -----------------------------------------------------------------------------
void DisplayError(uint32_t errorID) {
  uint8_t offx2 = 8;
  uint8_t offy = 8;
  uint8_t strdata[10] = {' ', ' ', ' ', ' ',' ', ' ',' ', ' ',' ', '\0'};
  char error_string[9] = "ERROR   ";
  
  lcd_ClearLocalFrame();
  DELAY_MS(10);
  lcd_LoadSymbol(SYMBOL_OK, 0, 0, -1);
  error_string[6] = (char) (errorID / 10 + 48);
  error_string[7] = (char) (errorID - ((errorID/10) * 10) + 48);
  lcd_DrawStringH(8, (uint8_t const*)error_string);
  DrawArialRString(offx2, offy, strdata, 128, 1);
  
  lcd_SendDisplayFrame();
}

void DisplayInitializing(void) {
  char string[16] = "Initializing...";
  
  lcd_ClearLocalFrame();
  DELAY_MS(10);
  lcd_LoadSymbol(SYMBOL_OK, 0, 0, -1);
  lcd_DrawStringH(8, (uint8_t const*)string);
  
  lcd_SendDisplayFrame();
  
  while (WAKD_FLAG_READY_TO_WORK != 1) {
    
    DELAY_MS(100);
  }
}

void DisplayOperatingState(void) {
  switch (operating_state) {
  case UIF_COMMAND_STOP_OPERATION:
    lcd_LoadSymbol(SYMBOL_ALL_STOPPED, 0, 0, -1);
    break;
  case UIF_COMMAND_GOTO_DIALYSIS:
    lcd_LoadSymbol(SYMBOL_DIALYSIS, 0, 0, -1);
    break;
  case UIF_COMMAND_GOTO_REGEN1:
    lcd_LoadSymbol(SYMBOL_REGEN_1, 0, 0, -1);    
    break;
  case UIF_COMMAND_GOTO_ULTRA:
    lcd_LoadSymbol(SYMBOL_ULTRAFILTRATION, 0, 0, -1);    
    break;
  case UIF_COMMAND_GOTO_REGEN2:
    lcd_LoadSymbol(SYMBOL_REGEN_2, 0, 0, -1);    
    break;
  case UIF_COMMAND_START_OPERATION:
    lcd_LoadSymbol(SYMBOL_NO_DIALYSATE, 0, 0, -1);    
    break;
  default:
    break;
  }
}

// -----------------------------------------------------------------------------------
// END LCDNEPHRONPLUS.C
// -----------------------------------------------------------------------------------