    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBAT1_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBAT1_INFO[w];
    for(w=MB_UIF_QTY_VBAT1_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBAT1_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBAT2_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBAT2_INFO[w];
    for(w=MB_UIF_QTY_VBAT2_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBAT2_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_VBATS_INFO;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_VBATS_INFO[w];
    for(w=MB_UIF_QTY_VBATS_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_VBATS_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_STATUS;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ATTITUDE;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ATTITUDE[w];
    for(w=MB_UIF_QTY_WAKD_ATTITUDE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ATTITUDE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_COMMLINK;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_COMMLINK[w];
    for(w=MB_UIF_QTY_WAKD_COMMLINK;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_COMMLINK){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }  
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_OPMODE;w++)            mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_OPMODE[w];
    for(w=MB_UIF_QTY_WAKD_OPMODE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_OPMODE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }

    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_MFCIRCUIT_STATUS[w];
    for(w=MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLPUMP_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLPUMP_INFO[w];
    for(w=MB_UIF_QTY_WAKD_BLPUMP_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLPUMP_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLPUMP_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLPUMP_INFO[w];
    for(w=MB_UIF_QTY_WAKD_FLPUMP_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLPUMP_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLTEMPERATURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLTEMPERATURE[w];
    for(w=MB_UIF_QTY_WAKD_BLTEMPERATURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLTEMPERATURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[w];
    for(w=MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[w];
    for(w=MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_FLCONDUCTIVITY;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_FLCONDUCTIVITY[w];
    for(w=MB_UIF_QTY_WAKD_FLCONDUCTIVITY;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_FLCONDUCTIVITY){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_HFD_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_HFD_INFO[w];
    for(w=MB_UIF_QTY_WAKD_HFD_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_HFD_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_SU_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_SU_INFO[w];
    for(w=MB_UIF_QTY_WAKD_SU_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_SU_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }   
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_POLAR_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_POLAR_INFO[w];
    for(w=MB_UIF_QTY_WAKD_POLAR_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_POLAR_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECP1_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECP1_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECP1_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECP1_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }    
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECP2_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECP2_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECP2_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECP2_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_ECPS_INFO;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_ECPS_INFO[w];
    for(w=MB_UIF_QTY_WAKD_ECPS_INFO;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_ECPS_INFO){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_FIRSTNAME;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_FIRSTNAME[w];
    for(w=MB_UIF_QTY_PDATA_FIRSTNAME;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_FIRSTNAME){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_LASTNAME;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_LASTNAME[w];
    for(w=MB_UIF_QTY_PDATA_LASTNAME;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_LASTNAME){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_GENDERAGE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_GENDERAGE[w];
    for(w=MB_UIF_QTY_PDATA_GENDERAGE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_GENDERAGE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_WEIGHT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_WEIGHT[w];
    for(w=MB_UIF_QTY_PDATA_WEIGHT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_WEIGHT){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_HEARTRATE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_HEARTRATE[w];
    for(w=MB_UIF_QTY_PDATA_HEARTRATE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_HEARTRATE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_BREATHRATE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_BREATHRATE[w];
    for(w=MB_UIF_QTY_PDATA_BREATHRATE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_BREATHRATE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ACTIVITY;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ACTIVITY[w];
    for(w=MB_UIF_QTY_PDATA_ACTIVITY;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ACTIVITY){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_SEWALL;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_SEWALL[w];
    for(w=MB_UIF_QTY_PDATA_SEWALL;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_SEWALL){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_BLPRESSURE;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_BLPRESSURE[w];
    for(w=MB_UIF_QTY_PDATA_BLPRESSURE;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_BLPRESSURE){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ECP1;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ECP1[w];
    for(w=MB_UIF_QTY_PDATA_ECP1;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ECP1){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_PDATA_ECP2;w++) mbuif_pbuffer[w] = MB_UIF_MSG_PDATA_ECP2[w];
    for(w=MB_UIF_QTY_PDATA_ECP2;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_PDATA_ECP2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_ALARMS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_ALARMS[w];
    for(w=MB_UIF_QTY_ALARMS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_ALARMS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_ERRORS;w++) mbuif_pbuffer[w] = MB_UIF_MSG_ERRORS[w];
    for(w=MB_UIF_QTY_ERRORS;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_ERRORS){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_TEST_LINK;w++) mbuif_pbuffer[w] = MB_UIF_MSG_TEST_LINK[w];
    for(w=MB_UIF_QTY_TEST_LINK;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_TEST_LINK+MB_UIF_QTY_ANSWER_2-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_SHUTDOWN;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_SHUTDOWN[w];
    for(w=MB_UIF_QTY_WAKD_SHUTDOWN;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_SHUTDOWN+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_START_OPERATION;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_START_OPERATION[w];
    for(w=MB_UIF_QTY_WAKD_START_OPERATION;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_START_OPERATION+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_WAKD_MODE_OPERATION;w++) mbuif_pbuffer[w] = MB_UIF_MSG_WAKD_MODE_OPERATION[w];
    for(w=MB_UIF_QTY_WAKD_MODE_OPERATION;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_WAKD_MODE_OPERATION+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SCALE_GET_WEIGHT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SCALE_GET_WEIGHT[w];
    for(w=MB_UIF_QTY_SCALE_GET_WEIGHT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SCALE_GET_WEIGHT+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SEW_START_STREAMING;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SEW_START_STREAMING[w];
    for(w=MB_UIF_QTY_SEW_START_STREAMING;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SEW_START_STREAMING+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_SEW_STOP_STREAMING;w++) mbuif_pbuffer[w] = MB_UIF_MSG_SEW_STOP_STREAMING[w];
    for(w=MB_UIF_QTY_SEW_STOP_STREAMING;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_SEW_STOP_STREAMING+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
    for(w=0;w<MB_UIF_QTY_NIBP_START_MEASUREMENT;w++) mbuif_pbuffer[w] = MB_UIF_MSG_NIBP_START_MEASUREMENT[w];
    for(w=MB_UIF_QTY_NIBP_START_MEASUREMENT;w<MB_UIF_MAXLEN;w++)mbuif_pbuffer[w] = 0;
    index = 0;
    // UCB0: SPI-SL RX INT (TEST)
    while(index<MB_UIF_QTY_NIBP_START_MEASUREMENT+MB_UIF_QTY_ANSWER_1-2){   
        // UCB0IFG  /* USCI B0 Interrupt Flags Register */
        // UCRXIFG  /* USCI Receive Interrupt Flag */
        UCB0IFG |= UCRXIFG;
        DELAY_MS(5);  // 500
    }
    // ----------------------------------
