// -----------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifmenuTest.h
//! \brief  Nephron+: UIF Processor
//!
//! Menu Test: Simulates the three behaviours
//!
//! \author  Gabriela Dudnik
//! \date    07.01.2012
//! \version 1.0 (gdu)
// -----------------------------------------------------------------------------------
#ifndef UIFMENUTEST_H
#define UIFMENUTEST_H

// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
extern void MB_UIF_CMD_TEST(void);
extern void UIF_MenuTest(void);
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// END UIFMENUTEST.H
// -----------------------------------------------------------------------------------
