// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   i2c.c
//! \brief  ...
//!
//! Declaration of useful macros, constants
//! Definition of user types
//!
//! \author  Porchet J-A
//! \date    09.02.2009
//! \version 1.0 First version (JAP)
// -----------------------------------------------------------------------------------
#include "i2c.h"
#include <msp430.h>
// -----------------------------------------------------------------------------------
//  Private definitions 
// -----------------------------------------------------------------------------------
unsigned char *PTxData;     // Pointer to TX data
unsigned char TXByteCtr;    // TX byte counter

unsigned char *PRxData;     // Pointer to RX data
unsigned char RXByteCtr;    // RX byte counter

// -----------------------------------------------------------------------------------
// Initialize the i2c port B1 used to communicate with the LED driver
//
// Inputs  : none
//
// Outputs : none
// -----------------------------------------------------------------------------------
void ucb1InitI2C(void)
{
    UCB1CTL1 |= UCSWRST;                    // Enable SW reset
    UCB1CTL0 = UCMST + UCMODE_3 + UCSYNC;   // I2C Master, synchronous mode
    UCB1CTL1 = UCSSEL_2 + UCSWRST;          // Use SMCLK, keep SW reset
    UCB1BR0 = 0xA0;                         // fSCL = SMCLK/80 = 100kHz
    UCB1BR1 = 0;

    UCB1I2CSA = SLAVEADDRESS;               // Slave Address

    UCB1CTL1 &= ~UCSWRST;                   // Clear SW reset, resume operation
    
    UCB1IE |= UCTXIE + UCRXIE;              // Enable TX and RX interrupts

    TXByteCtr = 0;
    RXByteCtr = 0;
}

// -----------------------------------------------------------------------------------
// Sends a byte to the LED driver
//
// Inputs : TxData = Pointer to the sending buffer
//          ByteNber = Number of bytes which will be sent
// 
// Outputs : none
// -----------------------------------------------------------------------------------
void ucb1I2CSendValue(uint8_t *TxData, uint8_t ByteNber)
{
    PTxData = TxData;                   // TX array start address
    TXByteCtr = ByteNber;               // Load TX byte counter
    while (UCB1CTL1 & UCTXSTP);         // Ensure stop condition got sent
    UCB1CTL1 |= UCTR + UCTXSTT;         // I2C TX, start condition
}

// -----------------------------------------------------------------------------------
// Reads a byte from the LED driver
//
// Inputs : *RxData = Pointer to the reading buffer
//          ByteNber = Number of bytes which will be sent
// 
// Outputs : none
// -----------------------------------------------------------------------------------
void ucb1I2CReadValue(uint8_t *RxData, uint8_t ByteNber)
{
    PRxData = RxData;                   // TX array start address
    RXByteCtr = ByteNber;               // Load TX byte counter
    
    while (UCB1CTL1 & UCTXSTP);         // Ensure stop condition got sent
    
    UCB1CTL1 &= ~UCTR;                  // I2C RX
    UCB1CTL1 |= UCTXSTT;                // I2C start condition

    while(UCB1CTL1 & UCTXSTT);          // Start condition sent?

    if(ByteNber == 1)        // Only one byte left?
        UCB1CTL1 |= UCTXSTP;    // Generate I2C stop condition
}


//------------------------------------------------------------------------------
// The USCIAB1TX_ISR is structured such that it can be used to transmit any
// number of bytes by pre-loading TXByteCtr with the byte count. Also, TXData
// points to the next byte to transmit.
//------------------------------------------------------------------------------
#pragma vector = USCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void)
{
    switch(__even_in_range(UCB1IV,12))
    {
        case  0: break;                 // Vector  0: No interrupts
        case  2: break;                 // Vector  2: ALIFG
        case  4: break;                 // Vector  4: NACKIFG
        case  6: break;                 // Vector  6: STTIFG
        case  8: break;                 // Vector  8: STPIFG
        case 10:                        // Vector 10: RXIFG
            RXByteCtr--;                // Decrement RX byte counter
            if(RXByteCtr)
            {
              *PRxData++ = UCB1RXBUF;   // Move RX data to address PRxData
              if(RXByteCtr == 1)        // Only one byte left?
                UCB1CTL1 |= UCTXSTP;    // Generate I2C stop condition
            }
            else
            {
              *PRxData = UCB1RXBUF;     // Move final RX data to PRxData
            }
            break;
        
        
        case 12:                        // Vector 12: TXIFG 
            if(TXByteCtr)               // Check TX byte counter
            {
                UCB1TXBUF = *PTxData++; // Load TX buffer
                TXByteCtr--;            // Decrement TX byte counter
            }
            else
            {
                UCB1CTL1 |= UCTXSTP;    // I2C stop condition
                UCB1IFG &= ~UCTXIFG;    // Clear USCI_B0 TX int flag
            }
            break;
        default: break;
    }
}
// -----------------------------------------------------------------------------------
// END I2C.C
// -----------------------------------------------------------------------------------
