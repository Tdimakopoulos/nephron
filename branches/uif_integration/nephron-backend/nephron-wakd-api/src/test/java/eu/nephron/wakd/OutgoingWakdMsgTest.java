package eu.nephron.wakd;

import org.apache.commons.codec.binary.Hex;
import org.testng.annotations.Test;

import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;
import eu.nephron.wakd.api.outgoing.ChangeStateMsg;
import eu.nephron.wakd.api.outgoing.ConfigureStateMsg;
import eu.nephron.wakd.api.outgoing.PeriodPolarizerToggleMsg;
import eu.nephron.wakd.api.outgoing.SetTimeMsg;
import eu.nephron.wakd.api.sensor.ActuatorCtrlData;
import eu.nephron.wakd.api.sensor.BloodPumpCtrlData;
import eu.nephron.wakd.api.sensor.FlowPumpCtrlData;
import eu.nephron.wakd.api.sensor.PolarizationCtrlData;
import eu.nephron.wakd.api.sensor.PolarizationCtrlData.PolarizationDirectionEnum;
import eu.nephron.wakd.api.sensor.PumpCtrlData.PumpDirectionEnum;


public class OutgoingWakdMsgTest {
	
	private static final int COMMAND_ID = 1;

	@Test
	public void testChangeStateMsg() {
		ChangeStateMsg msg = new ChangeStateMsg(
				COMMAND_ID, 
				WakdStateEnum.NO_DIALYSATE, 
				WakdOpStateEnum.AUTOMATIC, 
				WakdStateEnum.ALL_STOPPED, 
				WakdOpStateEnum.AUTOMATIC);
		this.printMsg(msg);
	}
	
	@Test
	public void testConfigureStateMsg() {
		BloodPumpCtrlData bloodPumpCtrlData = new BloodPumpCtrlData(PumpDirectionEnum.PLUS, 255);
		FlowPumpCtrlData flowPumpCtrlData = new FlowPumpCtrlData(PumpDirectionEnum.MINUS, 251);
		PolarizationCtrlData polarizationCtrlData = new PolarizationCtrlData(PolarizationDirectionEnum.PLUS, 100);
		ActuatorCtrlData actuatorCtrlData = new ActuatorCtrlData(bloodPumpCtrlData, flowPumpCtrlData, polarizationCtrlData);
		ConfigureStateMsg msg = new ConfigureStateMsg(COMMAND_ID, WakdStateEnum.REGEN1, actuatorCtrlData);
		this.printMsg(msg);
	}
	
	@Test
	public void testPeriodPolarizerToggleMsg() {
		PeriodPolarizerToggleMsg msg = new PeriodPolarizerToggleMsg(COMMAND_ID, 1);
		this.printMsg(msg);
	}
	
	@Test
	public void testSetTimeMsg() {
		long sysTime = System.currentTimeMillis() / 1000L;
		SetTimeMsg msg = new SetTimeMsg(1, sysTime);
		this.printMsg(msg);
	}
	
	private void printMsg(OutgoingWakdMsg msg) {
		byte[] data = msg.encode();
		System.out.println(ByteUtils.toBinaryString(data));
		System.out.println(Hex.encodeHex(data));
	}
	
}
