package eu.nephron.wakd.api.enums;

public enum WakdOpStateEnum {
	UNDEFINED((byte)-1),
	UNDEFINED_INITIALIZING((byte)0),
	AUTOMATIC((byte)1),
	SEMI_AUTOMATIC((byte)2);
	
	public static WakdOpStateEnum getWakdOpStateEnum(byte value) {
		switch (value) {
		case (byte)0:
			return UNDEFINED_INITIALIZING;
		case (byte)1:
			return AUTOMATIC;
		case (byte)2:
			return SEMI_AUTOMATIC;
		default:
			return UNDEFINED;
		}
	}
	
	private byte identifier;

	private WakdOpStateEnum(byte identifier) {
		this.identifier = identifier;
	}

	public byte getIdentifier() {
		return identifier;
	}

	public void setIdentifier(byte identifier) {
		this.identifier = identifier;
	}
}
