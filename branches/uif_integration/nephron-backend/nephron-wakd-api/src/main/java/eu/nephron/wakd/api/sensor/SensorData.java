package eu.nephron.wakd.api.sensor;

import java.io.Serializable;

public abstract class SensorData implements Serializable {

	private static final long serialVersionUID = 2342000568774230222L;
	
	public abstract byte[] encode();
	
}
