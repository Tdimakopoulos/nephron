package eu.nephron.wakd.api.enums;

public enum WakdAlertEnum {
	
	// Generic commands/data relevant for all Who.
	UNDEFINED((byte)-1),			
	decTreeMsg_detectedBPsysAbsL((byte)0),
	decTreeMsg_detectedBPsysAbsH((byte)1),
	decTreeMsg_detectedBPsysAAbsH((byte)2),
	decTreeMsg_detectedBPdiaAbsL((byte)3),
	decTreeMsg_detectedBPdiaAbsH((byte)4),
	decTreeMsg_detectedBPdiaAAbsH((byte)5),
	
	// weight
	dectreeMsg_detectedWghtAbsL((byte)6),
	dectreeMsg_detectedWghtAbsH((byte)7),
	dectreeMsg_detectedWghtTrdT((byte)8),
	
	//actuator data
	dectreeMsg_detectedpumpBaccDevi((byte)9),
	dectreeMsg_detectedpumpFaccDevi((byte)10),
	
	// physical
	dectreeMsg_detectedBPorFPhigh((byte)11),
	dectreeMsg_detectedBPorFPlow((byte)12),
	dectreeMsg_detectedBTSoTH((byte)13),
	dectreeMsg_detectedBTSoTL((byte)14),
	dectreeMsg_detectedBTSiTH((byte)15),
	dectreeMsg_detectedBTSiTL((byte)16),
	
	// physiological
	dectreeMsg_detectedPhAAbsL((byte)17),
	dectreeMsg_detectedPhAAbsH((byte)18),
	dectreeMsg_detectedPhAbsLorPhAbsH((byte)19),
	dectreeMsg_detectedPhTrdLPsT((byte)20),
	dectreeMsg_detectedPhTrdHPsT((byte)21),
	dectreeMsg_detectedNaAbsLorNaAbsH((byte)22),
	dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT((byte)23),
	dectreeMsg_detectedKAbsLorKAbsHandSorDys((byte)24),
	dectreeMsg_detectedKAbsLorKAbsHnoSorDys((byte)25),
	dectreeMsg_detectedKAAL((byte)26),
	dectreeMsg_detectedKAAHandKincrease((byte)27),
	dectreeMsg_detectedKAAHnoKincrease((byte)28),
	dectreeMsg_detectedKTrdLPsT((byte)29),
	dectreeMsg_detectedKTrdHPsT((byte)30),
	dectreeMsg_detectedKTrdofnriandSorDys((byte)31),
	dectreeMsg_detectedPhTrdofnriandSorDys((byte)32),
	dectreeMsg_detectedUreaAAbsHandSorDys((byte)33),
	dectreeMsg_detectedUreaAAbsHnoSorDys((byte)34),
	dectreeMsg_detectedUreaTrdHPsT((byte)35);
	
	public static WakdAlertEnum getWakdAlertEnum(byte value) {
		switch (value) {
		case (byte)0:
			return decTreeMsg_detectedBPsysAbsL;
		case (byte)1:
			return decTreeMsg_detectedBPsysAbsH;
		case (byte)2:
			return decTreeMsg_detectedBPsysAAbsH;
		case (byte)3:
			return decTreeMsg_detectedBPdiaAbsL;
		case (byte)4:
			return decTreeMsg_detectedBPdiaAbsH;
		case (byte)5:
			return decTreeMsg_detectedBPdiaAAbsH;
		case (byte)6:
			return dectreeMsg_detectedWghtAbsL;
		case (byte)7:
			return dectreeMsg_detectedWghtAbsH;
		case (byte)8:
			return dectreeMsg_detectedWghtTrdT;
		case (byte)9:
			return dectreeMsg_detectedpumpBaccDevi;
		case (byte)10:
			return dectreeMsg_detectedpumpFaccDevi;
		case (byte)11:
			return dectreeMsg_detectedBPorFPhigh;
		case (byte)12:
			return dectreeMsg_detectedBPorFPlow;
		case (byte)13:
			return dectreeMsg_detectedBTSoTH;
		case (byte)14:
			return dectreeMsg_detectedBTSoTL;
		case (byte)15:
			return dectreeMsg_detectedBTSiTH;
		case (byte)16:
			return dectreeMsg_detectedBTSiTL;
		case (byte)17:
			return dectreeMsg_detectedPhAAbsL;
		case (byte)18:
			return dectreeMsg_detectedPhAAbsH;
		case (byte)19:
			return dectreeMsg_detectedPhAbsLorPhAbsH;
		case (byte)20:
			return dectreeMsg_detectedPhTrdLPsT;
		case (byte)21:
			return dectreeMsg_detectedPhTrdHPsT;
		case (byte)22:
			return dectreeMsg_detectedNaAbsLorNaAbsH;
		case (byte)23:
			return dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT;
		case (byte)24:
			return dectreeMsg_detectedKAbsLorKAbsHandSorDys;
		case (byte)25:
			return dectreeMsg_detectedKAbsLorKAbsHnoSorDys;
		case (byte)26:
			return dectreeMsg_detectedKAAL;
		case (byte)27:
			return dectreeMsg_detectedKAAHandKincrease;
		case (byte)28:
			return dectreeMsg_detectedKAAHnoKincrease;
		case (byte)29:
			return dectreeMsg_detectedKTrdLPsT;
		case (byte)30:
			return dectreeMsg_detectedKTrdHPsT;
		case (byte)31:
			return dectreeMsg_detectedKTrdofnriandSorDys;
		case (byte)32:
			return dectreeMsg_detectedPhTrdofnriandSorDys;
		case (byte)33:
			return dectreeMsg_detectedUreaAAbsHandSorDys;
		case (byte)34:
			return dectreeMsg_detectedUreaAAbsHnoSorDys;
		case (byte)35:
			return dectreeMsg_detectedUreaTrdHPsT;
		default:
			return UNDEFINED;
		}
	}
	
	private byte value;

	private WakdAlertEnum(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
	
}
