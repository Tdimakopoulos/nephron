package eu.nephron.wakd.api.outgoing;

import java.util.Arrays;

import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;


public class ChangeStateMsg extends OutgoingWakdMsg {

	private static final long serialVersionUID = -8522286386866209503L;

	private WakdStateEnum from;
	
	private WakdOpStateEnum opFrom;
	
	private WakdStateEnum to;
	
	private WakdOpStateEnum opTo;
	
	public ChangeStateMsg(int id, WakdStateEnum from, WakdOpStateEnum opFrom, WakdStateEnum to, WakdOpStateEnum opTo) {
		super();
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.CHANGE_STATE_FROM_TO);
		this.header.setSize(10);
		this.from = from;
		this.opFrom = opFrom;
		this.to = to;
		this.opTo = opTo;
	}
	
	public WakdStateEnum getFrom() {
		return from;
	}

	public void setFrom(WakdStateEnum from) {
		this.from = from;
	}

	public WakdOpStateEnum getOpFrom() {
		return opFrom;
	}

	public void setOpFrom(WakdOpStateEnum opFrom) {
		this.opFrom = opFrom;
	}

	public WakdStateEnum getTo() {
		return to;
	}

	public void setTo(WakdStateEnum to) {
		this.to = to;
	}

	public WakdOpStateEnum getOpTo() {
		return opTo;
	}

	public void setOpTo(WakdOpStateEnum opTo) {
		this.opTo = opTo;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+4);
		bytes[6] = from.getIdentifier();
		bytes[7] = opFrom.getIdentifier();
		bytes[8] = to.getIdentifier();
		bytes[9] = opTo.getIdentifier();
		return bytes;
	}
	
}
