package eu.nephron.wakd.api.enums;

public enum WakdStateEnum {
	UNDEFINED((byte)-1),
	UNDEFINED_INITIALIZING((byte)0), 	// Internal state after PowerOn, initialize and proceed directly to AllStopped
	ALL_STOPPED((byte)1),
	MAINTENANCE((byte)2),
	NO_DIALYSATE((byte)3),				// formerly known as "BloodFlowOn"
	DIALYSIS((byte)4),
	REGEN1((byte)5),
	ULTRAFILTRATION((byte)6),
	REGEN2((byte)7);
	
	public static WakdStateEnum getWakdStateEnum(byte value) {
		switch (value) {
		case (byte)0:
			return UNDEFINED_INITIALIZING;
		case (byte)1:
			return ALL_STOPPED;
		case (byte)2:
			return MAINTENANCE;
		case (byte)3:
			return NO_DIALYSATE;
		case (byte)4:
			return DIALYSIS;
		case (byte)5:
			return REGEN1;
		case (byte)6:
			return ULTRAFILTRATION;
		case (byte)7:
			return REGEN2;
		default:
			return UNDEFINED;
		}
	}
	
	private byte identifier;

	private WakdStateEnum(byte identifier) {
		this.identifier = identifier;
	}

	public byte getIdentifier() {
		return identifier;
	}

	public void setIdentifier(byte identifier) {
		this.identifier = identifier;
	}

}
