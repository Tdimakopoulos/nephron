package eu.nephron.wakd.api.outgoing;

import java.util.Arrays;

import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;
import eu.nephron.wakd.api.sensor.ActuatorCtrlData;


public class ConfigureStateMsg extends OutgoingWakdMsg {

	private static final long serialVersionUID = -5676900691602215814L;

	private WakdStateEnum stateToConfigure;
	
	private ActuatorCtrlData ctrlData;
	
	public ConfigureStateMsg(int id, WakdStateEnum stateToConfigure, ActuatorCtrlData ctrlData) {
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.CONFIGURE_STATE);
		this.header.setSize(16);
		this.stateToConfigure = stateToConfigure;
		this.ctrlData = ctrlData;
	}

	public WakdStateEnum getStateToConfigure() {
		return stateToConfigure;
	}

	public void setStateToConfigure(WakdStateEnum stateToConfigure) {
		this.stateToConfigure = stateToConfigure;
	}

	public ActuatorCtrlData getCtrlData() {
		return ctrlData;
	}

	public void setCtrlData(ActuatorCtrlData ctrlData) {
		this.ctrlData = ctrlData;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+10);
		bytes[6] = stateToConfigure.getIdentifier();
		System.arraycopy(this.ctrlData.encode(), 0, bytes, 7, 9);
		return bytes;
	}
	
}
