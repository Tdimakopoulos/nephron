package eu.nephron.wakd.api.sensor;

import eu.nephron.utils.ByteUtils;


public class PolarizationCtrlData extends SensorCtrlData {

	private static final long serialVersionUID = 8709894520265296144L;
	
	private PolarizationDirectionEnum direction;
	
	private int voltageReference;
	
	public PolarizationCtrlData(PolarizationDirectionEnum direction, int voltageReference) {
		this.direction = direction;
		this.voltageReference = voltageReference;
	}

	public PolarizationDirectionEnum getDirection() {
		return direction;
	}

	public void setDirection(PolarizationDirectionEnum direction) {
		this.direction = direction;
	}

	public int getVoltageReference() {
		return voltageReference;
	}

	public void setVoltageReference(int voltageReference) {
		this.voltageReference = voltageReference;
	}
	
	@Override
	public byte[] encode() {
		byte[] bytes = new byte[3];
		bytes[0] = direction.getValue();
		System.arraycopy(ByteUtils.intToByteArray(this.voltageReference, 2), 0, bytes, 1, 2);
		return bytes; 
	}

	public static enum PolarizationDirectionEnum {
		PLUS((byte)1),
		MINUS((byte)2);
		
		private byte value;

		private PolarizationDirectionEnum(byte value) {
			this.value = value;
		}

		public byte getValue() {
			return value;
		}

		public void setValue(byte value) {
			this.value = value;
		}
	}
}
