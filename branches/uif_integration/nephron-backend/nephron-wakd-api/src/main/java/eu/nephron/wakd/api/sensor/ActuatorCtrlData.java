package eu.nephron.wakd.api.sensor;


public class ActuatorCtrlData extends SensorData {

	private static final long serialVersionUID = -5968106621129007884L;

	private BloodPumpCtrlData bloodPumpCtrlData;
	
	private FlowPumpCtrlData flowPumpCtrlData;
	
	private PolarizationCtrlData polarizationCtrlData;

	public ActuatorCtrlData(
			BloodPumpCtrlData bloodPumpCtrlData,
			FlowPumpCtrlData flowPumpCtrlData,
			PolarizationCtrlData polarizationCtrlData) {

		this.bloodPumpCtrlData = bloodPumpCtrlData;
		this.flowPumpCtrlData = flowPumpCtrlData;
		this.polarizationCtrlData = polarizationCtrlData;
	}

	public BloodPumpCtrlData getBloodPumpCtrlData() {
		return bloodPumpCtrlData;
	}

	public void setBloodPumpCtrlData(BloodPumpCtrlData bloodPumpCtrlData) {
		this.bloodPumpCtrlData = bloodPumpCtrlData;
	}

	public FlowPumpCtrlData getFlowPumpCtrlData() {
		return flowPumpCtrlData;
	}

	public void setFlowPumpCtrlData(FlowPumpCtrlData flowPumpCtrlData) {
		this.flowPumpCtrlData = flowPumpCtrlData;
	}

	public PolarizationCtrlData getPolarizationCtrlData() {
		return polarizationCtrlData;
	}

	public void setPolarizationCtrlData(PolarizationCtrlData polarizationCtrlData) {
		this.polarizationCtrlData = polarizationCtrlData;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = new byte[9];
		System.arraycopy(bloodPumpCtrlData.encode(), 0, bytes, 0, 3);
		System.arraycopy(flowPumpCtrlData.encode(), 0, bytes, 3, 3);
		System.arraycopy(polarizationCtrlData.encode(), 0, bytes, 6, 3);
		return bytes;
	}
	
}
