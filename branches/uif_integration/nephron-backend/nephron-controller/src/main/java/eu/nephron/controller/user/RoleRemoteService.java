package eu.nephron.controller.user;

import java.util.List;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;
import eu.nephron.model.role.Role;

@Remote
public interface RoleRemoteService extends ControllerService {

	public List<Role> findAllRoles();
	
	public Role findRoleByCode(String code);
	
	public Role findRoleByName(String name);
	
	public void createPatient(Long userID);
	
	public void createDoctor(Long userID);
	
	public void createWakd(Long userID);
	
	public void createSmartPhone(Long userID);
	
}
