package eu.nephron.controller.persistence;

import javax.ejb.Local;

@Local
public interface CrudService<T> extends CrudServiceRemote<T> {
    
}
