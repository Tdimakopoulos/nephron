package eu.nephron.controller.act.wakd;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.WakdState;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;

public class CurrentStateMsgHandler extends IncomingWakdMsgHandler {

	@Override
	protected boolean canHandle(IncomingWakdMsg msg) {
		return CurrentStateMsg.class.isInstance(msg);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean doHandle(IncomingWakdMsg msg, Device device) {
		CurrentStateMsg stateMsg = (CurrentStateMsg)msg;
		WakdState currentState = new WakdState();
		currentState.setDate(new Date());
		currentState.setDevice(device);
		currentState.setState(new Integer(stateMsg.getCurrentState().getIdentifier()));
		currentState.setOpState(new Integer(stateMsg.getCurrentOpState().getIdentifier()));
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceID", device.getId());
		WakdState parentState = (WakdState)this.getCrudService().findSingleWithNamedQuery("WakdState.findWakdStateByMaxDate", params);
		currentState.setParent(parentState);
		this.getCrudService().create(currentState);
		return true;
	}

}
