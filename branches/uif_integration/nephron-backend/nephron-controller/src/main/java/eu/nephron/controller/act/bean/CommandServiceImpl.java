package eu.nephron.controller.act.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.nephron.controller.ActionAuditInterceptor;
import eu.nephron.controller.act.CommandRemoteService;
import eu.nephron.controller.act.CommandService;
import eu.nephron.controller.act.wakd.IncomingWakdMsgHandler;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.model.act.Act;
import eu.nephron.model.act.ActRelationship;
import eu.nephron.model.act.ActStatusEnum;
import eu.nephron.model.act.CommandAct;
import eu.nephron.model.act.MoodCodeEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.OutgoingWakdMsg;

@SuppressWarnings("unchecked")
@Stateless
public class CommandServiceImpl implements CommandService, CommandRemoteService {

	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public ActRelationship readCommand(Long deviceID, Long userID) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceID", deviceID);
		ActRelationship actRelationship = 
				(ActRelationship)crudService.findSingleWithNamedQuery("ActRelationship.findCommandBySmartphoneID", params);
		if (null != actRelationship) {
			Act inboundAct = actRelationship.getInboundAct();
			inboundAct.setActStatus(ActStatusEnum.SUCCESSFUL);
			crudService.update(inboundAct);
		}
		return actRelationship;
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createCommand(OutgoingWakdMsg message, Long doctorID, Long smartPhoneID, Long userID) {
		
		Person doctor = (Person)crudService.find(Person.class, doctorID);
		Device smartPhone = (Device)this.crudService.find(Device.class, smartPhoneID);
		
		CommandAct request = new CommandAct();
		request.setWakdMsg(message.encode());
		request.setMoodCode(MoodCodeEnum.REQUEST);
		request.setActStatus(ActStatusEnum.PENDING);
		crudService.create(request);
		
		Participation doctorRP = new Participation();
		doctorRP.setMedicalEntity(doctor);
		doctorRP.setAct(request);
		crudService.create(doctorRP);
		
		Participation smartPhoneRP = new Participation();
		smartPhoneRP.setMedicalEntity(smartPhone);
		smartPhoneRP.setAct(request);
		crudService.create(smartPhoneRP);
		
		CommandAct event = new CommandAct();
		event.setMoodCode(MoodCodeEnum.EVENT);
		event.setActStatus(ActStatusEnum.PENDING);
		crudService.create(event);
		
		Participation smartPhoneEP = new Participation();
		smartPhoneEP.setMedicalEntity(smartPhone);
		smartPhoneEP.setAct(event);
		crudService.create(smartPhoneEP);
		
		Participation wakdEP = new Participation();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("deviceID", smartPhoneID);
		Device wakd = (Device)crudService.findSingleWithNamedQuery("Device.findAssociatedDevice", params);
		wakdEP.setMedicalEntity(wakd);
		wakdEP.setAct(event);
		crudService.create(wakdEP);
		
		ActRelationship relationShip = new ActRelationship();
		relationShip.setInboundAct(request);
		relationShip.setOutboundAct(event);
		crudService.create(relationShip);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void updateCommand(IncomingWakdMsg msg, Long commandID, Integer statusID, Long userID) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("commandID", commandID);
		Device device = 
				(Device)crudService.findSingleWithNamedQuery("Device.findWakdByCommandID", params);
		IncomingWakdMsgHandler.handleIncomingWakdMsg(msg, device);
		
		CommandAct act = (CommandAct)crudService.find(CommandAct.class, commandID);
		act.setWakdMsg(msg.encode());
		act.setActStatus(ActStatusEnum.getActStatusEnum(statusID));
		crudService.update(act);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void updateCommandStatus(Long commandID, Integer statusID, Long userID) {
		
		CommandAct act = (CommandAct)crudService.find(CommandAct.class, commandID);
		act.setActStatus(ActStatusEnum.getActStatusEnum(statusID));
		crudService.update(act);
	}
	
}
