package eu.nephron.controller.act.wakd;

import javax.naming.NamingException;

import eu.nephron.controller.ServiceLocator;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.model.entity.Device;
import eu.nephron.wakd.api.IncomingWakdMsg;

public abstract class IncomingWakdMsgHandler {
	
	private static CurrentStateMsgHandler stateMsgHandler = new CurrentStateMsgHandler();
	
	public static boolean handleIncomingWakdMsg(IncomingWakdMsg msg, Device device) {
		return stateMsgHandler.handle(msg, device);
	}

	protected IncomingWakdMsgHandler successor;
	
	public IncomingWakdMsgHandler getSuccessor() {
		return successor;
	}

	public void setSuccessor(IncomingWakdMsgHandler successor) {
		this.successor = successor;
	}
	
	public boolean handle(IncomingWakdMsg msg, Device device) {
		if (canHandle(msg)) {
			return this.doHandle(msg, device);
		} else {
			return null != successor ? successor.handle(msg, device) : false;
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected CrudService getCrudService() {
		try {
			return ServiceLocator.getResource("nephron/CrudServiceImpl/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

	protected abstract boolean canHandle(IncomingWakdMsg msg);
	
	protected abstract boolean doHandle(IncomingWakdMsg msg, Device device);
}
