package eu.nephron.controller;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import eu.nephron.controller.user.UserService;
import eu.nephron.model.audit.ActionAudit;
import eu.nephron.model.user.NephronUser;

public class ActionAuditInterceptor {

	@AroundInvoke
	public Object intercept(InvocationContext ctx) throws Exception {
		Object[] params = ctx.getParameters();
		Long userID = (Long) params[params.length-1];
		if (null != userID) {
			UserService userService = ServiceLocator.getResource("nephron/UserServiceImpl/local");
			NephronUser user = userService.findUserByUserID(userID);
			if (null == user) {
				throw new RuntimeException("User with ID: " + userID + " not found.");
			}
			ActionAudit.storeUser(Thread.currentThread(), user);
		}
		return ctx.proceed();
	}
	
}
