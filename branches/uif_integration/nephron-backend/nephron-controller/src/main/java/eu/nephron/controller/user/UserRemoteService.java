package eu.nephron.controller.user;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;
import eu.nephron.model.user.NephronUser;

@Remote
public interface UserRemoteService extends ControllerService {

	public NephronUser findUserByUserID(Long userID);
	
	public NephronUser findUserByUserName(String userName);
	
	public NephronUser findUserByUserNameAndPassword(String userName, String password);
	
	public void createUser(NephronUser user, Long userID);
	
}
