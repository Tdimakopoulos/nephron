package eu.nephron.controller.act.bean;

import java.util.HashSet;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.nephron.controller.ActionAuditInterceptor;
import eu.nephron.controller.act.ActRemoteService;
import eu.nephron.controller.act.ActService;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.model.act.ActStatusEnum;
import eu.nephron.model.act.AssociationAct;
import eu.nephron.model.act.MoodCodeEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.entity.MedicalEntity;

@SuppressWarnings("unchecked")
@Stateless
public class ActServiceImpl implements ActService, ActRemoteService {

	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createAssociationAct(List<Long> medicalEntityIDs, Long userID) {
		AssociationAct act = new AssociationAct();
		act.setParticipations(new HashSet<Participation>());
		act.setActStatus(ActStatusEnum.SUCCESSFUL);
		act.setMoodCode(MoodCodeEnum.EVENT);
		crudService.create(act);
		for (Long medicalEntityID : medicalEntityIDs) {
			MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
			Participation participation = new Participation();
			participation.setAct(act);
			participation.setMedicalEntity(entity);
			crudService.create(participation);
		}
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void updateAssociationAct(Long actID, List<Long> medicalEntityIDs, Long userID) {
		AssociationAct act = (AssociationAct) crudService.find(AssociationAct.class, actID);
		for (Long medicalEntityID : medicalEntityIDs) {
			MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
			Participation participation = new Participation();
			participation.setAct(act);
			participation.setMedicalEntity(entity);
			crudService.create(participation);
		}

	}
}
