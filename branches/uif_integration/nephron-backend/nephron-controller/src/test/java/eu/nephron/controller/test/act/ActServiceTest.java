package eu.nephron.controller.test.act;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;

public class ActServiceTest extends ControllerServiceTest {
	
	@Test(groups="createDB")
	public void testCreateAssociations() {
		
		Person patient = medicalEntityService.findPatientByLastName("Mein");
		Person doctor = medicalEntityService.findDoctorByLastName("Mein");
		Device smartphone = medicalEntityService.findSmartPhoneBySerialNumber("SP1");
		Device wakd = medicalEntityService.findWakdBySerialNumber("XYWZ");
		
		List<Long> l1 = new ArrayList<Long>();
		l1.add(patient.getId());
		l1.add(doctor.getId());
		actService.createAssociationAct(l1, this.userID);
		
		List<Long> l2 = new ArrayList<Long>();
		l2.add(patient.getId());
		l2.add(smartphone.getId());
		l2.add(wakd.getId());
		actService.createAssociationAct(l2, this.userID);
	}
	
}
