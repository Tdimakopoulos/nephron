package eu.nephron.controller.test;

import org.testng.annotations.BeforeClass;

import eu.nephron.controller.act.ActRemoteService;
import eu.nephron.controller.act.CommandRemoteService;
import eu.nephron.controller.address.AddressRemoteService;
import eu.nephron.controller.entity.MedicalEntityRemoteService;
import eu.nephron.controller.user.RoleRemoteService;
import eu.nephron.controller.user.UserRemoteService;
import eu.nephron.model.user.NephronUser;
import eu.nephron.test.ResourceLocator;

public class ControllerServiceTest {

	protected static final String USER_NAME = "ctri";
	protected Long userID;
	
	protected ActRemoteService actService;
	protected UserRemoteService userService;
	protected RoleRemoteService roleService;
	protected AddressRemoteService addressService;
	protected CommandRemoteService commandService;
	protected MedicalEntityRemoteService medicalEntityService;
	
	@BeforeClass
	public void beforeMethod() {
		actService = ResourceLocator.lookup("nephron/ActServiceImpl/remote");
		userService = ResourceLocator.lookup("nephron/UserServiceImpl/remote");
		roleService = ResourceLocator.lookup("nephron/RoleServiceImpl/remote");
		addressService = ResourceLocator.lookup("nephron/AddressServiceImpl/remote");
		commandService = ResourceLocator.lookup("nephron/CommandServiceImpl/remote");
		medicalEntityService = ResourceLocator.lookup("nephron/MedicalEntityServiceImpl/remote");
		
		NephronUser user = userService.findUserByUserName(USER_NAME);
		if (null != user) {
			userID = user.getId();
		}
	}
}
