package eu.nephron.controller.test.user;

import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;
import eu.nephron.model.user.NephronUser;

public class UserServiceTest extends ControllerServiceTest {
	
	@Test(groups="createDB")
	public void testCreateUsers() {
		NephronUser ctri = new NephronUser();
		ctri.setUserName("ctri");
		ctri.setPassword("ctri12");
		ctri.setFullName("Chris Trichias");
		userService.createUser(ctri, null);
		
		NephronUser gleo = new NephronUser();
		gleo.setUserName("gleo");
		gleo.setPassword("gleo12");
		gleo.setFullName("George Leoleis");
		userService.createUser(gleo, null);
		
		NephronUser ckot = new NephronUser();
		ckot.setUserName("kotso");
		ckot.setPassword("kotso12");
		ckot.setFullName("Costas Kotsokalis");
		userService.createUser(ckot, null);
	}
	
}
