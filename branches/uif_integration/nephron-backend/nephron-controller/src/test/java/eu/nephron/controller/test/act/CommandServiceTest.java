package eu.nephron.controller.test.act;

import org.apache.commons.codec.binary.Hex;
import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;
import eu.nephron.model.act.ActRelationship;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;
import eu.nephron.model.entity.WakdState;
import eu.nephron.wakd.api.enums.WakdAlertEnum;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;
import eu.nephron.wakd.api.incoming.AlertMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import eu.nephron.wakd.api.outgoing.ChangeStateMsg;

public class CommandServiceTest extends ControllerServiceTest {
	
	private static final int COMMAND_ID = 1;
	
	@Test(groups="createDB")
	public void testCreateState() throws InterruptedException {
		Device device = medicalEntityService.findSmartPhoneBySerialNumber("SP1");
		medicalEntityService.createState(device.getId(), WakdStateEnum.DIALYSIS, WakdOpStateEnum.AUTOMATIC, this.userID);
		Thread.sleep(1000);
	}
	
	@Test(groups="createDB")
	public void testCreateShutdownCommand() {
		
		Device device = medicalEntityService.findSmartPhoneBySerialNumber("SP1");
		Person person = medicalEntityService.findDoctorByLastName("Mein");
		WakdState currentState = medicalEntityService.findCurrentState(device.getId(), this.userID);
		
		ChangeStateMsg msg = new ChangeStateMsg(
				COMMAND_ID,
				WakdStateEnum.getWakdStateEnum((byte)currentState.getState().intValue()),
				WakdOpStateEnum.getWakdOpStateEnum((byte)currentState.getOpState().intValue()),
				WakdStateEnum.ALL_STOPPED,
				WakdOpStateEnum.AUTOMATIC);

		commandService.createCommand(msg, person.getId(), device.getId(), this.userID);
		System.out.println(Hex.encodeHexString(msg.encode()));
	}
	
	@Test(groups="createDB")
	public void testUpdateCommand() {
		
		Device device = medicalEntityService.findSmartPhoneBySerialNumber("SP1");
		ActRelationship actRelationship = commandService.readCommand(device.getId(), this.userID);
		CurrentStateMsg message = new CurrentStateMsg(COMMAND_ID, WakdStateEnum.NO_DIALYSATE, WakdOpStateEnum.AUTOMATIC);
		commandService.updateCommand(message, actRelationship.getOutboundAct().getId(), 0, this.userID);
	}
	
	@Test(groups="createDB")
	public void testCreateAlert() {
		AlertMsg msg = new AlertMsg(1, WakdAlertEnum.decTreeMsg_detectedBPdiaAAbsH);
		System.out.println(Hex.encodeHexString(msg.encode()));
	}
	
}
