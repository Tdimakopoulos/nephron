package eu.nephron.controller.test.entity;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;
import eu.nephron.model.address.Address;
import eu.nephron.model.entity.ContactInfo;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.EducationalLevelEnum;
import eu.nephron.model.entity.GenderEnum;
import eu.nephron.model.entity.Person;
import eu.nephron.model.entity.PersonalInfo;

public class MedicalEntityServiceTest extends ControllerServiceTest {
	
	@Test(groups="createDB")
	public void testCreatePatient() {
		Person person = new Person();
		person.setBirthDate(DateUtils.addYears(new Date(), -25));
		person.setGender(GenderEnum.HERMAPHRODITE);
		
		PersonalInfo personalInfo = new PersonalInfo();
		personalInfo.setTitle("Mr");
		personalInfo.setFirstName("Klein");
		personalInfo.setLastName("Mein");
		personalInfo.setFatherName("Christmas");
		personalInfo.setMiddleName("NY");
		personalInfo.setEducationalLevel(EducationalLevelEnum.ELEMENTARY);
		person.setPersonalInfo(personalInfo);
		
		Address address = new Address();
		address.setCountry(addressService.findCountryByCode("GR"));
		address.setCity("Athens");
		address.setStreet("Kritis");
		address.setStreetNo("15");
		address.setPostCode("15121");
		person.setAddress(address);
		
		ContactInfo contactInfo =  new ContactInfo();
		contactInfo.setPhone("+302106520578");
		contactInfo.setMobile("+302106974320804");
		contactInfo.setEmail("test1@email.com");
		person.setContactInfo(contactInfo);
		
		medicalEntityService.createPatient(person, this.userID);
	}
	
	@Test(groups="createDB")
	public void testCreateDoctor() {
		Person person = new Person();
		person.setBirthDate(DateUtils.addYears(new Date(), -50));
		person.setGender(GenderEnum.HERMAPHRODITE);
		
		PersonalInfo personalInfo = new PersonalInfo();
		personalInfo.setTitle("Mrs");
		personalInfo.setFirstName("Klein");
		personalInfo.setLastName("Mein");
		personalInfo.setFatherName("Doctor");
		personalInfo.setMiddleName("NY");
		personalInfo.setEducationalLevel(EducationalLevelEnum.PHD);
		person.setPersonalInfo(personalInfo);
		
		Address address = new Address();
		address.setCountry(addressService.findCountryByCode("GR"));
		address.setCity("Athens");
		address.setStreet("Vyronos");
		address.setStreetNo("5");
		address.setPostCode("15122");
		person.setAddress(address);
		
		ContactInfo contactInfo =  new ContactInfo();
		contactInfo.setPhone("+302109220578");
		contactInfo.setMobile("+302106979320804");
		contactInfo.setEmail("test2@email.com");
		person.setContactInfo(contactInfo);
		
		medicalEntityService.createDoctor(person, this.userID);
	}
	
	@Test(groups="createDB")
	public void testCreateWAKD() {
		Device wakd = new Device();
		wakd.setSerialNumber("XYWZ");
		wakd.setManufacturedDate(new Date());
		wakd.setExpirationDate(DateUtils.addYears(new Date(), 5));
		
		medicalEntityService.createWakd(wakd, this.userID);
	}
	
	@Test(groups="createDB")
	public void testCreateSmartphone() {
		Device smartphone = new Device();
		smartphone.setSerialNumber("SP1");
		smartphone.setManufacturedDate(new Date());
		smartphone.setExpirationDate(DateUtils.addYears(new Date(), 15));
		
		medicalEntityService.createSmartPhone(smartphone, this.userID);
	}
	
}
