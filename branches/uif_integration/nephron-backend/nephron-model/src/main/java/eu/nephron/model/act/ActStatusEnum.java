package eu.nephron.model.act;

import org.apache.commons.lang.math.NumberUtils;

public enum ActStatusEnum {
	UNDEFINED(0), PENDING(1), SUCCESSFUL(2), FAILED(3);
	
	public static ActStatusEnum getActStatusEnum(Integer actStatus) {
		switch (null != actStatus ? actStatus : NumberUtils.INTEGER_ZERO) {
		case 1:
			return PENDING;
		case 2:
			return SUCCESSFUL;
		case 3:
			return FAILED;
		default:
			return UNDEFINED;
		}
	}

	private Integer identifier;

	private ActStatusEnum(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
