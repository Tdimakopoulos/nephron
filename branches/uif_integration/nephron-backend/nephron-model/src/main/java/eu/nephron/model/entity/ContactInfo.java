package eu.nephron.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import eu.nephron.model.NephronEntity;

@Entity
@Table(name="CONTACT_INFO")
public class ContactInfo extends NephronEntity<Long> {

	private static final long serialVersionUID = 5269529322117841153L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="CONTACT_INFO_ID")
	protected Long id;
	
	@Column(name="PHONE", nullable=false)
	private String phone;	
	
	@Column(name="COMPANY_PHONE")
	private String companyPhone;
	
	@Column(name="MOBILE")
	private String mobile;
	
	@Column(name="COMPANY_MOBILE")
	private String companyMobile;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="COMPANY_EMAIL")
	private String companyEmail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCompanyMobile() {
		return companyMobile;
	}

	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	
}
