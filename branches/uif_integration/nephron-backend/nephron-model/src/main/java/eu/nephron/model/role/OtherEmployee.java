package eu.nephron.model.role;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class OtherEmployee extends Employee {

	private static final long serialVersionUID = -4669406842789704181L;
		
}
