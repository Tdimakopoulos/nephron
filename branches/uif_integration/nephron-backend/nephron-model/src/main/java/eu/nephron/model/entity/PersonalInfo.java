package eu.nephron.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

@Entity
@Table(name="PERSONAL_INFO")
public class PersonalInfo implements Serializable {
	
	private static final long serialVersionUID = -7906364657162554898L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PERSONAL_INFO_ID")
	protected Long id;

	@Column(name="TITLE")
	private String title;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="FATHER_NAME")
	private String fatherName;
	
	@Column(name="MARITAL_STATUS")
	@Type(
        type = "eu.nephron.model.GenericEnumUserType",
        parameters = {
                @Parameter(name = "enumClass", value = "eu.nephron.model.entity.MaritalStatusEnum"),
                @Parameter(name = "identifierMethod", value = "getIdentifier"),
                @Parameter(name = "valueOfMethod", value="getMaritalStatusEnum")})
	private MaritalStatusEnum maritalStatus;
	
	@Column(name="EDUCATIONAL_LEVEL")
	@Type(
        type = "eu.nephron.model.GenericEnumUserType",
        parameters = {
                @Parameter(name = "enumClass", value = "eu.nephron.model.entity.EducationalLevelEnum"),
                @Parameter(name = "identifierMethod", value = "getIdentifier"),
                @Parameter(name = "valueOfMethod", value="getEducationalLevelEnum")})
	private EducationalLevelEnum educationalLevel;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public MaritalStatusEnum getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatusEnum maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public EducationalLevelEnum getEducationalLevel() {
		return educationalLevel;
	}

	public void setEducationalLevel(EducationalLevelEnum educationalLevel) {
		this.educationalLevel = educationalLevel;
	}

}
