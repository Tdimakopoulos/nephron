package eu.nephron.model.act;

import org.apache.commons.lang.math.NumberUtils;

public enum MoodCodeEnum {
	UNDEFINED(0), DEFINITION(1), INTENT(2), REQUEST(3), EVENT(4), CRITERION(5), GOAL(6);
	
	public static MoodCodeEnum getMoodCodeEnum(Integer moodCode) {
		switch (null != moodCode ? moodCode : NumberUtils.INTEGER_ZERO) {
		case 1:
			return DEFINITION;
		case 2:
			return INTENT;
		case 3:
			return REQUEST;
		case 4:
			return EVENT;
		case 5:
			return CRITERION;
		case 6:
			return GOAL;
		default:
			return UNDEFINED;
		}
	}

	private Integer identifier;

	private MoodCodeEnum(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
