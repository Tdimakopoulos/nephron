package eu.nephron.model.act;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class MedicationAct extends Act {

	private static final long serialVersionUID = -9011841015683493140L;

	@Column(name="DOSAGE")
	private BigDecimal dosage;
	
	@Column(name="OUTCOME")
	private Integer outcome;

	public BigDecimal getDosage() {
		return dosage;
	}

	public void setDosage(BigDecimal dosage) {
		this.dosage = dosage;
	}

	public Integer getOutcome() {
		return outcome;
	}

	public void setOutcome(Integer outcome) {
		this.outcome = outcome;
	}
	
}
