package eu.nephron.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import eu.nephron.model.audit.ActionAudit;
import eu.nephron.model.user.NephronUser;

@MappedSuperclass
public abstract class NephronEntity<T> implements Identifiable<T> {
	
	private static final long serialVersionUID = 1817847475673418083L;

	@Column(name="DESCR", length=1023)
	protected String description;
	
	@Column(name="RECORD_STATUS")
	private Integer recordStatus;
	
	@Embedded
	@AssociationOverride(name="user", joinColumns=@JoinColumn(name="CREATED_BY"))
	@AttributeOverride(name="date", column=@Column(name="CREATION_DATE", nullable=false)) 
	private ActionAudit createAudit; 
	
	@Embedded
	@AssociationOverride(name="user", joinColumns=@JoinColumn(name="UPDATED_BY"))
	@AttributeOverride(name="date", column=@Column(name="UPDATE_DATE"))
	private ActionAudit updateAudit;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(Integer recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	@PrePersist
	public void prePersist() {
		if (null == this.createAudit) {
			this.createAudit = new ActionAudit();
			NephronUser user = ActionAudit.getUser(Thread.currentThread());
			if (null != user) {
				this.createAudit.setUser(user);
			}
		}
	}
	
	@PreUpdate
	public void preUpdate() {
		if (null == this.updateAudit) {
			this.updateAudit = new ActionAudit();
			NephronUser user = ActionAudit.getUser(Thread.currentThread());
			if (null != user) {
				this.updateAudit.setUser(user);
			}
		}
	}
	
}