package eu.nephron.model.entity;

import org.apache.commons.lang.math.NumberUtils;

public enum MaritalStatusEnum {
	UNDEFINED(0), SINGLE(1), MARRIED(2), DIVORCED(3);
	
	public static MaritalStatusEnum getMaritalStatusEnum(Integer maritalStatus) {
		switch (null != maritalStatus ? maritalStatus : NumberUtils.INTEGER_ZERO) {
		case 1:
			return SINGLE;
		case 2:
			return MARRIED;
		case 3:
			return DIVORCED;
		default:
			return UNDEFINED;
		}
	}

	private Integer identifier;

	private MaritalStatusEnum(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
