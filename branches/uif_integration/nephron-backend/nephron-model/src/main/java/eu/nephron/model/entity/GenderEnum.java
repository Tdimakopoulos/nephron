package eu.nephron.model.entity;

import org.apache.commons.lang.math.NumberUtils;

public enum GenderEnum {
	UNDEFINED(0), MALE(1), FEMALE(2), HERMAPHRODITE(3);
	
	public static GenderEnum getGenderEnum(Integer gender) {
		switch (null != gender ? gender : NumberUtils.INTEGER_ZERO) {
		case 1:
			return MALE;
		case 2:
			return FEMALE;
		case 3:
			return HERMAPHRODITE;
		default:
			return UNDEFINED;
		}
	}

	private Integer identifier;

	private GenderEnum(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
