package eu.nephron.model.audit;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.nephron.model.user.NephronUser;

@Embeddable
public class ActionAudit implements Serializable {

	private static final long serialVersionUID = -7234944727522572621L;
	
	private static Map<Thread, ThreadLocal<NephronUser>> tLocals = new WeakHashMap<Thread, ThreadLocal<NephronUser>>();
	
	public static void storeUser(Thread thread, NephronUser user) {
		ThreadLocal<NephronUser> tLocal = tLocals.get(thread);
		if (null == tLocal) {
			tLocal = new ThreadLocal<NephronUser>();
		}
		tLocal.set(user);
		tLocals.put(thread, tLocal);
	}

	public static NephronUser getUser(Thread thread) {
		ThreadLocal<NephronUser> threadLocal = tLocals.get(thread);
		return null != threadLocal ? threadLocal.get() : null;
	}
	
	@ManyToOne
	private NephronUser user;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	public ActionAudit() {
		super();
		this.date = new Date();
	}

	public NephronUser getUser() {
		return user;
	}

	public void setUser(NephronUser user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ActionAudit [user=" + user + ", date=" + date + "]";
	}

}
