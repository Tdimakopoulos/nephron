package eu.nephron.model.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.nephron.model.NephronEntity;

@Entity
@Table(name="COUNTRY")
@NamedQueries({
	@NamedQuery(name="Country.findByCode", query="select c from Country c where c.code=:code"),
	@NamedQuery(name="Country.findByName", query="select c from Country c where c.name=:name")
})
public class Country extends NephronEntity<Long> {

	private static final long serialVersionUID = 8316819814812054853L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COUNTRY_ID")
	private Long id;
	
	@Column(name="CODE", unique=true, nullable=false)
	private String code;
	
	@Column(name="NAME", unique=true, nullable=false)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
