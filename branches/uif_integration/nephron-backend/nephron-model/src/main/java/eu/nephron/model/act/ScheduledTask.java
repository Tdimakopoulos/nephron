package eu.nephron.model.act;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.nephron.model.NephronEntity;

@Entity
@Table(name="SCHEDULED_TASK")
public class ScheduledTask extends NephronEntity<Long> {

	private static final long serialVersionUID = 3662992255773507629L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHEDULED_TASK_ID")
	private Long id;
	
	@Column(name="OUTCOME")
	private Integer outcome;
	
	@ManyToOne
	@JoinColumn(name="ACT_ID")
	private ScheduledAct act;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOutcome() {
		return outcome;
	}

	public void setOutcome(Integer outcome) {
		this.outcome = outcome;
	}

	public ScheduledAct getAct() {
		return act;
	}

	public void setAct(ScheduledAct act) {
		this.act = act;
	}
	
}
