package eu.nephron.jaxb.model.act;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.NephronEntityDTO;

@XmlType(name="ActRelationship")
@XmlRootElement(name="actRelationship")
public class ActRelationshipDTO extends NephronEntityDTO {

	private static final long serialVersionUID = -4787953255378901056L;
	
	private Long id;
	
	private ActDTO inboundAct;	
	
	private ActDTO outboundAct;

	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public ActDTO getInboundAct() {
		return inboundAct;
	}

	public void setInboundAct(ActDTO inboundAct) {
		this.inboundAct = inboundAct;
	}

	@XmlElement
	public ActDTO getOutboundAct() {
		return outboundAct;
	}

	public void setOutboundAct(ActDTO outboundAct) {
		this.outboundAct = outboundAct;
	}

}
