package eu.nephron.jaxb.model.user;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.NephronEntityDTO;

@XmlType(name="NephronUser")
@XmlRootElement(name="nephronUser")
public class NephronUserDTO extends NephronEntityDTO {

	private static final long serialVersionUID = 5136059229672697705L;

	private Long id;

	private String userName;

	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
