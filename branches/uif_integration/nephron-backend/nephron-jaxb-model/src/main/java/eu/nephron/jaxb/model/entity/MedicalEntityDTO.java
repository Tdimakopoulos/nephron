package eu.nephron.jaxb.model.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.NephronEntityDTO;

@XmlType(name="MedicalEntity")
@XmlSeeAlso({
	MaterialDTO.class
})
public abstract class MedicalEntityDTO extends NephronEntityDTO {

	private static final long serialVersionUID = 4600001522572083187L;

	protected Long id;
	
	protected String code;
	
	protected String name;

	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
