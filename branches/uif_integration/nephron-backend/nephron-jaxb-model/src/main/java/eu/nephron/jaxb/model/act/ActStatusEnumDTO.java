package eu.nephron.jaxb.model.act;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="ActStatusEnum")
@XmlRootElement(name="actStatusEnum")
public enum ActStatusEnumDTO {
	UNDEFINED(0), PENDING(1), SUCCESSFUL(2), FAILED(3);

	private Integer identifier;

	private ActStatusEnumDTO(Integer identifier) {
		this.identifier = identifier;
	}

	@XmlElement
	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
