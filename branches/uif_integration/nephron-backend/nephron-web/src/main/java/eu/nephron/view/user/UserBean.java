package eu.nephron.view.user;

import javax.ejb.EJB;
import javax.enterprise.inject.Produces;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import eu.nephron.controller.user.UserService;
import eu.nephron.model.user.NephronUser;

@ManagedBean(name="userBean")
@SessionScoped
public class UserBean {
	
	private static final String LOGGED_IN = "loggedIn";
	
	@EJB
	private UserService userService;
	
	private NephronUser user;
	
	private String userName;
	
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public String login() {
    	user = userService.findUserByUserNameAndPassword(userName, password);
    	return LOGGED_IN;
    }

    public void logout() {
        user = null;
    }
    
    public boolean isLoggedIn() {
       return null != user;
    }
    
    @Produces
    @LoggedIn 
    public NephronUser getCurrentUser() {
        return user;
    }
	
}
