package eu.nephron.view.user;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import eu.nephron.controller.user.RoleService;
import eu.nephron.model.role.Role;

@ManagedBean(name="roleBean")
@SessionScoped
public class RoleBean {
	
	@EJB
	private RoleService roleService;
	
	public List<Role> getAllRoles() {
		return roleService.findAllRoles();
	}
	
}
