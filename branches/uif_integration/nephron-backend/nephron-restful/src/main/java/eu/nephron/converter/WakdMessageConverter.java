package eu.nephron.converter;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.dozer.DozerConverter;

public class WakdMessageConverter extends DozerConverter<byte[], String> {

	public WakdMessageConverter() {
		super(byte[].class, String.class);
	}

	public String convertTo(byte[] source, String destination) {
		return null != source ? Hex.encodeHexString(source) : null;
	}

	public byte[] convertFrom(String source, byte[] destination) {
		try {
			return Hex.decodeHex(source.toCharArray());
		} catch (DecoderException e) {
			throw new RuntimeException(e);
		}
	}

}
