package eu.nephron.restful.resource;

import javax.naming.NamingException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.util.Hex;

import eu.nephron.controller.ServiceLocator;
import eu.nephron.controller.act.CommandService;
import eu.nephron.jaxb.model.act.ActRelationshipDTO;
import eu.nephron.model.act.ActRelationship;
import eu.nephron.utils.mapping.MappingService;
import eu.nephron.utils.mapping.MappingServiceFactory;
import eu.nephron.utils.mapping.MappingServiceFactory.MappingServiceType;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.IncomingWakdMsgFactory;

@Path("/command")
public class CommandResource {
	
	private final static MappingService mapper = 
			MappingServiceFactory.getMappingService(MappingServiceType.CONFIG);
	
	@PUT
	@Path("/read")
	@Produces(MediaType.APPLICATION_XML)
	public ActRelationshipDTO readCommandsAsXML(
			@QueryParam("deviceID") Long deviceID,
			@QueryParam("userID") Long userID) {

		return this.readCommand(deviceID, userID);
	}
	
	@PUT
	@Path("/update")
	public void update(
			@QueryParam("commandID") Long commandID,
			@QueryParam("statusID") Integer statusID,
			@QueryParam("message") String hexStr,
			@QueryParam("userID") Long userID) {
		
		IncomingWakdMsg msg = IncomingWakdMsgFactory.createIncomingWakdMsg(Hex.decodeHex(hexStr));
		this.getCommandService().updateCommand(msg, commandID, statusID, userID);
	}
	
	private ActRelationshipDTO readCommand(Long deviceID, Long userID) {
		ActRelationship actRelationship = getCommandService().readCommand(deviceID, userID);
		return null != actRelationship ? mapper.transform(actRelationship, ActRelationshipDTO.class) : null;
	}
	
	private CommandService getCommandService() {
		try {
			return ServiceLocator.getResource("nephron/CommandServiceImpl/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
