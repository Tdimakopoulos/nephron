// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   math_tool01.c
//! \brief  math routines
//!
//! Some math conversion routines
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include <stdint.h>         // to use uintx_t definitions
#include "main.h"

// -----------------------------------------------------------------------------------
// Constants and Tables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// CRC16
// -----------------------------------------------------------------------------------
#define CRC16_INIT      0x1D0F                            //!< Modified CRC-CCITT initial value

const uint16_t crc16_table[256] = {
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0, 
};
// -----------------------------------------------------------------------------------
//! \brief  This function process the CRC 16 from a whole frame
//!
//! This function process the CRC 16 from a whole frame
//!
//! \param[in]      Data    : Pointer to the data
//! \param[in]      Length  : Number of data
//! \return         Crc 16 value
// -----------------------------------------------------------------------------------
uint16_t crc16_compute( uint8_t* data, uint16_t length )
{
    uint16_t I0 = 0;
    // Initialization
    uint16_t crc = CRC16_INIT;
    uint16_t crc_aux1 = 0;
    uint16_t crc_aux2 = 0;
    
    // Process the CRC 16
    for( I0=0;I0<length; I0++){
        crc_aux1 = crc << 8;
        crc_aux2 = crc >> 8;
        crc_aux2 ^= data[I0];
        crc_aux2 &= 0xFF;
        crc = crc16_table[crc_aux2] ^ crc_aux1;
    }
    return crc;
}
// -----------------------------------------------------------------------------------
//! \brief  ascii_to_char
//!
//! converts an ASCII digit into the correspondent HEXA equivalent
//!
//! \param[IN]  _d0: ASCII digit to be converted
//!
//! \return     _d0hexa: HEXA equivalent
// -----------------------------------------------------------------------------------
uint8_t ascii_to_char(uint8_t _d0){
  uint8_t _d0hexa = 0;
  if(_d0 >= 48 && _d0 <= 57){         // 0 ... 9
    _d0hexa = _d0 - 48;
  }
  else if(_d0 >= 65 && _d0 <= 70){    // A ... F
    _d0hexa = _d0 - 55;
  }
  else if(_d0>=97 && _d0 <=102){      // a ... f
    _d0hexa = _d0 - 87;
  }
   return _d0hexa;
}
// -----------------------------------------------------------------------------------
//! \brief  asciix2_to_char
//!
//! converts 2 ASCII chars into a byte value
//!
//! \param[IN]  _dh: MSB of the byte
//! \param[IN]  _dl: LSB of the byte
//!
//! \return     _d0res: byte composition
// -----------------------------------------------------------------------------------
uint8_t asciix2_to_char(uint8_t _dh, uint8_t _dl){
  uint8_t _d0res = 0;
  _d0res = ascii_to_char(_dh)*16 + ascii_to_char(_dl);
  return _d0res;
}
// -----------------------------------------------------------------------------------
//! \brief  calculate_lrc
//!
//! calculates MODICOM LRC of a set of bytes
//!
//! \param[IN]  chain: pointer to the set of bytes
//! \param[IN]  max_index: max qty of bytes
//!
//! \return     lrc_buffer: calculated lrs
// -----------------------------------------------------------------------------------
uint8_t calculate_lrc(uint8_t* chain, int max_index){
  uint8_t lrc_buffer = 0;
  int i;
  for(i=1; i< max_index; i=i+2){
    lrc_buffer += asciix2_to_char(chain[i], chain[i+1]);
  }
  lrc_buffer = 0xFF - (lrc_buffer&0xFF);
  lrc_buffer += 1;
  return lrc_buffer;
}
// -----------------------------------------------------------------------------------
//! \brief  char_to_ascii
//!
//! converts or MSB nibble or LSB nibble from a char to ASCII
//!
//! \param[IN]  _d0: char value
//! \param[IN]  mode: [mode=1 higher nibble, mode=0 lower nibble]
//!
//! \return     _result: ascii representation of the nibble
// -----------------------------------------------------------------------------------
uint8_t char_to_ascii(uint8_t _d0, int mode){
  uint8_t _result = 0;
  uint8_t _ascii = 0;
  if(mode == 1)       _ascii = (_d0 & 0xF0)/16;
  else if (mode == 0) _ascii = (_d0 & 0x0F);
  if(_ascii <=9) _result = _ascii + 48;
  else _result = _ascii + 55;
  return _result;
}
// -----------------------------------------------------------------------------------
//! \brief  Search_Char
//!
//! Searches a specific character all along a buffer
//!
//! \param[IN]  chain: pointer to the data buffer
//! \param[IN]  CHD: character to be searched
//!
//! \return     chd_pos: position of the character in the buffer, -1 if not found
// -----------------------------------------------------------------------------------
int8_t Search_Char(uint8_t* chain, int8_t max_index, uint8_t CHD){
	int8_t chd_pos = -1;	
	int8_t i;
	for(i=0;i< max_index;i++){
		if(chain[i] == CHD) {
			chd_pos = i;
			break;
		}
	}	
	return chd_pos;			
}
// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! 2-bytes --> dec --> character string [trueqty]
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint8_t decword_to_charstr(uint16_t numbers, uint8_t *charstr, uint8_t charqty, uint16_t multiplier){
    uint8_t ix=0;
    uint16_t decaux = 0;
    uint8_t zeroflag = 0;
    uint8_t trueqty = 0;
    for(ix=0;ix<charqty;ix++) charstr[ix]=0;
    decaux = numbers;
    for(ix=0;ix<(charqty-1);ix++){
            charstr[ix] = (uint8_t)(decaux/multiplier);
            if(charstr[ix]>0) {
                    if((decaux - (charstr[ix]*multiplier))>=0)decaux = decaux - (charstr[ix]*multiplier);
                    else decaux = 0;
                    zeroflag = 1;
            }
            if((charstr[ix]==0) && (zeroflag==0)&& (ix<(charqty-2))) 	
                    charstr[ix] = '0';
            else 					
                  charstr[ix] = decimal_to_char(charstr[ix]);
            multiplier/=10;		
    }
    trueqty = charqty;
    return trueqty;
}

// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! 0..9 value --> character
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint8_t decimal_to_char(uint8_t _charn){
    return (uint8_t)(_charn + 48);
}
// ------------------------------------------------------       
// Applies COBS encoding 
// and eliminates all zeroes ('\0') in the packet
// param1: input packet
// param2: input packet lenght
// param3: pointer to output (codified) packet
// param4: byref final length
// return: pointer to output (codified) packet
// ------------------------------------------------------       
uint8_t* COBS_Encode(uint8_t* txdata, uint16_t tx_len, uint8_t *COBS, uint16_t* cobs_len){

  uint16_t i=0;
  uint8_t search = 0;
  uint8_t idx = 0;
  uint16_t out_p = 0;
  uint16_t int_p = 0;
  
  while(i<tx_len){
          search = 0;
          do{	
                  i++;search++;		
          }while ((txdata[i-1]!=0) && (i<=tx_len) && ((search)<254));
          if(search == 1){
                  COBS[out_p+0] = search;
                  out_p +=search;		
                  int_p +=search;
          }
          else{
                  if(search == 254)		COBS[out_p+0] = 0xFF;
                  else if (search < 254)	COBS[out_p+0] = search;	
                  idx =0;		
                  for(idx=0; idx<search;idx++) {
                          COBS[out_p+1+idx] = txdata[int_p+idx];
                  }
                  if(search == 254){
                          out_p +=search+1;		
                          int_p +=search;
                  }
                  else{
                          out_p +=search;//+1;		
                          int_p +=search;//+1;
                          //i++;
                  }
          }
  }
  cobs_len[0] = out_p;
  return COBS;
}
// ------------------------------------------------------     
// Applies COBS decoding 
// and restores all the zeroes ('\0') eliminated by encoding
// param1: input (codified) packet
// param2: input (codified) packet lenght
// param3: pointer to output (decoded) packet
// return: pointer to output (decoded) packet
// ------------------------------------------------------     
void COBS_Decode(uint8_t* DECOBS, uint16_t rx_len, uint8_t* rxdata, uint16_t* cobs_len){

    uint8_t idx = 0;
    uint16_t out_p = 0;
    uint16_t int_p = 0;
    uint8_t n_block = 0;
    
    while(int_p<rx_len){
            if(DECOBS[int_p] == 0xFF){
              idx =0;		
              for(idx=0; idx<254;idx++) {
                      rxdata[out_p+idx] = DECOBS[int_p+1+idx];
              }
              int_p+=254+1;
              out_p+=254;
            }
            else if(DECOBS[int_p] > 1){
                n_block = DECOBS[int_p];
                idx =0;		
                for(idx=0; idx<n_block-1;idx++) {
                        rxdata[out_p+idx] = DECOBS[int_p+1+idx];
                }
                rxdata[out_p+n_block-1]=0;
                int_p+=n_block;
                out_p+=n_block;
            }
            else if(DECOBS[int_p] == 1){
                rxdata[out_p] = 0;
                int_p++;
                out_p++;
            }
            else if(DECOBS[int_p] == 0){
                break;
            }
    }
    cobs_len[0] = out_p;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Encodes COB (transmitter)
// -----------------------------------------------------------------------------------
//! \brief  PACKETS_CRC16_COBS
//!         
//! Adds Frame Counter, Calculates crc16, encodes COB, Adds final 0
//!
//! \param[in]      PACKETS, raw packet
//! \param[in]      PACKS_LEN, raw packet length
//! \param[in]      BufferXYZ, auxilar buffer
//! \param[out]     TxBufferX_COBS
//! \return         encoded packet length
// -----------------------------------------------------------------------------------
int16_t PACKETS_CRC16_COBS (uint8_t* PACKETS, uint16_t PACKS_LEN, uint8_t* BufferXYZ, uint8_t* TxBufferX_COBS, uint16_t TXRX_RS422_XXX_MAXVALUE){
  uint16_t  txcobslenX[2];
  uint16_t  I0;
  uint16_t crc16_calc = 0;

  if((PACKS_LEN+1)>(TXRX_RS422_XXX_MAXVALUE-24)) return -1;
  // crc --> cobs --> decobs --> crc verif	
  for(I0=0;I0<PACKS_LEN;I0++) BufferXYZ[I0] = PACKETS[I0];
  crc16_calc = crc16_compute(BufferXYZ, PACKS_LEN);

  BufferXYZ[PACKS_LEN] = crc16_calc>>8;
  BufferXYZ[PACKS_LEN+1] = crc16_calc & 0x00FF;

  txcobslenX[0]=0;
  txcobslenX[1]=0;

  *TxBufferX_COBS = *COBS_Encode(&BufferXYZ[0], PACKS_LEN+2, &TxBufferX_COBS[0], &txcobslenX[0]);

  if(txcobslenX[0]>(TXRX_RS422_XXX_MAXVALUE-24))	txcobslenX[0] = TXRX_RS422_RTMCB_MAXVALUE-24;
  TxBufferX_COBS[txcobslenX[0]] = '\0';	
  return (txcobslenX[0]+1);
}
// -----------------------------------------------------------------------------------
// Decodes COB (receiver)
// -----------------------------------------------------------------------------------
//! \brief  DECOBS_DECRC16_PACKETS (previous, no true frame)
//!         
//! Erases final 0, decodes COB, Checks CRC16 and Verifies Frame Counter
//! if OK, non-zero lenght packet in DL_RxBuffer & returns 0 else returns -1
//!
//! \param[in]      ENC_PACKETS, encoded packet
//! \param[in]      ENC_PACKS_LEN, encoded packet length
//! \param[in]      sequence counter
//! \param[in]      BufferXYZ, auxilar buffer
//! \param[out]     RxBufferX
//! \return         decoded packet length
// -----------------------------------------------------------------------------------
int16_t DECOBS_DECRC16_PACKETS(uint8_t *ENC_PACKETS, uint16_t ENC_PACKETS_LEN, uint8_t* BufferXYZ, uint8_t *RxBuffer, uint16_t TXRX_RS422_XXX_MAXVALUE){
    
    uint16_t I0;    
    uint16_t rxcobslenX[2] = {0,0};
    uint16_t crc16_calc = 0;
    uint16_t crc16_recv = 0;

    if(ENC_PACKETS_LEN > (TXRX_RS422_XXX_MAXVALUE-24)) return -1;
    
    COBS_Decode(&ENC_PACKETS[0], ENC_PACKETS_LEN, &BufferXYZ[0],&rxcobslenX[0]);

    if((rxcobslenX[0]-2)>(TXRX_RS422_XXX_MAXVALUE-24)) return -1;

    crc16_calc = crc16_compute(BufferXYZ, rxcobslenX[0] - 2);
    crc16_recv = (BufferXYZ[rxcobslenX[0] - 2] << 8) + BufferXYZ[rxcobslenX[0] - 1];
    if (crc16_calc != crc16_recv) return -1;
    
    for(I0=0;I0< rxcobslenX[0]; I0++) RxBuffer[I0] = BufferXYZ[I0];
    return (rxcobslenX[0]-2); // size of decoded packet
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
