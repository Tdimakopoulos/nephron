// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_acc_application.h
//! \brief  UIF & ACCELEROMETER application
//!
//! scheduler and complementary routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef UIF_ACC_APPLICATION_H_
#define UIF_ACC_APPLICATION_H_
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constants & macros
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void UIF_ACC_SCHEDULER(uint16_t uif_acc_index);
extern uint8_t Send_VBAT1_INFO (uint8_t *data_inp);
extern uint8_t Send_VBAT2_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_STATUS (uint8_t *data_inp);
extern uint8_t Send_WAKD_ATTITUDE(uint8_t *data_inp);
extern uint8_t Send_WAKD_COMMLINK (uint8_t *data_inp);
extern uint8_t Send_WAKD_OPMODE (uint8_t *data_inp);
extern uint8_t Send_WAKD_BLCIRCUIT_STATUS (uint8_t *data_inp);
extern uint8_t Send_WAKD_FLCIRCUIT_STATUS (uint8_t *data_inp);
extern uint8_t Send_WAKD_BLPUMP_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_FLPUMP_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_BLTEMPERATURE (uint8_t *data_inp);
extern uint8_t Send_WAKD_BLCIRCUIT_PRESSURE (uint8_t *data_inp);
extern uint8_t Send_WAKD_FLCIRCUIT_PRESSURE (uint8_t *data_inp);
extern uint8_t Send_WAKD_FLCONDUCTIVITY (uint8_t *data_inp);
extern uint8_t Send_WAKD_HFD_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_SU_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_POLAR_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_ECP1_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_ECP2_INFO (uint8_t *data_inp);
extern uint8_t Send_WAKD_ALARMS (uint8_t *data_inp);
extern uint8_t Send_WAKD_ERRORS (uint8_t *data_inp);
extern uint8_t Send_PDATA_FIRSTNAME (uint8_t *data_inp);
extern uint8_t Send_PDATA_LASTNAME (uint8_t *data_inp);
extern uint8_t Send_PDATA_GENDERAGE (uint8_t *data_inp);
extern uint8_t Send_PDATA_WEIGHT (uint8_t *data_inp);
extern uint8_t Send_PDATA_HEARTRATE (uint8_t *data_inp);
extern uint8_t Send_PDATA_BREATHRATE (uint8_t *data_inp);
extern uint8_t Send_PDATA_ACTIVITY (uint8_t *data_inp);
extern uint8_t Send_PDATA_SEWALL (uint8_t *data_inp);
extern uint8_t Send_PDATA_BLPRESSURE (uint8_t *data_inp);
extern uint8_t Send_PDATA_ECP1 (uint8_t *data_inp);
extern uint8_t Send_PDATA_ECP2 (uint8_t *data_inp);
//extern uint8_t Read_WAKD_SHUTDOWN (uint8_t *data_inp);
extern uint8_t Read_WAKD_START_OPERATION (uint8_t *data_inp);
extern uint8_t Read_WAKD_MODE_OPERATION (uint8_t *data_inp);
extern uint8_t Read_SCALE_GET_WEIGHT (uint8_t *data_inp);
extern uint8_t Read_SEW_START_STREAMING (uint8_t *data_inp);
extern uint8_t Read_SEW_STOP_STREAMING (uint8_t *data_inp);
extern uint8_t Read_NIBP_START_MEASUREMENT (uint8_t *data_inp);

// STATE DIAGRAMS
extern uint8_t SPI_TRIALS_TIMEOUT(void);
extern void SPI_TRIALS_RESET(void);
extern uint8_t STATE_DIAGRAM_TEST_LINK (void);
extern uint8_t STATE_DIAGRAM_WAKD_BLTEMPERATURE (void);
extern uint8_t STATE_DIAGRAM_WAKD_SHUTDOWN (void);
// -----------------------------------------------------------------------------------
#endif /* UIF_ACC_APPLICATION_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
