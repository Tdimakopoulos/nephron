// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_acc_application.c
//! \brief  UIF & ACCELEROMETER application
//!
//! scheduler and complementary routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
// ---------------------------------------- //
// PM-DLDIG Protocol Test
// ---------------------------------------- //
uint8_t mb_uif_acc_qty = 0;


// -----------------------------------------------------------------------------------
//! \brief  UIF_ACC_SCHEDULER
//!
//! Sends one command each time to update or to read information
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void UIF_ACC_SCHEDULER(uint16_t uif_acc_index)
{
    uint8_t x =0;
    for(x=0;x<SPI2_RXTX_LEN;x++) mb_uif_acc_exchange[x] = MB_UIF_CMD_FORBIDDEN;

    switch(uif_acc_index){
        case MB_UIF_SCHNO_VBAT1_INFO:
            mb_uif_acc_qty = Send_VBAT1_INFO (&mb_uif_acc_exchange[0]);            
        break;
        case MB_UIF_SCHNO_VBAT2_INFO:
            mb_uif_acc_qty = Send_VBAT2_INFO (&mb_uif_acc_exchange[0]);            
        break;
        case MB_UIF_SCHNO_WAKD_STATUS:
            mb_uif_acc_qty = Send_WAKD_STATUS (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_ATTITUDE:
            mb_uif_acc_qty = Send_WAKD_ATTITUDE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_COMMLINK:
            mb_uif_acc_qty = Send_WAKD_COMMLINK (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_OPMODE:
            mb_uif_acc_qty = Send_WAKD_OPMODE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS:
            mb_uif_acc_qty = Send_WAKD_BLCIRCUIT_STATUS (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS:
            mb_uif_acc_qty = Send_WAKD_FLCIRCUIT_STATUS (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_BLPUMP_INFO:
            mb_uif_acc_qty = Send_WAKD_BLPUMP_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_FLPUMP_INFO:
            mb_uif_acc_qty = Send_WAKD_FLPUMP_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_BLTEMPERATURE:
            mb_uif_acc_qty = Send_WAKD_BLTEMPERATURE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE:
            mb_uif_acc_qty = Send_WAKD_BLCIRCUIT_PRESSURE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE:
            mb_uif_acc_qty = Send_WAKD_FLCIRCUIT_PRESSURE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY:
            mb_uif_acc_qty = Send_WAKD_FLCONDUCTIVITY (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_HFD_INFO:
            mb_uif_acc_qty = Send_WAKD_HFD_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_SU_INFO:
            mb_uif_acc_qty = Send_WAKD_SU_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_POLAR_INFO:
            mb_uif_acc_qty = Send_WAKD_POLAR_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_ECP1_INFO:
            mb_uif_acc_qty = Send_WAKD_ECP1_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_ECP2_INFO:
            mb_uif_acc_qty = Send_WAKD_ECP2_INFO (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_ALARMS:
            mb_uif_acc_qty = Send_WAKD_ALARMS (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_ERRORS:
            mb_uif_acc_qty = Send_WAKD_ERRORS (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_TEST_LINK:
            //mb_uif_acc_qty = Send_TEST_LINK (&mb_uif_acc_exchange[0]);       
        break;
        case MB_UIF_SCHNO_PDATA_FIRSTNAME:
            mb_uif_acc_qty = Send_PDATA_FIRSTNAME (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_LASTNAME:
            mb_uif_acc_qty = Send_PDATA_LASTNAME (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_GENDERAGE:
            mb_uif_acc_qty = Send_PDATA_GENDERAGE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_WEIGHT:
            mb_uif_acc_qty = Send_PDATA_WEIGHT (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_HEARTRATE:
            mb_uif_acc_qty = Send_PDATA_HEARTRATE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_BREATHRATE:
            mb_uif_acc_qty = Send_PDATA_BREATHRATE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_ACTIVITY:
            mb_uif_acc_qty = Send_PDATA_ACTIVITY (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_SEWALL:
            mb_uif_acc_qty = Send_PDATA_SEWALL (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_BLPRESSURE:
            mb_uif_acc_qty = Send_PDATA_BLPRESSURE (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_ECP1_A:
        case MB_UIF_SCHNO_PDATA_ECP1_B:
            mb_uif_acc_qty = Send_PDATA_ECP1 (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_PDATA_ECP2_A:
        case MB_UIF_SCHNO_PDATA_ECP2_B:
            mb_uif_acc_qty = Send_PDATA_ECP2 (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_SHUTDOWN:
            //mb_uif_acc_qty = Read_WAKD_SHUTDOWN (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_START_OPERATION:
            mb_uif_acc_qty = Read_WAKD_START_OPERATION (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_WAKD_MODE_OPERATION:
            mb_uif_acc_qty = Read_WAKD_MODE_OPERATION (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_SCALE_GET_WEIGHT:
            mb_uif_acc_qty = Read_SCALE_GET_WEIGHT (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_SEW_START_STREAMING:
            mb_uif_acc_qty = Read_SEW_START_STREAMING (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_SEW_STOP_STREAMING:
            mb_uif_acc_qty = Read_SEW_STOP_STREAMING (&mb_uif_acc_exchange[0]);         
        break;
        case MB_UIF_SCHNO_NIBP_START_MEASUREMENT:
            mb_uif_acc_qty = Read_NIBP_START_MEASUREMENT (&mb_uif_acc_exchange[0]);         
        break;
    }
}

// -----------------------------------------------------------------------------------
//! \brief  PM_DL_Data08ExchangeX
//!
//! Send a 8-bit value
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t UIF_ACC_Data08ExchangeX (uint8_t data_out){

    uint8_t data_in = 0;
    uint8_t k=0;
    data_in = UIF_ACC_SendReceive(DEV_UIF, data_out);
    if(data_in !=0xFF) return data_in;
    else{
      for (k=0; k< 16; k++){  // 8
            data_in = UIF_ACC_SendReceive(DEV_UIF, 0xFF);
            if(data_in !=0xFF) return data_in;
      }
    }
    return data_in;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_VBAT1_INFO
//!
//! Sends BAT1 information
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_VBAT1_INFO (uint8_t *data_inp){
    uint8_t i = 0;

    // Update information:
    // Battery Pack 1
    SET16(&MB_UIF_MSG_VBAT1_INFO[0], 1,MB_BPACK_1_Voltage);
    SET16(&MB_UIF_MSG_VBAT1_INFO[0], 3,MB_BPACK_1_RemainingCapacity);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_VBAT1_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_VBAT1_INFO[i]);

    return MB_UIF_QTY_VBAT1_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_VBAT2_INFO
//!
//! Sends BAT2 information
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_VBAT2_INFO (uint8_t *data_inp){
    uint8_t i = 0;

    // Update information:
    // Battery Pack 2
    SET16(&MB_UIF_MSG_VBAT2_INFO[0], 1,MB_BPACK_2_Voltage);
    SET16(&MB_UIF_MSG_VBAT2_INFO[0], 3,MB_BPACK_2_RemainingCapacity);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_VBAT2_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_VBAT2_INFO[i]);

    return MB_UIF_QTY_VBAT2_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_STATUS
//!
//! Sends WAKD STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_STATUS (uint8_t *data_inp){
    uint8_t i = 0;

    // Update information:
    // WAKD STATUS
    SET32(&MB_UIF_MSG_WAKD_STATUS[0], 1,MB_WAKD_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_STATUS; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_STATUS[i]);

    return MB_UIF_QTY_WAKD_STATUS;
}

// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_ATTITUDE
//!
//! Sends WAKD ATTITUDE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_ATTITUDE(uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Attitude
    SET16(&MB_UIF_MSG_WAKD_ATTITUDE[0], 1,MB_WAKD_Attitude);
    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_ATTITUDE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_ATTITUDE[i]);

    return MB_UIF_QTY_WAKD_ATTITUDE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_COMMLINK
//!
//! Sends WAKD CB LINK STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_COMMLINK (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // CB Status
    SET16(&MB_UIF_MSG_WAKD_COMMLINK[0], 1,MB_CB_Status);

    // see if necessary to do as for sd_card, send some bytes
    //for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_COMMLINK; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_COMMLINK[i]);

    return MB_UIF_QTY_WAKD_COMMLINK;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_OPMODE
//!
//! Sends CURRENT WAKD OPERATING MODE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_OPMODE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Operating Mode
    SET16(&MB_UIF_MSG_WAKD_OPMODE[0], 1,MB_WAKD_OperatingMode);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_OPMODE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_OPMODE[i]);

    return MB_UIF_QTY_WAKD_OPMODE;

}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_BLCIRCUIT_STATUS
//!
//! Sends WAKD BLOOD CIRCUIT STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_BLCIRCUIT_STATUS (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Microfluidics
    SET32(&MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[0], 1,MB_WAKD_BLCircuit_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[i]);

    return MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_FLCIRCUIT_STATUS
//!
//! Sends WAKD FILTRATE CIRCUIT STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_FLCIRCUIT_STATUS (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Microfluidics
    SET32(&MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[0], 1,MB_WAKD_FLCircuit_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[i]);

    return MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS;

}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_BLPUMP_INFO
//!
//! Sends WAKD BLOOD PUMP INFO
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_BLPUMP_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    SET16(&MB_UIF_MSG_WAKD_BLPUMP_INFO[0], 1,MB_WAKD_BLPump_Speed);
    SET16(&MB_UIF_MSG_WAKD_BLPUMP_INFO[0], 3,MB_WAKD_BLPump_DirStatus);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_BLPUMP_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_BLPUMP_INFO[i]);

    return MB_UIF_QTY_WAKD_BLPUMP_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_FLPUMP_INFO
//!
//! Sends WAKD DIALYSATE PUMP INFO
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_FLPUMP_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    SET16(&MB_UIF_MSG_WAKD_FLPUMP_INFO[0], 1,MB_WAKD_FLPump_Speed);
    SET16(&MB_UIF_MSG_WAKD_FLPUMP_INFO[0], 3,MB_WAKD_FLPump_DirStatus);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_FLPUMP_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_FLPUMP_INFO[i]);

    return MB_UIF_QTY_WAKD_FLPUMP_INFO;
}

// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_BLTEMPERATURE
//!
//! Sends BLOOD INLET/OUTLET TEMPERATURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_BLTEMPERATURE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Temperature Sensor
    SET16(&MB_UIF_MSG_WAKD_BLTEMPERATURE[0], 1,MB_WAKD_BTS_InletTemperature);
    SET16(&MB_UIF_MSG_WAKD_BLTEMPERATURE[0], 3,MB_WAKD_BTS_OutletTemperature);
    SET16(&MB_UIF_MSG_WAKD_BLTEMPERATURE[0], 5,MB_WAKD_BTS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_BLTEMPERATURE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_BLTEMPERATURE[i]);

    return MB_UIF_QTY_WAKD_BLTEMPERATURE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_BLCIRCUIT_PRESSURE
//!
//! Sends BLOOD CIRCUIT PRESSURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_BLCIRCUIT_PRESSURE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Pressure Sensors
    SET16(&MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[0], 1,MB_WAKD_BPS_Pressure);
    SET16(&MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[0], 3,MB_WAKD_BPS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[i]);

    return MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_FLCIRCUIT_PRESSURE
//!
//! Sends WAKD FILTRATE CIRCUIT PRESSURE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_FLCIRCUIT_PRESSURE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Pressure Sensors
    SET16(&MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[0], 1,MB_WAKD_FPS_Pressure);
    SET16(&MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[0], 3,MB_WAKD_FPS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[i]);

    return MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_FLCONDUCTIVITY
//!
//! Sends WAKD FILTRATE CIRCUIT CONDUCTIVITY
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_FLCONDUCTIVITY (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Conductivity Sensor
    SET16(&MB_UIF_MSG_WAKD_FLCONDUCTIVITY[0], 1,MB_WAKD_DCS_Conductance);
    SET16(&MB_UIF_MSG_WAKD_FLCONDUCTIVITY[0], 3,MB_WAKD_DCS_Susceptance);
    SET16(&MB_UIF_MSG_WAKD_FLCONDUCTIVITY[0], 5,MB_WAKD_DCS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_FLCONDUCTIVITY; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_FLCONDUCTIVITY[i]);

    return MB_UIF_QTY_WAKD_FLCONDUCTIVITY;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_HFD_INFO
//!
//! Sends WAKD HIGH-FLUX-DIALYSER INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_HFD_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD High Flux Dialyser
    SET16(&MB_UIF_MSG_WAKD_HFD_INFO[0], 1,MB_WAKD_HFD_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_HFD_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_HFD_INFO[i]);

    return MB_UIF_QTY_WAKD_HFD_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_SU_INFO
//!
//! Sends WAKD SORBENT UNIT INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_SU_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Sorbent Unit
    SET16(&MB_UIF_MSG_WAKD_SU_INFO[0], 1,MB_WAKD_SU_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_SU_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_SU_INFO[i]);

    return MB_UIF_QTY_WAKD_SU_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_POLAR_INFO
//!
//! Sends WAKD POLARIZER INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_POLAR_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Polarizer
    SET16(&MB_UIF_MSG_WAKD_POLAR_INFO[0], 1,MB_WAKD_POLAR_Voltage);
    SET16(&MB_UIF_MSG_WAKD_POLAR_INFO[0], 3,MB_WAKD_POLAR_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_POLAR_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_POLAR_INFO[i]);

    return MB_UIF_QTY_WAKD_POLAR_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_ECP1_INFO
//!
//! Sends WAKD ECP1 INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_ECP1_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD ECPs
    SET16(&MB_UIF_MSG_WAKD_ECP1_INFO[0], 1,MB_WAKD_ECP1_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_ECP1_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_ECP1_INFO[i]);

    return MB_UIF_QTY_WAKD_ECP1_INFO;

}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_ECP2_INFO
//!
//! Sends WAKD ECP2 INFORMATION
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_ECP2_INFO (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD ECPs
    SET16(&MB_UIF_MSG_WAKD_ECP2_INFO[0], 1,MB_WAKD_ECP2_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_ECP2_INFO; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_ECP2_INFO[i]);

    return MB_UIF_QTY_WAKD_ECP2_INFO;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_ALARMS
//!
//! Sends WAKD ALARMS STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_ALARMS (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Alarms
    SET32(&MB_UIF_MSG_ALARMS[0], 1,MB_WAKD_ALARMS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_ALARMS; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_ALARMS[i]);

    return MB_UIF_QTY_ALARMS;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_WAKD_ERRORS
//!
//! Sends WAKD ERRORS STATUS
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_WAKD_ERRORS (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    // WAKD Alarms
    SET32(&MB_UIF_MSG_ERRORS[0], 1,MB_WAKD_ERRORS_Status);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_ERRORS; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_ERRORS[i]);

    return MB_UIF_QTY_ERRORS;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_FIRSTNAME
//!
//! Sends PATIENT FIRSTNAME
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_FIRSTNAME (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    for(i=0;i<14;i++)MB_UIF_MSG_PDATA_FIRSTNAME[i+1] = P_DATA_FIRSTNAME[i];

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_FIRSTNAME; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_FIRSTNAME[i]);

    return MB_UIF_QTY_PDATA_FIRSTNAME;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_LASTNAME
//!
//! Sends PATIENT LASTNAME
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_LASTNAME (uint8_t *data_inp){

    uint8_t i = 0;
    // Update information:
    for(i=0;i<14;i++)MB_UIF_MSG_PDATA_LASTNAME[i+1] = P_DATA_LASTNAME[i];

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_LASTNAME; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_LASTNAME[i]);

    return MB_UIF_QTY_PDATA_LASTNAME;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_GENDERAGE
//!
//! Sends PATIENT GENDER AND AGE
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_GENDERAGE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_PDATA_GENDERAGE[1] = P_DATA_GENDER[0];
    MB_UIF_MSG_PDATA_GENDERAGE[2] = P_DATA_AGE;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_GENDERAGE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_GENDERAGE[i]);

    return MB_UIF_QTY_PDATA_GENDERAGE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_WEIGHT
//!
//! Sends PATIENT WEIGHT
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_WEIGHT (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    SET16(&MB_UIF_MSG_PDATA_WEIGHT[0], 1,P_DATA_WEIGHT);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_WEIGHT; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_WEIGHT[i]);

    return MB_UIF_QTY_PDATA_WEIGHT;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_HEARTRATE
//!
//! Sends PATIENT CURRENT HEART RATE (if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_HEARTRATE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_PDATA_HEARTRATE[1] = P_DATA_HEARTRATE;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_HEARTRATE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_HEARTRATE[i]);

    return MB_UIF_QTY_PDATA_HEARTRATE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_BREATHRATE
//!
//! Sends PATIENT CURRENT BREATHING RATE (if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_BREATHRATE (uint8_t *data_inp){

    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_PDATA_BREATHRATE[1] = P_DATA_BREATHINGRATE;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_BREATHRATE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_BREATHRATE[i]);

    return MB_UIF_QTY_PDATA_BREATHRATE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_ACTIVITY
//!
//! Sends PATIENT CURRENT ACTIVITY INFORMATION (if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_ACTIVITY (uint8_t *data_inp){

    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_PDATA_ACTIVITY[1] = P_DATA_ACTIVITYCODE;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_ACTIVITY; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_ACTIVITY[i]);

    return MB_UIF_QTY_PDATA_ACTIVITY;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_SEWALL
//!
//! Sends PATIENT CURRENT HEART RATE, BREATHING RATE, ACTIVITY INFO (if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_SEWALL (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    MB_UIF_MSG_PDATA_SEWALL[1] = P_DATA_HEARTRATE;
    MB_UIF_MSG_PDATA_SEWALL[2] = P_DATA_BREATHINGRATE;
    MB_UIF_MSG_PDATA_SEWALL[3] = P_DATA_ACTIVITYCODE;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_SEWALL; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_SEWALL[i]);

    return MB_UIF_QTY_PDATA_SEWALL;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_BLPRESSURE
//!
//! Sends PATIENT BLOOD PRESSURE (NIBP MEASUREMENT, if available)
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_BLPRESSURE (uint8_t *data_inp){
    uint8_t i = 0;
    // Update information:
    SET16(&MB_UIF_MSG_PDATA_BLPRESSURE[0], 1,P_DATA_SISTOLICBP);
    SET16(&MB_UIF_MSG_PDATA_BLPRESSURE[0], 3,P_DATA_DIASTOLICBP);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_BLPRESSURE; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_BLPRESSURE[i]);

    return MB_UIF_QTY_PDATA_BLPRESSURE;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_ECP1
//!
//! Sends ECP1 VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_ECP1 (uint8_t *data_inp){
    uint8_t i = 0;
#if 0
    // Update information:
    SET16(&MB_UIF_MSG_PDATA_ECP1[0], 1,P_DATA_NA_ECP1);
    SET16(&MB_UIF_MSG_PDATA_ECP1[0], 3,P_DATA_K_ECP1);
    SET16(&MB_UIF_MSG_PDATA_ECP1[0], 5,P_DATA_PH_ECP1);
    SET16(&MB_UIF_MSG_PDATA_ECP1[0], 7,P_DATA_UREA_ECP1);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_ECP1; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_ECP1[i]);
#endif
    return MB_UIF_QTY_PDATA_ECP1;
}
// -----------------------------------------------------------------------------------
//! \brief  Send_PDATA_ECP2
//!
//! Sends ECP2 VALUES
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Send_PDATA_ECP2 (uint8_t *data_inp){
    uint8_t i = 0;
#if 0
    // Update information:
    SET16(&MB_UIF_MSG_PDATA_ECP2[0], 1,P_DATA_NA_ECP2);
    SET16(&MB_UIF_MSG_PDATA_ECP2[0], 3,P_DATA_K_ECP2);
    SET16(&MB_UIF_MSG_PDATA_ECP2[0], 5,P_DATA_PH_ECP2);
    SET16(&MB_UIF_MSG_PDATA_ECP2[0], 7,P_DATA_UREA_ECP2);

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_PDATA_ECP2; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_PDATA_ECP2[i]);
#endif
    return MB_UIF_QTY_PDATA_ECP2;
}
// -----------------------------------------------------------------------------------
//! \brief  Read_WAKD_START_OPERATION
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_WAKD_START_OPERATION (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_START_OPERATION; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_START_OPERATION[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_WAKD_START_OPERATION] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_WAKD_START_OPERATION+MB_UIF_QTY_ANSWER_1;
}
// -----------------------------------------------------------------------------------
//! \brief  Read_WAKD_MODE_OPERATION
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_WAKD_MODE_OPERATION (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_WAKD_MODE_OPERATION; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_WAKD_MODE_OPERATION[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_WAKD_MODE_OPERATION] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_WAKD_MODE_OPERATION+MB_UIF_QTY_ANSWER_1;

}
// -----------------------------------------------------------------------------------
//! \brief  Read_SCALE_GET_WEIGHT
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_SCALE_GET_WEIGHT (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_SCALE_GET_WEIGHT; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_SCALE_GET_WEIGHT[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_SCALE_GET_WEIGHT] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_SCALE_GET_WEIGHT+MB_UIF_QTY_ANSWER_1;
}
// -----------------------------------------------------------------------------------
//! \brief  Read_SEW_START_STREAMING
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_SEW_START_STREAMING (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_SEW_START_STREAMING; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_SEW_START_STREAMING[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_SEW_START_STREAMING] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_SEW_START_STREAMING+MB_UIF_QTY_ANSWER_1;
}
// -----------------------------------------------------------------------------------
//! \brief  Read_SEW_STOP_STREAMING
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_SEW_STOP_STREAMING (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_SEW_STOP_STREAMING; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_SEW_STOP_STREAMING[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_SEW_STOP_STREAMING] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_SEW_STOP_STREAMING+MB_UIF_QTY_ANSWER_1;
}
// -----------------------------------------------------------------------------------
//! \brief  Read_NIBP_START_MEASUREMENT
//!
//! ...
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t Read_NIBP_START_MEASUREMENT (uint8_t *data_inp){
    uint8_t i = 0;

    // see if necessary to do as for sd_card, send some bytes
    for (i=0; i< 8; i++) UIF_ACC_SendReceive(DEV_UIF, 0xFF);
    // send the data in the buffer
    for (i=0; i< MB_UIF_QTY_NIBP_START_MEASUREMENT; i++) data_inp[i] = UIF_ACC_Data08ExchangeX(MB_UIF_MSG_NIBP_START_MEASUREMENT[i]);
    for (i=0; i< MB_UIF_QTY_ANSWER_1; i++) data_inp[i+MB_UIF_QTY_NIBP_START_MEASUREMENT] = UIF_ACC_Data08ExchangeX(0xFF);

    return MB_UIF_QTY_NIBP_START_MEASUREMENT+MB_UIF_QTY_ANSWER_1;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// STATE DIAGRAMS
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_TEST_LINK
//!
//! Sends LINK TEST
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_TEST_LINK (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send receive CMD
        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // send receive DATA
        uif_acc_din = MB_UIF_MSG_TEST_LINK[UIF_ACC_INDEX];
        if(UIF_ACC_INDEX == 0) UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(UIF_ACC_INDEX>0) uif_data_inpx[UIF_ACC_INDEX-1] = uif_acc_dout;
        UIF_ACC_INDEX++;
        if(UIF_ACC_INDEX>=UIF_ACC_BYTES){
            UIF_ACC_STATEDIAG = UIF_STATE_CHKTX;
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_CHKTX){
        sb_ok = 1;
        for (spicnt=0; spicnt<(UIF_ACC_INDEX-2);spicnt++){
            if(uif_data_inpx[spicnt]!=MB_UIF_MSG_TEST_LINK[spicnt]){
                sb_ok=0; break;
            }
        }
        if(sb_ok == 1) {
            uif_acc_dout = uif_data_inpx[UIF_ACC_INDEX-2];
            if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_ACKNOWLEDGE;
                UIF_ACC_ACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ok++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                UIF_ACC_NACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else{
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ko++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
        }
        else {
          uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
          uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
          uif_acc_data_ko++;
          UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_WAKD_BLTEMPERATURE
//!
//! Sends BTS DATA
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_WAKD_BLTEMPERATURE (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send receive CMD
        // Update information:
        // WAKD Temperature Sensor
        SET16(&MB_UIF_MSG_WAKD_BLTEMPERATURE[0], 1,MB_WAKD_BTS_InletTemperature);
        SET16(&MB_UIF_MSG_WAKD_BLTEMPERATURE[0], 3,MB_WAKD_BTS_OutletTemperature);

        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // send receive DATA
        uif_acc_din = MB_UIF_MSG_WAKD_BLTEMPERATURE[UIF_ACC_INDEX];
        if(UIF_ACC_INDEX == 0)  UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(UIF_ACC_INDEX>0) uif_data_inpx[UIF_ACC_INDEX-1] = uif_acc_dout;
        UIF_ACC_INDEX++;
        if(UIF_ACC_INDEX>=UIF_ACC_BYTES){
            UIF_ACC_STATEDIAG = UIF_STATE_CHKTX;
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_CHKTX){
        sb_ok = 1;
        for (spicnt=0; spicnt<(UIF_ACC_INDEX-2);spicnt++){
            if(uif_data_inpx[spicnt]!=MB_UIF_MSG_WAKD_BLTEMPERATURE[spicnt]){
                sb_ok=0; break;
            }
        }
        if(sb_ok == 1) {
            uif_acc_dout = uif_data_inpx[UIF_ACC_INDEX-2];
            if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_ACKNOWLEDGE;
                UIF_ACC_ACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ok++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                UIF_ACC_NACK_NO++;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
            else{
                uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
                uif_acc_data_ko++;
                UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            }
        }
        else {
          uif_acc_din = MB_UIF_CMD_NOT_ACKNOWLEDGE;
          uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
          uif_acc_data_ko++;
          UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        completed = true;
    }
    return completed;
}

// -----------------------------------------------------------------------------------
//! \brief  STATE_DIAGRAM_WAKD_SHUTDOWN
//!
//! Sends BTS DATA
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t STATE_DIAGRAM_WAKD_SHUTDOWN (){
    uint8_t completed = false;
    uint8_t spicnt = 0;
    uint8_t sb_ok = 0;

    uif_acc_dout=0;
    if(UIF_ACC_STATEDIAG == UIF_STATE_START){             // send receive CMD
        for (spicnt = 0; spicnt<UIF_TXRX_LEN;spicnt++) uif_data_inpx[spicnt] = MB_UIF_CMD_CLR;
        uif_acc_din = MB_UIF_CMD_TEST1;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == uif_acc_din){
            UIF_ACC_INDEX = 0;
            UIF_ACC_STATEDIAG = UIF_STATE_TXCMD;  
            SPI_TRIALS_RESET();
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXCMD){       // TX CMD RX TEST1
        uif_acc_din = MB_UIF_MSG_WAKD_SHUTDOWN[0];
        UIF_ACC_CMD_ID = uif_acc_din; // ID = BYTE0
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == MB_UIF_CMD_TEST1){
            UIF_ACC_STATEDIAG = UIF_STATE_RXD2;  
            SPI_TRIALS_RESET();
        }
        else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD2){       // TX END RX CMD
        uif_acc_din = MB_UIF_MSG_WAKD_SHUTDOWN[1];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout == UIF_ACC_CMD_ID){
            UIF_ACC_STATEDIAG = UIF_STATE_RXD1;  
            SPI_TRIALS_RESET();
        }
        else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
        }
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD1){       // TX TEST2 RX D[1]
        uif_acc_din = MB_UIF_CMD_TEST2;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        uif_data_inpx[0] = uif_acc_dout;
        UIF_ACC_STATEDIAG = UIF_STATE_RXD0;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXD0){       // TX D[1] RX D[0]
        uif_acc_din = uif_data_inpx[0];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        uif_data_inpx[1] = uif_acc_dout;
        UIF_ACC_STATEDIAG = UIF_STATE_RXACKNK;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_RXACKNK){    // TX D[0] RX ACK/NACK
        uif_acc_din = uif_data_inpx[1];
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        UIF_ACC_STATEDIAG = UIF_STATE_TXACKNK;  
    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_TXACKNK){    // TX ACK/NACK RX --
        uif_acc_din = uif_acc_dout;
        uif_acc_dout =  UIF_SendReceiveHWSW(1, uif_acc_din);
        if(uif_acc_dout==MB_UIF_CMD_ACKNOWLEDGE) {
            UIF_ACC_ACK_NO++;
            uif_acc_data_ok++;
            UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            SPI_TRIALS_RESET();
        }
        else if(uif_acc_dout==MB_UIF_CMD_NOT_ACKNOWLEDGE) {
            UIF_ACC_NACK_NO++;
            UIF_ACC_STATEDIAG = UIF_STATE_ACKNK;  // look for re-start or change cmd
            SPI_TRIALS_RESET();
       }
       else{
            completed = SPI_TRIALS_TIMEOUT();
            if(completed==true){
                UIF_ACC_TEST_TIMEOUT++;
                UIF_ACC_STATEDIAG = UIF_STATE_START;
            }
       }

    }
    else if(UIF_ACC_STATEDIAG == UIF_STATE_ACKNK){
        UIF_ACC_STATEDIAG = UIF_STATE_DUMMY;
    }
    else if (UIF_ACC_STATEDIAG == UIF_STATE_DUMMY){
        UIF_ACC_STATEDIAG = UIF_STATE_START;
        UIF_ACC_TEST_OK++;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
//! \brief  SPI_TRIALS_TIMEOUT
//!
//! returns TRUE if tried communication UIF_COM_TRIALS times
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t SPI_TRIALS_TIMEOUT(){
    uint8_t completed = false;
    uif_acc_trial_counter++;
    if(uif_acc_trial_counter>=UIF_COM_TRIALS){
        uif_acc_trial_counter = 0;
        completed = true;
    }
    return completed;
}
// -----------------------------------------------------------------------------------
//! \brief  SPI_TRIALS_RESET
//!
//! returns TRUE if tried communication UIF_COM_TRIALS times
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void SPI_TRIALS_RESET(){
    uif_acc_trial_counter = 0;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
