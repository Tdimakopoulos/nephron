// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   SerialPortRTMCB.c
//! \brief  RTMCB Protocol (RS422)
//!
//! Communication Tests
//!
//! \author  Dudnik G.
//! \date    10.01.2012
//! \version 1.0 First version (GDU)
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include <stdint.h>
#include <string.h>

#include "main.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  CLR_OUT_COMM_BUFFERS
//!         
//! Routine to be used in USART_Rx int or main
//!
//! \param[in,out]  none
//! \return         nothing
// -----------------------------------------------------------------------------------
void CLR_RTMCB_COMM_BUFFERS(void){
    out_rtmcb_reclen_0 = 0;
    out_rtmcb_reclen_1 = 0;
    OUT_COMM_RTMCB_FLAG = 0;
    INP_COMM_RTMCB_FLAG = 0;
    rs422_rtmcb_whichgroup = RS422_NOGROUP;
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_Transaction Reads Data and analyzes when the message is ready. Sends answer.
//!         
//! Routine to be used in USART_Rx int or main.
//! Reads Data and analyzes when the message is ready. Sends answer.
//!
//! \param[in,out]  none
//! \return         usart_rxchck
// -----------------------------------------------------------------------------------
uint16_t RTMCB_Transaction (USART_TypeDef* USARTx){
    uint16_t iq = 0;
    uint8_t net_address = 0;
    uint8_t zrx_function[2];
    uint16_t usart_rxdata = 0xFFFF;
    uint16_t usart_rxchck = 0x0000;

    if (MB_SYSTEM_READY==DEVICE_NOTREADY) return usart_rxchck;

    usart_rxdata =  USART_READ_CHECKERRORS (USARTx);
    usart_rxchck =  usart_rxdata & 0xF000;
    if(usart_rxchck!=0) return usart_rxchck;

    if((usart_rxdata == '\0')&&(INP_COMM_RTMCB_FLAG==0)) return usart_rxchck;


    if((usart_rxdata !=0)&&(INP_COMM_RTMCB_FLAG==0)) {		
        out_rtmcb_reclen_0 = 0;
        INP_COMM_RTMCB_FLAG = 0x80;
    }
    if (INP_COMM_RTMCB_FLAG == 0x80){		
        if(out_rtmcb_reclen_0 <(TXRX_RS422_RTMCB_MAXVALUE-24)){
            if (usart_rxdata == '\0') {
                INP_COMM_RTMCB_FLAG = 0x99; // 0xFF;
            }	
            else{
                zrx_out_rtmcb_str_0[out_rtmcb_reclen_0] = usart_rxdata;
                out_rtmcb_reclen_0 ++;
            }
        }
        else{
            INP_COMM_RTMCB_FLAG = 0;
            out_rtmcb_reclen_0 = 0;
        }
    }

    if(OUT_COMM_RTMCB_FLAG == 0xFF) return usart_rxchck;

    if(INP_COMM_RTMCB_FLAG == 0x99) {
        for(iq=0; iq<out_rtmcb_reclen_0;iq++){
            zrx_out_rtmcb_str_1[iq] = zrx_out_rtmcb_str_0[iq];
            zrx_out_rtmcb_str_0[iq]=0;
        }
        out_rtmcb_reclen_1 = out_rtmcb_reclen_0;
        out_rtmcb_reclen_0 = 0;
        INP_COMM_RTMCB_FLAG = 0x00;
        OUT_COMM_RTMCB_FLAG = 0xFF;
    }
        
    return usart_rxchck;   
}

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------