// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_acc_spi0.c
//! \brief  spi0 for accelerometer (attitude measurement) and UIF 
//!
//! spi0 initialization, send & receive data routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
#define DELAY_SPI   25
// -----------------------------------------------------------------------------------
//! \brief  UIF_ACC_SPIConfig
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void UIF_ACC_SPIConfig(void)
{

  SPI_InitTypeDef    SPI_InitStructure;

  SPI_I2S_DeInit(UIF_ACC_SPI);
 // GPIO CONFIGURED IN RTMCB_GPIO_Config
 
  // Enable SPI clock 
  RCC_APB1PeriphClockCmd(UIF_ACC_SPI_CLK, ENABLE); 
   
  // SPI Config 
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge; // SPI_CPHA_2Edge; // SPI_CPHA_1Edge;
  // SPI_NSS_Soft --> SD_NCS_PIN configure as OUT_PP 
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; //128 ok
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;

  SPI_Init(UIF_ACC_SPI, &SPI_InitStructure);

  // SPI enable
  SPI_Cmd(UIF_ACC_SPI, ENABLE);
}

// -----------------------------------------------------------------------------------
//! \brief  UIF_SendData
//!
//! Sends data to the UIF Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _data
//!
//! \return       none
// -----------------------------------------------------------------------------------
void UIF_SendData(uint8_t mode, uint8_t _data)
{
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(UIF_ACC_SPI, _data);
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET);
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  mode = mode;  // bp
}
// -----------------------------------------------------------------------------------
//! \brief  UIF_ReceiveData
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \return       _data
// -----------------------------------------------------------------------------------
uint8_t UIF_ReceiveData(uint8_t mode)
{
  uint8_t _data = 0;
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _data = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  return _data;
}


// -----------------------------------------------------------------------------------
//! \brief  UIF_SendHWSW
//!
//! WRITES DATA
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
void UIF_SendHWSW(uint8_t mode, uint8_t _dataIN)
{
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); 
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
  // 05.02.2012
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET);
  DelayBySoft (100); 
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); 
}
// -----------------------------------------------------------------------------------
//! \brief  UIF_ReceiveHWSW
//!
//! READS a DATA
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
uint8_t UIF_ReceiveHWSW(uint8_t mode, uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
  
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); 
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (100); 
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); 
  return _dataOUT;
}
// -----------------------------------------------------------------------------------
//! \brief  UIF_SendReceiveHWSW
//!
//! WRITES & READS a DATA
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
// ORIGINAL
// -----------------------------------------------------------------------------------
#if 0
// -----------------------------------------------------------------------------------
uint8_t UIF_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
  
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
  // 05.02.2012
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET);
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (100); // 100 OK
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  return _dataOUT;
}
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// MODIFIED...
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
#if 1
// -----------------------------------------------------------------------------------
uint8_t UIF_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
  uint32_t timeout_counter = 0;
    
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
  // 05.02.2012
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0 
  timeout_counter = 0;
  while((SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET)&&(timeout_counter<SPI_TIMEOUT)){
      timeout_counter++; DelayBySoft (1); 
  }
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while((SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET)&&(timeout_counter<SPI_TIMEOUT)){
      timeout_counter++; DelayBySoft (1); 
  }
  _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (100); // 100 OK
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  return _dataOUT;
}
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
//! \brief  UIF_ANSWER_8BYTES
//!
//! READS UNTIL !=0xFF
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
uint8_t UIF_ANSWER_8BYTES(uint8_t nbytes)
{
	uint8_t i;
	uint8_t resp;
	
	/* Respone will come after 1 - 8 pings */
	for(i=0;i<nbytes;i++){
		resp = UIF_SendReceiveHWSW(1, 0xFF);
		if(resp != 0xFF)
			return(resp);
	}	
	return(resp);
}
// -----------------------------------------------------------------------------------
//! \brief  UIF_ACC_SendReceive
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    whichres
//!               1 UIF
//!               2 ACCELEROMETER
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
uint8_t UIF_ACC_SendReceive(uint8_t whichres, uint8_t _dataIN)
{
    uint8_t _dataOUT = 0;
    
    switch(whichres){
        case DEV_UIF:
            STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
            break;
        case DEV_ACC:
            STM32F_GPIOOff(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
            break;
    }

    DelayBySoft (DELAY_SPI); 
#if 1    // 05.02.2012
    // SPI_I2S_FLAG_TXE = 1 EMPTY
    while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
    SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
    // 05.02.2012
    // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0 
    while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET);
    // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
    while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
    _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
#else
    SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
    while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET);
    while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
    _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
#endif
  
    DelayBySoft (DELAY_SPI); 

    switch(whichres){
        case DEV_UIF:
            STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
            break;
        case DEV_ACC:
            STM32F_GPIOOn(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
            break;
    }
  
    return _dataOUT;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
