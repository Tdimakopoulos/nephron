/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifdef LINUX
	#include "nephron.h"
	#include "interfaces.h"
	#include "ioHelperFunc.h"
	#include "main.h"
	#include "byteTransfer.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
	#include "..\\includes_nephron\\interfaces.h"
	#include "..\\ioHelperFunc.h"
	#include "..\\main\\main.h"
	#include "..\\byteTransfer.h"
#endif

// getData pulls a data block from the HW-interface buffer according to the selected sender by "whoId".
// Location of the buffer is shared knowledge between HW-interface API and TASKs. Caller of the function is
// responsible to provide pointer to sufficient storage location starting at "startData" with the size of
// "lengthData". getData will then copy "lengthDat" of Bytes to the location of "startData". If successfull,
// getData will return 0, 1 if error occured.
// Calling getData will also signal the API that the HW-interface buffer that was previously blocked can now
// be reused and overwritten gain.

// Helper function to get first byte of buffer vom serial interface and interprate it as dataId
extern tdDataId readDataId(tdWhoId whoId);

#ifdef VP_SIMULATION
	extern tdDataId decodeMsg(void);
#endif
