/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "fcHelperFunc.h"

#define PREFIX "OFFIS FreeRTOS task FC"

uint8_t changeIntoStateRequest(tdWakdStates newState, tdWakdOperationalState newOpState, tdMsgCurrentWAKDstateIs *pCurrentState, xQueueHandle *pQueueHandle)
{
	uint8_t err = 0;
	tdQtoken Qtoken;
	tdMsgWakdStateFromTo *pMsgWakdStateFromTo = NULL;
	pMsgWakdStateFromTo = (tdMsgWakdStateFromTo *) pvPortMalloc(sizeof(tdMsgWakdStateFromTo));
	if (pMsgWakdStateFromTo==NULL)
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		pMsgWakdStateFromTo->header.dataId		= dataID_changeWAKDStateFromTo;
		pMsgWakdStateFromTo->header.issuedBy	= whoId_MB;
		pMsgWakdStateFromTo->header.recipientId	= whoId_RTB;
		pMsgWakdStateFromTo->fromWakdOpState	= pCurrentState->wakdOpStateIs;
		pMsgWakdStateFromTo->fromWakdState		= pCurrentState->wakdStateIs;
		pMsgWakdStateFromTo->toWakdOpState		= newOpState;
		pMsgWakdStateFromTo->toWakdState		= newState;

		// Sending the data to task RTBPA
		Qtoken.command	= command_RtbChangeIntoStateRequest;
		Qtoken.pData	= (void *) pMsgWakdStateFromTo;

		#if defined DEBUG_FC || defined SEQ0 || defined SEQ4
			taskMessage("I", PREFIX, "Request for RTB to change into state '%s'|'%s'.", stateIdToString(newState), opStateIdToString(newOpState));
		#endif
		if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'command_RtbChangeIntoStateRequest'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
			err = 1;
		} else {
			// The request to change the state is on its way. Since we never know when the
			// task FC is interrupted by the scheduler, at this point, we might already be
			// in the new state or still in the old. So we don't know which means we are
			// setting the state to undefined to document this. FC will be informed later
			// by msg "dataID_currentWAKDstateIs" and correct this back.
			pCurrentState->wakdOpStateIs = wakdStatesOS_UndefinedInitializing;
			pCurrentState->wakdStateIs	 = wakdStates_UndefinedInitializing;
		}
	}
	return(err);
}
// Current state will be answered by RTB!!
//void sendCurrentState(tdMsgCurrentWAKDstateIs *pCurrentState, tdQtoken *pQtoken, xQueueHandle *pQueueHandle)
//{
//	#if defined DEBUG_FC || defined SEQ0
//		taskMessage("I", PREFIX, "sendCurrentState() is %s", stateIdToString(pCurrentState->wakdStateIs));
//	#endif
//
//	tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs  = NULL;
//	pMsgCurrentWAKDstateIs  = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs)); // Will be freed by receiver task!
//
//	pMsgCurrentWAKDstateIs->header.recipientId	= whoId_RTB;
//	pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
//	pMsgCurrentWAKDstateIs->header.issuedBy		= whoId_MB;
//	pMsgCurrentWAKDstateIs->wakdStateIs			= pCurrentState->wakdStateIs;
//	pMsgCurrentWAKDstateIs->wakdOpStateIs		= pCurrentState->wakdOpStateIs;
//
//	pQtoken->command = command_UseAtachedMsgHeader;
//	pQtoken->pData = (void *) pMsgCurrentWAKDstateIs;
//	if ( xQueueSend( pQueueHandle, pQtoken, FC_BLOCKING) != pdPASS )
//	{	// Seemingly the queue is full for too long time. Do something accordingly!
//		taskMessage("E", PREFIX, "Queue of DISP did not accept 'dataID_currentWAKDstateIs'!");
//		errMsg(errmsg_QueueOfDispatcherTaskFull);
//		sFree(&(pQtoken->pData));
//	}
//}

// will return 0 if successful, 1 in case of error.
uint8_t sendWakdStateConfig(tdWakdStates stateToConfig, tdWAKDStateConfigurationParameters *pWAKDStateConfigurationParameters, xQueueHandle *pQueueHandle)
{
	uint8_t err = 0;
	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	Qtoken.pData	= NULL;

	tdMsgConfigureState *pMsgConfigureStateforRTB  = NULL;
	pMsgConfigureStateforRTB  = (tdMsgConfigureState *) pvPortMalloc(sizeof(tdMsgConfigureState)); // Will be freed by receiver task!
	if (pMsgConfigureStateforRTB==NULL)
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		// msg header information
		pMsgConfigureStateforRTB->header.recipientId						= whoId_RTB;
		pMsgConfigureStateforRTB->header.msgSize							= sizeof(tdMsgConfigureState); // will be overwritten by RTBPA
		pMsgConfigureStateforRTB->header.dataId								= dataID_configureState;
		pMsgConfigureStateforRTB->header.msgCount							= 0;	// will be overwritten by RTBPA
		pMsgConfigureStateforRTB->header.issuedBy							= whoId_MB;
		// which state should be configured?
		pMsgConfigureStateforRTB->stateToConfigure							= stateToConfig;
		// configuration parameters of that state
		pMsgConfigureStateforRTB->actCtrl.BLPumpCtrl.direction				= PD_FORWARD; //Flow control NEVER switches to pump backwards!
		pMsgConfigureStateforRTB->actCtrl.BLPumpCtrl.flowReference			= pWAKDStateConfigurationParameters->defSpeedBp_mlPmin;
		pMsgConfigureStateforRTB->actCtrl.FLPumpCtrl.direction				= PD_FORWARD; //Flow control NEVER switches to pump backwards!
		pMsgConfigureStateforRTB->actCtrl.FLPumpCtrl.flowReference			= pWAKDStateConfigurationParameters->defSpeedFp_mlPmin;
		pMsgConfigureStateforRTB->actCtrl.PolarizationCtrl.direction		= pWAKDStateConfigurationParameters->direction;
		pMsgConfigureStateforRTB->actCtrl.PolarizationCtrl.voltageReference	= pWAKDStateConfigurationParameters->defPol_V;
		// Attach time to when this configuration was created. Maybe RTB is able to use this. Maybe this is redundant.
		pMsgConfigureStateforRTB->actCtrl.msOffset_ActCtrl					= 0;// NOT getTime()! this struct.member is not for the absolte time, but for an offset!

		// Sending the data to task RTBPA
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData 	= (void *) pMsgConfigureStateforRTB;

		// Our queue contains a pointer to state-configuration data. The memory was allocated here.
		// We forwarded this pointer to Dispatcher, to be forwarded to RTB which will free the memory when it is done with it.
		#if defined DEBUG_FC || defined SEQ0
			taskMessage("I", PREFIX, "Sending configuration for state: %s to RTB (and DS).", stateIdToString(stateToConfig));
		#endif
		if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'dataID_configureState'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
			err = 1;
		}
	}
	return(err);
}

uint8_t sendACK(tdMsgOnly * pMsgToAck, xQueueHandle *pQueueHandle)
{
	uint8_t err = 0;
	tdQtoken Qtoken;
	tdMsgOnly *pMsgAck = NULL;
	pMsgAck = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
	if (pMsgAck == NULL)
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		pMsgAck->header.dataId		= dataID_ack;
		pMsgAck->header.issuedBy	= pMsgToAck->header.recipientId;
		pMsgAck->header.recipientId	= pMsgToAck->header.issuedBy;
		pMsgAck->header.msgCount	= pMsgToAck->header.msgCount;
		pMsgAck->header.msgSize		= 6;
		// Sending the data to task Dispatcher to forward to original sender of msg (pMsgToNack->header.issuedBy)
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData 	= (void *) pMsgAck;
		#if defined DEBUG_FC || defined SEQ4
			taskMessage("I", PREFIX, "Sending ACK to: %s.", whoIdToString(pMsgAck->header.recipientId));
		#endif
		if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'dataID_ack'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
			err = 1;
		}
	}
	return(err);
}

uint8_t sendNACK(tdMsgOnly * pMsgToNack, xQueueHandle *pQueueHandle)
{
	uint8_t err = 0;
	tdQtoken Qtoken;
	tdMsgOnly *pMsgNack = NULL;
	pMsgNack = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
	if (pMsgNack == NULL)
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		pMsgNack->header.dataId			= dataID_nack;
		pMsgNack->header.issuedBy		= pMsgToNack->header.recipientId;
		pMsgNack->header.recipientId	= pMsgToNack->header.issuedBy;
		pMsgNack->header.msgCount		= pMsgToNack->header.msgCount;
		pMsgNack->header.msgSize		= 6;
		// Sending the data to task Dispatcher to forward to original sender of msg (pMsgToNack->header.issuedBy)
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData 	= (void *) pMsgNack;
		#if defined DEBUG_FC || defined SEQ4
			taskMessage("I", PREFIX, "Sending NACK to: %s.", whoIdToString(pMsgNack->header.recipientId));
		#endif
		if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'dataID_nack'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
			err = 1;
		}
	}
	return(err);
}

// *********forceChangeState**************************************** >>
uint8_t forceChangeState(tdWakdStates newState, xQueueHandle *pQueueHandleDISPin)
{
	// global WAKD state variables
	extern tdWakdStates 			staticBufferStateRTB;
	extern tdWakdOperationalState	staticBufferOperationalStateRTB;
	uint8_t err = 0;

	// If we are in "AllStopped" we are already as save as possible. No further state change needed
	if (staticBufferStateRTB != wakdStates_AllStopped) 
	{	// We are not in state AllStopped
		// If the new state is the same as the one we are already in, we do not need to do anything.
		if (staticBufferStateRTB != newState)
		{	// We need to change into the new state.
			#if defined DEBUG_FC
				taskMessage("I", PREFIX, "Due to ALARM changing from state '%s' to state '%s'.", stateIdToString(staticBufferStateRTB), stateIdToString(newState));
			#endif
			tdMsgCurrentWAKDstateIs  msgCurrentWAKDstateIs;
			msgCurrentWAKDstateIs.wakdStateIs= staticBufferStateRTB;
			msgCurrentWAKDstateIs.wakdOpStateIs= staticBufferOperationalStateRTB;
			while  ( staticBufferStateRTB != newState)
			{
				if (changeIntoStateRequest(newState, staticBufferOperationalStateRTB, &msgCurrentWAKDstateIs, pQueueHandleDISPin))
				{	// Changing WAKD state has failed!
					taskMessage("E", PREFIX, "Changing into new state has failed: %s.", stateIdToString(newState));
					errMsg(errmsg_ErrFlowControl);
				}
				// Give CPU free for other task. When we come back here, hopefully we have changed the state, or else yield again.
				//taskYIELD();
				vTaskDelay(1);	// wait for one Tick. Period of one tick defined by Tick HW-Counter. In our case 10 ms.
			} // of while
		}
	}
	return(err);
}
// *********forceChangeState**************************************** <<


// *********interpretSCCmessage**************************************** >>
uint8_t interpretSCCmessage (tdInfoMsg  sccMsg, xQueueHandle *pQueueHandleDISPin)
{
	// #if defined DEBUG_FC
	// taskMessage("I", "interpretSCCmessage() got sccMsg: %d", ((int)sccMsg));
	// #endif
	uint8_t err=0;
	switch (sccMsg) {
		case decTreeMsg_detectedBPsysAbsL: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "BP-sys lower than normal range");
			break;
		}
		case decTreeMsg_detectedBPsysAbsH: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "BP-sys higher than normal range");
			break;
		}
		case decTreeMsg_detectedBPsysAAbsH: {								
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: BP-sys high");
			break;
		}
		case decTreeMsg_detectedBPdiaAbsL: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "BP-dia lower than normal range");
			break;
		}
		case decTreeMsg_detectedBPdiaAbsH: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "BP-dia higher than normal range");
			break;
		}
		case decTreeMsg_detectedBPdiaAAbsH: {								
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: BP-dia high");
			break;
		}
		case dectreeMsg_detectedWghtAbsL: {									
			// taskMessage("I", msgDebug,  "Weight out of normal range(low)");
			break;
		}
		case dectreeMsg_detectedWghtAbsH: {									
			// taskMessage("I", msgDebug,  "Weight out of normal range(high)");
			break;
		}
		case dectreeMsg_detectedWghtTrdT: {									
			// taskMessage("I", msgDebug,  "weight trend exceeds normal range");
			break;
		}
		case dectreeMsg_detectedpumpBaccDevi: {						
			// taskMessage("I", msgDebug,  "Blood Pump Flow deviation");
			break;
		}
		case dectreeMsg_detectedpumpFaccDevi: {
			// taskMessage("I", msgDebug,  "Fluidic Pump Flow deviation");
			break;
		}
		case dectreeMsg_detectedBPorFPhigh: {
			// taskMessage("I", msgDoctor,  "ALARM: pressure (high) excess, all pumps stopped");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDebug,  "pressure too high, stop device here");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedBPorFPlow: {
			// taskMessage("I", msgDoctor,  "ALARM: pressure (high) excess, all pumps stopped");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDebug,  "pressure too low, stop device here");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedBTSoTH: {			
			// taskMessage("I", msgDebug,  "temperature (high) excess at BTSo, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSo, all pumps stopped");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedBTSoTL: {						
			// taskMessage("I", msgDebug,  "temperature (low) excess at BTSo, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSo, all pumps stopped");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedBTSiTH: {								
			// taskMessage("I", msgDebug,  "temperature (high) excess at BTSi, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSi, all pumps stopped");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedBTSiTL: {								
			// taskMessage("I", msgDebug,  "temperature (low) excess at BTSi, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSi, all pumps stopped");
			forceChangeState(wakdStates_AllStopped, pQueueHandleDISPin);
			break;
		}		
		case dectreeMsg_detectedSorDys: {	
			// taskMessage("I", msgDoctor, "Sorbend Dysfunction detected. Plasma pump stopped, blood pump still running");
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDebug, "stop Plasma pump and keep blood pump running");
			forceChangeState(wakdStates_NoDialysate, pQueueHandleDISPin);
			break;
		}			
		case dectreeMsg_detectedPhAAbsL: {						
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: pH low");
			break;
		}
		case dectreeMsg_detectedPhAAbsH: {						
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: pH high");
			break;
		}
		case dectreeMsg_detectedPhAbsLorPhAbsH: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "pH out of normal range");
			break;
		}
		case dectreeMsg_detectedPhTrdLPsT: {						
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "pH decrease out of normal range");
			break;
		}
		case dectreeMsg_detectedPhTrdHPsT: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "pH increase out of normal range");
			break;
		}
		case dectreeMsg_detectedNaAbsLorNaAbsH: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Na+ out of normal range. Plasma pump stopped, blood pump still running.");
			// taskMessage("I", msgDebug,   "Stop plasma pump.");
			forceChangeState(wakdStates_NoDialysate, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Na+ trend out of normal range. No change in device function was initiated.");
			break;
		}		
		case dectreeMsg_detectedKAbsLorKAbsHandSorDys: {								
			// taskMessage("I", msgPatient, "replace cartridge");
			break;
		}
		case dectreeMsg_detectedKAbsLorKAbsHnoSorDys: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "K+ out of normal range");
			break;
		}		
		case dectreeMsg_detectedKAAL: {						
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,   "K+ Alarm(low). Stopped plasma pump. Blood pump still runnning");
			// taskMessage("I", msgDebug,   "K+ Alarm(low). Stop plasma pump.");
			forceChangeState(wakdStates_NoDialysate, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedKAAHandKincrease: {						
			// taskMessage("I", msgPatient, "replace sorbent cartridge and contact physician");
			// taskMessage("I", msgDebug,   "K+ Alarm(high) < Kmeasafter");
			break;
		}
		case dectreeMsg_detectedKAAHnoKincrease: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDebug,   "K+ Alarm(high) >= Kmeasafter. Stop plasma pump.");
			// taskMessage("I", msgDoctor,   "K+ Alarm(high) and increasing behing the sorbend filter. Stopped plasma pump. Blood pump still runnning");
			forceChangeState(wakdStates_NoDialysate, pQueueHandleDISPin);
			break;
		}
		case dectreeMsg_detectedKTrdLPsT: {							
			// taskMessage("I", msgPatient, "contact physician");
			break;
		}	
		case dectreeMsg_detectedKTrdHPsT: {							
			// taskMessage("I", msgPatient, "contact physician");
			break;
		}		
		case dectreeMsg_detectedKTrdofnriandSorDys: {						
			// taskMessage("I", msgPatient, "replace cartridge");
			break;
		}
		
		case dectreeMsg_detectedPhTrdofnriandSorDys: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDebug,   "control pumprate to reach target range");
			break;
		}
		case dectreeMsg_detectedUreaAAbsHandSorDys: {						
			// taskMessage("I", msgPatient, "replace cartridge");
			break;
		}			
		case dectreeMsg_detectedUreaAAbsHnoSorDys: {						
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Urea out of normal range (high)");
			break;
		}	
		case dectreeMsg_detectedUreaTrdHPsT: {									
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Urea increase out of normal range (high)");
			break;
		}
		default:{
			err=1;
			taskMessage("E", PREFIX, "Default in interpretSCCmessage() / switch (sccMsg)was %d. This should not have happened!", ((int) sccMsg));
			break;		
		}
	}
	return(err);
}


