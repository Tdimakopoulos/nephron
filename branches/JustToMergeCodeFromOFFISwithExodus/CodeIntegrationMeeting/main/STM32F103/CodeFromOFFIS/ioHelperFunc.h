/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef IOHELPERFUNC_H
#define IOHELPERFUNC_H

#include <stdarg.h>

void printConfigureState(const char *pPrefix, tdMsgConfigureState *pConfigureState);

void printActuatorData(const char *pPrefix, tdActuatorData *pActuators);

void printPhysiologicalData(const char *pPrefix, tdPhysiologicalData *pPhysiologicalData);

void printPhysicalData(const char *pPrefix, tdPhysicalsensorData *pPhysicalData);

void printMsgWakdStateFromTo(tdMsgWakdStateFromTo *pMsgWakdStateFromTo);

void taskMessage(const char * severity, const char * pref, const char * message, ...);

void sendSystemInfo(const char *pPrefix, xQueueHandle *pQHDISPin, tdInfoMsg msgEnum);

void errMsg(tdErrMsg msg);

void errMsgISR(tdErrMsg msg);

void defaultCommandHandling(char *prefix, tdCommand command);

void defaultIdHandling(char *prefix, tdDataId dataId);

// My safer version of free() which assigns NULL to pointer after it was freed.
void sFree(void **vp);

char *opStateIdToString(tdWakdOperationalState opStateId);

void printfOpStateId(tdWakdOperationalState opStateId);

char *stateIdToString(tdWakdStates stateId);

char *errmsgToString(tdErrMsg errMsg);

char *infoMsgToString(tdInfoMsg enuMsg);

void printfStateId(tdWakdStates stateId);

char *whoIdToString(tdWhoId whoId);

void printfWhoId(tdWhoId whoId);

char *dataIdToString(tdDataId dataId);

void printfDataId(tdDataId dataId);

#endif
