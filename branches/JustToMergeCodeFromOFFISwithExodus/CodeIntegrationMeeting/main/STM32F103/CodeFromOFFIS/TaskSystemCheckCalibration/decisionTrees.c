/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "decisionTrees.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#define PREFIX "OFFIS FreeRTOS task SCC Decision Tree"

// **********************************************************************************************
// DISCLAIMER: all the used threshold values names are in										*
// accordance with the sensor naming as in the specification Document 'RTMCBSpecificationV01'	*
// **********************************************************************************************

/*/ &&&&&&&&&&&&&&&&&&&&&&&&&&&&&& The Following sensors have communications not yet specified... TODO &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	sensorType LeakDet,
	sensorType VertDefleciton,
	sensorType FexV,   sensorType FexV_set,
	sensorType BatStat,
// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&	
	if (LeakDet == 1) { // #57
		// taskMessage("I", msgDebug,  "Leak Detected -> Stop pumps here!");
		// taskMessage("I", msgPatient, "pumps stopped - contact physician");
		// taskMessage("I", msgDoctor,  "ALARM: Leakage detected");
		sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN57);
	}

	if (VertDefleciton>pParametersScc->VertDeflecitonT) { // #58
		// taskMessage("I", msgDebug,  "Device Tilted -> Stop pumps here!");
		// taskMessage("I", msgPatient, "pumps stopped - contact physician");
		// taskMessage("I", msgDoctor,  "ALARM: Device Tilted");
		sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN58);
	}
	
	// {  following skipped, since no excreation-amount-sensor in waistbag is there!
		// if ((FexV-FexV_set)*100/FexV_set < pParametersScc->FDpTL) { // #60
			//taskMessage("I", msgDebug,  "excreted Fluid too low");
			// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN60);
		// }

		// if ((FexV-FexV_set)*100/FexV_set > pParametersScc->FDpTH) { // #61
			//taskMessage("I", msgDoctor,  "excreted Fluid too high");
			// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN61);
		// }
	// // following skipped, since no excreation-amount-sensor in waistbag is there! END 
	// }
	if (BatStat<pParametersScc->BatStatT) { // #68
		// taskMessage("I", msgDebug,  "power loss, stop device here");
		// taskMessage("I", msgPatient, "power loss, contact physician");
		sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN68);
	}
// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&	 */

//################### decisionTrees_physical ############################################################################
// *pParametersScc		: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pPhysicalsensorData	: should include BPSo,BPSi,FPS1,FPS2,BTSo,BTSi measurements. 
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_physical should never be turned out, since they incorporate critical information (like over-pressure) that are always valid
// !!!! should be executed in high frequency, since short changes e.g. in VerticalDeflection might be critical, also considering the AirBubbles
//####################################################################################################################>>
void decisionTrees_physical(tdParametersSCC *pParametersScc, tdMsgPhysicalsensorData *pMsgPhysicalsensorData, xQueueHandle *pQHDISPin)
{
	#define msgPatient  "PATIENT"
	#define msgDoctor   "DOCTOR"
	#define msgDebug    "DEBUG"
	#define msgDetre    "DecisionTREE"
	
	// %*************************************************************************
	// %************************ here the decisions start************************
	
		// %#51 NA
		// %#54 NA
		#ifdef DEBUG_SCC
//			printPhysicalData(PREFIX, (tdPhysicalsensorData *)&(pMsgPhysicalsensorData->physicalsensorData));
			taskMessage("I", msgDetre, "PressureBCO: %f | BPSoTH: %f", (float)pMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure/((float)FIXPOINTSHIFT_PRESSURE), (float)pParametersScc->BPSoTH);
		#endif

		if (   ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				> ((float)pParametersScc->BPSoTH) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				> ((float)pParametersScc->BPSiTH) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureFCO.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				> ((float)pParametersScc->FPSoTH) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				> ((float)pParametersScc->FPSTH) ) { // #62
			// taskMessage("I", msgDebug,  "pressure too high, stop device here");
			#if defined DEBUG_SCC || defined SEQ11
				taskMessage("I", msgDetre,  "detected pressure-high excess, error messages should be displayed...");
			#endif
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBPorFPhigh);
		}
		
		if (   ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				< ((float)pParametersScc->BPSoTL) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				< ((float)pParametersScc->BPSiTL) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				< ((float)pParametersScc->FPSoTL) 
			|| ( ((float)pMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )
				< ((float)pParametersScc->FPSTL) ) { // #63
			// taskMessage("I", msgDebug,  "pressure too low, stop device here");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected pressure-low excess, error messages should be displayed...");
			#endif
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBPorFPlow);
		}

		if (( ((float)pMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.TemperatureOutlet_Value) / ((float)FIXPOINTSHIFT_OUTLET_TEMPERATURE)  )
			> (float)pParametersScc->BTSoTH) { // #64
			// taskMessage("I", msgDebug,  "temperature (high) excess at BTSo, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSo, all pumps stopped");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected BTSoTH excess because BTSo: %f. error messages should be displayed...", ( ((float)pMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.TemperatureOutlet_Value) / ((float)FIXPOINTSHIFT_OUTLET_TEMPERATURE)  ) );
			#endif				
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBTSoTH);
		}

		if (( ((float)pMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.TemperatureOutlet_Value) / ((float)FIXPOINTSHIFT_OUTLET_TEMPERATURE)  )
			< (float)pParametersScc->BTSoTL) { //#65
			// taskMessage("I", msgDebug,  "temperature (low) excess at BTSo, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSo, all pumps stopped");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected temperature-outlet-low excess, error messages should be displayed...");
			#endif
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBTSoTL);
		}

		if (( ((float)pMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.TemperatureInlet_Value) / ((float)FIXPOINTSHIFT_INLET_TEMPERATURE)  )
			> (float)pParametersScc->BTSiTH) { // #66
			// taskMessage("I", msgDebug,  "temperature (high) excess at BTSi, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSi, all pumps stopped");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected temperature-inlet-high excess, error messages should be displayed...");
			#endif
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBTSiTH);
		}

		if (( ((float)pMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.TemperatureInlet_Value) / ((float)FIXPOINTSHIFT_INLET_TEMPERATURE)  )
			< (float)pParametersScc->BTSiTL) { // #67
			// taskMessage("I", msgDebug,  "temperature (low) excess at BTSi, stop device here");
			// taskMessage("I", msgPatient, "pumps stopped - contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSi, all pumps stopped");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected temperature-inlet-low excess, error messages should be displayed...");
			#endif			
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedBTSiTL);
		}
}
//################### decisionTrees_physical ############################################################################<<


//################### decisionTrees_actuator ############################################################################
// *pParametersScc		: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pMsgActuatorData		: should include pumpBflow, pumpFflow, pumpBflow_set, pumpFflow_set, U_volt
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_actuator should never be turned out, since they presets of actuators shall always equal their actual values, this is always valid
//####################################################################################################################>>
void decisionTrees_actuator(tdParametersSCC *pParametersScc, tdMsgActuatorData *pMsgActuatorData, xQueueHandle *pQHDISPin)
{
	#ifdef DEBUG_SCC
		taskMessage("I", msgDetre, "BLPumpFlow: %f | BLPumpFlowRef: %f | pumpBaccDevi: %f", (float)pMsgActuatorData->actuatorData.BLPumpData.Flow/((float)FIXPOINTSHIFT_PUMP_FLOW), (float)pMsgActuatorData->actuatorData.BLPumpData.FlowReference/((float)FIXPOINTSHIFT_PUMP_FLOW), (float)pParametersScc->pumpBaccDevi);
	#endif
		
	if ( (fabs(((float)pMsgActuatorData->actuatorData.BLPumpData.Flow)
				- ((float)pMsgActuatorData->actuatorData.BLPumpData.FlowReference)) / ((float)FIXPOINTSHIFT_PUMP_FLOW) )
		>pParametersScc->pumpBaccDevi ) { // #56
		// taskMessage("I", msgDebug,  "Blood-Circle Pump Flow deviation");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedpumpBaccDevi);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pump threshold violation, error messages should be displayed...");
		#endif			
	}
	
	if ( (fabs(((float)pMsgActuatorData->actuatorData.FLPumpData.Flow)
				- ((float)pMsgActuatorData->actuatorData.FLPumpData.FlowReference)) / ((float)FIXPOINTSHIFT_PUMP_FLOW) )
		>pParametersScc->pumpFaccDevi ) { // #56b
		// taskMessage("I", msgDebug,  "Fluidic-Circle Pump Flow deviation");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedpumpFaccDevi);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pump threshold violation, error messages should be displayed...");
		#endif					
	}
	
	// NOTE: no real need to check the polariser, as it will be checked via the check of urea decomposition measured by chemical sensors!
}
//################### decisionTrees_actuator ############################################################################<<	


//################### decisionTrees_physiological ############################################################################
// *pParametersScc		: contains all the threshold values to be checked against
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// *pMsgPhysiologicalData: should include all actual measurements: c_Na, c_Na_after, c_K, c_K_after, c_Urea, c_Urea_after, Ph, Ph_after, 
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_physiological should should be able to be turned out when switching to a new operational-state (until the chemical sensors readings have stabilized)
//####################################################################################################################>>
void decisionTrees_physiological(	
	tdParametersSCC *pParametersScc,
	tdMsgPhysiologicalData *pMsgPhysiologicalData, 
	//sensorType c_Ca,   sensorType c_Ca_after,   sensorType c_Ca_past,    //This sensor will not be in the device
	//sensorType c_Crea, sensorType c_Crea_after, sensorType c_Crea_past, //This sensor will not be in the device
	//sensorType c_Phos, sensorType c_Phos_after, sensorType c_Phos_past, //This sensor will not be in the device
	//sensorType c_HCO3, sensorType c_HCO3_after, sensorType c_HCO3_past, //This sensor will not be in the device
	//sensorType Ph_past,												  //This sensor will not be in the device
	sensorType pumpFflow,
	sensorType U_volt,
	// sensorType det_S1, sensorType det_S2, sensorType det_S3, //decided to be skipped for now, see explanation below!
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin
){
	// Keep in mind:
	// 1: for Sorbent dysfunciton: the flow of the time of chemical measurement is needed	
	// 2: we need here signed float! CaTrd=c_Ca_past-c_Ca; %increase: positive decrease:negative
	
	// %*************************************************************************
	// %************************ here the decisions start************************
	
	// #65 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% checking the funciton of the adsorption filter*****************>>
		//***************************************************************************************************************************>>
		// calculating rC_(substance) the expected adsorption rate is calculated - and compared afterwards to the actual measured rate.
		// These following two are the obsolete equations!! and should be updated inthe end
		sensorType rC_K = pow((float)pParametersScc->P_K, ( (((float)pParametersScc->cap_K -0) * ((float)pParametersScc->Fref_K) ) / (((float)pParametersScc->cap_K) * ((float)pumpFflow)) )); 		// rC= Cout/Cin
		// sensorType rC_Ph= pow(pParametersScc->P_Ph,( ((pParametersScc->cap_Ph-0) * (pParametersScc->Fref_Ph)) / ((pParametersScc->cap_Ph) * (pumpFflow)) ));		// no phos sensor
	
		sensorType RRUrea = ( ((float)U_volt) - 1.6) * ((float)pParametersScc->A_dm2) * 6.7;
		sensorType dC= RRUrea*1000/(24*60* ((float)pumpFflow)); 							// = Cdi � Cdo
		// calculating rC_(substance) the expected adsorption rate is calculated - and compared afterwards to the actual measured rate. END
		//***************************************************************************************************************************<<
	
	
		sensorType ADR_K_exp    = 1-rC_K; 		// expected Adsorption Rates due to WAKD-presets (like flow, voltage...)
		//sensorType ADR_Phos_exp = 1-rC_Ph; 	// no phos sensor
		sensorType ADR_Ur_exp   = dC / ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea / (float)FIXPOINTSHIFT_UREA);
		
		sensorType NaAdr=-ADR_K_exp; // respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
		sensorType KAdr=ADR_K_exp;
		sensorType UrAdr=ADR_Ur_exp;
		//sensorType PhosAdr=ADR_Phos_exp; // no phos sensor
		
		// calculate  relative adsorption rates
		sensorType NaAdrAct= ( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium /(float)FIXPOINTSHIFT_NA) 
								- ((float)pMsgPhysiologicalData->physiologicalData.ECPDataO.Sodium/(float)FIXPOINTSHIFT_NA) )
									/((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium/(float)FIXPOINTSHIFT_NA);
		sensorType KAdrAct    = (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium /(float)FIXPOINTSHIFT_K)
								- ((float)pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium/(float)FIXPOINTSHIFT_K) )
									/((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K);
		// sensorType CaAdrAct   = (c_Ca-c_Ca_after)/c_Ca; //This sensor will not be in the device
		sensorType UrAdrAct   = ( (pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea/ (float)FIXPOINTSHIFT_UREA)
									- (pMsgPhysiologicalData->physiologicalData.ECPDataO.Urea/ (float)FIXPOINTSHIFT_UREA) )
								/ (pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea/ (float)FIXPOINTSHIFT_UREA);
		// sensorType CreaAdrAct = (c_Crea-c_Crea_after)/c_Crea; //This sensor will not be in the device
		//sensorType PhosAdrAct = (c_Phos-c_Phos_after)/c_Phos;  //This sensor will not be in the device
		//sensorType HCO3AdrAct = (c_HCO3-c_HCO3_after)/c_HCO3;  //This sensor will not be in the device
		// #65
		
		// NaAdr(...) are the expected adsorption rates, NaAdrAct(...) are the actual adsorptin rates: calcualted above!
		if (NaAdrAct<NaAdr*0.1 || KAdrAct<KAdr*0.1 || UrAdrAct<UrAdr*0.1) {
			*pSorDys=1; // this is set regarding Na, K, Urea. (for the other chemicals no sensors expected)
			taskMessage("I", msgDebug, "non-fuzzy adsorption filter rating exceeds thresold");
			taskMessage("I", msgDebug, "stop Plasma pump and keep blood pump running");
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected sorbend disfunction, error messages should be displayed...");
			#endif						
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedSorDys);
		}
	// #65 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% checking the funciton of the adsorption filter*****************<<end
		
		// #65 but fuzzy %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% refer to D6.1 for explanation of fuzzy Adsorption Filter Observer!*******>>
		sensorType ADR_rel_Na=NaAdrAct/NaAdr;
		sensorType ADR_rel_K=KAdrAct/KAdr;
		//sensorType ADR_rel_Ca=CaAdrAct/pParametersScc->CaAdr;  //This sensor will not be in the device
		sensorType ADR_rel_Ur=UrAdrAct/UrAdr;
		//sensorType ADR_rel_Crea=CreaAdrAct/pParametersScc->CreaAdr; 	//This sensor will not be in the device
		//sensorType ADR_rel_Phos=PhosAdrAct/PhosAdr; 					//This sensor will not be in the device
		//sensorType ADR_rel_HCO3=HCO3AdrAct/pParametersScc->HCO3Adr;	//This sensor will not be in the device

		sensorType msp_Na = 0;
		if (ADR_rel_Na<0.3)	msp_Na = -500*ADR_rel_Na+150; // calculate 'Membership' based on relative-Arsorption rate: 0.3->0 ... 0.2->50 ... 0.1->100
		if (msp_Na>1) msp_Na=1;
		if (msp_Na<0) msp_Na=0;
		
		sensorType msp_K=0;
		if (ADR_rel_K<0.3) msp_K=-500*ADR_rel_K+150;
		if (msp_K>1) msp_K=1;
		if (msp_K<0) msp_K=0;
		
		// sensorType msp_Ca=0;  //This sensor will not be in the device
		// if (ADR_rel_Ca<0.3) msp_Ca=-500*ADR_rel_Ca+150;
		// if (msp_Ca>1) msp_Ca=1;
		// if (msp_Ca<0) msp_Ca=0;
		
		sensorType msp_Ur=0;
		if (ADR_rel_Ur<0.3) msp_Ur=-500*ADR_rel_Ur+150;
		if (msp_Ur>1) msp_Ur=1;
		if (msp_Ur<0) msp_Ur=0;
		
		{   // following skipped, since it was decided to remove the Crea-sensor
			// sensorType msp_Crea=0;  //This sensor will not be in the device
			// if (ADR_rel_Crea<0.3) msp_Crea=-500*ADR_rel_Crea+150;
			// if (msp_Crea>1) msp_Crea=1;
			// if (msp_Crea<0) msp_Crea=0;
			
			// sensorType msp_Phos=0;  //This sensor will not be in the device
			// if (ADR_rel_Phos<0.3) msp_Phos=-500*ADR_rel_Phos+150;
			// if (msp_Phos>1) msp_Phos=1;
			// if (msp_Phos<0) msp_Phos=0;

			// sensorType msp_HCO3=0;  //This sensor will not be in the device
			// if (ADR_rel_HCO3<0.3) msp_HCO3=-500*ADR_rel_HCO3+150;
			// if (msp_HCO3>1) msp_HCO3=1;
			// if (msp_HCO3<0) msp_HCO3=0;
		}
		
		sensorType msp_combi = (
			msp_Na  *pParametersScc->wgtNa   + 
			msp_K   *pParametersScc->wgtK    + 
			//msp_Ca  *pParametersScc->wgtCa   + 
			msp_Ur  *pParametersScc->wgtUr   
			//msp_Crea*pParametersScc->wgtCrea + 
			//msp_Phos*pParametersScc->wgtPhos + 
			//msp_HCO3*pParametersScc->wgtHCO3)/ (pParametersScc->wgtNa+pParametersScc->wgtK+pParametersScc->wgtCa+pParametersScc->wgtUr+pParametersScc->wgtCrea+pParametersScc->wgtPhos+pParametersScc->wgtHCO3);
				/ (pParametersScc->wgtNa+pParametersScc->wgtK+pParametersScc->wgtUr));
				
		if (msp_combi>pParametersScc->mspT) {
			taskMessage("I", msgDebug, "fuzzy adsorption filter rating exceeds thresold");
			taskMessage("E", PREFIX,   "!IMPLEMENT THIS FUNCTIONALITY!");
		}
		// #65 but fuzzy %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% refer to D6.1 for explanation of fuzzy Adsorption Filter Observer!*******<< end
		
		if (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.pH/ ((float)FIXPOINTSHIFT_PH)) 
				< pParametersScc->PhAAbsL) { // #40
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: pH low");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhAAbsL);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected pH threshold violation, error messages should be displayed...");
			#endif
		}
		if (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.pH/ ((float)FIXPOINTSHIFT_PH))
				> pParametersScc->PhAAbsH) { // #41
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "ALARM: pH high");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhAAbsH);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected pH threshold violation, error messages should be displayed...");
			#endif					
		}
		if ( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.pH/ ((float)FIXPOINTSHIFT_PH))
				< pParametersScc->PhAbsL
			|| ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.pH/ ((float)FIXPOINTSHIFT_PH))
				> pParametersScc->PhAbsH) { // #42
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "pH out of normal range");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhAbsLorPhAbsH);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected pH threshold violation, error messages should be displayed...");
			#endif			
		}


		if (( (pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium/(float)FIXPOINTSHIFT_NA)
				< pParametersScc->NaAbsL) 
			|| ((pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium/(float)FIXPOINTSHIFT_NA)
				> pParametersScc->NaAbsH)) { // #1
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Na+ out of normal range. Plasma pump stopped.");
			// taskMessage("I", msgDebug,   "Stop plasma pump.");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedNaAbsLorNaAbsH);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected Na+ threshold violation, error messages should be displayed...");
			#endif			
		}


		if((( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				< pParametersScc->KAbsL) 
			|| (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
					> pParametersScc->KAbsH)) 
				&& (*pSorDys) == 1) { // #3
			// taskMessage("I", msgPatient, "replace cartridge");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKAbsLorKAbsHandSorDys);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected K+ threshold violation, error messages should be displayed...");
			#endif					
		}

		if(( (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				< pParametersScc->KAbsL)
			|| (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
					> pParametersScc->KAbsH) )
			&& (*pSorDys) == 0) { // #4
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "K+ out of normal range");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKAbsLorKAbsHnoSorDys);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected K+ threshold violation, error messages should be displayed...");
			#endif				
		}

		if((((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
			< pParametersScc->KAAL)) { // #7
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDebug,   "K+ Alarm(low)");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKAAL);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected K+ threshold violation, error messages should be displayed...");
			#endif				
		}

		if ( (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				> pParametersScc->KAAH) 
			&& (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				< ((float)pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium/(float)FIXPOINTSHIFT_K) )) { // #8
			// taskMessage("I", msgPatient, "replace sorbent cartridge and contact physician");
			// taskMessage("I", msgDebug,   "K+ Alarm(high) < Kmeasafter");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKAAHandKincrease);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected K+ threshold violation, error messages should be displayed...");
			#endif				
		}

		if ( (((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				> pParametersScc->KAAH) 
			&& ( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium/(float)FIXPOINTSHIFT_K)
				>= ((float)pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium/(float)FIXPOINTSHIFT_K) )) { // #9
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDebug,   "K+ Alarm(high) >= Kmeasafter");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKAAHnoKincrease);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected K+ threshold violation, error messages should be displayed...");
			#endif				
		}



		{	/* following skipped, since it was decided to remove the Ca-sensor
			// CaTrdofnrd will be used by #12 and #13/#14
			unsigned char CaTrdofnrd = 0;
			// CaTrd will be used by #19 and #20
			sensorType CaTrd = c_Ca_past-c_Ca; // increase: positive decrease:negative
			if ((CaTrd/pParametersScc->CaTrdT) < pParametersScc->CaTrdLPsT) CaTrdofnrd=1;

			if (CaTrdofnrd == 1 && (KTrdofnrd == 0 && KTrdofnri == 0)) { // #18
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Ca2+ trend out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN18);
			}

			// CaTrdofnri will be used by #12 and #13/#14 too
			//unsigned char CaTrdofnri = 0; //-> no CAsensor
			if ((CaTrd/pParametersScc->CaTrdT) > pParametersScc->CaTrdHPsT) { // #20
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Ca2+ increase > normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN20);
				CaTrdofnri = 1;
			}
			// following skipped, since it was decided to remove the Ca-sensor END*/
		}


		{   // The following cases will never occur, as no Ca sensor is available
			//if(KTrdofnri == 1 && (*pSorDys) == 0 && PhTrdofnrd == 0 && (CaTrdofnrd == 1 || CaTrdofnri == 1)) { // #12
				// taskMessage("I", msgDebug,   "control pumprate to reach target range");
			//	sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN12); 
			// } 

			//if(KTrdofnri == 1 && (*pSorDys) == 0 && (CaTrdofnrd == 1 || CaTrdofnri == 1)) { // #14
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDebug,   "control pumprate to reach target range");
			//	sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN14);
			//}
		}
		
		{	// // following skipped, since it was decided to remove the Ca-sensor
			// if (c_Ca < pParametersScc->CaAAbsL) { // #15
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "ALARM:�Ca2+ low");
				// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN15);
			// }
			// if (c_Ca > pParametersScc->CaAAbsH) { // #16
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "ALARM:�Ca2+ high");
				// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN16);
			// }
			// if ((c_Ca < pParametersScc->CaAbsL || c_Ca > pParametersScc->CaAbsH) && (*pSorDys) == 1) { // #16 (2nd)
				// taskMessage("I", msgPatient, "replace cartridge");
				// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN16b);
			// }
			// if ((c_Ca < pParametersScc->CaAbsL || c_Ca > pParametersScc->CaAbsH) && (*pSorDys) == 0) { // #17
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Ca2+ out of normal range");
				// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN17);
			// }

			// if (CaTrdofnrd == 1 && KTrdofnri == 1) { // #19
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Ca2+ decrease > normal range�and K+ trend out of normal range");
				// sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN19);
			// }
			// // following skipped, since it was decided to remove the Ca-sensor END 
		}
		
		if ( ( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea) / ((float)FIXPOINTSHIFT_UREA) )
				> pParametersScc->UreaAAbsH && (*pSorDys) == 1) { // #21
			// taskMessage("I", msgPatient, "replace cartridge");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedUreaAAbsHandSorDys);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected Urea threshold violation, error messages should be displayed...");
			#endif				
		}

		if ( ( ((float)pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea) / ((float)FIXPOINTSHIFT_UREA) )
				> pParametersScc->UreaAAbsH && (*pSorDys) == 0) { // #22
			// taskMessage("I", msgPatient, "contact physician");
			// taskMessage("I", msgDoctor,  "Urea out of normal range (high)");
			sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedUreaAAbsHnoSorDys);
			#ifdef DEBUG_SCC
				taskMessage("I", msgDetre,  "detected Urea threshold violation, error messages should be displayed...");
			#endif							
		}
		

		
		{	/* following skipped, since it was decided to remove the Crea-sensor
			if (((c_Crea < pParametersScc->CreaAbsL) || (c_Crea > pParametersScc->CreaAbsH)) && (*pSorDys) == 1) { // #24
				// taskMessage("I", msgPatient, "replace cartridge");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN24);
			}

			if (((c_Crea < pParametersScc->CreaAbsL) || (c_Crea > pParametersScc->CreaAbsH)) && (*pSorDys) == 0) { // #25
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "creatinine out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN25);
			}

			sensorType CreaTrd = c_Crea_past-c_Crea; // increase: positive decrease:negative
			if ((CreaTrd / pParametersScc->CreaTrdT) > pParametersScc->CreaTrdHPsT) { // #26
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Creatinine trend out of normal range (high)");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN26);
			}
			// following skipped, since it was decided to remove the Crea-sensor*/
			// will be used by #27, #28 and #32
			/* following skipped, since it was decided to remove the Phos-sensor
			unsigned char Phosofnrl = 0;
			if(c_Phos < pParametersScc->PhosAbsL) Phosofnrl = 1;

			unsigned char Phosofnri = 0;
			if (c_Phos > pParametersScc->PhosAbsH) Phosofnri = 1;
		
			if (Phosofnrl == 1 && (*pSorDys) == 1) { // # 27
				// taskMessage("I", msgPatient, "replace cartridge");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN27);
			}

			if (Phosofnrl == 1 && (*pSorDys) == 0) { // #28
				// taskMessage("I", msgPatient, "advice: phosphate intake");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN28);
			}

			if (Phosofnri == 1 && (*pSorDys) == 1) { // #29
				// taskMessage("I", msgPatient, "replace cartridge");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN29);
			}

			if (Phosofnri == 1 && (*pSorDys) == 0) { // #30
				// taskMessage("I", msgDebug,   "Phosphate > normal range and no sorbent disfunction");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN30);
			}

			if (c_Phos > pParametersScc->PhosAAbsH) { // #31
				// taskMessage("I", msgPatient, "phosphate binders intake if problem persists contact physician");
				// taskMessage("I", msgDoctor,  "phosphate absolute out of alarm range (high)");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN31);
			}

			sensorType PhosTrd = c_Phos_past-c_Phos; // increase: positive decrease:negative
			if (((PhosTrd / pParametersScc->PhosTrdT) < pParametersScc->PhosTrdLPsT) && Phosofnrl == 1) { // #32
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "Phosphate decrease out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN32);
			}

			if (((PhosTrd / pParametersScc->PhosTrdT) > pParametersScc->PhosTrdHPsT) && Phosofnri == 1) { // #33
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "msgDoctor, 'Phosphate increase out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN33);
			}
			// following skipped, since it was decided to remove the Phos-sensor END*/
			/* following skipped, since it was decided to remove the HCO3-sensor
			if (c_HCO3 < pParametersScc->HCO3AAbsL && (*pSorDys) == 1) { // #34
				// taskMessage("I", msgPatient, "replace cartridge");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN34);
			}
			if (c_HCO3 < pParametersScc->HCO3AAbsL && (*pSorDys) == 0) { // #35
				// taskMessage("I", msgPatient, "replace cartridge");
				// taskMessage("I", msgDoctor,  "ALARM:�HCO3 low");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN35);
			}

			if ((c_HCO3 < pParametersScc->HCO3AbsL || c_HCO3 > pParametersScc->HCO3AbsH) && (*pSorDys) == 1) { // #36
				// taskMessage("I", msgPatient, "replace cartridge");
				// taskMessage("I", PREFIX,     "Sending out Qtoken Msg#36");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN36);
			}

			if ((c_HCO3 < pParametersScc->HCO3AbsL || c_HCO3 > pParametersScc->HCO3AbsH) && (*pSorDys) == 0) { // #37
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "HCO3 out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN37);
			}

			sensorType HCO3Trd = c_HCO3_past-c_HCO3; // increase: positive decrease:negative
			if ((HCO3Trd / pParametersScc->HCO3TrdT) < pParametersScc->HCO3TrdLPsT) { // #38
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "HCO3 decrease out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN38);
			}
			if ((HCO3Trd / pParametersScc->HCO3TrdT) > pParametersScc->HCO3TrdHPsT) { // #39
				// taskMessage("I", msgPatient, "contact physician");
				// taskMessage("I", msgDoctor,  "HCO3 increase out of normal range");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN39);
			}
			// following skipped, since it was decided to remove the HCO3-sensor */ 
		}

		{ /* following was decided to be skipped, since no experimental chemical-sensor data have been available to adjust the sensor-failure-detection-algorithm untile the delivery of this code
			if (det_S1 == 1 || det_S2 == 1 || det_S3 == 1) { // #69
				// taskMessage("I", msgDoctor,  "sensor malfunction detected");
				// taskMessage("I", msgPatient, "contact physician");
				sendDecissionTreeMsg(pQtoken, pQHDISPin, command_MsgN69);
			} */
		}
}
//################### decisionTrees_physiological ############################################################################<<



//################### decisionTrees_KTrnd ############################################################################
// quite the same as decisionTrees_physiological but with trend analysis!
//####################################################################################################################>>
void decisionTrees_KTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin
){
	
	sensorType KTrd = ((float)conc_past->Potassium /(float)FIXPOINTSHIFT_K) 
						- ((float)(*valueNow) / (float)FIXPOINTSHIFT_K); // increase: positive decrease:negative
	unsigned char KTrdofnrd = 0;
	if( pParametersScc->KTrdLPsT!=0 && ((KTrd / pParametersScc->KTrdT) < pParametersScc->KTrdLPsT) ) { // #10
		// taskMessage("I", msgPatient, "contact physician");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKTrdLPsT);
		KTrdofnrd = 1; // means: K trend out of normal range decrease
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected negative K+ trend excess, error messages should be displayed...");
		#endif			
	}
	
	unsigned char KTrdofnri = 0;
	if ( pParametersScc->KTrdHPsT!=0 && ((KTrd / pParametersScc->KTrdT) > pParametersScc->KTrdHPsT)) KTrdofnri = 1;
		
	if(KTrdofnri == 1 && (*pSorDys) == 1) { // #11
		// taskMessage("I", msgPatient, "replace cartridge");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedKTrdofnriandSorDys);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected negative K+ trend excess and sorbend disfunction, error messages should be displayed...");
		#endif			
	}

}
//################### decisionTrees_KTrnd ############################################################################<<

//################### decisionTrees_NaTrnd ############################################################################
// quite the same as decisionTrees_physiological but with trend analysis!
//####################################################################################################################>>
void decisionTrees_NaTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin
){
	sensorType NaTrd = ((float)conc_past->Sodium /(float)FIXPOINTSHIFT_NA) 
						- ((float)(*valueNow) / (float)FIXPOINTSHIFT_NA); // increase: positive decrease:negative
	unsigned char NaTrdofnr = 0;
	if( (pParametersScc->NaTrdLPsT!=0 && (NaTrd / pParametersScc->NaTrdT) < pParametersScc->NaTrdLPsT )
		|| (pParametersScc->NaTrdHPsT!=0 && (NaTrd / pParametersScc->NaTrdT) < pParametersScc->NaTrdHPsT )
		) { // #10
			NaTrdofnr =1;
		// taskMessage("I", msgPatient, "contact physician");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected Na+ trend excess, error messages should be displayed...");
		#endif			
	}
}
//################### decisionTrees_KTrnd ############################################################################<<


//################### decisionTrees_PhTrnd ############################################################################
// quite the same as decisionTrees_physiological but with trend analysis!
//####################################################################################################################>>
void decisionTrees_PhTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow,
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin
){
	
	sensorType PhTrd = ((float)conc_past->pH/ ((float)FIXPOINTSHIFT_PH))
						- ((float) (*valueNow) / (float)FIXPOINTSHIFT_PH); // increase: positive decrease:negative
	unsigned char PhTrdofnrd = 0;
	if ( pParametersScc->PhTrdLPsT!=0 && (PhTrd / pParametersScc->PhTrdT) < pParametersScc->PhTrdLPsT) { // #43
		PhTrdofnrd = 1;
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "pH decrease out of normal range");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhTrdLPsT);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected negative pH trend excess, error messages should be displayed...");
		#endif		
	}
	if ( pParametersScc->PhTrdHPsT!=0 && (PhTrd / pParametersScc->PhTrdT) > pParametersScc->PhTrdHPsT) { // #43b
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "pH increase out of normal range");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhTrdHPsT);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected negative pH trend excess, error messages should be displayed...");
		#endif				
	}
	
	if(PhTrdofnrd == 1 && (*pSorDys) == 0 ) { // #13
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDebug,   "control pumprate to reach target range");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedPhTrdofnriandSorDys);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected pH trend excess and sorbend disfunction, error messages should be displayed...");
		#endif				
	}
	
}
//################### decisionTrees_PhTrnd ############################################################################<<


//################### decisionTrees_UrTrnd ############################################################################
// quite the same as decisionTrees_physiological but with trend analysis!
//####################################################################################################################>>
void decisionTrees_UrTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow,
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin
){	
	sensorType UreaTrd = (conc_past->Urea/ (float)FIXPOINTSHIFT_UREA)
		- ((float)(*valueNow) / (float)FIXPOINTSHIFT_UREA) ; // increase: positive decrease:negative
	if ( pParametersScc->UreaTrdHPsT!=0 && ((UreaTrd / pParametersScc->UreaTrdT) > pParametersScc->UreaTrdHPsT)) { // #23
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "Urea increase out of normal range (high)");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedUreaTrdHPsT);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected urea trend excess, error messages should be displayed...");
		#endif			
	}
}
//################### decisionTrees_UrTrnd ############################################################################<<



//################### decisionTrees_Wght ############################################################################
// *pParametersScc	: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pMsgWeightData Wght : should contian: Wght, Wght_past
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_Wght should never be turned out, since they incorporate information not dependent on any WAKD operation, that are always valid
void decisionTrees_Wght(	
	tdParametersSCC *pParametersScc, 
	float *weightNow,
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin
){ 
	if ( *weightNow < pParametersScc->WghtAbsL) { // #52
		// taskMessage("I", msgDebug,  "Weight out of normal range(low)");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedWghtAbsL);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected weight low excess, error messages should be displayed...");
		#endif			
	}

	if ( *weightNow > pParametersScc->WghtAbsH) { // #53
		// taskMessage("I", msgDebug,  "Weight out of normal range(high)");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedWghtAbsH);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected weight high excess, error messages should be displayed...");
		#endif					
	}
}	
//################### decisionTrees_Wght ############################################################################<<


//################### decisionTrees_WghtTrnd ############################################################################
// *pParametersScc	: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pMsgWeightData Wght : should contian: Wght, Wght_past
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_Wght should never be turned out, since they incorporate information not dependent on any WAKD operation, that are always valid
void decisionTrees_WghtTrnd(	
	tdParametersSCC *pParametersScc, 
	float *weightNow,
	float *weightOld,
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin
){
	// sensorType WghtTrd = ((float)pMsgOLDWeightData->weightData.Weight/ (float)FIXPOINTSHIFT_WEIGHTSCALE)
		// - ((float)pMsgWeightData->weightData.Weight/ (float)FIXPOINTSHIFT_WEIGHTSCALE); // increase: positive decrease:negative
	sensorType WghtTrd = weightNow - weightOld;
	if (pParametersScc->WghtTrdT && (fabs(WghtTrd) > pParametersScc->WghtTrdT)) { // #59
		// taskMessage("I", msgDebug,  "weight trend exceeds normal range");
		sendSystemInfo(PREFIX, pQHDISPin, dectreeMsg_detectedWghtTrdT);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected weight trend excess, error messages should be displayed...");
		#endif		
	}
}
//################### decisionTrees_WghtTrnd ############################################################################<<


//################### decisionTrees_ECG ############################################################################
// *pParametersScc	: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pMsgECGData		: should contian heartRate
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_ECG should never be turned out, since they incorporate information not dependent on any WAKD operation, that are always valid
// void decisionTrees_ECG(	
	// tdParametersSCC *pParametersScc,
	// tdMsgEcgData *pMsgECGData,
	// //required API information to be able to sent out queue messages
	// tdQtoken *pQtoken,
	// xQueueHandle *pQHDISPin
// ){
	// It was decided that the ECG - watching should be handeled by the smartphone exclusively, as the amount of data is to high ti be handeled by the limited ressources of the WAKD
	
		// if (ECGArr == 1) { // #55	//ECGArr the smart-shirt indicates if it detected an arrithia by itself
		//	 // taskMessage("I", msgPatient, "ALARM!");
		//	// taskMessage("I", msgPatient, "contact physician");
		//	// taskMessage("I", msgDoctor,  "ALARM: Arrythmia detected");
		// sendDecissionTreeMsg(pQtoken, pQHDISPin, dectreeMsg_detectedECGArr);
		// }
// }
//################### decisionTrees_ECG ############################################################################<<


//################### decisionTrees_BP ############################################################################
// *pParametersScc		: contains all the threshold values to be checked against. 
// 						  Hand over this pointer with distinct parametrization for the WAKD-state we are currently in.
// pMsgBPData...		: should include all actual measurements: BPsys, BPdia
//						  IMPORTANT typecast these values into float (sensortype) here 
// ******************************************************************************************************************
// decisionTrees_BP should never be turned out, since they incorporate information not dependent on any WAKD operation, that are always valid
void decisionTrees_BP(	
	tdParametersSCC *pParametersScc, 
	tdMsgBpData *pMsgBPData,
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin
){

	if ( ((float)pMsgBPData->bpData.systolic / (float)FIXPOINTSHIFT_BPRESSURE )
		< pParametersScc->BPsysAbsL) { //	#44
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "BP-sys lower than normal range");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPsysAbsL);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif					
	}

	if (((float)pMsgBPData->bpData.systolic / (float)FIXPOINTSHIFT_BPRESSURE )
		> pParametersScc->BPsysAbsH) { // #45
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "BP-sys higher than normal range");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPsysAbsH);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif				
	}

	if (((float)pMsgBPData->bpData.systolic / (float)FIXPOINTSHIFT_BPRESSURE )
		> pParametersScc->BPsysAAbsH) { // #46
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "ALARM: BP-sys high");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPsysAAbsH);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif				
	}

	// %#47 NA

	if (((float)pMsgBPData->bpData.diastolic / (float)FIXPOINTSHIFT_BPRESSURE )
		< pParametersScc->BPdiaAbsL) { // #48
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "BP-dia lower than normal range");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPdiaAbsL);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif				
	}

	if (((float)pMsgBPData->bpData.diastolic / (float)FIXPOINTSHIFT_BPRESSURE )
		> pParametersScc->BPdiaAbsH) { // #49
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "BP-dia higher than normal range");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPdiaAbsH);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif				
	}

	if (((float)pMsgBPData->bpData.diastolic / (float)FIXPOINTSHIFT_BPRESSURE )
		> pParametersScc->BPdiaAAbsH) { // #50
		// taskMessage("I", msgPatient, "contact physician");
		// taskMessage("I", msgDoctor,  "ALARM: BP-dia high");
		sendSystemInfo(PREFIX, pQHDISPin, decTreeMsg_detectedBPdiaAAbsH);
		#ifdef DEBUG_SCC
			taskMessage("I", msgDetre,  "detected some blood pressure threshold excess, error messages should be displayed...");
		#endif				
	}
}
//################### decisionTrees_BP ############################################################################<<

