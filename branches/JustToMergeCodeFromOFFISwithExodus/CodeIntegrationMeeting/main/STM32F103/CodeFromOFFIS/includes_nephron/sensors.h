/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef SENSORS_H
#define SENSORS_H 

// The types "Ctrl" are used to send control parameters to actuators.
// Used to configure the states in the RTB. Compare with "dataID_ConfigureState"
typedef struct strPolarizationCtrl
{
    uint8_t			direction;			// 1=(+), 2=(-)
    uint16_t		voltageReference;	// controlled voltage ??????? in steps of 0.1 Volts ????????
} tdPolarizationCtrl;

#define FIXPOINTSHIFT_POLARIZ_VOLT		10

// Types "Data" are used to read the status from actuators.
typedef struct strPolarizationData
{
    uint8_t			SwitchONnOFF;					// 0=OFF, 1=ON
    uint8_t			Direction;						// 1=(+), 2=(-)
    tdMsTimeType	msOffset_Direction;			// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
    uint16_t		VoltageReference;				// controlled voltage ??????? in steps of 0.1 Volts ????????
    tdMsTimeType	msOffset_VoltageReference;		// relative time value counting ms!
    uint16_t		Voltage;						// measured voltage ??????? in steps of 0.1 Volts ???????? FIXPOINTSHIFT_POLARIZ_VOLT
    tdMsTimeType	msOffset_Voltage;				// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
} tdPolarizationData;

// Fluidic switches are only controlled by RTB. So "Ctrl" structure is not defined for SW Interface.
// Only this "Data" structure is defined to read out the status from Switch Actuators.
typedef struct strMultiSwitchData
{		
    uint8_t			SwitchONnOFF;					// 0=OFF, 1=ON
    uint8_t			Position;						// ??????? to be defined ???????
    tdMsTimeType	msOffset_Position;				// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
} tdMultiSwitchData;

// Write to pumps
typedef struct strPumpCtrl
{
    uint8_t			direction;						// 1=(+), 2=(-)
    uint16_t		flowReference;					// *1 ml/min
} tdPumpCtrl;

#define FIXPOINTSHIFT_PUMP_SPEED	1
#define FIXPOINTSHIFT_PUMP_FLOW		1

// Read from pumps.
typedef struct strPumpData
{
    uint8_t			SwitchONnOFF;					// 0=OFF, 1=ON
    uint8_t			Direction;						// 1=(+), 2=(-)
    tdMsTimeType	msOffset_Direction;			// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
    uint16_t		Speed;							// ??????? to be defined ???????
    tdMsTimeType	msOffset_Speed;				// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
    uint16_t		FlowReference;					// *1 ml/min
    tdMsTimeType	msOffset_FlowReference;		// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
    uint16_t		Flow;							// *1 ml/min
    tdMsTimeType	msOffset_Flow;					// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
    uint16_t		Current;						// ??????? to be defined ??????? *1 mA ??
    tdMsTimeType	msOffset_Current;				// relative time value counting ms! Absolut Time is part of "tdPhysicalData"
} tdPumpData;

// If you change any of the values below, make sure to also change the "gain" attached to
// it in the simulink model. Why is this? Here the story:
// The transfer of values from Simulink to the OVP-Simlink-Interface is Uns32 based. So, if
// we want to receive a double value like 123.45678 we do e.g. a mult 10000 there to
// 1234567.8. After the transfer Simlink gets 1234567. We have to make sure to correct this by
// a division of 10000 resulting to 123.4567
// So 10000 gives us 4 decimal places. 10 would give us one: 123.4
// BE AWARE Uns32 Bit means largest range is (2^32)-1. So your simulation must not generate larger
// numbers than this!
// Physiological Sensors
#define FIXPOINTSHIFT_NA			10
#define FIXPOINTSHIFT_K				10
#define FIXPOINTSHIFT_PHOSPHATE		10
#define FIXPOINTSHIFT_PH			10
#define FIXPOINTSHIFT_UREA			10
#define FIXPOINTSHIFT_CREATININE	10
#define FIXPOINTSHIFT_TEMPERATURE	10

// Sensor data received from one Electro Chemical Board
typedef struct strECPData
{
	uint16_t		Sodium;				// OFFIS is using *0.1 mmol/L [FIXPOINTSHIFT_NA] -> 1420 is 142 mmol/L
    tdMsTimeType	msOffset_Sodium;	// relative time value counting ms! Absolut Time is part of "tdPhysiologicalData"
	uint16_t		Potassium;			// OFFIS is using *0.1 mmol/L [FIXPOINTSHIFT_K] -> 1420 is 142 mmol/L
    tdMsTimeType	msOffset_Potassium;
	uint16_t		pH;					// OFFIS is using *0.1 [FIXPOINTSHIFT_PH] -> 1420 is 142
    tdMsTimeType	msOffset_pH;
	uint16_t		Urea;				// OFFIS is using *0.1 mmol/L [FIXPOINTSHIFT_UREA] -> 1420 is 142 mmol/L
    tdMsTimeType	msOffset_Urea;
	uint16_t		Temperature;		// 0.1 x -> 370 is 37�C [FIXPOINTSHIFT_TEMPERATURE]
    tdMsTimeType	msOffset_Temperature;
	// uint16_t		Phosphate;			// OFFIS is using *0.1 mmol/L [FIXPOINTSHIFT_PHOSPHATE] -> 1420 is 142 mmol/L
    // tdMsTimeType	TimeStamp_Phosphate;
	// uint16_t		Creatinine;			// OFFIS is using *0.1 mmol/L [FIXPOINTSHIFT_CREATININE] -> 1420 is 142 mmol/L
    // tdMsTimeType	TimeStamp_Creatinine;
} tdECPData;

#define FIXPOINTSHIFT_PRESSURE	1

typedef struct strPressureSensorData
{
    int16_t			Pressure;			// *1 mmHG
    tdMsTimeType	msOffset_Pressure;		
} tdPressureSensorData;

#define FIXPOINTSHIFT_WEIGHTSCALE 10
// Read from weight scale
typedef struct strWeightMeasure {
	uint16_t		Weight;    		// weight in 0.1 kg  as in FIXPOINTSHIFT_WEIGHTSCALE
	uint8_t			BodyFat;   		// body fat percentage in %
	uint16_t		WSBatteryLevel; // ????? percentage in *1 % ?????
	uint32_t		WSStatus;		// !!!!!!!!!!!!!!!!!!!!!!!!!!!to be defined??
	tdUnixTimeType	TimeStamp;
} tdWeightMeasure;

// Data from ECG
typedef struct strEcgData
{
	uint8_t			heartRate;
	uint8_t			respirationRate;
	tdUnixTimeType	TimeStamp;
} tdEcgData;

// Data from blood pressure sensor
// NEEDS TO BE DEFINED AND CORRECTED BY IMST. FOLLOWING JUST A DUMMY IDEA.
#define FIXPOINTSHIFT_BPRESSURE	1
typedef struct strBpData
{
	uint8_t			systolic;
	uint8_t			diastolic;
	uint8_t			heartRate;
	tdUnixTimeType	TimeStamp;
} tdBpData;

#define FIXPOINTSHIFT_INLET_TEMPERATURE 10
#define FIXPOINTSHIFT_OUTLET_TEMPERATURE 10

typedef struct strTemperatureSensorData
{	
    uint16_t		TemperatureInlet_Value;		// 0.1 x -> 370 is 37�C
    uint16_t		TemperatureOutlet_Value;	// 0.1 x -> 370 is 37�C
    tdMsTimeType	msOffset_Temperature;		
} tdTemperatureSensorData;

#define FIXPOINTSHIFT_DCS_CONDUCT_FCR		10
#define FIXPOINTSHIFT_DCS_CONDUCT_FCQ		10
#define FIXPOINTSHIFT_DCS_CONDUCT_PT1000	10

typedef struct strConductivitySensorData
{
    uint16_t	 	Cond_FCR;					// OFFIS is using *0.1 [FIXPOINTSHIFT_DCS_CONDUCT_FCR] -> 1420 is 142
    uint16_t 		Cond_FCQ;					// OFFIS is using *0.1 [FIXPOINTSHIFT_DCS_CONDUCT_FCQ] -> 1420 is 142
    uint16_t 		Cond_PT1000;				// OFFIS is using *0.1 [FIXPOINTSHIFT_DCS_CONDUCT_PT1000] -> 1420 is 142
    tdMsTimeType	msOffset_Cond;
} tdConductivitySensorData;

typedef struct strActCtrl
{
    tdPumpCtrl			BLPumpCtrl;			
    tdPumpCtrl			FLPumpCtrl;			
	tdPolarizationCtrl	PolarizationCtrl;
	tdMsTimeType		msOffset_ActCtrl;
} tdActCtrl;

#endif
