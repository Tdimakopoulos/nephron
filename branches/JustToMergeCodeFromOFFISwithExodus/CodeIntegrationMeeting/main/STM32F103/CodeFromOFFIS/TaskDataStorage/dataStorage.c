/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifdef LINUX
	#include "dataStorage.h"
	#include "unified_write.h"
	#include "unified_read.h"
	#include "unified_delete.h"
	#include "getphysiopatientdata.h"
	#include "filenames.h"
	#include "typedefsNephron.h"
	#include "enums.h"
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "dataStorage.h"
	#include "fileIO\\unified_write.h"
	#include "fileIO\\unified_read.h"
	#include "fileIO\\unified_delete.h"
	#include "fileIO\\getphysiopatientdata.h"
	#include "fileIO\\filenames.h"
	#include "..\\typedefsNephron.h"
	#include "..\\includes_nephron/enums.h"
	#include "..\\nephron.h"
#endif


#define PREFIX "OFFIS FreeRTOS task DS"

uint8_t heartBeatDS = 0;

void tskDS( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting Data Storage task!");

	xQueueHandle *qhDISPin  = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhFCin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhFCin;
	xQueueHandle *qhDSin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDSin;
	xQueueHandle *qhSCCin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhSCCin;
  
	// xTaskHandle *handleSCC  = ((tdAllHandles *) pvParameters)->allTaskHandles.handleSCC;
	// xTaskHandle *handleFC	= ((tdAllHandles *) pvParameters)->allTaskHandles.handleFC;
  
 	tdQtoken Qtoken;
	Qtoken.command = command_DefaultError;
	Qtoken.pData   = NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif
	

	
	// these removes are here, because error occurs, when trying to add new data to files, caused by 'rename' function
	#ifndef VP_SIMULATION
		#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
	#endif
#if defined VP_SIMULATION

	deleteFiles(FILE_PHYSIOLOGICALDATA, 0, 10);
	deleteFiles(FILE_PHYSICALDATA, 0, 10);
	deleteFiles(FILE_ACTUATORDATA, 0, 10);
	deleteFiles(FILE_WEIGHTDATA, 0, 10);
	deleteFiles(FILE_BPDATA, 0, 10);
	deleteFiles(FILE_ECGDATA, 0, 10);	
	deleteFiles(FILE_DEVICESSTATUS, 0, 10);	
	deleteFiles(FILE_MSGCONFIGURESTATE, 0, 10);		
	taskMessage("I", PREFIX, "ALL DATA-STORAGE FILES FROM PREVIOUS SIMULATION RUN DELETED ");	

#endif
	for( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every DS_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatDS++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		
		if ( xQueueReceive( qhDSin, &Qtoken, DS_PERIOD) == pdPASS ) {
			#ifdef DEBUG_DS
				taskMessage("I", PREFIX, "Data with id %d in queue.", Qtoken.command);
			#endif

			switch (Qtoken.command) {
				case command_DefaultError: {
					// Misused pointer must not be interpreted as such. Cast back to original type.
					tdErrMsg err = (tdErrMsg) Qtoken.pData;
					taskMessage("I", PREFIX, "Received system error msg: %d", err);
					// Now handle (save) this error message. Needs to be implemented.
					#ifndef VP_SIMULATION
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
					#endif
					break;
				}
				case command_UseAtachedMsgHeader:{
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case dataID_physiologicalData: {
							#if defined SEQ6
								printPhysiologicalData(PREFIX, &(((tdMsgPhysiologicalData *) Qtoken.pData)->physiologicalData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "physiological Data received. Storing.");
							#endif
							unified_write(dataID_physiologicalData, &(((tdMsgPhysiologicalData *) Qtoken.pData)->physiologicalData));
							sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_physicalData: {
							#if defined DEBUG_DS || defined SEQ7
								printPhysicalData(PREFIX, &(((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "physical Data timestamp %d received. Storing.", ((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData.TimeStamp);
							#endif
							unified_write(dataID_physicalData, &(((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData));
							sFree((void *)&(Qtoken.pData));
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "DONE: storing physical Data.");
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}		
						case dataID_actuatorData: {
							#if defined SEQ8
								printActuatorData(PREFIX, &(((tdMsgActuatorData *) (Qtoken.pData))->actuatorData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "actuator Data received. Storing.");
							#endif
							unified_write(dataID_actuatorData, &(((tdMsgActuatorData *) (Qtoken.pData))->actuatorData));
							sFree((void *)&(Qtoken.pData));
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "DONE: storing actuator Data.");
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}						
						case (dataID_weightDataOK):{
							// This weight measure was checked by the patient. Store the validated measurement.
							#if defined DEBUG_DS || defined SEQ3
								taskMessage("I", PREFIX, "Storing weight measure: %f", ( ((double)(((tdMsgWeightData *)(Qtoken.pData))->weightData.Weight))/FIXPOINTSHIFT_WEIGHTSCALE) );
							#endif
							unified_write(dataID_weightDataOK, &(((tdMsgWeightData *) Qtoken.pData)->weightData));
							sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_bpData: {
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "BP Data received. Storing.");
							#endif
							unified_write(dataID_bpData, &(((tdMsgBpData *) Qtoken.pData)->bpData));
							sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_EcgData: {
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "ECG Data received. Storing.");
							#endif
							unified_write(dataID_EcgData, &(((tdMsgEcgData *) Qtoken.pData)->ecgData));
							sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}		
						case dataID_statusRTB:{
							tdMsgDeviceStatus *pMsgDeviceStatus  = NULL;
							pMsgDeviceStatus  = (tdMsgDeviceStatus *) (Qtoken.pData);
							// Our queue contains a pointer to status data. The memory was allocated by RTBPA
							#if defined DEBUG_DS || defined SEQ7
								taskMessage("I", PREFIX, "Status Data timestamp %d received. Storing.", ((tdMsgDeviceStatus *) Qtoken.pData)->devicesStatus.TimeStamp) ;
							#endif
							unified_write(dataID_statusRTB, &(((tdMsgDeviceStatus *) Qtoken.pData)->devicesStatus));
							sFree((void *)&(Qtoken.pData));
							#if defined DEBUG_DS || defined SEQ7
								taskMessage("I", PREFIX, "DONE: storing Status Data.") ;
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							
							break;
						}		
						
						case dataID_configureStateDump: {
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "dataID_configureStateDump Data received. Storing.");
							#endif
							unified_write(dataID_configureStateDump, ((tdMsgConfigureState *) Qtoken.pData) );
							sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_PatientProfile: {
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "Patient Profile Data received. Storing.");
							#endif
							unified_write(dataID_PatientProfile, &(((tdMsgPatientProfileData *) Qtoken.pData)->patientProfile));
							sFree((void *)&(Qtoken.pData));
							
			taskMessage("F", PREFIX, "we should send command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_PatientProfile to FC to make the new setting work! but not yet implemented.");
			
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_WAKDAllStateConfigure: {
						
			taskMessage("F", PREFIX, "we should be storing dataID_WAKDAllStateConfigure here (e.g. the SP has sent a new parametrization), but not yet implemented.");
							
							//unified_write(dataID_WAKDAllStateConfigure, &((tdWAKDAllStateConfigure *) Qtoken.pData) );
			taskMessage("F", PREFIX, "we should send command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_configureState to FC to make the new setting work! but not yet implemented.");
							// we need to make sure, that command_ConfigurationChanged arrives and FC or SCC reconfigures - can we do this with the reminder-task? It could wait for 2 seconds to recieve a new command_FC reconfigured - if not, then re-send command_ConfigurationChanged again!
							// -> ggf. in die beschreibung der Architektur mir aufnehmen!
							
							sFree((void *)&(Qtoken.pData));						
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_configureState: {
						
			taskMessage("F", PREFIX, "we should be storing dataID_configureState here (e.g. the SP has sent a new parametrization), but not yet implemented.");
			
							//unified_write(dataID_configureState, &((tdWAKDStateConfigurationParameters *) Qtoken.pData) );
			taskMessage("F", PREFIX, "we should send command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_configureState to FC to make the new setting work! but not yet implemented.");
							sFree((void *)&(Qtoken.pData));						
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_AllStatesParametersSCC: {
						
			taskMessage("F", PREFIX, "we should be storing dataID_AllStatesParametersSCC here (e.g. the SP has sent a new parametrization), but not yet implemented.");
			
							//unified_write(dataID_AllStatesParametersSCC, &((tdAllStatesParametersSCC *) Qtoken.pData) );
			taskMessage("F", PREFIX, "we should send command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_StateParametersSCC to FC to make the new setting work! but not yet implemented.");
			
							sFree((void *)&(Qtoken.pData));						
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}				
						case dataID_StateParametersSCC: {
						
			taskMessage("F", PREFIX, "we should be storing dataID_StateParametersSCC here (e.g. the SP has sent a new parametrization), but not yet implemented.");
			
							//unified_write(dataID_StateParametersSCC, &((tdParametersSCC *) Qtoken.pData) );
			
			taskMessage("F", PREFIX, "we should send command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_StateParametersSCC to FC to make the new setting work! but not yet implemented.");
			
							sFree((void *)&(Qtoken.pData));						
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif
							break;
						}
						case (dataID_shutdown): // fall through, continue with reset.
						case (dataID_reset):{
						#if defined DEBUG_CBPA || defined SEQ0
							taskMessage("I", PREFIX, "Shutdown of task.");
						#endif
						// Do here whatever needs to be done for shutting down.
						((tdAllHandles *) pvParameters)->allTaskHandles.handleDS = NULL;
						vTaskDelete(NULL);	// Shut down and wait forever.
						vTaskSuspend(NULL); // Never run again (until after reset)
						}
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				case command_RequestDatafromDS: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){				
						case dataID_physiologicalData: {
							unified_read(dataID_physiologicalData, &(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData));
							respondRequestDatafromDS(&dataId,  qhDISPin);							
							break;
						}
						case dataID_physicalData: {
							unified_read(dataID_physicalData, &(((tdMsgPhysicalsensorData *)Qtoken.pData)->physicalsensorData));
							// respondRequestDatafromDS(&dataId,  qhDISPin);
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading dataID_physicalData complete");
							#endif
							break;
						}
						case dataID_actuatorData: {
							unified_read(dataID_actuatorData, &(((tdMsgActuatorData *)Qtoken.pData)->actuatorData));
							// if wanted respondRequestDatafromDS(&dataId,  qhDISPin); might be done here
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading dataID_actuatorData complete.");
							#endif
							break;
						}	
						case (dataID_weightDataOK):{
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "Request for dataID_weightDataOK recieved");
							#endif
							//((tdMsgWeightData *)Qtoken.pData)->weightData.Weight = 99;
							//((tdMsgWeightData *)Qtoken.pData)->weightData.TimeStamp
							int readouterr = unified_read(dataID_weightDataOK, &(((tdMsgWeightData *)Qtoken.pData)->weightData));
							// vTaskResume(handleSCC); //we need to wake up task SCC again.... 
							// #ifdef DEBUG_DS
								// taskMessage("I", PREFIX, "Reading dataID_weightDataOK complete");
							// #endif
							if ( ((tdMsgWeightData *)Qtoken.pData)->weightData.Weight >0 
									&& readouterr==0) { // only if data from the time requested or older was found
								taskMessage("I", PREFIX, "Weigth %d read out for time %d",
									((tdMsgWeightData *)Qtoken.pData)->weightData.Weight, ((tdMsgWeightData *)Qtoken.pData)->weightData.TimeStamp);

								//if there was found a valid weight, do respondRequestDatafromDS so SCC will perform Trend-decision-Trees
								respondRequestDatafromDS(&dataId, qhDISPin);
							}
							break;
						}
						case dataID_bpData: {
							if(!unified_read(dataID_bpData, &(((tdMsgBpData *)Qtoken.pData)->bpData)))
							{
								// if wanted respondRequestDatafromDS(&dataId,  qhDISPin); might be done here
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_bpData complete.");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_bpData failed!");
								#endif
							}
							break;
						}
						case dataID_EcgData: {
							if(!unified_read(dataID_EcgData, &(((tdMsgEcgData *)Qtoken.pData)->ecgData)))
							{
								// if wanted respondRequestDatafromDS(&dataId,  qhDISPin); might be done here
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_EcgData complete");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_EcgData failed!");
								#endif
							}
							break;
						}
						case dataID_statusRTB: {
							if(!unified_read(dataID_statusRTB, &(((tdMsgDeviceStatus *)Qtoken.pData)->devicesStatus) ))
							{
								// if wanted respondRequestDatafromDS(&dataId,  qhDISPin); might be done here
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_statusRTB complete");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_statusRTB failed!");
								#endif
							}
							break;
						}
						case dataID_PatientProfile: { 
							if(!unified_read(dataID_PatientProfile, &(((tdMsgPatientProfileData *)Qtoken.pData)->patientProfile)))
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_PatientProfile %s from file complete",(((tdMsgPatientProfileData *)Qtoken.pData)->patientProfile.name) );
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_PatientProfile failed!");
								#endif
							}
							break;
						}
						case dataID_WAKDAllStateConfigure: {
						
			taskMessage("F", PREFIX, "we should be reding out dataID_configureState (e.g. request by user/smpartphone) ALTERNATIVELY TO THE RECONFIGURE CALL here, but not yet implemented.");
						/*	if(!unified_read(dataID_WAKDAllStateConfigure, &(((tdWAKDAllStateConfigure *)Qtoken.pData)) ) )
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_WAKDAllStateConfigure %s from file complete");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_WAKDAllStateConfigure failed!");
								#endif
							}
						*/	break;
						}							
						
						case dataID_configureState: {
						//tdWAKDStateConfigurationParameters
			taskMessage("F", PREFIX, "we should be reding out dataID_configureState (e.g. request by user/smpartphone) ALTERNATIVELY TO THE RECONFIGURE CALL here, but not yet implemented.");
			
							break;
						}	
						case dataID_AllStatesParametersSCC: {
						//tdAllStatesParametersSCC
			taskMessage("F", PREFIX, "we should be reding out dataID_AllStatesParametersSCC (e.g. request by user/smpartphone) ALTERNATIVELY TO THE RECONFIGURE CALL here, but not yet implemented.");
			
							break;
						}
						case dataID_StateParametersSCC: {
						//tdParametersSCC
			taskMessage("F", PREFIX, "we should be reding out dataID_StateParametersSCC (e.g. request by user/smpartphone) ALTERNATIVELY TO THE RECONFIGURE CALL here, but not yet implemented.");
			
							break;
						}						
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}				
					}
					break;
				}
				case command_InitializeFC: {
				
					tdFCConfigPointerStruct *pFCConfigPointerStruct = ((tdFCConfigPointerStruct *)(Qtoken.pData));
					#if defined DEBUG_DS || defined SEQ0
						taskMessage("I", PREFIX, "Request to initialize FC. Reading %s parameters from SD Card.", stateIdToString((pFCConfigPointerStruct->pWAKDStateConfigure)->thisState)  );
					#endif
					
					// The FC task sent this token and with it in pData the pointer of its own data structure
					// where it expects the data to be written to. So DO NOT use vPortFree on this pointer!
					if(!unified_read(dataID_PatientProfile,pFCConfigPointerStruct->pPatientProfile))
					{
						#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "Reading in dataID_PatientProfile %s from file complete",(pFCConfigPointerStruct->pPatientProfile)->name );
						#endif
						if(!unified_read(dataID_configureState, ((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure))
						{
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading in of dataID_configureState %s complete", stateIdToString( (pFCConfigPointerStruct->pWAKDStateConfigure)->thisState) );
								taskMessage("I", PREFIX, "Duration preset in state: %i", (pFCConfigPointerStruct->pWAKDStateConfigure)->duration_sec );
							#endif
							#if defined DEBUG_DS || defined SEQ0 || defined DEBUG_FC
								taskMessage("I", PREFIX, "FC was initialized. Notifying task FC.");
							#endif
							
						
							Qtoken.pData = (void *) pFCConfigPointerStruct; // send back the same pointer we got from FC to FC (FC has no local copy!)!
							Qtoken.command = command_InitializeFCReady;
							
							if ( xQueueSend( qhFCin, &Qtoken, DS_BLOCKING) != pdPASS )
							{	// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of FC did not accept 'command_RequestDatafromDSReady'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
								sFree(&(Qtoken.pData));
							};
							// sending command_RequestDatafromDSReady  END
						} else { // Errors occurred during read of dataID_configureState
							taskMessage("E", PREFIX, "Errors occurred during read of dataID_WAKDAllStateConfigure");
							errMsg(errmsg_SDcardError);
						}
					} else { // Errors occurred during read of dataID_PatientProfile
						taskMessage("E", PREFIX, "Errors occurred during read of dataID_PatientProfile");
						errMsg(errmsg_SDcardError);
					}
					#ifndef VP_SIMULATION
						// HANDLE CASE INITILIZATION FAILS
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
					#endif
					break;
				}
				case command_InitializeSCC: {
					#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Request to initialize SCC. Reading parameters from SD Card.");
					#endif	
					tdParametersSCC *pParametersSCC = (tdParametersSCC *)Qtoken.pData;
					if(!unified_read(dataID_StateParametersSCC, pParametersSCC))
					{
						#if defined DEBUG_DS || defined DEBUG_SCC
							taskMessage("I", PREFIX, "Reading in of dataID_StateParametersSCC complete. Notifying task SCC.");
						#endif
						// sending command_InitializeSCCReady, to inform that new configuration was read out and is available by FC
						// Qtoken.pData = (void *) pMsgForSCC;
						Qtoken.pData = (void *) pParametersSCC; // send back the very same pointer (command for clarification!)
						Qtoken.command = command_InitializeSCCReady;
						if ( xQueueSend( qhSCCin, &Qtoken, DS_BLOCKING) != pdPASS )
						{	// Seemingly the queue is full for too long time. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of SCC did not accept 'command_InitializeSCCReady'!");
							errMsg(errmsg_QueueOfFlowControlTaskFull);
							sFree(&(Qtoken.pData));
						};
						// sending command_InitializeSCCReady  END
					} else { // Errors occurred during read
						taskMessage("E", PREFIX, "Errors occurred during read of dataID_StateParametersSCC");
						errMsg(errmsg_SDcardError);
					}
					#ifndef VP_SIMULATION
						// HANDLE CASE INITILIZATION FAILS
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
					#endif
					break;
				}
				case command_getPhysioPatientData: {
					tdDataId dataId = ((tdMsgPhysiologicalData *)(Qtoken.pData))->header.dataId;	
					#if defined DEBUG_DS
						taskMessage("I", PREFIX, "Attempt to read out PhysioPatientData with Timestamp %i", (((tdMsgPhysiologicalData *)(Qtoken.pData))->physiologicalData.TimeStamp) );
					#endif
					if (((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp >= 1) {
						if(!getPhysioPatientData( &(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData), 120)) // last atribute is duraiton of UF-state (2*60=120seconds)
						// we only see 'real' patient's values, when the UF state lasted enough long time!
						{
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading in of command_getPhysioPatientData complete");
							#endif
							
							//if there was found a valid weight, do respondRequestDatafromDS so SCC will perform Trend-decision-Trees
							respondRequestDatafromDS(&dataId, qhDISPin);
							
						} else { // Errors occurred during read
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Not possible to read desired dataID_physiologicalUFData. Indicating by setting timestamp '0' ");
							#endif
							((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp =0;
							// errMsg(errmsg_SDcardError); no errMSG here! probably just the requested data is not in the file - thats ok! jsut no trend-observation will be done
						}
					} else {
						taskMessage("I", PREFIX, "Not possible to read out dataID_physiologicalUFData with Timestamp < 1");
						((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp =0;
						errMsg(errmsg_SDcardError);
					}
					#ifndef VP_SIMULATION
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
					#endif

					break;
				}
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
		} else {
			taskMessage("E", PREFIX, "Did not get anything to do for a loooong time. This is most likely faulty behavior!");
		}
	}
}

uint8_t respondRequestDatafromDS(tdDataId *dataId2get, xQueueHandle *pQH)
{
	tdQtoken Qtoken;
	Qtoken.command = command_RequestDatafromDSReady;
	uint8_t err = 1;

	tdMsgSystemInfo *pMsgRespondRequest = NULL; // fetch an old measurement form Data Storage
	pMsgRespondRequest = (tdMsgSystemInfo *) pvPortMalloc(sizeof(tdMsgSystemInfo));		
	if ( pMsgRespondRequest == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "respondRequestDatafromDS: Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	} else {
		pMsgRespondRequest->header.recipientId = whoId_MB;
		pMsgRespondRequest->header.msgSize	= sizeof(tdMsgSystemInfo);
		pMsgRespondRequest->header.dataId = *dataId2get;
		pMsgRespondRequest->header.msgCount	= 0; // not defined; unused
		pMsgRespondRequest->header.issuedBy	= whoId_MB;	
		pMsgRespondRequest->msgEnum = command_RequestDatafromDSReady;
		
		Qtoken.pData	= (void *) pMsgRespondRequest;
		#if defined DEBUG_DS
			taskMessage("I", PREFIX, "Requested for data form DS processed. Sending command_RequestDatafromDSReady.");
		#endif
		if ( xQueueSend( pQH, &Qtoken, DS_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Data Storage Message is lost!!!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pMsgRespondRequest);
			err = 1;
		} else {
			err = 0;
		}

	} // of if ( pMsgRespondRequest == NULL )
	
	return (err);
}

