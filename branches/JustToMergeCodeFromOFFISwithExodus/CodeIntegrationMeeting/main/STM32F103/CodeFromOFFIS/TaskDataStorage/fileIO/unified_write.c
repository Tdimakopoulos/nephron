 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>

#include <errno.h>

#include "filenames.h"

#ifdef LINUX
	#ifdef __arm__
		#include "nephron.h"
	#endif
	#include "interfaces.h"
	#include "enums.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#ifdef __arm__
		#include "..\\..\\nephron.h"
	#endif
	#include "..\\..\\includes_nephron\\interfaces.h"
	#include "..\\..\\includes_nephron\\enums.h"
#endif
// for testing purposes
//#ifdef __arm__
//	#include "csvparselib/pstdint.h"
//	#include "..\\..\\nephron.h"
//	#include "..\\..\\typedefsNephron.h"
//	#include "..\\..\\includes_nephron\\enums.h"
//#else
//	#include <stdint.h>
//	//#define taskMessage printf
//	#include "..\interfaces.h"
//	#include "..\typedefsNephron.h"
//	#include "..\enums.h"
//#endif

#define PREFIX "OFFIS FreeRTOS task DS - CSV write"

#ifdef LINUX
	#include "csvparselib.h"
	#include "unified_write.h"
	#include "unified_read.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "csvparselib/csvparselib.h"
	#include "unified_write.h"
	#include "unified_read.h"
#endif

//#include "typedefsNephron.h"

// just for testing purposes
/*
int main()
{
	tdWAKDAllStateConfigure db;
	db.
	defSpeedBp_mlPmin;
	defSpeedFp_mlPmin;
	defPol_V;
	direction;
	duration_sec;
	//printf("%d",unified_write(3,&db));
	return 0;
}
*/

//! Writes the values contained in \a db to a given file. If you don't pass the matching \a datatype, behavior will be undefined. 
/*!
  Behaviour is dependent on the passed datatype.
  For a \a datatype which uses timestamps:
	  You need to fill your \a db struct with a valid timestamp and values before passing it.
	  Set them to \a SELECT_NOT_FLT for float values or \a SELECT_NOT_INT for int32 values if you DON'T want to write that value now.
	  If you don't want to, the function will write \a VALUE_MAX_CHARS whitespaces instead.
	  Behaviour will be undefined if you do this with timestamp, so please don't. All timestamps in the file
	  (if the file isn't empty though) need to be in descending order, which they are if you don't mess it up manually.
	  If the file doesn't exists, it will be created with an corresponding header.
	  So there are 3 cases what this function could do if you call it:
	  First case: Your timestamp is greater than all timestamps which already exist in the file. The new timestamp and all
	  corresponding values will be appended to the end. This is the easiest case.
	  Second case: Your timestamp is smaller than a timestamp which already exists in the file. A new file will be created,
	  (oldfilename + .tmp) all data before the point where the new line should be added will be written into the .tmp file. The new
	  line and after that all data after the point where the new line should be added will be written too into the .tmp file. Now we can
	  delete the old file and rename the .tmp to the old file.
	  Third case: Your timestamp already exists in the file. You have to make sure by yourself that you only write values which haven't
	  be written before. Your new values will be inserted in the free whitespaces of the matching timestamp.
  For other \a datatype without timestamps::
	  Just fill your \a db with data. Old files will be overwritten so be careful.
  Common informations:
	WARNING: Due use of fseek etc. which limits to 2147483647 bytes , this WON'T WORK ON FILES > 2GB!
  \param datatype defines which struct is contained in \a db
  \param db will be casted to the datatype passed by \a datatype
  \return 1 on errors, 0 otherwise
  \sa unified_read
*/
uint8_t unified_write(tdDataId datatype, void* db)
{
	char filename[FILENAME_LENGTH];
	FILE *pFile = NULL;
	uint32_t lastLF = 0;
	uint8_t firstLine = 1; // boolean to check if we read in only one line so far
	uint8_t seekResult = 0;
	uint8_t writeLineafterHeader = 0;
	char dataBuffer [DBUFFER_CSVLINE_SIZE];
	uint32_t cur_timestamp = ULONG_MAX;
	#ifdef __arm__
		tdUnixTimeType bufferedTime = getTime();
	#else
		tdUnixTimeType bufferedTime = 0;
	#endif
	
	char * ptr = 0;
	char * end = 0;
	uint8_t erroroccured = 0;
#if defined VP_SIMULATION
	switch(datatype)
	{
		case dataID_physiologicalData:
		{
			strncpy(filename,FILE_PHYSIOLOGICALDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_physiologicalData,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;currentState;ECPDataI.Sodium;ECPDataI.msOffset_Sodium;ECPDataO.Sodium;ECPDataO.msOffset_Sodium;ECPDataI.Potassium;ECPDataI.msOffset_Potassium;ECPDataO.Potassium;ECPDataO.msOffset_Potassium;ECPDataI.Urea;ECPDataI.msOffset_Urea;ECPDataO.Urea;ECPDataO.msOffset_Urea;ECPDataI.pH;ECPDataI.msOffset_pH;ECPDataO.pH;ECPDataO.msOffset_pH;ECPDataI.Temperature;ECPDataI.msOffset_Temperature;ECPDataO.Temperature;ECPDataO.msOffset_Temperature;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_physicalData:
		{
			strncpy(filename,FILE_PHYSICALDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_physicalData,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;PressureFCI.Pressure;PressureFCI.msOffset_Pressure;PressureFCO.Pressure;PressureFCO.msOffset_Pressure;PressureBCI.Pressure;PressureBCI.msOffset_Pressure;PressureBCO.Pressure;PressureBCO.msOffset_Pressure;TemperatureInOut.TemperatureInlet_Value;TemperatureInOut.TemperatureOutlet_Value;TemperatureInOut.msOffset_Temperature;CSENS1Data.Cond_FCR;CSENS1Data.Cond_FCQ;CSENS1Data.Cond_PT1000;CSENS1Data.msOffset_Cond;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_actuatorData:
		{
			strncpy(filename,FILE_ACTUATORDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_actuatorData,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;MultiSwitchBIData.SwitchONnOFF;MultiSwitchBIData.Position;MultiSwitchBIData.msOffset_Position;MultiSwitchBOData.SwitchONnOFF;MultiSwitchBOData.Position;MultiSwitchBOData.msOffset_Position;MultiSwitchBUData.SwitchONnOFF;MultiSwitchBUData.Position;MultiSwitchBUData.msOffset_Position;BLPumpData.SwitchONnOFF;BLPumpData.Direction;BLPumpData.msOffset_Direction;BLPumpData.Speed;BLPumpData.msOffset_Speed;BLPumpData.FlowReference;BLPumpData.msOffset_FlowReference;BLPumpData.Flow;BLPumpData.msOffset_Flow;BLPumpData.Current;BLPumpData.msOffset_Current;FLPumpData.SwitchONnOFF;FLPumpData.Direction;FLPumpData.msOffset_Direction;FLPumpData.Speed;FLPumpData.msOffset_Speed;FLPumpData.FlowReference;FLPumpData.msOffset_FlowReference;FLPumpData.Flow;FLPumpData.msOffset_Flow;FLPumpData.Current;FLPumpData.msOffset_Current;PolarizationData.SwitchONnOFF;PolarizationData.Direction;PolarizationData.msOffset_Direction;PolarizationData.VoltageReference;PolarizationData.msOffset_VoltageReference;PolarizationData.Voltage;PolarizationData.msOffset_Voltage;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_weightDataOK:
		{
			strncpy(filename,FILE_WEIGHTDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_weightDataOK,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;Weight;BodyFat;WSBatteryLevel;WSStatus;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_bpData:
		{
			strncpy(filename,FILE_BPDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_bpData,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;systolic;diastolic;heartRate;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_EcgData:
		{
			strncpy(filename,FILE_ECGDATA,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_EcgData,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;heartRate;respirationRate;\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_statusRTB:
		{
			strncpy(filename,FILE_DEVICESSTATUS,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_statusRTB,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;statusECPI;statusECPO;statusBPSI;statusBPSO;statusFPSI;statusFPSO;statusBTS;statusDCS;statusBLPUMP;statusFLPUMP;statusMFSI;statusMFSO;statusMFSBL;statusPOLAR;statusRTMCB\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;
		case dataID_configureStateDump:
		{
			strncpy(filename,FILE_MSGCONFIGURESTATE,FILENAME_LENGTH);
			if(changeFileName(filename, extract_timestamp(dataID_configureStateDump,db,bufferedTime)))
			{
				taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
				return 1;
			}
			if(validateFile(filename, pFile, "timestamp;stateToConfigure;BLdirection;BLflowReference;FLdirection;FLflowReference;PolDirection;voltageReference;msOffsetActCtrl\n"))
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			pFile=fopen(filename, "rb+");
		}
		break;		
		case dataID_PatientProfile:
		{
			strncpy(filename,FILE_PATIENTPROFILE,FILENAME_LENGTH);
			pFile=fopen(filename, "wb+");
			if(pFile==NULL)
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			fprintf(pFile, "name;gender;measureTimeHours;measureTimeMinutes;wghtCtlTarget;wghtCtlLastMeasurement;wghtCtlDefRemPerDay;kCtlTarget;kCtlLastMeasurement;kCtlDefRemPerDay;urCtlTarget;urCtlLastMeasurement;urCtlDefRemPerDay;minDialPlasFlow;maxDialPlasFlow;minDialVoltage;maxDialVoltage;\n");
		}
		break;
		case dataID_WAKDAllStateConfigure:
		{
			strncpy(filename,FILE_WAKDALLSTATECONFIGURE,FILENAME_LENGTH);
			pFile=fopen(filename, "wb+");
			if(pFile==NULL)
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			fprintf(pFile, "State;defSpeedBp_mlPmin;defSpeedFp_mlPmin;defPol_V;direction;duration_sec;\n");
		}
		break;
		case dataID_configureState:
		{
			strncpy(filename,FILE_WAKDALLSTATECONFIGURE,FILENAME_LENGTH);
			pFile=fopen(filename, "rb");
			if(pFile==NULL)
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			fclose(pFile);
		}
		break;		
		case dataID_AllStatesParametersSCC:
		{
			strncpy(filename,FILE_ALLSTATESPARAMETERSSCC,FILENAME_LENGTH);
			pFile=fopen(filename, "wb+");
			if(pFile==NULL)
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			fprintf(pFile, "State;NaAbsL;NaAbsH;NaTrdLT;NaTrdHT;NaTrdLPsT;NaTrdHPsT;KAbsL;KAbsH;KAAL;KAAH;KTrdLT;KTrdHT;KTrdLPsT;KTrdHPsT;CaAbsL;CaAbsH;CaAAbsL;CaAAbsH;CaTrdLT;CaTrdHT;CaTrdLPsT;CaTrdHPsT;UreaAAbsH;UreaTrdHT;UreaTrdHPsT;CreaAbsL;CreaAbsH;CreaTrdHT;CreaTrdHPsT;PhosAbsL;PhosAbsH;PhosAAbsH;PhosTrdLT;PhosTrdHT;PhosTrdLPsT;PhosTrdHPsT;HCO3AAbsL;HCO3AbsL;HCO3AbsH;HCO3TrdLT;HCO3TrdHT;HCO3TrdLPsT;HCO3TrdHPsT;PhAAbsL;PhAAbsH;PhAbsL;PhAbsH;PhTrdLT;PhTrdHT;PhTrdLPsT;PhTrdHPsT;BPsysAbsL;BPsysAbsH;BPsysAAbsH;BPdiaAbsL;BPdiaAbsH;BPdiaAAbsH;pumpFaccDevi;pumpBaccDevi;VertDeflecitonT;WghtAbsL;WghtAbsH;WghtTrdT;FDpTL;FDpTH;BPSoTH;BPSoTL;BPSiTH;BPSiTL;FPSoTH;FPSoTL;FPSTH;FPSTL;BTSoTH;BTSoTL;BTSiTH;BTSiTL;BatStatT;f_K;SCAP_K;P_K;Fref_K;cap_K;f_Ph;SCAP_Ph;P_Ph;Fref_Ph;cap_Ph;A_dm2;HCO3Adr;wgtNa;wgtK;wgtCa;wgtUr;wgtCrea;wgtPhos;wgtHCO3;mspT\n");
		}
		break;
		case dataID_StateParametersSCC:
		{
			strncpy(filename,FILE_ALLSTATESPARAMETERSSCC,FILENAME_LENGTH);
			pFile=fopen(filename, "rb");
			if(pFile==NULL)
			{
				taskMessage("F", PREFIX, "Error while opening file %s", filename);
				return 1;
			}
			fclose(pFile);
		}
		break;
		default: 
			taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
			return 1;
		break;
	}	
	if(pFile!=NULL) // should never reach this if pFile == NULL but in case, we'll cover it here anyway hehe
	{
		switch(datatype)
		{
			case dataID_physiologicalData: // these are using timestamps 
			case dataID_physicalData:
			case dataID_actuatorData:
			case dataID_weightDataOK:
			case dataID_bpData:
			case dataID_EcgData:
			case dataID_statusRTB:
			case dataID_configureStateDump:
			{
				fseek (pFile, 0, SEEK_END);	// jump to the end of file
				movebeforeCRLF(pFile);		
				while(!seekResult)
				{	
					seekResult = seekToLastLF(pFile);
					/*	Seek backwards the begin of the line - and stop if we reach the first byte.
						It may reach the header if we don't got something in the file beside of it.
						In this case, we read in a whole line to bypass the header again and write
						a new line afterwards.
					*/
					if(!seekResult)
						{
						lastLF = ftell(pFile); // remember position of the last line
						fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile); // read in a whole line

						// puts(dataBuffer); // debug output
						/*
							ptr is used to tell parseLine where to start 
							and save the current pointer position for every parse.
						*/
						ptr = dataBuffer;
						/*
							Parse timestamp of the current line. Needs to be parseLine because timestamp is INT
						*/
						cur_timestamp = parseLine(&ptr, end);
					}
					/*
						SPECIAL CASES IF WE DON'T GOT A SINGLE LINE OF DATA OR ONLY ONE SO WE CANT COMPARE
					*/
					else 
					{	
						// we are at the beginning of the file, so bypass header
						fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile);
						// option 1: we don't got a single line of data in it atm
						if(firstLine)
						{
							writeLineafterHeader = 1;
						}
						// option 2: we want to write a single line to the top of the file and got no comparator
						else if(cur_timestamp > extract_timestamp(datatype, db, bufferedTime) || cur_timestamp == 0)
						{
							firstLine = 0;
							writeLineafterHeader = 1;
						}
						else
						// option 3: something is completely fucked up.
						{
							taskMessage("F", PREFIX, "File %s corrupt or something wrong happened", filename);
							return 1;
						}
					}
							
					if(cur_timestamp == extract_timestamp(datatype, db,bufferedTime)) // value found
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "DEBUG: ==\n");
						#endif		
						char* tmp_ptr;
						tmp_ptr = findCRC(dataBuffer, DBUFFER_CSVLINE_SIZE);
						if(tmp_ptr == NULL)
						{
							taskMessage("F", PREFIX, "File error, no CRC found");
							return 1;
						}
						/*
							Check for CRC errors first, write faulty flag if error occured and exit
						*/
						if(verifyCRC(tmp_ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
						{
							taskMessage("F", PREFIX, "CRC error while reading line with timestamp %d, exiting", extract_timestamp(datatype, db,bufferedTime));
							/* 
								rewind to remembered position, add parsed bytes in dataBuffer and 4 to pass CRC:
							*/
							fseek(pFile,lastLF - ftell(pFile) + (tmp_ptr - dataBuffer) + 4,SEEK_CUR);
							// mark line as faulty by changing crc value to CRC:ERRD
							fprintf(pFile,"ERRD");
							return 1;
						}			
						// rewind to remembered position because fgets has put us at the end of the line
						fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); 
						/*
							write out values using writeOutMemberValues, which calls writeFloat and so on...
						*/			
						writeOutMemberValues(datatype, ptr, end, db, pFile, dataBuffer, lastLF, 0, 0);	
						writeCRC(pFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE,1);
						/*
							Line should be completely parsed, we can close the file.
						*/

						fclose (pFile);
						return 0;
					}
					/*	Exact timestamp doesn't exist, maybe a bigger one if we got a next line?
						Needs to be implemented with temporary files, because you only can append to
						the end of a file or overwrite content of existing files.
					*/
					else if((extract_timestamp(datatype, db,bufferedTime) > cur_timestamp && !firstLine) || (writeLineafterHeader && !firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "DEBUG: > timestamp & !firstLine\n");
						#endif
						seekToLastLF(pFile); // bypass CR&LF at the end of the line
						lastLF = ftell(pFile); // remember actual position, we'll just reuse lastLF here
						if(strnlen(filename, FILENAME_LENGTH) + 4 > FILENAME_LENGTH)
						{
							taskMessage("F", PREFIX, "New filename would be %i characters long, limit is %i",strnlen(filename, FILENAME_LENGTH) + 4, FILENAME_LENGTH);
							fclose(pFile);
							return 1;
						}
						FILE *pTMPFile;
						// filename +5 chars because .tmp will be appended
						char TMPfilename[strnlen(filename, FILENAME_LENGTH)+5];
						/* 	currentChar has to contain more than a 8-bit char, for an explanation look at 
							http://en.wikipedia.org/wiki/C_file_input/output#Opening_a_file_using_fopen
						*/
						int16_t currentChar; 
						strncpy(TMPfilename,filename,FILENAME_LENGTH);
						strncat(TMPfilename,".tmp",4);					
						pTMPFile = fopen(TMPfilename, "w+b");
						fseek (pFile, 0, SEEK_SET); // rewind to beginning of the file and write it to the new one
						// write data before inserted line to temporary file
						while((currentChar = getc(pFile)) != EOF)
						{
							fputc(currentChar, pTMPFile);
							if( (int)lastLF == ftell(pTMPFile))
								break;
						}
						lastLF = ftell(pTMPFile); // remember actual position, reuse again
						// now write the timestamp, which we always need
						fprintf(pTMPFile, "%d;", (int) extract_timestamp(datatype, db,bufferedTime));
						
						// write out values in a new line into the temporary file
						writeOutMemberValues(datatype, ptr, end, db, pTMPFile, dataBuffer, lastLF, 0, 1);
						writeCRC(pTMPFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE,0);			
						/*
							We have written our new line and need write the remaining data
						*/
						while((currentChar = getc(pFile)) != EOF)
						{
							fputc(currentChar, pTMPFile);
						}
						fclose (pTMPFile);
						fclose (pFile); //In POSIX system you can always remove an open file and it will be removed from the directory, although, a the same time, it will exist, without a name, until the last process closes it. Just one of the differences between Windows family and POSIX. 
						// remove "old" .csv file and rename .csv.tmp to .csv
						//taskMessage("I", PREFIX, "Stack left to use is: %d", uxTaskGetStackHighWaterMark(NULL));
						taskMessage("I", PREFIX, "now deleting %s", filename);
						
						if(remove(filename) != 0)
						{
							taskMessage("F", PREFIX, "Error deleting %s", filename);
							perror("Der Grund:");
							//taskMessage("I", PREFIX, "Stack left to use afterwards is: %d", uxTaskGetStackHighWaterMark(NULL));
							return 1;
						}
						taskMessage("I", PREFIX, "Now renaming  %s", filename);
						if(rename(TMPfilename, filename) != 0) //The rename() system call guarantees that an instance of new will always exist, even if the system should crash in the middle of the operation.
						{
							perror("Error for remove in unified_write:");
							taskMessage("F", PREFIX, "Error renaming %s", TMPfilename);
							return 1;
						}
						return 0;
					}
					/*	
						the last timestamp is smaller than the one we want to write - so append to the end
					*/
					else if((extract_timestamp(datatype, db,bufferedTime) > cur_timestamp && firstLine) || (writeLineafterHeader && firstLine))
					{
						#ifdef DEBUG_DS_IO
						taskMessage("I", PREFIX, "DEBUG: > timestamp & firstLine\n");
						#endif
						fseek (pFile, 0, SEEK_END);
						lastLF = ftell(pFile); // remember position of the last line
						
						fprintf(pFile, "%d;", (int) extract_timestamp(datatype, db,bufferedTime));
						
						// write out values in a new line into file
						writeOutMemberValues(datatype, ptr, end, db, pFile, dataBuffer, lastLF, 0, 1);
						writeCRC(pFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE,0);
						fclose (pFile);
						return 0;
					}
					fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); // rewind to remembered position
					movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
					firstLine = 0;
				}
				// this shouldn't be reached?
				return 1;
			}
			break;
			case dataID_PatientProfile: // patient data
			{
				lastLF = ftell(pFile);
				/* write out all patient data.
				*/
				erroroccured |= fprintf(pFile,"%s;",((tdPatientProfile*)db)->name) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%c;",((tdPatientProfile*)db)->gender) > 0 ? 0 : 1;
				
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->measureTimeHours) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->measureTimeMinutes) > 0 ? 0 : 1;
				// lu for unsigned long, which is at least 32 bit on every platform to remove compiler warning
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->wghtCtlTarget) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->wghtCtlLastMeasurement) > 0 ? 0 : 1;
				
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->wghtCtlDefRemPerDay) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->kCtlTarget) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->kCtlLastMeasurement) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->kCtlDefRemPerDay) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->urCtlTarget) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->urCtlLastMeasurement) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%lu;",((tdPatientProfile*)db)->urCtlDefRemPerDay) > 0 ? 0 : 1;
				
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->minDialPlasFlow) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->maxDialPlasFlow) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->minDialVoltage) > 0 ? 0 : 1;
				erroroccured |= fprintf(pFile,"%u;",((tdPatientProfile*)db)->maxDialVoltage) > 0 ? 0 : 1;
				
				erroroccured |= writeCRC(pFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE, 0); // adds a newline to the end because update = 0
				fclose(pFile);
				if(erroroccured)
				{
					taskMessage("F", PREFIX, "Errors occured while writing to file %s", filename);
					return 1;
				}
				else
				{
					return 0;
				}
			}
			break;
			case dataID_WAKDAllStateConfigure: // state configuration
			{
				erroroccured |= writeState(pFile, wakdStates_AllStopped, &((tdWAKDAllStateConfigure*)db)->stateAllStoppedConfig, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_Dialysis, &((tdWAKDAllStateConfigure*)db)->stateDialysisConfig, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_Regen1, &((tdWAKDAllStateConfigure*)db)->stateRegen1Config, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_Ultrafiltration, &((tdWAKDAllStateConfigure*)db)->stateUltrafiltrationConfig, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_Regen2, &((tdWAKDAllStateConfigure*)db)->stateRegen2Config, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_Maintenance, &((tdWAKDAllStateConfigure*)db)->stateMaintenanceConfig, dataBuffer);
				erroroccured |= writeState(pFile, wakdStates_NoDialysate, &((tdWAKDAllStateConfigure*)db)->stateNoDialysateConfig, dataBuffer);
				fclose(pFile);
				if(erroroccured)
				{
					taskMessage("F", PREFIX, "Errors occured while writing to file %s", filename);
					return 1;
				}
				else
				{
					return 0;
				}
			}
			break;
			case dataID_configureState: // one state configuration
			{
				tdWAKDAllStateConfigure* tmpStates;
				tmpStates = pvPortMalloc(sizeof(tdWAKDAllStateConfigure));
				if (tmpStates==NULL)
				{
					// unable to allocate memory
					taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
					#ifdef __arm__
						errMsg(errmsg_pvPortMallocFailed);
					#endif
				}
				/*
					read in all existing states
				*/
				if(unified_read(dataID_WAKDAllStateConfigure, tmpStates))
				{
					taskMessage("E", PREFIX, "File %s doesn't contains all states or another error occured. You need to write all states first before you can change a single state", filename);
					sFree((void *)&tmpStates);
					return 1;
				}
				/*
					overwrite the selected state
				*/
				switch(((tdWAKDStateConfigurationParameters*)db)->thisState)
				{
					case wakdStates_AllStopped:
						tmpStates->stateAllStoppedConfig = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_Dialysis:
						tmpStates->stateDialysisConfig = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_Regen1:
						tmpStates->stateRegen1Config = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_Ultrafiltration:
						tmpStates->stateUltrafiltrationConfig = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_Regen2:
						tmpStates->stateRegen2Config = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_Maintenance:
						tmpStates->stateMaintenanceConfig = *((tdWAKDStateConfigurationParameters*)db);
					break;
					case wakdStates_NoDialysate:
						tmpStates->stateNoDialysateConfig = *((tdWAKDStateConfigurationParameters*)db);
					break;
					default:
						taskMessage("F", PREFIX, "Unknown state recognized while processing tdWAKDStateConfigure");
						sFree((void *)&tmpStates);
						return 1;
					break;
				}
				/*
					and write a new file afterwards
				*/
				if(unified_write(dataID_WAKDAllStateConfigure, tmpStates))
				{
					taskMessage("E", PREFIX, "Unified Write returned error while trying to write file %s", filename);
					sFree((void *)&tmpStates);
					return 1;
				}
				sFree((void *)&tmpStates);
				return 0;
			}
			break;
			case dataID_AllStatesParametersSCC: // state params
			{
				erroroccured |= writeStateParam(pFile, wakdStates_AllStopped, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateAllStopped, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_Dialysis, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateDialysis, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_Regen1, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen1, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_Ultrafiltration, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateUltrafiltration, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_Regen2, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateRegen2, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_Maintenance, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateMaintenance, dataBuffer);
				erroroccured |= writeStateParam(pFile, wakdStates_NoDialysate, &((tdAllStatesParametersSCC*)db)->parametersSCC_stateNoDialysate, dataBuffer);
				fclose(pFile);
				if(erroroccured)
				{
					taskMessage("F", PREFIX, "Errors occured while writing to file %s", filename);
					return 1;
				}
				else
				{
					return 0;
				}
			}
			break;
			case dataID_StateParametersSCC: // single state params
			{
				tdAllStatesParametersSCC* tmpParams;
				tmpParams = pvPortMalloc(sizeof(tdAllStatesParametersSCC));
				if (tmpParams==NULL)
				{
					// unable to allocate memory
					taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
					#ifdef __arm__
						errMsg(errmsg_pvPortMallocFailed);
					#endif
				}
				/*
					read in all existing states
				*/
				if(unified_read(dataID_AllStatesParametersSCC, tmpParams))
				{
					taskMessage("E", PREFIX, "File %s doesn't contains all states or another error occured. You need to write all states first before you can change a single state", filename);
					sFree((void *)&tmpParams);
					return 1;
				}
				/*
					overwrite the selected state
				*/
				switch(((tdParametersSCC*)db)->thisState)
				{
					case wakdStates_AllStopped:
						tmpParams->parametersSCC_stateAllStopped = *((tdParametersSCC*)db);
					break;
					case wakdStates_Dialysis:
						tmpParams->parametersSCC_stateDialysis = *((tdParametersSCC*)db);
					break;
					case wakdStates_Regen1:
						tmpParams->parametersSCC_stateRegen1 = *((tdParametersSCC*)db);
					break;
					case wakdStates_Ultrafiltration:
						tmpParams->parametersSCC_stateUltrafiltration = *((tdParametersSCC*)db);
					break;
					case wakdStates_Regen2:
						tmpParams->parametersSCC_stateRegen2 = *((tdParametersSCC*)db);
					break;
					case wakdStates_Maintenance:
						tmpParams->parametersSCC_stateMaintenance = *((tdParametersSCC*)db);
					break;
					case wakdStates_NoDialysate:
						tmpParams->parametersSCC_stateNoDialysate = *((tdParametersSCC*)db);
					break;
					default:
						taskMessage("F", PREFIX, "Unknown state recognized while processing tdParametersSCC");
						sFree((void *)tmpParams);
						return 1;
					break;
				}
				/*
					and write a new file afterwards
				*/
				if(unified_write(dataID_AllStatesParametersSCC, tmpParams))
				{
					taskMessage("E", PREFIX, "Unified Write returned error while trying to write file %s", filename);
					sFree((void *)&tmpParams);
					return 1;
				}
				sFree((void *)&tmpParams);
				return 0;
			}			
			default:
			{
				defaultIdHandling(PREFIX, datatype);
				return 1;
			}
			break;
		}
	}
	else
	{
		taskMessage("F", PREFIX, "Error while opening %s",filename);
		fclose (pFile);
		return 1;
	}
	taskMessage("F", PREFIX, "If you can read this, something special(in a wrong way) has happened.");
#endif
	return 1; // won't be reached, but hey nice to have
}

//! Writes the member values contained in \a db. This is a wrapper to prevent calling writeFloat multiple times in the code.
/*! For further information about the parameters, you should take a look at \ writeFloat.
*/
uint8_t writeOutMemberValues(tdDataId datatype, char * ptr, char * end,
										void* db, FILE* pFile, char* dataBuffer,
										uint32_t lastLF, uint8_t lastvalue, uint8_t newline)
{
	switch(datatype)
		{
			case dataID_physiologicalData: {
				tdPhysiologicalData *pPhysiologicalData = NULL;
				pPhysiologicalData = (tdPhysiologicalData *) db;
				ptr = writeFloat(ptr, end, (float)pPhysiologicalData->currentState,					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Sodium,					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.msOffset_Sodium,		pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Sodium,					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.msOffset_Sodium,		pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Potassium,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.msOffset_Potassium,		pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Potassium,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.msOffset_Potassium,		pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Urea, 					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.msOffset_Urea, 			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Urea, 					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.msOffset_Urea, 			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.pH,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.msOffset_pH,			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.pH,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.msOffset_pH,			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Temperature,			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.msOffset_Temperature,	pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Temperature,			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.msOffset_Temperature,	pFile, dataBuffer, lastLF, lastvalue, newline);
				// ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Creatinine,				pFile, dataBuffer, lastLF, lastvalue, newline);
				// ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Creatinine,				pFile, dataBuffer, lastLF, lastvalue, newline);
				// ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataI.Phosphate,				pFile, dataBuffer, lastLF, lastvalue, newline);
				// ptr = writeFloat(ptr, end, pPhysiologicalData->ECPDataO.Phosphate,				pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;
				break;
			}
			case dataID_physicalData: {
				tdPhysicalsensorData *pPhysicalsensorData = NULL;
				pPhysicalsensorData = (tdPhysicalsensorData *) db;
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->statusECPI,								pFile, dataBuffer, lastLF, lastvalue, newline);
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->statusECPO,								pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureFCI.Pressure,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureFCI.msOffset_Pressure,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureFCO.Pressure,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureFCO.msOffset_Pressure,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureBCI.Pressure,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureBCI.msOffset_Pressure,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureBCO.Pressure,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->PressureBCO.msOffset_Pressure,				pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->TemperatureInOut.TemperatureInlet_Value,	pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->TemperatureInOut.TemperatureOutlet_Value,	pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->TemperatureInOut.msOffset_Temperature,	pFile, dataBuffer, lastLF, lastvalue, newline);
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->TemperatureInOut.Status,					pFile, dataBuffer, lastLF, lastvalue, newline);
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->TemperatureInOut.msOffset_Status,			pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.Cond_FCR,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.Cond_FCQ,						pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.Cond_PT1000,					pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.msOffset_Cond,					pFile, dataBuffer, lastLF, lastvalue, newline);
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.Status,							pFile, dataBuffer, lastLF, lastvalue, newline);
//				ptr = writeFloat(ptr, end,  pPhysicalsensorData->CSENS1Data.msOffset_Status,				pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;
				break;
			}
			case dataID_actuatorData:
			{
				tdActuatorData *pActDta = NULL;
				pActDta = (tdActuatorData *) db;
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBIData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBIData.Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBIData.msOffset_Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBOData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBOData.Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBOData.msOffset_Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBUData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBUData.Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->MultiSwitchBUData.msOffset_Position, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.msOffset_Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.Speed, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.msOffset_Speed, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.FlowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.msOffset_FlowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.Flow, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.msOffset_Flow, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.Current, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->BLPumpData.msOffset_Current, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.msOffset_Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.Speed, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.msOffset_Speed, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.FlowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.msOffset_FlowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.Flow, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.msOffset_Flow, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.Current, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->FLPumpData.msOffset_Current, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.SwitchONnOFF, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.msOffset_Direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.VoltageReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.msOffset_VoltageReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.Voltage, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, (float)pActDta->PolarizationData.msOffset_Voltage, pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;
			}
			break;
			case dataID_weightDataOK:
			{
				tdWeightMeasure  *pWeightSensorReadout = NULL; 
				pWeightSensorReadout = (tdWeightMeasure *) db; 
				ptr = writeFloat(ptr, end, (float)pWeightSensorReadout->Weight, pFile, dataBuffer, lastLF, lastvalue, newline);	
				ptr = writeFloat(ptr, end, (float)pWeightSensorReadout->BodyFat, pFile, dataBuffer, lastLF, lastvalue, newline);	
				ptr = writeFloat(ptr, end, (float)pWeightSensorReadout->WSBatteryLevel, pFile, dataBuffer, lastLF, lastvalue, newline);	
				ptr = writeFloat(ptr, end, (float)pWeightSensorReadout->WSStatus, pFile, dataBuffer, lastLF, lastvalue, newline);	
				
				return 0;
			}
			break;
			case dataID_bpData:
			{
				ptr = writeFloat(ptr, end, ((tdBpData *)db)->systolic, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdBpData *)db)->diastolic, pFile, dataBuffer, lastLF, lastvalue, newline);					
				ptr = writeFloat(ptr, end, ((tdBpData *)db)->heartRate, pFile, dataBuffer, lastLF, lastvalue, newline);					
				return 0;
			}
			break;
			case dataID_EcgData:
			{
				ptr = writeFloat(ptr, end, ((tdEcgData *)db)->heartRate, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, ((tdEcgData *)db)->respirationRate , pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;
			}
			break;
			case dataID_statusRTB:
			{
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusECPI, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusECPO, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusBPSI, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusBPSO, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusFPSI, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusFPSO, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusBTS, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusDCS, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusBLPUMP, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusFLPUMP, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusMFSI, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusMFSO, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusMFSBL, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusPOLAR, pFile, dataBuffer, lastLF, lastvalue, newline);			
				ptr = writeFloat(ptr, end, ((tdDevicesStatus *)db)->statusRTMCB, pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;				
			}
			break;
			case dataID_configureStateDump:
			{
				tdMsgConfigureState *msgConfigureState = NULL;
				msgConfigureState = (tdMsgConfigureState *) db;
				ptr = writeFloat(ptr, end, (float)msgConfigureState->stateToConfigure, pFile, dataBuffer, lastLF, lastvalue, newline);	
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.BLPumpCtrl.direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.BLPumpCtrl.flowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.FLPumpCtrl.direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.FLPumpCtrl.flowReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.PolarizationCtrl.direction, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.PolarizationCtrl.voltageReference, pFile, dataBuffer, lastLF, lastvalue, newline);
				ptr = writeFloat(ptr, end, msgConfigureState->actCtrl.msOffset_ActCtrl, pFile, dataBuffer, lastLF, lastvalue, newline);
				return 0;
			}
			break;
			default: 
				taskMessage("F", PREFIX, "Passed wrong datatype: %d", datatype);
				return 1;
			break;
		}
}

//! Returns true if the file exists and closes it, otherwise it will open it for writing and return 0.
uint8_t validateFile(char* filename, FILE* pFile, char* header)
{
#if defined VP_SIMULATION
	pFile=fopen(filename, "rb+");
	if(pFile==NULL) {
		pFile=fopen(filename, "wb");
		#ifdef DEBUG_DS_IO
		taskMessage("I", PREFIX, "File %s doesn't exist, creating file and header.", filename);
		#endif	
		if(fprintf(pFile, header) < 0) {
			taskMessage("F", PREFIX, "Error while writing to file %s", filename);
			fclose(pFile);
			return 1;
		}
		fclose(pFile);
	}
	else
	{
		fclose(pFile);
	}
#endif
	return 0;
}

//! Needed for writing out states
uint8_t writeState(FILE *pFile, tdWakdStates statenr, tdWAKDStateConfigurationParameters *state, char* dataBuffer)
{
	uint8_t erroroccured = 0;
#if defined VP_SIMULATION
	uint32_t lastLF;
	lastLF = ftell(pFile);
	erroroccured |= (fprintf(pFile,"%i;",statenr) > 0 ? 0 : 1); // write current state 
	erroroccured |= (fprintf(pFile,"%i;",state->defSpeedBp_mlPmin) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%i;",state->defSpeedFp_mlPmin) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%i;",state->defPol_V) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%i;",state->direction) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%i;",(int)state->duration_sec) > 0 ? 0 : 1); 
    erroroccured |= writeCRC(pFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE, 0); // adds a newline to the end because update = 0
#endif
  return erroroccured;
}

uint8_t writeStateParam(FILE *pFile, tdWakdStates statenr, tdParametersSCC *state, char* dataBuffer)
{
	uint8_t erroroccured = 0;
#if defined VP_SIMULATION
	uint32_t lastLF;
	lastLF = ftell(pFile);
	erroroccured |= (fprintf(pFile,"%i;",statenr) > 0 ? 0 : 1); // write current state 
	erroroccured |= (fprintf(pFile,"%g;",state->NaAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->NaAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->NaTrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->NaTrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->NaTrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->NaTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KAAL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KAAH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KTrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->KTrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KTrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->KTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaAAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaTrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->CaTrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaTrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->UreaAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->UreaTrdT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->UreaTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CreaAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CreaAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CreaTrdT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CreaTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosTrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->PhosTrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosTrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhosTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3AAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3AbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3AbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3TrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->HCO3TrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3TrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3TrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhAAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhTrdT) > 0 ? 0 : 1); 
	//erroroccured |= (fprintf(pFile,"%g;",state->PhTrdHT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhTrdLPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->PhTrdHPsT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPsysAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPsysAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPsysAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPdiaAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPdiaAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPdiaAAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->pumpFaccDevi) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->pumpBaccDevi) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->VertDeflecitonT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->WghtAbsL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->WghtAbsH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->WghtTrdT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FDpTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FDpTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPSoTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPSoTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPSiTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BPSiTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FPSoTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FPSoTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FPSTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->FPSTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BTSoTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BTSoTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BTSiTH) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BTSiTL) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->BatStatT) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->f_K) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->SCAP_K) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->P_K) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->Fref_K) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->cap_K) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->f_Ph) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->SCAP_Ph) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->P_Ph) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->Fref_Ph) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->cap_Ph) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->A_dm2) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CaAdr) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->CreaAdr) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->HCO3Adr) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtNa) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtK) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtCa) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtUr) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtCrea) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtPhos) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->wgtHCO3) > 0 ? 0 : 1); 
	erroroccured |= (fprintf(pFile,"%g;",state->mspT) > 0 ? 0 : 1); 
    erroroccured |= writeCRC(pFile, lastLF, dataBuffer, DBUFFER_CSVLINE_SIZE, 0); // adds a newline to the end because update = 0
#endif
  return erroroccured;
}

