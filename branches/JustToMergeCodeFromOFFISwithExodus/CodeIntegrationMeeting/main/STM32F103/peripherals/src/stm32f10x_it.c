// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   stm32f10x_it.c
//! \brief  main interrupt routine
//!
//! Main Interrupt Service Routines.
//!  This file provides template for all exceptions handler and 
//!  peripherals interrupt service routine.
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported Constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
uint32_t tim1_counter = 0;
uint32_t tim2_counter = 0;
// -----------------------------------------------------------------------------------
// Private functions
// Cortex-M3 Processor Exceptions Handlers 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  NMI_Handler
//!
//! This function handles NMI exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void NMI_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  HardFault_Handler
//!
//! This function handles Hard Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void HardFault_Handler(void)
{
  uint8_t errorHW=0;
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
    errorHW =1;
  }
}
// -----------------------------------------------------------------------------------
//! \brief  MemManage_Handler
//!
//! This function handles Memory Manage exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MemManage_Handler(void)
{
  // Go to infinite loop when Memory Manage exception occurs 
  while (1)
  {
  }
}
// -----------------------------------------------------------------------------------
//! \brief  BusFault_Handler
//!
//! This function handles Bus Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void BusFault_Handler(void)
{
  // Go to infinite loop when Bus Fault exception occurs 
  while (1)
  {
  }
}
// -----------------------------------------------------------------------------------
//! \brief  UsageFault_Handler
//!
//! This function handles Usage Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void UsageFault_Handler(void)
{
  // Go to infinite loop when Usage Fault exception occurs 
  while (1)
  {
  }
}
// -----------------------------------------------------------------------------------
//! \brief  SVC_Handler
//!
//! This function handles SVCall exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void SVC_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  DebugMon_Handler
//!
//! This function handles Debug Monitor exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void DebugMon_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  PendSV_Handler
//!
//! This function handles PendSVC exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void PendSV_Handler(void)
{
}
// -----------------------------------------------------------------------------------
// This function handles SysTick Handler
// 
// RTMCB: SysTick is generated every 1ms
//
// Inputs : 
//          none
// 
// Output : none
// -----------------------------------------------------------------------------------
void SysTick_Handler(void)
{  
 
#ifndef MBFREERTOS

  // This function decrements a counter to create delays.
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
  // MB TimeStamp
  globalTimestamp++;

//  if (LOCK_CRITICAL_INTS==0){
    ADCDMACounter++;
    if(ADCDMACounter == ADCDMA_SAMPLEPERIOD){
        ADCDMACounter = 0;
        /* Start ADC1 Software Conversion */ 
        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    }
//  }

#ifdef STM32F_LED_INT_TICK
    led_toggle_counter++;
    if(led_toggle_counter == LED_TOGGLE_PERIOD){
        led_toggle_counter = 0;
        STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
    }
#endif

#ifdef STM32F_SPORTS_STINT
        usart_tx_counter++;
        if(usart_tx_counter == USART_TX_PERIOD){
        usart_tx_counter = 0;
#ifndef STM32_USART1_LBACK   // USART1 TX
        SerialPort_SendPacket(USART1);
#endif
#ifndef STM32_USART2_LBACK   // USART2 TX
        SerialPort_SendPacket(USART2);
#endif
#ifndef STM32_USART3_LBACK  // USART3 TX
        SerialPort_SendPacket(USART3);
#endif
#ifndef STM32_UART4_LBACK   // UART4 TX
        SerialPort_SendPacket(UART4);
#endif
#ifndef STM32_UART5_LBACK  // UART5 TX
        SerialPort_SendPacket(UART5);
#endif 
        }
#endif  // STM32F_SPORTS_MAIN

    uif_acc_data_counter++;
    if(uif_acc_data_counter == UIF_ACC_DATA_SIMUL){
        uif_acc_data_counter = 0;
        MB_WAKD_BTS_InletTemperature+=2;
        if(MB_WAKD_BTS_InletTemperature==100) MB_WAKD_BTS_InletTemperature = 0;
        MB_WAKD_BTS_OutletTemperature++;
        if(MB_WAKD_BTS_OutletTemperature==100) MB_WAKD_BTS_OutletTemperature = 0;
    }

#endif

}

// -----------------------------------------------------------------------------------
//! \brief  PVD_IRQHandler
//!
//! This function handles PVD_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void PVD_IRQHandler(void)
{
  BKP_TamperPinCmd(DISABLE);

  EXTI_ClearITPendingBit(EXTI_Line16);
  NVIC_ClearPendingIRQ(PVD_IRQn);
}
// -----------------------------------------------------------------------------------
//! \brief  RTC_IRQHandler
//!
//! This function handles RTC_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void RTC_IRQHandler(void)
{

  NVIC_ClearPendingIRQ(RTC_IRQn);
  RTC_ClearITPendingBit(RTC_IT_SEC);
  // do not use Calculate Time Routine here
  // causes hardware fault when writing sdcard
  
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI0_IRQHandler
//!
//! This function handles EXTI0_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI0_IRQHandler(void)
{

  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line0);
    // DO THE TASK
  }
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI1_IRQHandler
//!
//! This function handles EXTI1_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI1_IRQHandler(void)
{
 
  if(EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line1);
    // DO THE TASK
      //LINE_RTMCB_uC_SYNC_STATUS = 
      Update_IO_STATUS (RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_PIN);
  }
}


// -----------------------------------------------------------------------------------
//! \brief  EXTI4_IRQHandler
//!
//! This function handles EXTI4_IRQHandler: SD_CARD INSERTED OR NOT
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI4_IRQHandler(void)
{
  portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
  if(EXTI_GetITStatus(EXTI_Line4) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line4);  
    // DO THE TASK	
    LINE_RTMCBuC_nIRQ = Update_IO_STATUS (RTMCBuC_nIRQ_GPIO_PORT, RTMCBuC_nIRQ_GPIO_PIN);    
    if(LINE_RTMCBuC_nIRQ == SIGNAL_OFF) {
        STATUS_RTMCBuC_nIRQ = RTMCB_INTERRUPTED;
        tdQtoken Qtoken;
        Qtoken.command = command_BufferFromRTBavailable;
        Qtoken.pData = NULL;
        xQueueSendToBackFromISR(allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken);
    }
    else STATUS_RTMCBuC_nIRQ = RTMCB_NOINTERRUPT;
    RTMCB_UART4_DMA_ReadData();
    //Update_SDCARD_Presence();
  }
}

// -----------------------------------------------------------------------------------
//! \brief  EXTI9_5_IRQHandler
//!
//! This function handles EXTI9_5_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI9_5_IRQHandler(void)
{
  uint32_t DelayBat;
  
  if(EXTI_GetITStatus(EXTI_Line8) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line8);    
      // DO THE TASK
      // LINE_BABD_ALARM_STATUS = 
      // Update_IO_STATUS (BABD_ALARM_GPIO_PORT, BABD_ALARM_GPIO_PIN);
  }

  // Battery Removal and Restore detection  
  if(EXTI_GetITStatus(EXTI_Line9) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line9);
      // DO THE TASK
      // LINE_DABD_ALARM_STATUS = 
      // Update_IO_STATUS (DABD_ALARM_GPIO_PORT, DABD_ALARM_GPIO_PIN);
  }
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI15_10_IRQHandler
//!
//! This function handles EXTI15_10_IRQHandler .
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI15_10_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line10) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line10);
      // DO THE TASK
      // LINE_BLD_ALARM_STATUS = 
      // Update_IO_STATUS (BLD_ALARM_GPIO_PORT, BLD_ALARM_GPIO_PIN);
  }

  if(EXTI_GetITStatus(EXTI_Line11) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line11);
      // DO THE TASK
      // LINE_FLD_ALARM_STATUS = 
      // Update_IO_STATUS (FLD_ALARM_GPIO_PORT, FLD_ALARM_GPIO_PIN);
  }
}
// -----------------------------------------------------------------------------------
//! \brief  RTCAlarm_IRQHandler
//!
//! This function handles RTCAlarm_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void RTCAlarm_IRQHandler(void)
{
  // Clear the Alarm Pending Bit 
  RTC_ClearITPendingBit(RTC_IT_ALR);

  // Clear the EXTI Line 17 
  EXTI_ClearITPendingBit(EXTI_Line17);
}
// -----------------------------------------------------------------------------------
//! \brief  TIM1_UP_IRQHandler
//!
//! This function handles TIM1 overflow and update interrupt request
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
// 21.01.2011: current period = 6.25ms
void TIM1_UP_TIM10_IRQHandler(void)
{
    // Clear the TIM1 Update pending bit 
    TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

    // debug
    tim1_counter++;
}
// -----------------------------------------------------------------------------------
//! \brief  TIM2_IRQHandler
//!
//! This function handles TIM2_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void TIM2_IRQHandler(void)
{
  uint8_t completed = 0;

  //if(OUT_COMM_RTMCB_FLAG == 0xFF) return;
  // WATCH OUT!!!!!
  if(USART_GetITStatus(USART2, USART_IT_RXNE)==SET)  return;

  TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

  if (MB_SYSTEM_READY==DEVICE_READY){

      // debug
      tim2_counter++;
 
#ifdef STM32F_LED_TIM2
  //STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif
#ifdef STM32F_LED_TIM2
  STM32F_GPIOOn(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif

#if 1
#ifdef STM32F_UIF_ACC_SCHEDULER   // OK

    switch(UIF_ACC_CMD_NO){      // switch case
        case MB_UIF_SCHNO_TEST_LINK:
            completed = STATE_DIAGRAM_TEST_LINK();
            if(completed) {
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_BLTEMPERATURE;
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_BLTEMPERATURE:
            completed = STATE_DIAGRAM_WAKD_BLTEMPERATURE();
            if(completed) {
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_SHUTDOWN; 
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_SHUTDOWN:
            completed = STATE_DIAGRAM_WAKD_SHUTDOWN();
            if(completed) {
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_TEST_LINK;
                uif_acc_trial_counter = 0;
            }
        break;
    } // end switch case
#endif

#ifdef STM32F_LED_TIM2
  STM32F_GPIOOff(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif

#endif
  
  }

}
// -----------------------------------------------------------------------------------
//! \brief  USART1_IRQHandler
//!
//! This function handles USART1_IRQHandler (USB-MAINTENANCE)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART1_IRQHandler(void){

    if(USART_GetITStatus(USART1, USART_IT_RXNE)==SET){
#ifdef STM32_USART1_LBACK    
        //USB_Transaction ();
        SerialPort_Transaction(USART1);
#else
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
#endif
    }   
}
// -----------------------------------------------------------------------------------
//! \brief  USART2_IRQHandler
//!
//! This function handles USART2_IRQHandler (CB/WCM)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART2_IRQHandler(void){
    if(USART_GetITStatus(USART2, USART_IT_RXNE)==SET){
#ifdef STM32_USART2_LBACK    
        //SerialPort_Transaction(USART2);
        CB_Transaction(USART2);
#else
        USART_ClearITPendingBit(USART2, USART_IT_RXNE);
#endif
    }
}
// -----------------------------------------------------------------------------------
//! \brief  USART3_IRQHandler
//!
//! This function handles USART3_IRQHandler (PM1)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART3_IRQHandler(void){
    if(USART_GetITStatus(USART3, USART_IT_RXNE)==SET){
#ifdef STM32_USART3_LBACK    
        //USB_Transaction ();
        SerialPort_Transaction(USART3);
#else
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
#endif
    }
}
// -----------------------------------------------------------------------------------
//! \brief  UART4_IRQHandler
//!
//! This function handles UART4_IRQHandler (RS422-RTMCB)
//!
//! \param      none
//! 
//! \return     none2
// -----------------------------------------------------------------------------------
void UART4_IRQHandler(void){
    if(USART_GetITStatus(UART4, USART_IT_RXNE)==SET){
#ifdef STM32F_UART4_DMA
      USART_ClearITPendingBit(UART4, USART_IT_RXNE);
#else

#ifdef STM32_UART4_LBACK    
        //SerialPort_Transaction(UART4);
        RTMCB_Transaction (UART4);
#else
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
#endif

#endif    
    }
}
// -----------------------------------------------------------------------------------
//! \brief  UART5_IRQHandler
//!
//! This function handles UART5_IRQHandler (PM2)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void UART5_IRQHandler(void){
    if(USART_GetITStatus(UART5, USART_IT_RXNE)==SET){
#ifdef STM32_UART5_LBACK    
        //USB_Transaction ();
        SerialPort_Transaction(UART5);
#else
        USART_ClearITPendingBit(UART5, USART_IT_RXNE);
#endif
    }
}

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Handle DMA1_Channel1_IRQHandler interrupt request.
//!
//! This function handles DMA1_Channel1_IRQHandler interrupt request.
//!
//! \param  void
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void DMA1_Channel1_IRQHandler(void)
{
    // End of transmission (ADC EOC request)
    DMA_Cmd(DMA1_Channel1, DISABLE);
    DMA_ClearFlag(DMA_IFCR_CTCIF1);                                // Clear transfer complete flag
    NVIC_ClearIRQPendingIRQ(DMA1_Channel1_IRQn);

    FEEDBACK_ADCDMA_ReadData();

    DMA_ClearFlag(DMA1_FLAG_TC1);
    DMA_Cmd(DMA1_Channel1, ENABLE);

    // Stop ADC1 Software Conversion  
    ADC_SoftwareStartConvCmd(ADC1, DISABLE);

}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Handle DMA1_Channel1_IRQHandler interrupt request.
//!
//! This function handles DMA1_Channel1_IRQHandler interrupt request.
//!
//! \param  void
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void DMA2_Channel3_IRQHandler(void)
{
    // Full Buffer Reception
    DMA_Cmd(DMA2_Channel3, DISABLE);
    // CTCIFx: Channel x transfer complete clear (x = 1 ..7)
    DMA_ClearFlag(DMA_IFCR_CTCIF3);                                // Clear transfer complete flag
    NVIC_ClearIRQPendingIRQ(DMA2_Channel3_IRQn);
    
#ifdef STM32F_UART4_DMA
    if (MB_SYSTEM_READY==DEVICE_READY){
        RTMCB_UART4_DMA_ReadData();
    }
#endif
    
    DMA_ClearFlag(DMA2_FLAG_TC3);
    DMA_Cmd(DMA2_Channel3, ENABLE);

}

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
