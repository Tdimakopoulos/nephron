warning('off');
TbaseSim=60;

CUr_Init_mmolPL=11;
CK_exInit_mmolPL=4.6;
CK_inInit_mmolPL=145;
CNa_exInit_mmolPL=145;

real_CURin=CUr_Init_mmolPL;
real_CKin=CK_inInit_mmolPL;
real_GUr=0.155; %normal=0,155pmin 74pday, overlayed by Transient(71)
real_GNa=0.000; % vermutung
real_GK=0.01125; %% 50mmol-10% = 45 mmol/day = 0,03125mmol/min
% K intake normally varies between 40 and 150 mEq/day. In the steady state, fecal losses are usually close to 10% of intake.

%aus: Potassium metabolism and gastrointestinal function; a review Spencer 1959: [0.06944]
%real_k_V=0.4;
real_V_tot=45;
real_V_ex=real_V_tot*0.375;
real_V_drink=1500;

real_GmKNa=0;


if TbaseSim>60 
    % due to production of chemical sensor Timestamp
    disp('TbaseSim should be [0.1 ..60] limiting it to 60!' );
    TbaseSim=1;
end;

if TbaseSim<0.01 
    % due to time delay in the pipes!
    disp('TbaseSim should be [0.1 ..60] limiting it to 0.1!' );
    TbaseSim=0.01;
end;

if mod(TbaseSim,0.01)~= 0
    % due to time delay in the pipes!
    disp('TbaseSim should be quantilized to 0.01 rounding it to 0.1!' );
    TbaseSim=TbaseSim*100;
    TbaseSim=round(TbaseSim);
    TbaseSim=TbaseSim/100;
end;