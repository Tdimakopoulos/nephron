/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

// VMI includes 
#include "vmi/vmiMessage.h" 
#include "vmi/vmiOSAttrs.h" 
#include "vmi/vmiOSLib.h" 
#include "vmi/vmiRt.h" 
#include "vmi/vmiTypes.h" 
#include "vmi/vmiVersion.h"  

#include <stdio.h>
#include <string.h>
#include <windows.h>
 
// Struct which contains all variables/data that is available inside all dll defined
// functions. 
typedef struct vmiosObjectS  {
	vmiRegInfoCP result;
	vmiRegInfoCP sp;
	memDomainP   pseDomain;
} vmiosObject;

// helper function to read function parameters from stack
void getArg(vmiProcessorP processor, vmiosObjectP object, Uns32 index, void *result);
