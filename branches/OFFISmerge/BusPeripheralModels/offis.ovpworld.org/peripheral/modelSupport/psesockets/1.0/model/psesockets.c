/*
 * Copyright (c) 2005-2011 Imperas Software Ltd., www.imperas.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//
// General purpose socket inteface
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PREFIX "PSE_SER"
#ifdef _WIN32
    #include <windows.h>
    #include <winsock2.h>
    #define MSG_DONTWAIT 0
#else
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
#endif

// VMI area includes
#include "vmi/vmiMessage.h"
#include "vmi/vmiOSAttrs.h"
#include "vmi/vmiOSLib.h"
#include "vmi/vmiRt.h"
#include "vmi/vmiTypes.h"
#include "vmi/vmiVersion.h"

#define UNUSED __attribute__((unused))
#define MAX(_a, _b)  ((_a) > (_b) ? (_a) : (_b))


typedef struct channelS {
    Bool  inUse;       // this channel in use
    Bool  verbose;     // write to simulator log
    Int32 socketFD;    // interact via a duplex socket
    FILE *log;         // write to this log
    FILE *src;         // read from this source file
} channel, *channelP;

#define MAX_CHANNELS 4

typedef struct vmiosObjectS  {

    // return register (standard ABI)
    vmiRegInfoCP result;

    // stack pointer (standard ABI)
    vmiRegInfoCP sp;

    const char *portFile;
    Bool finishOnDisconnect;

    channel channels[MAX_CHANNELS];
} vmiosObject;


static channelP getChannel(vmiosObjectP object, int chn) {
    if (chn < 0 || chn >= MAX_CHANNELS) {
        vmiMessage("F", PREFIX, "Channel number (%d) out of bounds", chn);
    }
    return &object->channels[chn];
}

static channelP nextChannel(vmiosObjectP object) {
    int i;

    for(i = 0; i < MAX_CHANNELS; i++) {
        if (!object->channels[i].inUse) {
            object->channels[i].inUse    = True;
            object->channels[i].verbose  = False;
            object->channels[i].socketFD = -1;
            object->channels[i].log      = NULL;
            object->channels[i].src      = NULL;
            return &(object->channels[i]);
        }
    }
    vmiMessage("F", PREFIX, "No serial channels available");
    return NULL;
}

static inline Int32 channelNumber(vmiosObjectP object, channelP ch) {
    return (ch - &object->channels[0]) / sizeof(channel);
}

//
// Close the channel and any files or sockets.
// Channel should then be re-usable.
//
static void closeChannel(vmiosObjectP  object, int chn) {
    channelP ch = getChannel(object, chn);
    ch->inUse = False;
    if (ch->socketFD >= 0) {
        shutdown(ch->socketFD, 2);
    }
    if (ch->src) {
        fclose(ch->src);
    }
    if (ch->log) {
        fclose(ch->log);
    }
}

//
// Read a function adument using the standard ABI
//
static void getArg(
    vmiProcessorP processor,
    vmiosObjectP  object,
    Uns32         index,
    void         *result
) {
    memDomainP domain    = vmirtGetProcessorDataDomain(processor);
    Uns32      argSize   = 4;
    Uns32      argOffset = (index+1)*argSize;
    Uns32      spAddr;

    // get the stack
    vmiosRegRead(processor, object->sp, &spAddr);

    // read argument value
    vmirtReadNByteDomain(domain, spAddr+argOffset, result, argSize, 0, True);
}

static void writePortnumFile(Uns32 port, const char *portfile)
{
    FILE *f = fopen(portfile, "w");
    if(f) {
        fprintf(f, "%d\n", port);
        fclose(f);
    }
}

//
// Connect to socket (block till it happens), then write a file with the
// port number if requested to.
//
static Int32 createSocket(Uns32 *portp, const char *portFile, Bool verbose)
{
#ifdef _WIN32
    static int done = 0;
    if ( !done ) {
        WSADATA info;
        if ( WSAStartup(MAKEWORD(2,0), &info) != 0 )
            vmiMessage("FN", PREFIX, "Error initializing Windows networking");
    }
#endif

    Uns32 option     = 1;
    Int32 sock       = socket(PF_INET, SOCK_STREAM, 0);
    Int32 port       = *portp;
    Bool  autoAssign = (port == 0);

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*) &option, sizeof(option));

    struct sockaddr_in sockaddr;
    sockaddr.sin_family      = PF_INET;
    sockaddr.sin_port        = htons(port);
    sockaddr.sin_addr.s_addr = INADDR_ANY;

    if (bind(sock, (struct sockaddr*) &sockaddr, sizeof(sockaddr)) < 0)
    {
        vmiMessage("FN", PREFIX,
            "Unable to bind address for socket connection"
        );
    }
    if (autoAssign) {
        struct sockaddr_in tmpSockaddr;
        Uns32 len = sizeof(tmpSockaddr);
        if (getsockname(sock, (struct sockaddr*)&tmpSockaddr, &len) < 0 ) {
            vmiMessage("FN", PREFIX, "Unable to get socket name");
        }
        *portp = ntohs(tmpSockaddr.sin_port);
        if(portFile)
            writePortnumFile(*portp, portFile);
        vmiMessage("I", "CHN_PN", "Got socket %d\n", *portp);
    }
    if (listen(sock, 2) < 0)
    {
        vmiMessage("FN", PREFIX,
            "Unable to listen for remote socket connection"
        );
    }
    // This call will block until someone connects to the other end of the socket
    if (verbose) {
        vmiMessage("I", PREFIX, "Waiting for connection on port %d", *portp);
    }
    Uns32  len = sizeof(sockaddr);
    Int32 r = accept(sock, (struct sockaddr*) &sockaddr, &len);
    if (r < 0){
        vmiMessage("FS", PREFIX,"Socket accept failed");
    }
    return r;
}

//
// Open a socket and block until a client connects.
// Note that pointing to a zero portnum means "assign a number"
//
static VMIOS_INTERCEPT_FN(sockOpenBlocking)
{
    // pointer to port for return
    Uns32 portp;
    getArg(processor, object, 0, &portp);

    // pointer to log file name
    Uns32 logAddr;
    getArg(processor, object, 1, &logAddr);

    // pointer to source file name
    Uns32 srcAddr;
    getArg(processor, object, 2, &srcAddr);

    Uns32 verbose;
    getArg(processor, object, 3, &verbose);

    if (portp && srcAddr) {
        vmiMessage("F", PREFIX, "Serial device cannot have two sources");
    }

    channelP   ch     = nextChannel(object);
    memDomainP domain = vmirtGetProcessorDataDomain(processor);

    ch->verbose = verbose;

    if (portp) {
        // read port number from pse
        Uns32 port;
        vmirtReadNByteDomain(domain, portp, &port, sizeof(port), 0, True);

        Uns32 result = createSocket(&port, object->portFile, ch->verbose);
        if (result >= 0){
            // write portnumber back to pse
            vmirtWriteNByteDomain(domain, portp, &port, sizeof(port), 0, True);
            ch->socketFD = result;
        }
    }
    if (logAddr) {
        const char *log = vmirtGetString(domain, logAddr);
        ch->log = fopen(log, "w");
        if(!ch->log) {
            vmiMessage("F", PREFIX, "Failed to create serial log file '%s'", log);
        }
    }
    if (srcAddr) {
        const char *src = vmirtGetString(domain, srcAddr);
        ch->src = fopen(src, "r");
        if(!ch->src) {
            vmiMessage("F", PREFIX, "Failed to find serial source file '%s'", src);
        }
    }
    // return index to channel descriptor
    Uns32 number = channelNumber(object, ch);
    vmiosRegWrite(processor, object->result, &number);
}



//
// Read from the socket
//
Bool socketHasData(Int32 fd)
{
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    Int32 r = select(fd+1, &fds, NULL, NULL, &timeout);
    if (r < 0) {
        vmiMessage("FS", PREFIX, "Select failed");
    }
    return (r != 0);
}

static VMIOS_INTERCEPT_FN(sockRead)
{
    Int32 chn;
    Uns32 bufPtr, bytes;
    getArg(processor, object, 0, &chn);
    getArg(processor, object, 1, &bufPtr);
    getArg(processor, object, 2, &bytes);

    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    channelP   ch     = getChannel(object, chn);
    Int32      result = 0;

    // A local buffer big enough for all the bytes
    Uns8 buffer[bytes+1];

    if (ch->socketFD >= 0) {
        if (socketHasData(ch->socketFD)) {
            result = recv(ch->socketFD, buffer, bytes, MSG_DONTWAIT);
            // r = 0 if other end of socket disappeared (do not terminate, though).
            if((result == 0) && object->finishOnDisconnect) {
                vmiMessage("I", PREFIX, "Socket disconnected - terminating simulation");
                vmirtFinish(0);
            }
            // r < 0 if no data
            if(result < 0) {
                result = 0;
            }
            if (result > 0) {
                if (result > bytes) {
                    vmiMessage("F", PREFIX, "recv got too many bytes");
                }
            }
        }
    } else if (ch->src) {
        result = fread(buffer, 1, bytes, ch->src);
    }
    // if any bytes, copy to pse space
    if (result) {
        vmirtWriteNByteDomain(domain, bufPtr, buffer, result, 0, True);
    }
    vmiosRegWrite(processor, object->result, &result);
}

//
// Write to the socket and log file.
// Return best result.
//
static VMIOS_INTERCEPT_FN(sockWrite)
{
    Uns32 chn, bufPtr, bytes;
    getArg(processor, object, 0, &chn);
    getArg(processor, object, 1, &bufPtr);
    getArg(processor, object, 2, &bytes);

    channelP    ch     = getChannel(object, chn);
    memDomainP  domain = vmirtGetProcessorDataDomain(processor);

    Uns8 buffer[bytes+1];
    vmirtReadNByteDomain(domain, bufPtr, buffer, bytes, 0, True);

    Uns32 sbytes=0, lbytes=0;
    if (ch->socketFD >= 0) {
        sbytes = send(ch->socketFD, buffer, bytes, 0);
        if((sbytes == -1) && object->finishOnDisconnect) {
            vmiMessage("I", PREFIX, "Socket disconnected - terminating simulation");
            vmirtFinish(0);
        }
    }
    if (ch->log) {
        lbytes = fwrite(buffer, 1, bytes, ch->log);

        // single characters tend to be buffered
        fflush(ch->log);
    }
    if (ch->verbose) {
        // convert non-printing characters to octal
        Uns32 max = (bytes*4) + 1;
        char tempBuf[max];
        Uns8 *src = buffer;
        char *dst = tempBuf;
        while(bytes) {
            Uns8 c = *src++;
            switch(c) {
                case '!' ... '~' :
                    *dst++ = c;
                    break;
                case '\n':
                    strcpy(dst, "\\n");
                    dst += 2;
                    break;
                case '\r':
                    strcpy(dst, "\\r");
                    dst += 2;
                    break;
                case ' ':
                    strcpy(dst, "SP");
                    dst += 2;
                    break;
                case '\t':
                    strcpy(dst, "TAB");
                    dst += 3;
                    break;
                default : {
                    int l = sprintf(dst, "\\0%o", c);
                    dst += l;
                    break;
                }
            }
            bytes--;
        }
        *dst = 0;
        vmiMessage("I", PREFIX, "%s", tempBuf);
    }
    Uns32 written = MAX(sbytes, lbytes);
    vmiosRegWrite(processor, object->result, &written);
}

static VMIOS_INTERCEPT_FN(sockClose)
{
    Uns32 chn;
    getArg(processor, object, 0, &chn);
    if (chn >= 0 && chn < MAX_CHANNELS) {
        closeChannel(object, chn);
    } else {
        int i;
        for (i = 0; i < MAX_CHANNELS; i++) {
            closeChannel(object, i);
        }
    }
    Uns32 r = 0;
    vmiosRegWrite(processor, object->result, &r);
}

//
// Constructor
//
static VMIOS_CONSTRUCTOR_FN(constructor)
{
    // return register (standard ABI)
    object->result   = vmiosGetRegDesc(processor, "eax");

    // stack pointer (standard ABI)
    object->sp       = vmiosGetRegDesc(processor, "esp");

    object->portFile = vmirtPlatformStringAttribute(processor, "portFile");
    object->finishOnDisconnect=(vmirtPlatformStringAttribute(processor, "finishOnDisconnect") != NULL);
}

//
// Destructor
//
static VMIOS_DESTRUCTOR_FN(destructor)
{
    //vmiMessage("I" ,"SER_DN", "Shutting down");
}

////////////////////////////////////////////////////////////////////////////////
// INTERCEPT ATTRIBUTES
////////////////////////////////////////////////////////////////////////////////

vmiosAttr modelAttrs = {

    ////////////////////////////////////////////////////////////////////////
    // VERSION
    ////////////////////////////////////////////////////////////////////////

    VMI_VERSION,                // version string (THIS MUST BE FIRST)
    "psesockets",               // description
    sizeof(vmiosObject),        // size in bytes of object

    ////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR/DESTRUCTOR ROUTINES
    ////////////////////////////////////////////////////////////////////////

    constructor,                // object constructor
    destructor,                 // object destructor

    ////////////////////////////////////////////////////////////////////////
    // INSTRUCTION INTERCEPT ROUTINES
    ////////////////////////////////////////////////////////////////////////

    0,                          // morph callback
    0,                          // get next instruction address
    0,                          // disassemble instruction

    ////////////////////////////////////////////////////////////////////////
    // ADDRESS INTERCEPT DEFINITIONS
    ////////////////////////////////////////////////////////////////////////
    // -------------------   -------   ------  -----------------
    // Name                  Address   Opaque  Callback
    // -------------------   -------   ------  -----------------
    {
        {"sockRead",            0,      True,   sockRead                   },
        {"sockWrite",           0,      True,   sockWrite                  },
        {"sockOpenBlocking",    0,      True,   sockOpenBlocking           },
        {"sockClose",           0,      True,   sockClose                  },
        {0}
    }
};
