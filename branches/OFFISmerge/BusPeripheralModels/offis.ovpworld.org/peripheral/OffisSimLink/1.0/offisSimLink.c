/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>

// tell simLink.h if we compile for PSE and not for semihost (Windwos DLL)
#define PSE
#include "offisSimLink.h"

//define no inline attribute for intercepted functions 
#define NOINLINE __attribute__((noinline)) 

#define MASK_BIT0       (0x0001)
#define MASK_BIT1       (0x0002)
#define MASK_BIT2       (0x0004)
#define MASK_BIT3       (0x0008)
#define MASK_BIT4       (0x0010)
#define MASK_BIT5       (0x0020)
#define MASK_BIT6       (0x0040)
#define MASK_BIT7       (0x0080)
#define MASK_BIT8       (0x0100)
#define MASK_BIT9       (0x0200)
#define MASK_BITA       (0x0400)
#define MASK_BITB       (0x0800)
#define MASK_BITC       (0x1000)
#define MASK_BITD       (0x2000)
#define MASK_BITE       (0x4000)
#define MASK_BITF       (0x8000)

////////////////////// Declaration of peripheral wide data structures ///////////////////////////////

OFFIS_sl_RegisterT OFFIS_sl_sPort; // structure with all public slave port registers
handles_type handles;              // structure of handles to ports and nets

//////////////////////////////// Bus Slave Ports ///////////////////////////////

// Offset | Regsiter
// =============================
// 0x0000 | irq0VectAddr (IRQ from Real Time Board)
// 0x0004 | irq1VectAddr (IRQ from Main Board)
// 0x0008 | irq2VectAddr (IRQ from Communication Board)
// 0x000c | irq3VectAddr (IRQ from Power Board)
// 0x0010 | actuator0
// 0x0014 | sensor0
// 0x0018 | actuator1
// 0x001c | sensor1
//   ...  |   ...

static void installSlavePorts(void)
{
	handles.OFFIS_sl_sPort = ppmCreateSlaveBusPort("sp", (2*NumberActuatorSensor*4)+16);  // size in bytes +4*4 for IRQ vectors
}

/////////////////////////////////// Net Ports //////////////////////////////////

static void installNetPorts(void)
{
	// To write to this net, use ppmWriteNet(handles.IRQ*, value);
	handles.IRQo0  = ppmOpenNetPort("IRQo0"); // (IRQ from Real Time Board)
	handles.IRQo1  = ppmOpenNetPort("IRQo1"); // (IRQ from Main Board, UI)
	handles.IRQo2  = ppmOpenNetPort("IRQo2"); // (IRQ from Communication Board)
	handles.IRQo3  = ppmOpenNetPort("IRQo3"); // (IRQ from Power Board)
}

// When simulation is started without Matlab/Simulink available, we will
// not use the intercepted function below to connect to Simulink. Instead
// we use this dummy function to generate some slopes for testing sensor
// readout.
void syncMatlabDUMMY(OFFIS_sl_RegisterT *pOFFIS_sl_sPort)
{
	static int time = 0;

	if ((time % 1) == 0)
	{	// Repeat transmission of physical values every second
		//		// Physical sensor definitions. Values that describe the state of the device WAKD
		OFFIS_sl_sPort.sensorValue[23] = time;	// SENSOR_SAMPLETIME_WAKD	SENSOR(23)
		OFFIS_sl_sPort.sensorValue[24] = 255;	// SENSOR_ECPI_STAT			SENSOR(24)
		OFFIS_sl_sPort.sensorValue[25] = 254;	// SENSOR_ECPO_STAT			SENSOR(25)
		OFFIS_sl_sPort.sensorValue[26] = 20;	// SENSOR_DCS_CONDUCT_FCR	SENSOR(26)
		OFFIS_sl_sPort.sensorValue[27] = 21;	// SENSOR_DCS_CONDUCT_FCQ	SENSOR(27)
		OFFIS_sl_sPort.sensorValue[28] = 50;	// SENSOR_FLOW_BLOOD		SENSOR(28)
		OFFIS_sl_sPort.sensorValue[29] = 20;	// SENSOR_FLOW_FLUID		SENSOR(29)
		OFFIS_sl_sPort.sensorValue[30] = 22;	// SENSOR_BPSI_BLOODPRESI	SENSOR(30)
		OFFIS_sl_sPort.sensorValue[31] = 21;	// SENSOR_BPSO_BLOODPRESO	SENSOR(31)
		OFFIS_sl_sPort.sensorValue[32] = 19;	// SENSOR_FPSI_FLUIDPRESSI	SENSOR(32)
		OFFIS_sl_sPort.sensorValue[33] = 18;	// SENSOR_FPSO_FLUIDPRESSO	SENSOR(33)
		OFFIS_sl_sPort.sensorValue[34] = 0;		// SENSOR_BABD_STAT_BUBBLE	SENSOR(34)
		OFFIS_sl_sPort.sensorValue[35] = 253;	// SENSOR_MFSI_STAT_SWITCH	SENSOR(35)
		OFFIS_sl_sPort.sensorValue[36] = 252;	// SENSOR_MFSO_STAT_SWITCH	SENSOR(36)
		OFFIS_sl_sPort.sensorValue[36] = 251;	// SENSOR_MFSU_STAT_SWITCH	SENSOR(36) //???????? not there in simulink! therefore duplicated 36
		OFFIS_sl_sPort.sensorValue[37] = 23;	// SENSOR_FLOWREF_BLOOD		SENSOR(37)
		OFFIS_sl_sPort.sensorValue[38] = 24;	// SENSOR_FLOWREF_FLUID 	SENSOR(38)
		OFFIS_sl_sPort.sensorValue[39] = 0;		// SENSOR_BLD_STAT_LEAK		SENSOR(39)
		OFFIS_sl_sPort.sensorValue[40] = 0;		// SENSOR_FLD_STAT_LEAK		SENSOR(40)
		OFFIS_sl_sPort.sensorValue[41] = 250;	// SENSOR_STAT_POLARIZ		SENSOR(41)


		// Create a very high level of pressure to trigger an alarm every 140 sec.
		if (time % 140 == 0)
		{
			OFFIS_sl_sPort.sensorValue[31] = 999999;	// SENSOR_BPSO_BLOODPRESO	SENSOR(31)
		}

		// send the RTB interrupt for new values
		OFFIS_sl_sPort.irq0VectAddr = (OFFIS_sl_sPort.irq0VectAddr | MASK_BIT1);

		// OFFIS_sl_sPort.sensorValue[42] = SENSOR_POLARIZ_ONOFF	SENSOR(42)
		// OFFIS_sl_sPort.sensorValue[43] = SENSOR_POLARIZ_DIRECT	SENSOR(43)
		// OFFIS_sl_sPort.sensorValue[44] = SENSOR_POLARIZ_VOLTAGEREF	SENSOR(44)
		// OFFIS_sl_sPort.sensorValue[45] = SENSOR_POLARIZ_VOLTAGE	SENSOR(45)

		// Status "sensors"
		//		#define SENSOR_ECPISTAT			SENSOR(80)
		//		#define SENSOR_ECPOSTAT     	SENSOR(81)
		//		#define SENSOR_BPSISTAT			SENSOR(82)
		//		#define SENSOR_BPSOSTAT			SENSOR(83)
		//		#define SENSOR_FPSISTAT			SENSOR(84)
		//		#define SENSOR_FPSOSTAT			SENSOR(85)
		//		#define SENSOR_BTSSTAT			SENSOR(86)
		//		#define SENSOR_DCSSTAT			SENSOR(87)
		//		#define SENSOR_BLPUMPSTAT		SENSOR(88)
		//		#define SENSOR_FLPUMPSTAT		SENSOR(89)
		//		#define SENSOR_MFSISTAT			SENSOR(90)
		//		#define SENSOR_MFSOSTAT			SENSOR(91)
		//		#define SENSOR_MFSBLSTAT		SENSOR(92)
		//		#define SENSOR_POLARIZSTAT		SENSOR(93)
		//		#define SENSOR_RTMCBSTAT		SENSOR(94)
	}

	// Physiological sensor definitions. Values that describe the state of the patient's health
	if ((time % 60) == 0)
	{	// Repeat this every minute (sync is called every second!)
		OFFIS_sl_sPort.sensorValue[ 0] = time;	// SENSOR_SAMPLETIME_PATI	SENSOR(0)
		OFFIS_sl_sPort.sensorValue[ 1] = 1420;	// SENSOR_ECPO_NA			SENSOR(1)
		OFFIS_sl_sPort.sensorValue[ 2] = 1418;	// SENSOR_ECPI_NA			SENSOR(2)
		OFFIS_sl_sPort.sensorValue[ 3] = 45;	// SENSOR_ECPO_K			SENSOR(3)
		OFFIS_sl_sPort.sensorValue[ 4] = 43;	// SENSOR_ECPI_K			SENSOR(4)
		OFFIS_sl_sPort.sensorValue[ 5] = 33;	// SENSOR_ECPO_UREA			SENSOR(5)
		OFFIS_sl_sPort.sensorValue[ 6] = 31;	// SENSOR_ECPI_UREA			SENSOR(6)
		OFFIS_sl_sPort.sensorValue[ 7] = 12100;	// SENSOR_ECPO_CA			SENSOR(7)
		OFFIS_sl_sPort.sensorValue[ 8] = 12000;	// SENSOR_ECPI_CA			SENSOR(8)
		OFFIS_sl_sPort.sensorValue[ 9] = 9000;	// SENSOR_ECPO_MG			SENSOR(9)
		OFFIS_sl_sPort.sensorValue[10] = 8000;	// SENSOR_ECPI_MG			SENSOR(10)
		OFFIS_sl_sPort.sensorValue[11] = 260000;// SENSOR_ECPO_HCO3			SENSOR(11)
		OFFIS_sl_sPort.sensorValue[12] = 250000;// SENSOR_ECPI_HCO3			SENSOR(12)
		OFFIS_sl_sPort.sensorValue[13] = 660;	// SENSOR_ECPO_C4H9N3O2		SENSOR(13)
		OFFIS_sl_sPort.sensorValue[14] = 650;	// SENSOR_ECPI_C4H9N3O2		SENSOR(14)
		OFFIS_sl_sPort.sensorValue[15] = 12;	// SENSOR_ECPO_H2PO4		SENSOR(15)
		OFFIS_sl_sPort.sensorValue[16] = 14;	// SENSOR_ECPI_H2PO4		SENSOR(16)
		OFFIS_sl_sPort.sensorValue[17] = 74;	// SENSOR_ECPO_PH			SENSOR(17)
		OFFIS_sl_sPort.sensorValue[18] = 73;	// SENSOR_ECPI_PH			SENSOR(18)
		OFFIS_sl_sPort.sensorValue[19] = 375;	// SENSOR_BTSO_TEMPERATURE	SENSOR(19)
		OFFIS_sl_sPort.sensorValue[20] = 365;	// SENSOR_BTSI_TEMPERATURE	SENSOR(20)
		OFFIS_sl_sPort.sensorValue[21] = 360;	// SENSOR_ECPO_TEMPERATURE	SENSOR(21)
		OFFIS_sl_sPort.sensorValue[22] = 370;	// SENSOR_ECPI_TEMPERATURE	SENSOR(22)
		// send the RTB interrupt for new values
		OFFIS_sl_sPort.irq0VectAddr = (OFFIS_sl_sPort.irq0VectAddr | MASK_BIT0);
	}
	time++;

//	switch (time){
//		case 0 ... 11:{
//			// Doing nothing
//			break;
//		}
//		case (12):{
//			bhmMessage("I", PREFIX, "Causing IRQ from communication board: dataID_weightRequest!");
//			pOFFIS_sl_sPort->irq2VectAddr = 9;
//			break;
//		}
//		case 13 ... 21:{
//			// Doing nothing
//			break;
//		}
//		case (22):{
//			bhmMessage("I", PREFIX, "Causing IRQ from communication board: dataID_weightData!");
//			// Weight verified by patient is 79.3 kg
//			pOFFIS_sl_sPort->sensorValue[43] = 793;
//			pOFFIS_sl_sPort->irq2VectAddr = 8;
//			break;
//		}
//		case 23 ... 31:{
//			// Doing nothing
//			break;
//		}
//		case (32):{
//			bhmMessage("I", PREFIX, "Causing IRQ from communication board: dataID_weightDataOK!");
//			// Weight transmitted by scale is 81.0 kg
//			pOFFIS_sl_sPort->sensorValue[43] = 810;
//			pOFFIS_sl_sPort->irq2VectAddr = 10;
//			break;
//		}
//		default:{
//			bhmMessage("I", PREFIX, "OFFIS: No more dummy OFFIS Simlink values!");
//			break;
//		}
//	}
	// // Build a nice rising slope on Sensor0 which is Na+
	// // Starting at 132 we should see a warning, go away and come back.
	// pOFFIS_sl_sPort->sensorValue[0] = 1320000+j*166;
	// // Build a nice rising slope on Sensor1 which is K+
	// // Starting at 1 we should see a warning, go away and come back.
	// pOFFIS_sl_sPort->sensorValue[1] = 10000+j*83;
	// // Build a nice rising slope on Sensor2 which is Urea
	// // Starting at 0 we should see a warning, go away and come back.
	// pOFFIS_sl_sPort->sensorValue[2] = 0+j*166;
} 

///////////////////////////// Interface functions to host native DLL  ////////////////////////////

NOINLINE void syncMatlab(Uns32 p_sPort, Uns32 strlenSimulinkModelName, char * simulinkModelName) { 
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: syncMatlab should be intercepted"); 
} 

NOINLINE void startMatlab(Uns32 strlenSimulinkModelPath, char * simulinkModelPath, Uns32 strlenSimulinkModelName, char * simulinkModelName)
{
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: startMatlab should be intercepted"); 
}

NOINLINE void closeMatlab()
{ 
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: closeMatlab should be intercepted"); 
} 

///////////////////////////// MMR Generic callbacks ////////////////////////////

static PPM_VIEW_CB(view32) {  *(Uns32*)data = *(Uns32*)user; }

PPM_REG_WRITE_CB(wcb_irq0VectAddr) {
	// Writing to register irqVectAddr does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to clear the reset.
	ppmWriteNet(handles.IRQo0, 0);
	*(Uns32*)user = 0;
}
PPM_REG_WRITE_CB(wcb_irq1VectAddr) {
	// Writing to register irqVectAddr does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to clear the reset.
	ppmWriteNet(handles.IRQo1, 0);
}
PPM_REG_WRITE_CB(wcb_irq2VectAddr) {
	// Writing to register irqVectAddr does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to clear the reset.
	ppmWriteNet(handles.IRQo2, 0);
	bhmMessage("I", PREFIX, "Clearing IRQ2!"); 
	*(Uns32*)user = 0;
}
PPM_REG_WRITE_CB(wcb_irq3VectAddr) {
	// Writing to register irqVectAddr does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to clear the reset.
	ppmWriteNet(handles.IRQo3, 0);
}

PPM_REG_READ_CB(rcb_irqVectAddr) {
  // bhmMessage("W", PREFIX, "Reading from OFFIsLink IRQ address!"); 
  return *(Uns32*)user;
}

PPM_REG_READ_CB(rcb_sensor) {
  return *(Uns32*)user;
}

PPM_REG_WRITE_CB(wcb_sensor) {
  bhmMessage("F", PREFIX, "Writing to sensor address not defined!"); 
}

PPM_REG_READ_CB(rcb_actuator) {
  bhmMessage("W", PREFIX, "Reading from actuator address!"); 
  return *(Uns32*)user;
}

PPM_REG_WRITE_CB(wcb_actuator) {
  *(Uns32*)user = data;
}

PPM_CONSTRUCTOR_CB(periphConstructor);

PPM_CONSTRUCTOR_CB(constructor) {
  // YOUR CODE HERE (pre constructor)
  periphConstructor();
  // YOUR CODE HERE (post constructor)
}

PPM_DESTRUCTOR_CB(destructor) {
	// YOUR CODE HERE (destructor)
	
	Uns32 strlenSimulinkModelPath;
	if (bhmIntegerAttribute("strlenSimulinkModelPath", &strlenSimulinkModelPath)) {
		// bhmMessage("I", PREFIX, "The model path has #characters: %d", strlenSimulinkModelPath); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelPath\" could not be read!"); 
	}
	Uns32 strlenSimulinkModelName;
	if (bhmIntegerAttribute("strlenSimulinkModelName", &strlenSimulinkModelName)) {
		// bhmMessage("I", PREFIX, "The model Name has #characters: %d", strlenSimulinkModelName); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelName\" could not be read!"); 
	}
	char *simulinkModelPath = malloc(strlenSimulinkModelPath);
	if (bhmStringAttribute("simulinkModelPath", simulinkModelPath, strlenSimulinkModelPath)) { 
		// bhmMessage("I", PREFIX, "The model path is %s", simulinkModelPath); 
	} else{
		bhmMessage("F", PREFIX, "No model path to simulink model defined!"); 
	}
	char *simulinkModelName = malloc(strlenSimulinkModelName);
	if (bhmStringAttribute("simulinkModelName", simulinkModelName, strlenSimulinkModelName)) { 
		// bhmMessage("I", PREFIX, "The model Name is %s", simulinkModelName); 
	} else{
		bhmMessage("F", PREFIX, "No model Name to simulink model defined!"); 
	}
	if ((strcmp(simulinkModelName, "DUMMY")==0) && (strcmp(simulinkModelPath, "DUMMY")==0)) {
		// No Matlab/Simulink model has been defined. Cosimulation is disabled.
		// so no need to do anything. Else close opened Matlab session.
	} else {
		closeMatlab();
	}
}





//////////////////////////// Memory mapped registers ///////////////////////////

static void installRegisters(void) {

	// small helper function to convert integer values to strings
	char* itoa(int val, int base){	
		static char buf[32] = {0};
		int i = 30;
		do {
			buf[i--] = "0123456789abcdef"[val % base];
			} while( ( val /= base ) && i );
		return &buf[i+1];
	}

	char name[128];
	char *i2a;
	int i = 0;
	
	// First address on OffisSimLink bus interafce is the IRQ status register
	// whenever this register is not 0 OffisSimLink creates an IRQ on its output
	ppmCreateRegister(	"irq0VectAddr",						// name
						"irq0VectAddr",						// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						0,									// port offset in bytes
						4,									// register size in bytes
						rcb_irqVectAddr,					// Read call back function
						wcb_irq0VectAddr,					// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.irq0VectAddr),		// register in my data structure
						True
					);
	ppmCreateRegister(	"irq1VectAddr",						// name
						"irq1VectAddr",						// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						4,									// port offset in bytes
						4,									// register size in bytes
						rcb_irqVectAddr,					// Read call back function
						wcb_irq1VectAddr,					// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.irq1VectAddr),		// register in my data structure
						True
					);
	ppmCreateRegister(	"irq2VectAddr",						// name
						"irq2VectAddr",						// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						8,									// port offset in bytes
						4,									// register size in bytes
						rcb_irqVectAddr,					// Read call back function
						wcb_irq2VectAddr,					// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.irq2VectAddr),		// register in my data structure
						True
					);
	ppmCreateRegister(	"irq3VectAddr",						// name
						"irq3VectAddr",						// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						12,									// port offset in bytes
						4,									// register size in bytes
						rcb_irqVectAddr,					// Read call back function
						wcb_irq3VectAddr,					// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.irq3VectAddr),		// register in my data structure
						True
					);
	// depending on the defined number of Actuator-Sensor-Pairs that many regsiter are created.
	for (i = 0; i < NumberActuatorSensor; i++) {
		// defining the actuators
		i2a = itoa(i, 10);
		strcpy(name, "actuator");
		strcat(name, i2a);
		// printf(name); printf(" OFFSET:%x\n", (i*8));
		ppmCreateRegister(	name,								// name
							name,								// description
							handles.OFFIS_sl_sPort,				// WINDOW base
							((i*8)+16),							// port offset in bytes
							4,									// register size in bytes
							rcb_actuator,						// Read call back function
							wcb_actuator,						// write call back function
							view32,								// debug view ????
							&(OFFIS_sl_sPort.actuatorValue[i]),	// register in my data structure
							True
						);
		// defining the sensors
		strcpy(name, "sensor");
		strcat(name, i2a);
		// printf(name); printf(" OFFSET:%x,\n", (i*8)+4);
		ppmCreateRegister(	name,								// name
							name,								// description
							handles.OFFIS_sl_sPort,				// WINDOW base
							((i*8)+20),							// port offset in bytes
							4,									// register size in bytes
							rcb_sensor,							// Read call back function
							wcb_sensor,							// write call back function
							view32,								// debug view ????
							&(OFFIS_sl_sPort.sensorValue[i]),	// register in my data structure
							True
						);		
	}
}

////////////////////////////////// Constructor /////////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor) {
	installSlavePorts();
	installRegisters();
	installNetPorts();

	Uns32 strlenSimulinkModelPath;
	if (bhmIntegerAttribute("strlenSimulinkModelPath", &strlenSimulinkModelPath)) {
		// bhmMessage("I", PREFIX, "The model path has #characters: %d", strlenSimulinkModelPath); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelPath\" could not be read!"); 
	}
	Uns32 strlenSimulinkModelName;
	if (bhmIntegerAttribute("strlenSimulinkModelName", &strlenSimulinkModelName)) {
		// bhmMessage("I", PREFIX, "The model Name has #characters: %d", strlenSimulinkModelName); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelName\" could not be read!"); 
	}
	char *simulinkModelPath = malloc(strlenSimulinkModelPath);
	if (bhmStringAttribute("simulinkModelPath", simulinkModelPath, strlenSimulinkModelPath)) { 
		// bhmMessage("I", PREFIX, "The model path is %s", simulinkModelPath); 
	} else{
		bhmMessage("F", PREFIX, "No model path to simulink model defined!"); 
	}
	char *simulinkModelName = malloc(strlenSimulinkModelName);
	if (bhmStringAttribute("simulinkModelName", simulinkModelName, strlenSimulinkModelName)) { 
		// bhmMessage("I", PREFIX, "The model Name is %s", simulinkModelName); 
	} else{
		bhmMessage("F", PREFIX, "No model Name to simulink model defined!"); 
	}
  	if ((strcmp(simulinkModelName, "DUMMY")==0) && (strcmp(simulinkModelPath, "DUMMY")==0)) {
		bhmMessage("W", PREFIX, "No Matlab/Simulink model has been defined. Cosimulation is disabled."); 
		bhmMessage("W", PREFIX, "Instead OffisSimLink peripheral will generate dummy values!");
	} else {
		// Matlab/Simulink is available. Go for it!
		startMatlab(strlenSimulinkModelPath, simulinkModelPath, strlenSimulinkModelName, simulinkModelName);
	}
}

////////////////////////////////// Create Threads////////////////////////////////
#define size (32*2048)

char stackA[size]; 

void myThread(void *user)
{
	// Read SyncInterval, ModelName and ModelPath from SimLink Peripheral Attribute List
	Uns32 simSyncInterval;
	if (bhmIntegerAttribute("simSyncInterval", &simSyncInterval)) {
		// bhmMessage("I", PREFIX, "The model Name has #characters: %d", strlenSimulinkModelName); 
	} else {
		bhmMessage("F", PREFIX, "\"simSyncInterval\" could not be read!"); 
	}
	Uns32 strlenSimulinkModelPath;
	if (bhmIntegerAttribute("strlenSimulinkModelPath", &strlenSimulinkModelPath)) {
		// bhmMessage("I", PREFIX, "The model path has #characters: %d", strlenSimulinkModelPath); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelPath\" could not be read!"); 
	}
	Uns32 strlenSimulinkModelName;
	if (bhmIntegerAttribute("strlenSimulinkModelName", &strlenSimulinkModelName)) {
		// bhmMessage("I", PREFIX, "The model Name has #characters: %d", strlenSimulinkModelName); 
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenSimulinkModelName\" could not be read!"); 
	}
	char *simulinkModelPath = malloc(strlenSimulinkModelPath);
	if (bhmStringAttribute("simulinkModelPath", simulinkModelPath, strlenSimulinkModelPath)) { 
		// bhmMessage("I", PREFIX, "The model path is %s", simulinkModelPath); 
	} else{
		bhmMessage("F", PREFIX, "No model path to simulink model defined!"); 
	}
	char *simulinkModelName = malloc(strlenSimulinkModelName);
	if (bhmStringAttribute("simulinkModelName", simulinkModelName, strlenSimulinkModelName)) { 
		// bhmMessage("I", PREFIX, "The model Name is %s", simulinkModelName); 
	} else{
		bhmMessage("F", PREFIX, "No model Name to simulink model defined!"); 
	}
  
	if ((strcmp(simulinkModelName, "DUMMY")==0) && (strcmp(simulinkModelPath, "DUMMY")==0)) {
		// Starting endless loop with dummy generation inside
		// OffisSimLink is disabled!
		while(1) {
			syncMatlabDUMMY(&OFFIS_sl_sPort);
			if (OFFIS_sl_sPort.irq0VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo0, 1);
			}
			if (OFFIS_sl_sPort.irq1VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo1, 1);
			}
			if (OFFIS_sl_sPort.irq2VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo2, 1);
			}
			if (OFFIS_sl_sPort.irq3VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo3, 1);
			}
			bhmWaitDelay(simSyncInterval*1000);  // time to wait in micro seconds
		}
	} else {
		// Starting endless loop with intercepted function to communicate with matlab simulink.
		while(1) {
			syncMatlab((Uns32)&OFFIS_sl_sPort, strlenSimulinkModelName, simulinkModelName);
			if (OFFIS_sl_sPort.irq0VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo0, 1);
			}
			if (OFFIS_sl_sPort.irq1VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo1, 1);
			}
			if (OFFIS_sl_sPort.irq2VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo2, 1);
			}
			if (OFFIS_sl_sPort.irq3VectAddr != 0) {
				// Interrupt from Simulink detected!
				ppmWriteNet(handles.IRQo3, 1);
			}
			bhmWaitDelay(simSyncInterval*1000);  // time to wait in micro seconds
		}
	}
}

void userInit(void)
{
  struct myThreadcontext { Uns32 myThreadData1; Uns32 myThreadData2; } contextA;
  bhmCreateThread(myThread, &contextA, "threadA", &stackA[size]);
}

///////////////////////////////////// Main /////////////////////////////////////

int main(int argc, char *argv[]) {
  constructor();
  userInit();
  bhmWaitEvent(bhmGetSystemEvent(BHM_SE_END_OF_SIMULATION));
  destructor();
  return 0;
}
