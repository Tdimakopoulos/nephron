#
set desc "Imperas  PL011 ARM-Style UART"
# Specification C/O QEMU
#

# A big hole in the middle, a few regs at each end
set range 0x1000

imodelnewperipheral \
    -imagefile pse.pse \
    -name UartPL011 \
    -vendor  arm.ovpworld.org  \
    -library peripheral \
    -version 1.0  \
    -constructor constructor \
    -destructor closeDown \
    -extensionfile model \
    -attributetable  modelAttrs
    
iadddocumentation -name Description -text $desc

imodeladdbusslaveport -name bport1 -size $range
imodeladdaddressblock -name ab     -port bport1 -width 32 -size $range

imodeladdformal -name variant -type enumeration
    imodeladdenumeration -formal variant -name ARM
    imodeladdenumeration -formal variant -name LUMINARY

imodeladdformal -name portnum  -type integer
imodeladdformal -name outfile  -type string
imodeladdformal -name infile   -type string
imodeladdformal -name log      -type integer
imodeladdformal -name finishOnDisconnect -type string

set addBlock     "bport1/ab"
set regWidth     32

#   name  offset ac description        readFn   writeFn   bits
set registers {
    dr          0      rw UARTDR       readDR   writeDR   0
	ecr         1      rw UARTECR      readECR  0         { TXFE 7 RXFF 6 TXFF 5 RXFE 4 }
    flags       6      rw UARTFR       0        0         0
    ilpr        8      rw UARTILPR     0        0         0
    ibrd        9      rw UARTIBRD     0        0         0
    vbrd        10     rw UARTFBRD     0        0         0
    lcr         11     rw UARTLCR_H    0        writeLCR  0
    cr          12     rw UARTCR       0        0         0
    ifl         13     rw UARTIFS      0        writeIFL  0
    int_enabled 14     rw UARTIMSC     0        writeIMSC 0
    int_level   15     r  UARTRIS      0        0         { TX 5 RX 4 }
    mis         16     r  UARTMIS      readMIS  0         0
    icr         17      w UARTDICR     0        writeICR  0
    dmacr       18     rw UARTDMACR    0        writeDMA  0
}

foreach { rname roffset raccess  desc readFn writeFn  bits } $registers {
    if { $readFn != 0 } {
        set r "-readfunction $readFn"
    } else { set r "" }
    if { $writeFn != 0 } {
        set w "-writefunction $writeFn"
    } else { set w "" }
    set offset [expr 4 * $roffset]
    set cmd "imodeladdmmregister -width 8 -name $rname -addressblock $addBlock -offset $offset -access $raccess $r $w"
	
    eval $cmd
    if { $desc != 0 } {
        iadddocumentation -name Description -text $desc -handle "$addBlock/$rname"
    }

    if { $bits != 0 } {
        foreach { n o } $bits {
            imodeladdfield -mmregister "$addBlock/$rname" -bitoffset $o -name $n
        }
    }
}

# ARM id codes
for { set i 0 } { $i < 8 } { incr i } {
    imodeladdmmregister -name id$i -offset [expr 4 * ($i + 1016)] -addressblock $addBlock -access r -readfunction readID
}

imodeladdnetport -name irq -type output
