The OFFIS Nephron virtual platform simulation can be configured using the
following environment variables:

export OFFIS_ELF=../FreeRTOSV6.1.0/rtosdemo.elf
export OFFIS_SIMEND=60
export OFFIS_SIMSTEP=1
export OFFIS_SIMDETAIL=0

$OFFIS_ELF points the path to the application elf file to be uploaded
to the platforms flash-memory and executed on the processor. If $OFFIS_ELF
is not defined, the default path "../FreeRTOSV6.1.0/rtosdemo.elf" is assumed.

$OFFIS_SIMEND defines the wall-clock time (simulated real time) in seconds
after which the simulation ends. 60 means therefor simulation of 1 minute.
If $OFFIS_SIMEND is not defined, the dafault value of 300 seconds (5 min) is
assumed.

$OFFIS_SIMSTEP defines a period after which the platform will give a message
of the current simulated wall-clock time. If not defined or if the step size
is larger than the simulation end time, messages will be disabled.

$OFFIS_SIMDETAIL tunrns on debugging information. To be further implemented
by OFFIS. Also refer to the "ICM_ATTR_TRACE*" attributes in the OVP manual.
Currently the value of this environment variable has not really an implemented
functionality.

$OFFIS_SIMULINKSYNCINTERVAL_MS defines the timeperiod in ms at which OVBsim
is synchronizing its shared register values with Matlab/Simulink. A value change
in one of the two simulation environments will be undetected by the other until
sync. If period is too large, multiple value changes might be unnoticed. if value
is too small, sync overhead can become too large, slowing down simulation performance.

$OFFIS_SIMULINKMODEL defines the path to the simulink model (.mdl) to be started and cosimulated 
with OVP. If undefined, cosimulation bus interface model will not be connected to uC 
memory space. So SW reading and writing to registers mapped to co simulation will fail!
Matlab/Simulink will start at the location of .mdl file. So expect any log files etc there
and not at path where OVP was startedl.
