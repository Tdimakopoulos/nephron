/*
    FreeRTOS V6.1.0 - Copyright (C) 2010 Real Time Engineers Ltd.

    ***************************************************************************
    *                                                                         *
    * If you are:                                                             *
    *                                                                         *
    *    + New to FreeRTOS,                                                   *
    *    + Wanting to learn FreeRTOS or multitasking in general quickly       *
    *    + Looking for basic training,                                        *
    *    + Wanting to improve your FreeRTOS skills and productivity           *
    *                                                                         *
    * then take a look at the FreeRTOS books - available as PDF or paperback  *
    *                                                                         *
    *        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
    *                  http://www.FreeRTOS.org/Documentation                  *
    *                                                                         *
    * A pdf reference manual is also available.  Both are usually delivered   *
    * to your inbox within 20 minutes to two hours when purchased between 8am *
    * and 8pm GMT (although please allow up to 24 hours in case of            *
    * exceptional circumstances).  Thank you for your support!                *
    *                                                                         *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/* Scheduler includes. */
#include "nephron.h"
#include <stdio.h>

/* Constants required to handle interrupts. */
// #define portTIMER_MATCH_ISR_BIT		( ( unsigned char ) 0x01 )
#define portCLEAR_VIC_INTERRUPT		( ( unsigned long ) 0 )

/* Constants required to handle critical sections. */
#define portNO_CRITICAL_NESTING		( ( unsigned long ) 0 )
volatile unsigned long ulCriticalNesting = 9999UL;

/*-----------------------------------------------------------*/

/* ISR to handle manual context switches (from a call to taskYIELD()). */
void vPortYieldProcessor( void ) __attribute__((interrupt("SWI"), naked));

/* 
 * The scheduler can only be started from ARM mode, hence the inclusion of this
 * function here.
 */
void vPortISRStartFirstTask( void );
/*-----------------------------------------------------------*/

void vPortISRStartFirstTask( void )
{
  /* Simply start the scheduler.  This is included here as it can only be
     called from ARM mode. */
  portRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

/*
 * Called by portYIELD() or taskYIELD() to manually force a context switch.
 *
 * When a context switch is performed from the task level the saved task 
 * context is made to look as if it occurred from within the tick ISR.  This
 * way the same restore context function can be used when restoring the context
 * saved from the ISR or that saved from a call to vPortYieldProcessor.
 */
void vPortYieldProcessor( void )
{
  /* Within an IRQ ISR the link register has an offset from the true return 
     address, but an SWI ISR does not.  Add the offset manually so the same 
     ISR return code can be used in both cases. */
  __asm volatile ( "ADD		LR, LR, #4" );
  
  /* Perform the context switch.  First save the context of the current task. */
  portSAVE_CONTEXT();
  
  /* Find the highest priority task that is ready to run. */
  __asm volatile ( "bl vTaskSwitchContext" );
  
  /* Restore the context of the new task. */
  portRESTORE_CONTEXT();	
}
/*-----------------------------------------------------------*/

/* 
 * The ISR used for the scheduler tick.
 */
void vTickISR( void ) __attribute__((naked));
void vTickISR( void )
{
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();
  
	// taskMessage("I", "vTickISR", "ISR Start");

	// Increment the RTOS tick count, then look for the highest priority
	// task that is ready to run.
	__asm volatile( "bl vTaskIncrementTick" );
  
	#if configUSE_PREEMPTION == 1
		__asm volatile( "bl vTaskSwitchContext" );
	#endif

	// taskMessage("I", "vTickISR", "ISR End");
  
	/* Ready for the next interrupt. */
	/* OFFIS: NOT AVAILABLE ON OVP (yet) T0_IR = portTIMER_MATCH_ISR_BIT; */
	VICVectAddr = portCLEAR_VIC_INTERRUPT;

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
}

/*-----------------------------------------------------------*/


//#ifdef VP_SIMULATION
//	// Virtual Platform code here! Must not end up in final version!
//	// The following global variable is been used by a dummy "getData" fuction.
//	// Usually data would come in via a serial interface and getData pulls it from
//	// the serial interface buffer.
//	// For simulation with Simulink there is not serial interface and the dummy getData
//	// pulls the values from simulink it self. To know which data to pull, this global variable is used.
//	extern uint32_t dummy_WhatDataToPullRTB;
//#endif

/*
 * ***************************************************************************************************
 * Interrupt Service Routine to handle IRQ on serial interface to real time board.
 * ***************************************************************************************************
 */

#ifdef VP_SIMULATION
	extern uint8_t newStaticBufferPhysiologicalData;
	extern uint8_t newStaticBufferPhysicalData;
	extern uint8_t newStaticBufferStatusData;
	extern uint8_t newStaticBufferActuatorData;
	extern tdWakdStates	staticBufferStateRTB;
	extern tdWakdOperationalState staticBufferOperationalStateRTB;

#endif

#define PREFIX "ISR_RTB"
void vISR_RTB( void ) __attribute__((naked));
void vISR_RTB( void )
{
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();	

	// taskMessage("I", "*********vISR_RTB", "ISR Start");

	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	tdQtoken Qtoken;
	Qtoken.pData = NULL;

	#ifdef VP_SIMULATION
		// read value of irqVectAddr to know what the IRQ stands for; what action has to be taken?
		// In the real HW this information is part of the msg header in the transmitted buffer.
		// Our Simulink model does not generate this header though. So this is a workaround implementation
		// that will have to be enhanced in the release code for the main board.
		uint32_t irq0VectAddr = *(REG32 IRQ0VECTADDR);

		// Might be that (due to simulation artifact!) more than one data block arrives.
		// We need to count them to send the appropriate number of commands to the Dispatcher Task
		uint8_t i = 0;
		if ( irq0VectAddr & MASK_BIT0 )
		{	// Physiological sensor read IRQ
			#if defined SEQ6
				taskMessage("I", PREFIX, "New Physiological Data in UART buffer. Inform dispatcher task.");
			#endif
			newStaticBufferPhysiologicalData = 1;
			i++;
		}
		if ( irq0VectAddr & MASK_BIT1 )
		{	// Physical sensor read IRQ
			#if defined SEQ7
				taskMessage("I", PREFIX, "New Physical Data in UART buffer. Inform dispatcher task.");
			#endif
			newStaticBufferPhysicalData = 1;
			i++;
		}
		if ( irq0VectAddr & MASK_BIT2 )
		{	// Actuator read IRQ
			#if defined SEQ8
				taskMessage("I", PREFIX, "New Actuator Data in UART buffer. Inform dispatcher task.");
			#endif
			newStaticBufferActuatorData = 1;
			i++;
		}
		if ( irq0VectAddr & MASK_BIT3 )
		{	// Actuator read IRQ
			#if defined SEQ8
				taskMessage("I", PREFIX, "New Status Data in UART buffer. Inform dispatcher task.");
			#endif
			newStaticBufferStatusData = 1;
			i++;
		}
	#endif

	// Inform system that CB has some data for us!
	while (i>0)
	{
		Qtoken.command = command_BufferFromRTBavailable;
		if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
		{
			// Seemingly the queue is full and we already waited for a long time
			taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'command_BufferFromRTBavailable'");
			errMsgISR(errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask);
		}
		i--;
	}

	// taskMessage("I", "*********vISR_RTB", "ISR End");

	/* Ready for the next interrupt. */
	/* OFFIS: NOT AVAILABLE ON OVP (yet) T0_IR = portTIMER_MATCH_ISR_BIT; */
	VICVectAddr = portCLEAR_VIC_INTERRUPT;
	*(REG32 IRQ0VECTADDR) = portCLEAR_VIC_INTERRUPT;

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
}

/*
 * ***************************************************************************************************
 * Interrupt Service Routine to handle IRQ from MB board.
 * ***************************************************************************************************
 */

#define PREFIX_MB "ISR_MB"
void vISR_MB( void ) __attribute__((naked));
void vISR_MB( void ) {
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();	

	// taskMessage("I", "*********vISR_MB", "ISR Start");

	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	tdQtoken Qtoken;
	Qtoken.command = command_ButtonEvent;
	Qtoken.pData   = NULL;

	#ifdef VP_SIMULATION
		// read value of irqVectAddr to know what the IRQ stands for; what action has to be taken?
		// In the real HW this information is part of the msg header in the transmitted buffer.
		// Our Simulink model does not generate this header though. So this is a workaroung implementation
		// that will have to be enhanced in the release code for the main board.
		uint32_t irq1VectAddr = *(REG32 IRQ1VECTADDR);

		if ( irq1VectAddr & MASK_BIT5 )
		{
			// WAKD user interface RESET button event IRQ
			// Most likely in the final version of the SW this will not be implemented as a simple button.
			// If SW reset is implemented, this will require a sequence of buttons to navigate through the
			// WAKD user interface menu to select reset. Than UI would generate the reset. Here
			// for simulation and first prototype we do it as IRQ on a pressed simulated button.
			#if defined SEQ0
				taskMessage("I", PREFIX, "Reset button from WAKD. Inform dispatcher task.");
			#endif
			Qtoken.pData	= (void *) btn_reset;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! RESET button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}
		if ( irq1VectAddr & MASK_BIT4 )
		{
			// WAKD user interface DOWN button event IRQ
			// Most likely in the final version of the SW this will not be implemented via IRQ
			// but as a polling taskUI that checks the status of the buttons e.g. every 10 ms.
			Qtoken.pData	= (void *) btn_down;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! DOWN button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}
		if ( irq1VectAddr & MASK_BIT3 )
		{
			// WAKD user interface UP button event IRQ
			// Most likely in the final version of the SW this will not be implemented via IRQ
			// but as a polling taskUI that checks the status of the buttons e.g. every 10 ms.
			Qtoken.pData	= (void *) btn_up;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! UP button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}
		if ( irq1VectAddr & MASK_BIT2 ){
			// WAKD user interface RIGHT button event IRQ
			// Most likely in the final version of the SW this will not be implemented via IRQ
			// but as a polling taskUI that checks the status of the buttons e.g. every 10 ms.
			Qtoken.pData	= (void *) btn_right;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! RIGHT button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}
		if ( irq1VectAddr & MASK_BIT1 ){
			// WAKD user interface LEFT button event IRQ
			// Most likely in the final version of the SW this will not be implemented via IRQ
			// but as a polling taskUI that checks the status of the buttons e.g. every 10 ms.
			Qtoken.pData	= (void *) btn_left;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! Left button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}
		if ( irq1VectAddr & MASK_BIT0 ){
			// WAKD user interface SEL button event IRQ
			// Most likely in the final version of the SW this will not be implemented via IRQ
			// but as a polling taskUI that checks the status of the buttons e.g. every 10 ms.
			Qtoken.pData	= (void *) btn_sel;
			if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
			{
				// Seemingly the queue is full
				taskMessage("E", PREFIX_MB, "Queue of Dispatcher did not accept data! SEL button event was lost!");
				errMsgISR(errmsg_IsrMbButtonEventLost);
			}
		}

	#else
		#error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
	#endif

	// taskMessage("I", "*********vISR_MB", "ISR End");

	/* Clear interrupt, ready for the next interrupt. */
	VICVectAddr = portCLEAR_VIC_INTERRUPT;

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
}


/*
 * ***************************************************************************************************
 * Interrupt Service Routine to handle IRQ on serial interface to communication board.
 * ***************************************************************************************************
 */

// declaring the UART Buffer for communication board.
extern char uartBufferCBin[SIZEUARTBUFFERCB];

#define PREFIX_CB "ISR_CB"
void vISR_CB( void ) __attribute__((naked));
void vISR_CB( void ) {
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();	

	// taskMessage("I", "*********vISR_CB", "ISR Start");

	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	tdQtoken Qtoken;
	Qtoken.pData = NULL;

	#ifdef VP_SIMULATION
		// read value of irqVectAddr to know what the IRQ stands for; what action has to be taken?
		// In the real HW this information is part of the msg header in the transmitted buffer.
		// Our Simulink model does not generate this header though. So this is a workaroung implementation
		// that will have to be enhanced in the release code for the main board.
		uint32_t dummy_WhatDataToPullCB = *(REG32 IRQ2VECTADDR);

		switch(dummy_WhatDataToPullCB){
			case (1):{// Command change into state wakdStates_AllStopped
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_AllStopped;			// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (2):{// Command change into state wakdStates_Dialysis
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];			// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;			// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_Dialysis;			// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  		// toWakdOpState = stay in current state
				break;
			}
			case (3):{// Command change into state wakdStates_Regen1
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_Regen1;				// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (4):{// Command change into state wakdStates_Ultrafiltration
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_Ultrafiltration;		// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (5):{// Command change into state wakdStates_Regen2
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_Regen2;				// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (6):{// Command change into state wakdStates_Maintenance
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_Maintenance;			// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (7):{// Command change into state wakdStates_NoDialysate
				uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
				uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
				uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
				uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
				uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
				uartBufferCBin[8]   = wakdStates_NoDialysate;			// toWakdState
				uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
				break;
			}
			case (8):{// CB did receive weight data
				#if defined SEQ2
					taskMessage("I", PREFIX_CB, "Weight scale event. Inform dispatcher task.");
				#endif
				// Receive sensor data from Simulink environment
				uint16_t Weight   = *(REG32 SENSOR_WEIGHTSCALE);	// weight in 0.1 kg
				// Stuffing the bytes into UART buffer
				uartBufferCBin[0]   = whoId_MB;						// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;							// header.msgSize = 15 bytes (most significant byte)
				uartBufferCBin[2]   = 15;							// header.msgSize = 15 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_weightData;			// header.dataId = dataID_weightData
				uartBufferCBin[4]   = ++uartBufferCBin[4];			// header.msgCount
				uartBufferCBin[5]   = whoId_CB;						// header.issuedBy = CB
				// Payload
				uartBufferCBin[6]   = *(((char *)(&Weight))+1);	// take second byte of Weight
				uartBufferCBin[7]   = *(((char *)(&Weight))+0);	// take first byte of weight
				uartBufferCBin[8]   = 173; 						// body fat percentage in 0.1%
				uartBufferCBin[9]   = 0;	 						// WSBatteryLevel; ????? percentage in *1 % ?????
				uartBufferCBin[10]  = 100;	 					// WSBatteryLevel; ????? percentage in *1 % ?????
				uartBufferCBin[11]  = 1;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[12]  = 2;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[13]  = 3;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[14]  = 4;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				break;
			}
			case (9):{
				// Incoming request to connect to weight scale
				#if defined SEQ1
					taskMessage("I", PREFIX_CB, "Request to connect to weight scale. Inform dispatcher task.");
				#endif
				uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;					// header.msgSize = 6 bytes (most significant byte)
				uartBufferCBin[2]   = 0x06;					// header.msgSize = 6 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_weightRequest;	// header.dataId = dataID_weightRequest
				uartBufferCBin[4]   = ++uartBufferCBin[4];		// header.msgCount
				uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
				break;
			}
			case (10):{
				// User (patient) sends acknowledged weight measure
				#if defined SEQ3
					taskMessage("I", PREFIX_CB, "Patient sends Ack for weight. Inform dispatcher task.");
				#endif
				// Receive sensor data from Simulink environment
				uint16_t Weight   = *(REG32 SENSOR_WEIGHTSCALE);	// weight in 0.1 kg
				// Stuffing the bytes into UART buffer
				uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;					// header.msgSize = ?? bytes (most significant byte)
				uartBufferCBin[2]   = 15;						// header.msgSize = ?? bytes (least significant byte)
				uartBufferCBin[3]   = dataID_weightDataOK;	// header.dataId = dataID_weightData
				uartBufferCBin[4]   = ++uartBufferCBin[4];		// header.msgCount
				uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
				// Payload
				uartBufferCBin[6]   = *(((char *)(&Weight))+1);	// take second byte of Weight
				uartBufferCBin[7]   = *(((char *)(&Weight))+0);	// take first byte of weight
				uartBufferCBin[8]   = 173; 						// body fat percentage in 0.1%
				uartBufferCBin[9]   = 0;	 						// WSBatteryLevel; ????? percentage in *1 % ?????
				uartBufferCBin[10]  = 100;	 					// WSBatteryLevel; ????? percentage in *1 % ?????
				uartBufferCBin[11]  = 1;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[12]  = 2;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[13]  = 3;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				uartBufferCBin[14]  = 4;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
				break;
			}
			case (11):{
				// Smart Phone request an update on WAKD status.
				#if defined SEQ5
					taskMessage("I", PREFIX_CB, "Smart Phone requests an update on WAKD status.");
				#endif
				uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;					// header.msgSize = 6 bytes (most significant byte)
				uartBufferCBin[2]   = 0x06;					// header.msgSize = 6 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_statusRequest;	// header.dataId = dataID_statusRequest
				uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
				uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
				break;
			}
			case (12):{
				// Smart Phone requests a change of state. Prepare UART buffer for getData() to pick up
				#if defined SEQ4
					taskMessage("I", PREFIX_CB, "Smart Phone requests a change of state.");
				#endif
				uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
				uartBufferCBin[1]   = 0x00;					// header.msgSize = 10 bytes (most significant byte)
				uartBufferCBin[2]   = 0x0a;					// header.msgSize = 10 bytes (least significant byte)
				uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;
				uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
				uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
				uartBufferCBin[6]   = *(REG32 SENSOR_FROMSTATE);	// fromWakdState
				uartBufferCBin[7]   = *(REG32 SENSOR_FROMOPSTATE);	// fromWakdOpState = wakdStatesOS_Automatic
				uartBufferCBin[8]   = *(REG32 SENSOR_TOSTATE);		// toWakdState = wakdStates_AllStopped
				uartBufferCBin[9]   = *(REG32 SENSOR_TOOPSTATE);	// toWakdOpState = wakdStatesOS_Automatic
				break;
			}
		}
	#endif

	// Inform system that CB has some data for us!
	Qtoken.command = command_BufferFromCBavailable;
	if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhCBPAin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
	{
		// Seemingly the queue is full and we already waited for a long time
		taskMessage("E", PREFIX_CB, "Queue of CBPA did not accept 'command_BufferFromCBavailable'");
		errMsgISR(errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask);
	}

	// taskMessage("I", "*********vISR_CB", "ISR End");
		
	/* Ready for the next interrupt. */
	/* OFFIS: NOT AVAILABLE ON OVP (yet) T0_IR = portTIMER_MATCH_ISR_BIT; */
	VICVectAddr = portCLEAR_VIC_INTERRUPT;
	*(REG32 IRQ2VECTADDR) = portCLEAR_VIC_INTERRUPT;

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

/*
 * ***************************************************************************************************
 * Interrupt Service Routine to handle IRQ on serial interface to communication board.
 * ***************************************************************************************************
 */

#define PREFIX_UARTTOBTPHONE "ISR_COM_BT"
void vISR_COM_BT( void ) __attribute__((naked));
void vISR_COM_BT( void ) {
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();

	static uint32_t i = 0;
	static uint16_t msgSize = 0;

	uartBufferCBin[i] = (uint8_t) *(REG32 0x80020000);
	// taskMessage("I", PREFIX_UARTTOBTPHONE, "Receiving byte from phone via bluetooth: %x", uartBufferCBin[i]);
	if (i==2)
	{	// Byte 1 and 2 define the size of this message.
		*(((uint8_t *)(&msgSize)))   = uartBufferCBin[2];	// swap bytes for uint16_t
		*(((uint8_t *)(&msgSize))+1) = uartBufferCBin[1];	// swap bytes for uint16_t
		taskMessage("I", PREFIX_UARTTOBTPHONE, "Expecting message of %d Bytes.", msgSize);
	}
	i++;
	if (i == msgSize)
	{	// Message is complete inform system about available buffer.
		portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
		tdQtoken Qtoken;
		Qtoken.pData = NULL;
		Qtoken.command = command_BufferFromCBavailable;
		if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhCBPAin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
		{
			// Seemingly the queue is full and we already waited for a long time
			taskMessage("E", PREFIX_CB, "Queue of CBPA did not accept 'command_BufferFromCBavailable'");
			errMsgISR(errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask);
		}
		// Start new message at the beginning of buffer
		i=0;
	}

	/* Ready for the next interrupt. */
	VICVectAddr = portCLEAR_VIC_INTERRUPT;
	*(REG32 0x80020008) = portCLEAR_VIC_INTERRUPT;

	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

/*
 * The interrupt management utilities can only be called from ARM mode.  When
 * THUMB_INTERWORK is defined the utilities are defined as functions here to
 * ensure a switch to ARM mode.  When THUMB_INTERWORK is not defined then
 * the utilities are defined as macros in portmacro.h - as per other ports.
 */
#ifdef THUMB_INTERWORK

void vPortDisableInterruptsFromThumb( void ) __attribute__ ((naked));
void vPortEnableInterruptsFromThumb( void ) __attribute__ ((naked));

void vPortDisableInterruptsFromThumb( void )
{
  __asm volatile ( 
		  "STMDB SP!, {R0}	\n\t"	/* Push R0.			*/
		  "MRS	 R0, CPSR	\n\t"	/* Get CPSR.			*/
		  "ORR	 R0, R0, #0xC0	\n\t"	/* Disable IRQ, FIQ.		*/
		  "MSR	 CPSR, R0	\n\t"	/* Write back modified value.	*/
		  "LDMIA SP!, {R0}	\n\t"	/* Pop R0.			*/
		  "BX	 R14" );		/* Return back to thumb.	*/
}

void vPortEnableInterruptsFromThumb( void )
{
  __asm volatile ( 
		  "STMDB SP!, {R0}	\n\t"	/* Push R0.			*/	
		  "MRS	 R0, CPSR	\n\t"	/* Get CPSR.			*/
		  "BIC	 R0, R0, #0xC0	\n\t"	/* Enable IRQ, FIQ.		*/
		  "MSR	 CPSR, R0	\n\t"	/* Write back modified value.	*/	
		  "LDMIA SP!, {R0}	\n\t"	/* Pop R0.			*/
		  "BX	 R14" );		/* Return back to thumb.	*/
}

#endif /* THUMB_INTERWORK */

/* The code generated by the GCC compiler uses the stack in different ways at
   different optimisation levels.  The interrupt flags can therefore not always
   be saved to the stack.  Instead the critical section nesting level is stored
   in a variable, which is then saved as part of the stack context. */
void vPortEnterCritical( void )
{
  /* Disable interrupts as per portDISABLE_INTERRUPTS(); 		      */
  __asm volatile ( 
		  "STMDB SP!, {R0}     \n\t"	/* Push R0.                   */
		  "MRS	 R0, CPSR      \n\t"	/* Get CPSR.	              */
		  "ORR	 R0, R0, #0xC0 \n\t"	/* Disable IRQ, FIQ.	      */
		  "MSR	 CPSR, R0      \n\t"	/* Write back modified value. */
		  "LDMIA SP!, {R0}" );		/* Pop R0.                    */
  
  /* Now interrupts are disabled ulCriticalNesting can be accessed 
     directly.  Increment ulCriticalNesting to keep a count of how many times
     portENTER_CRITICAL() has been called. */
  ulCriticalNesting++;
}

void vPortExitCritical( void )
{
  if( ulCriticalNesting > portNO_CRITICAL_NESTING )
    {
      /* Decrement the nesting count as we are leaving a critical section. */
      ulCriticalNesting--;
      
      /* If the nesting level has reached zero then interrupts should be
	 re-enabled. */
      if( ulCriticalNesting == portNO_CRITICAL_NESTING )
	{
	  /* Enable interrupts as per portEXIT_CRITICAL().					*/
	  __asm volatile ( 
			  "STMDB SP!, {R0}	\n\t"	/* Push R0.			*/	
			  "MRS	 R0, CPSR	\n\t"	/* Get CPSR.			*/	
			  "BIC	 R0, R0, #0xC0	\n\t"	/* Enable IRQ, FIQ.		*/	
			  "MSR	 CPSR, R0	\n\t"	/* Write back modified value.	*/	
			  "LDMIA SP!, {R0}" );		/* Pop R0.			*/
	}
    }
}
