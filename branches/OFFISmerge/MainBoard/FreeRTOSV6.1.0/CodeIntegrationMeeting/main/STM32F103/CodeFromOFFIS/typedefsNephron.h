/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

// Initially task specific types were included in task specific header files.
// But it turned out to be that this made it necessary to include many header in many
// task from all over the place. So we decided to them all here in one header.

#ifndef TYPEDEFSNEPHRON_H
#define TYPEDEFSNEPHRON_H

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "includes_nephron/interfaces.h"
#include "includes_nephron/SPinterfaces.h"

#define sensorType    float
#define actuatorType  float

// Array to send dataBlocks between tasks. Size in Bytes
//#define sizeDataBlock 128
//typedef unsigned char  tdDataBlock[sizeDataBlock];

// main.c will create the queues using "xQueueCreate" which returns a pointer
// to that queue. The following is a global structure to access the queues.
typedef struct strAllQueueHandles
{
	xQueueHandle qhDISPin;		// input queue of Dispatcher task
	xQueueHandle qhUIin;     	// input queue of User Interface task
	xQueueHandle qhDSin;		// input queue of Data Storage task
	xQueueHandle qhSCCin;		// input queue of Safety task
    xQueueHandle qhFCin;		// input queue of Flow Control task
	xQueueHandle qhCBPAin;		// input queue of Communication Board Protocol Abstraction
	xQueueHandle qhREMINDERin;	// Input queue for reminder task of CBPA
	xQueueHandle qhRTBPAin;		// input queue of Flow Control task
	xQueueHandle qhPMBPA0in;	// input queue of Power Management Board 0 Task
	xQueueHandle qhPMBPA1in;	// input queue of Power Management Board 1 Task
	// xQueueHandle qhEGin;		// input queue of Event Generator (OUTDATED)
	// xQueueHandle qhSAFETYin;
	// xQueueHandle qhDUPin;
	// xQueueHandle qhMTNAin;
} tdAllQueueHandles;

// xTaskCreate creates a pointer to that task. The following is a global
// structure to access tasks
typedef struct strAllTaskHandles
{
	xTaskHandle handleDISP;
	xTaskHandle handleUI;
	xTaskHandle handleDS;
	xTaskHandle handleSCC;
	xTaskHandle handleWD;
	xTaskHandle handleFC;
	xTaskHandle handleCBPA;
	xTaskHandle handleReminder;
	xTaskHandle handleRTBPA;
	xTaskHandle handlePMBPA0;
	xTaskHandle handlePMBPA1;
	// xTaskHandle handleEG;
	// xTaskHandle handleSAFETY;
	// xTaskHandle handleDUP;
	// xTaskHandle handleMTN;
} tdAllTaskHandles;

// A struct that combines the two above
typedef struct strAllHandles
{
	tdAllQueueHandles allQueueHandles;
	tdAllTaskHandles  allTaskHandles;
} tdAllHandles;

// The power management boards need one additional information, if they are board 1 or 2
typedef struct strAllHandlesPMB
{
	uint8_t			instance;	// The WAKD has two PMB, so we instantiate the PMBAP Task twice
	tdAllHandles	*pAllHandles;
} tdAllHandlesPMB;

typedef enum enumErrMsg
{
	errmsg_TimeoutOnDispQueue,
	errmsg_TimeoutOnDsQueue,
	errmsg_TimeoutOnFcQueue,
	errmsg_TimeoutOnRtbpaQueue,
	errmsg_TimeoutOnCbpaQueue,
	errmsg_TimeoutOnSccQueue,
	errmsg_TimeoutOnUiQueue,
	errmsg_QueueOfDispatcherTaskFull,
	errmsg_QueueOfDataStorageTaskFull,
	errmsg_QueueOfSystemCheckCalibrationTaskFull,
	errmsg_QueueOfCommunicationBoardAbstractionTaskFull,
	errmsg_QueueOfRealtimeBoardAbstractionTaskFull,
	errmsg_QueueOfFlowControlTaskFull,
	errmsg_QueueOfReminderTaskFull,
	errmsg_QueueOfUserInterfaceTaskFull,
	errmsg_nackFromCB,
	errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask,
	errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask,
	errmsg_IsrMbButtonEventLost,
	errmsg_DispDataWeightSensorReadEventLost,
	errmsg_DispDataPatientProfielWriteEventLost,
	errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask,
	errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask,
	errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask,
	errmsg_DispChangeWAKDStateFromToEventLost,
	errmsg_pvPortMallocFailed,
	errmsg_getDataFailed,
	errmsg_putDataFailed,
	errmsg_ConfiguringWakdStateFailed,
	errmsg_ConfiguredStateTimesMismatch,
	errmsg_ErrFlowControl,
	errmsg_ErrSCC,
	errmsg_ErrSendNack,
	errmsg_ErrSendAck,
	errmsg_command_WakeupCallEventLost,
	errmsg_CommunicationBoardUnreachable,
	errmsg_NotInListForAckOnMsgCBPA,
	errmsg_PutDataFunctionFailed,
	errmsg_UnknownRecipient,
	errmsg_SDcardError,
	errmsg_FlowControlStateIsUnknown,
} tdErrMsg;


typedef enum enumTasks
{
	tasks_CBPA,
	tasks_RTBPA,
} tdTasks;

typedef struct strReminder
{
	uint8_t		 		sentTrials;
	portTickType        wakeUpTime;
	tdTasks		 		remindTask;
	void 		 		*pMsg;
	struct strReminder	*pPrevInList;
	struct strReminder	*pNextInList;
} tdReminder;

typedef enum strButtons
{
	btn_up,
	btn_down,
	btn_left,
	btn_right,
	btn_sel,
	btn_reset,
} tdButtons;

#endif
