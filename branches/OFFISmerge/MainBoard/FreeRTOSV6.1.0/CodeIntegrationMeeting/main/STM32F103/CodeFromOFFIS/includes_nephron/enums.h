/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef ENUMS_H
#define ENUMS_H 

// HW-interface API-functions need to know where data needs to go to or come from.
// By using the following enumeration type, API-functions decide on which UART to
// use to reach correct recipient/sender.
typedef enum enumWhoId
{
	whoId_nd,	// not defined, used whenever whoId is irrelevant (sender does not need Ack/Nack)
	whoId_MB,	// Main Board
	whoId_RTB,	// Real Time Board
	whoId_CB,	// Communication Board
	whoId_SP,	// Smart Phone
	whoId_PMB1,	// Power Management Board 1
	whoId_PMB2,	// Power management Board 2
	// Are some missing?
} tdWhoId; 

// WAKD states/modes as defined in specification document
// Embedded Software: Task Flow Control Version 10:36, 29 June 2011
/*
  IMPORTANT: numbers specified explicitly, to ensure consistency for a written csv-file!
			(otherwise when you change the enum and use an old csv-file the values e.g.
			state Regen1 might be read out as Ultrafiltration)
*/
typedef enum enumWakdStates
{
	wakdStates_UndefinedInitializing, // Internal state after PowerOn, initialize and proceed directly to AllStopped
	
	// NON OPERATIONAL States
	wakdStates_AllStopped,
	wakdStates_Maintenance,
	wakdStates_NoDialysate,			// formerly known as "BloodFlowOn"
	
	// OPERATIONAL States
	wakdStates_Dialysis,
	wakdStates_Regen1,
	wakdStates_Ultrafiltration,
	wakdStates_Regen2,

	// *** WARNING ***
	// Do not define more than 256 states (one byte limit)
	// since this enum is processed as an uint8_t

} tdWakdStates;

// WAKD states/modes as defined in specification document
// tdWakdOperationalState differentiates between 
typedef enum enumWakdOperationalState
{
	wakdStatesOS_UndefinedInitializing, // Internal state after PowerOn, initialize and proceed directly to OperationalState Automatic
	wakdStatesOS_Automatic, 	// OperationalState Automatic
	wakdStatesOS_Semiautomatic, // OperationalState Semi-Automatic (falling back to All Stopped after this)

	// *** WARNING ***
	// Do not define more than 256 states (one byte limit)
	// since this enum is processed as an uint8_t

} tdWakdOperationalState;


// Transmission channels (UART, BlueTooth, TCP/IP) transport data packages without
// interpretation byte for byte. The sender receives a bytewise copy of the transmitted data.
// The following enumaration type defines commands/actions/datatypes
typedef enum enumDataId
{
	dataID_undefined,
	/* generic commands/data relevant for all WhoId. */
	dataID_reset,						// Recipient should reset itself no data attached (refer to strMessageOnly)
	dataID_shutdown,					// request to put system in safe state to turn of power in the end.
	dataID_setTimeDate,					// Recipient should set Unix time according to attached data (refer to tdsetTime)
	dataID_currentWAKDstateIs,			// Recipient is informed about the current state of WAKD
	dataID_ack,
	dataID_nack,
	dataID_statusRequest,
	dataID_systemInfo,
	
	/* commands/data relevant for RTB */
	// configures actuator values for the states implemented in the RTB
	dataID_configureState,					// For RTB only: set WAKD actuators in state according to attached data (refer to tdActCtrl)
	dataID_configurePeriodPolarizerToggle,	// For RTB only: tdMsgPeriodPolarizerToggle
		
	// Command id that triggers to switch into another state. The RTB will automatically apply
	// the preconfigured actuator values (see above) whenever it changes into another state.
	dataID_changeWAKDStateFromTo,		// Request a state change from State A to B. (refer to tdWakdStateFromTo)
	
	// Command ids relevant for exchange of sensor data from RTB
	dataID_physiologicalData,			// Recipient gets all physioloical data in one set. (refer to tdPhysiologicalData)
	dataID_physiologicalData_K,
	dataID_physiologicalData_Na,
	dataID_physiologicalData_pH,
	dataID_physiologicalData_Ur,
	dataID_physicalData,				// Recipient gets all physical data in one set. (refer to tdPhysicalData)
	dataID_actuatorData,
	dataID_alarmRTB,					// Recipient is informed about alarm status from RTB. (refer to tdAlarms)
	dataID_statusRTB,
	
	/* commands/data relevant for CB */
	dataID_weightRequest,				// MessageOnly command to ask CB to connect to weight scale and receive new value.
	dataID_weightData,					// Recipient gets weight measurement. (refer to tdWeightData)
	dataID_weightDataOK,				// Weight data that was verified (and corrected) by the patient
	dataID_bpRequest,  // SHOULD BE OIN ONE BLOCK
	dataID_bpData,
	dataID_EcgRequest,
	dataID_EcgData,
	
	/*data to configure the WAKD control presets */
	dataID_PatientProfile,
	dataID_WAKDAllStateConfigure, 	// used for task FC, single state: dataID_configureState
	dataID_StateParametersSCC,	 	// used for task SCC
	dataID_AllStatesParametersSCC,	// used for task SCC
	dataID_configureStateDump,		//used to log the configutaion send out from FC to the RTB

	dataID_CurrentParameters,		// maintains IDs of parameters that current values WAKD should send to SP.
	dataID_ActualParameters,		// answer to dataID_CurrentParameters. Maintains name-value pairs of the parameters required.
	dataID_ConfigureParameters,		// Used by Smartphone (Exodus) to send new parameter data to WAKD (minboard, OFFIS)
} tdDataId;


// Mainly for Mainboard internal use. The following commands can be sent in between queues of FreeRTOS tasks
// so that receiving tasks knows what to do.
typedef enum enumCommand
{
	command_DefaultError,
//	command_DataPhysiologicalSensorRead,
//	command_DataPhysicalSensorRead,
//	command_DataActuatorSensorRead,
//	command_DataActuatorValuesFromFC,	// Now obsolete when RTB was decided to have states and MB is not controlling actuators directly
//	command_SemiAutomaticModeFC,		// Flow Control goes into Semi mode (after each state we jump back to state No Dialysate)
//	command_AutomaticModeFC,			// Flow Control goes into auto mode (loops through all four states: dialys, regen, ...)
//	command_ConfigureWakdState,			// Moved to "command_UseAtachedMsgHeader", the header info there contains "dataID_configureState"
	command_InitializeSCC,
	command_InitializeSCCReady,
//	command_DataSCCInitialized,
	command_ButtonEvent,
	command_InitializeFC,
	command_InitializeFCReady,
	command_InformGuiConnectBag,
	command_InformGuiDisconnectBag,
	command_BufferFromRTBavailable,
	command_BufferFromCBavailable,
	command_BufferFromPB1available,
	command_BufferFromPB2available,
//	command_TransmissionErrorRTB,		// e.g. CRC error while RTB tried to transmit data
//	command_TransmissionErrorCB,		// e.g. CRC error while CB tried to transmit data
//	command_TransmissionErrorPB1,		// e.g. CRC error while PB tried to transmit data
//	command_TransmissionErrorPB2,		// e.g. CRC error while PB tried to transmit data
	command_WaitOnMsgCBPA,				// msg to be sent to Reminder TAsk to list msg for reminding later if ACK arrived.
	command_AckOnMsgCBPA,				// msg to be sent to Reminder Task to unlist msg from reminding list.
	command_WakeupCall,					// msg from Reminder Task that ACK for some msg is missing: send again!
	command_UseAtachedMsgHeader,		// look into header for dataID_* to know what to do with this.
	command_RequestDatafromDS,
	command_RequestDatafromDSReady,
	command_getPhysioPatientData,
	command_RtbChangeIntoStateRequest,
	command_ConfigurationChanged,
} tdCommand;

typedef enum enumCParameter {

	// patient profile
	parameter_name,						// e.g. "Frank Poppen" This is a STRING!!!
	// name[50]; is a string does this "fit into" the defined transmission? How to transmit this?
	parameter_gender,					// m/f
	parameter_measureTimeHours,			// daily time for weight & bp measurement, hour
	parameter_measureTimeMinutes,		// daily time for weight & bp measurement, minute
	parameter_wghtCtlTarget,			// weight control target [same unit as weigth measurement on RTB]
	parameter_wghtCtlLastMeasurement,	// weight control last measurement [same unit as weigth measurement on RTB]
	parameter_wghtCtlDefRemPerDay,		// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
	parameter_kCtlTarget,				// potassium control target use FIXPOINTSHIFT for [mmol/l]
	parameter_kCtlLastMeasurement,		// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
	parameter_kCtlDefRemPerDay,			// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
	parameter_urCtlTarget,				// urea control target use FIXPOINTSHIFT for [mmol/l]
	parameter_urCtlLastMeasurement,		// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
	parameter_urCtlDefRemPerDay,		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
	parameter_minDialPlasFlow,			// minimal flowrate for plasma in Dialysis mode  ml/min
	parameter_maxDialPlasFlow,			// maximal flowrate for plasma in Dialysis mode  ml/min
	parameter_minDialVoltage,			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
	parameter_maxDialVoltage,			// maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT

	// To be configured for every state of "enumWakdStates"
	parameter_defSpeedBp_mlPmin,		// setting speed blood pump [ml/min]
	parameter_defSpeedFp_mlPmin,		// setting speed fluidic pump [ml/min]
	parameter_defPol_V,					// setting voltage polarization [V]
	parameter_direction,				// polarity of polarization voltage.
	parameter_duration_sec,				// pre-setting for duration of "enumWakdStates"

	// WAKD parameters
	parameter_NaAbsL,		    // lower Na absolute threshold
	parameter_NaAbsH,     		// upper Na absolute threshold
	parameter_NaTrdLT,			// lower Na time period
	parameter_NaTrdHT,			// upper Na time period
	parameter_NaTrdLPsT,		// lower Na trend per Specified Time threshold
	parameter_NaTrdHPsT,		// upper Na trend per Specified Time threshold
	parameter_KAbsL,			// lower K absolute threshold
	parameter_KAbsH,			// upper K absolute threshold
	parameter_KAAL,				// lower K absolute alarm threshold
	parameter_KAAH,				// upper K absolute alarm threshold
	parameter_KTrdLT,			// lower K trend low time-difference
	parameter_KTrdHT,			// upper K trend low time-difference
	parameter_KTrdLPsT,			// lower K trend per Specified Time threshold
	parameter_KTrdHPsT,			// upper K trend per Specified Time threshold
	parameter_CaAbsL,			// lower Ca absolute threshold
	parameter_CaAbsH,			// upper Ca absolute threshold
	parameter_CaAAbsL,			// lower K absolute alarm threshold
	parameter_CaAAbsH,			// upper K absolute alarm threshold
	parameter_CaTrdLT,			// lower Ca time period
	parameter_CaTrdHT,			// upper Ca time period
	parameter_CaTrdLPsT,		// lower Ca trend per Specified Time threshold
	parameter_CaTrdHPsT,		// upper Ca trend per Specified Time threshold
	parameter_UreaAAbsH,		// upper Urea absolute alarm threshold
	parameter_UreaTrdHT,		// upper Urea time period
	parameter_UreaTrdHPsT,		// upper Urea trend per Specified Time
	parameter_CreaAbsL,			// lower Crea absolute threshold
	parameter_CreaAbsH,			// upper Crea absolute threshold
	parameter_CreaTrdHT,		// upper Crea time period
	parameter_CreaTrdHPsT,		// upper Crea trend per Specified Time
	parameter_PhosAbsL,			// lower Phos absolute threshold
	parameter_PhosAbsH,			// higher Phos absolute threshold
	parameter_PhosAAbsH,		// upper Phos absolute alarm threshold
	parameter_PhosTrdLT,		// lower Phos time period
	parameter_PhosTrdHT,		// upper Phos time period
	parameter_PhosTrdLPsT,		// lower Phos trend per Specified Time threshold
	parameter_PhosTrdHPsT,		// upper Phos trend per Specified Time
	parameter_HCO3AAbsL,		// lower HCO3 absolute alarm threshold
	parameter_HCO3AbsL,			// lower HCO3 absolute threshold
	parameter_HCO3AbsH,			// upper HCO3 absolute threshold
	parameter_HCO3TrdLT,		// lower HCO3 time period
	parameter_HCO3TrdHT,		// upper HCO3 time period
	parameter_HCO3TrdLPsT,		// lower HCO3 trend per Specified Time threshold
	parameter_HCO3TrdHPsT,		// upper HCO3 trend per Specified Time
	parameter_PhAAbsL,			// lower pH absolute alarm threshold
	parameter_PhAAbsH,			// upper pH absolute alarm threshold
	parameter_PhAbsL,			// lower pH absolute threshold
	parameter_PhAbsH,			// upper pH absolute threshold
	parameter_PhTrdLT,			// lower pH time period
	parameter_PhTrdHT,			// upper pH time period
	parameter_PhTrdLPsT,		// lower pH trend per Specified Time threshold
	parameter_PhTrdHPsT,		// upper pH trend per Specified Time threshold
	parameter_BPsysAbsL,		// lower BPsys absolute threshold
	parameter_BPsysAbsH,		// upper BPsys absolute threshold
	parameter_BPsysAAbsH,		// upper BPsys absolute alarm threshold
	parameter_BPdiaAbsL,		// lower BPdia absolute threshold
	parameter_BPdiaAbsH,		// upper BPdia absolute threshold
	parameter_BPdiaAAbsH,		// upper BPdia absolute alarm threshold
	parameter_pumpFaccDevi,		// accepted flow deviation ml/min
	parameter_pumpBaccDevi,		// accepted flow deviation ml/min
	parameter_VertDeflecitonT,	// accepted vertical deflection threshold
	parameter_WghtAbsL,			// lower Weight absolute threshold
	parameter_WghtAbsH,			// upper Weight absolute threshold
	parameter_WghtTrdT,			// Weight thrend threshold
	parameter_FDpTL,			// Fluid Extraction Deviation Percentage Threshold Low
	parameter_FDpTH,			// Fluid Extraction Deviation Percentage Threshold
	parameter_BPSoTH,			// sensor 'BPSo' pressure upper threshold
	parameter_BPSoTL,			// sensor 'BPSo' pressure lower threshold
	parameter_BPSiTH,			// sensor 'BPSi' pressure upper threshold
	parameter_BPSiTL,			// sensor 'BPSi' pressure lower threshold
	parameter_FPS1TH,			// sensor 'FPS1' pressure upper threshold
	parameter_FPS1TL,			// sensor 'FPS1' pressure lower threshold
	parameter_FPS2TH,			// sensor 'FPS2' pressure upper threshold
	parameter_FPS2TL,			// sensor 'FPS2' pressure lower threshold
	parameter_BTSoTH,			// temperature sensor BTSo value upper threshold
	parameter_BTSoTL,			// temperature sensor BTSo value lower threshold
	parameter_BTSiTH,			// temperature sensor BTSi value upper threshold
	parameter_BTSiTL,			// temperature sensor BTSi value lower threshold
	parameter_BatStatT,			//battery status threshold
	parameter_f_K,
	parameter_SCAP_K,
	parameter_P_K,
	parameter_Fref_K,
	parameter_cap_K,
	parameter_f_Ph,
	parameter_SCAP_Ph,
	parameter_P_Ph,
	parameter_Fref_Ph,
	parameter_cap_Ph,
	parameter_A_dm2,
	parameter_CaAdr,
	parameter_CreaAdr,
	parameter_HCO3Adr,
	parameter_wgtNa,			// the fuzzy importance of the adsorption of this substance
	parameter_wgtK,
	parameter_wgtCa,
	parameter_wgtUr,
	parameter_wgtCrea,
	parameter_wgtPhos,
	parameter_wgtHCO3,
	parameter_mspT,				// set the threshold here!
} tdParameter;

typedef enum enumMsg 
{
	// here are messages send by the decision tree, if certain states detected
	// just like violations of thresholds for medical parameters
	// Prefix_ooRangeThresold
	// 'detected' indicates a occurred situation
	// a threshold under/overshoot or a detected anomaly or...
	// this is followed then by the name of the threshold/anomaly
	// afterwards the rest of the system needs to react, like when 'BPdiaAAbsH' exceeds:
	// 1. a msg to patient "contact physician" and
	// 2. a msg to physician "ALARM: BP-diastolic is high" NEED TO GO OFF!
	
	// blood pressure
	decTreeMsg_detectedBPsysAbsL, // #0
	decTreeMsg_detectedBPsysAbsH,
	decTreeMsg_detectedBPsysAAbsH,
	decTreeMsg_detectedBPdiaAbsL,
	decTreeMsg_detectedBPdiaAbsH,
	decTreeMsg_detectedBPdiaAAbsH,
	
	// weight
	dectreeMsg_detectedWghtAbsL, //#6
	dectreeMsg_detectedWghtAbsH,
	dectreeMsg_detectedWghtTrdT,
	
	//actuator data
	dectreeMsg_detectedpumpBaccDevi, //#9
	dectreeMsg_detectedpumpFaccDevi,
	
	// physical
	dectreeMsg_detectedBPorFPhigh, //#11
	dectreeMsg_detectedBPorFPlow,
	dectreeMsg_detectedBTSoTH,
	dectreeMsg_detectedBTSoTL,
	dectreeMsg_detectedBTSiTH,
	dectreeMsg_detectedBTSiTL,
	
	// physiological
	dectreeMsg_detectedSorDys, //#17
	dectreeMsg_detectedPhAAbsL,
	dectreeMsg_detectedPhAAbsH,
	dectreeMsg_detectedPhAbsLorPhAbsH, //#20
	dectreeMsg_detectedPhTrdLPsT,
	dectreeMsg_detectedPhTrdHPsT,
	dectreeMsg_detectedNaAbsLorNaAbsH,
	dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT,
	dectreeMsg_detectedKAbsLorKAbsHandSorDys, //#25
	dectreeMsg_detectedKAbsLorKAbsHnoSorDys,
	dectreeMsg_detectedKAAL,
	dectreeMsg_detectedKAAHandKincrease,
	dectreeMsg_detectedKAAHnoKincrease,
	dectreeMsg_detectedKTrdLPsT, //#30
	dectreeMsg_detectedKTrdHPsT,
	dectreeMsg_detectedKTrdofnriandSorDys,
	dectreeMsg_detectedPhTrdofnriandSorDys,
	dectreeMsg_detectedUreaAAbsHandSorDys,
	dectreeMsg_detectedUreaAAbsHnoSorDys,  //#35
	dectreeMsg_detectedUreaTrdHPsT,
	
	// Messages from Flow Control.
	flowControlMsg_excretionBagNotConnected,

} tdInfoMsg;
#endif
