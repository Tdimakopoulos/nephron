\contentsline {chapter}{\numberline {1}Data Structure Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Data Structures}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Data Structure Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}strActuators Struct Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Field Documentation}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}bloodPump}{5}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}fluidPump}{5}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}polarizer}{6}{subsubsection.3.1.2.3}
\contentsline {subsubsection}{\numberline {3.1.2.4}switch1}{6}{subsubsection.3.1.2.4}
\contentsline {subsubsection}{\numberline {3.1.2.5}switch2}{6}{subsubsection.3.1.2.5}
\contentsline {subsubsection}{\numberline {3.1.2.6}timestamp}{6}{subsubsection.3.1.2.6}
\contentsline {section}{\numberline {3.2}strParametersSCC Struct Reference}{6}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Field Documentation}{8}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}A\_\discretionary {-}{}{}dm2}{8}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}BatStatT}{9}{subsubsection.3.2.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.3}BPdiaAAbsH}{9}{subsubsection.3.2.2.3}
\contentsline {subsubsection}{\numberline {3.2.2.4}BPdiaAbsH}{9}{subsubsection.3.2.2.4}
\contentsline {subsubsection}{\numberline {3.2.2.5}BPdiaAbsL}{9}{subsubsection.3.2.2.5}
\contentsline {subsubsection}{\numberline {3.2.2.6}BPSiTH}{9}{subsubsection.3.2.2.6}
\contentsline {subsubsection}{\numberline {3.2.2.7}BPSiTL}{9}{subsubsection.3.2.2.7}
\contentsline {subsubsection}{\numberline {3.2.2.8}BPSoTH}{9}{subsubsection.3.2.2.8}
\contentsline {subsubsection}{\numberline {3.2.2.9}BPSoTL}{9}{subsubsection.3.2.2.9}
\contentsline {subsubsection}{\numberline {3.2.2.10}BPsysAAbsH}{9}{subsubsection.3.2.2.10}
\contentsline {subsubsection}{\numberline {3.2.2.11}BPsysAbsH}{9}{subsubsection.3.2.2.11}
\contentsline {subsubsection}{\numberline {3.2.2.12}BPsysAbsL}{10}{subsubsection.3.2.2.12}
\contentsline {subsubsection}{\numberline {3.2.2.13}BTSiTH}{10}{subsubsection.3.2.2.13}
\contentsline {subsubsection}{\numberline {3.2.2.14}BTSiTL}{10}{subsubsection.3.2.2.14}
\contentsline {subsubsection}{\numberline {3.2.2.15}BTSoTH}{10}{subsubsection.3.2.2.15}
\contentsline {subsubsection}{\numberline {3.2.2.16}BTSoTL}{10}{subsubsection.3.2.2.16}
\contentsline {subsubsection}{\numberline {3.2.2.17}CaAAbsH}{10}{subsubsection.3.2.2.17}
\contentsline {subsubsection}{\numberline {3.2.2.18}CaAAbsL}{10}{subsubsection.3.2.2.18}
\contentsline {subsubsection}{\numberline {3.2.2.19}CaAbsH}{10}{subsubsection.3.2.2.19}
\contentsline {subsubsection}{\numberline {3.2.2.20}CaAbsL}{10}{subsubsection.3.2.2.20}
\contentsline {subsubsection}{\numberline {3.2.2.21}CaAdr}{10}{subsubsection.3.2.2.21}
\contentsline {subsubsection}{\numberline {3.2.2.22}cap\_\discretionary {-}{}{}K}{11}{subsubsection.3.2.2.22}
\contentsline {subsubsection}{\numberline {3.2.2.23}cap\_\discretionary {-}{}{}Ph}{11}{subsubsection.3.2.2.23}
\contentsline {subsubsection}{\numberline {3.2.2.24}CaTrdHPsT}{11}{subsubsection.3.2.2.24}
\contentsline {subsubsection}{\numberline {3.2.2.25}CaTrdHT}{11}{subsubsection.3.2.2.25}
\contentsline {subsubsection}{\numberline {3.2.2.26}CaTrdLPsT}{11}{subsubsection.3.2.2.26}
\contentsline {subsubsection}{\numberline {3.2.2.27}CaTrdLT}{11}{subsubsection.3.2.2.27}
\contentsline {subsubsection}{\numberline {3.2.2.28}CreaAbsH}{11}{subsubsection.3.2.2.28}
\contentsline {subsubsection}{\numberline {3.2.2.29}CreaAbsL}{11}{subsubsection.3.2.2.29}
\contentsline {subsubsection}{\numberline {3.2.2.30}CreaAdr}{11}{subsubsection.3.2.2.30}
\contentsline {subsubsection}{\numberline {3.2.2.31}CreaTrdHPsT}{11}{subsubsection.3.2.2.31}
\contentsline {subsubsection}{\numberline {3.2.2.32}CreaTrdHT}{12}{subsubsection.3.2.2.32}
\contentsline {subsubsection}{\numberline {3.2.2.33}f\_\discretionary {-}{}{}K}{12}{subsubsection.3.2.2.33}
\contentsline {subsubsection}{\numberline {3.2.2.34}f\_\discretionary {-}{}{}Ph}{12}{subsubsection.3.2.2.34}
\contentsline {subsubsection}{\numberline {3.2.2.35}FDpTH}{12}{subsubsection.3.2.2.35}
\contentsline {subsubsection}{\numberline {3.2.2.36}FDpTL}{12}{subsubsection.3.2.2.36}
\contentsline {subsubsection}{\numberline {3.2.2.37}FPS1TH}{12}{subsubsection.3.2.2.37}
\contentsline {subsubsection}{\numberline {3.2.2.38}FPS1TL}{12}{subsubsection.3.2.2.38}
\contentsline {subsubsection}{\numberline {3.2.2.39}FPS2TH}{12}{subsubsection.3.2.2.39}
\contentsline {subsubsection}{\numberline {3.2.2.40}FPS2TL}{12}{subsubsection.3.2.2.40}
\contentsline {subsubsection}{\numberline {3.2.2.41}Fref\_\discretionary {-}{}{}K}{12}{subsubsection.3.2.2.41}
\contentsline {subsubsection}{\numberline {3.2.2.42}Fref\_\discretionary {-}{}{}Ph}{13}{subsubsection.3.2.2.42}
\contentsline {subsubsection}{\numberline {3.2.2.43}HCO3AAbsL}{13}{subsubsection.3.2.2.43}
\contentsline {subsubsection}{\numberline {3.2.2.44}HCO3AbsH}{13}{subsubsection.3.2.2.44}
\contentsline {subsubsection}{\numberline {3.2.2.45}HCO3AbsL}{13}{subsubsection.3.2.2.45}
\contentsline {subsubsection}{\numberline {3.2.2.46}HCO3Adr}{13}{subsubsection.3.2.2.46}
\contentsline {subsubsection}{\numberline {3.2.2.47}HCO3TrdHPsT}{13}{subsubsection.3.2.2.47}
\contentsline {subsubsection}{\numberline {3.2.2.48}HCO3TrdHT}{13}{subsubsection.3.2.2.48}
\contentsline {subsubsection}{\numberline {3.2.2.49}HCO3TrdLPsT}{13}{subsubsection.3.2.2.49}
\contentsline {subsubsection}{\numberline {3.2.2.50}HCO3TrdLT}{13}{subsubsection.3.2.2.50}
\contentsline {subsubsection}{\numberline {3.2.2.51}KAAH}{13}{subsubsection.3.2.2.51}
\contentsline {subsubsection}{\numberline {3.2.2.52}KAAL}{14}{subsubsection.3.2.2.52}
\contentsline {subsubsection}{\numberline {3.2.2.53}KAbsH}{14}{subsubsection.3.2.2.53}
\contentsline {subsubsection}{\numberline {3.2.2.54}KAbsL}{14}{subsubsection.3.2.2.54}
\contentsline {subsubsection}{\numberline {3.2.2.55}KTrdHPsT}{14}{subsubsection.3.2.2.55}
\contentsline {subsubsection}{\numberline {3.2.2.56}KTrdHT}{14}{subsubsection.3.2.2.56}
\contentsline {subsubsection}{\numberline {3.2.2.57}KTrdLPsT}{14}{subsubsection.3.2.2.57}
\contentsline {subsubsection}{\numberline {3.2.2.58}KTrdLT}{14}{subsubsection.3.2.2.58}
\contentsline {subsubsection}{\numberline {3.2.2.59}mspT}{14}{subsubsection.3.2.2.59}
\contentsline {subsubsection}{\numberline {3.2.2.60}NaAbsH}{14}{subsubsection.3.2.2.60}
\contentsline {subsubsection}{\numberline {3.2.2.61}NaAbsL}{14}{subsubsection.3.2.2.61}
\contentsline {subsubsection}{\numberline {3.2.2.62}NaTrdHPsT}{15}{subsubsection.3.2.2.62}
\contentsline {subsubsection}{\numberline {3.2.2.63}NaTrdHT}{15}{subsubsection.3.2.2.63}
\contentsline {subsubsection}{\numberline {3.2.2.64}NaTrdLPsT}{15}{subsubsection.3.2.2.64}
\contentsline {subsubsection}{\numberline {3.2.2.65}NaTrdLT}{15}{subsubsection.3.2.2.65}
\contentsline {subsubsection}{\numberline {3.2.2.66}P\_\discretionary {-}{}{}K}{15}{subsubsection.3.2.2.66}
\contentsline {subsubsection}{\numberline {3.2.2.67}P\_\discretionary {-}{}{}Ph}{15}{subsubsection.3.2.2.67}
\contentsline {subsubsection}{\numberline {3.2.2.68}PhAAbsH}{15}{subsubsection.3.2.2.68}
\contentsline {subsubsection}{\numberline {3.2.2.69}PhAAbsL}{15}{subsubsection.3.2.2.69}
\contentsline {subsubsection}{\numberline {3.2.2.70}PhAbsH}{15}{subsubsection.3.2.2.70}
\contentsline {subsubsection}{\numberline {3.2.2.71}PhAbsL}{15}{subsubsection.3.2.2.71}
\contentsline {subsubsection}{\numberline {3.2.2.72}PhosAAbsH}{16}{subsubsection.3.2.2.72}
\contentsline {subsubsection}{\numberline {3.2.2.73}PhosAbsH}{16}{subsubsection.3.2.2.73}
\contentsline {subsubsection}{\numberline {3.2.2.74}PhosAbsL}{16}{subsubsection.3.2.2.74}
\contentsline {subsubsection}{\numberline {3.2.2.75}PhosTrdHPsT}{16}{subsubsection.3.2.2.75}
\contentsline {subsubsection}{\numberline {3.2.2.76}PhosTrdHT}{16}{subsubsection.3.2.2.76}
\contentsline {subsubsection}{\numberline {3.2.2.77}PhosTrdLPsT}{16}{subsubsection.3.2.2.77}
\contentsline {subsubsection}{\numberline {3.2.2.78}PhosTrdLT}{16}{subsubsection.3.2.2.78}
\contentsline {subsubsection}{\numberline {3.2.2.79}PhTrdHPsT}{16}{subsubsection.3.2.2.79}
\contentsline {subsubsection}{\numberline {3.2.2.80}PhTrdHT}{16}{subsubsection.3.2.2.80}
\contentsline {subsubsection}{\numberline {3.2.2.81}PhTrdLPsT}{16}{subsubsection.3.2.2.81}
\contentsline {subsubsection}{\numberline {3.2.2.82}PhTrdLT}{17}{subsubsection.3.2.2.82}
\contentsline {subsubsection}{\numberline {3.2.2.83}pumpBaccDevi}{17}{subsubsection.3.2.2.83}
\contentsline {subsubsection}{\numberline {3.2.2.84}pumpFaccDevi}{17}{subsubsection.3.2.2.84}
\contentsline {subsubsection}{\numberline {3.2.2.85}SCAP\_\discretionary {-}{}{}K}{17}{subsubsection.3.2.2.85}
\contentsline {subsubsection}{\numberline {3.2.2.86}SCAP\_\discretionary {-}{}{}Ph}{17}{subsubsection.3.2.2.86}
\contentsline {subsubsection}{\numberline {3.2.2.87}UreaAAbsH}{17}{subsubsection.3.2.2.87}
\contentsline {subsubsection}{\numberline {3.2.2.88}UreaTrdHPsT}{17}{subsubsection.3.2.2.88}
\contentsline {subsubsection}{\numberline {3.2.2.89}UreaTrdHT}{17}{subsubsection.3.2.2.89}
\contentsline {subsubsection}{\numberline {3.2.2.90}VertDeflecitonT}{17}{subsubsection.3.2.2.90}
\contentsline {subsubsection}{\numberline {3.2.2.91}WghtAbsH}{17}{subsubsection.3.2.2.91}
\contentsline {subsubsection}{\numberline {3.2.2.92}WghtAbsL}{18}{subsubsection.3.2.2.92}
\contentsline {subsubsection}{\numberline {3.2.2.93}WghtTrdT}{18}{subsubsection.3.2.2.93}
\contentsline {subsubsection}{\numberline {3.2.2.94}wgtCa}{18}{subsubsection.3.2.2.94}
\contentsline {subsubsection}{\numberline {3.2.2.95}wgtCrea}{18}{subsubsection.3.2.2.95}
\contentsline {subsubsection}{\numberline {3.2.2.96}wgtHCO3}{18}{subsubsection.3.2.2.96}
\contentsline {subsubsection}{\numberline {3.2.2.97}wgtK}{18}{subsubsection.3.2.2.97}
\contentsline {subsubsection}{\numberline {3.2.2.98}wgtNa}{18}{subsubsection.3.2.2.98}
\contentsline {subsubsection}{\numberline {3.2.2.99}wgtPhos}{18}{subsubsection.3.2.2.99}
\contentsline {subsubsection}{\numberline {3.2.2.100}wgtUr}{18}{subsubsection.3.2.2.100}
\contentsline {section}{\numberline {3.3}strPhysicalSensorReadout Struct Reference}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{19}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Field Documentation}{19}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}babdStatusBubble}{19}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}bpsiBloodPressure}{19}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}bpsoBloodPressure}{19}{subsubsection.3.3.2.3}
\contentsline {subsubsection}{\numberline {3.3.2.4}dcsConductFcq}{19}{subsubsection.3.3.2.4}
\contentsline {subsubsection}{\numberline {3.3.2.5}dcsConductFcr}{20}{subsubsection.3.3.2.5}
\contentsline {subsubsection}{\numberline {3.3.2.6}flowBlood}{20}{subsubsection.3.3.2.6}
\contentsline {subsubsection}{\numberline {3.3.2.7}flowFluidic}{20}{subsubsection.3.3.2.7}
\contentsline {subsubsection}{\numberline {3.3.2.8}fpsiFluidicPressure}{20}{subsubsection.3.3.2.8}
\contentsline {subsubsection}{\numberline {3.3.2.9}fpsoFluidicPressure}{20}{subsubsection.3.3.2.9}
\contentsline {subsubsection}{\numberline {3.3.2.10}mfsiStatusSwitch}{20}{subsubsection.3.3.2.10}
\contentsline {subsubsection}{\numberline {3.3.2.11}mfsoStatusSwitch}{20}{subsubsection.3.3.2.11}
\contentsline {subsubsection}{\numberline {3.3.2.12}pumpSpeedBlood}{20}{subsubsection.3.3.2.12}
\contentsline {subsubsection}{\numberline {3.3.2.13}pumpSpeedFluidic}{20}{subsubsection.3.3.2.13}
\contentsline {subsubsection}{\numberline {3.3.2.14}statusBloodLeakage}{20}{subsubsection.3.3.2.14}
\contentsline {subsubsection}{\numberline {3.3.2.15}statusEcpi}{21}{subsubsection.3.3.2.15}
\contentsline {subsubsection}{\numberline {3.3.2.16}statusEcpo}{21}{subsubsection.3.3.2.16}
\contentsline {subsubsection}{\numberline {3.3.2.17}statusFluidicLeakage}{21}{subsubsection.3.3.2.17}
\contentsline {subsubsection}{\numberline {3.3.2.18}statusPolarization}{21}{subsubsection.3.3.2.18}
\contentsline {subsubsection}{\numberline {3.3.2.19}timestamp}{21}{subsubsection.3.3.2.19}
\contentsline {section}{\numberline {3.4}strPhysiologicalSensorReadout Struct Reference}{21}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{22}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Field Documentation}{22}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}btsiTemperature}{22}{subsubsection.3.4.2.1}
\contentsline {subsubsection}{\numberline {3.4.2.2}btsoTemperature}{22}{subsubsection.3.4.2.2}
\contentsline {subsubsection}{\numberline {3.4.2.3}ecpiC4H9N3O2}{22}{subsubsection.3.4.2.3}
\contentsline {subsubsection}{\numberline {3.4.2.4}ecpiCa}{22}{subsubsection.3.4.2.4}
\contentsline {subsubsection}{\numberline {3.4.2.5}ecpiH2PO4}{22}{subsubsection.3.4.2.5}
\contentsline {subsubsection}{\numberline {3.4.2.6}ecpiHCO3}{22}{subsubsection.3.4.2.6}
\contentsline {subsubsection}{\numberline {3.4.2.7}ecpiK}{23}{subsubsection.3.4.2.7}
\contentsline {subsubsection}{\numberline {3.4.2.8}ecpiMg}{23}{subsubsection.3.4.2.8}
\contentsline {subsubsection}{\numberline {3.4.2.9}ecpiNa}{23}{subsubsection.3.4.2.9}
\contentsline {subsubsection}{\numberline {3.4.2.10}ecpiPH}{23}{subsubsection.3.4.2.10}
\contentsline {subsubsection}{\numberline {3.4.2.11}ecpiTemperature}{23}{subsubsection.3.4.2.11}
\contentsline {subsubsection}{\numberline {3.4.2.12}ecpiUrea}{23}{subsubsection.3.4.2.12}
\contentsline {subsubsection}{\numberline {3.4.2.13}ecpoC4H9N3O2}{23}{subsubsection.3.4.2.13}
\contentsline {subsubsection}{\numberline {3.4.2.14}ecpoCa}{23}{subsubsection.3.4.2.14}
\contentsline {subsubsection}{\numberline {3.4.2.15}ecpoH2PO4}{23}{subsubsection.3.4.2.15}
\contentsline {subsubsection}{\numberline {3.4.2.16}ecpoHCO3}{23}{subsubsection.3.4.2.16}
\contentsline {subsubsection}{\numberline {3.4.2.17}ecpoK}{24}{subsubsection.3.4.2.17}
\contentsline {subsubsection}{\numberline {3.4.2.18}ecpoMg}{24}{subsubsection.3.4.2.18}
\contentsline {subsubsection}{\numberline {3.4.2.19}ecpoNa}{24}{subsubsection.3.4.2.19}
\contentsline {subsubsection}{\numberline {3.4.2.20}ecpoPH}{24}{subsubsection.3.4.2.20}
\contentsline {subsubsection}{\numberline {3.4.2.21}ecpoTemperature}{24}{subsubsection.3.4.2.21}
\contentsline {subsubsection}{\numberline {3.4.2.22}ecpoUrea}{24}{subsubsection.3.4.2.22}
\contentsline {subsubsection}{\numberline {3.4.2.23}timestamp}{24}{subsubsection.3.4.2.23}
\contentsline {chapter}{\numberline {4}File Documentation}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}crc16.c File Reference}{25}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{25}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Function Documentation}{25}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}crc\_\discretionary {-}{}{}reflect}{25}{subsubsection.4.1.2.1}
\contentsline {subsubsection}{\numberline {4.1.2.2}crc\_\discretionary {-}{}{}update}{26}{subsubsection.4.1.2.2}
\contentsline {section}{\numberline {4.2}crc16.h File Reference}{26}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Define Documentation}{27}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}CRC\_\discretionary {-}{}{}ALGO\_\discretionary {-}{}{}TABLE\_\discretionary {-}{}{}DRIVEN}{27}{subsubsection.4.2.2.1}
\contentsline {subsubsection}{\numberline {4.2.2.2}taskMessage}{27}{subsubsection.4.2.2.2}
\contentsline {subsection}{\numberline {4.2.3}Typedef Documentation}{27}{subsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.3.1}crc\_\discretionary {-}{}{}t}{27}{subsubsection.4.2.3.1}
\contentsline {subsection}{\numberline {4.2.4}Function Documentation}{27}{subsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.4.1}crc\_\discretionary {-}{}{}reflect}{27}{subsubsection.4.2.4.1}
\contentsline {subsubsection}{\numberline {4.2.4.2}crc\_\discretionary {-}{}{}update}{27}{subsubsection.4.2.4.2}
\contentsline {section}{\numberline {4.3}csvparselib.c File Reference}{28}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Define Documentation}{29}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}PREFIX}{29}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}SELECT\_\discretionary {-}{}{}NOT\_\discretionary {-}{}{}FLT}{29}{subsubsection.4.3.1.2}
\contentsline {subsubsection}{\numberline {4.3.1.3}SELECT\_\discretionary {-}{}{}NOT\_\discretionary {-}{}{}INT}{29}{subsubsection.4.3.1.3}
\contentsline {subsubsection}{\numberline {4.3.1.4}taskMessage}{29}{subsubsection.4.3.1.4}
\contentsline {subsubsection}{\numberline {4.3.1.5}VALUE\_\discretionary {-}{}{}MAX\_\discretionary {-}{}{}CHARS}{29}{subsubsection.4.3.1.5}
\contentsline {subsection}{\numberline {4.3.2}Function Documentation}{30}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}findCRC}{30}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}movebeforeCRLF}{30}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}parseLine}{30}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}parseLineFloat}{31}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}seekToLastLF}{31}{subsubsection.4.3.2.5}
\contentsline {subsubsection}{\numberline {4.3.2.6}strnlen}{31}{subsubsection.4.3.2.6}
\contentsline {subsubsection}{\numberline {4.3.2.7}verifyCRC}{32}{subsubsection.4.3.2.7}
\contentsline {subsubsection}{\numberline {4.3.2.8}writeCRC}{32}{subsubsection.4.3.2.8}
\contentsline {subsubsection}{\numberline {4.3.2.9}writeFloat}{32}{subsubsection.4.3.2.9}
\contentsline {section}{\numberline {4.4}csvparselib.h File Reference}{33}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Define Documentation}{34}{subsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.1.1}taskMessage}{34}{subsubsection.4.4.1.1}
\contentsline {subsection}{\numberline {4.4.2}Function Documentation}{34}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}findCRC}{34}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}movebeforeCRLF}{35}{subsubsection.4.4.2.2}
\contentsline {subsubsection}{\numberline {4.4.2.3}parseLine}{35}{subsubsection.4.4.2.3}
\contentsline {subsubsection}{\numberline {4.4.2.4}parseLineFloat}{35}{subsubsection.4.4.2.4}
\contentsline {subsubsection}{\numberline {4.4.2.5}seekToLastLF}{35}{subsubsection.4.4.2.5}
\contentsline {subsubsection}{\numberline {4.4.2.6}strnlen}{36}{subsubsection.4.4.2.6}
\contentsline {subsubsection}{\numberline {4.4.2.7}verifyCRC}{36}{subsubsection.4.4.2.7}
\contentsline {subsubsection}{\numberline {4.4.2.8}writeCRC}{36}{subsubsection.4.4.2.8}
\contentsline {subsubsection}{\numberline {4.4.2.9}writeFloat}{37}{subsubsection.4.4.2.9}
\contentsline {section}{\numberline {4.5}pstdint.h File Reference}{38}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Define Documentation}{39}{subsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.1.1}\_\discretionary {-}{}{}PSTDINT\_\discretionary {-}{}{}H\_\discretionary {-}{}{}INCLUDED}{39}{subsubsection.4.5.1.1}
\contentsline {subsubsection}{\numberline {4.5.1.2}INT16\_\discretionary {-}{}{}MAX}{39}{subsubsection.4.5.1.2}
\contentsline {subsubsection}{\numberline {4.5.1.3}INT16\_\discretionary {-}{}{}MIN}{39}{subsubsection.4.5.1.3}
\contentsline {subsubsection}{\numberline {4.5.1.4}INT32\_\discretionary {-}{}{}MAX}{40}{subsubsection.4.5.1.4}
\contentsline {subsubsection}{\numberline {4.5.1.5}INT32\_\discretionary {-}{}{}MIN}{40}{subsubsection.4.5.1.5}
\contentsline {subsubsection}{\numberline {4.5.1.6}INT8\_\discretionary {-}{}{}MAX}{40}{subsubsection.4.5.1.6}
\contentsline {subsubsection}{\numberline {4.5.1.7}INT8\_\discretionary {-}{}{}MIN}{40}{subsubsection.4.5.1.7}
\contentsline {subsubsection}{\numberline {4.5.1.8}INT\_\discretionary {-}{}{}FAST16\_\discretionary {-}{}{}MAX}{40}{subsubsection.4.5.1.8}
\contentsline {subsubsection}{\numberline {4.5.1.9}INT\_\discretionary {-}{}{}FAST16\_\discretionary {-}{}{}MIN}{40}{subsubsection.4.5.1.9}
\contentsline {subsubsection}{\numberline {4.5.1.10}INT\_\discretionary {-}{}{}FAST32\_\discretionary {-}{}{}MAX}{40}{subsubsection.4.5.1.10}
\contentsline {subsubsection}{\numberline {4.5.1.11}INT\_\discretionary {-}{}{}FAST32\_\discretionary {-}{}{}MIN}{40}{subsubsection.4.5.1.11}
\contentsline {subsubsection}{\numberline {4.5.1.12}INT\_\discretionary {-}{}{}FAST8\_\discretionary {-}{}{}MAX}{40}{subsubsection.4.5.1.12}
\contentsline {subsubsection}{\numberline {4.5.1.13}INT\_\discretionary {-}{}{}FAST8\_\discretionary {-}{}{}MIN}{40}{subsubsection.4.5.1.13}
\contentsline {subsubsection}{\numberline {4.5.1.14}INT\_\discretionary {-}{}{}LEAST16\_\discretionary {-}{}{}MAX}{41}{subsubsection.4.5.1.14}
\contentsline {subsubsection}{\numberline {4.5.1.15}INT\_\discretionary {-}{}{}LEAST16\_\discretionary {-}{}{}MIN}{41}{subsubsection.4.5.1.15}
\contentsline {subsubsection}{\numberline {4.5.1.16}INT\_\discretionary {-}{}{}LEAST32\_\discretionary {-}{}{}MAX}{41}{subsubsection.4.5.1.16}
\contentsline {subsubsection}{\numberline {4.5.1.17}INT\_\discretionary {-}{}{}LEAST32\_\discretionary {-}{}{}MIN}{41}{subsubsection.4.5.1.17}
\contentsline {subsubsection}{\numberline {4.5.1.18}INT\_\discretionary {-}{}{}LEAST8\_\discretionary {-}{}{}MAX}{41}{subsubsection.4.5.1.18}
\contentsline {subsubsection}{\numberline {4.5.1.19}INT\_\discretionary {-}{}{}LEAST8\_\discretionary {-}{}{}MIN}{41}{subsubsection.4.5.1.19}
\contentsline {subsubsection}{\numberline {4.5.1.20}INTMAX\_\discretionary {-}{}{}C}{41}{subsubsection.4.5.1.20}
\contentsline {subsubsection}{\numberline {4.5.1.21}INTMAX\_\discretionary {-}{}{}MAX}{41}{subsubsection.4.5.1.21}
\contentsline {subsubsection}{\numberline {4.5.1.22}PRINTF\_\discretionary {-}{}{}INT16\_\discretionary {-}{}{}DEC\_\discretionary {-}{}{}WIDTH}{41}{subsubsection.4.5.1.22}
\contentsline {subsubsection}{\numberline {4.5.1.23}PRINTF\_\discretionary {-}{}{}INT16\_\discretionary {-}{}{}HEX\_\discretionary {-}{}{}WIDTH}{41}{subsubsection.4.5.1.23}
\contentsline {subsubsection}{\numberline {4.5.1.24}PRINTF\_\discretionary {-}{}{}INT32\_\discretionary {-}{}{}DEC\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.24}
\contentsline {subsubsection}{\numberline {4.5.1.25}PRINTF\_\discretionary {-}{}{}INT32\_\discretionary {-}{}{}HEX\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.25}
\contentsline {subsubsection}{\numberline {4.5.1.26}PRINTF\_\discretionary {-}{}{}INT64\_\discretionary {-}{}{}DEC\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.26}
\contentsline {subsubsection}{\numberline {4.5.1.27}PRINTF\_\discretionary {-}{}{}INT64\_\discretionary {-}{}{}HEX\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.27}
\contentsline {subsubsection}{\numberline {4.5.1.28}PRINTF\_\discretionary {-}{}{}INT8\_\discretionary {-}{}{}DEC\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.28}
\contentsline {subsubsection}{\numberline {4.5.1.29}PRINTF\_\discretionary {-}{}{}INT8\_\discretionary {-}{}{}HEX\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.29}
\contentsline {subsubsection}{\numberline {4.5.1.30}PRINTF\_\discretionary {-}{}{}INTMAX\_\discretionary {-}{}{}DEC\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.30}
\contentsline {subsubsection}{\numberline {4.5.1.31}PRINTF\_\discretionary {-}{}{}INTMAX\_\discretionary {-}{}{}HEX\_\discretionary {-}{}{}WIDTH}{42}{subsubsection.4.5.1.31}
\contentsline {subsubsection}{\numberline {4.5.1.32}PRINTF\_\discretionary {-}{}{}INTMAX\_\discretionary {-}{}{}MODIFIER}{42}{subsubsection.4.5.1.32}
\contentsline {subsubsection}{\numberline {4.5.1.33}PRINTF\_\discretionary {-}{}{}LEAST16\_\discretionary {-}{}{}MODIFIER}{42}{subsubsection.4.5.1.33}
\contentsline {subsubsection}{\numberline {4.5.1.34}PRINTF\_\discretionary {-}{}{}LEAST32\_\discretionary {-}{}{}MODIFIER}{43}{subsubsection.4.5.1.34}
\contentsline {subsubsection}{\numberline {4.5.1.35}SIG\_\discretionary {-}{}{}ATOMIC\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.35}
\contentsline {subsubsection}{\numberline {4.5.1.36}SIZE\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.36}
\contentsline {subsubsection}{\numberline {4.5.1.37}STDINT\_\discretionary {-}{}{}H\_\discretionary {-}{}{}UINTPTR\_\discretionary {-}{}{}T\_\discretionary {-}{}{}DEFINED}{43}{subsubsection.4.5.1.37}
\contentsline {subsubsection}{\numberline {4.5.1.38}UINT16\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.38}
\contentsline {subsubsection}{\numberline {4.5.1.39}UINT32\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.39}
\contentsline {subsubsection}{\numberline {4.5.1.40}UINT8\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.40}
\contentsline {subsubsection}{\numberline {4.5.1.41}UINT\_\discretionary {-}{}{}FAST16\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.41}
\contentsline {subsubsection}{\numberline {4.5.1.42}UINT\_\discretionary {-}{}{}FAST32\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.42}
\contentsline {subsubsection}{\numberline {4.5.1.43}UINT\_\discretionary {-}{}{}FAST8\_\discretionary {-}{}{}MAX}{43}{subsubsection.4.5.1.43}
\contentsline {subsubsection}{\numberline {4.5.1.44}UINT\_\discretionary {-}{}{}LEAST16\_\discretionary {-}{}{}MAX}{44}{subsubsection.4.5.1.44}
\contentsline {subsubsection}{\numberline {4.5.1.45}UINT\_\discretionary {-}{}{}LEAST32\_\discretionary {-}{}{}MAX}{44}{subsubsection.4.5.1.45}
\contentsline {subsubsection}{\numberline {4.5.1.46}UINT\_\discretionary {-}{}{}LEAST8\_\discretionary {-}{}{}MAX}{44}{subsubsection.4.5.1.46}
\contentsline {subsubsection}{\numberline {4.5.1.47}UINTMAX\_\discretionary {-}{}{}C}{44}{subsubsection.4.5.1.47}
\contentsline {subsubsection}{\numberline {4.5.1.48}UINTMAX\_\discretionary {-}{}{}MAX}{44}{subsubsection.4.5.1.48}
\contentsline {subsubsection}{\numberline {4.5.1.49}ULONG\_\discretionary {-}{}{}LONG\_\discretionary {-}{}{}MAX}{44}{subsubsection.4.5.1.49}
\contentsline {subsection}{\numberline {4.5.2}Typedef Documentation}{44}{subsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.2.1}int\_\discretionary {-}{}{}fast16\_\discretionary {-}{}{}t}{44}{subsubsection.4.5.2.1}
\contentsline {subsubsection}{\numberline {4.5.2.2}int\_\discretionary {-}{}{}fast32\_\discretionary {-}{}{}t}{44}{subsubsection.4.5.2.2}
\contentsline {subsubsection}{\numberline {4.5.2.3}int\_\discretionary {-}{}{}fast8\_\discretionary {-}{}{}t}{44}{subsubsection.4.5.2.3}
\contentsline {subsubsection}{\numberline {4.5.2.4}int\_\discretionary {-}{}{}least16\_\discretionary {-}{}{}t}{44}{subsubsection.4.5.2.4}
\contentsline {subsubsection}{\numberline {4.5.2.5}int\_\discretionary {-}{}{}least32\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.5}
\contentsline {subsubsection}{\numberline {4.5.2.6}int\_\discretionary {-}{}{}least8\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.6}
\contentsline {subsubsection}{\numberline {4.5.2.7}intmax\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.7}
\contentsline {subsubsection}{\numberline {4.5.2.8}intptr\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.8}
\contentsline {subsubsection}{\numberline {4.5.2.9}uint\_\discretionary {-}{}{}fast16\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.9}
\contentsline {subsubsection}{\numberline {4.5.2.10}uint\_\discretionary {-}{}{}fast32\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.10}
\contentsline {subsubsection}{\numberline {4.5.2.11}uint\_\discretionary {-}{}{}fast8\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.11}
\contentsline {subsubsection}{\numberline {4.5.2.12}uint\_\discretionary {-}{}{}least16\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.12}
\contentsline {subsubsection}{\numberline {4.5.2.13}uint\_\discretionary {-}{}{}least32\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.13}
\contentsline {subsubsection}{\numberline {4.5.2.14}uint\_\discretionary {-}{}{}least8\_\discretionary {-}{}{}t}{45}{subsubsection.4.5.2.14}
\contentsline {subsubsection}{\numberline {4.5.2.15}uintmax\_\discretionary {-}{}{}t}{46}{subsubsection.4.5.2.15}
\contentsline {section}{\numberline {4.6}typedefsNephron.h File Reference}{46}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Define Documentation}{46}{subsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.1.1}actuatorType}{46}{subsubsection.4.6.1.1}
\contentsline {subsubsection}{\numberline {4.6.1.2}sensorType}{46}{subsubsection.4.6.1.2}
\contentsline {subsection}{\numberline {4.6.2}Typedef Documentation}{46}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}tdActuators}{46}{subsubsection.4.6.2.1}
\contentsline {subsubsection}{\numberline {4.6.2.2}tdParametersSCC}{46}{subsubsection.4.6.2.2}
\contentsline {subsubsection}{\numberline {4.6.2.3}tdPhysicalSensorReadout}{46}{subsubsection.4.6.2.3}
\contentsline {subsubsection}{\numberline {4.6.2.4}tdPhysiologicalSensorReadout}{47}{subsubsection.4.6.2.4}
\contentsline {section}{\numberline {4.7}unified\_\discretionary {-}{}{}delete.c File Reference}{47}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Define Documentation}{47}{subsection.4.7.1}
\contentsline {subsubsection}{\numberline {4.7.1.1}PREFIX}{47}{subsubsection.4.7.1.1}
\contentsline {subsubsection}{\numberline {4.7.1.2}taskMessage}{47}{subsubsection.4.7.1.2}
\contentsline {subsection}{\numberline {4.7.2}Function Documentation}{47}{subsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.2.1}main}{47}{subsubsection.4.7.2.1}
\contentsline {subsubsection}{\numberline {4.7.2.2}unified\_\discretionary {-}{}{}delete}{48}{subsubsection.4.7.2.2}
\contentsline {section}{\numberline {4.8}unified\_\discretionary {-}{}{}delete.h File Reference}{48}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Function Documentation}{48}{subsection.4.8.1}
\contentsline {subsubsection}{\numberline {4.8.1.1}unified\_\discretionary {-}{}{}delete}{48}{subsubsection.4.8.1.1}
\contentsline {section}{\numberline {4.9}unified\_\discretionary {-}{}{}read.c File Reference}{49}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}Define Documentation}{49}{subsection.4.9.1}
\contentsline {subsubsection}{\numberline {4.9.1.1}PREFIX}{49}{subsubsection.4.9.1.1}
\contentsline {subsubsection}{\numberline {4.9.1.2}taskMessage}{50}{subsubsection.4.9.1.2}
\contentsline {subsection}{\numberline {4.9.2}Function Documentation}{50}{subsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.2.1}extract\_\discretionary {-}{}{}timestamp}{50}{subsubsection.4.9.2.1}
\contentsline {subsubsection}{\numberline {4.9.2.2}main}{50}{subsubsection.4.9.2.2}
\contentsline {subsubsection}{\numberline {4.9.2.3}ReadInMemberValues}{50}{subsubsection.4.9.2.3}
\contentsline {subsubsection}{\numberline {4.9.2.4}unified\_\discretionary {-}{}{}read}{51}{subsubsection.4.9.2.4}
\contentsline {section}{\numberline {4.10}unified\_\discretionary {-}{}{}read.h File Reference}{51}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}Define Documentation}{52}{subsection.4.10.1}
\contentsline {subsubsection}{\numberline {4.10.1.1}taskMessage}{52}{subsubsection.4.10.1.1}
\contentsline {subsection}{\numberline {4.10.2}Function Documentation}{52}{subsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.2.1}extract\_\discretionary {-}{}{}timestamp}{52}{subsubsection.4.10.2.1}
\contentsline {subsubsection}{\numberline {4.10.2.2}ReadInMemberValues}{52}{subsubsection.4.10.2.2}
\contentsline {subsubsection}{\numberline {4.10.2.3}unified\_\discretionary {-}{}{}read}{53}{subsubsection.4.10.2.3}
\contentsline {section}{\numberline {4.11}unified\_\discretionary {-}{}{}write.c File Reference}{53}{section.4.11}
\contentsline {subsection}{\numberline {4.11.1}Define Documentation}{54}{subsection.4.11.1}
\contentsline {subsubsection}{\numberline {4.11.1.1}PREFIX}{54}{subsubsection.4.11.1.1}
\contentsline {subsubsection}{\numberline {4.11.1.2}taskMessage}{54}{subsubsection.4.11.1.2}
\contentsline {subsection}{\numberline {4.11.2}Function Documentation}{54}{subsection.4.11.2}
\contentsline {subsubsection}{\numberline {4.11.2.1}unified\_\discretionary {-}{}{}write}{54}{subsubsection.4.11.2.1}
\contentsline {subsubsection}{\numberline {4.11.2.2}writeOutMemberValues}{55}{subsubsection.4.11.2.2}
\contentsline {section}{\numberline {4.12}unified\_\discretionary {-}{}{}write.h File Reference}{55}{section.4.12}
\contentsline {subsection}{\numberline {4.12.1}Define Documentation}{56}{subsection.4.12.1}
\contentsline {subsubsection}{\numberline {4.12.1.1}DBUFFER\_\discretionary {-}{}{}CSVLINE\_\discretionary {-}{}{}SIZE}{56}{subsubsection.4.12.1.1}
\contentsline {subsubsection}{\numberline {4.12.1.2}FILENAME\_\discretionary {-}{}{}LENGTH}{56}{subsubsection.4.12.1.2}
\contentsline {subsubsection}{\numberline {4.12.1.3}SELECT\_\discretionary {-}{}{}NOT\_\discretionary {-}{}{}FLT}{56}{subsubsection.4.12.1.3}
\contentsline {subsubsection}{\numberline {4.12.1.4}SELECT\_\discretionary {-}{}{}NOT\_\discretionary {-}{}{}INT}{56}{subsubsection.4.12.1.4}
\contentsline {subsubsection}{\numberline {4.12.1.5}taskMessage}{57}{subsubsection.4.12.1.5}
\contentsline {subsection}{\numberline {4.12.2}Function Documentation}{57}{subsection.4.12.2}
\contentsline {subsubsection}{\numberline {4.12.2.1}unified\_\discretionary {-}{}{}write}{57}{subsubsection.4.12.2.1}
\contentsline {subsubsection}{\numberline {4.12.2.2}writeOutMemberValues}{57}{subsubsection.4.12.2.2}
