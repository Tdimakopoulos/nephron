/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "nephron.h"
	#include "userInterface.h"
	#include "fcHelperFunc.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
	#include "userInterface.h"
	#include "..\\TaskFlowControl\\fcHelperFunc.h"
#endif


#define PREFIX "OFFIS FreeRTOS task UI"

#define msgPatient  "OFFIS FreeRTOS task UI: send 2 PATIENT"
#define msgDoctor   "OFFIS FreeRTOS task UI: send 2 DOCTOR"
#define msgDebug	"OFFIS FreeRTOS task UI: the device should:"
	
uint8_t heartBeatUI = 0;

/*-----------------------------------------------------------*/
//void undefPackageId(tdCommand command);
//void displayDecissionTreeWarning ( const char *patient, const char *doctor, unsigned short id );
//void changeWAKDState( const tdWakdStates fromState, const tdWakdStates toState, tdAllQueueHandles *pAllQueueHandles, tdQtoken *pQtoken);

void tskUI( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting User Interface task!");

	xQueueHandle *qhDISPin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhUIin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhUIin;

	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	Qtoken.pData	= NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	// Here we have a very crude HACK to have a rudimentary communication with a user GUI
	// that is implemented in Simulink. We are using an integer value to call predefined messages
	// in that GUI. We are sending this value as an actor value. THIS IS A HACK.
//	tdWakdStates stateOfWakdAsKnownByUI = wakdStates_UndefinedInitializing;
//	setActuatorInSimulink (ACTUATOR_MESSAGEUI, 1, stateOfWakdAsKnownByUI);
	
	for ( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every CBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatUI++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif

		if ( xQueueReceive( qhUIin, &Qtoken, UI_PERIOD) == pdPASS )
		{	// We did found something in the queue of UI: User Interface
			#ifdef DEBUG_UI
				taskMessage("I", PREFIX, "Data in UI queue!");
			#endif
			switch (Qtoken.command) {
				case command_InformGuiConnectBag:{
					// Here we would usually ask the patient to do something and press OK.
					// For this dummy simulation we pretend that the patient has done so.
					// Attached to the incoming command is the information to what state
					// we should change.
					taskMessage("I", PREFIX, "Patient acknowledges connecting bag by pressing 'OK' on WAKD UI.");
					forceChangeState((tdWakdStates)Qtoken.pData, qhDISPin);
					break;
				}
				case command_InformGuiDisconnectBag:{
					// Here we would usually ask the patient to do something and press OK.
					// For this dummy simulation we pretend that the patient has done so.
					// The GUI has to account for it!
					taskMessage("I", PREFIX, "Patient acknowledges DISconnecting bag by pressing 'OK' on WAKD UI.");
					sFree((void *)&(Qtoken.pData));
					break;
				}				
				case command_UseAtachedMsgHeader: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_weightData):{
							// This weight measure needs to be checked by the patient.
							// This same measure has also already been forwarded to the communication board to forward
							// to the smartphone GUI. So the checking by the patient can come back either from task UI or SP.
							#if defined DEBUG_UI || defined SEQ2
								taskMessage("I", PREFIX, "Please verify correctness of weight measure: %f", ((double)(((tdMsgWeightData *)(Qtoken.pData))->weightData.Weight))/FIXPOINTSHIFT_WEIGHTSCALE);
							#endif
							sFree((void *)&(Qtoken.pData));
							break;
						}
						case dataID_systemInfo: {
						// Look into the message to know which error this is
							#if defined DEBUG_UI
								taskMessage("I", PREFIX, "Received message '%s'from SCC.", infoMsgToString(((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum));
							#endif
							tdUnixTimeType timeNow = 99;//getTime();
							switch (((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum) {
								case dectreeMsg_detectedSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDoctor, "Sorbend Dysfunction detected. Plasma pump stopped, blood pump still running");
									taskMessage("I", msgPatient, "contact physician, to replace filter");
									taskMessage("I", msgDebug, "stop Plasma pump and keep blood pump running %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBPorFPhigh: {
								#if defined DEBUG_UI_MSG || defined SEQ11
									taskMessage("I", msgDoctor,  "ALARM: pressure (high) excess, all pumps stopped");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDebug,  "pressure too high, stop device here %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBPorFPlow: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDoctor,  "ALARM: pressure (low) excess, all pumps stopped");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDebug,  "pressure too low, stop device here %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBTSoTH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "temperature (high) excess at BTSo, stop device here");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSo, all pumps stopped %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBTSoTL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "temperature (low) excess at BTSo, stop device here");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSo, all pumps stopped %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBTSiTH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "temperature (high) excess at BTSi, stop device here");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDoctor,  "ALARM: temperature (high) excess at BTSi, all pumps stopped %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedBTSiTL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "temperature (low) excess at BTSi, stop device here");
									taskMessage("I", msgPatient, "device stopped - contact physician");
									taskMessage("I", msgDoctor,  "ALARM: temperature (low) excess at BTSi, all pumps stopped %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedpumpBaccDevi: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "Blood Pump Flow deviation - ");
									taskMessage("E", msgDebug,  "... something should happen now, but still undefined what");
								#endif
									break;
								}
								case dectreeMsg_detectedpumpFaccDevi: {
									#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "Fluidic Pump Flow deviation");
									taskMessage("E", msgDebug,  "... something should happen now, but still undefined what");
									#endif
									break;
								}
								case dectreeMsg_detectedPhAAbsL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "ALARM: pH low");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedPhAAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "ALARM: pH high");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedPhAbsLorPhAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "pH out of normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
								#endif
									break;
								}
								case dectreeMsg_detectedNaAbsLorNaAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "Na+ out of normal range. Plasma pump stopped, blood pump still running.");
									taskMessage("I", msgDebug,   "Stop plasma pump. %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "Na+ trend out of normal range. No change in device function initiated.");
								#endif
									break;
								}								
								case dectreeMsg_detectedKAbsLorKAbsHandSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "replace cartridge");
									taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again)");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway.");
								#endif
									break;
								}
								case dectreeMsg_detectedKAbsLorKAbsHnoSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "K+ out of normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedKAAL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,   "K+ Alarm(low). Stopped plasma pump. Blood pump still runnning");
									taskMessage("I", msgDebug,   "K+ Alarm(low). Stop plasma pump. %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedKAAHandKincrease: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,   "K+ Alarm(high) < Kmeasafter");
									taskMessage("I", msgPatient, "replace sorbent cartridge and contact physician");
									taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again) %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedKAAHnoKincrease: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,   "K+ Alarm(high) and increasing behing the sorbend filter. Stopped plasma pump. Blood pump still runnning");
									taskMessage("I", msgDebug,   "K+ Alarm(high) >= Kmeasafter. Stop plasma pump.");
									taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient what to do (stop device.. do corrective actions - 2 be specified.. turn on device again) %i", timeNow);
								#endif
									break;
								}
								case dectreeMsg_detectedUreaAAbsHandSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "replace cartridge");
									taskMessage("I", msgDebug,  "... now the user interface needs to guide the patient through the replacement (stop device.. instructions to replace.. turn on device again) %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedUreaAAbsHnoSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "Urea out of normal range (high)");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedKTrdLPsT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "K+ Trend is decreasing too fast");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedKTrdofnriandSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "replace cartridge");
									taskMessage("I", msgDoctor,  "K+ Trend is decreasing too fast and sorbend dysfunction detected!");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedPhTrdLPsT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "pH decrease out of normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedPhTrdHPsT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "pH increase out of normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedPhTrdofnriandSorDys: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDebug,   "FC is controlling pumprate to reach target range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically anyway. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedUreaTrdHPsT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "Urea increase out of normal range (high)");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedWghtAbsL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDebug,  "Weight out of normal range(low)");
									taskMessage("I", msgDebug,   "FC is controlling adsorp to reach target range %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedWghtAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDoctor,  "probably a notification to doctor needs to be specified");
									taskMessage("I", msgDebug,  "Weight out of normal range(high)");
									taskMessage("I", msgDebug,  "... Flow Control is adapting Ultrafiltration anyway,");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react immediately. %i", timeNow);
								#endif
								break;
								}
								case dectreeMsg_detectedWghtTrdT: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgDoctor, "probably a notification to doctor needs to be specified");
									taskMessage("I", msgDebug,  "weight trend exceeds normal range");
									taskMessage("I", msgDebug,  "... Flow Control is adapting Ultrafiltration anyway,");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react immediately. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPsysAbsL: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "BP-sys lower than normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPsysAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "BP-sys higher than normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPsysAAbsH: {
								#if defined DEBUG_UI_MSG
								taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "ALARM: BP-sys high");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPdiaAbsL: {
								#if defined DEBUG_UI_MSG
								taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "BP-dia lower than normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPdiaAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "BP-dia higher than normal range");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
								break;
								}
								case decTreeMsg_detectedBPdiaAAbsH: {
								#if defined DEBUG_UI_MSG
									taskMessage("I", msgPatient, "contact physician");
									taskMessage("I", msgDoctor,  "ALARM: BP-dia high");
									taskMessage("I", msgDebug,  "... but it is not specified that the device should react automatically in any way. %i", timeNow);
								#endif
									break;
								}
								default:{
									taskMessage("E", PREFIX, "Default in interpretSCCmessage() / switch (sccMsg). This should not have happened!");
									break;
								}
							} // of switch (((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum)
							sFree((void *)&(Qtoken.pData));
							break;
						} // of case command_ProcessSCCmessage:
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				// */
				

//				case command_DefaultError: {
//					// Misused pointer must not be interpreted as such. Cast back to original type.
//					tdErrMsg errMsg = (tdErrMsg) Qtoken.pData;
//					taskMessage("I", PREFIX, "Received system error msg: %d", errMsg);
//					#ifndef VP_SIMULATION
//						#error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
//					#endif
//					break;
//				}
//				case command_CurrentWAKDstateIs:{
//					// THIS PART IS JUST A DEMO A NEEDS TO BE REPLACED BY REAL GUI IMPLEMENTATION!
//					stateOfWakdAsKnownByUI = ((tdMsgCurrentWAKDstateIs *)(Qtoken.pData))->wakdStateIs; // Misused pointer to transport value of current State
//					#ifdef DEBUG_UI
//						taskMessage("I", PREFIX, "New WAKD State is %d", stateOfWakdAsKnownByUI);
//					#endif
//					setActuatorInSimulink (ACTUATOR_MESSAGEUI, 1, stateOfWakdAsKnownByUI);
//					break;
//				}

			default:{
				// The following function covers all possible commands with one big switch.
				// The reason for this implementation is as follows. The function switch does
				// not contain a 'default' so that compilation will generate a warning, whenever
				// a new command should have been forgotten to be coded.
				defaultCommandHandling(PREFIX, Qtoken.command);
				break;
					// Process any button event from UI depending on state
					// THIS PART IS JUST A DEMO A NEEDS TO BE REPLACED BY REAL GUI IMPLEMENTATION!
//					switch (stateOfWakdAsKnownByUI) {
//						case wakdStates_AllStopped: {
//							switch (Qtoken.command) {
//								case command_ButtonEvent:{
//									// pointer pData was misused to transport data. Be beware of this!
//									tdButtons btn = (tdButtons) Qtoken.pData;
//									switch (btn) {
//										case btn_right:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing RIGHT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_left:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing LEFT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_up:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing UP doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_down:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing DOWN doesn't do anything.");
//											#endif
//											break;
//										}
//										case btn_sel:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing SEL doesn't do anything.");
//											#endif
//												break;
//										}
//									}
//								}
//								default:{
//									undefPackageId(Qtoken.command);
//									break;
//								}
//							}
//							break;
//						}
//						}
////						 case wakdStates_Configuration: {
////							 switch (Qtoken.command) {
////								 case command_ButtonRIGHTonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing RIGHT doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case command_ButtonLEFTonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing LEFT doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case command_ButtonUPonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing UP doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case command_ButtonDOWNonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing DOWN doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case command_ButtonOKonWAKDpressed:{
////									 // Change from state Configuration to state NoDialysate
////									 changeWAKDState(stateOfWakdAsKnownByUI, wakdStates_NoDialysate, pAllQueueHandles, &Qtoken);
////									 break;
////								 }
////								 default:{
////									 undefPackageId(Qtoken.command);
////									 break;
////								 }
////							 }
////							 break;
////						 }
//						case wakdStates_NoDialysate: {
//							switch (Qtoken.command) {
//								case command_ButtonEvent:{
//									// pointer pData was missused to transport data. Be beware of this!
//									tdButtons btn = (tdButtons) Qtoken.pData;
//									switch (btn) {
//										case btn_right:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing RIGHT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_left:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing LEFT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_up:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing UP doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_down:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing DOWN doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_sel:{
//											// Change from state NoDialysate to state Dialysis
//											changeWAKDState(stateOfWakdAsKnownByUI, wakdStates_Dialysis, pAllQueueHandles, &Qtoken);
//											break;
//										}
//									}
//								}
//								default:{
//									undefPackageId(Qtoken.command);
//									break;
//								}
//							}
//							break;
//						}
//						case wakdStates_Dialysis: {
//							switch (Qtoken.command) {
//								case command_InformGuiConnectBag: {
//									// This is where we should implement the functionality to show a message to
//									// the patient "Please connect disposal bags and press OK."
//									// To be done. For the time we now just assume, that the patient did press OK
//									// Change from state Dialysis to state Regen1
//									changeWAKDState(stateOfWakdAsKnownByUI, wakdStates_Regen1, pAllQueueHandles, &Qtoken);
//									break;
//								}
//								case command_ButtonEvent:{
//									// pointer pData was missused to transport data. Be beware of this!
//									tdButtons btn = (tdButtons) Qtoken.pData;
//									switch (btn) {
//										case btn_right:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing RIGHT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_left:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing LEFT doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_up:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing UP doesn't do anything.");
//											#endif
//												break;
//										}
//										case btn_down:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing DOWN doesn't do anything.");
//											#endif
//											break;
//										}
//										case btn_sel:{
//											#ifdef DEBUG_UI
//												taskMessage("I", PREFIX, "Pressing SEL doesn't do anything.");
//											#endif
//												break;
//										}
//									}
//								}
//								default:{
//									undefPackageId(Qtoken.command);
//									break;
//								}
//							}
//							break;
//					}
////						 case DialysisToRegenerationReversedPolarization1: {
////							 switch (Qtoken.command) {
////								 case ButtonRIGHTonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing RIGHT doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case ButtonLEFTonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing LEFT doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case ButtonUPonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing UP doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case ButtonDOWNonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Pressing DOWN doesn't do anything.");
////									 #endif
////									 break;
////								 }
////								 case ButtonOKonWAKDpressed:{
////									 #ifdef DEBUG_UI
////										 taskMessage("I", PREFIX, "Patient has confirmed that he attached disposal bags.");
////									 #endif
////									 // Flow Control can now continue with transition from Dialysis to Regeneration.
////									 changeWAKDState(stateOfWakdAsKnownByUI, DialysisToRegenerationReversedPolarization2, pAllQueueHandles, &Qtoken);
////									 break;
////								 }
////								 default:{
////									 undefPackageId(Qtoken.command);
////									 break;
////								 }
////							 }
////							 break;
////						 }
//						default:{
//							taskMessage("F", PREFIX, "Critical Error: WAKD in unknown state!");
//							break;
//						}
//					}
//					break;
				}
			}	
		} else {
			// We now waited for "xDelayPeriod" and nothing arrived in input queue.
			// Doing nothing and keep waiting
			#ifndef VP_SIMULATION
				#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
			#endif
		}
	}
}


//void undefPackageId(tdCommand command)
//{
//	taskMessage("E", PREFIX, "Undefined package ID %i in queue. We have no idea, what to do!", command);
//	// here we should start doing something suitable for this sort of message
//	#ifndef VP_SIMULATION
//		#error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
//	#endif
//}

//void displayDecissionTreeWarning ( const char *patient, const char *doctor, unsigned short id )
//{
//	#ifdef DEBUG_UI
//		taskMessage("W", PREFIX, "*************************************************");
//		taskMessage("W", PREFIX, "  Warning message #%i RECEIVED", id);
//		taskMessage("W", PREFIX, "  Patient: %s", patient);
//		taskMessage("W", PREFIX, "  Doctor:  %s", doctor );
//		taskMessage("W", PREFIX, "*************************************************");
//	#endif
//}

//void changeWAKDState( const tdWakdStates fromState, const tdWakdStates toState, tdAllQueueHandles *pAllQueueHandles, tdQtoken *pQtoken)
//{
//	tdMsgWakdStateFromTo *pMsgWakdStateFromTo = NULL;
//	pMsgWakdStateFromTo = (tdMsgWakdStateFromTo *) pvPortMalloc(sizeof(tdMsgWakdStateFromTo)); // Will be freed by receiver task!
//	pMsgWakdStateFromTo->fromWakdState = fromState;
//	pMsgWakdStateFromTo->toWakdState   = toState;
//	pQtoken->command = command_ChangeWAKDStateFromTo;
//	pQtoken->pData   = (void *) pMsgWakdStateFromTo;
//	if ( xQueueSend( pAllQueueHandles->qhDISPin, pQtoken, DISP_BLOCKING) == pdPASS ) {
//		// Seemingly we were able to write to the queue. Great!
//		#ifdef DEBUG_UI
//			taskMessage("I", PREFIX, "Message to Flow Control, change state. id:%d.", pQtoken->command);
//		#endif
//	} else {
//		// Seemingly the queue is full. Do something accordingly!
//		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Command id:%d lost!!!", pQtoken->command);
//		vPortFree(pMsgWakdStateFromTo);
//		#ifndef VP_SIMULATION
//			#error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
//		#endif
//	}
//}
