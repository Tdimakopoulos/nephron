 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
 
/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#ifdef LINUX
	#include "nephron.h"
#else
	#include "..\\..\\nephron.h"
#endif

// for testing purposes
//#ifdef __arm__ 
//	#include "csvparselib/pstdint.h"
//	#include "..\\..\\nephron.h"
//#else
//	#include <stdint.h>
//	//#define taskMessage printf
//#endif

#define PREFIX "OFFIS FreeRTOS task DS - CSV delete"

#ifdef LINUX
	#include "filenames.h"
	#include "csvparselib.h"
	#include "unified_delete.h"
	#include "unified_write.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "filenames.h"
	#include "csvparselib\\csvparselib.h"
	#include "unified_delete.h"
	#include "unified_write.h"
#endif

#if defined VP_SIMULATION
#include <errno.h>

// int main()
// {
	// unified_delete("delete_test.csv", 104, 240);
	// return 0;
// }
//! Deletes files with a timestamp-based ending. \a startFile has to smaller \a numberofFiles. 
/*!
  This function deletes files based on a given \a startFile and \a numberofFiles.
  Inside there is a loop which will delete all files from \a startFile to \a startFile + \a numberofFiles.
  Both are calculated by multiplying the actual abstract file number in the loop with \a MAX_TIMESTAMPS_IN_FILE.
  An example: MAX_TIMESTAMPS_IN_FILE is 3600 and we want to delete 10 files.
  Timestamps 0-3599 are saved in filename0101197000.csv, 3600-7199 in  filename0101197001.csv and so on.
  If we set startFile to 0 and numberofFiles to 2, we would delete the first two files as mentioned above.
  If a file does not exist(e.g. by choosing a too high \a numberofFiles) no error is returned, only on unusual events
  like storage medium errors.
  \param filename Name of the file which should be worked on.
  \param startFile Entry file from which \a numberofFiles files should be deleted.
  \param numberofFiles Amount of files which should be deleted.
  \return 1 on errors, 0 otherwise
 */
uint8_t deleteFiles(char* filename, uint32_t startFile, uint32_t numberofFiles)
{
	char filename_mod[FILENAME_LENGTH];
	if(numberofFiles == 0)
	{
		taskMessage("F", PREFIX, "numberofFiles cannot be 0.");
		return 1;
	}
	uint32_t i;
	int errorcode = 0;
	
	for(i = 0; i < numberofFiles; ++i)
	{
		strncpy(filename_mod,filename,FILENAME_LENGTH);
		// +1 to make sure we generate a new filename on every iteration of this for loop
		if(changeFileName(filename_mod, ((i + startFile)* MAX_TIMESTAMPS_IN_FILE) + 1))
		{
			taskMessage("F", PREFIX, "Error while creating filename for file: %s", filename);
			return 1;
		}
		errno = 0; // reset error code

		//taskMessage("D", PREFIX, "deleting %s",filename_mod);
		remove(filename_mod);
		errorcode = errno;
		/* if an error occured and differs from file not found aka ENOENT and not owner aka EPERM.
		   The minus is needed because some unix bastards decided decades ago to return negative error
		   codes even if they are defined as regular positive numbers in the errno.h
		*/
		if(errorcode != 0 && errorcode != ENOENT && errorcode != EPERM)
		{
			taskMessage("F", PREFIX, "Error %s while deleting file: %s", strerror(errorcode), filename_mod);
			return 1;
		}
	}
	return 0;
}
#endif
//! Deletes lines from a given file respectively filename based on timestamps. \a From has to be greater than \a to. 
/*!
  If from is greater than the last timestamp in the file, the function will just exit.
  The file needs to contain correct timestamps, otherwise behaviour is undefined.
  WARNING: Due use of fseek etc. which limits to 2147483647 bytes , this WON'T WORK ON FILES > 2GB!
  \param filename Name of the file which should be worked on.
  \param from Deletes from this timestamp on, including the line \a from
  \param to Deletes till(and including) this timestamp.
  \return 1 on errors, 0 otherwise
  \sa unified_read(uint8_t datatype, void* db)
*/
uint8_t unified_delete(char* filename, uint32_t from, uint32_t to)
{
	if(to < from)
	{
		taskMessage("F", PREFIX, "to cannot be smaller than from!");
		return 1;
	}
	FILE* pFile;
	char dataBuffer [DBUFFER_CSVLINE_SIZE];
	uint32_t from_pos, to_pos;
	uint32_t cur_timestamp = 0;
	char * ptr = 0;
	char * end = 0;

#if defined VP_SIMULATION
	pFile=fopen(filename, "rb+");
	if(pFile!=NULL)
	{
		// pass first line containing descriptions
		fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile); 
		/*	should loop until we passed the line which contains the from
			timestamp or the next greater one. after this loop, we know
			the exact starting position from where on we want to pass.
			to ensure that we got a correct from_pos even if the loop 
			won't run, we set it before the loop.
		*/
		from_pos = ftell(pFile);
		while(cur_timestamp < from)
		{
			from_pos = ftell(pFile);
			ptr = dataBuffer;
			if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
			{
				taskMessage("F", PREFIX, "Error while reading %s, unexpected EOF before from", filename);
				fclose(pFile);
				return 1;
			}
			cur_timestamp = parseLine(&ptr, end);
		}
		/*
			no timestamp in the given range, so we need to exit 
		*/
		if(cur_timestamp > to)
		{
			fclose(pFile);
			return 0;
		}
		/*
			same loop to find the to position. important: to_pos need to
			be set like from_pos before the maybe passed loop.
		*/
		to_pos = ftell(pFile);
		while(cur_timestamp < to)
		{
			to_pos = ftell(pFile);
			ptr = dataBuffer;
			if(fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile) == NULL)
			{
				// maybe just jump to the end of file instead of returning an error...?
				taskMessage("F", PREFIX, "Error while reading %s, unexpected EOF before to", filename);
				fclose(pFile);
				return 1;
			}
			cur_timestamp = parseLine(&ptr, end);
		}
		FILE *pTMPFile;
		// filename +5 chars because .tmp will be appended
		char TMPfilename[strnlen(filename, FILENAME_LENGTH)+5];
		/* 	currentChar has to contain more than a 8-bit char, for an explanation look at 
			http://en.wikipedia.org/wiki/C_file_input/output#Opening_a_file_using_fopen
		*/
		int16_t currentChar; 
		strcpy(TMPfilename,filename);
		strcat(TMPfilename,".tmp");
		pTMPFile = fopen(TMPfilename, "w+b");
		fseek (pFile, 0, SEEK_SET); // rewind to beginning of the file and write it to the new one
		while( (currentChar = fgetc(pFile)) != EOF)
		{
			fputc(currentChar, pTMPFile);
			/*
				if we reached from_pos.... skip bytes in the file until we reach to_pos and 
				continue to copy till we finally reach EOF
			*/
			if(ftell(pTMPFile) == (int)from_pos)
			{
				fseek(pFile,to_pos - from_pos,SEEK_CUR); 
			}
		}
		fclose (pTMPFile);
		fclose (pFile);
		// remove "old" .csv file and rename .csv.tmp to .csv
		if(remove(filename) != 0)
		{
			taskMessage("F", PREFIX, "Error deleting %s", filename);
			return 1;
		}
		if(rename(TMPfilename, filename) != 0)
		{
			taskMessage("F", PREFIX, "Error renaming %s", TMPfilename);
			return 1;
		}
		// after here we should be able to exit without problems :)
		return 0;
	}
	taskMessage("F", PREFIX, "Cannot open file %s", filename);
#endif
	return 1;
}
