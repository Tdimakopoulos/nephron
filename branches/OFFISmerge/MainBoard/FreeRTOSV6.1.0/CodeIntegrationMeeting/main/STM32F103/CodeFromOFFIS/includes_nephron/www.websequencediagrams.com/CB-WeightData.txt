note left of MB
	before this sequence starts
	SEQ1 was executed. After
	SEQ1 CB is expecting data
	from Scale.
end note
CB -> MB: dataID_weightData
MB -> CB: dataID_ack
note over MB
	The received weight needs to
	be verified by the patient.
	DO NOT USE AS IS!
end note
MB -> CB: dataID_weightData
note over CB
	looks into header for
	... id information
	... size information
end note
CB -> SP: bytewise forward
SP -> CB: dataID_ack
note over CB
	looks into header for
	... size information
end note
CB -> MB: bytewise forward
note over SP
	The smart phone presents
	weight measure on GUI to
	have patient to correct if
	required and then Ack.
end note