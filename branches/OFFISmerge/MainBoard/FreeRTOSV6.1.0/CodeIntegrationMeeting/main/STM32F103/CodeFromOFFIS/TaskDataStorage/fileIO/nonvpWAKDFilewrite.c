/*********************************************************
*	x86 test program. uses alternative / dummy functions
*	to display messages and allocate memory
*********************************************************/
#include "unified_read.h"
#include "unified_write.h"
#include <stdarg.h>
#include "../../includes_nephron/sensors.h"

void taskMessage(const char * severity, const char * pref, const char * message, ...)
{
	//#ifdef VP_SIMULATION
		// during simulation we want to make "printf"s on simulation trace window
		va_list vargzeiger;
		va_start(vargzeiger, message);
		if (strcmp(severity, "I") == 0) {
			printf("Info ");
		} else if (strcmp(severity, "W") == 0) {
			printf("Warning ");
		} else if (strcmp(severity, "E") == 0) {
			printf("Error ");
		} else if (strcmp(severity, "F") == 0) {
			printf("Failure ");
		} else {
			printf("N.D. ");
		}
		printf("(%s) ", pref);
		vprintf(message, vargzeiger);
		printf("\n");
		va_end(vargzeiger);
	//#else
		// Simple printf will not work on real HW. With VP we can just printf to cosole.
		// If we want to give a message with real HW we have to implement an according
		// functionality here. Remove warning once implementation is done.
		//#warning(Makro "VP" not defined. Compiling for Nephron target actual HW. Target implementation missing here!)
	//#endif
}

void *pvPortMalloc( size_t xWantedSize )
{
	return malloc(xWantedSize);
}

void sFree(void **vp)
{
	free(*vp);
	*vp = NULL;
}

void defaultIdHandling(char *prefix, tdDataId dataId) {}

int main() {

		/*
	//physio_sensor_readout.csv - dataID_physiologicalData
		//timestamp;ecpiNa;ecpoNa;ecpiK;ecpoK;ecpiUrea;ecpoUrea;ecpiPH;ecpoPH;ecpiTemp;ecpoTemp;
		{ tdPhysiologicalData db_1;
		db_1.TimeStamp = 237;
		db_1.ECPDataI.Sodium = 1.337;
		db_1.ECPDataO.Sodium = 1.337;
		db_1.ECPDataI.Potassium = 1.337;
		db_1.ECPDataO.Potassium = 1.337;
		db_1.ECPDataI.Urea = 1.337;
		db_1.ECPDataO.Urea = 1.337;
		db_1.ECPDataI.pH = 1.337;
		db_1.ECPDataO.pH = 1.337;
		db_1.ECPDataI.Temperature = 1.337;
		db_1.ECPDataO.Temperature = 1.337;
		taskMessage("I", PREFIX, "writing db1 1st time");
		unified_write(dataID_physiologicalData,&db_1);
		db_1.TimeStamp = 137;
		taskMessage("I", PREFIX, "writing db1 2nd time");
		unified_write(dataID_physiologicalData,&db_1); }
		taskMessage("I", PREFIX, "writing db1 done");
	*/	
/*			
	//physical_sensor_readout.csv - dataID_physicalData	//timestamp;statusEcpi;statusEcpo;dcsConductFcr;dcsConductFcq;dcsConductPT100;flowBlood;flowFluid;bpsiBloodPressure;bpsoBloodPressure;fpsiFluidicPressure;fpsoFluidicPressure;mfsiSwitch;mfsoSwitch;mfsuSwitch;voltagePolarization;
		{ tdPhysicalsensorData db_2;
		db_2.TimeStamp = 1337;
		db_2.statusECPI = 1.337;
		db_2.statusECPO = 1.337;
		db_2.CSENS1Data.Cond_FCR = 1.337;
		db_2.CSENS1Data.Cond_FCQ = 1.337;
		db_2.CSENS1Data.Cond_PT1000 = 1.337;
		db_2.PressureBCI.Pressure = 1.337;
		db_2.PressureBCO.Pressure = 1.337;
		db_2.PressureFCI.Pressure = 1.337;
		db_2.PressureFCO.Pressure = 1.337;
		taskMessage("I", PREFIX, "writing db2 1st time");
		unified_write(dataID_physicalData,&db_2); 
		taskMessage("I", PREFIX, "writing db2 1st time");
		unified_write(dataID_physicalData,&db_2); }
		
		
	//actuator.csv - dataID_actuatorData
		//timestamp;bloodPump;fluidPump;polarizer;mfso;mfsu;mfsi;
		{ tdActCtrl db_3;
		db_3.BLPumpCtrl.flowReference = 1.337;
		db_3.FLPumpCtrl.flowReference = 1.337;
		db_3.PolarizationCtrl.voltageReference = 1.337;
		db_3.TimeStamp_ActCtrl = 123;
		taskMessage("I", PREFIX, "writing db3 1st time");
		unified_write(dataID_actuatorData,&db_3); 
		unified_write(dataID_actuatorData,&db_3); }
		
	//weight.csv - dataID_weightData
		//timestamp: weight
		{ tdWeightMeasure db_4;
		db_4.TimeStamp_Weight = 1337;
		db_4.Weight = 337;
		taskMessage("I", PREFIX, "writing db4 1st time");
		unified_write(dataID_weightData,&db_4);
		taskMessage("I", PREFIX, "writing db4 2nd time");
		unified_write(dataID_weightData,&db_4);
		}
		
	//bloodpressure.csv - dataID_bpData
		//timestamp;BPsys;BPdia;
		{ tdBpData db_5;
		db_5.systolic=220;
		db_5.diastolic=25;
		db_5.TimeStamp_Bp=1337;
		taskMessage("I", PREFIX, "writing db5 1st time");
		unified_write(dataID_bpData,&db_5); 
		unified_write(dataID_bpData,&db_5); }
	
	//heartrate.csv - dataID_EcgData
		//timestamp;Heartrate;
		{ tdEcgData db_6;
		db_6.heartRate=44;
		db_6.TimeStamp_heartRate=1221;
		taskMessage("I", PREFIX, "writing db6 1st time");
		unified_write(dataID_EcgData,&db_6); 
		unified_write(dataID_EcgData,&db_6); }
 */		
	//patient_data.csv - dataID_PatientProfile
		//name;gender;measureTimeHours;measureTimeMinutes;wghtCtlTarget;wghtCtlLastMeasurement;wghtCtlDefRemPerDay;kCtlTarget;kCtlLastMeasurement;kCtlDefRemPerDay;urCtlTarget;urCtlLastMeasurement;urCtlDefRemPerDay
		tdPatientProfile db_7;
		strcpy(db_7.name,"Hans Smith");
		db_7.gender='m';
		db_7.measureTimeHours=8;
		db_7.measureTimeMinutes=30;
		db_7.wghtCtlTarget= (int) ( 86.0 * (float)FIXPOINTSHIFT_WEIGHTSCALE); 
		db_7.wghtCtlLastMeasurement=(int) ( 86.0 * (float)FIXPOINTSHIFT_WEIGHTSCALE);
		db_7.wghtCtlDefRemPerDay=(int) ( 1.2 * (float)FIXPOINTSHIFT_WEIGHTSCALE);;	// when 12000 it is exeggerated to force exremely often changing of operation modes! should be app. 1.2 kg!
		db_7.kCtlTarget= (int) ( 4.1 * (float)FIXPOINTSHIFT_K);
		db_7.kCtlLastMeasurement=(int) ( 4.3 * (float)FIXPOINTSHIFT_K);
		db_7.kCtlDefRemPerDay=17;
		db_7.urCtlTarget=(int) ( 10.5 * (float)FIXPOINTSHIFT_UREA);
		db_7.urCtlLastMeasurement=(int) ( 13.3 * (float)FIXPOINTSHIFT_UREA);
		db_7.urCtlDefRemPerDay=34;
		db_7.minDialPlasFlow = (int) ( 0.0 * (float)FIXPOINTSHIFT_PUMP_FLOW);
		db_7.maxDialPlasFlow = (int) ( 60.0 * (float)FIXPOINTSHIFT_PUMP_FLOW);
		db_7.minDialVoltage = (int) ( 0.0 * (float)FIXPOINTSHIFT_POLARIZ_VOLT);
		db_7.maxDialVoltage = (int) ( 20.0 * (float)FIXPOINTSHIFT_POLARIZ_VOLT);
		unified_write(dataID_PatientProfile,&db_7); 
	// 
	//state_configuration.csv - dataID_WAKDAllStateConfigure
		//State;defSpeedBp_mlPmin;defSpeedFp_mlPmin;defPol_V;defPol_Direction;duration_sec & Patient profile!
		{ tdWAKDAllStateConfigure db_8;
		db_8.stateAllStoppedConfig.defSpeedBp_mlPmin=(int) ( 0.0 * (float)FIXPOINTSHIFT_PUMP_FLOW);
		db_8.stateAllStoppedConfig.defSpeedFp_mlPmin=(int) ( 0.0 * (float)FIXPOINTSHIFT_PUMP_FLOW);
		db_8.stateAllStoppedConfig.defPol_V=(int) ( 0.0 * (float)FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateAllStoppedConfig.direction=1;
		db_8.stateAllStoppedConfig.duration_sec=0;
		db_8.stateDialysisConfig.defSpeedBp_mlPmin=(int) (100.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateDialysisConfig.defSpeedFp_mlPmin=(int) (20.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateDialysisConfig.defPol_V=(int) (4.0*FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateDialysisConfig.direction=1;
		db_8.stateDialysisConfig.duration_sec= 40;//changed for debugging, it was before 5*60*60; 
		db_8.stateRegen1Config.defSpeedBp_mlPmin=(int) (10.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateRegen1Config.defSpeedFp_mlPmin=(int) (5.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateRegen1Config.defPol_V=(int) (1.1*FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateRegen1Config.direction=1;
		db_8.stateRegen1Config.duration_sec=2*60;//changed for debugging, it was before: 5*60;  // 5 Minutes
		db_8.stateUltrafiltrationConfig.defSpeedBp_mlPmin=(int) (100.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateUltrafiltrationConfig.defSpeedFp_mlPmin=(int) (30.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateUltrafiltrationConfig.defPol_V=(int) (0.0*FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateUltrafiltrationConfig.direction=1;
		db_8.stateUltrafiltrationConfig.duration_sec=2*60;//changed for debugging, it was before: 30*60; ;  // 30 Minutes
		db_8.stateRegen2Config.defSpeedBp_mlPmin=(int) (10.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateRegen2Config.defSpeedFp_mlPmin=(int) (5.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateRegen2Config.defPol_V=(int) (2.2 * (float) FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateRegen2Config.direction=0;
		db_8.stateRegen2Config.duration_sec=2*60; //changed for debugging, it was before: 5*60; ;  // 5 Minutes		//!!!
		db_8.stateMaintenanceConfig.defSpeedBp_mlPmin=(int) (0.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateMaintenanceConfig.defSpeedFp_mlPmin=(int) (0.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateMaintenanceConfig.defPol_V=(int) (0.0*FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateMaintenanceConfig.direction=0;
		db_8.stateMaintenanceConfig.duration_sec=0; 
		db_8.stateNoDialysateConfig.defSpeedBp_mlPmin=(int) (10.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateNoDialysateConfig.defSpeedFp_mlPmin=(int) (0.0*FIXPOINTSHIFT_PUMP_FLOW); 
		db_8.stateNoDialysateConfig.defPol_V=(int) (0.0*FIXPOINTSHIFT_POLARIZ_VOLT);
		db_8.stateNoDialysateConfig.direction=1;
		db_8.stateNoDialysateConfig.duration_sec=0; 
		// now ALWAYS! call unified_write for patientProfile, since it is member of WAKDAllStateConfigure
		memcpy(&db_8.patientProfile, &db_7, sizeof(tdPatientProfile));
		//remove(FILE_WAKDALLSTATECONFIGURE);
		unified_write(dataID_WAKDAllStateConfigure,&db_8);
		// dont forget about patientProfile
		} //
		
		


	//stateThresh_configuration.csv - tdAllStatesParametersSCC - dataID_AllStatesParametersSCC
		//State;NaAbsL;NaAbsH;NaTrdT;NaTrdLPsT;NaTrdHPsT;KAbsL;KAbsH;KAAL;KAAH;KTrdT;KTrdLPsT;KTrdHPsT;CaAbsL;CaAbsH;CaAAbsL;CaAAbsH;CaTrdT;CaTrdLPsT;CaTrdHPsT;UreaAAbsH;UreaTrdT;UreaTrdHPsT;CreaAbsL;CreaAbsH;CreaTrdT;CreaTrdHPsT;PhosAbsL;PhosAbsH;PhosAAbsH;PhosTrdT;PhosTrdLPsT;PhosTrdHPsT;HCO3AAbsL;HCO3AbsL;HCO3AbsH;HCO3TrdT;HCO3TrdLPsT;HCO3TrdHPsT;PhAAbsL;PhAAbsH;PhAbsL;PhAbsH;PhTrdT;PhTrdLPsT;PhTrdHPsT;BPsysAbsL;BPsysAbsH;BPsysAAbsH;BPdiaAbsL;BPdiaAbsH;BPdiaAAbsH;pumpFaccDevi;pumpBaccDevi;VertDeflecitonT;WghtAbsL;WghtAbsH;WghtTrdT;FDpTL;FDpTH;BPSoTH;BPSoTL;BPSiTH;BPSiTL;FPSoTH;FPSoTL;FPSTH;FPSTL;BTSoTH;BTSoTL;BTSiTH;BTSiTL;BatStatT;f_K;SCAP_K;P_K;Fref_K;cap_K;f_Ph;SCAP_Ph;P_Ph;Fref_Ph;cap_Ph;A_dm2;HCO3Adr;wgtNa;wgtK;wgtCa;wgtUr;wgtCrea;wgtPhos;wgtHCO3;mspT
		{ tdAllStatesParametersSCC db_9;
		// for parametersSCC_stateAllStopped
			db_9.parametersSCC_stateAllStopped.NaAbsL=0.1;     // lower Na absolute threshold
			db_9.parametersSCC_stateAllStopped.NaAbsH=999;     // upper Na absolute threshold
			db_9.parametersSCC_stateAllStopped.NaTrdT=0;      // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.NaTrdLPsT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.NaTrdHPsT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateAllStopped.KAbsL=0.1;      // lower K absolute threshold
			db_9.parametersSCC_stateAllStopped.KAbsH=999;        // upper K absolute threshold
			db_9.parametersSCC_stateAllStopped.KAAL=0.1;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.KAAH=999;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.KTrdT=0;       // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.CaAbsL=0.1;    // lower Ca absolute threshold
			db_9.parametersSCC_stateAllStopped.CaAbsH=999;     // upper Ca absolute threshold
			db_9.parametersSCC_stateAllStopped.CaAAbsL=0.1;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.CaAAbsH=999;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.CaTrdT=0;       //actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.CaTrdLPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.CaTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateAllStopped.UreaAAbsH=100;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.UreaTrdT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.UreaTrdHPsT=0; // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.CreaAbsL=0.1;  	// lower Crea absolute threshold
			db_9.parametersSCC_stateAllStopped.CreaAbsH=999;   // upper Crea absolute threshold
			db_9.parametersSCC_stateAllStopped.CreaTrdT=0;  	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.CreaTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.PhosAbsL=0.1;   // lower Phos absolute threshold
			db_9.parametersSCC_stateAllStopped.PhosAbsH=999;   // higher Phos absolute threshold
			db_9.parametersSCC_stateAllStopped.PhosAAbsH=999;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.PhosTrdT=0;    	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.PhosTrdLPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.PhosTrdHPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.HCO3AAbsL=0.1;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.HCO3AbsL=0.1;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateAllStopped.HCO3AbsH=999;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateAllStopped.HCO3TrdT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.HCO3TrdLPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.HCO3TrdHPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.PhAAbsL=0.1;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.PhAAbsH=10;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.PhAbsL=3;    // lower pH absolute threshold
			db_9.parametersSCC_stateAllStopped.PhAbsH=9;    // upper pH absolute threshold
			db_9.parametersSCC_stateAllStopped.PhTrdT=0;		// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.PhTrdLPsT=0;		// actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateAllStopped.PhTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateAllStopped.BPsysAbsL=70;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateAllStopped.BPsysAbsH=140;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateAllStopped.BPsysAAbsH=50; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateAllStopped.BPdiaAbsL=40;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateAllStopped.BPdiaAbsH=100;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateAllStopped.BPdiaAAbsH=120; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateAllStopped.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateAllStopped.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateAllStopped.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far) [? no information about units or values so far)

			db_9.parametersSCC_stateAllStopped.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateAllStopped.WghtAbsH=100;   // upper Weight absolute threshold
			db_9.parametersSCC_stateAllStopped.WghtTrdT=200;   // Weight thrend threshold
			db_9.parametersSCC_stateAllStopped.FDpTL=90;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateAllStopped.FDpTH=90;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateAllStopped.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateAllStopped.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateAllStopped.BPSiTH=50;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateAllStopped.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateAllStopped.FPSoTH=50;    // sensor 'FPSo' pressure upper threshold
			db_9.parametersSCC_stateAllStopped.FPSoTL=1;       // sensor 'FPSo' pressure lower threshold
			db_9.parametersSCC_stateAllStopped.FPSTH=50;    // sensor 'FPS' pressure upper threshold
			db_9.parametersSCC_stateAllStopped.FPSTL=1;       // sensor 'FPS' pressure lower threshold

			db_9.parametersSCC_stateAllStopped.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateAllStopped.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateAllStopped.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateAllStopped.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateAllStopped.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateAllStopped.f_K=2;
			db_9.parametersSCC_stateAllStopped.SCAP_K=2;
			db_9.parametersSCC_stateAllStopped.P_K=2;
			db_9.parametersSCC_stateAllStopped.Fref_K=2;
			db_9.parametersSCC_stateAllStopped.cap_K=2;
			db_9.parametersSCC_stateAllStopped.f_Ph=2;
			db_9.parametersSCC_stateAllStopped.SCAP_Ph=2;
			db_9.parametersSCC_stateAllStopped.P_Ph=2;
			db_9.parametersSCC_stateAllStopped.Fref_Ph=2;
			db_9.parametersSCC_stateAllStopped.cap_Ph=2;
			db_9.parametersSCC_stateAllStopped.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateAllStopped.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateAllStopped.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateAllStopped.HCO3Adr=2;
			
			db_9.parametersSCC_stateAllStopped.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateAllStopped.wgtK=1;
			db_9.parametersSCC_stateAllStopped.wgtCa=1;
			db_9.parametersSCC_stateAllStopped.wgtUr=1;
			db_9.parametersSCC_stateAllStopped.wgtCrea=1;
			db_9.parametersSCC_stateAllStopped.wgtPhos=1;
			db_9.parametersSCC_stateAllStopped.wgtHCO3=1;
			db_9.parametersSCC_stateAllStopped.mspT=1; // set the threshold here!	
			
		// for parametersSCC_stateDialysis
			db_9.parametersSCC_stateDialysis.NaAbsL=135;     // lower Na absolute threshold
			db_9.parametersSCC_stateDialysis.NaAbsH=146;     // upper Na absolute threshold
			db_9.parametersSCC_stateDialysis.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.NaTrdLPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.NaTrdHPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateDialysis.KAbsL=4;      // lower K absolute threshold
			db_9.parametersSCC_stateDialysis.KAbsH=5;        // upper K absolute threshold
			db_9.parametersSCC_stateDialysis.KAAL=3;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateDialysis.KAAH=6;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateDialysis.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.CaAbsL=1.05;    // lower Ca absolute threshold
			db_9.parametersSCC_stateDialysis.CaAbsH=1.3;     // upper Ca absolute threshold
			db_9.parametersSCC_stateDialysis.CaAAbsL=0.9;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateDialysis.CaAAbsH=1.5;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateDialysis.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateDialysis.UreaAAbsH=40;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateDialysis.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.CreaAbsL=0.15;  // lower Crea absolute threshold
			db_9.parametersSCC_stateDialysis.CreaAbsH=0.5;   // upper Crea absolute threshold
			db_9.parametersSCC_stateDialysis.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.PhosAbsL=0.8;   // lower Phos absolute threshold
			db_9.parametersSCC_stateDialysis.PhosAbsH=1.5;   // higher Phos absolute threshold
			db_9.parametersSCC_stateDialysis.PhosAAbsH=1.8;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateDialysis.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.HCO3AAbsL=15;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateDialysis.HCO3AbsL=22;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateDialysis.HCO3AbsH=30;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateDialysis.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.PhAAbsL=7.3;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateDialysis.PhAAbsH=7.6;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateDialysis.PhAbsL=7.35;    // lower pH absolute threshold
			db_9.parametersSCC_stateDialysis.PhAbsH=7.45;    // upper pH absolute threshold
			db_9.parametersSCC_stateDialysis.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateDialysis.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateDialysis.BPsysAbsL=90;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateDialysis.BPsysAbsH=130;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateDialysis.BPsysAAbsH=140; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateDialysis.BPdiaAbsL=50;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateDialysis.BPdiaAbsH=80;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateDialysis.BPdiaAAbsH=90; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateDialysis.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateDialysis.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateDialysis.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateDialysis.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateDialysis.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateDialysis.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateDialysis.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateDialysis.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateDialysis.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateDialysis.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateDialysis.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateDialysis.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateDialysis.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateDialysis.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateDialysis.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateDialysis.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateDialysis.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateDialysis.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateDialysis.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateDialysis.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateDialysis.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateDialysis.f_K=2;
			db_9.parametersSCC_stateDialysis.SCAP_K=2;
			db_9.parametersSCC_stateDialysis.P_K=2;
			db_9.parametersSCC_stateDialysis.Fref_K=2;
			db_9.parametersSCC_stateDialysis.cap_K=2;
			db_9.parametersSCC_stateDialysis.f_Ph=2;
			db_9.parametersSCC_stateDialysis.SCAP_Ph=2;
			db_9.parametersSCC_stateDialysis.P_Ph=2;
			db_9.parametersSCC_stateDialysis.Fref_Ph=2;
			db_9.parametersSCC_stateDialysis.cap_Ph=2;
			db_9.parametersSCC_stateDialysis.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateDialysis.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateDialysis.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateDialysis.HCO3Adr=2;
			
			db_9.parametersSCC_stateDialysis.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateDialysis.wgtK=1;
			db_9.parametersSCC_stateDialysis.wgtCa=1;
			db_9.parametersSCC_stateDialysis.wgtUr=1;
			db_9.parametersSCC_stateDialysis.wgtCrea=1;
			db_9.parametersSCC_stateDialysis.wgtPhos=1;
			db_9.parametersSCC_stateDialysis.wgtHCO3=1;
			db_9.parametersSCC_stateDialysis.mspT=1; // set the threshold here!	

		// for parametersSCC_stateRegen1
			db_9.parametersSCC_stateRegen1.NaAbsL=135;     // lower Na absolute threshold
			db_9.parametersSCC_stateRegen1.NaAbsH=146;     // upper Na absolute threshold
			db_9.parametersSCC_stateRegen1.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.NaTrdLPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.NaTrdHPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateRegen1.KAbsL=4;      // lower K absolute threshold
			db_9.parametersSCC_stateRegen1.KAbsH=5;        // upper K absolute threshold
			db_9.parametersSCC_stateRegen1.KAAL=3;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateRegen1.KAAH=6;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateRegen1.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.CaAbsL=1.05;    // lower Ca absolute threshold
			db_9.parametersSCC_stateRegen1.CaAbsH=1.3;     // upper Ca absolute threshold
			db_9.parametersSCC_stateRegen1.CaAAbsL=0.9;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateRegen1.CaAAbsH=1.5;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateRegen1.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateRegen1.UreaAAbsH=40;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateRegen1.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.CreaAbsL=0.15;  // lower Crea absolute threshold
			db_9.parametersSCC_stateRegen1.CreaAbsH=0.5;   // upper Crea absolute threshold
			db_9.parametersSCC_stateRegen1.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.PhosAbsL=0.8;   // lower Phos absolute threshold
			db_9.parametersSCC_stateRegen1.PhosAbsH=1.5;   // higher Phos absolute threshold
			db_9.parametersSCC_stateRegen1.PhosAAbsH=1.8;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateRegen1.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.HCO3AAbsL=15;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateRegen1.HCO3AbsL=22;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateRegen1.HCO3AbsH=30;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateRegen1.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.PhAAbsL=7.3;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateRegen1.PhAAbsH=7.6;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateRegen1.PhAbsL=7.35;    // lower pH absolute threshold
			db_9.parametersSCC_stateRegen1.PhAbsH=7.45;    // upper pH absolute threshold
			db_9.parametersSCC_stateRegen1.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen1.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen1.BPsysAbsL=90;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateRegen1.BPsysAbsH=130;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateRegen1.BPsysAAbsH=140; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateRegen1.BPdiaAbsL=50;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateRegen1.BPdiaAbsH=80;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateRegen1.BPdiaAAbsH=90; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateRegen1.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateRegen1.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateRegen1.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateRegen1.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateRegen1.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateRegen1.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateRegen1.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateRegen1.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateRegen1.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateRegen1.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateRegen1.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateRegen1.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateRegen1.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateRegen1.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateRegen1.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateRegen1.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateRegen1.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateRegen1.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateRegen1.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateRegen1.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateRegen1.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateRegen1.f_K=2;
			db_9.parametersSCC_stateRegen1.SCAP_K=2;
			db_9.parametersSCC_stateRegen1.P_K=2;
			db_9.parametersSCC_stateRegen1.Fref_K=2;
			db_9.parametersSCC_stateRegen1.cap_K=2;
			db_9.parametersSCC_stateRegen1.f_Ph=2;
			db_9.parametersSCC_stateRegen1.SCAP_Ph=2;
			db_9.parametersSCC_stateRegen1.P_Ph=2;
			db_9.parametersSCC_stateRegen1.Fref_Ph=2;
			db_9.parametersSCC_stateRegen1.cap_Ph=2;
			db_9.parametersSCC_stateRegen1.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateRegen1.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateRegen1.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateRegen1.HCO3Adr=2;
			
			db_9.parametersSCC_stateRegen1.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateRegen1.wgtK=1;
			db_9.parametersSCC_stateRegen1.wgtCa=1;
			db_9.parametersSCC_stateRegen1.wgtUr=1;
			db_9.parametersSCC_stateRegen1.wgtCrea=1;
			db_9.parametersSCC_stateRegen1.wgtPhos=1;
			db_9.parametersSCC_stateRegen1.wgtHCO3=1;
			db_9.parametersSCC_stateRegen1.mspT=1; // set the threshold here!	
			
		// for parametersSCC_stateUltrafiltration
			db_9.parametersSCC_stateUltrafiltration.NaAbsL=135;     // lower Na absolute threshold
			db_9.parametersSCC_stateUltrafiltration.NaAbsH=146;     // upper Na absolute threshold
			db_9.parametersSCC_stateUltrafiltration.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.NaTrdLPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.NaTrdHPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateUltrafiltration.KAbsL=4;      // lower K absolute threshold
			db_9.parametersSCC_stateUltrafiltration.KAbsH=5;        // upper K absolute threshold
			db_9.parametersSCC_stateUltrafiltration.KAAL=3;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.KAAH=6;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.CaAbsL=1.05;    // lower Ca absolute threshold
			db_9.parametersSCC_stateUltrafiltration.CaAbsH=1.3;     // upper Ca absolute threshold
			db_9.parametersSCC_stateUltrafiltration.CaAAbsL=0.9;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.CaAAbsH=1.5;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateUltrafiltration.UreaAAbsH=40;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.CreaAbsL=0.15;  // lower Crea absolute threshold
			db_9.parametersSCC_stateUltrafiltration.CreaAbsH=0.5;   // upper Crea absolute threshold
			db_9.parametersSCC_stateUltrafiltration.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.PhosAbsL=0.8;   // lower Phos absolute threshold
			db_9.parametersSCC_stateUltrafiltration.PhosAbsH=1.5;   // higher Phos absolute threshold
			db_9.parametersSCC_stateUltrafiltration.PhosAAbsH=1.8;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.HCO3AAbsL=15;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.HCO3AbsL=22;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateUltrafiltration.HCO3AbsH=30;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateUltrafiltration.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.PhAAbsL=7.3;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.PhAAbsH=7.6;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.PhAbsL=7.35;    // lower pH absolute threshold
			db_9.parametersSCC_stateUltrafiltration.PhAbsH=7.45;    // upper pH absolute threshold
			db_9.parametersSCC_stateUltrafiltration.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateUltrafiltration.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateUltrafiltration.BPsysAbsL=90;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateUltrafiltration.BPsysAbsH=130;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateUltrafiltration.BPsysAAbsH=140; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateUltrafiltration.BPdiaAbsL=50;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateUltrafiltration.BPdiaAbsH=80;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateUltrafiltration.BPdiaAAbsH=90; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateUltrafiltration.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateUltrafiltration.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateUltrafiltration.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateUltrafiltration.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateUltrafiltration.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateUltrafiltration.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateUltrafiltration.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateUltrafiltration.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateUltrafiltration.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateUltrafiltration.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateUltrafiltration.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateUltrafiltration.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateUltrafiltration.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateUltrafiltration.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateUltrafiltration.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateUltrafiltration.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateUltrafiltration.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateUltrafiltration.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateUltrafiltration.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateUltrafiltration.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateUltrafiltration.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateUltrafiltration.f_K=2;
			db_9.parametersSCC_stateUltrafiltration.SCAP_K=2;
			db_9.parametersSCC_stateUltrafiltration.P_K=2;
			db_9.parametersSCC_stateUltrafiltration.Fref_K=2;
			db_9.parametersSCC_stateUltrafiltration.cap_K=2;
			db_9.parametersSCC_stateUltrafiltration.f_Ph=2;
			db_9.parametersSCC_stateUltrafiltration.SCAP_Ph=2;
			db_9.parametersSCC_stateUltrafiltration.P_Ph=2;
			db_9.parametersSCC_stateUltrafiltration.Fref_Ph=2;
			db_9.parametersSCC_stateUltrafiltration.cap_Ph=2;
			db_9.parametersSCC_stateUltrafiltration.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateUltrafiltration.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateUltrafiltration.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateUltrafiltration.HCO3Adr=2;
			
			db_9.parametersSCC_stateUltrafiltration.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateUltrafiltration.wgtK=1;
			db_9.parametersSCC_stateUltrafiltration.wgtCa=1;
			db_9.parametersSCC_stateUltrafiltration.wgtUr=1;
			db_9.parametersSCC_stateUltrafiltration.wgtCrea=1;
			db_9.parametersSCC_stateUltrafiltration.wgtPhos=1;
			db_9.parametersSCC_stateUltrafiltration.wgtHCO3=1;
			db_9.parametersSCC_stateUltrafiltration.mspT=1; // set the threshold here!	
			
		// for parametersSCC_stateRegen2
			db_9.parametersSCC_stateRegen2.NaAbsL=135;     // lower Na absolute threshold
			db_9.parametersSCC_stateRegen2.NaAbsH=146;     // upper Na absolute threshold
			db_9.parametersSCC_stateRegen2.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.NaTrdLPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.NaTrdHPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateRegen2.KAbsL=4;      // lower K absolute threshold
			db_9.parametersSCC_stateRegen2.KAbsH=5;        // upper K absolute threshold
			db_9.parametersSCC_stateRegen2.KAAL=3;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateRegen2.KAAH=6;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateRegen2.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.CaAbsL=1.05;    // lower Ca absolute threshold
			db_9.parametersSCC_stateRegen2.CaAbsH=1.3;     // upper Ca absolute threshold
			db_9.parametersSCC_stateRegen2.CaAAbsL=0.9;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateRegen2.CaAAbsH=1.5;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateRegen2.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateRegen2.UreaAAbsH=40;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateRegen2.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.CreaAbsL=0.15;  // lower Crea absolute threshold
			db_9.parametersSCC_stateRegen2.CreaAbsH=0.5;   // upper Crea absolute threshold
			db_9.parametersSCC_stateRegen2.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.PhosAbsL=0.8;   // lower Phos absolute threshold
			db_9.parametersSCC_stateRegen2.PhosAbsH=1.5;   // higher Phos absolute threshold
			db_9.parametersSCC_stateRegen2.PhosAAbsH=1.8;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateRegen2.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.HCO3AAbsL=15;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateRegen2.HCO3AbsL=22;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateRegen2.HCO3AbsH=30;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateRegen2.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.PhAAbsL=7.3;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateRegen2.PhAAbsH=7.6;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateRegen2.PhAbsL=7.35;    // lower pH absolute threshold
			db_9.parametersSCC_stateRegen2.PhAbsH=7.45;    // upper pH absolute threshold
			db_9.parametersSCC_stateRegen2.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateRegen2.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateRegen2.BPsysAbsL=90;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateRegen2.BPsysAbsH=130;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateRegen2.BPsysAAbsH=140; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateRegen2.BPdiaAbsL=50;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateRegen2.BPdiaAbsH=80;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateRegen2.BPdiaAAbsH=90; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateRegen2.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateRegen2.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateRegen2.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateRegen2.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateRegen2.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateRegen2.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateRegen2.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateRegen2.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateRegen2.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateRegen2.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateRegen2.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateRegen2.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateRegen2.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateRegen2.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateRegen2.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateRegen2.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateRegen2.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateRegen2.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateRegen2.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateRegen2.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateRegen2.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateRegen2.f_K=2;
			db_9.parametersSCC_stateRegen2.SCAP_K=2;
			db_9.parametersSCC_stateRegen2.P_K=2;
			db_9.parametersSCC_stateRegen2.Fref_K=2;
			db_9.parametersSCC_stateRegen2.cap_K=2;
			db_9.parametersSCC_stateRegen2.f_Ph=2;
			db_9.parametersSCC_stateRegen2.SCAP_Ph=2;
			db_9.parametersSCC_stateRegen2.P_Ph=2;
			db_9.parametersSCC_stateRegen2.Fref_Ph=2;
			db_9.parametersSCC_stateRegen2.cap_Ph=2;
			db_9.parametersSCC_stateRegen2.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateRegen2.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateRegen2.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateRegen2.HCO3Adr=2;
			
			db_9.parametersSCC_stateRegen2.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateRegen2.wgtK=1;
			db_9.parametersSCC_stateRegen2.wgtCa=1;
			db_9.parametersSCC_stateRegen2.wgtUr=1;
			db_9.parametersSCC_stateRegen2.wgtCrea=1;
			db_9.parametersSCC_stateRegen2.wgtPhos=1;
			db_9.parametersSCC_stateRegen2.wgtHCO3=1;
			db_9.parametersSCC_stateRegen2.mspT=1; // set the threshold here!	
			
		// for parametersSCC_stateMaintenance
			db_9.parametersSCC_stateMaintenance.NaAbsL=135;     // lower Na absolute threshold
			db_9.parametersSCC_stateMaintenance.NaAbsH=146;     // upper Na absolute threshold
			db_9.parametersSCC_stateMaintenance.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.NaTrdLPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.NaTrdHPsT=0;   // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateMaintenance.KAbsL=4;      // lower K absolute threshold
			db_9.parametersSCC_stateMaintenance.KAbsH=5;        // upper K absolute threshold
			db_9.parametersSCC_stateMaintenance.KAAL=3;         // lower K absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.KAAH=6;         // upper K absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.CaAbsL=1.05;    // lower Ca absolute threshold
			db_9.parametersSCC_stateMaintenance.CaAbsH=1.3;     // upper Ca absolute threshold
			db_9.parametersSCC_stateMaintenance.CaAAbsL=0.9;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.CaAAbsH=1.5;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateMaintenance.UreaAAbsH=40;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.CreaAbsL=0.15;  // lower Crea absolute threshold
			db_9.parametersSCC_stateMaintenance.CreaAbsH=0.5;   // upper Crea absolute threshold
			db_9.parametersSCC_stateMaintenance.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.PhosAbsL=0.8;   // lower Phos absolute threshold
			db_9.parametersSCC_stateMaintenance.PhosAbsH=1.5;   // higher Phos absolute threshold
			db_9.parametersSCC_stateMaintenance.PhosAAbsH=1.8;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.HCO3AAbsL=15;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.HCO3AbsL=22;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateMaintenance.HCO3AbsH=30;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateMaintenance.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.PhAAbsL=7.3;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.PhAAbsH=7.6;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.PhAbsL=7.35;    // lower pH absolute threshold
			db_9.parametersSCC_stateMaintenance.PhAbsH=7.45;    // upper pH absolute threshold
			db_9.parametersSCC_stateMaintenance.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateMaintenance.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateMaintenance.BPsysAbsL=90;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateMaintenance.BPsysAbsH=130;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateMaintenance.BPsysAAbsH=140; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateMaintenance.BPdiaAbsL=50;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateMaintenance.BPdiaAbsH=80;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateMaintenance.BPdiaAAbsH=90; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateMaintenance.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateMaintenance.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateMaintenance.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateMaintenance.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateMaintenance.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateMaintenance.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateMaintenance.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateMaintenance.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateMaintenance.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateMaintenance.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateMaintenance.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateMaintenance.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateMaintenance.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateMaintenance.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateMaintenance.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateMaintenance.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateMaintenance.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateMaintenance.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateMaintenance.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateMaintenance.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateMaintenance.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateMaintenance.f_K=2;
			db_9.parametersSCC_stateMaintenance.SCAP_K=2;
			db_9.parametersSCC_stateMaintenance.P_K=2;
			db_9.parametersSCC_stateMaintenance.Fref_K=2;
			db_9.parametersSCC_stateMaintenance.cap_K=2;
			db_9.parametersSCC_stateMaintenance.f_Ph=2;
			db_9.parametersSCC_stateMaintenance.SCAP_Ph=2;
			db_9.parametersSCC_stateMaintenance.P_Ph=2;
			db_9.parametersSCC_stateMaintenance.Fref_Ph=2;
			db_9.parametersSCC_stateMaintenance.cap_Ph=2;
			db_9.parametersSCC_stateMaintenance.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateMaintenance.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateMaintenance.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateMaintenance.HCO3Adr=2;
			
			db_9.parametersSCC_stateMaintenance.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateMaintenance.wgtK=1;
			db_9.parametersSCC_stateMaintenance.wgtCa=1;
			db_9.parametersSCC_stateMaintenance.wgtUr=1;
			db_9.parametersSCC_stateMaintenance.wgtCrea=1;
			db_9.parametersSCC_stateMaintenance.wgtPhos=1;
			db_9.parametersSCC_stateMaintenance.wgtHCO3=1;
			db_9.parametersSCC_stateMaintenance.mspT=1; // set the threshold here!	
			
		// for parametersSCC_stateNoDialysate
			db_9.parametersSCC_stateNoDialysate.NaAbsL=0.1;     // lower Na absolute threshold
			db_9.parametersSCC_stateNoDialysate.NaAbsH=150;     // upper Na absolute threshold
			db_9.parametersSCC_stateNoDialysate.NaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.NaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.NaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateNoDialysate.KAbsL=0.1;      // lower K absolute threshold
			db_9.parametersSCC_stateNoDialysate.KAbsH=150;    // upper K absolute threshold
			db_9.parametersSCC_stateNoDialysate.KAAL=0.1;       // lower K absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.KAAH=200;     // upper K absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.KTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.CaAbsL=0.1;    // lower Ca absolute threshold
			db_9.parametersSCC_stateNoDialysate.CaAbsH=150;     // upper Ca absolute threshold
			db_9.parametersSCC_stateNoDialysate.CaAAbsL=0.1;      // lower K absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.CaAAbsH=200;      // upper K absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.CaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.CaTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.CaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			
			db_9.parametersSCC_stateNoDialysate.UreaAAbsH=150;   // upper Urea absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.UreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.UreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.CreaAbsL=0.1;  // lower Crea absolute threshold
			db_9.parametersSCC_stateNoDialysate.CreaAbsH=150;   // upper Crea absolute threshold
			db_9.parametersSCC_stateNoDialysate.CreaTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.CreaTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.PhosAbsL=0.1;   // lower Phos absolute threshold
			db_9.parametersSCC_stateNoDialysate.PhosAbsH=150;   // higher Phos absolute threshold
			db_9.parametersSCC_stateNoDialysate.PhosAAbsH=200;    // upper Phos absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.PhosTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.PhosTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.PhosTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.HCO3AAbsL=0.1;   // lower HCO3 absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.HCO3AbsL=0.2;    // lower HCO3 absolute threshold
			db_9.parametersSCC_stateNoDialysate.HCO3AbsH=200;    // upper HCO3 absolute threshold
			db_9.parametersSCC_stateNoDialysate.HCO3TrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.HCO3TrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.HCO3TrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.PhAAbsL=0.1;    // lower pH absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.PhAAbsH=100;    // upper pH absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.PhAbsL=0.2;    // lower pH absolute threshold
			db_9.parametersSCC_stateNoDialysate.PhAbsH=10;    // upper pH absolute threshold
			db_9.parametersSCC_stateNoDialysate.PhTrdT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.PhTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
			db_9.parametersSCC_stateNoDialysate.PhTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

			db_9.parametersSCC_stateNoDialysate.BPsysAbsL=2;    // lower BPsys absolute threshold
			db_9.parametersSCC_stateNoDialysate.BPsysAbsH=300;  // upper BPsys absolute threshold
			db_9.parametersSCC_stateNoDialysate.BPsysAAbsH=400; // upper BPsys absolute alarm threshold
			db_9.parametersSCC_stateNoDialysate.BPdiaAbsL=300;  // lower BPdia absolute threshold
			db_9.parametersSCC_stateNoDialysate.BPdiaAbsH=300;  // upper BPdia absolute threshold    
			db_9.parametersSCC_stateNoDialysate.BPdiaAAbsH=400; // upper BPdia absolute alarm threshold

			db_9.parametersSCC_stateNoDialysate.pumpFaccDevi=10;	// accepted flow deviation ml/min
			db_9.parametersSCC_stateNoDialysate.pumpBaccDevi=10;	// accepted flow deviation ml/min

			db_9.parametersSCC_stateNoDialysate.VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far)

			db_9.parametersSCC_stateNoDialysate.WghtAbsL=20;     // lower Weight absolute threshold
			db_9.parametersSCC_stateNoDialysate.WghtAbsH=3000;   // upper Weight absolute threshold
			db_9.parametersSCC_stateNoDialysate.WghtTrdT=5555;   // Weight thrend threshold
			db_9.parametersSCC_stateNoDialysate.FDpTL=1;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
			db_9.parametersSCC_stateNoDialysate.FDpTH=2000;    // Fluid Extraction Deviation Percentage Threshold High(increase)

			db_9.parametersSCC_stateNoDialysate.BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
			db_9.parametersSCC_stateNoDialysate.BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
			db_9.parametersSCC_stateNoDialysate.BPSiTH=5000;    // sensor 'BPSi' pressure upper threshold
			db_9.parametersSCC_stateNoDialysate.BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
			db_9.parametersSCC_stateNoDialysate.FPSoTH=5000;    // sensor 'FPS1' pressure upper threshold
			db_9.parametersSCC_stateNoDialysate.FPSoTL=1;       // sensor 'FPS1' pressure lower threshold
			db_9.parametersSCC_stateNoDialysate.FPSTH=5000;    // sensor 'FPS2' pressure upper threshold
			db_9.parametersSCC_stateNoDialysate.FPSTL=1;       // sensor 'FPS2' pressure lower threshold

			db_9.parametersSCC_stateNoDialysate.BTSoTH=50;     // temperature sensor BTSo value upper threshold
			db_9.parametersSCC_stateNoDialysate.BTSoTL=5;      // temperature sensor BTSo value lower threshold
			db_9.parametersSCC_stateNoDialysate.BTSiTH=50;     // temperature sensor BTSi value upper threshold
			db_9.parametersSCC_stateNoDialysate.BTSiTL=5;      // temperature sensor BTSi value lower threshold

			db_9.parametersSCC_stateNoDialysate.BatStatT=10;   //battery status threshold

			// Expected adsorption rate calculation
			db_9.parametersSCC_stateNoDialysate.f_K=2;
			db_9.parametersSCC_stateNoDialysate.SCAP_K=2;
			db_9.parametersSCC_stateNoDialysate.P_K=2;
			db_9.parametersSCC_stateNoDialysate.Fref_K=2;
			db_9.parametersSCC_stateNoDialysate.cap_K=2;
			db_9.parametersSCC_stateNoDialysate.f_Ph=2;
			db_9.parametersSCC_stateNoDialysate.SCAP_Ph=2;
			db_9.parametersSCC_stateNoDialysate.P_Ph=2;
			db_9.parametersSCC_stateNoDialysate.Fref_Ph=2;
			db_9.parametersSCC_stateNoDialysate.cap_Ph=2;
			db_9.parametersSCC_stateNoDialysate.A_dm2=20;

			// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
			// KAdr=ADR_K_exp;
			db_9.parametersSCC_stateNoDialysate.CaAdr=2;
			// UrAdr=ADR_Ur_exp;
			db_9.parametersSCC_stateNoDialysate.CreaAdr=2;
			// PhosAdr=ADR_Phos_exp;
			db_9.parametersSCC_stateNoDialysate.HCO3Adr=2;
			
			db_9.parametersSCC_stateNoDialysate.wgtNa=1; // the fuzzy importance of the adsorption of this substance
			db_9.parametersSCC_stateNoDialysate.wgtK=1;
			db_9.parametersSCC_stateNoDialysate.wgtCa=1;
			db_9.parametersSCC_stateNoDialysate.wgtUr=1;
			db_9.parametersSCC_stateNoDialysate.wgtCrea=1;
			db_9.parametersSCC_stateNoDialysate.wgtPhos=1;
			db_9.parametersSCC_stateNoDialysate.wgtHCO3=1;
			db_9.parametersSCC_stateNoDialysate.mspT=1; // set the threshold here!
		unified_write(dataID_AllStatesParametersSCC,&db_9); };		//*/
		return 0;
}
