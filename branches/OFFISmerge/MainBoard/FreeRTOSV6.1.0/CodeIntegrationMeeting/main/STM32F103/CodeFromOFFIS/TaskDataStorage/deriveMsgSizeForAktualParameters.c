/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "deriveMsgSizeForAktualParameters.h"

uint16_t deriveMsgSizeForAktualParameters(uint16_t numberParameter, void *pData, char *PREFIX)
{
	// size of msg to send back. 8 Bytes for header
	// one byte for the WAKD state in question
	// and more to figure out below.
	uint16_t msgSize = sizeof(tdMsgOnly)+1;

	int i = 0;
	// Start loop through the parameters. Right after the header + one byte for defining the
	// relevant state the loop begins. And continues from there to the number of listed parameter names.
	for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
	{
		tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
		switch (parameter) {
			case parameter_name: {
				// char	name[50];
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating 50 bytes for parameter '%s'.", parameterToString(parameter));
				#endif
				// 50 bytes will be needed to store the string. One extra byte is needed defining the size of the string (50)
				msgSize = msgSize + 51;
				break;
			}
			case parameter_gender:						// char	gender; // m/f
			case parameter_measureTimeHours:			// uint8_t	measureTimeHours;		// daily time for weight & bp measurement, hour
			case parameter_measureTimeMinutes: {		// uint8_t 	measureTimeMinutes;		// daily time for weight & bp measurement, minute
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating 1 byte for parameter '%s'.", parameterToString(parameter));
				#endif
				msgSize = msgSize + 1;
				break;
			}
			case parameter_minDialPlasFlow:				//	uint16_t  	minDialPlasFlow;		// minimal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
			case parameter_maxDialPlasFlow:				//	uint16_t  	maxDialPlasFlow;		// maximal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
			case parameter_minDialVoltage:				//	uint16_t	minDialVoltage;			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
			case parameter_maxDialVoltage: {			//	uint16_t	maxDialVoltage;			// maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating 2 bytes for parameter '%s'.", parameterToString(parameter));
				#endif
				msgSize = msgSize + 2;
				break;
			}
			case parameter_wghtCtlTarget:				// uint32_t	wghtCtlTarget;			// weight control target [same unit as weigth measurement on RTB]
			case parameter_wghtCtlLastMeasurement:		// uint32_t	wghtCtlLastMeasurement;	// weight control last measurement [same unit as weigth measurement on RTB]
			case parameter_wghtCtlDefRemPerDay:			// uint32_t	wghtCtlDefRemPerDay;	// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
			case parameter_kCtlTarget:					// uint32_t	kCtlTarget;				// potassium control target use FIXPOINTSHIFT for [mmol/l]
			case parameter_kCtlLastMeasurement:			// uint32_t	kCtlLastMeasurement;	// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
			case parameter_kCtlDefRemPerDay:			// uint32_t	kCtlDefRemPerDay;		// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
			case parameter_urCtlTarget:					// uint32_t	urCtlTarget;			// urea control target use FIXPOINTSHIFT for [mmol/l]
			case parameter_urCtlLastMeasurement:		// uint32_t	urCtlLastMeasurement;	// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
			case parameter_urCtlDefRemPerDay:{			// uint32_t	urCtlDefRemPerDay;		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating 4 byte for parameter '%s'.", parameterToString(parameter));
				#endif
				msgSize = msgSize + 4;
				break;
			}
			default: {
				taskMessage("E", PREFIX, "Unknown parameter inside 'dataID_CurrentParameters'.");
				break;
			}
		}
	}
	// The memory Size needs to be increased some more to also store the 'names' for each value
	msgSize = msgSize + numberParameter;	// dataSize is equivalent to the number of parameters requested.
	return(msgSize);
}
