/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "readRequestedParametersFromSD.h"

uint8_t readRequestedParametersFromSD(uint16_t numberParameter, void *pData, char *PREFIX, uint8_t *pMsg)
{
	uint8_t err = 0; // No error on default

	// We will fill in the read data after the header.
	uint16_t bufferPosition = sizeof(tdMsgOnly);

	// Right after the header comes the byte defining the state the values count for.
	uint8_t *pTmp = (uint8_t *)pData;	// pTmp to point to the beginning of the data we received from phone
	pTmp = pTmp+sizeof(tdMsgOnly);		// pTmp repointed to directly after the header. This byte is the state definition
	*(pMsg+bufferPosition) = *pTmp;		// assign this byte in the correct position for our answer.
	bufferPosition++;					// increment for next byte to store.

	// ############################################################################################################
	// ## Step 1: Process patient profile data
	// ############################################################################################################

	// Read values from the patient profile of SD card
	tdMsgPatientProfileData *pMsgPatientProfileData;
	pMsgPatientProfileData = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData));
	if ( pMsgPatientProfileData == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory for patient profile. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		if(unified_read(dataID_PatientProfile, &(pMsgPatientProfileData->patientProfile)))
		{
			taskMessage("E", PREFIX, "Reading dataID_PatientProfile failed!");
			errMsg(errmsg_ReadingSDcardFailed);
			err = 1;
		} else {
			#if defined DEBUG_DS || defined SEQ12
				taskMessage("I", PREFIX, "Reading patient profile of '%s' complete.", pMsgPatientProfileData->patientProfile.name);
		//		taskMessage("I", PREFIX, "Gender:                 '%c'.", pMsgPatientProfileData->patientProfile.gender);
		//		taskMessage("I", PREFIX, "kCtlDefRemPerDay:       '%d'.", pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);
		//		taskMessage("I", PREFIX, "kCtlLastMeasurement:    '%d'.", pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);
		//		taskMessage("I", PREFIX, "kCtlTarget:             '%d'.", pMsgPatientProfileData->patientProfile.kCtlTarget);
		//		taskMessage("I", PREFIX, "maxDialPlasFlow:        '%d'.", pMsgPatientProfileData->patientProfile.maxDialPlasFlow);
		//		taskMessage("I", PREFIX, "maxDialVoltage:         '%d'.", pMsgPatientProfileData->patientProfile.maxDialVoltage);
		//		taskMessage("I", PREFIX, "measureTimeHours:       '%d'.", pMsgPatientProfileData->patientProfile.measureTimeHours);
		//		taskMessage("I", PREFIX, "measureTimeMinutes:     '%d'.", pMsgPatientProfileData->patientProfile.measureTimeMinutes);
		//		taskMessage("I", PREFIX, "minDialPlasFlow:        '%d'.", pMsgPatientProfileData->patientProfile.minDialPlasFlow);
		//		taskMessage("I", PREFIX, "minDialVoltage:         '%d'.", pMsgPatientProfileData->patientProfile.minDialVoltage);
		//		taskMessage("I", PREFIX, "urCtlDefRemPerDay:      '%d'.", pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);
		//		taskMessage("I", PREFIX, "urCtlLastMeasurement:   '%d'.", pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);
		//		taskMessage("I", PREFIX, "urCtlTarget:            '%d'.", pMsgPatientProfileData->patientProfile.urCtlTarget);
		//		taskMessage("I", PREFIX, "wghtCtlDefRemPerDay:    '%d'.", pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);
		//		taskMessage("I", PREFIX, "wghtCtlLastMeasurement: '%d'.", pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);
		//		taskMessage("I", PREFIX, "wghtCtlTarget:          '%d'.", pMsgPatientProfileData->patientProfile.wghtCtlTarget);
			#endif

			// Copy the required values from read SD card into message to be prepared.
			int i = 0;
			for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
			{
				tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
				switch (parameter) {
					case parameter_name: {
						// char	name[50];
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %s.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.name);
						#endif
						*(pMsg+bufferPosition) = parameter_name;
						bufferPosition++;
						*(pMsg+bufferPosition) = 50;	// number of characters that follow
						bufferPosition++;
						strcpy(((char *)(pMsg+bufferPosition)), pMsgPatientProfileData->patientProfile.name);
						bufferPosition = bufferPosition + 50;
						break;
					}
					case parameter_gender: {
						// char	gender; // m/f
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.gender);
						#endif
						*(pMsg+bufferPosition) = parameter_gender;
						bufferPosition++;
						*(pMsg+bufferPosition) = pMsgPatientProfileData->patientProfile.gender;
						bufferPosition++;
						break;
					}
					case parameter_measureTimeHours: {
						//	uint8_t	measureTimeHours;		// daily time for weight & bp measurement, hour
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeHours);
						#endif
						*(pMsg+bufferPosition) = parameter_measureTimeHours;
						bufferPosition++;
						*(pMsg+bufferPosition) = pMsgPatientProfileData->patientProfile.measureTimeHours;
						bufferPosition++;
						break;
					}
					case parameter_measureTimeMinutes: {
						//		uint8_t		measureTimeMinutes;		// daily time for weight & bp measurement, minute
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeMinutes);
						#endif
						*(pMsg+bufferPosition) = parameter_measureTimeMinutes;
						bufferPosition++;
						*(pMsg+bufferPosition) = pMsgPatientProfileData->patientProfile.measureTimeMinutes;
						bufferPosition++;
						break;
					}
					case parameter_wghtCtlTarget:{
						// uint32_t	wghtCtlTarget;			// weight control target [same unit as weigth measurement on RTB]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlTarget;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+3);
						bufferPosition++;
						break;
					}
					case parameter_wghtCtlLastMeasurement:{
						// uint32_t	wghtCtlLastMeasurement;	// weight control last measurement [same unit as weigth measurement on RTB]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlLastMeasurement;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+3);
						bufferPosition++;
						break;
					}
					case parameter_wghtCtlDefRemPerDay:{
						// uint32_t	wghtCtlDefRemPerDay;	// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlDefRemPerDay;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+3);
						bufferPosition++;
						break;
					}
					case parameter_kCtlTarget:{
						// uint32_t	kCtlTarget;				// potassium control target use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlTarget;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+3);
						bufferPosition++;
						break;
					}
					case parameter_kCtlLastMeasurement:{
						// uint32_t	kCtlLastMeasurement;	// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlLastMeasurement;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+3);
						bufferPosition++;
						break;
					}
					case parameter_kCtlDefRemPerDay:{
						// uint32_t	kCtlDefRemPerDay;		// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlDefRemPerDay;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+3);
						bufferPosition++;
						break;
					}
					case parameter_urCtlTarget:{
						// uint32_t	urCtlTarget;			// urea control target use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlTarget;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+3);
						bufferPosition++;
						break;
					}
					case parameter_urCtlLastMeasurement:{
						// uint32_t	urCtlLastMeasurement;	// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlLastMeasurement;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+3);
						bufferPosition++;
						break;
					}
					case parameter_urCtlDefRemPerDay:{
						// uint32_t	urCtlDefRemPerDay;		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlDefRemPerDay;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+1);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+2);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+3);
						bufferPosition++;
						break;
					}
					case parameter_minDialPlasFlow:{
						//	uint16_t  	minDialPlasFlow;		// minimal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialPlasFlow);
						#endif
						*(pMsg+bufferPosition) = parameter_minDialPlasFlow;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialPlasFlow)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialPlasFlow)))+1);
						bufferPosition++;
						break;
					}
					case parameter_maxDialPlasFlow:{
						//	uint16_t  	maxDialPlasFlow;		// maximal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialPlasFlow);
						#endif
						*(pMsg+bufferPosition) = parameter_maxDialPlasFlow;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialPlasFlow)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialPlasFlow)))+1);
						bufferPosition++;
						break;
					}
					case parameter_minDialVoltage:{
						//	uint16_t	minDialVoltage;			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialVoltage);
						#endif
						*(pMsg+bufferPosition) = parameter_minDialVoltage;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialVoltage)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialVoltage)))+1);
						bufferPosition++;
						break;
					}
					case parameter_maxDialVoltage:{

						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialVoltage);
						#endif
						*(pMsg+bufferPosition) = parameter_maxDialVoltage;
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialVoltage)))+0);
						bufferPosition++;
						*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialVoltage)))+1);
						bufferPosition++;
						break;
					}
					default: {
						// Ignore parameter, this should be covered in one of the next switch statements.
						taskMessage("I", PREFIX, "Parameter '%s' ignored.", parameterToString(parameter));
						break;
					}
				}
			}
		}
		// Copied all values from patient profile. Free mem.
		sFree((void *)(&pMsgPatientProfileData));
	}

	// ############################################################################################################
	// ## Step 2:
	// ############################################################################################################


	// ############################################################################################################
	// ## Step 3:
	// ############################################################################################################

	return(err);
}
