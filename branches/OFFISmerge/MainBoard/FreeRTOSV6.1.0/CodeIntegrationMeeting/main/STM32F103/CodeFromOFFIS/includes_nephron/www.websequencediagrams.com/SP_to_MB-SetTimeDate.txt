SP -> MB: dataID_setTimeDate
note over SP MB
	tdMsgSetTimeDate varMsg;
	varMsg.header.recipientId = whoId_MB;
	varMsg.header.msgSize  = sizeof(tdMsgSetTimeDate);
	varMsg.header.dataId   = dataID_setTimeDate;
	varMsg.header.msgCount = 103;
	varMsg.header.issuedBy = whoId_SP;
	varMsg.TimeStamp = 1321530900;
	putData(&varMsg);
end note

alt msg OK

	note over MB
		Store diff. old to new time.
		Update local Clock.
	end note

	MB -> RTB: dataID_setTimeDate
	
	alt RTB OK
		RTB -> MB: dataID_ack
		MB -> SP: dataID_ack
	else RTB ERROR
		RTB -> MB: dataID_nack
		
		note over MB
			Use stored diff. to update
			local clock back to old time.
		end note
		MB -> SP: dataID_nack
	end
else msg ERROR
	MB -> SP: dataID_nack
end