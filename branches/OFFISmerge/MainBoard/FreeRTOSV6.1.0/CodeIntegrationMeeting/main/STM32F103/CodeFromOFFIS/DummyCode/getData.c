/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany

 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
 
/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "getData.h"
  
//extern char uartBufferCBin[SIZEUARTBUFFERCB];
//extern char uartBufferCBout[SIZEUARTBUFFERCB];

// allocating static DATA Buffer for real time board
extern tdPhysiologicalData 		staticBufferPhysiologicalData;
extern tdPhysicalsensorData		staticBufferPhysicalsensorData;
extern tdDevicesStatus			staticBufferDevicesStatusData;
extern tdActuatorData			staticBufferActuatorData;
extern tdWakdStates				staticBufferStateRTB;
extern tdWakdOperationalState	staticBufferOperationalStateRTB;

void printfDataId(tdDataId dataId);

void printfWhoId(tdWhoId whoId);

// Whenever putData transfers a msg successfully, the following variables will be set.
// This is to simulate the ACK of the recipient of this msg. This is all
// dummy code here and will be replaced by CSEM's put & get.

typedef struct strDummyAckList
{
	tdMsgOnly 				msgAck;
	struct strDummyAckList *pNextInList;
} tdDummyAckList;

static tdDummyAckList *pDummyAckListCBPA = NULL;
//static tdAckDummy ackDummyRTBPA;

// getData pulls a data block from the HW-interface buffer according to the selected sender by "whoId".
// Location of the buffer is shared knowledge between HW-interface API and TASKs. Caller of the function is
// responsible to provide pointer to sufficient storage location starting at "startData" with the size of
// "lengthData". getData will then copy "lengthDat" of Bytes to the location of "startData". If successful,
// getData will return 0, 1 if error occurred.
// Calling getData will also signal the API that the HW-interface buffer that was previously blocked can now
// be reused and overwritten gain.

// Helper function to get first byte of buffer from serial interface and interpret it as dataId
tdDataId readDataId(tdWhoId whoId)
{
	tdDataId id = 0;
	switch (whoId) {
		case whoId_RTB: {
                #if defined VP_SIMULATION
			printf("\n********** ERROR in readDataId function! for RTB use function id=decodeMsg() instead! **************\n\n");
                        #endif
			break;
		}
		case whoId_CB: {
			if (pDummyAckListCBPA != NULL)
			{	// Send an Ack on a previously send msg.
				id = dataID_ack;
			} else {
				// This was caused by ISR. Let's see what is transmitting to us.
				id = uartBufferCBin[3];
			}
			break;
		}
		default: {
			// unknown case! Error.
                #if defined VP_SIMULATION
			printf("\n********** ERROR in readDataId function! (whoId not known!) **************\n\n");
                        #endif
			break;
		}
	}
	return(id);
}

extern uint8_t newStaticBufferPhysiologicalData;
extern uint8_t newStaticBufferPhysicalData;
extern uint8_t newStaticBufferStatusData;
extern uint8_t newStaticBufferActuatorData;
extern uint8_t newStaticBufferStatesRTB;

#ifdef VP_SIMULATION
	tdDataId decodeMsg()
	{
		tdDataId id = dataID_undefined;

		if (newStaticBufferPhysiologicalData)
		{
			id = dataID_physiologicalData;
			// dummy decoding was successful. Usually here in this function
			// CSEM would put the code to take the Physiological values from the UART
			// into the static data structure. The final version
			// of decodeMsg() will be implemented later by CSEM.
			staticBufferPhysiologicalData.TimeStamp							= (tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferPhysiologicalData.ECPDataI.Sodium					= (uint16_t)		*(REG32 SENSOR_ECPI_NA);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Sodium			= 0;
			staticBufferPhysiologicalData.ECPDataI.Potassium				= (uint16_t)		*(REG32 SENSOR_ECPI_K);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Potassium		= 0;
			staticBufferPhysiologicalData.ECPDataI.pH						= (uint16_t)		*(REG32 SENSOR_ECPI_PH);
			staticBufferPhysiologicalData.ECPDataI.msOffset_pH				= 0;
			staticBufferPhysiologicalData.ECPDataI.Urea						= (uint16_t)		*(REG32 SENSOR_ECPI_UREA);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Urea			= 0;
			staticBufferPhysiologicalData.ECPDataI.Temperature				= (uint16_t)		*(REG32 SENSOR_ECPI_TEMPERATURE);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Temperature	= 0;
//			staticBufferPhysiologicalData.ECPDataI.Status					= 0;
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Status			= 0;
//			staticBufferPhysiologicalData.ECPDataI.Phosphate				= (uint16_t)		*(REG32 SENSOR_ECPI_H2PO4);
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Phosphate		= 0;
//			staticBufferPhysiologicalData.ECPDataI.Creatinine				= (uint16_t)		*(REG32 SENSOR_ECPI_C4H9N3O2);
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Creatinine		= 0;
			staticBufferPhysiologicalData.ECPDataO.Sodium					= (uint16_t)		*(REG32 SENSOR_ECPO_NA);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Sodium			= 0;
			staticBufferPhysiologicalData.ECPDataO.Potassium				= (uint16_t)		*(REG32 SENSOR_ECPO_K);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Potassium		= 0;
			staticBufferPhysiologicalData.ECPDataO.pH						= (uint16_t)		*(REG32 SENSOR_ECPO_PH);
			staticBufferPhysiologicalData.ECPDataO.msOffset_pH				= 0;
			staticBufferPhysiologicalData.ECPDataO.Urea						= (uint16_t)		*(REG32 SENSOR_ECPO_UREA);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Urea			= 0;
			staticBufferPhysiologicalData.ECPDataO.Temperature				= (uint16_t)		*(REG32 SENSOR_ECPO_TEMPERATURE);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Temperature	= 0;
//			staticBufferPhysiologicalData.ECPDataO.Status					= 0;
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Status			= 0;
//			staticBufferPhysiologicalData.ECPDataO.Phosphate				= (uint16_t)		*(REG32 SENSOR_ECPO_H2PO4);
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Phosphate		= 0;
//			staticBufferPhysiologicalData.ECPDataO.Creatinine				= (uint16_t)		*(REG32 SENSOR_ECPO_C4H9N3O2);
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Creatinine		= 0;
			newStaticBufferPhysiologicalData = 0;
		} else if (newStaticBufferPhysicalData) {
			id = dataID_physicalData;
			staticBufferPhysicalsensorData.TimeStamp								= (tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_PATI);
//			staticBufferPhysicalsensorData.statusECPI								= (uint8_t)			*(REG32 SENSOR_ECPI_STAT);
//			staticBufferPhysicalsensorData.statusECPO								= (uint8_t)			*(REG32 SENSOR_ECPO_STAT);
			staticBufferPhysicalsensorData.PressureFCI.Pressure						= (uint16_t)		*(REG32 SENSOR_FPSI_FLUIDPRESSI);
			staticBufferPhysicalsensorData.PressureFCI.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCI.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCI.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureFCO.Pressure						= (uint16_t)		*(REG32 SENSOR_FPSO_FLUIDPRESSO);
			staticBufferPhysicalsensorData.PressureFCO.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCO.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCO.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureBCI.Pressure						= (uint16_t)		*(REG32 SENSOR_BPSI_BLOODPRESI);
			staticBufferPhysicalsensorData.PressureBCI.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCI.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCI.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureBCO.Pressure						= (uint16_t)		*(REG32 SENSOR_BPSO_BLOODPRESO);
			staticBufferPhysicalsensorData.PressureBCO.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCO.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCO.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.TemperatureInOut.TemperatureInlet_Value	= (uint16_t)		*(REG32 SENSOR_BTSI_TEMPERATURE);
			staticBufferPhysicalsensorData.TemperatureInOut.TemperatureOutlet_Value	= (uint16_t)		*(REG32 SENSOR_BTSO_TEMPERATURE);
			staticBufferPhysicalsensorData.TemperatureInOut.msOffset_Temperature	= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.TemperatureInOut.Status					= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.TemperatureInOut.msOffset_Status		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.CSENS1Data.Cond_FCR						= (uint16_t)		*(REG32 SENSOR_DCS_CONDUCT_FCR);
			staticBufferPhysicalsensorData.CSENS1Data.Cond_FCQ						= (uint16_t)		*(REG32 SENSOR_DCS_CONDUCT_FCQ);
			staticBufferPhysicalsensorData.CSENS1Data.Cond_PT1000					= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.CSENS1Data.msOffset_Cond				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.CSENS1Data.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.CSENS1Data.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			newStaticBufferPhysicalData = 0;
		} else if (newStaticBufferStatusData) {
			id = dataID_statusRTB;
			staticBufferDevicesStatusData.TimeStamp					= 		(tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferDevicesStatusData.statusECPI				= 		(uint32_t)			*(REG32 SENSOR_ECPISTAT);
			staticBufferDevicesStatusData.statusECPO				= 		(uint32_t)			*(REG32 SENSOR_ECPOSTAT);
			staticBufferDevicesStatusData.statusBPSI				= 		(uint32_t)			*(REG32 SENSOR_BPSISTAT);
			staticBufferDevicesStatusData.statusBPSO				= 		(uint32_t)			*(REG32 SENSOR_BPSOSTAT);
			staticBufferDevicesStatusData.statusFPSI				= 		(uint32_t)			*(REG32 SENSOR_FPSISTAT);
			staticBufferDevicesStatusData.statusFPSO				= 		(uint32_t)			*(REG32 SENSOR_FPSOSTAT);
			staticBufferDevicesStatusData.statusBTS					= 		(uint32_t)			*(REG32 SENSOR_BTSSTAT);
			staticBufferDevicesStatusData.statusDCS					= 		(uint32_t)			*(REG32 SENSOR_DCSSTAT);
			staticBufferDevicesStatusData.statusBLPUMP				= 		(uint32_t)			*(REG32 SENSOR_BLPUMPSTAT);
			staticBufferDevicesStatusData.statusFLPUMP				= 		(uint32_t)			*(REG32 SENSOR_FLPUMPSTAT);
			staticBufferDevicesStatusData.statusMFSI				= 		(uint32_t)			*(REG32 SENSOR_MFSISTAT);
			staticBufferDevicesStatusData.statusMFSO				= 		(uint32_t)			*(REG32 SENSOR_MFSOSTAT);
			staticBufferDevicesStatusData.statusMFSBL				= 		(uint32_t)			*(REG32 SENSOR_MFSBLSTAT);
			staticBufferDevicesStatusData.statusPOLAR				= 		(uint32_t)			*(REG32 SENSOR_POLARIZSTAT);
			staticBufferDevicesStatusData.statusRTMCB				=		(uint32_t)			*(REG32 SENSOR_RTMCBSTAT);
			newStaticBufferStatusData = 0;
		} else if (newStaticBufferActuatorData) {
			id = dataID_actuatorData;
			// Fluidic Switches
			staticBufferActuatorData.TimeStamp										= (tdUnixTimeType)  *(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferActuatorData.MultiSwitchBIData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBIData.Position						= (uint16_t)		*(REG32 SENSOR_MFSI_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBIData.msOffset_Position			= (tdMsTimeType)	5;
//			staticBufferActuatorData.MultiSwitchBIData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBIData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.MultiSwitchBOData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBOData.Position						= (uint16_t)		*(REG32 SENSOR_MFSU_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBOData.msOffset_Position			= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBOData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBOData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.MultiSwitchBUData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBUData.Position						= (uint16_t)		*(REG32 SENSOR_MFSO_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBUData.msOffset_Position			= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBUData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBUData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			// Pumps
			staticBufferActuatorData.BLPumpData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Direction						= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Direction			= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Speed							= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Speed				= (tdMsTimeType)	3;
			staticBufferActuatorData.BLPumpData.FlowReference					= (uint16_t) 		*(REG32 SENSOR_FLOWREF_BLOOD);
			staticBufferActuatorData.BLPumpData.msOffset_FlowReference		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Flow							= (uint16_t)		*(REG32 SENSOR_FLOW_BLOOD);
			staticBufferActuatorData.BLPumpData.msOffset_Flow				= (tdMsTimeType)	3;
			staticBufferActuatorData.BLPumpData.Current						= (uint16_t)		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Current				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.BLPumpData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.BLPumpData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.FLPumpData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.Direction						= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Direction			= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.Speed							= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Speed				= (tdMsTimeType)	3;
			staticBufferActuatorData.FLPumpData.FlowReference					= (uint16_t) 		*(REG32 SENSOR_FLOWREF_FLUID); 
			staticBufferActuatorData.FLPumpData.msOffset_FlowReference		= (tdMsTimeType)	0; // not implemented in Simulink Model
//Flow: should be the 'flow' value in simulink			
			staticBufferActuatorData.FLPumpData.Flow							= (uint16_t)		*(REG32 SENSOR_FLOW_FLUID);
			staticBufferActuatorData.FLPumpData.msOffset_Flow				= (tdMsTimeType)	3;
			staticBufferActuatorData.FLPumpData.Current						= (uint16_t)		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Current				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.FLPumpData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.FLPumpData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			// Polarizer Circuit
			staticBufferActuatorData.PolarizationData.SwitchONnOFF			= (uint8_t)  			*(REG32 SENSOR_POLARIZ_ONOFF);
			staticBufferActuatorData.PolarizationData.Direction				= (uint8_t)  			*(REG32 SENSOR_POLARIZ_DIRECT);
			staticBufferActuatorData.PolarizationData.msOffset_Direction		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.PolarizationData.VoltageReference		= (uint16_t)  			*(REG32 SENSOR_POLARIZ_VOLTAGEREF);
			staticBufferActuatorData.PolarizationData.msOffset_VoltageReference= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.PolarizationData.Voltage					= (uint16_t)  			*(REG32 SENSOR_POLARIZ_VOLTAGE);
			staticBufferActuatorData.PolarizationData.msOffset_Voltage		= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.PolarizationData.Status					= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.PolarizationData.msOffset_Status		= (tdMsTimeType)	0; // not implemented in Simulink Model
			newStaticBufferActuatorData = 0;
		} else if (newStaticBufferStatesRTB) {
			id = dataID_currentWAKDstateIs;
			// The current state is already in
			// extern tdWakdStates           staticBufferStateRTB;
			// extern tdWakdOperationalState staticBufferOperationalStateRTB;
			#if defined SEQ0 || defined SEQ4
				taskMessage("I", "decodeMsg()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
			#endif
			newStaticBufferStatesRTB = 0;
		}
		return(id);
	}
#endif

// The following implementation is just a dummy function to interface in the virtual platform
// with the Matlab Simulink interface (OFFIS SimLink). The "real" functionality will be coded by CSEM.
// This code will be attached to a serial interface to communication board, real time board, etc.
uint8_t dumpBuffToFile(uint8_t *uartBuffer, char* filename);
uint8_t dumpBuffToFile(uint8_t *uartBuffer, char* filename)
{
	uint8_t err = 0;
	#ifdef VP_SIMULATION
		FILE *pFile = fopen(filename, "a");
		if(pFile!=NULL) {
			fprintf(pFile,"*** Time: %lu ms: Start of Msg\n", xTaskGetTickCount());

			uint16_t msgSize, i;
			*(((uint8_t *)(&msgSize)))   = uartBuffer[2];	// swap bytes for uint16_t
			*(((uint8_t *)(&msgSize))+1) = uartBuffer[1];	// swap bytes for uint16_t
			for (i=0; i<msgSize; i++)
			{
				fprintf(pFile,"Byte %d: 0x%x", i, uartBuffer[i]);
				switch (i){
					case 0:{
						fprintf(pFile," (Recipient: %s)\n",whoIdToString(uartBuffer[i]));
						break;
					}
					case 1:{
						fprintf(pFile," (Most significant byte of msg-size)\n");
						break;
					}
					case 2:{
						fprintf(pFile," (Least significant byte of msg-size)\n");
						break;
					}
					case 3:{
						fprintf(pFile," (dataID: %s)\n", dataIdToString(uartBuffer[i]));
						break;
					}
					case 4:{
						fprintf(pFile," (msg-count: %d)\n", uartBuffer[i]);
						break;
					}
					case 5:{
						fprintf(pFile," (Sender: %s)\n",whoIdToString(uartBuffer[i]));
						break;
					}
					default:{
						fprintf(pFile,"\n");
						break;
					}
				}
			}
			fprintf(pFile,"*** End of Msg\n");
			fclose(pFile);
		} else {
			#if defined VP_SIMULATION
				printf("*** ERROR: Could not open file: uartCBout.dump!\n");
			#endif
			err = 1;
		}
	#endif
	return(err);
}

uint8_t getData(tdWhoId whoId, void *startData)
{
	switch (whoId) {
		case whoId_RTB: {
                #if defined VP_SIMULATION
			printf("\n********** ERROR in getData function! for RTB use function decodeMsg() instead! **************\n\n");
                #endif
			break;
		}
		case whoId_CB: {
			if (pDummyAckListCBPA != NULL)
			{	// Send an Ack on a previously send msg.
				tdMsgOnly * pMsgOnly = NULL;
				pMsgOnly = (tdMsgOnly *) startData;
				pMsgOnly->header.dataId      = pDummyAckListCBPA->msgAck.header.dataId;
				pMsgOnly->header.issuedBy    = pDummyAckListCBPA->msgAck.header.issuedBy;
				pMsgOnly->header.msgCount    = pDummyAckListCBPA->msgAck.header.msgCount;
				pMsgOnly->header.msgSize     = sizeof(tdMsgOnly);
				pMsgOnly->header.recipientId = pDummyAckListCBPA->msgAck.header.recipientId;
				// If we had a real communication partner who would have sent this Ack, we
				// would have found it in the input UART buffer. Well, instead of reading it from
				// there we put it there, since we had to generate it ourself for testing reasons.
				if(!(htonMsg(uartBufferCBin, startData)))
				{
					if(dumpBuffToFile(uartBufferCBin, "uartCBin.dump"))
					{
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() Could not dump UART buffer to file!\n");
						#endif
					}
				} else {
					#if defined VP_SIMULATION
						printf("**** ERROR: getData() call of ntohMsg failed!\n");
					#endif
				}

				// Remove first element from list of Acks
				tdDummyAckList *pDeleteThis = pDummyAckListCBPA;
				pDummyAckListCBPA = pDummyAckListCBPA->pNextInList;
				vPortFree(pDeleteThis);
			} else {
				if(!(ntohMsg(startData, uartBufferCBin)))
				{
					if(dumpBuffToFile(uartBufferCBin, "uartCBin.dump"))
					{
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() Could not dump UART buffer to file!\n");
						#endif
					}
				} else {
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() call of ntohMsg failed!\n");
						#endif
				}
				#ifndef VP_SIMULATION
					// Remove lock from uartBufferCBin to be able to receive new messages from CB
					lockUartBufferCBin = SIGNAL_OFF;
					CB_CLR_OUT_COM_BUFFERS();
				#endif
			}
			break;
		}
		default: {
			// unknown case! Error.
			#ifdef VP_SIMULATION
				printf("\n********** ERROR in getData function! (whoId not known!) **************\n\n");
			#endif
			return(1); // ERROR
			break;
		}
	}
	return(0);
}

uint8_t putData(void *startData)
{
	uint8_t err = 0;
	// Function to be implemented by CSEM. Will copy the data to serial interface of whoId and transmit.
	// Header information beginning at position "startData" gives target recipient as well as size of message.
	// Here we are doing nothing and just pretend it did work. Maybe a nice printf for debugging:
	tdMsgOnly *pMsgOnly = (tdMsgOnly *) startData;

//	printf("**** putData() is transmitting '");
//	printfDataId(pMsgOnly->header.dataId);
//	printf("' to '");
//	printfWhoId(pMsgOnly->header.recipientId);
//	printf("' from '");
//	printfWhoId(pMsgOnly->header.issuedBy);
//	printf("'.\n");

	if (pMsgOnly->header.recipientId == whoId_RTB)
	{	// This message needs to take the route via the real time board
		switch (pMsgOnly->header.dataId) {
			case dataID_configureState :{
				#if defined SEQ9
					printConfigureState("putData()", (tdMsgConfigureState *) (startData));
				#endif
				// This part of the RealTimeBoard is simulated in the Simulink model
				// Write configuration into simulink model.
				tdMsgConfigureState *msgConfigureState = (tdMsgConfigureState *) (startData);
				switch(msgConfigureState->stateToConfigure) {
					case wakdStates_AllStopped :{
						*(REG32 ACTUATOR_BP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ALLSTOPPED)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Maintenance :{
						*(REG32 ACTUATOR_BP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_NoDialysate :{
						*(REG32 ACTUATOR_BP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Dialysis :{
						*(REG32 ACTUATOR_BP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen1 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN1)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Ultrafiltration :{
						*(REG32 ACTUATOR_BP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ULTRAFILT)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen2 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN2)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					default :{
						taskMessage("E", "putData Function", "Unknown state cannot be configured!");
					}
				}
				break;
			}
			case dataID_statusRequest:{
				// Here we now have dummy functionality that pretends to be the RTB and generates the proper answer.
				// Pretend to be the ISR and inform system that new data is in RTB UART Buffer.
				newStaticBufferStatesRTB = 1;	// for decode() to know what data has just arrived.
				tdQtoken Qtoken;
				Qtoken.command = command_BufferFromRTBavailable;
				Qtoken.pData	= NULL;
				#if defined SEQ0
					taskMessage("I", "putData Function", "RTB sends answer current state: '%s'|'%s'", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
				#endif
				if ( xQueueSendToBack( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
				{	// Seemingly the queue is full and we already waited for a long time
					taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromRTBavailable'");
				}
				break;
			}
			case dataID_changeWAKDStateFromTo:{
				// The RTB will change into the new state without any condition.
				// RTB will answer this with its current status. Just as if it had received: dataID_statusRequest
				tdMsgWakdStateFromTo *pMsgWakdStateFromTo = (tdMsgWakdStateFromTo *) startData;
				staticBufferStateRTB = pMsgWakdStateFromTo->toWakdState;
				staticBufferOperationalStateRTB = pMsgWakdStateFromTo->toWakdOpState;
				// Communicate this state transition to the Simulink model
				*(REG32 ACTUATOR_WAKD_STATE) = pMsgWakdStateFromTo->toWakdState;
				*(REG32 ACTUATOR_WAKD_OPERATIONAL_STATE) = pMsgWakdStateFromTo->toWakdOpState;

				#ifdef VP_SIMULATION
					// For debugging tracing we protocol each change in state in log file
					FILE *pFile = fopen("stateTransition.dump", "a");
					if(pFile!=NULL) {
						fprintf(pFile,"*** Time: %lu ms: Changing into state ", xTaskGetTickCount());
						fprintf(pFile,"'%s'|'%s'.\n", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
						fclose(pFile);
					}
				#endif

				#if defined SEQ0 || defined SEQ4
					taskMessage("I", "putData()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
				#endif
				newStaticBufferStatesRTB = 1;	// for decode() to know what data has just arrived.
				tdQtoken Qtoken;
				Qtoken.command = command_BufferFromRTBavailable;
				Qtoken.pData	= NULL;
				if ( xQueueSendToBack( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
				{	// Seemingly the queue is full and we already waited for a long time
					taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromRTBavailable'");
				}
				break;
			}
		}
	} else if ((pMsgOnly->header.recipientId == whoId_CB)||(pMsgOnly->header.recipientId == whoId_SP))
	{	// This message needs to take the route via the Communication Board.
		if (!(htonMsg(uartBufferCBout, startData)))
		{
			// Writing the UART output to a file for trace debugging.
			if(dumpBuffToFile(uartBufferCBout, "uartCBout.dump"))
			{
                #if defined VP_SIMULATION
					printf("**** ERROR: putData() Could not dump UART buffer to file!\n");
				#endif
				err = 1;
			}

			#if defined VP_UartBtToPhone_ENABLED
				// Actually sending the bytes to the OVP UART link to the real phone
				// connected via serial bluetooth protocol. This is implemented via the
				// bus peripheral model "OFFIS_UartBtToPhone" at address 0x80020000
				// For the OFFIS setup, the real smartphone opens COM5 for communication
				// once the Nephron App has started.
//				if (uartBufferCBout[3]==dataID_currentWAKDstateIs)
//#warning All other messages have to be enabled as well!!
//				{	// Sending only this message to phone for testing. Everything else is not sent.
					int i;
					uint16_t msgSize = 0;
					*(((uint8_t *)(&msgSize)))   = uartBufferCBout[2];	// swap bytes for uint16_t
					*(((uint8_t *)(&msgSize))+1) = uartBufferCBout[1];	// swap bytes for uint16_t
					for (i=0;i<msgSize;i++)
					{
						*(REG32 UARTOUT) = uartBufferCBout[i];
					}
//				}
			#endif

			// the function host to network (hton) transfered the data to network protocol.
			// this changed the size of the message (mostly smaller). The size is now defined
			// in Byte 1 and Byte 2, but the byte order (most significant) is inverted to the host.
			// we have to fix that.
			#ifndef VP_SIMULATION
				uint16_t CB_DATALEN = ((uartBufferCBout[1]<<8) + uartBufferCBout[2]);
				#warning DEBUG CODE MUST BE REMOVED
				//DISABLED SLIP SHOULD BE ENABLED LATER AGAIN!!!
				// SLIP GDU
				//CB_DATALEN = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALEN, &SLIP_TX_CB[0]);
				//CB_DATALEN = CB_SendPacket (SLIP_TX_CB, CB_DATALEN);
				// NO SLIP GDU
				CB_DATALEN = CB_SendPacket (uartBufferCBout, CB_DATALEN);
				//CB_DATALEN = CB_SendPacket (uartBufferCBout, (uartBufferCBout[1]<<8+uartBufferCBout[2]));
			#endif
		} else {
                #if defined VP_SIMULATION
			printf("**** ERROR: putData() call of htonMsg failed!\n");
                        #endif
			err = 1;
		}

	//
	//	printf("Transfered data as HEX:\n");
	//	printf("Byte 00: %x\n", *((char *)(startData)+1));
	//	printf("Byte 01: %x\n", *((char *)(startData)+2));	// Manually change byte order!!
	//	printf("Byte 02: %x\n", *((char *)(startData)+1));	// Manually change byte order!!
	//	printf("Byte 03: %x\n", *((char *)(startData)+3));
	//	printf("Byte 04: %x\n", *((char *)(startData)+4));
	//	printf("Byte 05: %x\n", *((char *)(startData)+5));

		#ifdef VP_SIMULATION
//			// Since we do not have a real recipient of the msg who would ACK the msg, we just do this
//			// here in the put() for debugging reasons. So out dummy put() here will ACK the msg it just processed.
//			switch (pMsgOnly->header.dataId) {
//				case (dataID_reset):				// fall through (do the same thing)
//				case (dataID_weightRequest):		// fall through (do the same thing)
//				case (dataID_currentWAKDstateIs):	// fall through (do the same thing)
//				case (dataID_physiologicalData):	// fall through (do the same thing)
//				case (dataID_systemInfo):			// fall through (do the same thing)
//				case (dataID_weightData):{
//					// Put this  at end of list.
//					if (pDummyAckListCBPA == NULL)
//					{	// First and only element in list
//						pDummyAckListCBPA = (tdDummyAckList *)pvPortMalloc(sizeof(tdDummyAckList));
//						pDummyAckListCBPA->msgAck.header.dataId			= dataID_ack;
//						pDummyAckListCBPA->msgAck.header.issuedBy 		= pMsgOnly->header.recipientId;
//						pDummyAckListCBPA->msgAck.header.msgCount 		= pMsgOnly->header.msgCount;
//						pDummyAckListCBPA->msgAck.header.msgSize		= sizeof(tdMsgOnly);
//						pDummyAckListCBPA->msgAck.header.recipientId	= whoId_MB;
//						pDummyAckListCBPA->pNextInList = NULL;
//					} else {
//						// Search end of List
//						tdDummyAckList *pSearchIndex = pDummyAckListCBPA;
//						while (pSearchIndex->pNextInList != NULL)
//						{
//							pSearchIndex = pSearchIndex->pNextInList;
//						}
//						// Create new list entry at end
//						pSearchIndex->pNextInList = (tdDummyAckList *)pvPortMalloc(sizeof(tdDummyAckList));
//						// Step into the end element
//						pSearchIndex = pSearchIndex->pNextInList;
//						pSearchIndex->msgAck.header.dataId			= dataID_ack;
//						pSearchIndex->msgAck.header.issuedBy 		= pMsgOnly->header.recipientId;
//						pSearchIndex->msgAck.header.msgCount 		= pMsgOnly->header.msgCount;
//						pSearchIndex->msgAck.header.msgSize			= sizeof(tdMsgOnly);
//						pSearchIndex->msgAck.header.recipientId		= whoId_MB;
//						pSearchIndex->pNextInList = NULL;
//					}
//					// Inform system that CB has an ACK for us!
//					tdQtoken Qtoken;
//					Qtoken.command	= command_BufferFromCBavailable;
//					Qtoken.pData	= NULL;
//					if ( xQueueSendToBack( allHandles.allQueueHandles.qhCBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
//					{
//						// Seemingly the queue is full and we already waited for a long time
//						taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromCBavailable'");
//					}
//					break;
//				}
//			}
		#endif
	} else {
		// This message needs to go via real time board
		// Here CSEM needs to implement the functionality that bytecopies the message to RTB
		// In this dummy we just printf the data to see and than free the mem.

	}
	return(err);
}


