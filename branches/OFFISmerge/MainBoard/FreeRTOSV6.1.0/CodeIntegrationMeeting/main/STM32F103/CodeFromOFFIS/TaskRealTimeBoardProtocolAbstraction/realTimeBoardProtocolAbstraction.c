/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "main.h"
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\main\\main.h"
	#include "..\\nephron.h"
#endif
#include "realTimeBoardProtocolAbstraction.h"

#define PREFIX "OFFIS FreeRTOS task RTBPA"

#ifdef VP_SIMULATION
	// Dummy functionality for functions getData and putData. The dummies operate with the
	// simlink interface instead of a real serial interface buffer!
	#include "getData.h"
#endif

	/*-----------------------------------------------------------*/

// allocating static DATA Buffer for real time board
extern tdPhysiologicalData  	staticBufferPhysiologicalData;
extern tdDevicesStatus			staticBufferDevicesStatusData;
extern tdPhysicalsensorData 	staticBufferPhysicalsensorData;
extern tdActuatorData			staticBufferActuatorData;
extern tdWakdStates				staticBufferStateRTB;
extern tdWakdOperationalState	staticBufferOperationalStateRTB;

uint8_t heartBeatRTBPA = 0;

void tskRTBPA( void *pvParameters )
{
        taskMessage("I", PREFIX, "Starting Real Time Board Protocol Abstraction task!");

    xQueueHandle *qhDISPin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
    xQueueHandle *qhRTBPAin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhRTBPAin;

 	tdQtoken Qtoken;
	Qtoken.command = command_DefaultError;
	Qtoken.pData   = NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	for( ;; ) { 
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every RTBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatRTBPA++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		/*
		 * Watch Task's input queue for commands to come in.
		 * If nothing happens in queue, the task will wake up anyway
		 * after period to give note that it is unusually quiet!
		 */
		if ( xQueueReceive( qhRTBPAin, &Qtoken, RTBPA_PERIOD) == pdPASS )
		{	// We did find something in the queue of RTBPA
            #ifdef DEBUG_RTBPA
			    taskMessage("I", PREFIX, "command: %d in RTBPA queue!", Qtoken.command);
			#endif

			switch (Qtoken.command) {
				case command_UseAtachedMsgHeader: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_configureState):{
							#if defined DEBUG_RTBPA || defined SEQ9 || defined SEQ0
								taskMessage("I", PREFIX, "Received 'dataID_configureState' for state '%s'.", stateIdToString(((tdMsgConfigureState *) (Qtoken.pData))->stateToConfigure));
							#endif
							if (putData(Qtoken.pData)) {
								errMsg(errmsg_PutDataFunctionFailed);
							}
							sFree((void *)&Qtoken.pData);
							break;
						}
						case (dataID_reset):{
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Received 'dataID_reset'.");
							#endif
							// sending msg to real time board via UART
							uint8_t err = putData(Qtoken.pData);
							if (err) {
								errMsg(errmsg_PutDataFunctionFailed);
							}
							sFree((void *)&Qtoken.pData);
							// System is going down because of reset. Nothing more to do. Terminate this task
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Shutting down task.");
							#endif
							((tdAllHandles *) pvParameters)->allTaskHandles.handleRTBPA = NULL;
							vTaskDelete(NULL);	// Shut down and wait forever.
							vTaskSuspend(NULL); // Never run again (until after reset)
							break;
						}
						case (dataID_statusRequest):{
							#if defined DEBUG_RTBPA || defined SEQ0
								taskMessage("I", PREFIX, "Sending 'dataID_statusRequest' to RTB.");
							#endif
							if (putData(Qtoken.pData)) {
								errMsg(errmsg_PutDataFunctionFailed);
							}
							sFree((void *)&Qtoken.pData);
							break;
						}
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				case command_RtbChangeIntoStateRequest: {
					// Forward this message to RTBPA
					#if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
						taskMessage("I", PREFIX, "Sending 'command_RtbChangeIntoStateRequest' to RTB.");
					#endif
					if (putData(Qtoken.pData)) {
						errMsg(errmsg_PutDataFunctionFailed);
					}
					sFree((void *)&Qtoken.pData);
					break;
				}
				case command_BufferFromRTBavailable: {
					tdDataId dataId = decodeMsg();
					#if defined DEBUG_RTBPA
						taskMessage("I", PREFIX, "Decoded id: '%s'.", dataIdToString(dataId));
					#endif
					switch (dataId) {
						case dataID_physiologicalData: {
							// SEQ6: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgPhysiologicalData *pMsgPhysiologicalData = NULL;
							pMsgPhysiologicalData = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
							if ( pMsgPhysiologicalData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ6
								taskMessage("I", PREFIX, "Copy static buffer from UART: physiologicalData.");
							#endif
							memcpy(&(pMsgPhysiologicalData->physiologicalData), &staticBufferPhysiologicalData, sizeof(tdPhysiologicalData));
							pMsgPhysiologicalData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ6: http://is.gd/IJSmbK)
							// Ack is being skipped.
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA || defined SEQ6
								taskMessage("I", PREFIX, "Forwarding 'dataID_physiologicalData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgPhysiologicalData;
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physiologicalData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgPhysiologicalData);
							}
							break;
						}
						case dataID_physicalData: {
							// SEQ7: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgPhysicalsensorData *pMsgPhysicalsensorData = NULL;
							pMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
							if ( pMsgPhysicalsensorData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ7
								taskMessage("I", PREFIX, "Copy static buffer from UART: PhysicalsensorData.");
							#endif
							memcpy(&(pMsgPhysicalsensorData->physicalsensorData), &staticBufferPhysicalsensorData, sizeof(tdPhysicalsensorData));
							pMsgPhysicalsensorData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ7: http://is.gd/IJSmbK)
							// Ack is being skipped.
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA || defined SEQ7
								taskMessage("I", PREFIX, "Forwarding 'dataID_physicalData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgPhysicalsensorData;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physicalData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgPhysicalsensorData);
							}
							break;
						}
						case dataID_actuatorData: {
							// SEQ8: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgActuatorData *pMsgActuatorData = NULL;
							pMsgActuatorData = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
							if ( pMsgActuatorData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA || defined SEQ8
								taskMessage("I", PREFIX, "Copy static buffer from UART: ActuatorData.");
							#endif
							memcpy(&(pMsgActuatorData->actuatorData), &staticBufferActuatorData, sizeof(tdActuatorData));
							pMsgActuatorData->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA || defined SEQ8
								taskMessage("I", PREFIX, "Forwarding 'dataID_actuatorData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgActuatorData;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_actuatorData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgActuatorData);
							}
							break;
						}
						case dataID_statusRTB: {				
							// SEQ????: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgDeviceStatus *pMsgDevicesStatus = NULL;
							pMsgDevicesStatus = (tdMsgDeviceStatus *) pvPortMalloc(sizeof(tdMsgDeviceStatus)); // Will be freed by receiver task!
							if ( pMsgDevicesStatus == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Copy static buffer from UART: StatusData.");
							#endif
							memcpy(&(pMsgDevicesStatus->devicesStatus), &staticBufferDevicesStatusData, sizeof(tdDevicesStatus));
							pMsgDevicesStatus->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Forwarding 'dataID_statusData' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgDevicesStatus;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_statusData'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgDevicesStatus);
							}
							break;
						}
						case dataID_alarmRTB: {
							// SEQ????: http://is.gd/IJSmbK
				// ** STEP 1: Receive from static Buffer
							tdMsgAlarms *pMsgAlarms = NULL;
							pMsgAlarms = (tdMsgAlarms *) pvPortMalloc(sizeof(tdMsgAlarms)); // Will be freed by receiver task!
							if ( pMsgAlarms == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Copy static buffer from UART: AlarmData.");
							#endif
							memcpy(&(pMsgAlarms->alarms), &staticBufferAlarms, sizeof(tdAlarms));
							pMsgAlarms->header.dataId = dataId;
				// ** STEP  2: Ack Buffer
							// CSEM did not see the need to implement a sequence with Ack (compare with SEQ8: http://is.gd/IJSmbK)
							// Ack is being skipped.
				// ** STEGP 3: Send data to Dispatcher Task to forward to all the others.
							#if defined DEBUG_RTBPA
								taskMessage("I", PREFIX, "Forwarding 'dataID_alarmRTB' to Dispatcher Task.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgAlarms;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_alarmRTB'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgAlarms);
							}
							break;
						}
						case dataID_currentWAKDstateIs:{
							tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs = NULL;
							pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs));
							if ( pMsgCurrentWAKDstateIs == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							pMsgCurrentWAKDstateIs->header.dataId		= dataID_currentWAKDstateIs;
							pMsgCurrentWAKDstateIs->header.issuedBy		= whoId_RTB;
							pMsgCurrentWAKDstateIs->header.recipientId	= whoId_MB;
							pMsgCurrentWAKDstateIs->wakdStateIs 		= staticBufferStateRTB;
							pMsgCurrentWAKDstateIs->wakdOpStateIs		= staticBufferOperationalStateRTB;
							// send token to Dispatcher task
							#if defined DEBUG_RTBPA || defined SEQ0 || defined SEQ4
								taskMessage("I", PREFIX, "Forwarding 'dataID_currentWAKDstateIs' to Dispatcher Task: '%s'|'%s'.", stateIdToString(pMsgCurrentWAKDstateIs->wakdStateIs), opStateIdToString(pMsgCurrentWAKDstateIs->wakdOpStateIs));
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData	= (void *) pMsgCurrentWAKDstateIs;
							// Send token to DISP
							if ( xQueueSend( qhDISPin, &Qtoken, RTBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_currentWAKDstateIs'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgCurrentWAKDstateIs);
							}
							break;
						}
						default : {
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
		} else {
			// We now waited for "RTBPA_PERIOD" and nothing arrived in input queue.
			// But the RTBPA should be rather busy. Issue an error.
			taskMessage("W", PREFIX, "No message for very long time. This doesn't look good!!!!");
			// Make an entry in the protocol of data storage. This is an unusual event.
			// errMsg(errmsg_TimeoutOnRtbpaQueue);
		}
	}
}


//void setActuators(tdActCtrl *pActuators)
//{
//	setActuatorInSimulink (ACTUATOR_BLOODPUMP,    FIXPOINTSHIFT_BLOODPUMP,    pActuators->BLPumpCtrl.flowReference);
//	setActuatorInSimulink (ACTUATOR_FLUIDPUMP,    FIXPOINTSHIFT_FLUIDPUMP,    pActuators->FLPumpCtrl.flowReference);
//	setActuatorInSimulink (ACTUATOR_POLARIZATION, FIXPOINTSHIFT_POLARIZATION, pActuators->PolarizationCtrl.voltageReference);
//	// setActuatorInSimulink (ACTUATOR_MFSO,   	  FIXPOINTSHIFT_MFSO,	      pActuators->mfso);
//	// setActuatorInSimulink (ACTUATOR_MFSU, 	      FIXPOINTSHIFT_MFSU,		  pActuators->mfsu);
//	// setActuatorInSimulink (ACTUATOR_MFSI, 	      FIXPOINTSHIFT_MFSI,		  pActuators->mfsi);
//}

