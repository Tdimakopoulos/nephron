// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   math_tool01.h
//! \brief  math routines
//!
//! Some math conversion routines
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef MATH_TOOL01_H_
#define MATH_TOOL01_H_
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern uint8_t ascii_to_char(uint8_t _d0);
extern uint8_t asciix2_to_char(uint8_t _dh, uint8_t _dl);
extern uint8_t calculate_lrc(uint8_t *chain, int max_index);
extern uint8_t char_to_ascii(uint8_t _d0, int mode);
extern int8_t Search_Char(uint8_t* chain, int8_t max_index, uint8_t CHD);
extern uint8_t decword_to_charstr(uint16_t numbers, uint8_t *charstr, uint8_t charqty, uint16_t multiplier);
extern uint8_t decimal_to_char(uint8_t _charn);
extern uint8_t* COBS_Encode(uint8_t* txdata, uint16_t tx_len, uint8_t *COBS, uint16_t* cobs_len);
extern void COBS_Decode(uint8_t* DECOBS, uint16_t rx_len, uint8_t* rxdata, uint16_t* cobs_len);
extern uint16_t crc16_compute( uint8_t* data, uint16_t length );
extern int16_t PACKETS_CRC16_COBS (uint8_t* PACKETS, uint16_t PACKS_LEN, uint8_t* BufferXYZ, uint8_t* TxBufferX_COBS, uint16_t TXRX_RS422_XXX_MAXVALUE);
extern int16_t DECOBS_DECRC16_PACKETS(uint8_t *ENC_PACKETS, uint16_t ENC_PACKETS_LEN, uint8_t* BufferXYZ, uint8_t *DL_RxBuffer, uint16_t TXRX_RS422_XXX_MAXVALUE);
// -----------------------------------------------------------------------------------
#endif /* __MATH_TOOL01_H */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
