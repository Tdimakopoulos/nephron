// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   rtmcb_initialization.c
//! \brief  hardware initialization
//!
//! Routines for individual initialization devices and variables
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  MB_INIT
//!
//! MB Aplication Initialization Routine
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MB_HW_INIT(void)
{
  // System Clocks Configuration 
  MB_RCC_Configuration();
  
  //Enables the clock to Backup and power interface peripherals    
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BKP | RCC_APB1Periph_PWR,ENABLE);

  EXTI_DeInit();                                    //<-- OK if it is done before GPIO

  // General Purpose I/O Configuration 
  MB_GPIO_Config();
 
  // ADC by DMA
  // FEEDBACK_ADCDMAConfig();
  // FEEDBACK_ADCDMA_NVIC_Config();

  // USART1: USB-MAINTENANCE 
  RTMCB_USART1_Init();
  // USART2: WCM/CB
  RTMCB_USART2_Init();
  // USART3: PM1
  RTMCB_USART3_Init();
  // UART4: RTMCB
  RTMCB_USART4_Init();
  // UART5: PM2
  RTMCB_USART5_Init();

  // SPI1: SPI IF for SD (GPIO in MB_GPIO_Config)
  SD_SPIConfig();

  // SPI2: SPI IF for UIF & ACCELEROMETER
  UIF_ACC_SPIConfig();

  // RTC_NVIC Configuration 
  RTC_NVIC_Configuration();                         //<-- RTC

  // RTC Configuration
  RTC_Configuration();

  // Configure TIM1 update on overflow & Interrupt
  TIM1_Config();

  // Configure TIM2 update on overflow & Interrupt
  // TIM2_Config();

}
// -----------------------------------------------------------------------------------
//! \brief  CONFIGURE SYSTICK
//!
//! 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void CONFIGURE_SYSTICK(){
    // Configure SysTick Interrupt & start...
    DEVICE_SysTickConfig();
    SYSTICK_Start();
}
// -----------------------------------------------------------------------------------
//! \brief  ENABLE_UART1_RX_TX
//!
//! Enables Isolator, P.Supply & FTDI reset at SUPPLIES_SWITCH_ALL_ON
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void ENABLE_USART1_RX_TX(){
    // MAINTENANCEIF: PC_OUTEN (OUT PUSH-PULL)
    // Init State: LOW (OUT DISABLED)
    STM32F_GPIOOn(PC_OUTEN_GPIO_PORT, PC_OUTEN_GPIO_PIN);                 // HIGH (TX/RX enabled) [ALWAYS]      
}
// -----------------------------------------------------------------------------------
//! \brief  ENABLE_UART2_RX_TX
//!
//! 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void ENABLE_USART2_RX_TX(){
    // Init State: LOW (TX DISABLED)
    STM32F_GPIOOn(WCMuC_CTRL_TX_GPIO_PORT, WCMuC_CTRL_TX_GPIO_PIN);     // HIGH (TX enabled) [ALWAYS]      
    // Init State: HIGH (RX DISABLED)
    STM32F_GPIOOff(WCMuC_CTRL_RX_GPIO_PORT, WCMuC_CTRL_RX_GPIO_PIN);    // LOW (RX enabled) [ALWAYS]      
}
// -----------------------------------------------------------------------------------
//! \brief  ENABLE_UART3_RX_TX
//!
//! 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void ENABLE_USART3_RX_TX(){
    // Init State: LOW (TX DISABLED)
    STM32F_GPIOOn(PM1uC_CTRL_TX_GPIO_PORT, PM1uC_CTRL_TX_GPIO_PIN);     // HIGH (TX enabled) [ALWAYS]      
    // Init State: HIGH (RX DISABLED)
    STM32F_GPIOOff(PM1uC_CTRL_RX_GPIO_PORT, PM1uC_CTRL_RX_GPIO_PIN);    // LOW (RX enabled) [ALWAYS]      

}
// -----------------------------------------------------------------------------------
//! \brief  ENABLE_UART4_RX_TX
//!
//! 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void ENABLE_UART4_RX_TX(){
    STM32F_GPIOOn(RTMCBuC_CTRL_TX_GPIO_PORT, RTMCBuC_CTRL_TX_GPIO_PIN);  // HIGH (RX enabled) [ALWAYS]      
    STM32F_GPIOOff(RTMCBuC_CTRL_RX_GPIO_PORT, RTMCBuC_CTRL_RX_GPIO_PIN);  // LOW (RX enabled) [ALWAYS]          
}
// -----------------------------------------------------------------------------------
//! \brief  ENABLE_UART5_RX_TX
//!
//! 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void ENABLE_UART5_RX_TX(){
    // Init State: LOW (TX DISABLED)
    STM32F_GPIOOn(PM2uC_CTRL_TX_GPIO_PORT, PM2uC_CTRL_TX_GPIO_PIN);     // HIGH (TX enabled) [ALWAYS]      
    // Init State: HIGH (RX DISABLED)
    STM32F_GPIOOff(PM2uC_CTRL_RX_GPIO_PORT, PM2uC_CTRL_RX_GPIO_PIN);    // LOW (RX enabled) [ALWAYS]      

}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_ReturnFromStopMode
//!
//! This function is executed after wakeup from STOP mode
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MB_ReturnFromStopMode(void)
{
  // RCC Configuration has to be called after waking from STOP Mode
  MB_RCC_Configuration();
  // Enables the clock to Backup and power interface peripherals after Wake Up 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BKP | RCC_APB1Periph_PWR,ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  MB_GPIO_Config
//!
//! Configuration of GPIO, Initialize and set outputs in RESET STATE
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MB_GPIO_Config(void){
    
    // ----------------------------------------------------------------
    // Configure GPIO
    // ----------------------------------------------------------------
    // PORT A
    // ----------------------------------------------------------------
    // PA3 ... PA0
    STM32F_GPIOInit(WCMuC_RTS_GPIO_PORT, WCMuC_RTS_GPIO_CLK, WCMuC_RTS_GPIO_PIN, WCMuC_RTS_GPIO_SPEED, WCMuC_RTS_GPIO_MODE);
    STM32F_GPIOInit(WCMuC_CTS_GPIO_PORT, WCMuC_CTS_GPIO_CLK, WCMuC_CTS_GPIO_PIN, WCMuC_CTS_GPIO_SPEED, WCMuC_CTS_GPIO_MODE);   
    STM32F_GPIOInit(WCMuC_RX_GPIO_PORT, WCMuC_RX_GPIO_CLK, WCMuC_RX_GPIO_PIN, WCMuC_RX_GPIO_SPEED, WCMuC_RX_GPIO_MODE);       
    STM32F_GPIOInit(WCMuC_TX_GPIO_PORT, WCMuC_TX_GPIO_CLK, WCMuC_TX_GPIO_PIN, WCMuC_TX_GPIO_SPEED, WCMuC_TX_GPIO_MODE);   
    // PA7 ... PA4
    STM32F_GPIOInit(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_CLK, SD_SPI_nCS_GPIO_PIN, SD_SPI_nCS_GPIO_SPEED, SD_SPI_nCS_GPIO_MODE);   
    STM32F_GPIOInit(SD_SPI_CLK_GPIO_PORT, SD_SPI_CLK_GPIO_CLK, SD_SPI_CLK_GPIO_PIN, SD_SPI_CLK_GPIO_SPEED, SD_SPI_CLK_GPIO_MODE);   
    STM32F_GPIOInit(SD_SPI_MISO_GPIO_PORT, SD_SPI_MISO_GPIO_CLK, SD_SPI_MISO_GPIO_PIN, SD_SPI_MISO_GPIO_SPEED, SD_SPI_MISO_GPIO_MODE);   
    STM32F_GPIOInit(SD_SPI_MOSI_GPIO_PORT, SD_SPI_MOSI_GPIO_CLK, SD_SPI_MOSI_GPIO_PIN, SD_SPI_MOSI_GPIO_SPEED, SD_SPI_MOSI_GPIO_MODE);   
    // PA11 ... PA8   
    STM32F_GPIOInit(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_CLK, DEBUG_LED_GPIO_PIN, DEBUG_LED_GPIO_SPEED, DEBUG_LED_GPIO_MODE);     
    STM32F_GPIOInit(PC_RX_GPIO_PORT, PC_RX_GPIO_CLK, PC_RX_GPIO_PIN, PC_RX_GPIO_SPEED, PC_RX_GPIO_MODE);   
    STM32F_GPIOInit(PC_TX_GPIO_PORT, PC_TX_GPIO_CLK, PC_TX_GPIO_PIN, PC_TX_GPIO_SPEED, PC_TX_GPIO_MODE);    
    STM32F_GPIOInit(FTDI_NRESET_GPIO_PORT, FTDI_NRESET_GPIO_CLK, FTDI_NRESET_GPIO_PIN, FTDI_NRESET_GPIO_SPEED, FTDI_NRESET_GPIO_MODE);   
    // PA15 ... PA12
    STM32F_GPIOInit(PC_USBVBUS_GPIO_PORT, PC_USBVBUS_GPIO_CLK, PC_USBVBUS_GPIO_PIN, PC_USBVBUS_GPIO_SPEED, PC_USBVBUS_GPIO_MODE);   
    // ----------------------------------------------------------------
    // PORT B
    // ----------------------------------------------------------------
    // PB3 ... PB0
    STM32F_GPIOInit(RTMCBuC_CTRL_RX_GPIO_PORT, RTMCBuC_CTRL_RX_GPIO_CLK, RTMCBuC_CTRL_RX_GPIO_PIN, RTMCBuC_CTRL_RX_GPIO_SPEED, RTMCBuC_CTRL_RX_GPIO_MODE);   
    STM32F_GPIOInit(RTMCBuC_CTRL_TX_GPIO_PORT, RTMCBuC_CTRL_TX_GPIO_CLK, RTMCBuC_CTRL_TX_GPIO_PIN, RTMCBuC_CTRL_TX_GPIO_SPEED, RTMCBuC_CTRL_TX_GPIO_MODE);       
    STM32F_GPIOInit(PB2_GPIO_PORT, PB2_GPIO_CLK, PB2_GPIO_PIN, PB2_GPIO_SPEED, PB2_GPIO_MODE);   
    // PB7 ... PB4
    STM32F_GPIOInit(PB5_GPIO_PORT, PB5_GPIO_CLK, PB5_GPIO_PIN, PB5_GPIO_SPEED, PB5_GPIO_MODE);   
    STM32F_GPIOInit(PUSHBUTTON_nINT_GPIO_PORT, PUSHBUTTON_nINT_GPIO_CLK, PUSHBUTTON_nINT_GPIO_PIN, PUSHBUTTON_nINT_GPIO_SPEED, PUSHBUTTON_nINT_GPIO_MODE);       
    STM32F_GPIOInit(DEBOUNCED_nMULTI_GPIO_PORT, DEBOUNCED_nMULTI_GPIO_CLK, DEBOUNCED_nMULTI_GPIO_PIN, DEBOUNCED_nMULTI_GPIO_SPEED, DEBOUNCED_nMULTI_GPIO_MODE);   
    // PB11 ... PB8
    STM32F_GPIOInit(RTMCBuC_nRST_GPIO_PORT, RTMCBuC_nRST_GPIO_CLK, RTMCBuC_nRST_GPIO_PIN, RTMCBuC_nRST_GPIO_SPEED, RTMCBuC_nRST_GPIO_MODE);   
    STM32F_GPIOInit(RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_CLK, RTMCBuC_SYNC_GPIO_PIN, RTMCBuC_SYNC_GPIO_SPEED, RTMCBuC_SYNC_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_RX_GPIO_PORT, PM1uC_RX_GPIO_CLK, PM1uC_RX_GPIO_PIN, PM1uC_RX_GPIO_SPEED, PM1uC_RX_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_TX_GPIO_PORT, PM1uC_TX_GPIO_CLK, PM1uC_TX_GPIO_PIN, PM1uC_TX_GPIO_SPEED, PM1uC_TX_GPIO_MODE);   
    // PB15 ... PB12
    STM32F_GPIOInit(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_CLK, MSPARM_STE_GPIO_PIN, MSPARM_STE_GPIO_SPEED, MSPARM_STE_GPIO_MODE);   
    STM32F_GPIOInit(SPI2_CLK_GPIO_PORT, SPI2_CLK_GPIO_CLK, SPI2_CLK_GPIO_PIN, SPI2_CLK_GPIO_SPEED, SPI2_CLK_GPIO_MODE);   
    STM32F_GPIOInit(SPI2_MISO_GPIO_PORT, SPI2_MISO_GPIO_CLK, SPI2_MISO_GPIO_PIN, SPI2_MISO_GPIO_SPEED, SPI2_MISO_GPIO_MODE);   
    STM32F_GPIOInit(SPI2_MOSI_GPIO_PORT, SPI2_MOSI_GPIO_CLK, SPI2_MOSI_GPIO_PIN, SPI2_MOSI_GPIO_SPEED, SPI2_MOSI_GPIO_MODE);   
    // ----------------------------------------------------------------
    // PORT C
    // ----------------------------------------------------------------
    // PC3 ... PC0
    STM32F_GPIOInit(RBAT_CHARGE_GPIO_PORT, RBAT_CHARGE_GPIO_CLK, RBAT_CHARGE_GPIO_PIN, RBAT_CHARGE_GPIO_SPEED, RBAT_CHARGE_GPIO_MODE);   
    STM32F_GPIOInit(BPACK_VOLTAGE_GPIO_PORT, BPACK_VOLTAGE_GPIO_CLK, BPACK_VOLTAGE_GPIO_PIN, BPACK_VOLTAGE_GPIO_SPEED, BPACK_VOLTAGE_GPIO_MODE);   
    STM32F_GPIOInit(PM2uC_PRESENT_GPIO_PORT, PM2uC_PRESENT_GPIO_CLK, PM2uC_PRESENT_GPIO_PIN, PM2uC_PRESENT_GPIO_SPEED, PM2uC_PRESENT_GPIO_MODE);   
    // PC3: EXTI
    STM32F_GPIOInit(PM2uC_nIRQ_GPIO_PORT, PM2uC_nIRQ_GPIO_CLK, PM2uC_nIRQ_GPIO_PIN, PM2uC_nIRQ_GPIO_SPEED, PM2uC_nIRQ_GPIO_MODE);   
    // PC7 ... PC4
    // PC4: (SD_DETECT) EXTI4, INTERRUPT (edge(+), edge(-), (EXTI4_IRQHandler) Priority 11
    STM32F_GPIOInit(SD_DETECT_GPIO_PORT, SD_DETECT_GPIO_CLK, SD_DETECT_GPIO_PIN, SD_DETECT_GPIO_SPEED, SD_DETECT_GPIO_MODE);   
    STM32F_GPIOInit(SD_PWR_EN_GPIO_PORT, SD_PWR_EN_GPIO_CLK, SD_PWR_EN_GPIO_PIN, SD_PWR_EN_GPIO_SPEED, SD_PWR_EN_GPIO_MODE);   
    STM32F_GPIOInit(PC6_GPIO_PORT, PC6_GPIO_CLK, PC6_GPIO_PIN, PC6_GPIO_SPEED, PC6_GPIO_MODE);   
    STM32F_GPIOInit(PC7_GPIO_PORT, PC7_GPIO_CLK, PC7_GPIO_PIN, PC7_GPIO_SPEED, PC7_GPIO_MODE);   
    // PC11 ... PC8
    STM32F_GPIOInit(PC8_GPIO_PORT, PC8_GPIO_CLK, PC8_GPIO_PIN, PC8_GPIO_SPEED, PC8_GPIO_MODE);   
    STM32F_GPIOInit(PC9_GPIO_PORT, PC9_GPIO_CLK, PC9_GPIO_PIN, PC9_GPIO_SPEED, PC9_GPIO_MODE);   
    STM32F_GPIOInit(RTMCBuC_RX_GPIO_PORT, RTMCBuC_RX_GPIO_CLK, RTMCBuC_RX_GPIO_PIN, RTMCBuC_RX_GPIO_SPEED, RTMCBuC_RX_GPIO_MODE);   
    STM32F_GPIOInit(RTMCBuC_TX_GPIO_PORT, RTMCBuC_TX_GPIO_CLK, RTMCBuC_TX_GPIO_PIN, RTMCBuC_TX_GPIO_SPEED, RTMCBuC_TX_GPIO_MODE);   
    // PC15 ... PC12
    STM32F_GPIOInit(PM2uC_RX_GPIO_PORT, PM2uC_RX_GPIO_CLK, PM2uC_RX_GPIO_PIN, PM2uC_RX_GPIO_SPEED, PM2uC_RX_GPIO_MODE);   
    STM32F_GPIOInit(PC13_GPIO_PORT, PC13_GPIO_CLK, PC13_GPIO_PIN, PC13_GPIO_SPEED, PC13_GPIO_MODE);   
    // ----------------------------------------------------------------
    // PORT D
    // ----------------------------------------------------------------
    // PD3 ... PD0
    STM32F_GPIOInit(WCMuC_CTRL_TX_GPIO_PORT, WCMuC_CTRL_TX_GPIO_CLK, WCMuC_CTRL_TX_GPIO_PIN, WCMuC_CTRL_TX_GPIO_SPEED, WCMuC_CTRL_TX_GPIO_MODE); 
    STM32F_GPIOInit(WCMuC_CTRL_RX_GPIO_PORT, WCMuC_CTRL_RX_GPIO_CLK, WCMuC_CTRL_RX_GPIO_PIN, WCMuC_CTRL_RX_GPIO_SPEED, WCMuC_CTRL_RX_GPIO_MODE);  
    STM32F_GPIOInit(PM2uC_TX_GPIO_PORT, PM2uC_TX_GPIO_CLK, PM2uC_TX_GPIO_PIN, PM2uC_TX_GPIO_SPEED, PM2uC_TX_GPIO_MODE);   
    STM32F_GPIOInit(RTMCBuC_PRESENT_GPIO_PORT, RTMCBuC_PRESENT_GPIO_CLK, RTMCBuC_PRESENT_GPIO_PIN, RTMCBuC_PRESENT_GPIO_SPEED, RTMCBuC_PRESENT_GPIO_MODE);   
    // PD7 ... PD4
    // PD4: EXTI
    STM32F_GPIOInit(RTMCBuC_nIRQ_GPIO_PORT, RTMCBuC_nIRQ_GPIO_CLK, RTMCBuC_nIRQ_GPIO_PIN, RTMCBuC_nIRQ_GPIO_SPEED, RTMCBuC_nIRQ_GPIO_MODE);   
    STM32F_GPIOInit(PD5_GPIO_PORT, PD5_GPIO_CLK, PD5_GPIO_PIN, PD5_GPIO_SPEED, PD5_GPIO_MODE);   
    STM32F_GPIOInit(PD6_GPIO_PORT, PD6_GPIO_CLK, PD6_GPIO_PIN, PD6_GPIO_SPEED, PD6_GPIO_MODE);   
    STM32F_GPIOInit(PD7_GPIO_PORT, PD7_GPIO_CLK, PD7_GPIO_PIN, PD7_GPIO_SPEED, PD7_GPIO_MODE);
    // PD11 ... PD8   
    STM32F_GPIOInit(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_CLK, ACC_SPI_nCS_GPIO_PIN, ACC_SPI_nCS_GPIO_SPEED, ACC_SPI_nCS_GPIO_MODE);   
    // PD9: EXTI
    STM32F_GPIOInit(ACC_IRQ_GPIO_PORT, ACC_IRQ_GPIO_CLK, ACC_IRQ_GPIO_PIN, ACC_IRQ_GPIO_SPEED, ACC_IRQ_GPIO_MODE);   
    STM32F_GPIOInit(PD10_GPIO_PORT, PD10_GPIO_CLK, PD10_GPIO_PIN, PD10_GPIO_SPEED, PD10_GPIO_MODE);   
    STM32F_GPIOInit(PD11_GPIO_PORT, PD11_GPIO_CLK, PD11_GPIO_PIN, PD11_GPIO_SPEED, PD11_GPIO_MODE);   
    // PD15 ... PD12
    STM32F_GPIOInit(PD12_GPIO_PORT, PD12_GPIO_CLK, PD12_GPIO_PIN, PD12_GPIO_SPEED, PD12_GPIO_MODE);   
    STM32F_GPIOInit(PD13_GPIO_PORT, PD13_GPIO_CLK, PD13_GPIO_PIN, PD13_GPIO_SPEED, PD13_GPIO_MODE);   
    STM32F_GPIOInit(PD14_GPIO_PORT, PD14_GPIO_CLK, PD14_GPIO_PIN, PD14_GPIO_SPEED, PD14_GPIO_MODE);   
    STM32F_GPIOInit(PD15_GPIO_PORT, PD15_GPIO_CLK, PD15_GPIO_PIN, PD15_GPIO_SPEED, PD15_GPIO_MODE);   
    // ----------------------------------------------------------------
    // PORT E
    // ----------------------------------------------------------------
    // PE3 ... PE0
    STM32F_GPIOInit(PC_OUTEN_GPIO_PORT, PC_OUTEN_GPIO_CLK, PC_OUTEN_GPIO_PIN, PC_OUTEN_GPIO_SPEED, PC_OUTEN_GPIO_MODE);   
    STM32F_GPIOInit(USB_PWR_EN_GPIO_PORT, USB_PWR_EN_GPIO_CLK, USB_PWR_EN_GPIO_PIN, USB_PWR_EN_GPIO_SPEED, USB_PWR_EN_GPIO_MODE);   
    STM32F_GPIOInit(PE2_GPIO_PORT, PE2_GPIO_CLK, PE2_GPIO_PIN, PE2_GPIO_SPEED, PE2_GPIO_MODE);   
    STM32F_GPIOInit(PM2uC_CTRL_RX_GPIO_PORT, PM2uC_CTRL_RX_GPIO_CLK, PM2uC_CTRL_RX_GPIO_PIN, PM2uC_CTRL_RX_GPIO_SPEED, PM2uC_CTRL_RX_GPIO_MODE);   
    // PE7 ... PE4
    STM32F_GPIOInit(PM2uC_CTRL_TX_GPIO_PORT, PM2uC_CTRL_TX_GPIO_CLK, PM2uC_CTRL_TX_GPIO_PIN, PM2uC_CTRL_TX_GPIO_SPEED, PM2uC_CTRL_TX_GPIO_MODE);   
    STM32F_GPIOInit(MB2uC_PRESENT_GPIO_PORT, MB2uC_PRESENT_GPIO_CLK, MB2uC_PRESENT_GPIO_PIN, MB2uC_PRESENT_GPIO_SPEED, MB2uC_PRESENT_GPIO_MODE);   
    STM32F_GPIOInit(PM2uC_SYNC_GPIO_PORT, PM2uC_SYNC_GPIO_CLK, PM2uC_SYNC_GPIO_PIN, PM2uC_SYNC_GPIO_SPEED, PM2uC_SYNC_GPIO_MODE);   
    STM32F_GPIOInit(POWER_GOOD_1_GPIO_PORT, POWER_GOOD_1_GPIO_CLK, POWER_GOOD_1_GPIO_PIN, POWER_GOOD_1_GPIO_SPEED, POWER_GOOD_1_GPIO_MODE);   
    // PE11 ... PE8
    STM32F_GPIOInit(POWER_GOOD_2_GPIO_PORT, POWER_GOOD_2_GPIO_CLK, POWER_GOOD_2_GPIO_PIN, POWER_GOOD_2_GPIO_SPEED, POWER_GOOD_2_GPIO_MODE);   
    STM32F_GPIOInit(WCM_PWR_EN_GPIO_PORT, WCM_PWR_EN_GPIO_CLK, WCM_PWR_EN_GPIO_PIN, WCM_PWR_EN_GPIO_SPEED, WCM_PWR_EN_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_CTRL_TX_GPIO_PORT, PM1uC_CTRL_TX_GPIO_CLK, PM1uC_CTRL_TX_GPIO_PIN, PM1uC_CTRL_TX_GPIO_SPEED, PM1uC_CTRL_TX_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_CTRL_RX_GPIO_PORT, PM1uC_CTRL_RX_GPIO_CLK, PM1uC_CTRL_RX_GPIO_PIN, PM1uC_CTRL_RX_GPIO_SPEED, PM1uC_CTRL_RX_GPIO_MODE);   
    // PE15 ... PE12
    STM32F_GPIOInit(MB1uC_PRESENT_GPIO_PORT, MB1uC_PRESENT_GPIO_CLK, MB1uC_PRESENT_GPIO_PIN, MB1uC_PRESENT_GPIO_SPEED, MB1uC_PRESENT_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_SYNC_GPIO_PORT, PM1uC_SYNC_GPIO_CLK, PM1uC_SYNC_GPIO_PIN, PM1uC_SYNC_GPIO_SPEED, PM1uC_SYNC_GPIO_MODE);   
    STM32F_GPIOInit(PM1uC_PRESENT_GPIO_PORT, PM1uC_PRESENT_GPIO_CLK, PM1uC_PRESENT_GPIO_PIN, PM1uC_PRESENT_GPIO_SPEED, PM1uC_PRESENT_GPIO_MODE);   
    // PE15: EXTI
    STM32F_GPIOInit(PM1uC_nIRQ_GPIO_PORT, PM1uC_nIRQ_GPIO_CLK, PM1uC_nIRQ_GPIO_PIN, PM1uC_nIRQ_GPIO_SPEED, PM1uC_nIRQ_GPIO_MODE);   
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // SET I/Os HIGH or LOW according to RESET STATE
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // SDCARD: SD_SPI_nCS (OUTPUT PUSH-PULL)
    // Init State: HIGH (SDCARD SPI disabled)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);        
    // ----------------------------------------------------------------
    // DEBUGGING LED (OUTPUT PUSH-PULL)
    // Init State: HIGH (LED OFF, before finishing initialization)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
    // ----------------------------------------------------------------
    // MAINTENANCEIF: FTDI_nRESET (OUT PUSH-PULL)
    // Init State: HIGH (NO RESET)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(FTDI_NRESET_GPIO_PORT, FTDI_NRESET_GPIO_PIN);        
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // RTMCBIF: RTMCBuC_CTRL_RX (OUT PUSH-PULL)
    // Init State: HIGH (RX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(RTMCBuC_CTRL_RX_GPIO_PORT, RTMCBuC_CTRL_RX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // RTMCBIF: RTMCBuC_CTRL_TX (OUT PUSH-PULL)
    // Init State: LOW (TX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(RTMCBuC_CTRL_TX_GPIO_PORT, RTMCBuC_CTRL_TX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PB2 [BOOT1] (OUT PUSH-PULL)
    // Init State: HIGH (arbitrary)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(PB2_GPIO_PORT, PB2_GPIO_PIN);        
    // ----------------------------------------------------------------
    // RTMCBIF: RTMCBuC_nRST (OUT PUSH-PULL)
    // Init State: HIGH (NOT RESET)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(RTMCBuC_nRST_GPIO_PORT, RTMCBuC_nRST_GPIO_PIN);        
    // ----------------------------------------------------------------
    // RTMCBIF: RTMCBuC_SYNC (OUT PUSH-PULL)
    // Init State: lOW ()
    // ----------------------------------------------------------------
    STM32F_GPIOOff(RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_PIN);        
    // ----------------------------------------------------------------
    // UIF: MSPARM_STE
    // Init State: HIGH (MSP SPI NOT SELECTED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);        
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // SDCARD: SD_PWR_EN (OUT PUSH_PULL)
    // Init State: LOW (OFF)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(SD_PWR_EN_GPIO_PORT, SD_PWR_EN_GPIO_PIN);        
    // ----------------------------------------------------------------
    // ----------------------------------------------------------------
    // CB: WCMuC_CTRL_TX (OUT PUSH-PULL)
    // Init State: LOW (TX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(WCMuC_CTRL_TX_GPIO_PORT, WCMuC_CTRL_TX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // CB: WCMuC_CTRL_RX (OUT PUSH-PULL)
    // Init State: HIGH (RX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(WCMuC_CTRL_RX_GPIO_PORT, WCMuC_CTRL_RX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // ACCELEROMETER: ACC_SPI_nCS (OUT PUSH-PULL)
    // Init State: HIGH (NOT SELECTED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);        
    // ----------------------------------------------------------------
    // MAINTENANCEIF: PC_OUTEN (OUT PUSH-PULL)
    // Init State: LOW (OUT DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(PC_OUTEN_GPIO_PORT, PC_OUTEN_GPIO_PIN);        
    // ----------------------------------------------------------------
    // USB_PWR_EN (OUT PUSH-PULL)
    // Init State: LOW (OFF)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(USB_PWR_EN_GPIO_PORT, USB_PWR_EN_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM2: PM2uC_CTRL_RX (OUT PUSH-PULL)
    // Init State: HIGH (RX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(PM2uC_CTRL_RX_GPIO_PORT, PM2uC_CTRL_RX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM2: PM2uC_CTRL_TX (OUT PUSH-PULL)
    // Init State: LOW (TX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(PM2uC_CTRL_TX_GPIO_PORT, PM2uC_CTRL_TX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM2: MB2uC_PRESENT (OUT PUSH-PULL)
    // Init State: HIGH (MB PRESENT)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(MB2uC_PRESENT_GPIO_PORT, MB2uC_PRESENT_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM2: PM2uC_SYNC (OUT PUSH-PULL)
    // Init State: LOW ()
    // ----------------------------------------------------------------
    STM32F_GPIOOff(PM2uC_SYNC_GPIO_PORT, PM2uC_SYNC_GPIO_PIN);        
    // ----------------------------------------------------------------
    // AB&C: WCM_PWR_EN (OUT PUSH-PULL)
    // Init State: LOW (OFF)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(WCM_PWR_EN_GPIO_PORT, WCM_PWR_EN_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM1: PM1uC_CTRL_TX (OUT PUSH-PULL)
    // Init State: LOW (TX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(PM1uC_CTRL_TX_GPIO_PORT, PM1uC_CTRL_TX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM1: PM1uC_CTRL_RX (OUT PUSH-PULL)
    // Init State: HIGH (RX DISABLED)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(PM1uC_CTRL_RX_GPIO_PORT, PM1uC_CTRL_RX_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM1: MB1uC_PRESENT (OUT PUSH-PULL)
    // Init State: HIGH (MB PRESENT)
    // ----------------------------------------------------------------
    STM32F_GPIOOn(MB1uC_PRESENT_GPIO_PORT, MB1uC_PRESENT_GPIO_PIN);        
    // ----------------------------------------------------------------
    // PM1: PM1uC_SYNC (OUT PUSH-PULL)
    // Init State: LOW (VRS485 disabled)
    // ----------------------------------------------------------------
    STM32F_GPIOOff(PM1uC_SYNC_GPIO_PORT, PM1uC_SYNC_GPIO_PIN);        
    // ----------------------------------------------------------------
}

// -----------------------------------------------------------------------------------
//! \brief  MB_EXTI_Config
//!
//!  MB Configures the external input interrupts
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MB_EXTI_Config(void){
#if 0    
    // ----------------------------------------------------------------
    // SLEEPWKUP: PUSHBUTTON_nINT (INPUT PULL_DOWN)
    // EXT INTERRUPT, LINKS TO PB6, EXTI6, BOTH EDGES
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource6); 
    EXTIx_IRQ_Config(PUSHBUTTON_nINT_GPIO_IRQ, PreemptivePriority_PUSHBUTTON_nINT_EXTI1, SubPriority_PUSHBUTTON_nINT_EXTI1);
    NVIC_IO_EXTI(EXTI_Line6,EXTI_Trigger_Rising_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line6);
    // ----------------------------------------------------------------
    // SLEEPWKUP: DEBOUNCED_nMULTI (INPUT PULL_UP)
    // EXT INTERRUPT, LINKS TO PB7, EXTI7, BOTH EDGES
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource7); 
    EXTIx_IRQ_Config(DEBOUNCED_nMULTI_GPIO_IRQ, PreemptivePriority_DEBOUNCED_nMULTI_EXTI7, SubPriority_DEBOUNCED_nMULTI_EXTI7);
    NVIC_IO_EXTI(EXTI_Line7,EXTI_Trigger_Rising_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line7);
    // ----------------------------------------------------------------
#if 0
    // ----------------------------------------------------------------
    // PM2: PM2uC_nIRQ (INPUT PULL_UP) 
    // EXT INTERRUPT, LINKS TO PC3, EXTI3, FALLING EDGE
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource3); 
    EXTIx_IRQ_Config(PM2uC_nIRQ_GPIO_IRQ, PreemptivePriority_PM2uC_nIRQ_EXTI3, SubPriority_PM2uC_nIRQ_EXTI3);
    NVIC_IO_EXTI(EXTI_Line3,EXTI_Trigger_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line3);
    // ----------------------------------------------------------------
#endif
    // ----------------------------------------------------------------
    // SDCARD: SD_DETECT (INPUT PULL_DOWN)
    // EXT INTERRUPT, LINKS TO PC4, BOTH EDGES
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource4); 
    EXTIx_IRQ_Config(SD_DETECT_GPIO_IRQ, PreemptivePriority_SD_DETECT_EXT4, SubPriority_SD_DETECT_EXT4);
    NVIC_SD_Detect();
    //EXTI_ClearITPendingBit(EXTI_Line4);
#endif    
    // ----------------------------------------------------------------
    // RTMCBIF: RTMCBuC_nIRQ (INPUT PULL_UP)
    // EXT INTERRUPT, LINKS TO PD4, FALLING EDGE
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource4); 
    EXTIx_IRQ_Config(RTMCBuC_nIRQ_GPIO_IRQ, PreemptivePriority_RTMCBuC_nIRQ_EXTI4, SubPriority_RTMCBuC_nIRQ_EXTI4);
    NVIC_IO_EXTI(EXTI_Line4,EXTI_Trigger_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line4);
#if 0    
    // ----------------------------------------------------------------
    // ACCELEROMETER: ACC_IRQ (INPUT PULL_UP)
    // EXT INTERRUPT, LINKS TO PD9, FALLING EDGE
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource9); 
    EXTIx_IRQ_Config(ACC_IRQ_GPIO_IRQ, PreemptivePriority_ACC_IRQ_EXTI9, SubPriority_ACC_IRQ_EXTI9);
    NVIC_IO_EXTI(EXTI_Line9,EXTI_Trigger_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line9);
    // ----------------------------------------------------------------
#if 0
    // ----------------------------------------------------------------
    // PM1: PM1uC_nIRQ (INPUT PULL_UP)
    // EXT INTERRUPT, LINKS TO PE15, FALLING EDGE
    // ----------------------------------------------------------------
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOE, GPIO_PinSource15); 
    EXTIx_IRQ_Config(PM1uC_nIRQ_GPIO_IRQ, PreemptivePriority_PM1uC_IRQ_EXTI15, SubPriority_PM1uC_IRQ_EXTI15);
    NVIC_IO_EXTI(EXTI_Line15,EXTI_Trigger_Falling);
    //EXTI_ClearITPendingBit(EXTI_Line15);
    // ----------------------------------------------------------------
#endif
    // ----------------------------------------------------------------
#endif
}

// -----------------------------------------------------------------------------------
//! \brief  MB_EXTI_Config
//!
//!  MB Configures the external input interrupts
//!
//! \param[IN]:      whichPM, 1: PORT_PM1, 2: PORT_PM2, 3: PORT_RTMCB
//! \param[IN]:      enablenDisable, 1: enable, 0: disable
//! 
//! \return     none
// -----------------------------------------------------------------------------------
#if 0
void MB_PMX_COM_ENABLE(uint8_t whichPM, uint8_t enablenDisable){
    switch(whichPM){
        case PORT_PM1:
            if(enablenDisable == SIGNAL_ON){
            else if(enablenDisable == SIGNAL_OFF){
                // HIGH (RX DISABLED)
                STM32F_GPIOOn(PM1uC_CTRL_RX_GPIO_PORT, PM1uC_CTRL_RX_GPIO_PIN);        
                // LOW (TX DISABLED)
                STM32F_GPIOOff(PM1uC_CTRL_TX_GPIO_PORT, PM1uC_CTRL_TX_GPIO_PIN);        
            }
        break;
        case PORT_PM2:
            if(enablenDisable == SIGNAL_ON){
            else if(enablenDisable == SIGNAL_OFF){
                // HIGH (RX DISABLED)
                STM32F_GPIOOn(PM2uC_CTRL_RX_GPIO_PORT, PM2uC_CTRL_RX_GPIO_PIN);        
                // LOW (TX DISABLED)
                STM32F_GPIOOff(PM2uC_CTRL_TX_GPIO_PORT, PM2uC_CTRL_TX_GPIO_PIN);        
            }
        break;
        case PORT_RTMCB:
            if(enablenDisable == SIGNAL_ON){
            else if(enablenDisable == SIGNAL_OFF){
            }
        break;
    }
}
#endif

// -----------------------------------------------------------------------------------
//! \brief  MB_RCC_Configuration
//!
//!  MB Configures the different system clocks
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MB_RCC_Configuration(void)
{
  ErrorStatus HSEStartUpStatus;
  
  // RCC system reset(for debug purpose) 
  RCC_DeInit();

  // Enable HSE 
  RCC_HSEConfig(RCC_HSE_ON);

  // Wait till HSE is ready 
  HSEStartUpStatus = RCC_WaitForHSEStartUp();

  if(HSEStartUpStatus == SUCCESS)
  {
    // Enable Prefetch Buffer 
    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);

    // Flash 2 wait state 
    FLASH_SetLatency(FLASH_Latency_2);

    // HCLK = SYSCLK 
    RCC_HCLKConfig(RCC_SYSCLK_Div1);

    // PCLK2 = HCLK 
    RCC_PCLK2Config(RCC_HCLK_Div1);

    // PCLK1 = HCLK/2 
    RCC_PCLK1Config(RCC_HCLK_Div2);

#ifdef STM32F_MB
    // PLLCLK = 14.7456MHz * 5 = 73.728 MHz [RTMCB]
    RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_5);
#else
    // gdu: 25.06.2011
    // PLLCLK = 25MHz * 9 = 225 MHz [NO VA!!] 
    RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_9);
    // PLLCLK = 25MHz * 3 = 75 MHz [EVAL]
    // RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_PLLMul_3); [correct]
#endif

    // Enable PLL 
    RCC_PLLCmd(ENABLE);

    // Wait till PLL is ready 
    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

    // Select PLL as system clock source 
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    
    // Wait till PLL is used as system clock source 
    while(RCC_GetSYSCLKSource() != 0x08);
  }
  
  // Enable GPIOA, GPIOB, GPIOC, GPIOD, GPIOE and AFIO clocks 
  RCC_APB2PeriphClockCmd(  RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB 
                         | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD
                         | RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO , ENABLE);

  // TIM2 clock enable -- PWM Input Mode 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  // TIM1 Periph clock enable 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

  // MCO OUTPUT
  RCC_MCOConfig(RCC_MCO_PLLCLK_Div2);     // around 76MHZ (PLLDIV9) > I/O, 37.74MHZ (PLLDIV3)
  // RCC_MCOConfig(RCC_MCO_HSE);          // 25MHZ
  // RCC_MCOConfig(RCC_MCO_SYSCLK);       // SYSTICK around 155MHZ, 75MHZ

}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_SysTickConfig
//!
//! Configure a SysTick Base time to 1 ms
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void DEVICE_SysTickConfig(void)
{
  if (SysTick_Config(SYSTICK_FREQUENCY)) {
      // Capture error 
      while (1);
  }
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
