// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   tools.c
//! \brief  useful routines
//!
//! mainly data conversion routines
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include <string.h>
#include "main.h"
// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! hexa value --> character
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint8_t HEXA_CHAR(uint8_t _charn){
    if (_charn >= 0 && _charn <= 9) return (uint8_t)(_charn + 48);
    else if (_charn >= 10 && _charn <= 15) return (uint8_t)(_charn + 55);
    else return 48;
}
// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! 1-byte --> hexa --> character string [2+'\0']
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
int BYTE_TO_CH_ASCII(uint8_t _recvb, char* _arrx, int qty)
{
    if(qty<2) return -1;
    uint8_t _dbh = (uint8_t)((_recvb & 0xF0) / 16);
    uint8_t _dbl = (uint8_t)(_recvb & 0x0F);
    char _dbf[2];
    _dbf[0] = HEXA_CHAR(_dbh);
    _dbf[1] = HEXA_CHAR(_dbl);
   _arrx[0] = _dbf[0];
   _arrx[1] = _dbf[1];            
    return 0;
}
// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! 2-byte --> hexa --> character string [4+'\0']
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
int UINT16_TO_CH_ASCII(uint16_t _recv_dw, char *_arr_dw, int qty) {
    if(qty<4) return -1;
    char _arrx[2];
    uint8_t _b1 = (uint8_t)((_recv_dw & 0x0000FF00) >> 8);
    BYTE_TO_CH_ASCII(_b1, &_arrx[0],2);
    _arr_dw[0] = _arrx[0];
    _arr_dw[1] = _arrx[1];
    uint8_t _b0 = (uint8_t)(_recv_dw & 0x000000FF);
    BYTE_TO_CH_ASCII(_b0, &_arrx[0],2);
    _arr_dw[2] = _arrx[0];
    _arr_dw[3] = _arrx[1];
    return 0;
}
// -----------------------------------------------------------------------------------
//! \brief  name
//!
//! 4-byte --> hexa --> character string [8+'\0']
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
int8_t UINT32_TO_CH_ASCII(uint32_t _recv_dw, char *_arr_dw, int qty) {
    if(qty<8) return -1;
    char _arrx[2];
    uint8_t _b3 = (uint8_t)((_recv_dw & 0xFF000000) >> 24);
    BYTE_TO_CH_ASCII(_b3, &_arrx[0],2);
    _arr_dw[0] = _arrx[0];
    _arr_dw[1] = _arrx[1];
    uint8_t _b2 = (uint8_t)((_recv_dw & 0x00FF0000)>> 16);
    BYTE_TO_CH_ASCII(_b2, &_arrx[0],2);
    _arr_dw[2] = _arrx[0];
    _arr_dw[3] = _arrx[1];
    uint8_t _b1 = (uint8_t)((_recv_dw & 0x0000FF00) >> 8);
    BYTE_TO_CH_ASCII(_b1, &_arrx[0],2);
    _arr_dw[4] = _arrx[0];
    _arr_dw[5] = _arrx[1];
    uint8_t _b0 = (uint8_t)(_recv_dw & 0x000000FF);
    BYTE_TO_CH_ASCII(_b0, &_arrx[0],2);
    _arr_dw[6] = _arrx[0];
    _arr_dw[7] = _arrx[1];
    return 0;
}
// -----------------------------------------------------------------------------------
//! \brief  GET08
//!
//! get 8-bit value from buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint8_t GET08(uint8_t* rxbuffer, uint16_t rxindex)
{
    return rxbuffer[rxindex + 0];
}
// -----------------------------------------------------------------------------------
//! \brief  GET16
//!
//! get 16-bit value from buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint16_t GET16(uint8_t *rxbuffer, uint16_t rxindex){
    uint16_t value = 0;
    value += rxbuffer[rxindex+0] << 8;
    value += rxbuffer[rxindex+1];
    return value;
}
// -----------------------------------------------------------------------------------
//! \brief  GET32
//!
//! get 32-bit value from buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
uint32_t GET32(uint8_t *rxbuffer, uint16_t rxindex){
    uint32_t value = 0;
    value += rxbuffer[rxindex+0] << 24;
    value += rxbuffer[rxindex+1] << 16;
    value += rxbuffer[rxindex+2] << 8;
    value += rxbuffer[rxindex+3];
    return value;
}


// -----------------------------------------------------------------------------------
//! \brief  SET32
//!
//! set a 32-bit value to buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SET32(uint8_t *txbuffer, uint16_t txindex, uint32_t data_out){
    txbuffer[txindex+0] = (uint8_t)(data_out>>24);
    txbuffer[txindex+1] = (uint8_t)(data_out>>16);
    txbuffer[txindex+2] = (uint8_t)(data_out>>8);
    txbuffer[txindex+3] = (uint8_t)(data_out);
}
// -----------------------------------------------------------------------------------
//! \brief  SET16
//!
//! set a 16-bit value to buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SET16(uint8_t *txbuffer, uint16_t txindex, uint16_t data_out){
    txbuffer[txindex+0] = (uint8_t)(data_out>>8);
    txbuffer[txindex+1] = (uint8_t)(data_out);
}
// -----------------------------------------------------------------------------------
//! \brief  SET08
//!
//! set a 8-bit value to buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SET08(uint8_t *txbuffer, uint16_t txindex, uint8_t data_out){
    txbuffer[txindex+0] = (uint8_t)(data_out);
}
// -----------------------------------------------------------------------------------
//! \brief  SET8_PV
//!
//! sets: 
//! parameterID     [8-bit]
//! parameterQTY    [8-bit]
//! parameterValue  [8-bit]
//!
//! \param   output buffer
//! \param   index in buffer
//! \param   parameterID
//! \param   parameterValue
//! \return  new index
// -----------------------------------------------------------------------------------
uint32_t SET8_PV (uint8_t *txbuffer, uint16_t txindex, uint8_t parameterID, uint32_t data_out ) {
    txbuffer[txindex+0] = parameterID;
    txbuffer[txindex+1] = 1;
    txbuffer[txindex+2] = (uint8_t) data_out;
    txindex+=3;
    return txindex;
}
// -----------------------------------------------------------------------------------
//! \brief  SET16_PV
//!
//! sets: 
//! parameterID     [8-bit]
//! parameterQTY    [8-bit]
//! parameterValue  [16-bit]
//!
//! \param   output buffer
//! \param   index in buffer
//! \param   parameterID
//! \param   parameterValue
//! \return  new index
// -----------------------------------------------------------------------------------
uint32_t SET16_PV(uint8_t *txbuffer, uint16_t txindex,  uint8_t parameterID, uint32_t data_out ) {
    txbuffer[txindex+0] = parameterID;
    txbuffer[txindex+1] = 2;
    txbuffer[txindex+2] = (uint8_t)(data_out>>8);
    txbuffer[txindex+3] = (uint8_t) data_out;
    txindex+=4;
    return txindex;
}
// -----------------------------------------------------------------------------------
//! \brief  SET32_PV
//!
//! sets: 
//! parameterID     [8-bit]
//! parameterQTY    [8-bit]
//! parameterValue  [32-bit]
//!
//! \param   output buffer
//! \param   index in buffer
//! \param   parameterID
//! \param   parameterValue
//! \return  new index
// -----------------------------------------------------------------------------------
uint32_t SET32_PV(uint8_t *txbuffer, uint16_t txindex, uint8_t parameterID, uint32_t value )
{
    txbuffer[txindex+0] = parameterID;
    txbuffer[txindex+1] = 4;
    txbuffer[txindex+2] = (uint8_t)(value>>24);
    txbuffer[txindex+3] = (uint8_t)(value>>16);
    txbuffer[txindex+4] = (uint8_t)(value>>8);
    txbuffer[txindex+5] = (uint8_t) value;
    txindex+=6;
    return txindex;
}
// -----------------------------------------------------------------------------------
//! \brief  SET32_PV
//!
//! sets: 
//! parameterID         [8-bit]
//! parameterQTY (size) [8-bit]
//! parameter Buffer    [8-bit x size]
//!
//! \param  output buffer
//! \param  index in buffer
//! \param  parameterID
//! \param  parameter Buffer
//! \param  buffer size
//! \return new index
// -----------------------------------------------------------------------------------
uint32_t SET_PV( uint8_t *txbuffer, uint16_t txindex, uint8_t parameterID, const uint8_t* pbuffer, uint16_t size )
{

    txbuffer[txindex+0] = parameterID;
    txbuffer[txindex+1] = size;
    memcpy( &txbuffer[txindex+2], pbuffer, size );
    txindex += (2+size);
    return txindex;
}
// ------------------------------------------------------
// SD CARD SPECIFIC
// ------------------------------------------------------
// ------------------------------------------------------
// Verifies if the file is data file
// ------------------------------------------------------
int ComparePrefixes(char *zLogFileNamePrefix, char *fileread, uint8_t PREFIXLEN){
  uint8_t ix=0;
  for(ix=0;ix< PREFIXLEN;ix++) {
    if(zLogFileNamePrefix[ix] != fileread[ix]) return 1;
  }
  return 0; // they are equal
}

// -----------------------------------------------------------------------------------
//! \brief  DelayBySoft
//!
//! generates a simple loop 
//!
//! \param  no of iterations
//!
//! \return nothing
// -----------------------------------------------------------------------------------
void DelayBySoft (uint32_t DELAYLOOP){
    uint32_t delaycounter = 0;
    for(delaycounter = 0;delaycounter<DELAYLOOP; delaycounter++);
}
// ------------------------------------------------------
// End tools_01.c
// ------------------------------------------------------
