// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   rtmcb_tasks.h
//! \brief  API: Link between the LLD and the TASK
//!
//! .....
//!
//! \author  Dudnik G.S.
//! \date    14.02.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private Definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_putData
//!
//! ....
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void RTMCB_putData(){
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_DecodeMsg
//!
//! ....
//!
//! \param      none
//!
//! \return     uint8_t 
// -----------------------------------------------------------------------------------

#ifndef VP_SIMULATION
	tdDataId decodeMsg(){

	#warning Debug code to receive the status without RTB available. Must be removed again later!
		extern uint8_t newStaticBufferStatesRTB;
		if (newStaticBufferStatesRTB) {
				// The current state is already in
				// extern tdWakdStates           staticBufferStateRTB;
				// extern tdWakdOperationalState staticBufferOperationalStateRTB;
				#if defined SEQ0 || defined SEQ4
					taskMessage("I", "decodeMsg()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
				#endif
				newStaticBufferStatesRTB = 0;
			}
			return(dataID_currentWAKDstateIs);


		// Flag for the task
		RTMCB_message = dataID_undefined;

		uint16_t w = 0;
		uint16_t OUT_STR_TX_LEN_RTMCB = 0;

		if (MB_SYSTEM_READY==DEVICE_READY){
			// OUT_COMM_RTMCB_FLAG = 0xFF, a full message has been received
			// RTB has sent a messsage
			if(OUT_COMM_RTMCB_FLAG == 0xFF){
				if(RTMCB_DMA_RX_NEW == SIGNAL_ON){
					for(w=0; w<out_rtmcb_reclen_0;w++){
						zrx_out_rtmcb_str_1[w] =  zrx_out_rtmcb_str_0[w];
						zrx_out_rtmcb_str_0[w] = 0;
					}
					out_rtmcb_reclen_1 = out_rtmcb_reclen_0;
					out_rtmcb_reclen_0 = 0;
					RTMCB_DMA_RX_NEW = SIGNAL_OFF;
				}

				if((out_rtmcb_reclen_1>0)&&(out_rtmcb_reclen_1<=TXRX_RS422_RTMCB_MAXVALUE)) {
					OUT_STR_TX_LEN_RTMCB = DECOBS_DECRC16_PACKETS(zrx_out_rtmcb_str_1, out_rtmcb_reclen_1, &BufferXYZ_RTMCB[0], &RAW_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
					if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
						rs422_rtmcb_whichgroup = RS422_NOGROUP;
						OUT_STR_TX_LEN_RTMCB =  RS422_ANALYZE_RESPONSE(OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &RAW_Buffer_RTMCB[0], &OK_RTMCB[0]);

						OUT_STR_TX_LEN_RTMCB = 0; // no meaning for this value as sender....
						if(rs422_rtmcb_whichgroup!=RS422_NOGROUP){
							switch(rs422_rtmcb_whichgroup){
								case RS422_ACK_RTMCB_READY:
									rs422_rtmcb_sequenceno++;
									OUT_STR_TX_LEN_RTMCB = RS422_MAKE_ANSWER_NP_SYS_ACKRTMCBREADY (&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
									// NEXT STEP, SEND SOME COMMANDS...
									RTMCB_SimulationNoRT = SIMULATION_MODE;
									// shortcut
									//rs422_rtmcb_commands_scheduler[0] = RS422_SIMULATIONVSRT;
									rs422_rtmcb_commands_scheduler[0] = RS422_NOGROUP;
									rtmcb_cmd_streaming = SIGNAL_ON;

								break;
								case RS422_SIMULATIONVSRT:
									RTMCB_StreamingYesNo = STREAMING_ON;
									rs422_rtmcb_commands_scheduler[0] = RS422_STSTSTREAMING;

								break;
								case RS422_STSTSTREAMING:
									rs422_rtmcb_commands_scheduler[0] = RS422_NOGROUP;
									rtmcb_cmd_streaming = SIGNAL_ON;
								break;
								case RS422_ACK_RECV_DATA:
									if (RTMCB_STATUS==INTERVAL){
									  rs422_rtmcb_sequenceno++;
									  OUT_STR_TX_LEN_RTMCB = RS422_MAKE_ANSWER_NP_DATA_ACKINTERVAL (&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
									}
								break;
							}
						}
						if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
							OUT_STR_TX_LEN_RTMCB = PACKETS_CRC16_COBS (RAW_Buffer_RTMCB, OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &COB_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
							if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
								for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {
									USART_SendData(UART4, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
									while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET) {}
								} // end send
								rs422_tx_rtmcb_counter=0;
							} // end if cobbed message len > 0
						} // end if message len > 0
					} // end if de-cobbed message len > 0
				} // end if received message len > 0
				CLR_RTMCB_COMM_BUFFERS();

				// if there is new data received from RTMCB to be transferred to internal buffer
				if (RTMCB_STATUS==INTERVAL){
					#if 0
					EXECUTE_SENSORS_ACTUATORS_UPDATE();
					#endif
					RTMCB_STATUS = NONE;
				}

			} // OUT_COMM_FLAG = 0xFF
		} // MB_SYSTEM_READY==DEVICE_READY

		if (MB_SYSTEM_READY==DEVICE_READY){
			// theoretically 100ms
			rs422_timecounter++;
			if(rs422_timecounter == RS422_RTMCB_TX_CNT){
				rs422_timecounter = 0;
				// if there is a command to be sent to RTMCB
				if((rs422_rtmcb_whichgroup !=RS422_ACK_RTMCB_READY) && (rs422_rtmcb_commands_scheduler[0]!=RS422_NOGROUP)){
					rs422_rtmcb_sequenceno++;
					OUT_STR_TX_LEN_RTMCB = RS422_RTMCB_SEND_COMMAND (0);
					if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
						OUT_STR_TX_LEN_RTMCB = PACKETS_CRC16_COBS (RAW_Buffer_RTMCB, OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &COB_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
						if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
							for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {
								USART_SendData(UART4, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
								while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET) {}
							} // end send
							OUT_STR_TX_LEN_RTMCB=0;
						} // end if cobbed message len >0
					} // end if message len > 0
				} // end if command to
			} // end if rs422_timecounter = RS422_RTMCB_TX_CNT (100ms)
		} // MB_SYSTEM_READY==DEVICE_READY


		return RTMCB_message;
	}
#endif

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
