/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "main.h"
	#include "reminder.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\main\\main.h"
	#include "reminder.h"
#endif

#define PREFIX "OFFIS FreeRTOS task Reminder"

/*-----------------------------------------------------------*/

tdReminder *msgAlreadyRegistered(tdReminder *searchIndex, tdQtoken *pQtoken, tdTasks remindTask);

void mvToEndOfList(tdReminder **pStartOfList, tdReminder **pEndOfList, tdReminder *pMove);

void delFromList(tdReminder **pStartOfList, tdReminder **pEndOfList, tdReminder *pDelete);

// void printOrderInList(tdReminder *pIndexer);

portTickType nextWakeupTime(tdReminder *pStartOfList);

void tskReminder( void *pReminder )
{
	taskMessage("I", PREFIX, "Starting Reminder task!");

	xQueueHandle *qhCBPAREMin = ((tdAllHandles *) pReminder)->allQueueHandles.qhREMINDERin;
	xQueueHandle *qhCBPAin    = ((tdAllHandles *) pReminder)->allQueueHandles.qhCBPAin;

 	tdQtoken Qtoken;
	Qtoken.command = command_WakeupCall;
	Qtoken.pData   = NULL;

	tdReminder   *pStartOfList  = NULL;
	tdReminder   *pEndOfList    = NULL;

	// On an empty list this tasks waits forever on something to arrive in its queue.
	// If a msg to remember is listed, the task computes how long to sleep from the current time
	// to the point were it needs to wake up and remind the msg.
	portTickType wakupAfterMs   = portMAX_DELAY; // initially never wake up

	// Defines the time period after which the task should remind/resend
	portTickType const waitingTime = 100 / portTICK_RATE_MS; // set 100 ms

	// Defines the number of tries before giving up.
	uint8_t const maxRetry = 3;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	for( ;; ) {
		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		if ( xQueueReceive( qhCBPAREMin, &Qtoken, wakupAfterMs) == pdPASS )
		{	// The task woke up due to an incoming queue item. Process token!
			#ifdef DEBUG_REMINDER
				taskMessage("I", PREFIX, "Found something in queue.");
			#endif

                        /*
			switch (Qtoken.command) {
				case command_WaitOnMsgCBPA: {
					#ifdef DEBUG_REMINDER
						taskMessage("I", PREFIX, "command_WaitOnMsg with cnt: %d", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount );
					#endif
					// Is this the first time we get this msg or is it already in the list?
					tdReminder *pEntry = NULL;
					pEntry = msgAlreadyRegistered(pStartOfList, &Qtoken, tasks_CBPA);
					if (pEntry == NULL)
					{	// First time we get this message. Allocate data structure to remember it
						#ifdef DEBUG_REMINDER
							taskMessage("I", PREFIX, "Listing new msg to remind!");
						#endif
						pEntry = (tdReminder *) pvPortMalloc(sizeof(tdReminder));
						if ( pEntry == NULL )
						{	// unable to allocate memory
							taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
							errMsg(errmsg_pvPortMallocFailed);
							sFree(&(Qtoken.pData));
							break;
						}
						pEntry->remindTask		= tasks_CBPA;
						pEntry->pMsg 			= Qtoken.pData;
						pEntry->sentTrials 		= 1; // Has been sent once
						pEntry->wakeUpTime 		= xTaskGetTickCount() + waitingTime;
						pEntry->pNextInList 	= NULL;	// Will be the new last element in list
						if (pStartOfList == NULL)
						{	// List is empty, create first and only (last) entry
							#ifdef DEBUG_REMINDER
								taskMessage("I", PREFIX, "This is the first msg in list!");
							#endif
							pEntry->pPrevInList = NULL;
							pStartOfList = pEntry;
						} else {
							pEntry->pPrevInList = pEndOfList;
							pEndOfList->pNextInList = pEntry;
						}
						pEndOfList = pEntry;
					} else {
						// This is not the first time we receive this msg. So we do not need to put it
						// in list, it is already there. But need to increase retry counter.
						pEntry->sentTrials++;
					}
					break;
				}
				case command_AckOnMsgCBPA: {
					#ifdef DEBUG_REMINDER
						taskMessage("I", PREFIX, "Received an ACK!");
					#endif
					tdReminder *pEntry = NULL;
					pEntry = msgAlreadyRegistered(pStartOfList, &Qtoken, tasks_CBPA);
					if (pEntry == NULL)
					{	// The msg to which the received Ack corresponds to could not be found.
						taskMessage("E", PREFIX, "Ack for unknown msg received!");
						errMsg(errmsg_NotInListForAckOnMsgCBPA);
					} else { // take the msg of our list. We do not need to remember it anymore.
						delFromList(&pStartOfList, &pEndOfList, pEntry);
					}
					// Do not forget to free the Ack message itself!
					sFree(&(Qtoken.pData));
					break;
				}
				default: {
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
                  */
                  sFree(&(Qtoken.pData));

		} else { // The task woke up on its own, NO TOKEN to process. The cause is, that first entry in list to
				 // remember is due. This entry has to be resent now, or discarded.
			if (pStartOfList == NULL)
			{	// This should never have happened! If the list is empty, the task should have slept forever and not be here now!
				taskMessage("E", PREFIX, "Woke up on empty list! This is a bug in the SW!");
			} else {
				uint8_t sentTrials = pStartOfList->sentTrials;
				if (sentTrials >= maxRetry)
				{	// We give up. Delete message.
					if( ((tdMsgOnly*)(pStartOfList->pMsg))->header.recipientId == whoId_SP )
					{	// smartphone is not responsive
						#ifdef DEBUG_REMINDER
							taskMessage("I", PREFIX, "Smartphone unreachable. Discarding msg for cnt: %d.", ((tdMsgOnly *)(pStartOfList->pMsg))->header.msgCount);
						#endif
						errMsg(errmsg_SmartphoneUnreachable);
					} else {
						// communication board is not responsive
						errMsg(errmsg_CommunicationBoardUnreachable);
						#ifdef DEBUG_REMINDER
							taskMessage("I", PREFIX, "ComBoard unreachable. Discarding msg for cnt: %d.", ((tdMsgOnly *)(pStartOfList->pMsg))->header.msgCount);
						#endif
					}
					delFromList(&pStartOfList, &pEndOfList, pStartOfList);
				} else {
					#ifdef DEBUG_REMINDER
						taskMessage("I", PREFIX, "Time to inform about missing Ack for cnt: %d.", ((tdMsgOnly *)(pStartOfList->pMsg))->header.msgCount );
					#endif
					Qtoken.command = command_WakeupCall;
					Qtoken.pData   = pStartOfList->pMsg;
					if ( xQueueSend( qhCBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
					{	// CBPA queue did not accept data. Free message and send error.
						taskMessage("E", PREFIX, "Queue of CBPA did not accept 'command_WakeupCall'!");
						errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
						delFromList(&pStartOfList, &pEndOfList, pStartOfList);
					} else {
						// Retransmission is on its way. Put back at end of queue and increase time for another waiting period.
						pStartOfList->wakeUpTime = pStartOfList->wakeUpTime + waitingTime;
						mvToEndOfList(&pStartOfList, &pEndOfList, pStartOfList);
					}
				}
			}
		}
		// Compute next time when to wake up
		wakupAfterMs = nextWakeupTime(pStartOfList);
	}
}

portTickType nextWakeupTime(tdReminder *pStartOfList)
{
	// Compute next time when to wake up
	if (pStartOfList == NULL)
	{	// Nothing in list to wait for. Wait forever until something new is in queue
		return(portMAX_DELAY);
	} else {
		portTickType now = xTaskGetTickCount();
		if (now < (pStartOfList->wakeUpTime))
		{	// Go to sleep until then
			return(pStartOfList->wakeUpTime - now);
		} else {
			// Don't go to sleep, continue right through (wait for 0)
			return(0);
		}
	}
}

tdReminder *msgAlreadyRegistered(tdReminder *searchIndex, tdQtoken *pQtoken, tdTasks remindTask)
{
	while (searchIndex != NULL)
	{
		if (	// is the sending task the same? e.g. CBPA or RTBPA are different msg even with same msgCount
				(remindTask == (searchIndex->remindTask))
				&&
				// And is the msg id/number the same?
				((((tdMsgOnly *)(pQtoken->pData))->header.msgCount)	== (((tdMsgOnly *)(searchIndex->pMsg))->header.msgCount))
				// only if sender and msg id are the same it is a msg we already received before!
			)
		{	// This msg is already in our list
			return(searchIndex);
		}
		searchIndex = searchIndex->pNextInList;
	}
	return(NULL);
}

void mvToEndOfList(tdReminder **pStartOfList, tdReminder **pEndOfList, tdReminder *pMove)
{
	if (pMove->pNextInList != NULL)	// if == NULL already at end, nothing to do!
	{
		if (pMove->pPrevInList == NULL) {
			// First element in list
			*pStartOfList = pMove->pNextInList;
			(pMove->pNextInList)->pPrevInList = NULL;
		} else {
			(pMove->pPrevInList)->pNextInList = pMove->pNextInList;
			(pMove->pNextInList)->pPrevInList = pMove->pPrevInList;
		}
		(*pEndOfList)->pNextInList = pMove;
		pMove->pPrevInList = *pEndOfList;
		pMove->pNextInList = NULL; // is now last in list
		*pEndOfList = pMove;
	}
}

// Watch out: this is a pointer to the pointer pStartOfList! **!
void delFromList(tdReminder **pStartOfList, tdReminder **pEndOfList, tdReminder *pDelete)
{
	if (pDelete->pPrevInList == NULL)
	{	// First element in list (can be both, first and last!!)
		*pStartOfList = pDelete->pNextInList;
	} else {
		(pDelete->pPrevInList)->pNextInList = pDelete->pNextInList;
	}
	if (pDelete->pNextInList == NULL)
	{	// Last element in list (can be both, first and last!!)
		*pEndOfList = pDelete->pPrevInList;
	} else {
		(pDelete->pNextInList)->pPrevInList = pDelete->pPrevInList;
	}
	sFree(&(pDelete->pMsg));	// Delete the msg
	sFree((void *)&pDelete);			// Delete the list data structure.
}

//#ifndef VP_SIMULATION
//	#error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
//#endif
//void printOrderInList(tdReminder *pIndexer)
//{
//	printf("The order in list is (id|trials): ");
//	while (pIndexer != NULL)
//	{
//		printf("%d|%d, ", ((tdMsgOnly *)(pIndexer->pMsg))->header.msgCount, pIndexer->sentTrials);
//		pIndexer = pIndexer->pNextInList;
//	}
//	printf("\n");
//}
