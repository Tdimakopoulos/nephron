/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef DECISIONTREE_H
#define DECISIONTREE_H

#ifdef LINUX
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
#endif

// Keep in mind:
// 1: for Sorbent dysfunciton: the flow of the time of chemical measurement is needed	
// 2: wir brauchen hier signed float! CaTrd=c_Ca_past-c_Ca; %increase: positive decrease:negative

/* this was the firt version where all the sonsors are chekced at once - careful some sensors will not be implemented in HW!
void decisionTrees(
	tdParametersSCC *parametersScc, //This pointer can stay the same for both the divided trees. Just the single decision trees accordingly look at the actual parameters, that they are concerned of!
	sensorType c_Na,   sensorType c_Na_after,   sensorType c_Na_past,
	sensorType c_K,    sensorType c_K_after,    sensorType c_K_past,
	sensorType c_Ca,   sensorType c_Ca_after,   sensorType c_Ca_past,
	sensorType c_Urea, sensorType c_Urea_after, sensorType c_Urea_past,
	sensorType c_Crea, sensorType c_Crea_after, sensorType c_Crea_past,
	sensorType c_Phos, sensorType c_Phos_after, sensorType c_Phos_past,
	sensorType c_HCO3, sensorType c_HCO3_after, sensorType c_HCO3_past,
	sensorType Ph,     sensorType Ph_after,     sensorType Ph_past,
	sensorType BPsys,  sensorType BPsys_past,   sensorType BPdia,       sensorType BPdia_past,
	sensorType Wght,   sensorType Wght_past,
	sensorType ECGArr,
	sensorType pumpBflow, sensorType pumpFflow,
	sensorType pumpBflow_set, sensorType pumpFflow_set,
	sensorType U_volt,
	sensorType LeakDet,
	sensorType VertDefleciton,
	sensorType FexV,   sensorType FexV_set,
	sensorType BPSo,   sensorType BPSi,
	sensorType FPS1,   sensorType FPS2,
	sensorType BTSo,   sensorType BTSi,
	sensorType BatStat,
	sensorType det_S1, sensorType det_S2, sensorType det_S3,
	tdQtoken *Qtoken,
	tdAllQueueHandles *pAllQueueHandles
);
*/

void decisionTrees_physical(tdParametersSCC *pParametersScc, tdMsgPhysicalsensorData *pMsgPhysicalsensorData, xQueueHandle *pQHDISPin);

void decisionTrees_actuator(tdParametersSCC *pParametersScc, tdMsgActuatorData *pMsgActuatorData, xQueueHandle *pQHDISPin);
	
void decisionTrees_physiological(
	tdParametersSCC *pParametersScc,
	tdMsgPhysiologicalData *pMsgPhysiologicalData,
	sensorType pumpFflow,
	sensorType U_volt,
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin);
	
void decisionTrees_KTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin);

void decisionTrees_NaTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin);

void decisionTrees_PhTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	unsigned char *pSorDys,
	xQueueHandle *pQHDISPin);
	
void decisionTrees_UrTrnd(	
	tdParametersSCC *pParametersScc,
	uint16_t *valueNow, 
	tdECPData *conc_past, // includes old measurements, that were recorded 'in the patient' durling last ultrafiltration
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin);
	
void decisionTrees_Wght(	
	tdParametersSCC *pParametersScc, 
	float *weightNow,
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin);
	
void decisionTrees_WghtTrnd(	
	tdParametersSCC *pParametersScc, 
	float *weightNow,
	float *weightOld,
	//required API information to be able to sent out queue messages
	xQueueHandle *pQHDISPin);	

	
	
// void decisionTrees_ECG(	
	// tdParametersSCC *pParametersScc,
	// tdMsgEcgData *pMsgECGData,
	// tdQtoken *pQtoken,
	// xQueueHandle *pQHDISPin);

void decisionTrees_BP(tdParametersSCC *pParametersScc, tdMsgBpData *pMsgBPData, xQueueHandle *pQHDISPin);
	
#endif
