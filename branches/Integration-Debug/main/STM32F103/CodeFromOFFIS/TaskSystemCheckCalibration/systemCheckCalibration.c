/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "systemCheckCalibration.h"

#define PREFIX "OFFIS FreeRTOS task SCC"

uint8_t getDataFromDS(tdDataId dataId2get, void *pDtaStructure, xQueueHandle *pQH);

uint8_t heartBeatSCC = 0;

void tskSCC( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting System Check and Calibration task!");
        #ifdef CROSSPRINTF
            debug_printf("Starting System Check and Calibration task!\n");
        #endif
  
	// tdAllQueueHandles *pAllQueueHandles = &(((tdAllHandles *) (pvParameters))->allQueueHandles);
	xQueueHandle *qhDISPin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhSCCin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhSCCin;
 	tdQtoken Qtoken;
	
	extern tdWakdStates 			staticBufferStateRTB;
	
	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	//tdParametersSCC *pParametersScc_toReconfig= NULL; // pointer for reconfiuration on-the-fly
	tdParametersSCC parametersScc;
	
	parametersScc.thisState = wakdStates_AllStopped; // get parameters for this state first
	Qtoken.command	= command_InitializeSCC;
 	Qtoken.pData	= (void *) &parametersScc;
	#if defined DEBUG_SCC || defined SEQ0
		taskMessage("I", PREFIX, "Sending 'command_InitializeSCC'.");
	#endif
        #ifdef CROSSPRINTF
            debug_printf("Sending 'command_InitializeSCC'.\n");
        #endif
	
	if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
	{	// Seemingly the queue is full. Do something accordingly!
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_InitializeSCC'.");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
	} else {
		// Receive answer to InitializeSCC request from DS
		int gotTheResponse=0;
		while (!gotTheResponse) // busy waiting
		{
			if ( xQueueReceive( qhSCCin, &Qtoken, FC_PERIOD) == pdPASS )
			{	// Check if we received status
				if (Qtoken.command == command_InitializeSCCReady)
				{
					#if defined DEBUG_FC || defined SEQ0
						taskMessage("I", PREFIX, "SCC reconfiguration for new state %s done.", stateIdToString(parametersScc.thisState));
					#endif	
                                        #ifdef CROSSPRINTF
                                            debug_printf("SCC reconfiguration for new state %s done.\n", stateIdToString(parametersScc.thisState));
                                        #endif

					gotTheResponse=1;

				} else {
					sFree(&(Qtoken.pData)); //discard the wrong message
					taskMessage("E", PREFIX, "Message to arrive should be 'command_InitializeSCCReady'.");
					errMsg(errmsg_ErrSCC);
				} // of if (Qtoken.command ==...
			}
		} // of while (!gotTheResponse)
	} // of if ( xQueueSend(...

	#if defined DEBUG_SCC || defined SEQ0
		taskMessage("I", PREFIX, "Watchdog configured and running.");
	#endif
	
	// need to remember some values from distinct measurement-packages for decision trees of the other packages!
	struct strLocalActuatorData {
		int voltage;
		int bPFlow;
		int fPFlow; 
	} lastActuatorData;
	lastActuatorData.voltage= -1;
	lastActuatorData.bPFlow= -1;
	lastActuatorData.fPFlow= -1;
	
	struct strLocalPhysioData {
		uint16_t K;
		uint16_t Na;
		uint16_t Ur;
		uint16_t pH; 
	} lastPhysiologicalData;
	lastPhysiologicalData.K = 0;
	lastPhysiologicalData.Na = 0;
	lastPhysiologicalData.Ur = 0;
	lastPhysiologicalData.pH = 0;
	unsigned char SorDys=0;
	
	tdUnixTimeType timelastStateChange=0; 
	
	tdMsgWeightData *pMsgOLDWeightData = NULL;
	pMsgOLDWeightData = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData));
	if ( pMsgOLDWeightData == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	}
	sensorType lastWeight_kgrams=0.0;
	
	tdMsgPhysiologicalData *pMsgOLDPhysiologicalData = NULL;
	tdMsgPhysiologicalData MsgPhysiologicalData;
	pMsgOLDPhysiologicalData = &MsgPhysiologicalData;
	/*
	pMsgOLDPhysiologicalData = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData));
	if ( pMsgOLDPhysiologicalData == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	}
	*/
		
	for( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every SCC_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatSCC++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif

                #if 0

		if ( xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) == pdPASS ) {
			#ifdef DEBUG_SCC
				// taskMessage("I", PREFIX, "Data in queue!");
			#endif
			switch (Qtoken.command)
			{
				case command_ConfigurationChanged: {
					if  ( staticBufferStateRTB != wakdStates_UndefinedInitializing ) {
						// undefinedInitializing-State (implies per se) there is no configuration
						// otherwise...
									
						tdParametersSCC *pParametersScc_toReconfig;
						
						pParametersScc_toReconfig = (tdParametersSCC *) pvPortMalloc(sizeof(tdParametersSCC));
						if ( pParametersScc_toReconfig == NULL )
						{	// unable to allocate memory
							taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
							errMsg(errmsg_pvPortMallocFailed);
						}
						pParametersScc_toReconfig->thisState = staticBufferStateRTB; // get parameters for this state
					
						#if defined DEBUG_SCC || defined SEQ4
							taskMessage("I", PREFIX, "Received command_ConfigurationChanged. Trying to reconfigure local copy of SCC parameters from Data storage.");
						#endif								
						// now try to retrieve  new configuraiton from data storage
						Qtoken.command	= command_InitializeSCC;
						//Qtoken.pData	= (void *) &parametersScc;
						Qtoken.pData	= pParametersScc_toReconfig;
						if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) == pdPASS )
						{ // Seemingly we were able to write to the queue. Great!
							#if defined DEBUG_SCC || defined SEQ4
								taskMessage("I", PREFIX, "Request for SCC initialization ID#%i sent. SCC will reconfigure as soon as DS indicates ready with command_RequestDatafromDSReady", Qtoken.command);
							#endif
						} else { // Seemingly the queue is full. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Message is lost!!!");
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
							#endif						
						}
						// We now have to wait for command_InitializeSCCReady from DS, indicating that values have been initialized...
					
					} else {
						#if defined DEBUG_SCC || defined SEQ0
							taskMessage("I", PREFIX, "Seems we are in wakdStates_UndefinedInitializing. No need to re-configure SCC parameters!" );
						#endif
					} 
					break;
				}
				case command_InitializeSCCReady: {
					// RECONFIG the SCC task parametrization now
						// replace the local parametrization with what we got from DS 
						memcpy(&parametersScc,  ((tdParametersSCC* )Qtoken.pData), sizeof(tdParametersSCC));
						sFree(&(Qtoken.pData)); // discard the temporary config-structure
						#if defined DEBUG_SCC
							taskMessage("I", PREFIX, "Recieved notification from DS and reconfigured SCC-parameters, freed the temporary SCC-parameter-structure from DS");
						#endif
					break;
				}				
				case command_RequestDatafromDSReady : { //for all the Trend-Decision Trees, a request from DS is necessary
					tdDataId dataId = ((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_weightDataOK): {
							// as the request for past data from DS worked out fine, we can now do the Trend-analysis decision-tree functions
							// tdParametersSCC *ptr_parametersSCC = parametersSCC;
							float oldWeight_kgrams = ((float)pMsgOLDWeightData->weightData.Weight) / ((float)FIXPOINTSHIFT_WEIGHTSCALE);
							if ( lastWeight_kgrams>0 && oldWeight_kgrams>0  
									&& (getTime()- (pMsgOLDWeightData->weightData.TimeStamp) ) >(12*60*60) ) 
							{ // weight measurement for trendanalysis at least 12 hours ago
								#if defined DEBUG_SCC
									taskMessage("I", PREFIX, "Received old weight data %5.2f kg and doing weight trend analysis", oldWeight_kgrams);
								#endif
//								decisionTrees_WghtTrnd(&parametersScc,&lastWeight_kgrams,&oldWeight_kgrams,qhDISPin);
							} else {
								taskMessage("E", PREFIX, "Weight measurements: new %5.2f kg | old %5.2f kg. Seemingly wrong value(s). Error!", lastWeight_kgrams, oldWeight_kgrams );
							} // of if ( lastWeight_kgrams>0 &&....
							sFree(&(Qtoken.pData));
							break;
						}
						case (dataID_physiologicalData_K): {
							// as the request for past data from DS worked out fine, we can now do the Trend-analysis decision-tree functions							
							if (lastPhysiologicalData.K >0) 
							{ // only if we got some value in this ultrafiltration
								// we got dataID_physiologicalData_K from DB, perform K-trend-decTrees and request pH-data for next one		
								#if defined DEBUG_SCC
									taskMessage("I", PREFIX, "Received old physiological data e.g. Na: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Sodium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. K: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Potassium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. Ur: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Urea));
									taskMessage("I", PREFIX, "K now: %i. ", (int)lastPhysiologicalData.K);
								#endif						
								decisionTrees_KTrnd(&parametersScc, &lastPhysiologicalData.K,&(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO), &SorDys, qhDISPin);
							}
							sFree(&(Qtoken.pData));; // free the notificaion message
/*	Code (trend-checking) skipped, to hinder acess to SD card							
							// decTrees_KTrnd DONE; starting Trend Analysis for Na, by requesting data 
							pMsgOLDPhysiologicalData->physiologicalData.TimeStamp= (int)getTime()- (int)parametersScc.NaTrdHT; // Timestamp of the 'old data' for trend analysis
							if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0) {
								if (getDataFromDS(dataID_physiologicalData_Na, pMsgOLDPhysiologicalData, qhDISPin)) 
								{  // if sending message to DB out of old values was not sucessful
									#ifdef DEBUG_SCC
									taskMessage("E", PREFIX, "Further trend checking will not be performed. Data readout request failed");
									#endif
								} 
							} else {
								#ifdef DEBUG_SCC
								taskMessage("I", PREFIX, "Not performing Na trend checking. No data from sufficiently long time ago recorded");
								#endif
							} // of if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0		
*/							
							// do not sFree((void*)pMsgOLDPhysiologicalData); // we sent it out again for pH analysis
							break;
						}
						case (dataID_physiologicalData_Na): {

							// as the request for past data from DS worked out fine, we can now do the Trend-analysis decision-tree functions
							if (lastPhysiologicalData.Na >0) 
							{ // only if we got some value in this ultrafiltration
								// we got dataID_physiologicalData_Na from DB, perform K-trend-decTrees and request pH-data for next one		
								#if defined DEBUG_SCC
									taskMessage("I", PREFIX, "Received old physiological data e.g. Na: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Sodium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. K: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Potassium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. Ur: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Urea));
								#endif
								
								decisionTrees_NaTrnd(&parametersScc, &lastPhysiologicalData.Na, &(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO), qhDISPin);
							}
/*	Code (trend-checking) skipped, to hinder acess to SD card							
							// decTrees_NaTrnd DONE; starting Trend Analysis for pH, by requesting data 
							pMsgOLDPhysiologicalData->physiologicalData.TimeStamp= (int)getTime()- (int)parametersScc.PhTrdHT; // Timestamp of the 'old data' for trend analysis
							if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0) {
								if (getDataFromDS(dataID_physiologicalData_pH, pMsgOLDPhysiologicalData, qhDISPin)) 
								{  // if sending message to DB out of old values was not sucessful
									#ifdef DEBUG_SCC
									taskMessage("E", PREFIX, "Further trend checking will not be performed. Data readout request failed");
									#endif
								} 
							} else {
								#ifdef DEBUG_SCC
								taskMessage("I", PREFIX, "Not performing pH trend checking. No data from sufficiently long time ago recorded");
								#endif
							} // of if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0		
*/							
							sFree(&(Qtoken.pData));; // free the notificaion message
							// do not sFree((void*)pMsgOLDPhysiologicalData); // we sent it out again for pH analysis
							break;
						}
						case (dataID_physiologicalData_pH): {
							// as the request for past data from DS worked out fine, we can now do the Trend-analysis decision-tree functions
							if (lastPhysiologicalData.pH >0) 
							{ // only if we got some value in this ultrafiltration
								// 	we got dataID_physiologicalData_pH from DB, perform pH-trend-decTrees and request Ur-data for next one		
								#if defined DEBUG_SCC
									taskMessage("I", PREFIX, "Received old physiological data e.g. Na: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Sodium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. K: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Potassium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. Ur: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Urea));
								#endif
								
									decisionTrees_PhTrnd(&parametersScc, &lastPhysiologicalData.pH,&(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO), &SorDys, qhDISPin);									
							}
/*	Code (trend-checking) skipped, to hinder acess to SD card							
							// decTrees_PhTrnd DONE; starting Trend Analysis for Ur, by requesting data 
							pMsgOLDPhysiologicalData->physiologicalData.TimeStamp=getTime()-parametersScc.UreaTrdT; // Timestamp of the 'old data' for trend analysis
							if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0) {
								if (getDataFromDS(dataID_physiologicalData_Ur, pMsgOLDPhysiologicalData, qhDISPin)) 
								{ // if sending message to DB out of old values was not sucessful
									#ifdef DEBUG_SCC
									taskMessage("E", PREFIX, "Further trend checking will not be performed. Data readout request failed");
									#endif
								}
							} else {
								#ifdef DEBUG_SCC
								taskMessage("I", PREFIX, "Not performing Ur trend checking. No data from sufficiently long time ago recorded");
								#endif
							} // of if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0
*/							
							sFree(&(Qtoken.pData));; // free the notificaion message
							// do not sFree((void*)pMsgOLDPhysiologicalData); // we sent it out again for Ur analysis			
							break;
						}
						case (dataID_physiologicalData_Ur): {
							// as the request for past data from DS worked out fine, we can now do the Trend-analysis decision-tree functions
/*	Code (trend-checking) skipped, to hinder acess to SD card	
							if (lastPhysiologicalData.Ur >0) 
							{ // only if we got some value in this ultrafiltration
								// we got dataID_physiologicalData_Ur from DB, perform Ur-trend-decTrees and DONE!
								#if defined DEBUG_SCC
									taskMessage("I", PREFIX, "Received old physiological data e.g. Na: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Sodium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. K: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Potassium));
									taskMessage("I", PREFIX, "Received old physiological data e.g. Ur: %i. Doing trend analysis", (int)(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO.Urea));
								#endif
								decisionTrees_UrTrnd(&parametersScc, &lastPhysiologicalData.Ur,&(pMsgOLDPhysiologicalData->physiologicalData.ECPDataO), qhDISPin);	
							}
*/							
							sFree(&(Qtoken.pData));; // free the notificaion message
							// do not sFree((void*)pMsgOLDPhysiologicalData); // let it persist for next Trend-Analysis
							break;
						}
						
						default:{
							taskMessage("F", PREFIX, "Reached default in case(command_RequestDatafromDSReady), this should not have happened");
							defaultIdHandling(PREFIX, dataId);
							sFree(&(Qtoken.pData));; // free the notificaion message
							break;
						}		
					} // of switch (dataId) ...
					break;
				} // of case command_RequestDatafromDSReady ...
				case command_UseAtachedMsgHeader: {
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_physicalData): {
							tdMsgPhysicalsensorData *pMsgPhysicalsensorData = NULL;
							pMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
							#if defined DEBUG_SCC || defined SEQ11
								taskMessage("I", PREFIX, "Received physical data. BPSi: %f. Evaluating in Decision Trees.",(float)pMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure);
							#endif
							// now calling respective decTrees to check against parametersScc
							decisionTrees_physical(&parametersScc, pMsgPhysicalsensorData,qhDISPin);
							sFree((void *)&pMsgPhysicalsensorData);
							break;
						}
						case (dataID_actuatorData): {
							tdMsgActuatorData *pMsgActuatorData = NULL;
							pMsgActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
							#if defined DEBUG_SCC || defined SEQ11
								taskMessage("I", PREFIX, "Received actuator data. FLflow: %f. Evaluating in Decision Trees.",(float)pMsgActuatorData->actuatorData.FLPumpData.Flow);
							#endif
							// now calling respective decTrees to check against parametersScc
							decisionTrees_actuator(&parametersScc,pMsgActuatorData,qhDISPin);
							// also store most important actuator values, as the other decision trees need these values!
							lastActuatorData.voltage=pMsgActuatorData->actuatorData.PolarizationData.Voltage;
							lastActuatorData.bPFlow =pMsgActuatorData->actuatorData.BLPumpData.Flow;
							lastActuatorData.fPFlow =pMsgActuatorData->actuatorData.FLPumpData.Flow;
							
							sFree((void *)&pMsgActuatorData);
							break;
						}
						case (dataID_physiologicalData): {
							tdMsgPhysiologicalData *pMsgPhysiologicalData = NULL;
							pMsgPhysiologicalData = (tdMsgPhysiologicalData *) Qtoken.pData;
							#if defined DEBUG_SCC
							taskMessage("I", PREFIX, "Received physiological data e.g. Na: %d.", pMsgPhysiologicalData->physiologicalData.ECPDataO.Sodium);
							#endif
						
							if ((getTime() - timelastStateChange) > (90) )
							{ // only if at least 1.5  minutes have passed in this state, the chemical relations are stable. then check the chemical values with decTrees
								uint8_t getData_err=0;
								#if defined DEBUG_SCC || defined SEQ11
									taskMessage("I", PREFIX, "Received physiological data. ECPI-K: %f. Evaluating in Decision Trees.",(float)pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium);
								#endif
								// now calling respective decTrees to check against parametersScc
								// the decTrees_physiological needs also to know the voltage and flows of both pumps, therefore only start this when physical data already there
								if (lastActuatorData.voltage !=-1 && lastActuatorData.bPFlow !=-1 && lastActuatorData.fPFlow !=-1 ) {
									decisionTrees_physiological(&parametersScc, pMsgPhysiologicalData, ((float)lastActuatorData.fPFlow)/((float)FIXPOINTSHIFT_PUMP_FLOW), ((float)lastActuatorData.voltage)/((float)FIXPOINTSHIFT_POLARIZ_VOLT), &SorDys, qhDISPin);

									if ( staticBufferStateRTB == wakdStates_Ultrafiltration) 
									{ //only if we are in excreation right now, the rend analysis should be performed
									//perform decTrees_xxxTrnd AFTER decTrees_physiological which sets the variable SorDys
										
										// remember actual values for trend analysis;
										lastPhysiologicalData.K =  (uint16_t) pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium;
										lastPhysiologicalData.Ur = (uint16_t) pMsgPhysiologicalData->physiologicalData.ECPDataO.Urea;
										lastPhysiologicalData.pH = (uint16_t) pMsgPhysiologicalData->physiologicalData.ECPDataO.pH;
										lastPhysiologicalData.Na = (uint16_t) pMsgPhysiologicalData->physiologicalData.ECPDataO.Sodium;
										
								// starting Trend Analysis for K+, by requesting data
									// taskMessage("I", PREFIX, "******************************pMsgOLDPhysiologicalData..time preset for debug here**********************************************************************************************!");

										pMsgOLDPhysiologicalData->physiologicalData.TimeStamp= ((int)getTime()) - ((int)parametersScc.KTrdT); // Timestamp of the 'old data' for trend analysis 
/*	Code (trend-checking) skipped, to hinder acess to SD card										
										if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0) {
											getData_err = getDataFromDS(dataID_physiologicalData_K, pMsgOLDPhysiologicalData, qhDISPin);	
											if (getData_err) { // only if read out of old values was sucessful
												#ifdef DEBUG_SCC
													taskMessage("E", PREFIX, "Not performing K trend checking. Data readout request failed");
												#endif
											}
										} else {
											#ifdef DEBUG_SCC
												taskMessage("I", PREFIX, "Not performing trend checking. No data from sufficiently long time ago recorded");
											#endif
										} // of if (pMsgOLDPhysiologicalData->physiologicalData.TimeStamp > 0					
*/									} else {
										#ifdef DEBUG_SCC
											// we do not know how chemical-trends will behave in the device , therefore 
											// we perform trend checking only in Ultrafiltration-mode, where patient's values are 'visible'
											taskMessage("I", PREFIX, "No trend checking, cause not in Ultrafiltration-mode");
										#endif
										lastPhysiologicalData.K = 0;
										lastPhysiologicalData.Ur = 0;
										lastPhysiologicalData.pH = 0;
									} // of if ( staticBufferStateRTB == wakdStates_Ultrafiltration
								} else {
									#ifdef DEBUG_SCC
										taskMessage("I", PREFIX, "Skipping the decision trees evaluation of physiological data, no physical data recieved in this state, yet.");
									#endif
								} // of if (lastActuatorData.voltage !=-1 &&...
							} // of if (getTime() - timelastStateChange >...
							sFree((void *)&pMsgPhysiologicalData);
							break;							
						}
						case (dataID_weightDataOK): {
							tdMsgWeightData *pMsgWeightData = NULL;
							pMsgWeightData = (tdMsgWeightData *) (Qtoken.pData);
							lastWeight_kgrams = ((float)pMsgWeightData->weightData.Weight / (float)FIXPOINTSHIFT_WEIGHTSCALE);
							sFree((void *)&pMsgWeightData);
							#if defined DEBUG_SCC || defined SEQ3
								taskMessage("I", PREFIX, "Received weight measurement: %5.2f kg. Evaluating in Decision Trees.", lastWeight_kgrams );
							#endif
							// now evaluate the actual weight (not the trend) in decision trees
							if (lastWeight_kgrams>0) {
								decisionTrees_Wght(&parametersScc, &lastWeight_kgrams, qhDISPin);
							} else {
								taskMessage("E", PREFIX, "Received weight measurement: %5.2f kg. Seemingly wrong value. Error!", lastWeight_kgrams );
							}
							
							// for thrend analysis: set the timestamp of data you want to recieve!
							uint8_t getData_err=0;
							// taskMessage("I", PREFIX, "******************************last_weitghtime preset for debug here**********************************************************************************************!");
							int last_weitghtime = getTime()-(18*60*60); // last weight measurement at least 18 hours ago
							#if defined DEBUG_SCC || defined SEQ3
								if ((getTime()-(18*60*60)) > 0)	{
									taskMessage("I", PREFIX, "weight time request: %i", last_weitghtime);
								};
							#endif
/*	Code (trend-checking) skipped, to hinder acess to SD card	
							if (last_weitghtime>0) { // only if the calculated time is valid 
							
								pMsgOLDWeightData->weightData.TimeStamp=(uint32_t) last_weitghtime; 							
							
								getData_err = getDataFromDS(dataID_weightDataOK, pMsgOLDWeightData, qhDISPin);

								if (getData_err>0 ) { // check if read out attempt of old values was sucessful
									#ifdef DEBUG_SCC
										taskMessage("E", PREFIX, "Request for weigth data failed");
									#endif
								}
							} else { 
								#ifdef DEBUG_SCC
									taskMessage("I", PREFIX, "Skipping the decision trees evaluation of weight-TREND, no old enough weight measurements available.");
								#endif
							} // of if (last_weitghtime>0)
*/
							break;
						}
						case (dataID_EcgData): {
							tdMsgEcgData *pMsgECGData = NULL;
							pMsgECGData = (tdMsgEcgData *) (Qtoken.pData);
							#ifdef DEBUG_SCC
								taskMessage("I", PREFIX, "Received ECG measurement:%d. Evaluating in Decision Trees.", pMsgECGData->ecgData.heartRate);
							#endif	
							// now calling respective decTrees to check against parametersScc
							// It was decided that the ECG - watching should be handeled by the smartphone exclusively, 
							//as the amount of data is to high to be handeled by the limited ressources of the WAKD
							// decTrees_ECG(parametersScc, pMsgECGData, &Qtoken,qhDISPin);			
							break;
						}
						case (dataID_bpData): {
							tdMsgBpData *pMsgBPData = NULL;
							pMsgBPData = (tdMsgBpData *) (Qtoken.pData);
							#ifdef DEBUG_SCC
								taskMessage("I", PREFIX, "Received BP measurement. BPsys:%d. Evaluating in Decision Trees.", pMsgBPData->bpData.systolic);
							#endif	
							
							// now calling respective decTrees to check against parametersScc
							decisionTrees_BP(&parametersScc, pMsgBPData, qhDISPin);
							sFree((void *)&pMsgBPData);
							break;
						}			
						case (dataID_currentWAKDstateIs) : {
							tdMsgCurrentWAKDstateIs *pMsgCurrentWAKDstateIs  = NULL;
							pMsgCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) (Qtoken.pData);
							
							if  ( ((tdMsgCurrentWAKDstateIs *) (Qtoken.pData))->wakdStateIs != wakdStates_UndefinedInitializing ) {
								// undefinedInitializing-State (implies per se) there is no configuration
								// otherwise:

								tdParametersSCC *pParametersScc_toReconfig;

								pParametersScc_toReconfig = (tdParametersSCC *) pvPortMalloc(sizeof(tdParametersSCC));
								if ( pParametersScc_toReconfig == NULL )
								{	// unable to allocate memory
									taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
									errMsg(errmsg_pvPortMallocFailed);
								}
								pParametersScc_toReconfig->thisState = pMsgCurrentWAKDstateIs->wakdStateIs; // get parameters for this state
							
								#if defined DEBUG_SCC || defined SEQ4
									taskMessage("I", PREFIX, "Received new WAKD state:%i. Trying to reconfigure SCC parameters from Data storage.", pMsgCurrentWAKDstateIs->wakdStateIs);
								#endif								
								// now try to retrieve  new configuraiton from data storage
								Qtoken.command	= command_InitializeSCC;
								Qtoken.pData	= pParametersScc_toReconfig;
								if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) == pdPASS )
								{ // Seemingly we were able to write to the queue. Great!
									#if defined DEBUG_SCC || defined SEQ4
										taskMessage("I", PREFIX, "Request for SCC initialization ID#%i sent. SCC will reconfigure as soon as DS indicates ready with command_RequestDatafromDSReady", Qtoken.command);
									#endif
								} else { // Seemingly the queue is full. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Message is lost!!!");
									#ifndef VP_SIMULATION
										#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
									#endif						
								}
								// We now have to wait for command_InitializeSCCReady from DS that values have been initialized...

								// as WAKD sate has changed, also delete the last remembered measurements, as they are obsolete in the new state
								lastActuatorData.voltage	= -1;
								lastActuatorData.bPFlow		= -1;
								lastActuatorData.fPFlow		= -1;
								
								timelastStateChange = getTime(); // remember the time now, to switch off the chemical decision trees
								
							} else {
								#if defined DEBUG_SCC || defined SEQ0
									taskMessage("I", PREFIX, "Seems we are in wakdStates_UndefinedInitializing. No need to re-configure SCC parameters!" );
								#endif
							}
							break;
							sFree((void *)&pMsgCurrentWAKDstateIs);
						}	
						default:{
							taskMessage("F", PREFIX, "Reached default in case(command_UseAtachedMsgHeader), this should not have happened");
							defaultIdHandling(PREFIX, dataId);
							break;
						}
						// sFree(&(Qtoken.pData));;
						// #ifndef VP_SIMULATION
							// #error(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
						// #endif
						// break;
					}
					break;
				}
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
		} else {
			#if defined DEBUG_SCC
				// taskMessage("I", PREFIX, "Wait on token in input-queue timed out. Continuing endless loop.");
			#endif
		}
                #endif
	}
}


// getDataFromDS(dataID_physiologicalData, pOLDPhysiologicalData, qhDISPin);	// pQH shall always be: qhDISPin
uint8_t getDataFromDS(tdDataId dataId2get, void *pDtaStructure, xQueueHandle *pQH)	// pQH shall always be: pointer to dispatcher queue
{
	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	uint8_t err = 0;
	
	switch (dataId2get) {
		case (dataID_weightDataOK): {
			((tdMsgWeightData *)(pDtaStructure))->header.dataId =dataId2get;
			((tdMsgWeightData *)(pDtaStructure))->header.issuedBy =whoId_MB;
			Qtoken.command	= command_RequestDatafromDS;  // not command_getPhysioPatientData
			Qtoken.pData	= (tdMsgWeightData *) pDtaStructure;
			#if defined DEBUG_SCC || defined SEQ0
				taskMessage("I", PREFIX, "Requested weight data form DS. Trend analysis to come - when DS responded.");
			#endif
			break;
		}
		case dataID_physiologicalData_K:
		case dataID_physiologicalData_Na:
		case dataID_physiologicalData_pH:
		case dataID_physiologicalData_Ur:
		{
			((tdMsgPhysiologicalData *)(pDtaStructure))->header.dataId = dataId2get;
			((tdMsgPhysiologicalData *)(pDtaStructure))->header.issuedBy = whoId_MB;
			Qtoken.command	= command_getPhysioPatientData;  // not command_RequestDatafromDS, as here 
			// we want to obtain 'real patient data' (only visible during ultrafiltraiton
			Qtoken.pData	= (tdMsgPhysiologicalData *) pDtaStructure;
			#ifdef DEBUG_SCC
				taskMessage("I", PREFIX, "Requested physiological data form DS. Trend analysis to come - when DS responded.");
			#endif
			break;
		}
		default:{
			taskMessage("E", PREFIX, "Default reched in switch (dataId2get) in funciton getDataFromDS(). This should not have happened.");
			defaultIdHandling(PREFIX, dataId2get);
			break;
		}
	}
	#if defined DEBUG_SCC || defined SEQ0 || defined SEQ4
		taskMessage("I", PREFIX, "getDataFromDS() sending request to DB for data.");
	#endif
	if ( xQueueSend( pQH, &Qtoken, SCC_BLOCKING) != pdPASS )
	{	// Seemingly the queue is full for too long time. Do something accordingly!
		taskMessage("E", PREFIX, "Queue of DISP did not accept request by getDataFromDS!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree(&(Qtoken.pData));
		err = 1;
	}

	switch (dataId2get) {
		case (dataID_weightDataOK): {
			((tdMsgWeightData *) pDtaStructure)->weightData.Weight =0; 
			#ifdef DEBUG_SCC
				taskMessage("I", PREFIX, "Data readout (time:%i) attempt sent. SCC continuing.", ((tdMsgWeightData *) pDtaStructure)->weightData.TimeStamp);
			#endif
			break;
		}
		case dataID_physiologicalData_K:
		case dataID_physiologicalData_pH:
		case dataID_physiologicalData_Ur:
		{
			#ifdef DEBUG_SCC
				taskMessage("I", PREFIX, "Data readout (time:%i) physiological data attempt sent. SCC continuing.", ((tdMsgPhysiologicalData *) pDtaStructure)->physiologicalData.TimeStamp );
			#endif	
			break;
		}
		default:{
			defaultIdHandling(PREFIX, dataId2get);
			break;
		}		
	}					
								
	return(err);
}
