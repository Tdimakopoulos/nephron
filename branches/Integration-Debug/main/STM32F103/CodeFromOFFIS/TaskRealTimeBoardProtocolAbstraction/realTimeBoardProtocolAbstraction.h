/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef REALTIMEBOARDPROTOCOLABSTRACTION_H
#define REALTIMEBOARDPROTOCOLABSTRACTION_H

#define MICROFLUIDIC_PUTDATA_TIMEOUT    24
#define MICROFLUIDIC_EXECUTION_TIMEOUT  24

#ifdef LINUX
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
	#include "..\\ioHelperFunc.h"
	#include "..\\DummyCode\\getData.h"
#endif
// void readPhysiologicalSensors(tdPhysiologicalSensorReadout *pPhysiologicalSensorReadout);

// void readPhysicalSensors(tdPhysicalSensorReadout *pPhysicalSensorReadout);

void tskRTBPA ( void *pvParameters );
void setActuatorsHW(tdWakdStates stateId);
extern uint8_t OperatingModeACKfromRTMCBInfo(uint32_t);
extern uint8_t OperatingModeEXCfromRTMCBInfo(uint32_t);

#endif
