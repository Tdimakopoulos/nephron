/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifdef LINUX
	#include "dataStorage.h"
	#include "deriveMsgSizeForAktualParameters.h"
	#include "readRequestedParametersFromSD.h"
	#include "writeConfigureParametersToSD.h"
	#include "informConfigurationChanged.h"
	#include "unified_write.h"
	#include "unified_read.h"
	#include "unified_delete.h"
	#include "getphysiopatientdata.h"
	#include "filenames.h"
	#include "typedefsNephron.h"
	#include "enums.h"
	#include "nephron.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
        #include "dataStorage.h"
	#include "deriveMsgSizeForAktualParameters.h"
	#include "readRequestedParametersFromSD.h"
	#include "writeConfigureParametersToSD.h"
	#include "informConfigurationChanged.h"
	#include "fileIO\\unified_write.h"
	#include "fileIO\\unified_read.h"
	#include "fileIO\\unified_delete.h"
	#include "fileIO\\getphysiopatientdata.h"
	#include "fileIO\\filenames.h"
	#include "..\\typedefsNephron.h"
	#include "..\\includes_nephron/enums.h"
	#include "..\\nephron.h"
        #include "..\\..\\main\\main.h"
#endif


#define PREFIX "OFFIS FreeRTOS task DS"

uint8_t heartBeatDS = 0;
uint8_t DS_defaulterror = 0;
uint8_t DS_initFC = 0;


void tskDS( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting Data Storage task!");

	xQueueHandle *qhDISPin  = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhFCin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhFCin;
	xQueueHandle *qhDSin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDSin;
	xQueueHandle *qhSCCin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhSCCin;
  
	// xTaskHandle *handleSCC  = ((tdAllHandles *) pvParameters)->allTaskHandles.handleSCC;
	// xTaskHandle *handleFC	= ((tdAllHandles *) pvParameters)->allTaskHandles.handleFC;
  
 	tdQtoken Qtoken;
	Qtoken.command = command_DefaultError;
	Qtoken.pData   = NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif


	

	
	// these removes are here, because error occurs, when trying to add new data to files, caused by 'rename' function
#ifndef VP_SIMULATION
		#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
#endif
#ifdef VP_SIMULATION
	deleteFiles(FILE_PHYSIOLOGICALDATA, 0, 10);
	deleteFiles(FILE_PHYSICALDATA, 0, 10);
	deleteFiles(FILE_ACTUATORDATA, 0, 10);
	deleteFiles(FILE_WEIGHTDATA, 0, 10);
	deleteFiles(FILE_BPDATA, 0, 10);
	deleteFiles(FILE_ECGDATA, 0, 10);	
	deleteFiles(FILE_DEVICESSTATUS, 0, 10);	
	deleteFiles(FILE_MSGCONFIGURESTATE, 0, 10);		
	taskMessage("I", PREFIX, "ALL DATA-STORAGE FILES FROM PREVIOUS SIMULATION RUN DELETED ");	
#endif

        // NOW Start up SD Card drivers (comes from CSEM, mb_task.c / sdcard.c )
        LOCK_CRITICAL_INTS = 1;
        // efsl library initialization
        SDCARD_PRESENT = SDCARD_NO_THERE;
        while(SDCARD_PRESENT != SDCARD_INSERTED){
            Update_SDCARD_Presence();
        }
        if(SDCARD_PRESENT == SDCARD_INSERTED){
            sdlib_initialised = 0x55;
            while(sdlib_initialised!=0){                
                SD_libInit();
            }
            sdfilecounter = 1;
            //sdlib_initialized = SIGNAL_ON;
        }
        abc = 0;
        LOCK_CRITICAL_INTS = 0;


	for( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every DS_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatDS++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		
		if ( xQueueReceive( qhDSin, &Qtoken, DS_PERIOD) == pdPASS ) {
			#ifdef DEBUG_DS
				taskMessage("I", PREFIX, "Data with id %d in queue.", Qtoken.command);
			#endif

                        
                        
			switch (Qtoken.command) {
				// ##############################################################################
				case command_DefaultError: {
				// ##############################################################################
					// Misused pointer must not be interpreted as such. Cast back to original type.
					tdErrMsg error = (tdErrMsg) Qtoken.pData;
					taskMessage("I", PREFIX, "Received system error msg: %s", errmsgToString(error));
					// Now handle (save) this error message. Needs to be implemented.
					#ifndef VP_SIMULATION
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
					#endif
                                        DS_defaulterror++;
					break;
				}
				
                                
                                // ##############################################################################
				case command_UseAtachedMsgHeader:{
				// ##############################################################################
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case dataID_physiologicalData: {
						// ##############################################################################
							#if defined SEQ6
								printPhysiologicalData(PREFIX, &(((tdMsgPhysiologicalData *) Qtoken.pData)->physiologicalData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "physiological Data received. Storing.");
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
                                                        unified_write(dataID_physiologicalData, &(((tdMsgPhysiologicalData *) Qtoken.pData)->physiologicalData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_physicalData: {
						// ##############################################################################
							#if defined DEBUG_DS || defined SEQ7
								printPhysicalData(PREFIX, &(((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "physical Data timestamp %d received. Storing.", ((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData.TimeStamp);
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_physicalData, &(((tdMsgPhysicalsensorData *) Qtoken.pData)->physicalsensorData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "DONE: storing physical Data.");
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}		
						case dataID_actuatorData: {
						// ##############################################################################
							#if defined SEQ8
								printActuatorData(PREFIX, &(((tdMsgActuatorData *) (Qtoken.pData))->actuatorData));
							#endif
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "actuator Data received. Storing.");
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_actuatorData, &(((tdMsgActuatorData *) (Qtoken.pData))->actuatorData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "DONE: storing actuator Data.");
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}						
						case (dataID_weightDataOK):{
						// ##############################################################################
							// This weight measure was checked by the patient (in ocntrast to "dataID_weightData"). Store the validated measurement.
							#if defined DEBUG_DS || defined SEQ3
								taskMessage("I", PREFIX, "Storing weight measure: %f", ( ((double)(((tdMsgWeightData *)(Qtoken.pData))->weightData.Weight))/FIXPOINTSHIFT_WEIGHTSCALE) );
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_weightDataOK, &(((tdMsgWeightData *) Qtoken.pData)->weightData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_bpData: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "BP Data (ID %i) with timestamp %d received. HR: %d. Storing.", dataId, (((tdMsgBpData *) Qtoken.pData)->bpData),(((tdMsgBpData *) Qtoken.pData)->bpData.heartRate) );
							#endif
							#ifndef DISABLE_SD_WRITE_IN_DS
                                                        unified_write(dataID_bpData, &(((tdMsgBpData *) Qtoken.pData)->bpData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_EcgData: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "ECG Data received. Storing.");
							#endif
							#ifndef DISABLE_SD_WRITE_IN_DS
                                                        unified_write(dataID_EcgData, &(((tdMsgEcgData *) Qtoken.pData)->ecgData));
							#endif
                                                        sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}		
						case dataID_statusRTB:{
						// ##############################################################################
							
                                                        tdMsgDeviceStatus *pMsgDeviceStatus  = NULL;
                                                        pMsgDeviceStatus  = (tdMsgDeviceStatus *) (Qtoken.pData);
							
                                                        // Our queue contains a pointer to status data. The memory was allocated by RTBPA
							#if defined DEBUG_DS || defined SEQ7
								taskMessage("I", PREFIX, "Status Data timestamp %d received. Storing.", ((tdMsgDeviceStatus *) Qtoken.pData)->devicesStatus.TimeStamp) ;
							#endif
							
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
                                                          unified_write(dataID_statusRTB, &(((tdMsgDeviceStatus *) Qtoken.pData)->devicesStatus));
							#endif
                                                        
                                                        sFree((void *)&(Qtoken.pData));
							
                                                        #if defined DEBUG_DS || defined SEQ7
								taskMessage("I", PREFIX, "DONE: storing Status Data.") ;
							#endif
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							
							break;
						}		
						case dataID_configureStateDump: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "dataID_configureStateDump Data received. Storing.");
							#endif
                                                        
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_configureStateDump, ((tdMsgConfigureState *) Qtoken.pData) );
							#endif
                                                        
                                                        sFree((void *)&(Qtoken.pData));
							#ifndef VP_SIMULATION
								#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
							#endif
							break;
						}
						case dataID_PatientProfile: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "Patient Profile Data received. Storing.");
							#endif
							
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
                                                        unified_write(dataID_PatientProfile, &(((tdMsgPatientProfileData *) Qtoken.pData)->patientProfile));
							if ( ((tdMsgPatientProfileData *)(Qtoken.pData))->header.issuedBy != whoId_MB) // only if this is not coming from task FC anyway
							{
								if (informConfigurationChanged(dataID_PatientProfile, qhDISPin, PREFIX))
								{
									taskMessage("E", PREFIX, "Unable to inform system about changed configuration!");
								}
							}
                                                        #endif
							sFree((void *)&(Qtoken.pData));
							break;
						}
						case dataID_WAKDAllStateConfigure: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "WAKDAllStateConfigure received. Storing.");
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_WAKDAllStateConfigure, &(((tdMsgWAKDAllStateConfigure *) Qtoken.pData)->WAKDAllStateConfigure));		
							unified_write(dataID_PatientProfile, &(((tdMsgWAKDAllStateConfigure *) Qtoken.pData)->WAKDAllStateConfigure.patientProfile));
							if ( ((tdMsgWAKDAllStateConfigure *)(Qtoken.pData))->header.issuedBy != whoId_MB) // only if this is not coming from task FC anyway
							{
								if (informConfigurationChanged(dataID_WAKDAllStateConfigure, qhDISPin, PREFIX))
								{
									taskMessage("E", PREFIX, "Unable to inform system about changed configuration!");
								}
							}
                                                        #endif
							sFree((void *)&(Qtoken.pData));
							break;
						}
						case dataID_configureState: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "MsgWAKDStateConfigurationParameters received. Storing.");
							#endif		

                                                        #ifndef DISABLE_SD_WRITE_IN_DS                                                                                        
							unified_write(dataID_configureState, &(((tdMsgWAKDStateConfigurationParameters *) Qtoken.pData)->WAKDStateConfigurationParameters));
							if ( ((tdMsgWAKDStateConfigurationParameters *)(Qtoken.pData))->header.issuedBy != whoId_MB) // only if this is not coming from task FC anyway
							{
								if (informConfigurationChanged(dataID_WAKDAllStateConfigure, qhDISPin, PREFIX))
								{
									taskMessage("E", PREFIX, "Unable to inform system about changed configuration!");
								}
							}
                                                        #endif
							sFree((void *)&(Qtoken.pData));
							break;
						}
						case dataID_AllStatesParametersSCC: {
						// ##############################################################################
							#if defined DEBUG_DS	
								taskMessage("I", PREFIX, "now sending command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_configureState to SCC to make the new setting work!");
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_AllStatesParametersSCC, &(((tdMsgWAKDStateConfigurationParameters *) Qtoken.pData)->WAKDStateConfigurationParameters));
							if (informConfigurationChanged(dataID_StateParametersSCC, qhDISPin, PREFIX))
							{
								taskMessage("E", PREFIX, "Unable to inform system about changed configuration!");
							}
                                                        #endif
							sFree((void *)&(Qtoken.pData));
							break;
						}				
						case dataID_StateParametersSCC: {
						// ##############################################################################
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "now sending command_ConfigurationChanged with tdMsgOnly->header.dataID= dataID_configureState to SCC to make the new setting work!");
							#endif
                                                        #ifndef DISABLE_SD_WRITE_IN_DS
							unified_write(dataID_StateParametersSCC, &(((tdMsgParametersSCC *) Qtoken.pData)->parametersSCC) );
							if (informConfigurationChanged(dataID_StateParametersSCC, qhDISPin, PREFIX))
							{
								taskMessage("E", PREFIX, "Unable to inform system about changed configuration!");
							}
                                                        #endif
							sFree((void *)&(Qtoken.pData));
							break;
						}
						case (dataID_CurrentParameters): {
						// ##############################################################################
							// Somebody (probably the smartphone) wants to know the current
							// parameters of WAKD configuration. Since this message can be
							// of variable size we need to compute this first.

							// The header information is of 6 bytes. The remainder is the state and names for the parameters
							// to receive.
							// The first byte after the header defines the state for which the parameters
							// should be received.

							// How many parameters are requested? First 6 bytes are the msgHeader
							// another byte is for defining the relevant WAKD state. So the names
							// of relevant parameters are listed after byte 7.
							uint16_t numberParameter = ((tdMsgOnly *)(Qtoken.pData))->header.msgSize-7;
							#if defined DEBUG_DS || defined SEQ12
								taskMessage("I", PREFIX, "Number of parameters to process: %d", numberParameter);
							#endif

							// The function deriveMsgSize does not know about the length of the string "patient name"
							// So initially this function assumes the maximum length of 50 bytes. This will be corrected
							// later by "readRequestedParameters". In the meantime we waste some bytes ... so be it.
							uint16_t msgSize = deriveMsgSizeForAktualParameters(numberParameter, Qtoken.pData, PREFIX);

							// Allocate the memory of the computed size
							uint8_t *pMsg = NULL;
							pMsg = pvPortMalloc(msgSize);
							if ( pMsg == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "'dataID_CurrentParameters': Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							}
							#if defined DEBUG_DS || defined SEQ12
								taskMessage("I", PREFIX, "Allocated bytes: %d", msgSize);
							#endif

							// Initialize the header information
							tdMsgOnly *pMsgOnly = (tdMsgOnly *)pMsg;
							pMsgOnly->header.recipientId	= whoId_SP;
							pMsgOnly->header.issuedBy		= whoId_MB;
							pMsgOnly->header.dataId			= dataID_ActualParameters;
							pMsgOnly->header.msgSize		= msgSize + 6 - sizeof(tdMsgOnly);	// correct size due to alignment and padding.

							// If patient name is part of the requested parameters the string at this point
							// is allocated with max 50 bytes. This will be corrected to the real number of chars
							// in the following.
							uint8_t err = readRequestedParametersFromSD(numberParameter, Qtoken.pData, PREFIX, pMsg);

							// Free old received message no longer needed.
							sFree((void *)(&(Qtoken.pData)));

							if (err)	// was "readRequestedParametersFromSD" successfull or not?
							{	// something went wrong. Terminate and inform system
								sFree((void *)(&pMsg));
								// It is not necessary to send errMsg here, since the function that has failed already did so.
								// errMsg(errmsg_ReadingSDcardFailed)
							} else {
								// The msg is complete.
								#if defined DEBUG_DS || defined SEQ12
									int i = 0;
									for (i=0; i<pMsgOnly->header.msgSize+2; i++)
									{
										taskMessage("I", PREFIX, "Byte %d: %d (hex %x)", i, *(pMsg+i), *(pMsg+i));
									}
								#endif
								// Return answer to sender.
								Qtoken.command	= command_UseAtachedMsgHeader;
								Qtoken.pData	= (void *) pMsg;

								if ( xQueueSend( qhDISPin, &Qtoken, DS_BLOCKING) != pdPASS )
								{	// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of DISP did not accept 'command_UseAtachedMsgHeader' with dataID_ActualParameters!");
									errMsg(errmsg_QueueOfDispatcherTaskFull);
									sFree(&(Qtoken.pData));
								};
							}
							break;
						}
						case (dataID_ConfigureParameters): {
						// ##############################################################################
							// Somebody (probably the smartphone) needs to reconfigure some parameters of the
							// WAKD. Extract these from the message and write them to the SD card.
                                                        
                                                        #ifndef DISABLE_SD_CARD_WRITE
							uint8_t err = writeConfigureParametersToSD(Qtoken.pData, qhDISPin, PREFIX);
                                                        #endif
							// Free old received message no longer needed.
							sFree((void *)(&(Qtoken.pData)));
							break;
						}
						case (dataID_shutdown): // fall through, continue with reset.
						case (dataID_reset):{
                                                  #if defined DEBUG_CBPA || defined SEQ0
                                                          taskMessage("I", PREFIX, "Shutdown of task.");
                                                  #endif
                                                  #ifdef CROSSPRINTF
                                                      debug_printf("DS: Shutdown of task.\n");
                                                  #endif
                                                  // Do here whatever needs to be done for shutting down.
                                                  ((tdAllHandles *) pvParameters)->allTaskHandles.handleDS = NULL;
                                                  vTaskDelete(NULL);	// Shut down and wait forever.
                                                  vTaskSuspend(NULL); // Never run again (until after reset)
                                                  break;
						}
						default:{
                                                        #ifdef CROSSPRINTF
                                                            debug_printf("DS: Defaulthandling for dataId %d...\n", dataId);
                                                        #endif
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				case command_RequestDatafromDS: {
					// Look at header for further information.
                                        #ifndef DISABLE_DATA_REQ_SD
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){				
						case dataID_physiologicalData: {
							unified_read(dataID_physiologicalData, &(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData));
							respondRequestDatafromDS(&dataId,  qhDISPin);							
							break;
						}
						case dataID_physicalData: {
							unified_read(dataID_physicalData, &(((tdMsgPhysicalsensorData *)Qtoken.pData)->physicalsensorData));
							#ifdef DSTEST
								respondRequestDatafromDS(&dataId,  qhDISPin);
							#endif
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading dataID_physicalData complete");
							#endif
							break;
						}
						case dataID_actuatorData: {
							unified_read(dataID_actuatorData, &(((tdMsgActuatorData *)Qtoken.pData)->actuatorData));
							#ifdef DSTEST
								respondRequestDatafromDS(&dataId,  qhDISPin);
							#endif
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading dataID_actuatorData complete.");
							#endif
							break;
						}	
						case (dataID_weightDataOK):{
							#if defined DEBUG_DS
								taskMessage("I", PREFIX, "Request for dataID_weightDataOK recieved");
							#endif
							int readouterr = unified_read(dataID_weightDataOK, &(((tdMsgWeightData *)Qtoken.pData)->weightData));
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading dataID_weightDataOK complete");
							#endif
							if ( ((tdMsgWeightData *)Qtoken.pData)->weightData.Weight >0 
									&& readouterr==0) { // only if data from the time requested or older was found
								taskMessage("I", PREFIX, "Weigth %d read out for time %d",
									((tdMsgWeightData *)Qtoken.pData)->weightData.Weight, ((tdMsgWeightData *)Qtoken.pData)->weightData.TimeStamp);
								//if there was found a valid weight, do respondRequestDatafromDS so SCC will perform Trend-decision-Trees
								respondRequestDatafromDS(&dataId, qhDISPin);
							}
							break;
						}
						case dataID_bpData: {
							if(!unified_read(dataID_bpData, &(((tdMsgBpData *)Qtoken.pData)->bpData)))
							{
								respondRequestDatafromDS(&dataId,  qhDISPin);
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_bpData complete.");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_bpData failed!");
								#endif
							}
							break;
						}
						case dataID_EcgData: {
							if(!unified_read(dataID_EcgData, &(((tdMsgEcgData *)Qtoken.pData)->ecgData)))
							{
								respondRequestDatafromDS(&dataId,  qhDISPin);
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_EcgData complete");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_EcgData failed!");
								#endif
							}
							break;
						}
						case dataID_statusRTB: {
							if(!unified_read(dataID_statusRTB, &(((tdMsgDeviceStatus *)Qtoken.pData)->devicesStatus) ))
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_statusRTB complete");
								#endif
								respondRequestDatafromDS(&dataId,  qhDISPin);								
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_statusRTB failed!");
								#endif
							}
							break;
						}
						case dataID_PatientProfile: { 
							if(!unified_read(dataID_PatientProfile, &(((tdMsgPatientProfileData *)Qtoken.pData)->patientProfile)))
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_PatientProfile %s from file complete",(((tdMsgPatientProfileData *)Qtoken.pData)->patientProfile.name) );
								#endif
								respondRequestDatafromDS(&dataId,  qhDISPin);
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_PatientProfile failed!");
								#endif
							}
							break;
						}
						case dataID_WAKDAllStateConfigure: {
							if(!unified_read(dataID_WAKDAllStateConfigure, &(((tdMsgWAKDAllStateConfigure *)Qtoken.pData)->WAKDAllStateConfigure)) )
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_WAKDAllStateConfigure from file almost complete. Now reading in dataID_PatientProfile.");
								#endif
								if (!unified_read(dataID_PatientProfile, &(((tdMsgWAKDAllStateConfigure *)Qtoken.pData)->WAKDAllStateConfigure.patientProfile)) ) 
								{
									#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_WAKDAllStateConfigure dataID_PatientProfile from file complete");
									#endif
									respondRequestDatafromDS(&dataId,  qhDISPin);
								} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_WAKDAllStateConfigure including dataID_PatientProfile failed!");
								#endif
								}
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_WAKDAllStateConfigure including dataID_PatientProfile failed!");
								#endif
							}
							break;
						}							
						
						case dataID_configureState: { //HINT: the processed type is tdWAKDStateConfigurationParameters
							if(!unified_read(dataID_configureState, &(((tdMsgWAKDStateConfigurationParameters *)Qtoken.pData)->WAKDStateConfigurationParameters)))
							{
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading in dataID_configureState from file complete");
								#endif
                                                                 #ifdef CROSSPRINTF
                                                                   debug_printf("Reading in dataID_configureState from file complete\n" );
                                                                #endif
								respondRequestDatafromDS(&dataId,  qhDISPin);
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_configureState failed!");
								#endif
                                                                 #ifdef CROSSPRINTF
                                                                   debug_printf("Reading in dataID_configureState from file FAILED\n" );
                                                                #endif
							}
							break;
						}	
						// decided not to implement the IO-handling of the whole set of parameters, but to do it for each state seperately!!!
						// case dataID_AllStatesParametersSCC: {
						//tdAllStatesParametersSCC
							// if(!unified_read(dataID_AllStatesParametersSCC, &(((???MsgNotNeededSoNotDefined??? *)Qtoken.pData)->???)))
							// {
								// respondRequestDatafromDS(&dataId,  qhDISPin);
								// #ifdef DEBUG_DS
									// taskMessage("I", PREFIX, "Reading in dataID_AllStatesParametersSCC %s from file complete");
								// #endif
							// } else {
								// #ifdef DEBUG_DS
									// taskMessage("F", PREFIX, "Reading dataID_AllStatesParametersSCC failed!");
								// #endif
							// }
							// break;
						// } 
						case dataID_StateParametersSCC: {
						//tdParametersSCC
							#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "Attempt to read out dataID_StateParametersSCC.");
							#endif
							if(!unified_read(dataID_StateParametersSCC, &(((tdMsgParametersSCC *)Qtoken.pData)->parametersSCC)))
							{
								#ifdef DSTEST
									respondRequestDatafromDS(&dataId,  qhDISPin);
								#endif
								#ifdef DEBUG_DS
									taskMessage("I", PREFIX, "Reading dataID_StateParametersSCC complete.");
								#endif
							} else {
								#ifdef DEBUG_DS
									taskMessage("F", PREFIX, "Reading dataID_StateParametersSCC failed!");
								#endif
							} // of if(!unified_read(dataID_Stat...
							break;
						}
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}				
					}
					#else
                                        
                                          sFree((void *)&(Qtoken.pData));
                                        
                                        #endif
                                        
                                        break;
                                        
				}
                                

				case command_InitializeFC: {
                                        DS_initFC++;
					tdFCConfigPointerStruct *pFCConfigPointerStruct = ((tdFCConfigPointerStruct *)(Qtoken.pData));
					#if defined DEBUG_DS || defined SEQ0
						taskMessage("I", PREFIX, "Request to initialize FC. Reading %s parameters from SD Card.", stateIdToString((pFCConfigPointerStruct->pWAKDStateConfigure)->thisState)  );
					#endif
                                        #ifdef CROSSPRINTF
                                            debug_printf("Request to initialize FC. Reading %s parameters from SD Card.\n", stateIdToString((pFCConfigPointerStruct->pWAKDStateConfigure)->thisState)  );
                                        #endif
	
					// The FC task sent this token and with it in pData the pointer of its own data structure
					// where it expects the data to be written to. So DO NOT use vPortFree on this pointer!

                                        
                                        //# ifndef VP_SIMULATION
                                        
                                        #ifdef DUMMY_READ_SD

                                          // INTEGRATION 20120928 - Initialize FC done here manually (not yet reading from SD)

                                          strncpy(((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->name, "Hans Smith", 10);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->name[10] = '\0'; // null-terminate

                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->gender = (char) "m"; 
                                          //30	860	860	12	41	43	17	105	133	34	0	60	0	200	CRC:e7a5

                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->measureTimeHours = 8; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->measureTimeMinutes = 30; // parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->wghtCtlTarget = 860; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->wghtCtlLastMeasurement = 860; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->wghtCtlDefRemPerDay = 12; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->kCtlTarget = 41; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->kCtlLastMeasurement = 43; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->kCtlDefRemPerDay = 17; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->urCtlTarget = 105; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->urCtlLastMeasurement = 133;//parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->urCtlDefRemPerDay = 34; //parseLine(&ptr,end);
                                          
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->minDialPlasFlow = 0; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->maxDialPlasFlow = 60; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->minDialVoltage = 0; //parseLine(&ptr,end);
                                          ((tdPatientProfile*)pFCConfigPointerStruct->pPatientProfile)->maxDialVoltage = 200; //parseLine(&ptr,end);

                                          #ifdef CROSSPRINTF
                                              debug_printf("Reading in dataID_PatientProfile %s from file complete\n",(pFCConfigPointerStruct->pPatientProfile)->name );
                                          #endif

                                          // Read stateconfiguration FIRST LINE
                                          uint8_t current_state = 0;
                                          current_state = 1;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 0;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 0;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 0;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 1;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 0;
                                          }
                                          current_state = 4;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 100;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 50;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 40;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 1;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 19800;
                                          }
                                          current_state = 5;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 100;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 40;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 40;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 0;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 300;
                                          }
                                          
                                          current_state = 6;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 100;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 30;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 0;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 0;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 600;
                                          }
                                          current_state = 7;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 100;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 40;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 40;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 1;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 300;
                                          }
                                          current_state = 2;
                                          if(current_state ==(((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 0;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 0;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 0;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 0;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 0;
                                          }
                                          current_state = 3;
                                          if(current_state == (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState){
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->thisState = current_state;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedBp_mlPmin = 100;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defSpeedFp_mlPmin = 0;	
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->defPol_V = 0;			
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->direction = 1;
                                            (((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure)->duration_sec = 0;
                                          }

                                          //NOW RETURN READ DATA TO FC
                                          Qtoken.pData = (void *) pFCConfigPointerStruct; // send back the same pointer we got from FC to FC (FC has no local copy!)!
                                          Qtoken.command = command_InitializeFCReady;
                                          
                                          if ( xQueueSend( qhFCin, &Qtoken, DS_BLOCKING) != pdPASS )
                                          {	// Seemingly the queue is full for too long time. Do something accordingly!
                                                  taskMessage("E", PREFIX, "Queue of FC did not accept 'command_InitializeFCReady'!");
                                                  errMsg(errmsg_QueueOfFlowControlTaskFull);
                                                  sFree(&(Qtoken.pData));
                                          };
                                        #else
                                        

					if(!unified_read(dataID_PatientProfile,pFCConfigPointerStruct->pPatientProfile))
					{ // Patient profile read..
						#ifdef DEBUG_DS
							taskMessage("I", PREFIX, "Reading in dataID_PatientProfile %s from file complete",(pFCConfigPointerStruct->pPatientProfile)->name );
                                                        #ifdef CROSSPRINTF
                                                            debug_printf("Reading in dataID_PatientProfile %s from file complete\n",(pFCConfigPointerStruct->pPatientProfile)->name );
                                                        #endif
                                                #endif
						
                                                if(!unified_read(dataID_configureState, ((tdFCConfigPointerStruct *)(Qtoken.pData))->pWAKDStateConfigure))
						{// State config read
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading in of dataID_configureState %s complete", stateIdToString( (pFCConfigPointerStruct->pWAKDStateConfigure)->thisState) );
								taskMessage("I", PREFIX, "Duration preset in state: %i", (pFCConfigPointerStruct->pWAKDStateConfigure)->duration_sec );
                                                                #ifdef CROSSPRINTF
                                                                  debug_printf("DS: Reading in of dataID_configureState %s complete\n", stateIdToString( (pFCConfigPointerStruct->pWAKDStateConfigure)->thisState) );
                                                                  //debug_printf("DS: Duration preset in state: %i\n", (pFCConfigPointerStruct->pWAKDStateConfigure)->duration_sec );
                                                              #endif
							#endif
							#if defined DEBUG_DS || defined SEQ0 || defined DEBUG_FC
								taskMessage("I", PREFIX, "FC was initialized. Notifying task FC.");
							#endif
							
							Qtoken.pData = (void *) pFCConfigPointerStruct; // send back the same pointer we got from FC to FC (FC has no local copy!)!
							Qtoken.command = command_InitializeFCReady;
							
							if ( xQueueSend( qhFCin, &Qtoken, DS_BLOCKING) != pdPASS )
							{	// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of FC did not accept 'command_RequestDatafromDSReady'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
								sFree(&(Qtoken.pData));
							};
							// sending command_RequestDatafromDSReady  END
						} else { // Errors occurred during read of dataID_configureState
							taskMessage("E", PREFIX, "Errors occurred during read of dataID_WAKDAllStateConfigure");
							errMsg(errmsg_SDcardError);
						}
					 
                                        } else { // Errors occurred during read of dataID_PatientProfile
						taskMessage("E", PREFIX, "Errors occurred during read of dataID_PatientProfile");
						errMsg(errmsg_SDcardError);
					}
                                        // #endif // VP_SIMULATION
                                        
                                        #endif // DUMMY_READ_SD

					#ifndef VP_SIMULATION
						// HANDLE CASE INITILIZATION FAILS
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
					#endif
					break;
				}
                                
				case command_InitializeSCC: {
					#ifdef DEBUG_DS
						taskMessage("I", PREFIX, "Request to initialize SCC. Reading parameters from SD Card.");
					#endif	

                                        //INTEGRATION
                                        sFree(&(Qtoken.pData));
                                        break;

					tdParametersSCC *pParametersSCC = (tdParametersSCC *)Qtoken.pData;
					if(!unified_read(dataID_StateParametersSCC, pParametersSCC))
					{
						#if defined DEBUG_DS || defined DEBUG_SCC
							taskMessage("I", PREFIX, "Reading in of dataID_StateParametersSCC complete. Notifying task SCC.");
						#endif
						// sending command_InitializeSCCReady, to inform that new configuration was read out and is available by FC
						Qtoken.pData = (void *) pParametersSCC; // send back the very same pointer (command for clarification!)
						Qtoken.command = command_InitializeSCCReady;
						if ( xQueueSend( qhSCCin, &Qtoken, DS_BLOCKING) != pdPASS )
						{	// Seemingly the queue is full for too long time. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of SCC did not accept 'command_InitializeSCCReady'!");
							errMsg(errmsg_QueueOfFlowControlTaskFull);
							sFree(&(Qtoken.pData));
						};
						// sending command_InitializeSCCReady  END
					} else { // Errors occurred during read
						taskMessage("E", PREFIX, "Errors occurred during read of dataID_StateParametersSCC");
						errMsg(errmsg_SDcardError);
					}
					#ifndef VP_SIMULATION
						// HANDLE CASE INITILIZATION FAILS
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
					#endif
					break;
				}
                                /*
				case command_getPhysioPatientData: {
					tdDataId dataId = ((tdMsgPhysiologicalData *)(Qtoken.pData))->header.dataId;	
					#if defined DEBUG_DS
						taskMessage("I", PREFIX, "Attempt to read out PhysioPatientData with Timestamp %i", (((tdMsgPhysiologicalData *)(Qtoken.pData))->physiologicalData.TimeStamp) );
					#endif
					if (((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp >= 1) {
#warning set 120 back to 60
						if(!getPhysioPatientData( &(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData), 60)) // last atribute is duraiton of UF-state (2*60=120seconds)
						// we only see 'real' patient's values, when the UF state lasted enough long time!
						{
							// #ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Reading in of command_getPhysioPatientData complete");
								taskMessage("I", PREFIX, "read old physiological data e.g. Na: %i",(int)(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.ECPDataO.Sodium));
								taskMessage("I", PREFIX, "read old physiological data e.g. K: %i", (int)(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.ECPDataO.Potassium));
								taskMessage("I", PREFIX, "read old physiological data e.g. Ur: %i",(int)(((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.ECPDataO.Urea));
							// #endif
							
							//if there was found a valid weight, do respondRequestDatafromDS so SCC will perform Trend-decision-Trees
							respondRequestDatafromDS(&dataId, qhDISPin);
							
						} else { // Errors occurred during read
							#ifdef DEBUG_DS
								taskMessage("I", PREFIX, "Not possible to read desired dataID_physiologicalUFData. Indicating by setting timestamp '0' ");
							#endif
							((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp =0;
							// errMsg(errmsg_SDcardError); no errMSG here! probably just the requested data is not in the file - thats ok! jsut no trend-observation will be done
						}
					} else {
						taskMessage("I", PREFIX, "Not possible to read out dataID_physiologicalUFData with Timestamp < 1");
						((tdMsgPhysiologicalData *)Qtoken.pData)->physiologicalData.TimeStamp =0;
						errMsg(errmsg_SDcardError);
					}
					#ifndef VP_SIMULATION
						#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). SD card IO needs implementation for real HW!)
					#endif

					break;
				}*/
				default: {
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
                                        #ifdef CROSSPRINTF
                                            debug_printf("DS: Default handling Qtoken.command %d...\n", Qtoken.command);
                                        #endif
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}

                        
                //sFree((void *)&(Qtoken.pData));

		} else {
			taskMessage("E", PREFIX, "Did not get anything to do for a loooong time. This is most likely faulty behavior!");
		}
	}
}

uint8_t respondRequestDatafromDS(tdDataId *dataId2get, xQueueHandle *pQH)
{
	tdQtoken Qtoken;
	Qtoken.command = command_RequestDatafromDSReady;
	uint8_t err = 1;

	tdMsgSystemInfo *pMsgRespondRequest = NULL; // fetch an old measurement form Data Storage
	pMsgRespondRequest = (tdMsgSystemInfo *) pvPortMalloc(sizeof(tdMsgSystemInfo));		
	if ( pMsgRespondRequest == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "respondRequestDatafromDS: Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	} else {
		pMsgRespondRequest->header.recipientId = whoId_MB;
		pMsgRespondRequest->header.msgSize	= sizeof(tdMsgSystemInfo);
		pMsgRespondRequest->header.dataId = *dataId2get;
		pMsgRespondRequest->header.msgCount	= 0; // not defined; unused
		pMsgRespondRequest->header.issuedBy	= whoId_MB;	
		pMsgRespondRequest->msgEnum = command_RequestDatafromDSReady;
		
		Qtoken.pData	= (void *) pMsgRespondRequest;
		#if defined DEBUG_DS
			taskMessage("I", PREFIX, "Requested for data form DS processed. Sening command_RequestDatafromDSReady.");
		#endif
		if ( xQueueSend( pQH, &Qtoken, DS_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Data Storage Message is lost!!!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pMsgRespondRequest);
			err = 1;
		} else {
			err = 0;
		}
		#if defined DEBUG_DS
			taskMessage("I", PREFIX, "Command_RequestDatafromDSReady sent.");
		#endif
	} // of if ( pMsgRespondRequest == NULL )
	
	return (err);
}

