 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <stdarg.h>

#ifdef LINUX
	#include "nephron.h"
	#include "global.h"
	#include "crc16.h"
	#include "csvparselib.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\..\\nephron.h"
	#include "..\\..\\includes_nephron\\global.h"
	#include "crc16\\crc16.h"
	#include "csvparselib\\csvparselib.h"
#endif

#include <limits.h>
#include <ctype.h>

#include "filenames.h"

#include "unified_read.h"
#include "unified_write.h"

#include "getphysiopatientdata.h"

#define PREFIX "OFFIS FreeRTOS task DS - getPhysioPatientData"

uint8_t getPhysioPatientData(tdPhysiologicalData* db, tdUnixTimeType duration)
{

	tdPhysiologicalData* lastValue = pvPortMalloc(sizeof(tdPhysiologicalData));
	if (lastValue==NULL)
	{
		// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		return 1;
	}
	if(duration == 0)
	{
		taskMessage("E", PREFIX, "You cannot check for a duration of 0.");
		return 1;
	}
	char* ptr = NULL;
	char* end = NULL;
	uint32_t lastLF;
	uint8_t lastValueLatest = 0; 
	tdUnixTimeType ultrafiltrationDuration = 0;
	char filename[FILENAME_LENGTH];
	
	char dataBuffer [DBUFFER_CSVLINE_SIZE]; // memory for reading in a maximum of DBUFFER_CSVLINE_SIZE chars
	//taskMessage("I", PREFIX, "db->TimeStamp = %lu",db->TimeStamp);
	tdUnixTimeType TimeStamp = db->TimeStamp;
	//taskMessage("I", PREFIX, "TimeStamp = %lu",TimeStamp);
	//uint32_t dabbeldu = db->TimeStamp;
	
	int i;

#if defined VP_SIMULATION
	// Search in 3 files
	for(i = 0; i < 3;++i)
	{	
		strncpy(filename,FILE_PHYSIOLOGICALDATA,FILENAME_LENGTH);
		/*
			calculate filename offset, normally we won't get values below 0 (so before 1970)
			but for testing purposes this could happen and the calculation would fail.
		*/
		tdUnixTimeType dateToFile;
		if((int32_t)(db->TimeStamp - (MAX_TIMESTAMPS_IN_FILE * i)) >= 0)
		{
			dateToFile = db->TimeStamp - (MAX_TIMESTAMPS_IN_FILE * i);
		}
		else
		{
			dateToFile = 0;
		}
		if(changeFileName(filename, dateToFile))
		{
			taskMessage("F", PREFIX, "Error while creating filename: %s", filename);
			sFree((void *)&lastValue);
			return 1;
		}	
		#ifdef DEBUG_DS
			taskMessage("I", PREFIX, "Opening file %s",filename);
		#endif	
		FILE* pFile = fopen(filename, "rb+");
		if (pFile == NULL)
		{
			taskMessage("F", PREFIX, "Error opening file %s", filename);
			sFree((void *)&lastValue);
			return 1;
		}
		fseek (pFile, 0, SEEK_END);	// jump to the end of file
		movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
		while(!seekToLastLF(pFile))
		{
			/*	Seek backwards the begin of the line - and break if we reach the first byte.
				Because we don't have negative timestamps and it will return with "nothing found"
				anyway if timestamp 0 doesn't match, this probably will never be reached. Just
				to make sure that we won't try to parse the header description line, we
				check it.
			*/
			lastLF = ftell(pFile); // remember position of the last line
			fgets (dataBuffer ,DBUFFER_CSVLINE_SIZE , pFile); // read in a whole line
			/*
				ptr is used to tell parseLine where to start 
				and save the current pointer position for every parse.
			*/
			ptr = dataBuffer;	
			/*
				Parse, parse, parse all values. 
			*/	
			ReadInMemberValues(dataID_physiologicalData, db, &ptr, end, 0);	
			/*
				no &ptr here because we want to pass by value and use the unchanged ptr
				in case of a CRC error to get to CRC: without seeking again.
			*/
			if(verifyCRC(ptr, end, dataBuffer, DBUFFER_CSVLINE_SIZE))
			{
				taskMessage("F", PREFIX, "CRC error while reading line with timestamp %i", db->TimeStamp);
				fclose(pFile);
				sFree((void *)&lastValue);
				return 1;
			}
			/*
				correct state, and a timestamp which is less than the passed one:
				lets count and save the first values found
			*/
			if(TimeStamp >= db->TimeStamp && db->currentState == wakdStates_Ultrafiltration) 
			{
				if(lastValueLatest == 0)
				{
					lastValueLatest = 1;
					memcpy(lastValue, db, sizeof(tdPhysiologicalData));
				}
				/*
					calculate the difference between the latest timestamp and the current one
					and voil�, we got the new duration of the ultrafiltration state 
				*/
				ultrafiltrationDuration = lastValue->TimeStamp - db->TimeStamp;
			}
			else // state has changed, so we need a new latest value - actual solution is to exit
			{
				lastValueLatest = 0;
				/*taskMessage("I", PREFIX, "State has changed, so nothing was found");
				fclose(pFile);
				sFree((void *)&lastValue);
				return 1;*/
			}
			if(ultrafiltrationDuration >= duration) // success
			{
				*db = *lastValue;
				#ifdef DEBUG_DS
					taskMessage("I", PREFIX, "Proof at timestamp %lu that the last ultrafiltration state is lasting at least %lu sec", (int)db->TimeStamp, (int)duration);
				#endif	
				fclose(pFile);
				sFree((void *)&lastValue);
				return 0;
			}
			/*
				move up in the file because we haven't found yet what we're looking for.
				if we reach the header the while loop will stop and return an error.
			*/
			fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); // rewind to remembered position
			movebeforeCRLF(pFile); // bypass CR/LF of the previous(filewise, logically next) line
		}
		fclose(pFile);
	}
	#ifdef DEBUG_DS
		taskMessage("I", PREFIX, "Reached header of the file, so nothing was found");
	#endif
	sFree((void *)&lastValue);
#endif
	return 1;
}
