 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef UNIFIED_WRITE_H
#define UNIFIED_WRITE_H

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#ifdef LINUX
	#include "enums.h"
	#include "interfaces.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\..\\includes_nephron\\enums.h"
	#include "..\\..\\includes_nephron\\interfaces.h"
#endif

// for testing purposes
//#ifdef __arm__ 
//	#include "csvparselib/pstdint.h"
//	#include "..\\..\\includes_nephron\\enums.h"
//	#include "..\\..\\typedefsNephron.h"
//#else
//	#include <stdint.h>
//	#include "..\enums.h"
//	#include "..\typedefsNephron.h"
//#endif

#define SELECT_NOT_FLT FLT_MIN		//!< Used to tell that a float value shouldn't be written.
#define SELECT_NOT_INT INT32_MIN	//!< Used to tell that a int value shouldn't be written.

#define DBUFFER_CSVLINE_SIZE 950	//!< Maximum amount of characters which will be read in per line. CAREFULLY HERE!

uint8_t unified_write(tdDataId datatype, void* db);
uint8_t writeOutMemberValues(tdDataId datatype, char * ptr, char * end,
										void* db, FILE* pFile, char* dataBuffer,
										uint32_t lastLF, uint8_t lastvalue, uint8_t newline);
uint16_t writeOutMemberValuesFixed(tdDataId datatype, char * ptr, char * end,
										void* db, FILE* pFile, char* dataBuffer,
										uint32_t lastLF, uint8_t lastvalue, uint8_t newline, uint16_t bp);
uint8_t validateFile(char* filename, FILE* pFile, char* header);
uint8_t writeState(FILE *pFile, tdWakdStates statenr, tdWAKDStateConfigurationParameters *state, char* dataBuffer);
uint8_t writeStateParam(FILE *pFile, tdWakdStates statenr, tdParametersSCC *state, char* dataBuffer);
#endif
