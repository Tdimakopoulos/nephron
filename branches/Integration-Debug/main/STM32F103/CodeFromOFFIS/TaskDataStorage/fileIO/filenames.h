 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
#ifndef DS_FILENAMES_H
#define DS_FILENAMES_H

#define FILENAME_LENGTH 60			//!< Maximum length of a filename including the ending and timestamp (e.g. .csv)

#define FILE_LOG				"LOGGING.LOG"         //!< Global logfile
    
#define FILE_PHYSIOLOGICALDATA			"PL-DATA.CSV"         //! \a dataID_physiologicalData
#define FILE_PHYSICALDATA			"PC-DATA.CSV"         //! \a dataID_physicalData
#define FILE_ACTUATORDATA			"AC-DATA.CSV"         //! \a dataID_actuatorData
#define FILE_WEIGHTDATA				"WE-DATA.CSV"         //! \a dataID_weightData
#define FILE_BPDATA				"BP-DATA.CSV"         //! \a dataID_bpData
#define FILE_ECGDATA				"HR-DATA.CSV"         //! \a dataID_EcgData
#define FILE_DEVICESSTATUS			"DE-DATA.CSV"         //! \a dataID_statusRTB
#define FILE_MSGCONFIGURESTATE			"CS-DATA.CSV"         //! \a dataID_changeWAKDStateFromTo

// INTEGRATION
//#define FILE_PATIENTPROFILE			"patient_data.csv" 			//! \a dataID_PatientProfile
#define FILE_PATIENTPROFILE			"patient.csv" 				//! \a dataID_PatientProfile
//#define FILE_WAKDALLSTATECONFIGURE		"state_configuration.csv"		//! \a dataID_WAKDAllStateConfigure
#define FILE_WAKDALLSTATECONFIGURE		"state_c.csv"	
//#define FILE_ALLSTATESPARAMETERSSCC		"statethresh_configuration.csv"		//! \a dataID_AllStatesParametersSCC
#define FILE_ALLSTATESPARAMETERSSCC		"state_t.csv"

// INTEGRATION, we use fixed sized lines for storing data in files (function lack in FS LIB)
#define FIXED_LINE_LENGTH_A   512
#define FIXED_LINE_LENGTH_B   1024

#endif
