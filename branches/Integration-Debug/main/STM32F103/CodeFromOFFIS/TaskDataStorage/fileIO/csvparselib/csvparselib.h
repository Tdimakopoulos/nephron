 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef CSVPARSELIB_H
#define CSVPARSELIB_H

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>


#ifdef LINUX
	//#include "nephron.h"
	#include "global.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\..\\..\\includes_nephron\\global.h"
	//#include "..\\..\\..\\nephron.h"
#endif

// for testing purposes
//#ifdef __arm__
//	#include "pstdint.h"
//	#include "..\\..\\..\\nephron.h"
//	#include "..\\..\\..\\includes_nephron\\global.h"
//#else
//	#include <stdint.h>
//	#include "..\\..\\..\\typedefsNephron.h"
//	void taskMessage(const char * severity, const char * pref, const char * message, ...);
//#endif
#define SELECT_NOT_FLT FLT_MIN		//!< Used to tell that a float value shouldn't be written.
#define SELECT_NOT_INT INT32_MIN	//!< Used to tell that a int value shouldn't be written.

//! Amount of whitespaces which should be written or padded.
/*!
	This should NOT be changed. Defines how much chars will be written / left free if 
	the value doesn't need all usable space. If you choose anything below 11, values
	which were left free and are overwritten by new ones which use all available chars
	will erase ; and therefore destroy the structure of the .csv. So don't change it.
*/
#define VALUE_MAX_CHARS 11 

//! A file should contain 3600 values (one each second, for one hour) as maximum before a new one will be created.
#define MAX_TIMESTAMPS_IN_FILE 3600

#include <limits.h>

uint8_t changeFileName(char* filename, int32_t startingTimeStamp);

size_t strnlen(const char *str, size_t max);

uint8_t seekToLastLF(FILE* pFile);
void movebeforeCRLF(FILE* pFile);
int32_t parseLine(char** ptr, char* end);
float parseLineFloat(char** ptr, char* end);

char* writeFloat(char * ptr, char * end, float dbValue, FILE* pFile, char* dataBuffer, uint32_t lastLF, uint8_t lastvalue, uint8_t newline);

uint8_t writeCRC(FILE* pFile, uint32_t lastLF, char* dataBuffer, size_t bufferSize, uint8_t update);
uint8_t verifyCRC(char* ptr, char* end, char* databuffer, size_t bufferSize);
char* findCRC(char* dataBuffer, size_t bufferSize);
uint8_t parseString(char** ptr, char* destination, size_t destinationSize);

#endif

