 /* -----------------------------------------------------------------------
 * Copyright (c) 2011     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>
#include <stdarg.h>
#include <ctype.h>
#include <time.h>


#ifdef LINUX
	#include "csvparselib.h"
	#include "filenames.h"
	#include "global.h"
	//#include "nephron.h"
	#include "interfaces.h"
	#include "enums.h"
	#ifdef __arm__
		#include "nephron.h" 
		#include "typedefsNephron.h"
		#include "ioHelperFunc.h"
	#endif
	#include "../crc16/crc16.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include ".\\csvparselib.h"
	#include "..\\filenames.h"
	#include "..\\..\\..\\includes_nephron\\global.h"
	//#include "..\\..\\..\\nephron.h"
	#include "..\\crc16\\crc16.h"
#endif

// for testing purposes
//#ifdef __arm__ 
//	#include "pstdint.h"
//	#include "..\\..\\..\\nephron.h"
//	#include "..\\..\\..\\includes_nephron\\global.h"
//#else
//	#include <stdint.h>
//	#include "..\\..\\..\\typedefsNephron.h"
//	void taskMessage(tdAllQueueHandles *const char * severity, const char * pref, const char * message, ...)
//	{
//		//#ifdef VP_SIMULATION
//			// during simulation we want to make "printf"s on simulation trace window
//			va_list vargzeiger;
//			va_start(vargzeiger, message);
//			if (strcmp(severity, "I") == 0) {
//				printf("Info ");
//			} else if (strcmp(severity, "W") == 0) {
//				printf("Warning ");
//			} else if (strcmp(severity, "E") == 0) {
//				printf("Error ");
//			} else if (strcmp(severity, "F") == 0) {
//				printf("Failure ");
//			} else {
//				printf("N.D. ");
//			}
//			printf("(%s) ", pref);
//			vprintf(message, vargzeiger);
//			printf("\n");
//			va_end(vargzeiger);
//		//#else
//			// Simple printf will not work on real HW. With VP we can just printf to cosole.
//			// If we want to give a message with real HW we have to implement an according
//			// functionality here. Remove warning once implementation is done.
//			//#warning(Makro "VP" not defined. Compiling for Nephron target actual HW. Target implementation missing here!)
//		//#endif
//	}
//#endif

#define PREFIX "OFFIS FreeRTOS task DS - CSV parse library"

//!Appends a date to the end of the filename
uint8_t changeFileName(char* filename, int32_t startingTimeStamp)
{
	if(strnlen(filename, FILENAME_LENGTH) + 10 > FILENAME_LENGTH)
	{
		taskMessage("F", PREFIX, "New filename would be %i characters long, limit is %i",strnlen(filename, FILENAME_LENGTH) + 10, FILENAME_LENGTH);
		return 1;
	}
	/*	+10 to hold a complete date and hour(in GMT), e.g. 2002201214
	* 	first, we need to remove the .tmp
	*/
	filename[strnlen(filename, FILENAME_LENGTH) - 4] = '\0';
	char FilenameTime[11];
	struct tm * timeinfo;
	timeinfo = localtime(&startingTimeStamp);
	strftime(FilenameTime,11,"%d%m%Y%H",timeinfo);
	strncat(filename,FilenameTime,11);	
	strncat(filename,".csv",4);	
	return 0;
}

//! Returns the length of a string just as strlen() but with a buffer overflow protection.
/*!
	This function is written by Simon Josefsson and normally covered by the GPL. The LGPL states that
	fewer than 2 lines of code doesn't mean that the whole software has to be LGPL'ed. Even if there
	is no such statement in the GPL licence, my opinion is that 2 generic lines of code are not worth
	copyrighting.
	* \param str			Pointer to the string which length should be returned.
	* \param max			Maximum size which should be checked to prevent a buffer overflow.
	* \return         		Size of the passed string.
*/
size_t strnlen (const char *str, size_t max)
{
    const char *end = memchr (str, 0, max);
	return end ? (size_t)(end - str) : max;
}

//! movebeforeCRLF needs to be executed first! Seeks the beginning of the line, mostly the next byte after a CR or LF.
/*!	
	To bypass the very last CR/LF at the end of the file, movebeforeCRLF needs to be executed first.
	Otherwise we'll try to read EOF here.
	* \param pFile        Stream pointer to the file
	* \return             1 if error occured, otherwise 0
*/
uint8_t seekToLastLF(FILE* pFile)
{
#if defined VP_SIMULATION
	uint8_t currentChar = getc(pFile);
	while(currentChar != '\n' && currentChar != '\r') 
	{
		fseek (pFile, -2, SEEK_CUR); // 2 backwards because getc increases the current position by 1
		if(ftell(pFile) <= 0) // reached start of the file, means we have reached the header
		{
			fseek (pFile, 0, SEEK_SET); // 
			return 1;
		}
		currentChar = getc(pFile);
	}
#endif
	return 0;
}


//! Moves before CR & LF from the stream.
/*!	
	* \param pFile        Stream pointer to the file
*/
void movebeforeCRLF(FILE* pFile)
{
	#ifdef VP_SIMULATION
	fseek (pFile, -1, SEEK_CUR);
	uint8_t currentChar = getc(pFile);
	if(currentChar == '\n' || currentChar == '\r') // check for LF or CR(99% LF but we'll never know)
	{
		fseek (pFile, -2, SEEK_CUR); // 2 backwards because getc increases the current position by 1
		currentChar = getc(pFile);
		if(currentChar == '\n' || currentChar == '\r') // check for following CR or LF(99% CR if there is another special character)
		{
			fseek (pFile, -2, SEEK_CUR);
		}
		else // only one LF or CR found
		{
			fseek (pFile, -1, SEEK_CUR);
		}
	}
	#endif
}

//! Returns the first value of the char* array which is seperated with ;
/*! 	
	No support for "". \a ptr and \a end will be changed after a run.
	\a ptr will point right behind the last ; which was found. \a end is just reused.
	* \param ptr       Pointer to the pointer so we can modifiy the dereferenced ptr.
	* \param end       Will contain afterwards the first char which doesn't belong to a number. Just passed to avoid recreating pointers.
	* \return          If a valid value was found, it will be returned, otherwise INT32_MIN (AKA \a SELECT_NOT_INT)
*/
int32_t parseLine(char** ptr, char* end)
{
	 int32_t read = strtol(*ptr, &end, 10); // 10 is for base 10, therefore decimal
	 /* evaluation order is determined for logical AND,
		so it'll never deference if end is a null pointer */
	 if (end && *end == ';') 
	 {
		if (*ptr != end)
		{
			*ptr = end + 1; // otherwise all next existing values won't be found
			return read;
		}
		*ptr = end + 1; // otherwise all next existing values won't be found
		#ifdef DEBUG_DS
		taskMessage("D", PREFIX, "Debug: No value found\n");
		#endif	
		return INT32_MIN;
	 }
	/* Skip whitespace if no value was found*/
	char c = **ptr;
	while(isspace(c))
	{
		*ptr = *ptr + 1;
		c = **ptr;
	}
	*ptr = *ptr + 1;
	return INT32_MIN;
}


//!	Same function as parseLine just for float values.
float parseLineFloat(char** ptr, char* end)
{
	 float read = strtof(*ptr, &end);
	 /* evaluation order is determined for logical AND,
		so it'll never deference if end is a null pointer */
	 if (end && *end == ';') 
	 {
		if (*ptr != end)
		{
			*ptr = end + 1; // otherwise all next existing values won't be found
			return read;
		}
		*ptr = end + 1; // otherwise all next existing values won't be found
		return FLT_MIN;
	 }
	/* Skip whitespace if no value was found*/
	char c = **ptr;
	while(isspace(c))
	{
		*ptr = *ptr + 1;
		c = **ptr;
	}
	*ptr = *ptr + 1;
	return FLT_MIN;
}

//! Either write to a new line or check if value is not present and we want to write this value now.
/*! 
	Because we read the file into a buffer, we need to know how much
	bytes the pointer on the buffer has advanced to increase the filestream
	by the same amount. 

	This is calculated by ptr(actual position) - dataBuffer(start position)
	
	We need to return the pointer ptr because otherwise we wouldn't know how
	much this function has increased it and where the next call should begin.
	* \param ptr			Pointer to the value in the buffer we want to parse.
	* \param end			Will contain afterwards the first char which doesn't belong to a number. Just passed to avoid recreating pointers.
	* \param dbValue		Value which is contained in the db struct. Will be checked if != SELECT_NOT_FLT
	* \param pFile			Stream pointer to the file
	* \param dataBuffer		Pointer to the buffer which contains the line we read in before calling this function.
	* \param lastLF			Position of the last line feed. Needed to rewind to the beginning of the line after parsing.
	* \param lastvalue		If 0, the written line will end with ";", otherwise ";\n"
	* \param newline		If 1, we want to write into a new line, otherwise an existing one.
	* \return         		Returns the modified pointer \a ptr afterwards because otherwise we wouldn't know how
	much this function has increased it and where the next call should begin.
*/
char* writeFloat(char * ptr, char * end, float dbValue, FILE* pFile, char* dataBuffer, uint32_t lastLF, uint8_t lastvalue, uint8_t newline)
{
	if(newline == 0)
	{
		if(parseLineFloat(&ptr, end) == SELECT_NOT_FLT && dbValue != SELECT_NOT_FLT)
		{
			/*
				Explanation: ptr - dataBuffer + lastLF - VALUE_MAX_CHARS - 1 means:
				Current absolute position in dataBuffer - starting position in dataBuffer returns 
				the amount of bytes we processed so far. Using this number we can advance the file
				object by the same amount of bytes to align them. Because we only got a fixed absolute
				value for calculating (lastLF) we need to set the absolute offset by adding lastLF 
				to the advanced bytes. We need to substract VALUE_MAX_CHARS and -1  because parsing
				the buffer increases the pointer too and last but not least to seek from the current
				position and don't have to start from the beginning we substract the current position
				and use SEEK_CUR instead of SEEK_SET.
			*/
			#if defined VP_SIMULATION
			fseek(pFile, ptr - dataBuffer + lastLF - VALUE_MAX_CHARS - 1 - ftell(pFile), SEEK_CUR);
			fprintf(pFile, "%*g",VALUE_MAX_CHARS,dbValue); // limit to VALUE_MAX_CHARS chars
			fseek(pFile, 1, SEEK_CUR); // to pass the ; again and be in sync with dataBuffer
			#endif
			return ptr;
		}
		else // if value exists, don't check - advance using fseek like above
		{
			#if defined VP_SIMULATION
			fseek(pFile, ptr - dataBuffer + lastLF - VALUE_MAX_CHARS - 1 - ftell(pFile), SEEK_CUR);
			#endif
			return ptr;
		}
	}
	else
	{
		if(dbValue != SELECT_NOT_FLT)
		{
			#if defined VP_SIMULATION
			fprintf(pFile, "%*g%s", VALUE_MAX_CHARS,dbValue, (lastvalue == 0 ? ";" : ";\n")); // if lastline == 1 then write \n
			#endif
		}
		else
		{
			/*
				VALUE_MAX_CHARS whitespaces if we don't want to write a value.
				The last value has to end with a line feed.
			*/
			int i;
			for(i=0;i < VALUE_MAX_CHARS;++i)
			{
				#if defined VP_SIMULATION
				fprintf(pFile, " ");
				#endif
			}
			#if defined VP_SIMULATION
			fprintf(pFile, "%s", (lastvalue == 0 ? ";" : ";\n")); 
			#endif
		}
		return ptr; // no sense, this function was merged and therefore needs to return a value
	}
}

//! Writes the CRC16 value of the line.
/*!
	Rewind to the beginning of the line. Read in the whole line again
	and calculate the CRC16 value of it. You need to set \a lastLF = ftell(pFile);
	BEFORE writing the values you want to calculate the CRC16 of.
	* \param pFile			Stream pointer to the file
	* \param lastLF			Position of the last line feed. Needed to rewind to the beginning of the line after parsing.
	* \param dataBuffer		Pointer to the buffer which contains the line we read in before calling this function.
	* \param bufferSize		Size of \a dataBuffer. You should pass \a DBUFFER_CSVLINE_SIZE here.
	* \param update			0 means writing the crc of a new line, 1 overwrite the existing CRC value with a new one.
	* \return         		Always returns 0 at the moment.
*/
uint8_t writeCRC(FILE* pFile, uint32_t lastLF, char* dataBuffer, size_t bufferSize, uint8_t update)
{
	#if defined VP_SIMULATION
		crc_t crc;
		fseek(pFile,lastLF - ftell(pFile),SEEK_CUR); 
		if(fgets (dataBuffer,bufferSize , pFile) == NULL)
		{
			taskMessage("F", PREFIX, "fgets() while writing CRC failed.");
			return 1;
		}
		crc = crc_init();
		/*	
			-10 means skip these 10 chars. \n, 4. of CRC, 3., 2., 1., :, C, R, C
			if you switch to e.g. CRC32, you need to pass 8 chars instead of 4.
			this could be improved to work with windows CL LF too.
		*/
		crc = crc_update(crc, (unsigned char *)dataBuffer, strnlen(dataBuffer,bufferSize) - (update == 1 ? 10 : 0));
		crc = crc_finalize(crc);
		if(update)
		{
			/* skip 6 to pass \n ; 4. of CRC, 3., 2., 1.
			   if you switch to e.g. CRC32, you need to pass 8 chars instead of 4 here too.
			   The following (f)printf uses %04x to force 4 chars by adding leading zeros if needed.
			*/
			fseek(pFile,-6,SEEK_CUR); 
			fprintf(pFile,"%04x",crc);
		}
		else
		{
			fprintf(pFile,"CRC:%04x;\n",crc);
		}
		#ifdef DEBUG_DS
		taskMessage("I", PREFIX, "CRC:%04x", crc);	
		#endif	
	#endif
	return 0;
}

//! Verifies if the CRC value contained in \a dataBuffer matches.
/*!
	* \param ptr			Pointer to the value in the buffer we want to parse.
	* \param end			Will contain afterwards the first char which doesn't belong to a number. Just passed to avoid recreating pointers.
	* \param dataBuffer		Pointer to the buffer which contains the line we read in before calling this function.
	* \param bufferSize		Size of \a dataBuffer. You should pass \a DBUFFER_CSVLINE_SIZE here.
	* \return         		0 if the CRC contained in \a dataBuffer matches with the calculated one, otherwise 1.
*/
uint8_t verifyCRC(char* ptr, char* end, char* dataBuffer, size_t bufferSize)
{
	crc_t crc_new;
	crc_new = crc_init();
	/*	
		-10 means skip these 10 chars. \n, 4. of CRC, 3., 2., 1., :, C, R, C
		if you switch to e.g. CRC32, you need to pass 8 chars instead of 4.
		this could be improved to work with windows CL LF too.
	*/
	crc_new = crc_update(crc_new, (unsigned char *)dataBuffer, strnlen(dataBuffer,bufferSize) - 10);
	crc_new = crc_finalize(crc_new);
	crc_t crc_read = strtol(ptr + 4, &end, 16); // base 16 aka hex, ptr + 4 to skip CRC:
	#ifdef DEBUG_DS
	taskMessage("I", PREFIX, "Comparing (read) %x with %x (computed)", crc_read, crc_new);
	#endif
	return crc_read != crc_new;
}

//! Finds the CRC value in the passed \a dataBuffer
/*!
	* \param dataBuffer		Pointer to the buffer which contains the line we read in before calling this function.
	* \param bufferSize		Size of \a dataBuffer. You should pass \a DBUFFER_CSVLINE_SIZE here.
	* \return         		Pointer to the char 'C' of "CRC:XXXX" in \a dataBuffer. If it wasn't found, a NULL pointer is returned.
*/
char* findCRC(char* dataBuffer, size_t bufferSize)
{
	unsigned int i;
	/* 
		-5 otherwise we would read out of bounds if the file is corrupt.
	*/
	for(i=0; i < strnlen(dataBuffer,bufferSize) - 5;++i) 
	{
		if(dataBuffer[i] == 'C')
		{
			if(dataBuffer[i+1] == 'R')
			{
				if(dataBuffer[i+2] == 'C')
				{
					if(dataBuffer[i+3] == ':')
					{
						/* 
							returns a pointer to the char 'C'
						*/
						return &dataBuffer[i]; 
					}
				}

			}
		}
	}
	return NULL; // no correct value found, return NULL pointer
}

//! Copies the string before the first semicolon found to a destination. ptr will be altered.
/*!
	* \param ptr   			    Pointer to the pointer so we can modifiy the dereferenced ptr.
	* \param destination		Pointer to the char buffer the result should be written at
	* \param destinationSize	Size of the \a destination. 
	* \return         			0 if everything should be okay, otherwise 1.
*/
uint8_t parseString(char** ptr, char* destination, size_t destinationSize)
{
	size_t passedChars = strcspn(*ptr, ";");
	if(passedChars == 0)
	{
		taskMessage("F", PREFIX, "No semicolon found while parsing");
		return 1;
	}
	if(passedChars + 1 > destinationSize)
	{
		taskMessage("F", PREFIX, "Value in file is too big to fit in destination");
		return 1;
	}
	strncpy(destination, *ptr, passedChars);
	destination[passedChars] = '\0'; // null-terminate
	(*ptr) += passedChars + 1;
	return 0;
}

