/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "readRequestedParametersFromSD.h"

uint8_t readRequestedParametersFromSD(uint16_t numberParameter, void *pData, char *PREFIX, uint8_t *pMsg)
{
	uint8_t err = 0; // No error on default

	// We will fill in the read data after the header.
	uint16_t bufferPosition = sizeof(tdMsgOnly);

	// Right after the header comes the byte defining the state the values count for.
	uint8_t *pWakdState = (uint8_t *)pData;	// pTmp to point to the beginning of the data we received from phone
	pWakdState = pWakdState+sizeof(tdMsgOnly);		// pTmp repointed to directly after the header. This byte is the state definition
	*(pMsg+bufferPosition) = *pWakdState;		// assign this byte in the correct position for our answer.
	bufferPosition++;					// increment for next byte to store.

	// ############################################################################################################
	// ## Step 1: Process patient profile data
	// ############################################################################################################

	// Read values from the patient profile of SD card
	tdMsgPatientProfileData *pMsgPatientProfileData = NULL;
	pMsgPatientProfileData = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData));
	if ( pMsgPatientProfileData == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory for patient profile. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		if(unified_read(dataID_PatientProfile, &(pMsgPatientProfileData->patientProfile)))
		{
			taskMessage("E", PREFIX, "Reading dataID_PatientProfile failed!");
			errMsg(errmsg_ReadingSDcardFailed);
			err = 1;
		} else {
			#if defined DEBUG_DS || defined SEQ12
				taskMessage("I", PREFIX, "Reading patient profile of '%s' complete.", pMsgPatientProfileData->patientProfile.name);
			#endif

			// Copy the required values from read SD card into message to be prepared.
			int i = 0;
			for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
			{
				tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
				switch (parameter) {
					case parameter_name: {
						// char	name[50];
						uint8_t strLength = strlen(pMsgPatientProfileData->patientProfile.name);
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %s.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.name);
							taskMessage("I", PREFIX, "Number of bytes really needed is %d", strLength);
						#endif
						*(pMsg+bufferPosition) = parameter_name;
						bufferPosition++;
						*(pMsg+bufferPosition) = strLength;	// number of characters that follow
						bufferPosition++;
						strcpy(((char *)(pMsg+bufferPosition)), pMsgPatientProfileData->patientProfile.name);
						bufferPosition = bufferPosition + strLength;
						// Correct the size of the message the was initially of maximum size.
						tdMsgOnly *pMsgOnly = (tdMsgOnly *)pMsg;
						pMsgOnly->header.msgSize = pMsgOnly->header.msgSize + strLength - sizeof(pMsgPatientProfileData->patientProfile.name);
						break;
					}
					case parameter_gender: {
						// char	gender; // m/f
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.gender);
						#endif
						*(pMsg+bufferPosition) = parameter_gender;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.gender);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.gender)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_measureTimeHours: {
						//	uint8_t	measureTimeHours;		// daily time for weight & bp measurement, hour
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeHours);
						#endif
						*(pMsg+bufferPosition) = parameter_measureTimeHours;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.measureTimeHours);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.measureTimeHours)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_measureTimeMinutes: {
						//		uint8_t		measureTimeMinutes;		// daily time for weight & bp measurement, minute
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.measureTimeMinutes);
						#endif
						*(pMsg+bufferPosition) = parameter_measureTimeMinutes;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.measureTimeMinutes);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.measureTimeMinutes)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wghtCtlTarget:{
						// uint32_t	wghtCtlTarget;			// weight control target [same unit as weigth measurement on RTB]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlTarget;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlTarget);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlTarget)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wghtCtlLastMeasurement:{
						// uint32_t	wghtCtlLastMeasurement;	// weight control last measurement [same unit as weigth measurement on RTB]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlLastMeasurement;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wghtCtlDefRemPerDay:{
						// uint32_t	wghtCtlDefRemPerDay;	// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_wghtCtlDefRemPerDay;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_kCtlTarget:{
						// uint32_t	kCtlTarget;				// potassium control target use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlTarget;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlTarget);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlTarget)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_kCtlLastMeasurement:{
						// uint32_t	kCtlLastMeasurement;	// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlLastMeasurement;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_kCtlDefRemPerDay:{
						// uint32_t	kCtlDefRemPerDay;		// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_kCtlDefRemPerDay;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_urCtlTarget:{
						// uint32_t	urCtlTarget;			// urea control target use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlTarget);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlTarget;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlTarget);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlTarget)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_urCtlLastMeasurement:{
						// uint32_t	urCtlLastMeasurement;	// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlLastMeasurement;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_urCtlDefRemPerDay:{
						// uint32_t	urCtlDefRemPerDay;		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);
						#endif
						*(pMsg+bufferPosition) = parameter_urCtlDefRemPerDay;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_minDialPlasFlow:{
						//	uint16_t  	minDialPlasFlow;		// minimal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialPlasFlow);
						#endif
						*(pMsg+bufferPosition) = parameter_minDialPlasFlow;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.minDialPlasFlow);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialPlasFlow)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_maxDialPlasFlow:{
						//	uint16_t  	maxDialPlasFlow;		// maximal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialPlasFlow);
						#endif
						*(pMsg+bufferPosition) = parameter_maxDialPlasFlow;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.maxDialPlasFlow);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialPlasFlow)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_minDialVoltage:{
						//	uint16_t	minDialVoltage;			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.minDialVoltage);
						#endif
						*(pMsg+bufferPosition) = parameter_minDialVoltage;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.minDialVoltage);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.minDialVoltage)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_maxDialVoltage:{

						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgPatientProfileData->patientProfile.maxDialVoltage);
						#endif
						*(pMsg+bufferPosition) = parameter_maxDialVoltage;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgPatientProfileData->patientProfile.maxDialVoltage);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgPatientProfileData->patientProfile.maxDialVoltage)))+j);
							bufferPosition++;
						}
						break;
					}
					default: {
						// Ignore parameter, this should be covered in one of the next switch statements.
						// taskMessage("I", PREFIX, "Parameter '%s' ignored.", parameterToString(parameter));
						break;
					}
				}
			}
		}
		// Copied all values from patient profile. Free mem.
		sFree((void *)(&pMsgPatientProfileData));
	}

	// ############################################################################################################
	// ## Step 2: Process WAKDStateConfigurationParameters
	// ############################################################################################################

	// Allocate space for values
	tdMsgWAKDStateConfigurationParameters *pMsgWAKDStateConfigurationParameters = NULL;
	pMsgWAKDStateConfigurationParameters = (tdMsgWAKDStateConfigurationParameters *) pvPortMalloc(sizeof(tdMsgWAKDStateConfigurationParameters));
	if ( pMsgWAKDStateConfigurationParameters == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory for WAKD State Configuration Parameters. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		// The function "unified_read" will take from the given data structure the state to read from
		// So we need to initialize that.
		pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState = *pWakdState;

		if(unified_read(dataID_configureState, &(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters)))
		{
			taskMessage("F", PREFIX, "Reading WAKD state configuration from SD card failed!");
		} else {
			#if defined DEBUG_DS || defined SEQ12
				taskMessage("I", PREFIX, "Reading configuration parameters for WAKD state %s", stateIdToString(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState));
//				taskMessage("I", PREFIX, "defSpeedBp_mlPmin is: %d", pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);
//				taskMessage("I", PREFIX, "defSpeedFp_mlPmin is: %d", pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);
//				taskMessage("I", PREFIX, "defPol_V is:          %d", pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);
//				taskMessage("I", PREFIX, "direction is:         %d", pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);
//				taskMessage("I", PREFIX, "duration_sec is:      %d", pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);
			#endif
			// Copy the required values from read SD card into message to be prepared.
			int i = 0;
			for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
			{
				tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
				switch (parameter) {
					case parameter_defSpeedBp_mlPmin: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);
						#endif
						*(pMsg+bufferPosition) = parameter_defSpeedBp_mlPmin;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_defSpeedFp_mlPmin: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);
						#endif
						*(pMsg+bufferPosition) = parameter_defSpeedFp_mlPmin;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_defPol_V: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);
						#endif
						*(pMsg+bufferPosition) = parameter_defPol_V;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_direction: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);
						#endif
						*(pMsg+bufferPosition) = parameter_direction;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_duration_sec: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);
						#endif
						*(pMsg+bufferPosition) = parameter_duration_sec;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec)))+j);
							bufferPosition++;
						}
						break;
					}
					default: {
						// Ignore parameter, this should be covered in one of the other switch statements.
						// taskMessage("I", PREFIX, "Parameter '%s' ignored.", parameterToString(parameter));
						break;
					}
				}
			}
		}
		// Copied all values from WAKD state configuration. Free mem.
		sFree((void *)(&pMsgWAKDStateConfigurationParameters));
	}

	// ############################################################################################################
	// ## Step 3:
	// ############################################################################################################

	// Allocate space for values
	tdParametersSCC *pParametersSCC = NULL;
	pParametersSCC = (tdParametersSCC *) pvPortMalloc(sizeof(tdParametersSCC));
	if ( pParametersSCC == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory for WAKD System Check Configuration Parameters. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		// The function "unified_read" will take from the given data structure the state to read from
		// So we need to initialize that.
		pParametersSCC->thisState = *pWakdState;

		if(unified_read(dataID_StateParametersSCC, pParametersSCC))
		{
			taskMessage("F", PREFIX, "Reading WAKD System Check configuration from SD card failed!");
		} else {
			#if defined DEBUG_DS || defined SEQ12
				taskMessage("I", PREFIX, "Reading System Check configuration parameters for WAKD state %s", stateIdToString(pParametersSCC->thisState));
//				taskMessage("I", PREFIX, "NaAbsL is:          %d", *((uint32_t *)(&pParametersSCC->NaAbsL)));
//				taskMessage("I", PREFIX, "NaAbsH is:          %d", *((uint32_t *)(&pParametersSCC->NaAbsH)));
//				taskMessage("I", PREFIX, "NaTrdLT is:         %d", *((uint32_t *)(&pParametersSCC->NaTrdLT)));
//				taskMessage("I", PREFIX, "NaTrdHT is:         %d", *((uint32_t *)(&pParametersSCC->NaTrdHT)));
//				taskMessage("I", PREFIX, "NaTrdLPsT is:       %d", *((uint32_t *)(&pParametersSCC->NaTrdLPsT)));
//				taskMessage("I", PREFIX, "NaTrdHPsT is:       %d", *((uint32_t *)(&pParametersSCC->NaTrdHPsT)));
//
//				taskMessage("I", PREFIX, "KAbsL is:           %d", pParametersSCC->KAbsL);
//				taskMessage("I", PREFIX, "KAbsH is:           %d", pParametersSCC->KAbsH);
//				taskMessage("I", PREFIX, "KAAL is:            %d", pParametersSCC->KAAL);
//				taskMessage("I", PREFIX, "KAAH is:            %d", pParametersSCC->KAAH);
//				taskMessage("I", PREFIX, "KTrdT is:          %d", pParametersSCC->KTrdT);
//				taskMessage("I", PREFIX, "KTrdLPsT is:        %d", pParametersSCC->KTrdLPsT);
//				taskMessage("I", PREFIX, "KTrdHPsT is:        %d", pParametersSCC->KTrdHPsT);
//
//				taskMessage("I", PREFIX, "CaAbsL is:          %d", pParametersSCC->CaAbsL);
//				taskMessage("I", PREFIX, "CaAbsH is:          %d", pParametersSCC->CaAbsH);
//				taskMessage("I", PREFIX, "CaAAbsL is:         %d", pParametersSCC->CaAAbsL);
//				taskMessage("I", PREFIX, "CaAAbsH is:         %d", pParametersSCC->CaAAbsH);
//				taskMessage("I", PREFIX, "CaTrdLT is:         %d", pParametersSCC->CaTrdLT);
//				taskMessage("I", PREFIX, "CaTrdHT is:         %d", pParametersSCC->CaTrdHT);
//				taskMessage("I", PREFIX, "CaTrdLPsT is:       %d", pParametersSCC->CaTrdLPsT);
//				taskMessage("I", PREFIX, "CaTrdHPsT is:       %d", pParametersSCC->CaTrdHPsT);
//
//				taskMessage("I", PREFIX, "UreaAAbsH is:       %d", pParametersSCC->UreaAAbsH);
//				taskMessage("I", PREFIX, "UreaTrdT is:        %d", pParametersSCC->UreaTrdT);
//				taskMessage("I", PREFIX, "UreaTrdHPsT is:     %d", pParametersSCC->UreaTrdHPsT);
//
//				taskMessage("I", PREFIX, "CreaAbsL is:        %d", pParametersSCC->CreaAbsL);
//				taskMessage("I", PREFIX, "CreaAbsH is:        %d", pParametersSCC->CreaAbsH);
//				taskMessage("I", PREFIX, "CreaTrdT is:        %d", pParametersSCC->CreaTrdT);
//				taskMessage("I", PREFIX, "CreaTrdHPsT is:     %d", pParametersSCC->CreaTrdHPsT);
//
//				taskMessage("I", PREFIX, "PhosAbsL is:        %d", pParametersSCC->PhosAbsL);
//				taskMessage("I", PREFIX, "PhosAbsH is:        %d", pParametersSCC->PhosAbsH);
//				taskMessage("I", PREFIX, "PhosAAbsH is:       %d", pParametersSCC->PhosAAbsH);
//				taskMessage("I", PREFIX, "PhosTrdLT is:       %d", pParametersSCC->PhosTrdLT);
//				taskMessage("I", PREFIX, "PhosTrdHT is:       %d", pParametersSCC->PhosTrdHT);
//				taskMessage("I", PREFIX, "PhosTrdLPsT is:     %d", pParametersSCC->PhosTrdLPsT);
//				taskMessage("I", PREFIX, "PhosTrdHPsT is:     %d", pParametersSCC->PhosTrdHPsT);
//
//				taskMessage("I", PREFIX, "HCO3AAbsL is:       %d", pParametersSCC->HCO3AAbsL);
//				taskMessage("I", PREFIX, "HCO3AbsL is:        %d", pParametersSCC->HCO3AbsL);
//				taskMessage("I", PREFIX, "HCO3AbsH is:        %d", pParametersSCC->HCO3AbsH);
//				taskMessage("I", PREFIX, "HCO3TrdLT is:       %d", pParametersSCC->HCO3TrdLT);
//				taskMessage("I", PREFIX, "HCO3TrdHT is:       %d", pParametersSCC->HCO3TrdHT);
//				taskMessage("I", PREFIX, "HCO3TrdLPsT is:     %d", pParametersSCC->HCO3TrdLPsT);
//				taskMessage("I", PREFIX, "HCO3TrdHPsT is:     %d", pParametersSCC->HCO3TrdHPsT);
//
//				taskMessage("I", PREFIX, "PhAAbsL is:         %d", pParametersSCC->PhAAbsL);
//				taskMessage("I", PREFIX, "PhAAbsH is:         %d", pParametersSCC->PhAAbsH);
//				taskMessage("I", PREFIX, "PhAbsL is:          %d", pParametersSCC->PhAbsL);
//				taskMessage("I", PREFIX, "PhAbsH is:          %d", pParametersSCC->PhAbsH);
//				taskMessage("I", PREFIX, "PhTrdLT is:         %d", pParametersSCC->PhTrdLT);
//				taskMessage("I", PREFIX, "PhTrdHT is:         %d", pParametersSCC->PhTrdHT);
//				taskMessage("I", PREFIX, "PhTrdLPsT is:       %d", pParametersSCC->PhTrdLPsT);
//				taskMessage("I", PREFIX, "PhTrdHPsT is:       %d", pParametersSCC->PhTrdHPsT);
//
//				taskMessage("I", PREFIX, "BPsysAbsL is:       %d", pParametersSCC->BPsysAbsL);
//				taskMessage("I", PREFIX, "BPsysAbsH is:       %d", pParametersSCC->BPsysAbsH);
//				taskMessage("I", PREFIX, "BPsysAAbsH is:      %d", pParametersSCC->BPsysAAbsH);
//				taskMessage("I", PREFIX, "BPdiaAbsL is:       %d", pParametersSCC->BPdiaAbsL);
//				taskMessage("I", PREFIX, "BPdiaAbsH is:       %d", pParametersSCC->BPdiaAbsH);
//				taskMessage("I", PREFIX, "BPdiaAAbsH is:      %d", pParametersSCC->BPdiaAAbsH);
//
//				taskMessage("I", PREFIX, "pumpFaccDevi is:    %d", pParametersSCC->pumpFaccDevi);
//				taskMessage("I", PREFIX, "pumpBaccDevi is:    %d", pParametersSCC->pumpBaccDevi);
//
//				taskMessage("I", PREFIX, "VertDeflecitonT is: %d", pParametersSCC->VertDeflecitonT);
//
//				taskMessage("I", PREFIX, "WghtAbsL is:        %d", pParametersSCC->WghtAbsL);
//				taskMessage("I", PREFIX, "WghtAbsH is:        %d", pParametersSCC->WghtAbsH);
//				taskMessage("I", PREFIX, "WghtTrdT is:        %d", pParametersSCC->WghtTrdT);
//				taskMessage("I", PREFIX, "FDpTL is:           %d", pParametersSCC->FDpTL);
//				taskMessage("I", PREFIX, "FDpTH is:           %d", pParametersSCC->FDpTH);
//
//				taskMessage("I", PREFIX, "BPSoTH is:          %d", pParametersSCC->BPSoTH);
//				taskMessage("I", PREFIX, "BPSoTL is:          %d", pParametersSCC->BPSoTL);
//				taskMessage("I", PREFIX, "BPSiTH is:          %d", pParametersSCC->BPSiTH);
//				taskMessage("I", PREFIX, "BPSiTL is:          %d", pParametersSCC->BPSiTL);
//				taskMessage("I", PREFIX, "FPSoTH is:          %d", pParametersSCC->FPSoTH);
//				taskMessage("I", PREFIX, "FPSoTL is:          %d", pParametersSCC->FPSoTL);
//				taskMessage("I", PREFIX, "FPSTH is:           %d", pParametersSCC->FPSTH);
//				taskMessage("I", PREFIX, "FPSTL is:           %d", pParametersSCC->FPSTL);
//
//				taskMessage("I", PREFIX, "BTSoTH is:          %d", pParametersSCC->BTSoTH);
//				taskMessage("I", PREFIX, "BTSoTL is:          %d", pParametersSCC->BTSoTL);
//				taskMessage("I", PREFIX, "BTSiTH is:          %d", pParametersSCC->BTSiTH);
//				taskMessage("I", PREFIX, "BTSiTL is:          %d", pParametersSCC->BTSiTL);
//
//				taskMessage("I", PREFIX, "BatStatT is:        %d", pParametersSCC->BatStatT);
//
//				taskMessage("I", PREFIX, "f_K is:             %d", pParametersSCC->f_K);
//				taskMessage("I", PREFIX, "SCAP_K is:          %d", pParametersSCC->SCAP_K);
//				taskMessage("I", PREFIX, "P_K is:             %d", pParametersSCC->P_K);
//				taskMessage("I", PREFIX, "Fref_K is:          %d", pParametersSCC->Fref_K);
//				taskMessage("I", PREFIX, "cap_K is:           %d", pParametersSCC->cap_K);
//				taskMessage("I", PREFIX, "f_Ph is:            %d", pParametersSCC->f_Ph);
//				taskMessage("I", PREFIX, "SCAP_Ph is:         %d", pParametersSCC->SCAP_Ph);
//				taskMessage("I", PREFIX, "P_Ph is:            %d", pParametersSCC->P_Ph);
//				taskMessage("I", PREFIX, "Fref_Ph is:         %d", pParametersSCC->Fref_Ph);
//				taskMessage("I", PREFIX, "cap_Ph is:          %d", pParametersSCC->cap_Ph);
//				taskMessage("I", PREFIX, "A_dm2 is:           %d", pParametersSCC->A_dm2);
//
//				taskMessage("I", PREFIX, "CaAdr is:           %d", pParametersSCC->CaAdr);
//				taskMessage("I", PREFIX, "CreaAdr is:         %d", pParametersSCC->CreaAdr);
//				taskMessage("I", PREFIX, "HCO3Adr; is:        %d", pParametersSCC->HCO3Adr);
//
//				taskMessage("I", PREFIX, "wgtNa is:           %d", pParametersSCC->wgtNa);
//				taskMessage("I", PREFIX, "wgtK is:            %d", pParametersSCC->wgtK);
//				taskMessage("I", PREFIX, "wgtCa is:           %d", pParametersSCC->wgtCa);
//				taskMessage("I", PREFIX, "wgtUr is:           %d", pParametersSCC->wgtUr);
//				taskMessage("I", PREFIX, "wgtCrea is:         %d", pParametersSCC->wgtCrea);
//				taskMessage("I", PREFIX, "wgtPhos is:         %d", pParametersSCC->wgtPhos);
//				taskMessage("I", PREFIX, "wgtHCO3 is:         %d", pParametersSCC->wgtHCO3);
//				taskMessage("I", PREFIX, "mspT is:            %d", pParametersSCC->mspT);
			#endif
			// Copy the required values from read SD card into message to be prepared.
			int i = 0;
			for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
			{
				tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
				switch (parameter) {
					case parameter_NaAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_NaAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_NaAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_NaAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_NaTrdLT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaTrdLT);
						#endif
						*(pMsg+bufferPosition) = parameter_NaTrdLT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaTrdLT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaTrdLT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_NaTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaTrdHT);
						#endif
						*(pMsg+bufferPosition) = parameter_NaTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaTrdHT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaTrdHT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_NaTrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaTrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_NaTrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaTrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaTrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_NaTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->NaTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_NaTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->NaTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->NaTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_KAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_KAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KAAL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KAAL);
						#endif
						*(pMsg+bufferPosition) = parameter_KAAL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KAAL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KAAL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KAAH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KAAH);
						#endif
						*(pMsg+bufferPosition) = parameter_KAAH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KAAH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KAAH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KTrdT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KTrdT);
						#endif
						*(pMsg+bufferPosition) = parameter_KTrdT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KTrdT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KTrdT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KTrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KTrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_KTrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KTrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KTrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_KTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->KTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_KTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->KTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->KTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_CaAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_CaAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaAAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaAAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_CaAAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaAAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaAAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_CaAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaTrdLT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaTrdLT);
						#endif
						*(pMsg+bufferPosition) = parameter_CaTrdLT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaTrdLT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaTrdLT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaTrdHT);
						#endif
						*(pMsg+bufferPosition) = parameter_CaTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaTrdHT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaTrdHT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaTrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaTrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_CaTrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaTrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaTrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_CaTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_UreaAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->UreaAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_UreaAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->UreaAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->UreaAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_UreaTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->UreaTrdT);
						#endif
						*(pMsg+bufferPosition) = parameter_UreaTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->UreaTrdT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->UreaTrdT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_UreaTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->UreaTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_UreaTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->UreaTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->UreaTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CreaAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CreaAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_CreaAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CreaAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CreaAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CreaAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CreaAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_CreaAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CreaAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CreaAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CreaTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CreaTrdT);
						#endif
						*(pMsg+bufferPosition) = parameter_CreaTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CreaTrdT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CreaTrdT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CreaTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CreaTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_CreaTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CreaTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CreaTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosTrdLT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosTrdLT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosTrdLT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosTrdLT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosTrdLT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosTrdHT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosTrdHT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosTrdHT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosTrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosTrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosTrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosTrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosTrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhosTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhosTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhosTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhosTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhosTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3AAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3AAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3AAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3AAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3AAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3AbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3AbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3AbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3AbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3AbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3AbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3AbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3AbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3AbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3AbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3TrdLT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3TrdLT);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3TrdLT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3TrdLT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3TrdLT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3TrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3TrdHT);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3TrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3TrdHT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3TrdHT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3TrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3TrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3TrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3TrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3TrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3TrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3TrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3TrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3TrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3TrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhAAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhAAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_PhAAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhAAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhAAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_PhAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_PhAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_PhAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhTrdLT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhTrdLT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhTrdLT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhTrdLT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhTrdLT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhTrdHT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhTrdHT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhTrdHT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhTrdHT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhTrdHT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhTrdLPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhTrdLPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhTrdLPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhTrdLPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhTrdLPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_PhTrdHPsT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->PhTrdHPsT);
						#endif
						*(pMsg+bufferPosition) = parameter_PhTrdHPsT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->PhTrdHPsT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->PhTrdHPsT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPsysAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPsysAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPsysAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPsysAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPsysAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPsysAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPsysAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPsysAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPsysAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPsysAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPdiaAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPdiaAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_BPdiaAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPdiaAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPdiaAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPdiaAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPdiaAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPdiaAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPdiaAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPdiaAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPdiaAAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPdiaAAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPdiaAAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPdiaAAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPdiaAAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_pumpFaccDevi: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->pumpFaccDevi);
						#endif
						*(pMsg+bufferPosition) = parameter_pumpFaccDevi;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->pumpFaccDevi);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->pumpFaccDevi)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_pumpBaccDevi: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->pumpBaccDevi);
						#endif
						*(pMsg+bufferPosition) = parameter_pumpBaccDevi;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->pumpBaccDevi);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->pumpBaccDevi)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_VertDeflecitonT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->VertDeflecitonT);
						#endif
						*(pMsg+bufferPosition) = parameter_VertDeflecitonT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->VertDeflecitonT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->VertDeflecitonT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_WghtAbsL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->WghtAbsL);
						#endif
						*(pMsg+bufferPosition) = parameter_WghtAbsL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->WghtAbsL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->WghtAbsL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_WghtAbsH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->WghtAbsH);
						#endif
						*(pMsg+bufferPosition) = parameter_WghtAbsH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->WghtAbsH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->WghtAbsH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_WghtTrdT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->WghtTrdT);
						#endif
						*(pMsg+bufferPosition) = parameter_WghtTrdT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->WghtTrdT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->WghtTrdT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FDpTL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FDpTL);
						#endif
						*(pMsg+bufferPosition) = parameter_FDpTL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FDpTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FDpTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FDpTH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FDpTH);
						#endif
						*(pMsg+bufferPosition) = parameter_FDpTH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FDpTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FDpTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPSoTH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPSoTH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPSoTH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPSoTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPSoTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPSoTL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPSoTL);
						#endif
						*(pMsg+bufferPosition) = parameter_BPSoTL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPSoTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPSoTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPSiTH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPSiTH);
						#endif
						*(pMsg+bufferPosition) = parameter_BPSiTH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPSiTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPSiTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BPSiTL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BPSiTL);
						#endif
						*(pMsg+bufferPosition) = parameter_BPSiTL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BPSiTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BPSiTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FPS1TH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FPSoTH);
						#endif
						*(pMsg+bufferPosition) = parameter_FPS1TH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FPSoTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FPSoTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FPS1TL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FPSoTL);
						#endif
						*(pMsg+bufferPosition) = parameter_FPS1TL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FPSoTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FPSoTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FPS2TH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FPSTH);
						#endif
						*(pMsg+bufferPosition) = parameter_FPS2TH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FPSTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FPSTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_FPS2TL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->FPSTL);
						#endif
						*(pMsg+bufferPosition) = parameter_FPS2TL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->FPSTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->FPSTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BTSoTH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BTSoTH);
						#endif
						*(pMsg+bufferPosition) = parameter_BTSoTH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BTSoTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BTSoTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BTSoTL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BTSoTL);
						#endif
						*(pMsg+bufferPosition) = parameter_BTSoTL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BTSoTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BTSoTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BTSiTH: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BTSiTH);
						#endif
						*(pMsg+bufferPosition) = parameter_BTSiTH;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BTSiTH);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BTSiTH)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BTSiTL: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BTSiTL);
						#endif
						*(pMsg+bufferPosition) = parameter_BTSiTL;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BTSiTL);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BTSiTL)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_BatStatT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->BatStatT);
						#endif
						*(pMsg+bufferPosition) = parameter_BatStatT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->BatStatT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->BatStatT)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_f_K: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->f_K);
						#endif
						*(pMsg+bufferPosition) = parameter_f_K;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->f_K);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->f_K)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_SCAP_K: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->SCAP_K);
						#endif
						*(pMsg+bufferPosition) = parameter_SCAP_K;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->SCAP_K);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->SCAP_K)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_P_K: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->P_K);
						#endif
						*(pMsg+bufferPosition) = parameter_P_K;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->P_K);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->P_K)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_Fref_K: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->Fref_K);
						#endif
						*(pMsg+bufferPosition) = parameter_Fref_K;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->Fref_K);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->Fref_K)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_cap_K: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->cap_K);
						#endif
						*(pMsg+bufferPosition) = parameter_cap_K;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->cap_K);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->cap_K)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_f_Ph: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->f_Ph);
						#endif
						*(pMsg+bufferPosition) = parameter_f_Ph;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->f_Ph);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->f_Ph)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_SCAP_Ph: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->SCAP_Ph);
						#endif
						*(pMsg+bufferPosition) = parameter_SCAP_Ph;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->SCAP_Ph);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->SCAP_Ph)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_P_Ph: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->P_Ph);
						#endif
						*(pMsg+bufferPosition) = parameter_P_Ph;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->P_Ph);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->P_Ph)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_Fref_Ph: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->Fref_Ph);
						#endif
						*(pMsg+bufferPosition) = parameter_Fref_Ph;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->Fref_Ph);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->Fref_Ph)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_cap_Ph: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->cap_Ph);
						#endif
						*(pMsg+bufferPosition) = parameter_cap_Ph;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->cap_Ph);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->cap_Ph)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_A_dm2: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->A_dm2);
						#endif
						*(pMsg+bufferPosition) = parameter_A_dm2;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->A_dm2);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->A_dm2)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CaAdr: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CaAdr);
						#endif
						*(pMsg+bufferPosition) = parameter_CaAdr;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CaAdr);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CaAdr)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_CreaAdr: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->CreaAdr);
						#endif
						*(pMsg+bufferPosition) = parameter_CreaAdr;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->CreaAdr);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->CreaAdr)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_HCO3Adr: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->HCO3Adr);
						#endif
						*(pMsg+bufferPosition) = parameter_HCO3Adr;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->HCO3Adr);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->HCO3Adr)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtNa: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtNa);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtNa;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtNa);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtNa)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtK: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtK);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtK;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtK);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtK)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtCa: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtCa);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtCa;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtCa);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtCa)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtUr: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtUr);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtUr;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtUr);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtUr)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtCrea: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtCrea);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtCrea;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtCrea);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtCrea)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtPhos: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtPhos);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtPhos;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtPhos);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtPhos)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_wgtHCO3: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->wgtHCO3);
						#endif
						*(pMsg+bufferPosition) = parameter_wgtHCO3;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->wgtHCO3);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->wgtHCO3)))+j);
							bufferPosition++;
						}
						break;
					}
					case parameter_mspT: {
						#if defined DEBUG_DS || defined SEQ12
							taskMessage("I", PREFIX, "Parameter '%s' is %d.", parameterToString(parameter), pParametersSCC->mspT);
						#endif
						*(pMsg+bufferPosition) = parameter_mspT;
						bufferPosition++;
						uint8_t j = 0;
						for (j = 0; j<sizeof(pParametersSCC->mspT);j++)
						{	// copy byte by byte
							*(pMsg+bufferPosition) = *(((uint8_t *)(&(pParametersSCC->mspT)))+j);
							bufferPosition++;
						}
						break;
					}
					default: {
						// Ignore parameter, this should be covered in one of the other switch statements.
						// taskMessage("I", PREFIX, "Parameter '%s' ignored.", parameterToString(parameter));
						break;
					}
				}
			}
		}
		// Copied all values from System Check configuration. Free mem.
		sFree((void *)(&pParametersSCC));
	}
	return(err);
}
