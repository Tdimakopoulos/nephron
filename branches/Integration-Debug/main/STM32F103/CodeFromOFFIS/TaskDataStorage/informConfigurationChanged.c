/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "informConfigurationChanged.h"

uint8_t informConfigurationChanged(tdDataId dataID, xQueueHandle *queue, char *PREFIX)
{
	uint8_t err = 0;
	tdQtoken Qtoken;
	Qtoken.command	= command_ConfigurationChanged;

	tdMsgOnly *pMsgOnly = NULL;
	pMsgOnly = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
	if (pMsgOnly == NULL)
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "'Unable to allocate memory inform about configuration change. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	} else {
		#if defined DEBUG_DS
			taskMessage("I", PREFIX, "Sending command_ConfigurationChanged for dataID '%s'.", dataIdToString(dataID));
		# endif
		pMsgOnly->header.dataId			= dataID;
		pMsgOnly->header.issuedBy		= whoId_MB;
		pMsgOnly->header.recipientId	= whoId_MB;

		Qtoken.pData	= (void *) pMsgOnly;

		if ( xQueueSend( queue, &Qtoken, DS_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'command_ConfigurationChanged'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
			err = 1;
		}
	}

	return(err);
}
