/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "apiWakdTime.h"

tdUnixTimeType getTime(void) {
	// xTaskGetTickCount() just gives a relative time measure counting 10ms 
	// beginning at the point where the WAKD was turned on. It needs to be replaced
	// by an absolute time function that delivers appropriate values.
	#ifndef HWBUILD
          return(xTaskGetTickCount()*portTICK_RATE_MS/1000);
        #else
          return(globalTimestampSec);
        #endif
}

// returns "1" if t1 is smaller than t2, if t1 >= t2 returns "0". 
// The use case for this API is to handle time values that are relatively close together.
// If the time difference is very large (more than a year: 31536000)
// it is assumed that overflow of variable t2 has occured and t1 is still smaller than t2.
unsigned char compTimeIsSmaller(portTickType t1, portTickType t2) {
	if (t1 < t2) {
		if ((t2-t1) > 31536000){
			// Overflow: t1 is not really smaller than t2
			return(0);
		} else {
			return(1);
		}
	} else {
		if ((t1-t2) > 31536000){
			//Overflow: t1 really is smaller than t2
			return(1);
		} else {
			return(0);
		}
	}
	
}
