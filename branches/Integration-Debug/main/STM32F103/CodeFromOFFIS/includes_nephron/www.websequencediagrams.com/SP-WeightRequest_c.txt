note left of SP
	3/3 possibilities.
	SP is communicating with MB
end note	
note left of SP 
	User interface on SP asks patient
	to step on scale. He does so
	activating the weight scale) and
	ackknowledges by pressing OK.
end note
SP -> CB: dataId_WeightRequest
note over CB
	looks into header for
	... size information
end note
CB -> MB: bytewise forward
note over MB
	looks into header for
	... id information
	... replace recipient with CB
	... replace sender with MB
	... generate Ack for SP
end note
MB -> CB: data_Id_Ack
note over CB
	looks into header for
	... id information
	... size information
end note
CB -> SP: bytewise forward
MB -> CB: dataId_WeightRequest
note over CB
	looks into header for
	... id information
end note
note over CB
	CB copy sender ID (MB)
	to recipient of Ack
end note
CB -> MB: dataId_Ack
