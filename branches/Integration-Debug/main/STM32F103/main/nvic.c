// ---------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// ---------------------------------------------------------------------------------------------------------------------
//
//! \file   nvic.c
//! \brief  NVIC functions
//!
//! This module provides the functions to configure the NVIC
//! These are additional functions to the ones declared in the core_cm3.h file (CSMIS).
//!
//! \author  Kaeser C.
//! \date    28.10.2010
//! \version 1.0 First version (CKs)
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Include section
// ---------------------------------------------------------------------------------------------------------------------
#include "nvic.h"

// ---------------------------------------------------------------------------------------------------------------------                               
// Private definitions 
// ---------------------------------------------------------------------------------------------------------------------
#define AIRCR_VECTKEY_MASK    ((uint32_t )0x05FA0000)

// ---------------------------------------------------------------------------------------------------------------------
// Private macros
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Private structures
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Private type
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Private constants
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Private variables
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Exported global variables
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Private function prototypes
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Inline code definition
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Function definitions
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Resets the NVIC peripheral registers to their default.
//!
//! This function resets the NVIC peripheral registers to their default.
//!
//! \param  void
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_Reset(void)
{
    uint32_t index = 0;
  
    NVIC->ICER[0] = 0xFFFFFFFF;
    NVIC->ICER[1] = 0x0FFFFFFF;
    NVIC->ICPR[0] = 0xFFFFFFFF;
    NVIC->ICPR[1] = 0x0FFFFFFF;
  
    for(index = 0; index < 0x0F; index++)
    {
        NVIC->IP[index] = 0x00000000;
    } 
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Resets the SCB peripheral registers to their default.
//!
//! This function resets the SCB peripheral registers to their default.
//!
//! \param  void
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_SCBReset(void)
{
    uint32_t index = 0x00;
  
    SCB->ICSR = 0x0A000000;
    SCB->VTOR = 0x00000000;
    SCB->AIRCR = AIRCR_VECTKEY_MASK;
    SCB->SCR = 0x00000000;
    SCB->CCR = 0x00000000;
    for(index = 0; index < 0x03; index++)
    {
        SCB->SHP[index] = 0;
    }
    SCB->SHCSR = 0x00000000;
    SCB->CFSR = 0xFFFFFFFF;
    SCB->HFSR = 0xFFFFFFFF;
    SCB->DFSR = 0xFFFFFFFF;
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Configures the priority grouping: pre-emption priority and subpriority.
//!
//! This function configures the priority grouping: pre-emption priority and subpriority.
//!
//! \param  uint32_t    PriorityGroup specifies the priority grouping bits length:
//!                     - NVIC_PRIORITY_GROUP0: 0 bits for pre-emption priority - 4 bits for subpriority
//!                     - NVIC_PRIORITY_GROUP1: 1 bits for pre-emption priority - 3 bits for subpriority
//!                     - NVIC_PRIORITY_GROUP2: 2 bits for pre-emption priority - 2 bits for subpriority
//!                     - NVIC_PRIORITY_GROUP3: 3 bits for pre-emption priority - 1 bits for subpriority
//!                     - NVIC_PRIORITY_GROUP4: 4 bits for pre-emption priority - 0 bits for subpriority
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_PriorityGroupConfig(const uint32_t PriorityGroup)
{
    SCB->AIRCR = AIRCR_VECTKEY_MASK | PriorityGroup;
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Initializes the NVIC peripheral according to the specified parameter.
//!
//! This function initializes the NVIC peripheral according to the specified parameter.
//!
//! \param  nvic_Init_ts        InitStruct is a pointer to a nvic_Init_ts structure that contains
//!                             the configuration information for the specified NVIC peripheral
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_Init(const nvic_Init_ts* InitStruct)
{
    uint32_t tmpreg = 0x00;
    uint32_t tmpgroup;

    if (InitStruct->nvic_IRQChannelCmd == ENABLE)
    {

        // Compute the Corresponding IRQ Priority
        tmpgroup = (SCB->AIRCR & (uint32_t)0x700);
        switch(tmpgroup)
        {
            case NVIC_PRIORITY_GROUP0:  // 0 bits for pre-emption priority - 4 bits for subpriority
                tmpreg = NVIC_EncodePriority(7, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
                break;
            case NVIC_PRIORITY_GROUP1:  // 1 bits for pre-emption priority - 3 bits for subpriority
                tmpreg = NVIC_EncodePriority(6, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
                break;
            case NVIC_PRIORITY_GROUP2:  // 2 bits for pre-emption priority - 2 bits for subpriority
                tmpreg = NVIC_EncodePriority(5, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
                break;
            case NVIC_PRIORITY_GROUP3: // 3 bits for pre-emption priority - 1 bits for subpriority
                tmpreg = NVIC_EncodePriority(4, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
                break;
            case NVIC_PRIORITY_GROUP4: // 4 bits for pre-emption priority - 0 bits for subpriority
                tmpreg = NVIC_EncodePriority(3, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
                break;
            default:      // 2 bits for pre-emption priority - 2 bits for subpriority
                tmpreg = NVIC_EncodePriority(5, InitStruct->nvic_IRQChannelPreemptionPriority, InitStruct->nvic_IRQChannelSubPriority);
        }
        NVIC_SetPriority(InitStruct->nvic_IRQChannel, tmpreg);
    
        // Enable the Selected IRQ Channels
        NVIC->ISER[(InitStruct->nvic_IRQChannel >> 0x05)] = (uint32_t)0x01 << (InitStruct->nvic_IRQChannel & (u8)0x1F);
    }
    else
    {
        // Disable the Selected IRQ Channels
        NVIC->ICER[(InitStruct->nvic_IRQChannel >> 0x05)] = (uint32_t)0x01 << (InitStruct->nvic_IRQChannel & (u8)0x1F);
    }
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Fills each InitStruct member with its default value.
//!
//! This function fills each InitStruct member with its default value.
//!
//! \param  nvic_Init_ts        InitStruct is a pointer to a nvic_Init_ts structure to be initialized
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_StructInit(nvic_Init_ts* InitStruct)
{
  InitStruct->nvic_IRQChannel = 0x00;
  InitStruct->nvic_IRQChannelPreemptionPriority = 0x00;
  InitStruct->nvic_IRQChannelSubPriority = 0x00;
  InitStruct->nvic_IRQChannelCmd = DISABLE;
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Sets the vector table location and Offset.
//!
//! This function sets the vector table location and Offset.
//!
//! \param  uint32_t        VectTab specified if the vector table is in RAM or in FLASH
//! \param  uint32_t        Offset specifies the base offset bield. Must be a multiple of 0x100
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void nvic_SetVectorTable(const uint32_t VectTab, const uint32_t Offset)
{  
  SCB->VTOR = VectTab | (Offset & (u32)0x1FFFFF80);
}