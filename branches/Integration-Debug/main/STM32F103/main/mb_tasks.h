// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   mb_tasks.h
//! \brief  MB tasks for main definition
//!
//! ...
//!
//! \author  Dudnik G.S.
//! \date    11.02.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
#ifndef MBTASKS_H_
#define MBTASKS_H_
// -----------------------------------------------------------------------------------
// Exported constants & Macros
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void prvSetupHardware( void );
extern void vApplicationIdleHook( void );
extern void vTaskCB( void *pvParameters );
extern void vTaskSDCARD( void *pvParameters );
// -----------------------------------------------------------------------------------
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
// -----------------------------------------------------------------------------------
extern void vTaskRTMCB( void *pvParameters );
// -----------------------------------------------------------------------------------
#endif                    // 20120608: MB_TASK_CMD_RTMCB
// -----------------------------------------------------------------------------------
#endif /* MBTASKS_H_*/
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
