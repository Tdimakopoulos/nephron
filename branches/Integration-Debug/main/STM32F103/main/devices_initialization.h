// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   devices_initialization.h
//! \brief  inherited from the STM3210C-EVAL board initialization
//!
//! Routines header for initialization of some devices and variables
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Define to prevent recursive inclusion
// -----------------------------------------------------------------------------------
#ifndef __DEVICES_INITIALIZATION_H
#define __DEVICES_INITIALIZATION_H
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported Constants
// -----------------------------------------------------------------------------------
#define PORTA_GPIO_CLK              RCC_APB2Periph_GPIOA
#define PORTB_GPIO_CLK              RCC_APB2Periph_GPIOB
#define PORTC_GPIO_CLK              RCC_APB2Periph_GPIOC
#define PORTD_GPIO_CLK              RCC_APB2Periph_GPIOD
#define PORTE_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PORTAFIO_GPIO_CLK           RCC_APB2Periph_GPIOAFIO

#define MCO_GPIO_PORT               GPIOA
#define MCO_GPIO_CLK                RCC_APB2Periph_GPIOA  
#define MCO_GPIO_PIN                GPIO_Pin_8
// -----------------------------------------------------------------------------------
// RTMCB: Definition for serial ports
// COM1-> USART1 
// COM2-> USART2
// COM3-> USART3 
// COM4-> USART4 
// COM5-> USART5 
// -----------------------------------------------------------------------------------
typedef enum 
{
  COM1 = 0,
  COM2 = 1,
  COM3 = 2,
  COM4 = 3,
  COM5 = 4
} COM_TypeDef;   

#define COMn        5
// ----------------------------------------------------------------
// COM1 DEFINITIONS
// ----------------------------------------------------------------
#define RTMCB_COM1                         USART1
#define RTMCB_COM1_CLK                     RCC_APB2Periph_USART1 
#define RTMCB_COM1_RX_PIN                  GPIO_Pin_10
#define RTMCB_COM1_RX_GPIO_PORT            GPIOA
#define RTMCB_COM1_RX_GPIO_CLK             RCC_APB2Periph_GPIOA
#define RTMCB_COM1_TX_PIN                  GPIO_Pin_9
#define RTMCB_COM1_TX_GPIO_PORT            GPIOA
#define RTMCB_COM1_TX_GPIO_CLK             RCC_APB2Periph_GPIOA
#define RTMCB_COM1_IRQn                    USART1_IRQn
// ----------------------------------------------------------------
// COM2 DEFINITIONS
// ----------------------------------------------------------------
#define RTMCB_COM2                         USART2
#define RTMCB_COM2_CLK                     RCC_APB1Periph_USART2
#define RTMCB_COM2_RX_PIN                  GPIO_Pin_3
#define RTMCB_COM2_RX_GPIO_PORT            GPIOA
#define RTMCB_COM2_RX_GPIO_CLK             RCC_APB2Periph_GPIOA
#define RTMCB_COM2_TX_PIN                  GPIO_Pin_2
#define RTMCB_COM2_TX_GPIO_PORT            GPIOA
#define RTMCB_COM2_TX_GPIO_CLK             RCC_APB2Periph_GPIOA
#define RTMCB_COM2_IRQn                    USART2_IRQn
// ----------------------------------------------------------------
// COM3 DEFINITIONS
// ----------------------------------------------------------------
#define RTMCB_COM3                         USART3
#define RTMCB_COM3_CLK                     RCC_APB1Periph_USART3
#define RTMCB_COM3_RX_PIN                  GPIO_Pin_11
#define RTMCB_COM3_RX_GPIO_PORT            GPIOB
#define RTMCB_COM3_RX_GPIO_CLK             RCC_APB2Periph_GPIOB
#define RTMCB_COM3_TX_PIN                  GPIO_Pin_10
#define RTMCB_COM3_TX_GPIO_PORT            GPIOB
#define RTMCB_COM3_TX_GPIO_CLK             RCC_APB2Periph_GPIOB
#define RTMCB_COM3_IRQn                    USART3_IRQn
// ----------------------------------------------------------------
// COM4 DEFINITIONS
// ----------------------------------------------------------------
#define RTMCB_COM4                         UART4
#define RTMCB_COM4_CLK                     RCC_APB1Periph_UART4
#define RTMCB_COM4_RX_PIN                  GPIO_Pin_11
#define RTMCB_COM4_RX_GPIO_PORT            GPIOC
#define RTMCB_COM4_RX_GPIO_CLK             RCC_APB2Periph_GPIOC
#define RTMCB_COM4_TX_PIN                  GPIO_Pin_10
#define RTMCB_COM4_TX_GPIO_PORT            GPIOC
#define RTMCB_COM4_TX_GPIO_CLK             RCC_APB2Periph_GPIOC
#define RTMCB_COM4_IRQn                    UART4_IRQn
// ----------------------------------------------------------------
// COM5 DEFINITIONS
// ----------------------------------------------------------------
#define RTMCB_COM5                         UART5
#define RTMCB_COM5_CLK                     RCC_APB1Periph_UART5
#define RTMCB_COM5_RX_PIN                  GPIO_Pin_2
#define RTMCB_COM5_RX_GPIO_PORT            GPIOD
#define RTMCB_COM5_RX_GPIO_CLK             RCC_APB2Periph_GPIOD
#define RTMCB_COM5_TX_PIN                  GPIO_Pin_12
#define RTMCB_COM5_TX_GPIO_PORT            GPIOC
#define RTMCB_COM5_TX_GPIO_CLK             RCC_APB2Periph_GPIOC
#define RTMCB_COM5_IRQn                    UART5_IRQn
// ----------------------------------------------------------------
// I2C1 DEFINITIONS [not used in Main Board]
// ----------------------------------------------------------------
#define IOE_I2C                             I2C1
#define IOE_I2C_PORT                        GPIOB
#define IOE_SCL_PIN                         GPIO_Pin_6
#define IOE_SDA_PIN                         GPIO_Pin_7
#define RCC_APB1Periph_IOE_I2C              RCC_APB1Periph_I2C1
#define RCC_APB_IOE_I2C_PORT                RCC_APB2Periph_GPIOB
#define IOE_I2C_SPEED                       100000    // OK measured 
#define I2C_ADDRESS                         0x5A      // 0x5A (WRITE), 0x5B (READ)
#define I2C_MIDSCALE                        0x40
#define I2C_NOSHUTDOWN                      0x00
#define I2C_SHUT_DOWN                       0x20
#define TIMEOUT_MAX                         0x00FFFFFF // 0xFFF /*<! The value of the maximal timeout for I2C waiting loops */
#define IOE_TIMEOUT                         99
#define IOE_FAILURE                         55
// -----------------------------------------------------------------------------------
// Exported Functions
// -----------------------------------------------------------------------------------
extern void STM32F_MCOInit(void);
extern void STM32F_GPIOInit(GPIO_TypeDef* _port, uint32_t _clk, uint16_t _pin, GPIOSpeed_TypeDef _speed, GPIOMode_TypeDef _mode);

extern void STM32F_GPIOOn(GPIO_TypeDef* _port, uint16_t _pin);
extern void STM32F_GPIOOff(GPIO_TypeDef* _port, uint16_t _pin);
extern void STM32F_GPIOToggle(GPIO_TypeDef* _port, uint16_t _pin);

extern void STM32F_COMInit(COM_TypeDef COMx, USART_InitTypeDef* USART_InitStruct); 
extern void USARTx_IRQ_Config(uint8_t USARTx_IRQn, uint8_t USARTx_PreemptivePriority, uint8_t USARTx_SubPriority);
extern void RTMCB_USART1_Init(void);
extern void RTMCB_USART2_Init(void);
extern void RTMCB_USART3_Init(void);
extern void RTMCB_USART4_Init(void);
extern void RTMCB_USART5_Init(void);
extern void RTMCB_USARTs_EnableINT(void);
extern void USART_RX_INT_ENABLE (USART_TypeDef* USARTx);
extern void USART_RX_INT_DISABLE (USART_TypeDef* USARTx);
extern uint16_t USART_READ_CHECKERRORS (USART_TypeDef* USARTx);

extern void EXTIx_IRQ_Config(uint8_t EXTIx_IRQn, uint8_t EXTIx_PreemptivePriority, uint8_t EXITx_SubPriority);
extern void NVIC_SD_Detect(void);

extern void IOE_GPIO_Config(void); 

extern void RTC_NVIC_Configuration(void);
extern void Tamper_NVIC_Configuration(void);

extern void NVIC_IO_EXTI(uint32_t EXTI_Line, uint8_t EXTI_Trigger);
extern uint8_t Update_IO_STATUS (GPIO_TypeDef* _port, uint16_t _pin);
extern void Generate_RTMCB_Interrupt(uint32_t OFFON_delayTime);

extern void DEVICE_IRQ_Config(uint8_t, uint8_t, uint8_t);
extern void TIM1_Config(void);
extern void TIM2_Config(void);
// -----------------------------------------------------------------------------------
#endif 
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
