// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   acc_application.h
//! \brief  ACCELEROMETER (WAKD ATTITUDE) application
//!
//! scheduler and complementary routines
//!
//! \author  Dudnik G.S.
//! \date    20.05.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef ACC_APPLICATION_H_
#define ACC_APPLICATION_H_
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
//#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constants & macros
// -----------------------------------------------------------------------------------
// ACCELEROMETER: LIS331DHL REGISTER ADDRESSES ---------------------------------------
#define LIS331DHL_ID                            0x32
// MSB (OR 1ST)
// read, write, auto address or fixed ------------------------------------------------
#define ACC_READ                                0x80
#define ACC_WRITE                               0x00
#define ACC_AUTOADDRESS                         0x40
// CONFIGURATION ADDRESSES -----------------------------------------------------------
#define ACC_WHO_AM_I                            0x0F
#define ACC_CTRL_REG1                           0x20
#define ACC_CTRL_REG2                           0x21
#define ACC_CTRL_REG3                           0x22
#define ACC_CTRL_REG4                           0x23
#define ACC_CTRL_REG5                           0x24

//STATUS_REG_AXES --------------------------------------------------------------------
#define STATUS_REG				0x27
//OUTPUT REGISTER
#define OUT_X_L				        0x28
#define OUT_X_H				        0x29
#define OUT_Y_L			        	0x2A
#define OUT_Y_H			        	0x2B
#define OUT_Z_L			        	0x2C
#define OUT_Z_H			        	0x2D

//INTERRUPT REGISTERS ----------------------------------------------------------------
#define INT1_CFG				0x30
#define INT1_SRC				0x31
#define INT1_THS                                0x32
#define INT1_DURATION                           0x33
#define INT2_CFG				0x34
#define INT2_SRC				0x35
#define INT2_THS                                0x36
#define INT2_DURATION                           0x37

// LSB (OR 2ND)
// ACCELEROMETER: LIS331DHL REGISTER CONTENTS ----------------------------------------
// CTRL_REG1
#define ACC_NORMAL_ON                           0x20  // NORMAL MODE, RATE=50HZ, XYZ
#define ACC_LOWPWR_1HZ                          0x60  // LOWPOWER, RATE=1HZ, XYZ
#define ACC_LOWPWR_10HZ                         0xC0  // LOWPOWER, RATE=10HZ, XYZ
#define ACC_ZYX_ENABLE                          0x07  // ENABLE 3 AXES
// CTRL_REG3
#define ACC_DATAREADY                           0x10  // INT2 ON DATA READY
#define ACC_LIR2_LATCH                          0x20  // LATCH INT2 UNTIL READ
// CTRL_REG4
#define ACC_DATANOUPD                           0x80
#define ACC_MSBATLADD                           0x40
#define ACC_FS_2GR                              0x00
#define ACC_FS_4GR                              0x10
#define ACC_FS_8GR                              0x30
#define ACC_SELFTESTE                           0x02
// INTX_CFG
#define ACC_6D_DIRECTION                        0x40
#define ACC_ENABLE_X_Y_Z                        0x3F

// INTX_SRC
#define ACC_INTACTIVE                           0x40
// STATUS
#define ACC_STATUS_OVR                          0x80
#define ACC_STATUS_NEW                          0x08
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern uint8_t ACC_Config(void);
extern uint8_t ACC_DataRead(void);
extern void ACC_CFG_READ_XYZ(void);
// -----------------------------------------------------------------------------------
#endif /* ACC_APPLICATION_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
