// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_application.h
//! \brief  UIF application
//!
//! scheduler and complementary routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef UIF_APPLICATION_H_
#define UIF_APPLICATION_H_
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constants & macros
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void MB_UIF_DATASIMULATION(void);
extern void MB_UIF_DATAREALTIME(void);
extern void UIF_ACC_SCHEDULER(uint16_t uif_acc_index);
extern void Update_VBAT1_INFO (void);
extern void Update_VBAT2_INFO (void);
extern void Update_WAKD_STATUS (void);
extern void Update_WAKD_ATTITUDE (void);
extern void Update_WAKD_COMMLINK (void);
extern void Update_WAKD_OPMODE (void);
extern void Update_WAKD_BLCIRCUIT_STATUS (void);
extern void Update_WAKD_FLCIRCUIT_STATUS (void);
extern void Update_WAKD_BLPUMP_INFO (void);
extern void Update_WAKD_FLPUMP_INFO (void);
extern void Update_WAKD_BLTEMPERATURE (void);
extern void Update_WAKD_BLCIRCUIT_PRESSURE (void);
extern void Update_WAKD_FLCIRCUIT_PRESSURE (void);
extern void Update_WAKD_FLCONDUCTIVITY (void);
extern void Update_WAKD_HFD_INFO (void);
extern void Update_WAKD_SU_INFO (void);
extern void Update_WAKD_POLAR_INFO (void);
extern void Update_WAKD_ECPS_INFO (void);
extern void Update_WAKD_PS_INFO (void);
extern void Update_WAKD_ACT_INFO (void);
extern void Update_WAKD_CMD_EXECUTED (void);
extern void Update_WAKD_ALARMS (void);
extern void Update_WAKD_ERRORS (void);
extern void Update_PDATA_INITIALS (void);
extern void Update_PDATA_PATIENTCODE (void);
extern void Update_PDATA_GENDERAGE (void);
extern void Update_PDATA_WEIGHT (void);
extern void Update_PDATA_SEWALL (void);
extern void Update_PDATA_BLPRESSURE (void);
extern void Update_PDATA_ECP1_A (void);
extern void Update_PDATA_ECP1_B (void);
extern void Update_PDATA_ECP2_A (void);
extern void Update_PDATA_ECP2_B (void);

// STATE DIAGRAMS
extern uint8_t SPI_TRIALS_TIMEOUT(void);
extern void SPI_TRIALS_RESET(void);
extern uint8_t STATE_DIAGRAM_TEST_LINK (void);
extern uint8_t STATE_DIAGRAM_WAKD_BLTEMPERATURE (void);
extern uint8_t STATE_DIAGRAM_WAKD_UIF_GET_COMMANDS (void);
extern uint8_t STATE_DIAGRAM_WAKD_DATA_GROUP_1 (void);
// -----------------------------------------------------------------------------------
#endif /* UIF_APPLICATION_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
