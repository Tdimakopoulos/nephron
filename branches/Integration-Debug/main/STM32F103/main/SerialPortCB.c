// ---------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// ---------------------------------------------------------------------------------------------------------------------
//
//! \file   SerialPortCB.c
//! \brief  Test of USART1, USART2, USART3, UART4, UART5
//!
//! Communication Tests
//!
//! \author  Dudnik G.
//! \date    05.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// ---------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

#include <stdint.h>
#include <string.h>

// ---------------------------------------------------------------------------------------------------------------------
// Constant definitions
// ---------------------------------------------------------------------------------------------------------------------
extern xSemaphoreHandle xCBMBSemaphore;

uint32_t CBGivenISR_OK = 0;
uint32_t CBGivenISR_KO = 0;

portBASE_TYPE mCBGivenISR = pdFAIL;
// ---------------------------------------------------------------------------------------------------------------------
// Function definitions
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_CLR_OUT_COM_BUFFERS
//!         
//!
//! Routine to be used in USART_Rx int or main
//!
//! \param[in,out]  none
//! \return         nothing
// ---------------------------------------------------------------------------------------------------------------------
void CB_CLR_OUT_COM_BUFFERS(void){
    out_cb_reclen_0 = 0;
    OUT_COM_CB_FLAG = 0;
}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_Transaction. SLIP Protocol. Reads Data and analyzes when the message is ready. Sends answer.
//!         
//!
//! Routine to be used in USART_Rx int or main.
//! Reads Data and analyzes when the message is ready. Sends answer.
//!
//! \param[in,out]  none
//! \return         nothing
// ---------------------------------------------------------------------------------------------------------------------

/* ORIGINAL FILE - FOR INTEGRATION, REPLACED WITH VERSION BELOW
 
    uint16_t CB_Transaction (USART_TypeDef* USARTx){
    
    portBASE_TYPE xHighPriorityTaskWoken = pdFALSE;

    uint16_t w = 0;
    uint16_t CB_DATALEN = 0;

    uint16_t usart_rxdata = 0xFFFF;
    uint16_t usart_rxchck = 0x0000;

    if (MB_SYSTEM_READY==DEVICE_NOTREADY) return usart_rxchck;

    usart_rxdata =  USART_READ_CHECKERRORS (USARTx);
    usart_rxchck =  usart_rxdata & 0xF000;
    if(usart_rxchck!=0) return usart_rxchck;

    if (OUT_COM_CB_FLAG == 0x80){		
        if(out_cb_reclen_0<SLIP_BLEN){
            if(usart_rxdata == SLIP_END) {
                if(out_cb_reclen_0 != 0) {
                  OUT_COM_CB_FLAG = 0xFF;
                } else {
                  // received end of message for an empty message (two END after each other!)
                  // instead we interpret this as a start.
                }
            }
            else{
                SLIP_RX_CB[out_cb_reclen_0] = usart_rxdata;
                out_cb_reclen_0 ++;
            }	
        }
    }
    if((usart_rxdata == SLIP_END) && (OUT_COM_CB_FLAG != 0x80)&& (OUT_COM_CB_FLAG != 0xFF)) {		
        out_cb_reclen_0 = 0;
        OUT_COM_CB_FLAG = 0x80;
    }
    if(OUT_COM_CB_FLAG == 0xFF) {

#ifdef STM32F_SEMAPHORE_INT
        mCBGivenISR = xSemaphoreGiveFromISR ( xCBMBSemaphore, &xHighPriorityTaskWoken );
        if(mCBGivenISR == pdPASS) CBGivenISR_OK++;
        else CBGivenISR_KO++;
        portEND_SWITCHING_ISR( xHighPriorityTaskWoken );
#else
        // ---------------------------------------------------
        // LoopBack Function
        // ---------------------------------------------------
        // SLIP_RX_CB -->[FROM SLIP]--> RAW_RX_CB_0
        CB_DATALEN = CB_ConvertSliptorawRX(SLIP_RX_CB, out_cb_reclen_0,&RAW_RX_CB_0[0]);
        // VERIFY CRC
#ifdef STM32F_CBCRC
        crc16_cb_ccrx = crc16_compute(RAW_RX_CB_0, CB_DATALEN - 2);
        crc16_cb_recv = (RAW_RX_CB_0[CB_DATALEN - 2] << 8) + RAW_RX_CB_0[CB_DATALEN- 1];
        //if (crc16_cb_ccrx != crc16_cb_recv) return -1;
        //if(CB_DATALEN>=2) CB_DATALEN-=2;
        //else return -2;
#endif
        // RAW_RX_CB_0 --> RAW_TX_CB_0
        CB_DATALEN = CB_Make_LoopBack_Packet(RAW_RX_CB_0, CB_DATALEN, &RAW_TX_CB[0]);
        // ADD CRC
#ifdef STM32F_CBCRC
        crc16_cb_cctx = crc16_compute(RAW_TX_CB, CB_DATALEN);
        RAW_TX_CB[CB_DATALEN] = crc16_cb_cctx>>8;
        RAW_TX_CB[CB_DATALEN+1] = crc16_cb_cctx & 0x00FF;
        CB_DATALEN+=2;
#endif
        // RAW_TX_CB -->[TO SLIP]--> SLIP_TX_CB
        CB_DATALEN = CB_ConvertToSlipTX(RAW_TX_CB, CB_DATALEN, &SLIP_TX_CB[0]);        
        // ---------------------------------------------------
        for(w=0;w<CB_DATALEN;w++) {    
            USART_SendData(USARTx, SLIP_TX_CB[w]);
            while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
        }
        // ---------------------------------------------------
        CB_CLR_OUT_COM_BUFFERS();	
#endif
    }
}
*/

uint16_t CB_Transaction (USART_TypeDef* USARTx){
    
    portBASE_TYPE xHighPriorityTaskWoken = pdFALSE;

    uint16_t w = 0;
    uint16_t CB_DATALEN = 0;

    uint16_t usart_rxdata = 0xFFFF;
    uint16_t usart_rxchck = 0x0000;

    if (MB_SYSTEM_READY==DEVICE_NOTREADY) return usart_rxchck;

    usart_rxdata =  USART_READ_CHECKERRORS (USARTx);
    usart_rxchck =  usart_rxdata & 0xF000;
    if(usart_rxchck!=0) return usart_rxchck;

    if (OUT_COM_CB_FLAG == 0x80){		
        if(out_cb_reclen_0<SLIP_BLEN){
            if(usart_rxdata == SLIP_END) {
              if(out_cb_reclen_0 != 0) {
                OUT_COM_CB_FLAG = 0xFF;
              } else {
                // received end of message for an empty message (two END after each other!)
                // instead we interpret this as a start.
              }
            }
            else{
                SLIP_RX_CB[out_cb_reclen_0] = usart_rxdata;
                out_cb_reclen_0 ++;
            }	
        }
    }
    if((usart_rxdata == SLIP_END) && (OUT_COM_CB_FLAG != 0x80)&& (OUT_COM_CB_FLAG != 0xFF)) {		
        out_cb_reclen_0 = 0;
        OUT_COM_CB_FLAG = 0x80;
    }
    if(OUT_COM_CB_FLAG == 0xFF) {

/*
#ifdef STM32F_SEMAPHORE_INT
        mCBGivenISR = xSemaphoreGiveFromISR ( xCBMBSemaphore, &xHighPriorityTaskWoken );
        if(mCBGivenISR == pdPASS) CBGivenISR_OK++;
        else CBGivenISR_KO++;
        portEND_SWITCHING_ISR( xHighPriorityTaskWoken );
#else*/
        // ---------------------------------------------------
        // LoopBack Function
        // ---------------------------------------------------
        // SLIP_RX_CB -->[FROM SLIP]--> uartBufferCBin
    	if (lockUartBufferCBin == SIGNAL_OFF)
    	{
			CB_DATALEN = CB_ConvertSliptorawRX(SLIP_RX_CB, out_cb_reclen_0,&uartBufferCBin[0]);
			lockUartBufferCBin = SIGNAL_ON;

			portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
			tdQtoken Qtoken;
			Qtoken.command = command_BufferFromCBavailable;
			Qtoken.pData = NULL;
			xQueueSendToBackFromISR(allHandles.allQueueHandles.qhDISPin, &Qtoken, &xHigherPriorityTaskWoken);
    	} else {
    		// discard the buffer we just received. CB will (hopefully) send again.
    		CB_CLR_OUT_COM_BUFFERS();
    	}

//        // RAW_RX_CB_0 --> RAW_TX_CB_0
//        CB_DATALEN = CB_Make_LoopBack_Packet(uartBufferCBin, CB_DATALEN, &RAW_TX_CB[0]);
//        // RAW_TX_CB -->[TO SLIP]--> SLIP_TX_CB
//        CB_DATALEN = CB_ConvertToSlipTX(RAW_TX_CB, CB_DATALEN, &SLIP_TX_CB[0]);
//        // ---------------------------------------------------
//        for(w=0;w<CB_DATALEN;w++) {
//            USART_SendData(USARTx, SLIP_TX_CB[w]);
//            while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
//        }
//        // ---------------------------------------------------
//        CB_CLR_OUT_COM_BUFFERS();
//#endif
    }
}




// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_SendPacket
//!         
//!
//! Prepares a Message and Sends through USARTx port
//!
//! \param[in]      USARTx
//! \return         no of bytes sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t CB_SendPacket (USART_TypeDef* USARTx){
    uint16_t iq = 0;
    // ---------------------------------------------------
    // LoopBack Function
    // ---------------------------------------------------
    OUT_STR_TX_LEN = Make_Testing_Packet(&ztx_out_str[0]);
    // ---------------------------------------------------
    for(iq=0;iq<OUT_STR_TX_LEN;iq++) {    
        USART_SendData(USARTx, ztx_out_str[iq]);
        while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET) {}
    }
    // ---------------------------------------------------
    return OUT_STR_TX_LEN;
}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_Make_LoopBack_Packet
//!         
//!
//! Prepares a return packet with the data received
//!
//! \param[in]      pointer to the reception buffer (by value)
//! \param[in]      quantity of data received
//! \param[in]      pointer to the transmission buffer (by reference)
//! \return         quantity of data to be sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t CB_Make_LoopBack_Packet(uint8_t * rx_inp_str, uint16_t rxpacketlen, uint8_t * tx_out_str){	

    uint16_t ix = 0;
    if(rxpacketlen >= SLIP_BLEN) rxpacketlen = SLIP_BLEN;
    for(ix=0; ix < rxpacketlen; ix++)           tx_out_str[ix] = rx_inp_str[ix];
    for(ix= rxpacketlen; ix < SLIP_BLEN; ix++)	tx_out_str[ix] = '\0';
    
    return rxpacketlen;
}

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_ConvertSliptorawRX
//!         
//!
//! Converts the received SLIP message to raw data
//!
//! \param[in]      pointer to the SLIP reception buffer (by reference)
//! \param[in]      quantity of SLIP received data
//! \param[in]      pointer to the raw reception buffer (by reference)
//!
//! \return         quantity of raw data received
// ---------------------------------------------------------------------------------------------------------------------
uint16_t CB_ConvertSliptorawRX(uint8_t *slip_rx_str, uint16_t sliprxlen,uint8_t *raw_rx_str)
{
    uint16_t x = 0;
    uint8_t D1 = 0, D2 = 0;
    uint16_t idx = 0;
    if (sliprxlen>SLIP_BLEN) sliprxlen = SLIP_BLEN;
    do{
        // handle bytestuffing if necessary
        D1 = slip_rx_str[x]; x++;        
        switch (D1)
        {
            // if it�s an END character then we�re done with the packet
            // already done outside...
            case SLIP_ESC:
                // if "c" is not one of these two, then we have a protocol violation. 
                // The best bet seems to be to leave the byte alone and * just stuff it into the packet
                D2 = slip_rx_str[x]; x++; 
                switch (D2)
                {
                    case SLIP_ESC_END:
                        raw_rx_str[idx] = SLIP_END; idx++;

                        break;
                    case SLIP_ESC_ESC:
                        raw_rx_str[idx] = SLIP_ESC; idx++;
                        break;
                }
            break;
            // here we fall into the default handler and let it store the character for us
            default:
                raw_rx_str[idx] = D1; idx++;
                break;
        }
        if(idx>= SLIP_BLEN) break;
    }while(x<sliprxlen);
    return idx;
}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_ConvertToSlipTX
//!         
//!
//! Converts the message to be sent to SLIP Protocol
//!
//! \param[in]      pointer to the raw transmission buffer (by reference)
//! \param[in]      quantity of raw data for transmission
//! \param[in]      pointer to the SLIP transmission buffer (by reference)
//!
//! \return         quantity of data to be sent
// ---------------------------------------------------------------------------------------------------------------------
uint16_t CB_ConvertToSlipTX(uint8_t *raw_tx_str, uint16_t rawtxlen, uint8_t *slip_tx_str)
{
    uint16_t x = 0;
    uint8_t D1 = 0;
    uint16_t idx = 0;

    if (rawtxlen>SLIP_BLEN) rawtxlen = SLIP_BLEN;
    slip_tx_str[idx] = SLIP_END; idx++;
    // for each byte in the packet, send the appropriate character sequence 
    do{
        D1 = raw_tx_str[x]; x++;
        switch (D1)
        {
            // if it�s the same code as an END character, we send a special two character 
            // code so as not to make the receiver think we sent an END
            case SLIP_END:
                slip_tx_str[idx] = SLIP_ESC; idx++;
                slip_tx_str[idx] = SLIP_ESC_END; idx++;
                break;
            // if it�s the same code as an ESC character, we send a special two character 
            // code so as not to make the receiver think we sent an ESC
            case SLIP_ESC:
                slip_tx_str[idx] = SLIP_ESC; idx++;
                slip_tx_str[idx] = SLIP_ESC_ESC; idx++;
                break;
            // otherwise, we just send the character
            default:
                slip_tx_str[idx] = D1; idx++;
                break;
        }
        if(idx>= SLIP_BLEN) break;
    }while(x<rawtxlen);
    // tell the receiver that we�re done sending the packet 
    slip_tx_str[idx] = SLIP_END; idx++;
    for(x=idx;x<SLIP_BLEN;x++)slip_tx_str[x]=0;
    return idx;
}
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
