// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_acc_spi0.c
//! \brief  spi0 for accelerometer (attitude measurement) and UIF 
//!
//! spi0 initialization, send & receive data routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
#define DELAY_SPI   25
// -----------------------------------------------------------------------------------
//! \brief  UIF_ACC_SPIConfig
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void UIF_ACC_SPIConfig(uint8_t uifacc)
{

  SPI_InitTypeDef    SPI_InitStructure;

  SPI_I2S_DeInit(UIF_ACC_SPI);
 // GPIO CONFIGURED IN RTMCB_GPIO_Config
 
  // Enable SPI clock 
  RCC_APB1PeriphClockCmd(UIF_ACC_SPI_CLK, ENABLE); 
   
  // SPI Config 
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  switch(uifacc){
      case DEV_ACC:
          SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
          SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;   // inactive high
          SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;  // SPI_CPHA_1Edge
      break;
      case DEV_UIF:
      default:
          SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
          // inactive low (I can change but also have to change in MSP)
          SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;    
          SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;  // SPI_CPHA_1Edge;
      break;
  }
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB; 
  // SPI_NSS_Soft --> SD_NCS_PIN configure as OUT_PP 
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; //128 ok

  SPI_Init(UIF_ACC_SPI, &SPI_InitStructure);

  // SPI enable
  SPI_Cmd(UIF_ACC_SPI, ENABLE);
}

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// UIF
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  UIF_SendReceiveHWSW
//!
//! Read UIF SPI DATA
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint8_t UIF_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
  uint32_t timeout_counter = 0;
    
  if(mode) STM32F_GPIOOff(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  SPI_I2S_SendData(UIF_ACC_SPI, _dataIN);
  // 05.02.2012
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0 
  timeout_counter = 0;
  while((SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY) != RESET)&&(timeout_counter<SPI_TIMEOUT)){
      timeout_counter++; DelayBySoft (1); 
  }
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while((SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET)&&(timeout_counter<SPI_TIMEOUT)){
      timeout_counter++; DelayBySoft (1); 
  }
  _dataOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (100); // 100 OK
  if(mode) STM32F_GPIOOn(MSPARM_STE_GPIO_PORT, MSPARM_STE_GPIO_PIN);
  DelayBySoft (100); // 100 OK
  return _dataOUT;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// ACCELEROMETER
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  ACC_Read
//!
//! Read ACC SPI DATA
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint16_t ACC_Read(uint8_t _regNum)
{
  uint16_t _dataINOUT = 0;
    
  STM32F_GPIOOff(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
  DelayBySoft (1); // 100 OK  
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY)!= RESET);
  _dataINOUT = (_regNum | ACC_READ)<<8;  
  SPI_I2S_SendData(UIF_ACC_SPI, _dataINOUT);
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _dataINOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (1); // 100 OK
  STM32F_GPIOOn(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
  DelayBySoft (1); // 100 OK
  return _dataINOUT & 0x00FF;
}

// -----------------------------------------------------------------------------------
//! \brief  ACC_SendCommandHWSW
//!
//! Read ACC SPI DATA
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint16_t ACC_Write(uint8_t _regNum, uint8_t _value)
{
  uint16_t _dataINOUT = 0;
    
  STM32F_GPIOOff(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
  DelayBySoft (1); // 100 OK  
  // SPI_I2S_FLAG_BSY = 1 BUSY, BUT MASTER, BIDIR, RX ALWAYS 0
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_BSY)!= RESET);
  _dataINOUT = (_regNum<<8) |_value;  
  SPI_I2S_SendData(UIF_ACC_SPI, _dataINOUT);
  // SPI_I2S_FLAG_TXE = 1 EMPTY
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_TXE) == RESET);
  // SPI_I2S_FLAG_RXNE = 1 VALID DATA 
  while(SPI_I2S_GetFlagStatus(UIF_ACC_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _dataINOUT = SPI_I2S_ReceiveData(UIF_ACC_SPI); 
  DelayBySoft (1); // 100 OK
  STM32F_GPIOOn(ACC_SPI_nCS_GPIO_PORT, ACC_SPI_nCS_GPIO_PIN);
  DelayBySoft (1); // 100 OK
  return _dataINOUT & 0x00FF;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
