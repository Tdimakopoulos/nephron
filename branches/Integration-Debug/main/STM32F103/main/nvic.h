// ---------------------------------------------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// ---------------------------------------------------------------------------------------------------------------------
//
//! \file   nvic.h
//! \brief  NVIC functions
//!
//! This module provides the functions to configure the NVIC
//!
//! \author  Kaeser C.
//! \date    28.10.2010
//! \version 1.0 First version (CKs)
// ---------------------------------------------------------------------------------------------------------------------

#ifndef _NVIC_H_
#define _NVIC_H_


// ---------------------------------------------------------------------------------------------------------------------
// Include
// ---------------------------------------------------------------------------------------------------------------------
#include <stm32f10x.h>
#include "global.h"

// ---------------------------------------------------------------------------------------------------------------------
// Exported constant & macro definitions
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Exported type definitions
// ---------------------------------------------------------------------------------------------------------------------
typedef struct
{
  uint8_t nvic_IRQChannel;                   //!< Specifies the IRQ channel to be enabled or disabled.Value: IRQn_Type 
                                             //!< (For the complete STM32 Devices IRQ Channels list, please
                                             //!< refer to stm32f10x.h file)
  uint8_t nvic_IRQChannelPreemptionPriority; //!< Specifies the pre-emption priority for the IRQ channel. Value: 0 to 15
  uint8_t nvic_IRQChannelSubPriority;        //!< Specifies the subpriority level for the IRQ channel. Value: 0 to 15
  FunctionalState nvic_IRQChannelCmd;        //!< Specifies whether the IRQ channel will be ENABLE or DISABLE  
} nvic_Init_ts;


// PREEMPTION PRIORITY GROUP
#define NVIC_PRIORITY_GROUP0    ((uint32_t) 0x700) // 0 bits for pre-emption priority - 4 bits for subpriority
#define NVIC_PRIORITY_GROUP1    ((uint32_t) 0x600) // 1 bits for pre-emption priority - 3 bits for subpriority
#define NVIC_PRIORITY_GROUP2    ((uint32_t) 0x500) // 2 bits for pre-emption priority - 2 bits for subpriority
#define NVIC_PRIORITY_GROUP3    ((uint32_t) 0x400) // 3 bits for pre-emption priority - 1 bits for subpriority
#define NVIC_PRIORITY_GROUP4    ((uint32_t) 0x300) // 4 bits for pre-emption priority - 0 bits for subpriority
                    
// Vector Table Base
#define NVIC_VECTTAB_RAM        ((uint32_t) 0x20000000)
#define NVIC_VECTTAB_FLASH      ((uint32_t) 0x08000000)
                    
// ---------------------------------------------------------------------------------------------------------------------
// Exported data
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// Exported inline code definitions
// ---------------------------------------------------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Clears the pending bit for an interrupt.
//!
//! This function Clears the pending bit for an interrupt.
//!
//! \param  IRQn_Type       IRQn specified the IRQ vector number
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
static inline void nvic_ClearIRQPendingBit(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}


// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Gets the pending bit for an interrupt.
//!
//! This function gets the pending bit for an interrupt.
//!
//! \param  IRQn_Type   IRQn specified the IRQ vector number
//!
//! \return IRQn_Type   IRQ specified vector number or 0
// ---------------------------------------------------------------------------------------------------------------------
static inline IRQn_Type nvic_GetPendingIRQ(IRQn_Type IRQn)
{
  return((IRQn_Type) (NVIC->ISPR[(uint32_t)(IRQn) >> 5] & (1 << ((uint32_t)(IRQn) & 0x1F))));
}

// ---------------------------------------------------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------------------------------------------------
extern void nvic_Reset(void);
extern void nvic_SCBReset(void);
extern void nvic_PriorityGroupConfig(const uint32_t PriorityGroup);
extern void nvic_Init(const nvic_Init_ts* InitStruct);
extern void nvic_StructInit(nvic_Init_ts* InitStruct);
extern void nvic_SetVectorTable(const uint32_t VectTab, const uint32_t Offset);

#endif // _NVIC_H_ //
