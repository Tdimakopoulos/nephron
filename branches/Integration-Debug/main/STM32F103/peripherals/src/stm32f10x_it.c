// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   stm32f10x_it.c
//! \brief  main interrupt routine
//!
//! Main Interrupt Service Routines.
//!  This file provides template for all exceptions handler and 
//!  peripherals interrupt service routine.
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported Constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
uint32_t tim1_counter = 0;
uint32_t tim2_uif_counter = 0;
uint32_t tim2_acc_counter = 0;

uint32_t rtmcb_uart4_dma_counter1 = 0;
uint32_t rtmcb_uart4_dma_counter2 = 0;

// HW FAULT TESTS
uint16_t BUS_FAULT_COUNTER = 0;
// -----------------------------------------------------------------------------------
// Private functions
// Cortex-M3 Processor Exceptions Handlers 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  NMI_Handler
//!
//! This function handles NMI exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void NMI_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  HardFault_Handler
//!
//! This function handles Hard Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void HardFault_Handler(void)
{
  uint8_t errorHW=0;
  uint32_t hfsr_rd =0;
  uint8_t errorMM=0;
  uint8_t errorBF=0;
  uint16_t errorUF=0;

  hfsr_rd = HardFault_Read(HFSR);
  if((hfsr_rd == 0x00000002)|| (hfsr_rd == 0x00000004)) {
      errorMM = MemoryManagementFault_Read(MMFSR);
      errorBF = BusFault_Read(BFSR);
      BusFault_Exception_Clear();
      errorUF = UsageFault_Read(UFSR);
  }
}
// -----------------------------------------------------------------------------------
//! \brief  MemManage_Handler
//!
//! This function handles Memory Manage exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void MemManage_Handler(void)
{
  // Go to infinite loop when Memory Manage exception occurs 
  while (1)
  {
  }
}
// -----------------------------------------------------------------------------------
//! \brief  BusFault_Handler
//!
//! This function handles Bus Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void BusFault_Handler(void)
{

  BusFault_Exception_Clear();
  BUS_FAULT_COUNTER++;
}
// -----------------------------------------------------------------------------------
//! \brief  UsageFault_Handler
//!
//! This function handles Usage Fault exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void UsageFault_Handler(void)
{
  // Go to infinite loop when Usage Fault exception occurs 
  while (1)
  {
  }
}
// -----------------------------------------------------------------------------------
//! \brief  SVC_Handler
//!
//! This function handles SVCall exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void SVC_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  DebugMon_Handler
//!
//! This function handles Debug Monitor exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void DebugMon_Handler(void)
{
}
// -----------------------------------------------------------------------------------
//! \brief  PendSV_Handler
//!
//! This function handles PendSVC exception
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void PendSV_Handler(void)  
{
}
// -----------------------------------------------------------------------------------
// This function handles SysTick Handler
// 
// RTMCB: SysTick is generated every 1ms
//
// Inputs : 
//          none
// 
// Output : none
// -----------------------------------------------------------------------------------
void SysTick_Handler(void) 
{  
 
#ifndef MBFREERTOS

  // This function decrements a counter to create delays.
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
  // MB TimeStamp
  globalTimestamp++;
  
//  if (LOCK_CRITICAL_INTS==0){
    ADCDMACounter++;
    if(ADCDMACounter == ADCDMA_SAMPLEPERIOD){
        ADCDMACounter = 0;
        /* Start ADC1 Software Conversion */ 
        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    }
//  }

#ifdef STM32F_LED_INT_TICK
    led_toggle_counter++;
    if(led_toggle_counter == LED_TOGGLE_PERIOD){
        led_toggle_counter = 0;
        STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
    }
#endif

    uif_acc_data_counter++;
    if(uif_acc_data_counter == UIF_ACC_DATA_SIMUL){
        uif_acc_data_counter = 0;
#ifdef UIF_SIMULATION
        MB_UIF_DATASIMULATION();
#else
        MB_UIF_DATAREALTIME();
#endif    }

    uif_acc_cmd_counter++;
    if(uif_acc_cmd_counter == 5000){
        uif_acc_cmd_counter = 0;
        MB_USER_COMMAND = 0;
    }

#endif

}

// -----------------------------------------------------------------------------------
//! \brief  PVD_IRQHandler
//!
//! This function handles PVD_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void PVD_IRQHandler(void)
{
  BKP_TamperPinCmd(DISABLE);

  EXTI_ClearITPendingBit(EXTI_Line16);
  NVIC_ClearPendingIRQ(PVD_IRQn);
}
// -----------------------------------------------------------------------------------
//! \brief  RTC_IRQHandler
//!
//! This function handles RTC_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void RTC_IRQHandler(void)
{

  NVIC_ClearPendingIRQ(RTC_IRQn);
  RTC_ClearITPendingBit(RTC_IT_SEC);
  // do not use Calculate Time Routine here
  // causes hardware fault when writing sdcard
  
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI0_IRQHandler
//!
//! This function handles EXTI0_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI0_IRQHandler(void)
{

  if(EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line0);
    // DO THE TASK
  }
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI1_IRQHandler
//!
//! This function handles EXTI1_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI1_IRQHandler(void)
{
 
  if(EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line1);
    // DO THE TASK
      //LINE_RTMCB_uC_SYNC_STATUS = 
      Update_IO_STATUS (RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_PIN);
  }
}


// -----------------------------------------------------------------------------------
//! \brief  EXTI4_IRQHandler
//!
//! This function handles EXTI4_IRQHandler: SD_CARD INSERTED OR NOT
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI4_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line4) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line4);  
    // DO THE TASK	
    LINE_RTMCBuC_nIRQ = Update_IO_STATUS (RTMCBuC_nIRQ_GPIO_PORT, RTMCBuC_nIRQ_GPIO_PIN);    
    if(LINE_RTMCBuC_nIRQ == SIGNAL_OFF) {
        RTMCB_UART4_DMA_ReadData();
        rtmcb_uart4_dma_counter1++;
    }
  }
}

// -----------------------------------------------------------------------------------
//! \brief  EXTI9_5_IRQHandler
//!
//! This function handles EXTI9_5_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI9_5_IRQHandler(void)
{
  uint32_t DelayBat;
  
  if(EXTI_GetITStatus(EXTI_Line8) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line8);    
  }

  // Battery Removal and Restore detection  
  if(EXTI_GetITStatus(EXTI_Line9) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line9);
  }
}
// -----------------------------------------------------------------------------------
//! \brief  EXTI15_10_IRQHandler
//!
//! This function handles EXTI15_10_IRQHandler .
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void EXTI15_10_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line10) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line10);
  }

  if(EXTI_GetITStatus(EXTI_Line11) != RESET)
  {
      EXTI_ClearITPendingBit(EXTI_Line11);
  }
}
// -----------------------------------------------------------------------------------
//! \brief  RTCAlarm_IRQHandler
//!
//! This function handles RTCAlarm_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void RTCAlarm_IRQHandler(void)
{
  // Clear the Alarm Pending Bit 
  RTC_ClearITPendingBit(RTC_IT_ALR);

  // Clear the EXTI Line 17 
  EXTI_ClearITPendingBit(EXTI_Line17);
}
// -----------------------------------------------------------------------------------
//! \brief  TIM1_UP_IRQHandler
//!
//! This function handles TIM1 overflow and update interrupt request
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
// 21.01.2011: current period = 6.25ms
void TIM1_UP_TIM10_IRQHandler(void)
{
    uint16_t w = 0;
    uint16_t OUT_STR_TX_LEN_RTMCB = 0;
    // Clear the TIM1 Update pending bit 
    TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

#ifdef MB_TASK_CMD_RTMCB
    
    if (MB_SYSTEM_READY==DEVICE_READY){
        // debug
        tim1_counter++;
#ifdef STM32F_LED_TIM1
        STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);   // PERIOD 12mS     
#endif

#ifndef MB_GATEWAY_RTMCB_PC // process here, not redirect data from USB to RTMCB directly

        // OUT_COMM_RTMCB_FLAG = 0xFF, a full message has been received
        if(OUT_COMM_RTMCB_FLAG == 0xFF){
            if(RTMCB_DMA_RX_NEW == SIGNAL_ON){
                for(w=0; w<out_rtmcb_reclen_0;w++){
                    zrx_out_rtmcb_str_1[w] =  zrx_out_rtmcb_str_0[w];
                    zrx_out_rtmcb_str_0[w] = 0;
                }
                out_rtmcb_reclen_1 = out_rtmcb_reclen_0;
                out_rtmcb_reclen_0 = 0;
                RTMCB_DMA_RX_NEW = SIGNAL_OFF;
            }

            if((out_rtmcb_reclen_1>0)&&(out_rtmcb_reclen_1<=TXRX_RS422_RTMCB_MAXVALUE)) {
                OUT_STR_TX_LEN_RTMCB = DECOBS_DECRC16_PACKETS(zrx_out_rtmcb_str_1, out_rtmcb_reclen_1, &BufferXYZ_RTMCB[0], &RAW_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
                if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
                    // RECEIVED SOMETHING FROM RTMCB
                    rs422_rtmcb_received_counter++;   // COUNTS RECEPTION EVENTS
                    rs422_rtmcb_whichgroup = RS422_NOGROUP;
                    RS422_ANALYZE_RESPONSE(OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &RAW_Buffer_RTMCB[0], &OK_RTMCB[0]);
                    OUT_STR_TX_LEN_RTMCB = 0; // no meaning for this value as sender....
                    
                    if ( RTMCB_message == dataID_physicalData ){
                               physicals_timer++;
                              //RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_physicalData;
                    }
                    
                    
                    if(rs422_rtmcb_whichgroup!=RS422_NOGROUP){
                        // ------------------------------------------ //
                        // IF rs422_rtmcb_whichgroup != RS422_NOGROUP MESSAGE SUCCEEDED!
                        // MESSAGES FOR MB TASK                             
                        RTMCB_MBTASK_GETDATA_WHICHGROUP = rs422_rtmcb_whichgroup;
                        RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = RTMCB_message;                        
                        
                        if ( RTMCB_message == dataID_physicalData ){
                              // physicals_timer++;
                              // RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_physicalData;
                        }

                       if(RTMCB_MBTASK_GETDATA_WHICHGROUP == RS422_ACK_RECV_OPMODES) opmode_packets_received++;

                        #ifndef DEMO_BYPASS 
                          // FLAG BELOW WAS USED FOR DEMO, TO INFORM DATA AVAILABLE
                          RTMCB_TASK_NEW_MESSAGE = SIGNAL_ON; // SIGNAL TO THE TASK                        
                        #else
                          
                          /*
                          // OPMODES ARE HANDLED AT TOP OF RTBPA, NOT IN BUFFER FROM RTB
                          if((RTMCB_MBTASK_GETDATA_WHICHGROUP == RS422_ACK_RECV_OPMODES){
                              RTMCB_TASK_NEW_MESSAGE = SIGNAL_ON;
                          }*/

                          // NOW WE INFORM THE RTBPA task with a message BUFFERFROMRTMCB AVAILABLE
                          RTMCB_TASK_NEW_MESSAGE = SIGNAL_ON;   // We do this also... for RTMCB_STATUS
                          portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
                          tdQtoken Qtoken;
                          Qtoken.command = command_BufferFromRTBavailable;
                          Qtoken.pData	= NULL;
                          if ( xQueueSendToBackFromISR( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, &xHigherPriorityTaskWoken) != pdPASS )
                          {	 // Seemingly the queue is full and we already waited for a long time
                                 #ifndef HWBUILD 
                                 taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'command_BufferFromRTBavailable'");
                                 #endif
                                 errMsgISR(errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask);
                          }                 
                          
                        #endif

                        // ------------------------------------------ //
                        switch(rs422_rtmcb_whichgroup){
                            case RS422_ACK_RTMCB_READY:
                                rs422_rtmcb_sequenceno++;
                                OUT_STR_TX_LEN_RTMCB = RS422_MAKE_ANSWER_NP_SYS_ACKRTMCBREADY (&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
                                // NEXT STEP, SEND SOME COMMANDS...
                                RTMCB_SimulationNoRT = SIMULATION_MODE; 
                                rs422_rtmcb_commands_scheduler[0] = RS422_SIMULATIONVSRT;
                            break;
                            case RS422_SIMULATIONVSRT:
                                RTMCB_StreamingYesNo = STREAMING_ON;
                                rs422_rtmcb_commands_scheduler[0] = RS422_STSTSTREAMING;

                            break;
                            case RS422_STSTSTREAMING:
                                rs422_rtmcb_commands_scheduler[0] = RS422_NOGROUP;
                                rtmcb_cmd_streaming = SIGNAL_ON;
                            break;
                            case RS422_ACK_RECV_DATA:
                                if (RTMCB_STATUS==INTERVAL){
                                  rs422_rtmcb_sequenceno++;
                                  OUT_STR_TX_LEN_RTMCB = RS422_MAKE_ANSWER_NP_DATA_ACKINTERVAL (&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
                                }
                            break;
                        }
                    }
                    if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
                        OUT_STR_TX_LEN_RTMCB = PACKETS_CRC16_COBS (RAW_Buffer_RTMCB, OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &COB_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
                        if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
                            for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {    
                                USART_SendData(UART4, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
                                while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET) {}
                            } // end send
                            // Generates INTERRUPT to let RTMCB Know about the new Data
                            Generate_RTMCB_Interrupt(100);   // 100=20uS (tEXTIpw = 10ns)
#ifdef PC_SPY_MB_RTMCB
                            // REPEATER TO USB (PC)
                            for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {    
                                USART_SendData(USART1, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
                                while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {}
                            } // end send
#endif
                            rs422_tx_rtmcb_counter=0;
                        } // end if cobbed message len > 0
                    } // end if message len > 0
                } // end if de-cobbed message len > 0
            } // end if received message len > 0
            CLR_RTMCB_COMM_BUFFERS();	
        } // OUT_COMM_FLAG = 0xFF

        // theoretically 100ms
        rs422_timecounter++;
        if(rs422_timecounter == RS422_RTMCB_TX_CNT){
            rs422_timecounter = 0;

            // if there is a command to be sent to RTMCB
            if((rs422_rtmcb_whichgroup !=RS422_ACK_RTMCB_READY) && ((rs422_rtmcb_commands_scheduler[0]!=RS422_NOGROUP)||(RTMCB_TASK_NEW_MB_CMD == SIGNAL_ON))){          
                rs422_rtmcb_sequenceno++;

                // RTMCB TASK OVERWRITES ANY OTHER BUT OTHERS CAN SEND COMMANDS ALSO (IF TIME ALLOWS)
                if(RTMCB_TASK_NEW_MB_CMD == SIGNAL_ON){   // it comes from RTMCB TASK
                    rs422_rtmcb_commands_scheduler_bkp = rs422_rtmcb_commands_scheduler[0];
                    rs422_rtmcb_commands_scheduler[0] = RTMCB_MBTASK_PUTDATA_WHICHGROUP;
                }

                OUT_STR_TX_LEN_RTMCB = RS422_RTMCB_SEND_COMMAND (0);
                if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
                    OUT_STR_TX_LEN_RTMCB = PACKETS_CRC16_COBS (RAW_Buffer_RTMCB, OUT_STR_TX_LEN_RTMCB, &BufferXYZ_RTMCB[0], &COB_Buffer_RTMCB[0],TXRX_RS422_RTMCB_MAXVALUE);
                    if((OUT_STR_TX_LEN_RTMCB>0)&&(OUT_STR_TX_LEN_RTMCB<=TXRX_RS422_RTMCB_MAXVALUE)) {
                        for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {    
                            USART_SendData(UART4, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
                            while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET) {}
                        } // end send
                        // Generates INTERRUPT to let RTMCB Know about the new Data
                        Generate_RTMCB_Interrupt(100);   // 100=20uS (tEXTIpw = 10ns)
#ifdef PC_SPY_MB_RTMCB
                            // REPEATER TO USB (PC)
                            for(rs422_tx_rtmcb_counter=0;rs422_tx_rtmcb_counter<OUT_STR_TX_LEN_RTMCB;rs422_tx_rtmcb_counter++) {    
                                USART_SendData(USART1, COB_Buffer_RTMCB[rs422_tx_rtmcb_counter]);
                                while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {}
                            } // end send
#endif
                        OUT_STR_TX_LEN_RTMCB=0;
                    } // end if cobbed message len >0
                } // end if message len > 0

                // RTMCB TASK OVERWRITES ANY OTHER BUT OTHERS CAN SEND COMMANDS ALSO (IF TIME ALLOWS)
                if(RTMCB_TASK_NEW_MB_CMD == SIGNAL_ON){   // it comes from RTMCB TASK
                    rs422_rtmcb_commands_scheduler[0] = rs422_rtmcb_commands_scheduler_bkp;
                    RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_NOGROUP;
                    RTMCB_TASK_NEW_MB_CMD = SIGNAL_OFF;
                    rtmcb_task_sent_counter++;            // COUNTS TRANSMISSION EVENTS (MB-TASK)
                }

            } // end if command to
            // if there is new data received from RTMCB to be transferred to internal buffer
            if (RTMCB_STATUS==INTERVAL){
#ifndef INTER_INTEGRATION_201103
                EXECUTE_SENSORS_ACTUATORS_UPDATE();
#endif
                RTMCB_STATUS = NONE;
            }
        } // end if rs422_timecounter = RS422_RTMCB_TX_CNT (100ms)
        

#endif // #ifndef MB_GATEWAY_RTMCB_PC process here, not redirect data from USB to RTMCB directly

    } // end if mb ready
#endif // if MB_TASK_CMD_RTMCB
}
// -----------------------------------------------------------------------------------
//! \brief  TIM2_IRQHandler
//!
//! This function handles TIM2_IRQHandler
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
// UIF+ACC
// -----------------------------------------------------------------------------------
void TIM2_IRQHandler(void)
{
  uint8_t completed = 0;

  // WATCH OUT!!!!!
  if(USART_GetITStatus(USART2, USART_IT_RXNE)==SET)  return;

  TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

  if (MB_SYSTEM_READY==DEVICE_READY){

      // debug
      tim2_uif_counter++;
 
#ifdef STM32F_LED_TIM2
  STM32F_GPIOOn(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif

    switch(UIF_ACC_CMD_NO){      // switch case
        // TEST LINK
        case MB_UIF_SCHNO_TEST_LINK:
            completed = STATE_DIAGRAM_TEST_LINK();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_BLTEMPERATURE;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_BLTEMPERATURE);
                uif_acc_trial_counter = 0;
            }
        break;
        // SEND INFO
        case MB_UIF_SCHNO_WAKD_BLTEMPERATURE:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_VBAT1_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_VBAT1_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_VBAT1_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_VBAT2_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_VBAT2_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_VBAT2_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_ATTITUDE; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_ATTITUDE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_ATTITUDE:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_STATUS; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_STATUS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_STATUS:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_OPMODE; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_OPMODE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_OPMODE:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_BLPUMP_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_BLPUMP_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_BLPUMP_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_FLPUMP_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_FLPUMP_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_FLPUMP_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_COMMLINK; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_COMMLINK);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_COMMLINK:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_HFD_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_HFD_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_HFD_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_SU_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_SU_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_SU_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_POLAR_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_POLAR_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_POLAR_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_ECPS_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_ECPS_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_ECPS_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_PS_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_PS_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_PS_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_ACT_INFO; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_ACT_INFO);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_WAKD_ACT_INFO:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_GET_COMMANDS; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_GET_COMMANDS);
                uif_acc_trial_counter = 0;
            }
        break;
        // READ COMMANDS
        case MB_UIF_SCHNO_WAKD_GET_COMMANDS:  
            completed = STATE_DIAGRAM_WAKD_UIF_GET_COMMANDS();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_WAKD_ACK_EXECUTED;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_WAKD_ACK_EXECUTED);
                uif_acc_trial_counter = 0;
            }
        break;
        // COMMANDS ACKNOWKEDGE
        case MB_UIF_SCHNO_WAKD_ACK_EXECUTED:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_ECP1_A; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_ECP1_A);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_ECP1_A:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_ECP1_B; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_ECP1_B);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_ECP1_B:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_ECP2_A; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_ECP2_A);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_ECP2_A:
            completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_ECP2_B; 
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_ECP2_B);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_ECP2_B:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_INITIALS;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_INITIALS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_INITIALS:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_PATIENTCODE;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_PATIENTCODE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_PATIENTCODE:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_GENDERAGE;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_GENDERAGE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_GENDERAGE:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_WEIGHT;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_WEIGHT);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_WEIGHT:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_SEWALL;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_SEWALL);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_SEWALL:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_PDATA_BLPRESSURE;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_PDATA_BLPRESSURE);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_PDATA_BLPRESSURE:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_ALARMS;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_ALARMS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_ALARMS:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_ERRORS;
                UIF_ACC_SCHEDULER(MB_UIF_SCHNO_ERRORS);
                uif_acc_trial_counter = 0;
            }
        break;
        case MB_UIF_SCHNO_ERRORS:
         completed = STATE_DIAGRAM_WAKD_DATA_GROUP_1();
            if(completed) {
                ACC_CFG_READ_XYZ();
                UIF_ACC_CMD_NO = MB_UIF_SCHNO_TEST_LINK;
                uif_acc_trial_counter = 0;
            }
        break;
        } // end switch case

#ifdef STM32F_LED_TIM2
  STM32F_GPIOOff(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
#endif 
  }
}
// -----------------------------------------------------------------------------------
//! \brief  USART1_IRQHandler
//!
//! This function handles USART1_IRQHandler (USB-MAINTENANCE)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART1_IRQHandler(void){
    USB_Transaction ();
    //SerialPort_Transaction(USART1);
  
}
// -----------------------------------------------------------------------------------
//! \brief  USART2_IRQHandler
//!
//! This function handles USART2_IRQHandler (CB/WCM)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART2_IRQHandler(void){
    if(USART_GetITStatus(USART2, USART_IT_RXNE)==SET){
#ifdef STM32_USART2_LBACK    
        CB_Transaction(USART2);   
#else
        USART_ClearITPendingBit(USART2, USART_IT_RXNE);
#endif
    }
}
// -----------------------------------------------------------------------------------
//! \brief  USART3_IRQHandler
//!
//! This function handles USART3_IRQHandler (PM1)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void USART3_IRQHandler(void){
    if(USART_GetITStatus(USART3, USART_IT_RXNE)==SET){
#ifdef STM32_USART3_LBACK    
        SerialPort_Transaction(USART3);
#else
        USART_ClearITPendingBit(USART3, USART_IT_RXNE);
#endif
    }
}
// -----------------------------------------------------------------------------------
//! \brief  UART4_IRQHandler
//!
//! This function handles UART4_IRQHandler (RS422-RTMCB)
//!
//! \param      none
//! 
//! \return     none2
// -----------------------------------------------------------------------------------
void UART4_IRQHandler(void){
    if(USART_GetITStatus(UART4, USART_IT_RXNE)==SET){
#ifdef STM32F_UART4_DMA
      USART_ClearITPendingBit(UART4, USART_IT_RXNE);
#else
#ifdef STM32_UART4_LBACK    
        RTMCB_Transaction (UART4);
#else
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
#endif

#endif    
    }
}
// -----------------------------------------------------------------------------------
//! \brief  UART5_IRQHandler
//!
//! This function handles UART5_IRQHandler (PM2)
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void UART5_IRQHandler(void){
    if(USART_GetITStatus(UART5, USART_IT_RXNE)==SET){
#ifdef STM32_UART5_LBACK    
        //USB_Transaction ();
        SerialPort_Transaction(UART5);
#else
        USART_ClearITPendingBit(UART5, USART_IT_RXNE);
#endif
    }
}

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Handle DMA1_Channel1_IRQHandler interrupt request.
//!
//! This function handles DMA1_Channel1_IRQHandler interrupt request.
//!
//! \param  void
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void DMA1_Channel1_IRQHandler(void)
{
    // End of transmission (ADC EOC request)
    DMA_Cmd(DMA1_Channel1, DISABLE);
    DMA_ClearFlag(DMA_IFCR_CTCIF1);                                // Clear transfer complete flag
    NVIC_ClearIRQPendingIRQ(DMA1_Channel1_IRQn);

    FEEDBACK_ADCDMA_ReadData();

    DMA_ClearFlag(DMA1_FLAG_TC1);
    DMA_Cmd(DMA1_Channel1, ENABLE);

    // Stop ADC1 Software Conversion  
    ADC_SoftwareStartConvCmd(ADC1, DISABLE);

}
// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Handle DMA1_Channel1_IRQHandler interrupt request.
//!
//! This function handles DMA1_Channel1_IRQHandler interrupt request.
//!
//! \param  void
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void DMA2_Channel3_IRQHandler(void)
{
    // Full Buffer Reception
    DMA_Cmd(DMA2_Channel3, DISABLE);
    // CTCIFx: Channel x transfer complete clear (x = 1 ..7)
    DMA_ClearFlag(DMA_IFCR_CTCIF3);                                // Clear transfer complete flag
    NVIC_ClearIRQPendingIRQ(DMA2_Channel3_IRQn);
    
#ifdef STM32F_UART4_DMA
    if (MB_SYSTEM_READY==DEVICE_READY){
        RTMCB_UART4_DMA_ReadData();
        rtmcb_uart4_dma_counter2++;
    }
#endif
    
    DMA_ClearFlag(DMA2_FLAG_TC3);
    DMA_Cmd(DMA2_Channel3, ENABLE);

}

// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
