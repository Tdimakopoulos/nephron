// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   RS422_Protocol.h
//! \brief  Global RS422 Protocol Handling HEADER
//!
//! Nephron+ Global RS422 Protocol Handling for IF-RS422
//! Individual functions in separate files
//!
//! \author  Dudnik G.
//! \date    17.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// -----------------------------------------------------------------------------------
#ifndef RS422_PROTOCOL_H_
#define RS422_PROTOCOL_H_
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
// PTP DEBUG SERIAL PORT (but to keep compatibility)
// -----------------------------------------------------------------------------------
//#define RS422_MININTERVALTYPES          1
//#define RS422_MAXINTERVALTYPES          5 // max types of interval data
//#define RS422_ALLINTERVALTYPES          6 // max types of interval data

// RTMCB --> MB
#define RS422_NOGROUP                   0
#define RS422_FULLSTATUS                1
#define RS422_PS_DATA                   2
#define RS422_ECP_DATA                  3
#define RS422_ACT_DATA                  4
#define RS422_ALARMS_DATA               5
//#define RS422_FULLDATA                6
// MB --> RTMCB
#define RS422_ACK_RTMCB_READY           7
#define RS422_ACK_RECV_DATA             8
#define RS422_ACK_RECV_OPMODES          9
//
//#define RS422_GETDATA                 1
#define RS422_ONOFF                     2
#define RS422_SETSETPOINT               3
#define RS422_SIMULATIONVSRT            4
#define RS422_STSTSTREAMING             5
//#define RS422_ISDEVREADY              6   // RTMCB SENDS
//#define RS422_ASKIFDEVREADY           7   // MB ASKS
#define RS422_MICROFLUIDICS_CONFIG      8
//#define RS422_GETSTATUS               9
//#define RS422_GETPSDATA               10
//#define RS422_GETECPDATA              11
//#define RS422_GETACTDATA              12
//#define RS422_GETALARMS               13
#define RS422_GETGROUPDATA              14
#define RS422_GETCLOCK                  15
#define RS422_SETCLOCK                  16
//#define RS422_GETRTMCBSTATUS          17
//#define RS422_SETONNOFFDEVICES        18

#define RS422_INDEX_A                   1
#define RS422_INDEX_Z                   6
// -----------------------------------------------------------------------------------
// Frame Codes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Frame Flags
// -----------------------------------------------------------------------------------
#define FRAMETYPE_NORMAL                0x00
#define FRAMETYPE_RETRANSMITTED         0x01
#define FRAMETYPE_RETRANSMIT_REQUEST    0x02
#define FRAMETYPE_RETRANSMIT_IMPOSSIBLE 0x03
#define FRAMETYPE_UNSUPPORTED_ERRFLAGS  0x04
#define FRAMETYPE_FRAME_TOOLARGE        0x05
// -----------------------------------------------------------------------------------
// Channel Number
// -----------------------------------------------------------------------------------
#define CHANNEL_CMDS                    0x00
//------------------------------------------
#define CHANNEL_SODIUM_FCI              0x01
#define CHANNEL_POTASSIUM_FCI           0x02
#define CHANNEL_PHOSPHATE_FCI           0x03
#define CHANNEL_PH_FCI                  0x04
#define CHANNEL_UREA_FCI                0x05
#define CHANNEL_ECP_TEMP_FCI            0x06

#define CHANNEL_SODIUM_FCO              0x07
#define CHANNEL_POTASSIUM_FCO           0x08
#define CHANNEL_PHOSPHATE_FCO           0x09
#define CHANNEL_PH_FCO                  0x0A
#define CHANNEL_UREA_FCO                0x0B
#define CHANNEL_ECP_TEMP_FCO            0x0C

#define CHANNEL_PRESSURE_BCI            0x0D
#define CHANNEL_PRESSURE_BCO            0x0E
#define CHANNEL_PRESSURE_FCI            0x0F
#define CHANNEL_PRESSURE_FCO            0x10
#define CHANNEL_TEMPERATURE_BCI         0x11
#define CHANNEL_TEMPERATURE_BCO         0x12
#define CHANNEL_CONDUCTIVITY_FCR        0x13
#define CHANNEL_CONDUCTIVITY_FCQ        0x14
#define CHANNEL_CONDUCTIVITY_T          0x15
#define CHANNEL_LEAKAGE_BC              0x16
#define CHANNEL_LEAKAGE_FC              0x17
#define CHANNEL_FLOW_CALC_BC            0x18
#define CHANNEL_FLOW_CALC_FC            0x19
#define CHANNEL_PUMP_SPEED_BC           0x1A
#define CHANNEL_PUMP_CURRENT_BC         0x1B
#define CHANNEL_PUMP_SPEED_FC           0x1C
#define CHANNEL_PUMP_CURRENT_FC         0x1D
#define CHANNEL_MFSI_POSITION           0x1E
#define CHANNEL_MFSO_POSITION           0x1F
#define CHANNEL_MFSBL_POSITION          0x20
#define CHANNEL_POLAR_VOLTAGE           0x21
#define CHANNEL_POLAR_DIRECTION         0x22
#define CHANNEL_PUMP_SPEEDCODE_BC       0x23
#define CHANNEL_PUMP_SPEEDCODE_FC       0x24

// Vitamin C
// S:14.11.2012 
#define CHANNEL_VITAMINC_VCS            0x25 
#define CHANNEL_DEGAS_VALVESTATUS       0x26 
//E:14.11.2012


#define CHANNEL_ECP1_STATUS             0x80
#define CHANNEL_ECP2_STATUS             0x81
#define CHANNEL_BPSI_STATUS             0x82
#define CHANNEL_BPSO_STATUS             0x83
#define CHANNEL_FPSI_STATUS             0x84
#define CHANNEL_FPSO_STATUS             0x85
#define CHANNEL_BTS_STATUS              0x86
#define CHANNEL_BLD_STATUS              0x87
#define CHANNEL_FLD_STATUS              0x88
#define CHANNEL_BLPUMP_STATUS           0x89
#define CHANNEL_FLPUMP_STATUS           0x8A
#define CHANNEL_MFSI_STATUS             0x8B
#define CHANNEL_MFSO_STATUS             0x8C
#define CHANNEL_MFSBL_STATUS            0x8D
#define CHANNEL_DCS_STATUS              0x8E
#define CHANNEL_POLAR_STATUS            0x8F
#define CHANNEL_ALARMS_STATUS           0x90
#define CHANNEL_RTMCB_STATUS            0x91
// S:14.11.2012 
#define CHANNEL_VCS_STATUS              0x92
#define CHANNEL_VCI_STATUS              0x93
#define CHANNEL_DEGAS_STATUS            0x94
#define CHANNEL_VCI_SPCURRENT           0x95
#define CHANNEL_VCI_SPSPEED             0x96
#define CHANNEL_VCI_AVGCURRENT          0x97
#define CHANNEL_VCI_AVGSPEED            0x98
// E:14.11.2012

// -----------------------------------------------------------------------------------
// Channel Value Size
// -----------------------------------------------------------------------------------
#define SIZE_VALUE_SODIUM               0x02
#define SIZE_VALUE_POTASSIUM            0x02
#define SIZE_VALUE_PHOSPHATE            0x02
#define SIZE_VALUE_PH                   0x02
#define SIZE_VALUE_UREA                 0x02
#define SIZE_VALUE_ECPTEMP              0x02

#define SIZE_VALUE_STATUS               0x04
// -----------------------------------------------------------------------------------
// New Received Value (Written)
// -----------------------------------------------------------------------------------
// RTMCB_channelsFlag_01
// -----------------------------------------------
#define WRITTEN_SODIUM_FCI              0x00000001
#define WRITTEN_POTASSIUM_FCI           0x00000002 
#define WRITTEN_PHOSPHATE_FCI           0x00000004
#define WRITTEN_PH_FCI                  0x00000008
#define WRITTEN_UREA_FCI                0x00000010
#define WRITTEN_ECP_TEMP_FCI            0x00000020

#define WRITTEN_SODIUM_FCO              0x00000040 
#define WRITTEN_POTASSIUM_FCO           0x00000080
#define WRITTEN_PHOSPHATE_FCO           0x00000100
#define WRITTEN_PH_FCO                  0x00000200
#define WRITTEN_UREA_FCO                0x00000400
#define WRITTEN_ECP_TEMP_FCO            0x00000800 

#define WRITTEN_PRESSURE_BCI            0x00001000
#define WRITTEN_PRESSURE_BCO            0x00002000
#define WRITTEN_PRESSURE_FCI            0x00004000
#define WRITTEN_PRESSURE_FCO            0x00008000
#define WRITTEN_TEMPERATURE_BCI         0x00010000
#define WRITTEN_TEMPERATURE_BCO         0x00020000
#define WRITTEN_CONDUCTIVITY_FCR        0x00040000
#define WRITTEN_CONDUCTIVITY_FCQ        0x00080000
#define WRITTEN_CONDUCTIVITY_T          0x00100000
#define WRITTEN_LEAKAGE_BC              0x00200000
#define WRITTEN_LEAKAGE_FC              0x00400000
#define WRITTEN_FLOW_CALC_BC            0x00800000
#define WRITTEN_FLOW_CALC_FC            0x01000000
#define WRITTEN_PUMP_SPEED_BC           0x02000000
#define WRITTEN_PUMP_CURRENT_BC         0x04000000
#define WRITTEN_PUMP_SPEED_FC           0x08000000
#define WRITTEN_PUMP_CURRENT_FC         0x10000000
#define WRITTEN_MFSI_POSITION           0x20000000
#define WRITTEN_MFSO_POSITION           0x40000000
#define WRITTEN_MFSBL_POSITION          0x80000000 // <--
// -----------------------------------------------
// RTMCB_channelsFlag_02
// -----------------------------------------------
#define WRITTEN_POLAR_VOLTAGE           0x00000001
#define WRITTEN_POLAR_DIRECTION         0x00000002

#define WRITTEN_ECP1_STATUS             0x00000004 //
#define WRITTEN_ECP2_STATUS             0x00000008 //
#define WRITTEN_BPSI_STATUS             0x00000010
#define WRITTEN_BPSO_STATUS             0x00000020
#define WRITTEN_FPSI_STATUS             0x00000040
#define WRITTEN_FPSO_STATUS             0x00000080
#define WRITTEN_BTS_STATUS              0x00000100
#define WRITTEN_BLD_STATUS              0x00000200
#define WRITTEN_FLD_STATUS              0x00000400
#define WRITTEN_BLPUMP_STATUS           0x00000800
#define WRITTEN_FLPUMP_STATUS           0x00001000
#define WRITTEN_MFSI_STATUS             0x00002000
#define WRITTEN_MFSO_STATUS             0x00004000
#define WRITTEN_MFSBL_STATUS            0x00008000
#define WRITTEN_DCS_STATUS              0x00010000
#define WRITTEN_POLAR_STATUS            0x00020000
#define WRITTEN_ALARMS_STATUS           0x00040000
#define WRITTEN_RTMCB_STATUS            0x00080000
#define WRITTEN_PUMP_SPEEDCODE_BC       0x00100000
#define WRITTEN_PUMP_SPEEDCODE_FC       0x00200000

// -----------------------------------------------------------------------------------
// Transferred Value to buffer (Read)
// -----------------------------------------------------------------------------------
// RTMCB_channelsFlag_01
// -----------------------------------------------
#define READOK_SODIUM_FCI               0xFFFFFFFE
#define READOK_POTASSIUM_FCI            0xFFFFFFFD
#define READOK_PHOSPHATE_FCI            0xFFFFFFFB
#define READOK_PH_FCI                   0xFFFFFFF7
#define READOK_UREA_FCI                 0xFFFFFFEF
#define READOK_ECP_TEMP_FCI             0xFFFFFFDF

#define READOK_SODIUM_FCO               0xFFFFFFBF
#define READOK_POTASSIUM_FCO            0xFFFFFF7F
#define READOK_PHOSPHATE_FCO            0xFFFFFEFF
#define READOK_PH_FCO                   0xFFFFFDFF
#define READOK_UREA_FCO                 0xFFFFFBFF
#define READOK_ECP_TEMP_FCO             0xFFFFF7FF

#define READOK_PRESSURE_BCI             0xFFFFEFFF
#define READOK_PRESSURE_BCO             0xFFFFDFFF
#define READOK_PRESSURE_FCI             0xFFFFBFFF
#define READOK_PRESSURE_FCO             0xFFFF7FFF
#define READOK_TEMPERATURE_BCI          0xFFFEFFFF
#define READOK_TEMPERATURE_BCO          0xFFFDFFFF
#define READOK_CONDUCTIVITY_FCR         0xFFFBFFFF
#define READOK_CONDUCTIVITY_FCQ         0xFFF7FFFF
#define READOK_CONDUCTIVITY_T           0xFFEFFFFF
#define READOK_LEAKAGE_BC               0xFFDFFFFF
#define READOK_LEAKAGE_FC               0xFFBFFFFF
#define READOK_FLOW_CALC_BC             0xFF8FFFFF
#define READOK_FLOW_CALC_FC             0xFEFFFFFF
#define READOK_PUMP_SPEED_BC            0xFDFFFFFF
#define READOK_PUMP_CURRENT_BC          0xFBFFFFFF
#define READOK_PUMP_SPEED_FC            0xF7FFFFFF
#define READOK_PUMP_CURRENT_FC          0xEFFFFFFF
#define READOK_MFSI_POSITION            0xDFFFFFFF
#define READOK_MFSO_POSITION            0xBFFFFFFF
#define READOK_MFSBL_POSITION           0x7FFFFFFF

// -----------------------------------------------
// RTMCB_channelsFlag_02
// -----------------------------------------------
#define READOK_POLAR_VOLTAGE            0xFFFFFFFE
#define READOK_POLAR_DIRECTION          0xFFFFFFFD

#define READOK_ECP1_STATUS              0xFFFFFFFB
#define READOK_ECP2_STATUS              0xFFFFFFF7
#define READOK_BPSI_STATUS              0xFFFFFFEF
#define READOK_BPSO_STATUS              0xFFFFFFDF
#define READOK_FPSI_STATUS              0xFFFFFFBF
#define READOK_FPSO_STATUS              0xFFFFFF7F
#define READOK_BTS_STATUS               0xFFFFFEFF
#define READOK_BLD_STATUS               0xFFFFFDFF
#define READOK_FLD_STATUS               0xFFFFFBFF
#define READOK_BLPUMP_STATUS            0xFFFFF7FF
#define READOK_FLPUMP_STATUS            0xFFFFEFFF
#define READOK_MFSI_STATUS              0xFFFFDFFF
#define READOK_MFSO_STATUS              0xFFFFBFFF
#define READOK_MFSBL_STATUS             0xFFFF7FFF
#define READOK_DCS_STATUS               0xFFFEFFFF
#define READOK_POLAR_STATUS             0xFFFDFFFF
#define READOK_ALARMS_STATUS            0xFFFBFFFF
#define READOK_RTMCB_STATUS             0xFFF7FFFF
#define READOK_PUMP_SPEEDCODE_BC        0xFFEFFFFF
#define READOK_PUMP_SPEEDCODE_FC        0xFFDFFFFF

// -----------------------------------------------------------------------------------
// EXPECTED BYTES OF PARAMETERS
// -----------------------------------------------------------------------------------
#define INTERVAL_REQ_PARAM_SIZE         0x04
#define SETSETPOINTS_PARAM_SIZE         0x0A
#define SWITCH_ONOFF_PARAM_SIZE         0x06
#define SET_SIMUL_RT_PARAM_SIZE         0x03
#define SET_STREAMST_PARAM_SIZE         0x03
#define SET_WORKMODE_PARAM_SIZE         0x03
#define READGROUPSGN_PARAM_SIZE         0x03
#define SYS_SETCLOCK_PARAM_SIZE         0x13
#define SWITCH_ALLOO_PARAM_SIZE         0x30 
#define NPSYS_GCLOCK_PARAM_SIZE         0x13
// -----------------------------------------------------------------------------------
// TO SWITCH ON/OFF THE DEVICES
// -----------------------------------------------------------------------------------
#define DEVICES_TOTAL                   0x10

#define SWITCH_ECP1_ON                  0x00000001
#define SWITCH_ECP1_OFF                 0xFFFFFFFE

#define SWITCH_ECP2_ON                  0x00000002
#define SWITCH_ECP2_OFF                 0xFFFFFFFD

#define SWITCH_FPSI_ON                  0x00000010
#define SWITCH_FPSI_OFF                 0xFFFFFFEF

#define SWITCH_FPSO_ON                  0x00000020
#define SWITCH_FPSO_OFF                 0xFFFFFFDF

#define SWITCH_BPSI_ON                  0x00000040
#define SWITCH_BPSI_OFF                 0xFFFFFFBF

#define SWITCH_BPSO_ON                  0x00000080
#define SWITCH_BPSO_OFF                 0xFFFFFF7F

#define SWITCH_BTS_ON                   0x00000100
#define SWITCH_BTS_OFF                  0xFFFFFEFF

#define SWITCH_DCS_ON                   0x00000200
#define SWITCH_DCS_OFF                  0xFFFFFDFF

#define SWITCH_BLD_ON                   0x00000400
#define SWITCH_BLD_OFF                  0xFFFFFBFF

#define SWITCH_FLD_ON                   0x00000800
#define SWITCH_FLD_OFF                  0xFFFFF7FF

#define SWITCH_MFSI_ON                  0x00001000
#define SWITCH_MFSI_OFF                 0xFFFFEFFF

#define SWITCH_MFSO_ON                  0x00002000
#define SWITCH_MFSO_OFF                 0xFFFFDFFF

#define SWITCH_MFSU_ON                  0x00004000
#define SWITCH_MFSU_OFF                 0xFFFFBFFF

#define SWITCH_BLPUMP_ON                0x00010000
#define SWITCH_BLPUMP_OFF               0xFFFEFFFF

#define SWITCH_FLPUMP_ON                0x00020000
#define SWITCH_FLPUMP_OFF               0xFFFDFFFF

#define SWITCH_POLAR_ON                 0x00040000
#define SWITCH_POLAR_OFF                0xFFFBFFFF



// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
// GLOBAL
// -----------------------------------------------------------------------------------
extern void RS422_FILL_PACKET_VALUE08(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t r_packet_flags, uint16_t r_packet_size, uint8_t r_channel_no, uint32_t timeStamp, uint16_t interval, uint8_t value8);
extern void RS422_FILL_PACKET_VALUE16(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t r_packet_flags, uint16_t r_packet_size, uint8_t r_channel_no, uint32_t timeStamp, uint16_t interval, uint16_t value16);
extern void RS422_FILL_PACKET_VALUE32(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t r_packet_flags, uint16_t r_packet_size, uint8_t r_channel_no, uint32_t timeStamp, uint16_t interval, uint32_t value32);

extern uint16_t AnalyzeCommandNephronPlusSys(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn);
extern uint16_t AnalyzeResponseNephronPlusCtrl(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn);
extern uint16_t AnalyzeResponseNephronPlusSys(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn);

extern uint16_t AnalyzeCommand(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn, uint8_t class_rs422);
extern uint16_t AnalyzeResponse(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn, uint8_t class_rs422);

extern uint16_t AnalyzeStructuredPacket(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn);
extern uint16_t AnalyzeIntervalPacket(uint8_t* RX, uint16_t RXI, uint16_t RXlen, uint16_t flags, uint16_t size, uint8_t CHANNEL, boolean_t* okyn);
extern uint16_t RS422_ANALYZE_RESPONSE(uint16_t ENC_PACKETS_LEN, uint8_t* BufferXYZ, uint8_t* RxBuffer, boolean_t* oks);

// COMMANDS
extern uint16_t RS422_RTMCB_SEND_COMMAND (uint8_t index_to_scheduler);
extern uint16_t RS422_MAKE_NP_TX_PACKET_SetSimulationRTMode (uint8_t* BufferXYZ, uint8_t comm_seqno);
extern uint16_t RS422_MAKE_NP_TX_PACKET_StartStopStreaming (uint8_t* BufferXYZ, uint8_t comm_seqno);
extern uint16_t RS422_MAKE_NP_TX_PACKET_GET_GROUP_DATA (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHGROUP);
extern uint16_t RS422_MAKE_NP_TX_PACKET_SetSetPoints (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHDEVICE, uint8_t WHICHDIRECTION, uint16_t ACT_REFERENCE);
extern uint16_t RS422_MAKE_NP_TX_PACKET_Switch_ONnOFF(uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHDEVICE, uint8_t ONNOFF);
extern uint16_t RS422_MAKE_NP_TX_PACKET_SetMicrofluidicCircuit(uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t OPERATING_MODE);
extern uint16_t RS422_MAKE_NP_TX_PACKET_GetClock(uint8_t* BufferXYZ, uint8_t comm_seqno);
extern uint16_t RS422_MAKE_NP_TX_PACKET_SetClock (uint8_t* BufferXYZ, uint8_t comm_seqno, uint16_t YEAR, uint8_t MONTH, uint8_t DAY, uint8_t HOURS, uint8_t MINUTES, uint8_t SECONDS);

// ANSWERS
extern uint16_t RS422_MAKE_ANSWER_NP_SYS_ACKRTMCBREADY (uint8_t* BufferXYZ, uint8_t comm_seqno);
extern uint16_t RS422_MAKE_ANSWER_NP_DATA_ACKINTERVAL (uint8_t* BufferXYZ, uint8_t comm_seqno);
// -----------------------------------------------------------------------------------
#endif /*RS422_PROTOCOL_H_*/
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------