// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   sdcard_spi1.c
//! \brief  spi1 for sdcard 
//!
//! spi1 initialization, send & receive data routines
//!
//! \author  Dudnik G.S.
//! \date    10/07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

// -----------------------------------------------------------------------------------
// Global Variables
// -----------------------------------------------------------------------------------
extern uint8_t LOCK_CRITICAL_INTS;
// -----------------------------------------------------------------------------------
//! \brief  SD_SPIConfig
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void SD_SPIConfig(void)
{

  SPI_InitTypeDef    SPI_InitStructure;


  SPI_I2S_DeInit(SD_SPI);
 // GPIO CONFIGURED IN RTMCB_GPIO_Config
 
  // Enable SPI clock 
  RCC_APB2PeriphClockCmd(SD_SPI_CLK, ENABLE); 
 
  
  // SPI Config 
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
#ifdef SDCARD_CTRL_HW
// SPI_NSS_Hard --> SD_NCS_PIN configure as AF_PP  
  SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
#else
// SPI_NSS_Soft --> SD_NCS_PIN configure as OUT_PP 
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
#endif
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2; // 2,4(ok, hw fault?) 32 (OK) (2) TBD
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;

  SPI_Init(SD_SPI, &SPI_InitStructure);

  // SPI enable
  SPI_Cmd(SD_SPI, ENABLE);
}

// -----------------------------------------------------------------------------------
//! \brief  SD_SendData
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _data
//!
//! \return       none
// -----------------------------------------------------------------------------------
void SD_SendData(uint8_t mode, uint8_t _data)
{
  if(mode) STM32F_GPIOOff(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);
  SPI_I2S_SendData(SD_SPI, _data);
  while(SPI_I2S_GetFlagStatus(SD_SPI, SPI_I2S_FLAG_BSY) != RESET);
  if(mode) STM32F_GPIOOn(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);
}
// -----------------------------------------------------------------------------------
//! \brief  SD_ReceiveData
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \return       _data
// -----------------------------------------------------------------------------------
uint8_t SD_ReceiveData(uint8_t mode)
{
  uint8_t _data = 0;
  if(mode) STM32F_GPIOOff(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);
  while(SPI_I2S_GetFlagStatus(SD_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _data = SPI_I2S_ReceiveData(SD_SPI); 
  if(mode) STM32F_GPIOOn(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);
  return _data;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_SendReceive
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
uint8_t SD_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
  
  if(mode) STM32F_GPIOOff(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);
  SPI_I2S_SendData(SD_SPI, _dataIN);
  while(SPI_I2S_GetFlagStatus(SD_SPI, SPI_I2S_FLAG_BSY) != RESET);
  while(SPI_I2S_GetFlagStatus(SD_SPI, SPI_I2S_FLAG_RXNE) == RESET);
  _dataOUT = SPI_I2S_ReceiveData(SD_SPI); 
  if(mode) STM32F_GPIOOn(SD_SPI_nCS_GPIO_PORT, SD_SPI_nCS_GPIO_PIN);

  return _dataOUT;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_SendReceive
//!
//! Configures the SPI for the SDCARD Interface
//!
//! \param[IN]    mode
//!               1 software nCS
//!               0 hardware NSS
//!
//! \param[IN]    _datain
//!
//! \return       _dataout
// -----------------------------------------------------------------------------------
uint8_t SD_SendReceive(uint8_t _dataIN)
{
  uint8_t _dataOUT = 0;
#ifdef SPI_MODE_HW  
  _dataOUT = SD_SendReceiveHWSW(0, _dataIN);
#else 
  _dataOUT = SD_SendReceiveHWSW(1, _dataIN);
#endif
  return _dataOUT;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
