package nephron.mobile.datafunctions;

public class MsgSimple extends OutgoingWakdMsg {

	private static final long serialVersionUID = 8598583075931094670L;

	public MsgSimple(int id, WakdCommandEnum command) {
		this.header.setId(id);
		this.header.setCommand(command);
		this.header.setSize(6);
	}

	@Override
	public byte[] encode() {
		return header.encode();
	}

}