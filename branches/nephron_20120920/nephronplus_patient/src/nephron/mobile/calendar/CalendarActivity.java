/*
 * Copyright (C) 2011 Chris Gao <chris@exina.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nephron.mobile.calendar;

import nephron.mobile.application.R;
import nephron.mobile.application.ScheduleDetails;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CalendarActivity extends Activity  implements CalendarView.OnCellTouchListener{
	public static final String MIME_TYPE = "vnd.android.cursor.dir/vnd.mobile.calendar.date";
	public static String date = "",dateForDB="";
	View _view = null;
	CalendarView mView = null;
	TextView mHit;
	Handler mHandler = new Handler();
	Button backButton;
		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedulelayout);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		        
        mView = (CalendarView)findViewById(R.id.calendar);
        mView.setOnCellTouchListener(this);
        
        backButton = (Button) findViewById(R.id.BackButtonSchedule);
        
        TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
        monthText.setText(DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
                
        if(getIntent().getAction().equals(Intent.ACTION_PICK))
        	findViewById(R.id.hint).setVisibility(View.INVISIBLE);
        
        backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
    }
    
	public void onTouch(Cell cell) 
	{
		Intent intent = getIntent();
		String action = intent.getAction();
		
		//int day = cell.getDayOfMonth();
		date = cell.getDayOfMonth()+"-"+ DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_SHORT) + "-"+mView.getYear();
		dateForDB = mView.getYear()+"-"+(mView.getMonth() < 10 ? "0" + mView.getMonth() : mView.getMonth()) +"-"+(cell.getDayOfMonth() < 10 ? "0" + cell.getDayOfMonth() : cell.getDayOfMonth());
		 
		if(action.equals(Intent.ACTION_PICK) || action.equals(Intent.ACTION_GET_CONTENT)) {
			Intent ret = new Intent();
			ret.putExtra("year", mView.getYear());
			ret.putExtra("month", mView.getMonth());
			ret.putExtra("day", cell.getDayOfMonth());
			this.setResult(RESULT_OK, ret);
			finish();
			return;
		}	
		
		if (cell.mPaint.getColor()!= Color.TRANSPARENT)
		{
			Intent openScheduleDetails = new Intent(CalendarActivity.this, ScheduleDetails.class);
			startActivity(openScheduleDetails);		
		}
		else
			return;
		
	}   
	
	public void onDateButtonTouch (View view)
	{   
		try
		{
			if(view.getId() == R.id.MonthForwardImageButton)
			{
				mView.nextMonth();
				TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
				monthText.setText (DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
				
			}
			else if(view.getId() == R.id.MonthBackImageButton)
			{
				mView.previousMonth();
				TextView monthText = (TextView)findViewById(R.id.MonthTitleTextBox);
				monthText.setText (DateUtils.getMonthString(mView.getMonth(), DateUtils.LENGTH_LONG) + " "+mView.getYear());
			}
			else
				return;
		}
		catch (Exception ex)
		{
    		
		}	
	}
}