package nephron.mobile.statistics;

import nephron.mobile.application.R;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class CalciumStatistics extends Activity
{	
	WebView dailyCalciumWebView;
	WebView weeklyCalciumWebView;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{		
		// Show layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calciumlayout);
				
		// Load daily statistics
		dailyCalciumWebView = (WebView)findViewById(R.id.DailyCalciumWebView);
		dailyCalciumWebView.getSettings().setJavaScriptEnabled(true);
		dailyCalciumWebView.loadUrl("file:///android_asset/dailycalciumgraph.html");
	
		// Load weekly statistics
		weeklyCalciumWebView = (WebView)findViewById(R.id.WeeklyCalciumWebView);
		weeklyCalciumWebView.getSettings().setJavaScriptEnabled(true);
		weeklyCalciumWebView.loadUrl("file:///android_asset/weeklycalciumgraph.html");
	}
}