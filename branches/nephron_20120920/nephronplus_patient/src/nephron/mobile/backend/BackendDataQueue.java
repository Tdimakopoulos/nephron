package nephron.mobile.backend;



public class BackendDataQueue {
	
	private byte[] command;
	
	private int commandId;
	
	private BackendDataQueueType type;
	
	public byte[] getCommand() {
		return command;
	}
	public void setCommand(byte[] command) {
		this.command = command;
	}
	public int getCommandId() {
		return commandId;
	}
	public void setCommandId(int commandId) {
		this.commandId = commandId;
	}
	public BackendDataQueueType getType() {
		return type;
	}
	public void setType(BackendDataQueueType type) {
		this.type = type;
	}

}
