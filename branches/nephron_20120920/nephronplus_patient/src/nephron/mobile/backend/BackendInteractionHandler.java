package nephron.mobile.backend;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class BackendInteractionHandler extends Handler {

	final int SHUTDOWN_OK = 0;
	final int BACKEND_SHUTDOWN = 1;

	private AlertDialog alertDialog;
	private AlertDialog.Builder dialogBuilder;
	Context context;

	public BackendInteractionHandler(Context context) {
		super();
		this.context = context;
		this.dialogBuilder = new AlertDialog.Builder(this.context);
	}

	public void handleMessage(android.os.Message msg) {

		if(alertDialog != null) {
			if(alertDialog.isShowing()){
				alertDialog.dismiss();
			}
		}

		switch (msg.what) {

		case BACKEND_SHUTDOWN:
			Bundle bundle = msg.getData();
			if(bundle.containsKey("commandID") && bundle.containsKey("command")) {

				Intent shutdownIntent = new Intent();
				shutdownIntent.setClass(context, BackendShutdownWAKD.class);
				shutdownIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				shutdownIntent.setAction("BACKEND_SHUTDOWN");
				shutdownIntent.putExtras(bundle);
				context.startActivity(shutdownIntent);
			}
			break;
			
			
//TEMPORARILY UNUSED
//		case SHUTDOWN_OK:
//			dialogBuilder.setTitle("Incoming Command from Backend");
//			dialogBuilder.setMessage("Shutdown command received from backend.");
//			dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					dialog.dismiss();
//					NephronRestClient pClient = new NephronRestClient("http://178.63.69.149:8787/nephronrest/webresources/smartphone/Updateshutdownack");
//					String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
//					pClient.AddParam("imei", "800");
//					try {
//						pClient.Execute(RequestMethod.GET);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//
//			});
//
//			break;

		default:
			dialogBuilder.setTitle("General Backend Info");
			dialogBuilder.setMessage("Unknown Event occured");
			dialogBuilder.setPositiveButton("OK", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			break;
		}

		alertDialog = dialogBuilder.create();
		alertDialog.show();
	};

}
