package nephron.mobile.backend;


public enum BackendDataQueueType {

	UNDEFINED,
	MEASUREMENT,		//	--> consists of the hex command, date, 
	ACK,
	ALARM;

	public static BackendDataQueueType getBackendDataTypeEnum(int value) {
		switch (value) {
		case (int)1:
			return MEASUREMENT;
		case (int)2:
			return ALARM;
		case (int)3:
			return ACK;
		default:
			return UNDEFINED;
		}
	}

	public static int MeasurementEnum(BackendDataQueueType value)
	{
		switch (value) {
		case MEASUREMENT:
			return 1;
		case ALARM:
			return 2;
		case ACK:
			return 3;
		}
		return 0;
	}
}