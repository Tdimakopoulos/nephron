package nephron.mobile.application;

import java.util.ArrayList;
import java.util.List;

import nephron.mobile.application.alarms.AlarmsMsg;
import nephron.mobile.application.alarms.QueueHandler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

class MyAdapter extends BaseAdapter {

	private List<AlarmsMsg> items = new ArrayList<AlarmsMsg>();
	public final String tag = "RSSReader";
	private LayoutInflater inflater;
	private Context context;
	int id;
	int len = 0;
	byte[] data1 = new byte[1024];
	AlertDialog alert;
	AlertDialog.Builder alertd;
	TextView alarms;

	public MyAdapter(Context context1) {
		context = context1;
		inflater = LayoutInflater.from(context1);
	}

	@SuppressLint("NewApi")
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		v = inflater.inflate(R.layout.listview, null);

		final AlarmsMsg o = items.get(position);
		if (o != null) {

			alarms = (TextView) v.findViewById(R.id.backtextview);
			alarms.setText(o.getmsgPatient() + "\n" + o.getDate());
			if (o.getStatus() == 1) {
				alarms.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						R.drawable.delete, 0);
				alarms.setTextColor(Color.RED);
				alarms.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						final View v=view;
						alertd = new AlertDialog.Builder(context);
						alertd.setTitle("Acknowledge Alarm");
						alertd.setMessage("Acknowledge Alarm :"+ o.getmsgPatient()+" sent on "+o.getDate());
						alertd.setPositiveButton("Acknowledge",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								ConnectionService.db.updateAlertStatus(o.getIdDatabase(), 2);

								if(!QueueHandler.removeAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getNewAlarmsList(), o.getIdDatabase())){
									if(!QueueHandler.removeAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList(), o.getIdDatabase())){
										Toast.makeText(context, "Something has gone wrong, alarm not found inside the queues", Toast.LENGTH_LONG).show();
									}
								}
								
								Intent itemintent = new Intent(v.getContext(),Alarms.class);
								((Activity) context).startActivityForResult(itemintent, 0);
								((Activity) context).overridePendingTransition(0, 0);
								((Activity) context).finish();
							}
						});
						alertd.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});

						alert = alertd.create();
						alert.show();
					}

				});


			}
		}
		return v;
	}

	public void onClick(View arg0) {
		alarms.setText("My text on click");  
	}

	public int getCount() {
		return items.size();
	}

	public AlarmsMsg getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public void addOrder(List<AlarmsMsg> order) {
		this.items = order;
	}

	public void clear() {
		this.items.clear();
	}

}
