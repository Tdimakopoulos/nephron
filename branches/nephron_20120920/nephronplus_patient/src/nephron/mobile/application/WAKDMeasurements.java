package nephron.mobile.application;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class WAKDMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	//	private static final String _measurementsElementArray[] = { "Physical",
	//			"Electro Chemical Measurements" };
	private static final String _measurementsElementArray[] = { "Physical Measurements",
	"Physiological Measurements" };

	Bundle b;
	Button backButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackButtonMeasurements);
		_measurementsListView = (ListView) findViewById(R.id.MeasurementsListView);

		_measurementsListView.setAdapter(
				new ArrayAdapter<String>(this,R.layout.backgroundlayout, R.id.backtextview,
						_measurementsElementArray));

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// handle the various choices
				try {
					switch (position) {
					case 0:   // display Physical Measurements

						Intent physicalMeasurements = new Intent(WAKDMeasurements.this, WAKDPhysicalMeasurements.class);
						startActivity(physicalMeasurements);
						break;

					case 1: //  display Measurements Menu
						Intent electroMeasurements = new Intent(WAKDMeasurements.this, WAKDPhysiologicalMeasurements.class);
						startActivity(electroMeasurements);
						break;
					default:
						break;
					}
				} catch (Exception ex) {
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage(ex.getMessage());
					_dialog.setButton("OK",	new DialogInterface.OnClickListener() {
						// Event of the OK button of the message box
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss(); // close dialog
						}
					});
					_dialog.show(); // show message box
				}
			}
		});

		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

	}
}
