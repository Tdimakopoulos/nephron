package nephron.mobile.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class infoDialog extends Activity {
	AlertDialog arrivedDataDialog;
	byte[] arrivedData;
	MessageConverter converter;
	Intent intent;
	Bundle extras;
	AlertDialog.Builder alertd;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(0, 0);
		converter = new  MessageConverter();
		intent =  getIntent();
	    extras = intent.getExtras();
    	if (extras != null) {
			
			if(extras.containsKey("dataArrived")){
				arrivedData = extras.getByteArray("dataArrived");
			    alertd = new AlertDialog.Builder(this);
				alertd.setTitle("Test");
				alertd.setMessage(converter.BytesToString(arrivedData));
				alertd.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
						 finish();
							}
						});
				 
			}
			else if(extras.containsKey("message")){ 
			    alertd = new AlertDialog.Builder(this);
				alertd.setTitle("Connection info");
				alertd.setMessage(extras.getString("message"));
				alertd.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
						 finish();
							}
						});
			}
			else if(extras.containsKey("BluetoothMessage")){ 
				  alertd = new AlertDialog.Builder(this);
					alertd.setTitle("Bluetooth Alert");
					alertd.setMessage(extras.getString("BluetoothMessage"));
					alertd.setPositiveButton("Activate",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int whichButton) {
									Intent enableBtIntent = new Intent(
											BluetoothAdapter.ACTION_REQUEST_ENABLE);
								 	 startActivityForResult(enableBtIntent, 1);
							 finish();
								}
							});
					alertd.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									Log.d("!noooooooooooooo!!!", "!!noooooooooooooooo!!");
									finish();
								}
							});
			}
			
			arrivedDataDialog = alertd.create();
			arrivedDataDialog.show();
			 
		} 

	 
 
	}

}
