package nephron.mobile.application.alarms;

import java.io.Serializable;
import java.util.List;

import nephron.mobile.application.ConnectionService;
import nephron.mobile.application.GlobalVar;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.widget.Toast;

public class AlertHandler extends Thread implements Serializable {

	private static final long serialVersionUID = -3878158368732984364L;

	Context context;

	public static List<AlarmsMsg> alerts;

	public AlertHandler(Context y) {
		this.context = y;
	}

	public void run() {

//		int period;
//		SharedPreferences userSettings = context.getSharedPreferences("userSettings", context.MODE_WORLD_READABLE);
//
//		period = Integer.parseInt(userSettings.getString("AlarmsFrequency", "1"));

		while (ConnectionService.alive) {
//			Looper.prepare();
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				new Thread(new AlertHandler(context)).start();
				e.printStackTrace();
			}
			
//			Toast.makeText(context, "Alert Handler running", Toast.LENGTH_LONG).show();
			
//			Log.e("INSIDE ALERT HANDLER", "WOKEN FROM SLEEP");
//			Log.e("INSIDE ALERT HANDLER", "NEW LIST SIZE : "+((GlobalVar)context.getApplicationContext()).getNewAlarmsList().size());
//			Log.e("INSIDE ALERT HANDLER", "ARCHIVED LIST SIZE : "+((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList().size());

			//Handler will trigger only when the dialog window is not open...
			if( !((GlobalVar)context.getApplicationContext()).isAlarmWindowOpen() ) {

				//				Boolean emptyNewList = ((GlobalVar)context.getApplicationContext()).getNewAlarmsList().isEmpty();

				if (!((GlobalVar)context.getApplicationContext()).getNewAlarmsList().isEmpty() ) {
					((GlobalVar)context.getApplicationContext()).setAlarmWindowOpen(true);
//					Log.e("NEXT ALARM MESSAGE IS FOR"," NEW ALARMS");
					AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getNewAlarmsList());
					Intent alarmIntent = new Intent("android.intent.action.MAIN");
					alarmIntent.setClass(context, MyAlertDialog.class);
					alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					alarmIntent.setAction("NEW_ALARM");
					alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
					alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
					context.startActivity(alarmIntent);
				}
				else
					if(!((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList().isEmpty()) {
						((GlobalVar)context.getApplicationContext()).setAlarmWindowOpen(true);
//						Log.e("NEXT ALARM MESSAGE IS FOR"," ARCHIVED ALARMS");
						AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList());

//						Log.e("SEE NULL POINTER", "ID = " + String.valueOf(alarm_msg.getIdDatabase()));
						Intent alarmIntent = new Intent("android.intent.action.MAIN");
						alarmIntent.setClass(context, MyAlertDialog.class);
						alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						alarmIntent.setAction("ARCHIVED_ALARM");
						alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
						alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
						context.startActivity(alarmIntent);
					}

				//				if(context.getSharedPreferences("userSettings", Context.MODE_WORLD_READABLE).getString("Vibration", "Off").equalsIgnoreCase("On")) {
				//					Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				//					mVibrator.vibrate(500);
				//				}
			}
//			Looper.loop();
		}
	}

}
