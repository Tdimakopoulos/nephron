package nephron.mobile.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import nephron.mobile.application.alarms.AlertHandler;
import nephron.mobile.backend.SPLogin;
import nephron.mobile.calendar.CalendarActivity;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.MyDataListener;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;

public class Nephron extends Activity {

	MyDataListener mylistener; 
	AlertDialog alert;
	AlertDialog.Builder alertd;
	ConnectionHandler connectionhandler;
	WakdCommandEnum command;
	MsgSimple statusRequest;
	final Context context = this;
	AlertDialog shutdownInfoDialog;
	SPLogin login;
	AlertHandler alertHandler;
	
	AlertDialog dialog;
	Button upfile;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
//		upfile = (Button) findViewById(R.id.uploadfile);
//		
//		upfile.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				
//				NephronSOAPClient client = new NephronSOAPClient(context/*, nameSpace, URL, soapAction, methodName*/);
//				String deviceID = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
//				String response = client.executeUploadFileRequest(deviceID, "String Test File for ECG FILE UPLOAD ");
//				
//				AlertDialog.Builder builder = new AlertDialog.Builder(context);
//				builder.setTitle("Upload File request");
//				builder.setMessage(response);
//				builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface arg0, int arg1) {
//						dialog.dismiss();
//					}
//				});
//				dialog = builder.create();
//				dialog.show();
//				
//				
//				
//			}
//		});


		//deleteDatabase("nephron");
		
		// Start Connection Service with WAKD
//		if(!((GlobalVar)getApplicationContext()).isConnectionServiceStarted()) {
//			Intent start = new Intent("nephron.mobile.application.ConnectionService");
//			this.startService(start);
//		}
		
	}

	protected void onDestroy() {
		try {
			backupDatabase();
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.onDestroy();
		System.exit(1);
	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.ButtonMeasurements: // Show list of measurements
			Intent openListOfMeasurements = new Intent(Nephron.this, MeasurementOption.class);
			startActivity(openListOfMeasurements);
			break;
		case R.id.FunctionsButton:	// Show Screen of Functions (Weight etc)
			Intent openFunctions = new Intent(Nephron.this, Functions.class);
			startActivity(openFunctions);
			break;
		case R.id.WAKStatusButton: // Show WAKD status
			Intent openWakStatus = new Intent(Nephron.this, WakStatus.class);
			startActivity(openWakStatus);
			break;
		case R.id.WAKControlButton: // Show WAKD settings
			Intent openWakControl = new Intent(Nephron.this, WakControl.class);
			startActivity(openWakControl);
			break;
		case R.id.ScheduleButton: // Show patient schedules
			startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, CalendarActivity.MIME_TYPE));
			break;
		case R.id.QuestionnairesButton: // Show questionnaires
			Intent openQuestionnairesControl = new Intent(Nephron.this, Questionnaire.class);
			startActivity(openQuestionnairesControl);
			break;
		case R.id.SettingsButton: // Show application settings
			Intent openSettingsControl = new Intent(Nephron.this, Settings.class);
			startActivity(openSettingsControl);
			break;
		case R.id.HistoryButton: // Show history
			Intent openHistory = new Intent(Nephron.this, History.class);
			startActivity(openHistory);
			break;
		case R.id.ExitImageButton:
			finish();
		default:
			break;
		}
	}

	//Create backup of Database on Smartphone
	public static void backupDatabase() throws IOException {

		//	Open your local db as the input stream
		String inFileName = "/data/data/nephron.mobile.application/databases/nephron";
		File dbFile = new File(inFileName);
		FileInputStream fis = new FileInputStream(dbFile);

		String outFileName = Environment.getExternalStorageDirectory() + "/nephron";

		//	Open the empty db as the output stream
		OutputStream output = new FileOutputStream(outFileName);

		//	transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = fis.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}

		//	Close the streams
		output.flush();
		output.close();
		fis.close();
	}
	
	
}
