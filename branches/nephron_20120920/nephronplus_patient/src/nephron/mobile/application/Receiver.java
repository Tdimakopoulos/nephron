package nephron.mobile.application;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import nephron.mobile.application.alarms.AlarmsMsg;
import nephron.mobile.application.alarms.MyAlertDialog;
import nephron.mobile.application.utils.DataTransformationUtils;
import nephron.mobile.backend.NephronSOAPClient;
import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

public class Receiver extends BroadcastReceiver {

	String current_State;
	MessageConverter converter ;
	boolean wait_for_update = false;
	public  List<AlarmsMsg> alerts;

	@SuppressWarnings("unchecked")
	@Override
	public void onReceive(Context context, Intent intent) {
		converter = new MessageConverter();
		String action = intent.getAction(); 

		if (action.equals("nephron.mobile.application.DataArrived")) {
			byte[] incomingMessage = intent.getByteArrayExtra("value");
			final SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");

			
			
			if (incomingMessage != null) {
				
				// TODO DEBUGGING, REMOVE WHEN READY
				//*********************************************************************
				
//				String  type = "";
//				if (unsignedByteToInt(incomingMessage[3]) == 5)
//					type = "ACK";
//				else if (unsignedByteToInt(incomingMessage[3]) == 4)
//					type = "WAKD STATUS";
//				else if (unsignedByteToInt(incomingMessage[3]) == 8)
//					type = "ALERT";
//				else if (unsignedByteToInt(incomingMessage[3]) == 12)
//					type = "PHYSIOLOGICAL";
//				else if (unsignedByteToInt(incomingMessage[3]) == 11)
//					type = "PHYSICAL SENSOR";
//				else if (unsignedByteToInt(incomingMessage[3]) == 22)
//					type = "WEIGHT";
//				else if (unsignedByteToInt(incomingMessage[3]) == 25)
//					type = "BLOOD PRESSURE";
//				else if (unsignedByteToInt(incomingMessage[3]) == 27)
//					type = "ECG PERIODIC";
//				else
//					type = "UNKNOWN INCOMING";
//				
//				Toast.makeText(context, "Incoming Data : " + unsignedByteToInt(incomingMessage[3]) +" / "+ type, Toast.LENGTH_SHORT).show();
				
				//*********************************************************************

				if (unsignedByteToInt(incomingMessage[3]) == 5) {  // ACK
					Intent i2 = new Intent("nephron.mobile.application.ACK"); 
					i2.putExtra("messageCount", unsignedByteToInt(incomingMessage[4]));
					context.sendBroadcast(i2);
				}
				else if (unsignedByteToInt(incomingMessage[3]) == 4) { // CurrentWakdStatus
					if(incomingMessage.length == 8) {
						// send Ack
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try {
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						createClientforWAKDStatus(context, incomingMessage);

						Date date = new Date();
						// Save WAKD Current State in db
						ConnectionService.db.insertAct(41, dateFormat.format(date), WakControl.wakdState(2, unsignedByteToInt(incomingMessage[6])), 2,
								dateFormat.format(date), 2, null, null, 4,
								null,  unsignedByteToInt(incomingMessage[6]),
								null, 1, 1);   

						// Save WAKD Current Operational State in db
						ConnectionService.db.insertAct(42, dateFormat.format(date), WakControl.wakdState(2, unsignedByteToInt(incomingMessage[7])), 2,
								dateFormat.format(date), 2, null, null, 4,
								null, unsignedByteToInt(incomingMessage[7]),
								null, 1, 1);

						// Publish event with incoming states
						Bundle b = new Bundle();
						b.putInt("wakdStateIs",unsignedByteToInt(incomingMessage[6]));
						b.putInt("wakdOpStateIs",unsignedByteToInt(incomingMessage[7]));
						Intent i2 = new Intent("nephron.mobile.application.WakStatusEvent");
						i2.putExtra("currentWakdState", b);
						context.sendBroadcast(i2);
					}
				}

				else if (unsignedByteToInt(incomingMessage[3]) == 8) { // ALERT
					if(incomingMessage.length == 8){
						// send Ack
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try {
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						createClientforStoreCommand(context, incomingMessage);

						byte[] alrm = new byte[] { incomingMessage[6], incomingMessage[7] };
						BigInteger inlet_sod = new BigInteger(alrm);
						String inlet_alrm = inlet_sod.toString(16);
						AlarmsMsg alarm_msg = new AlarmsMsg(Integer.parseInt(inlet_alrm, 16));

						final Date date = new Date();
						// Save Alert in db
						int alertid = (int) ConnectionService.db.insertAlert(dateFormat.format(date),
								alarm_msg.getmsgAlert(), 1,
								dateFormat.format(date),
								dateFormat.format(date),
								Integer.parseInt(inlet_alrm, 16), 2, 1, 1, 4);

						alarm_msg.setIdDatabase(alertid);
						((GlobalVar)context.getApplicationContext()).getNewAlarmsList().add(alarm_msg);

						Toast.makeText(context, "New Alert has arrived : " +Integer.parseInt(inlet_alrm, 16)
								+"\n"+alarm_msg.getmsgPatient(), Toast.LENGTH_SHORT).show();

						if(context.getSharedPreferences("userSettings", Context.MODE_WORLD_READABLE).getString("Vibration", "Off").equalsIgnoreCase("On")) {
							Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
							mVibrator.vibrate(500);
						}

						// send dataID_statusRequest
						WakdCommandEnum command2 = WakdCommandEnum.STATUS_REQUEST;
						MsgSimple statusRequest = new MsgSimple(incomingMessage[4] + 1,	command2);

						try {
							// Thread.sleep(10000); //10secs
							ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} else if (unsignedByteToInt(incomingMessage[3]) == 12) { // Physiological
					if(incomingMessage.length == 70){
						// send Ack to WAKD
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try { 
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						createClientforStoreCommand(context, incomingMessage);

						// Get Measurements from incoming Message
						byte[] inlet_Sodium = new byte[] { incomingMessage[10], incomingMessage[11] };
						BigInteger inlet_sod = new BigInteger(inlet_Sodium);
						String inlet_s = inlet_sod.toString(16);

						byte[] outlet_Sodium = new byte[] { incomingMessage[40], incomingMessage[41] };
						BigInteger outlet_sod = new BigInteger(outlet_Sodium);
						String outlet_s = outlet_sod.toString(16);

						byte[] inlet_Potassium = new byte[] { incomingMessage[16], incomingMessage[17] };
						BigInteger inlet_pot = new BigInteger(inlet_Potassium);
						String inlet_s1 = inlet_pot.toString(16);

						byte[] outlet_Potassium = new byte[] { incomingMessage[46], incomingMessage[47] };
						BigInteger outlet_pot = new BigInteger(outlet_Potassium);
						String outlet_s1 = outlet_pot.toString(16);

						byte[] inlet_pH = new byte[] { incomingMessage[22], incomingMessage[23] };
						BigInteger inlet_ph = new BigInteger(inlet_pH);
						String inlet_s2 = inlet_ph.toString(16);

						byte[] outlet_pH = new byte[] { incomingMessage[52], incomingMessage[53] };
						BigInteger outlet_ph = new BigInteger(outlet_pH);
						String outlet_s2 = outlet_ph.toString(16);

						byte[] inlet_Urea = new byte[] { incomingMessage[28], incomingMessage[29] };
						BigInteger inlet_ur = new BigInteger(inlet_Urea);
						String inlet_s3 = inlet_ur.toString(16);

						byte[] outlet_Urea = new byte[] { incomingMessage[58], incomingMessage[59] };
						BigInteger outlet_ur = new BigInteger(outlet_Urea);
						String outlet_s3 = outlet_ur.toString(16);

						byte[] inlet_Temperature = new byte[] { incomingMessage[34], incomingMessage[35] };
						BigInteger inlet_temp = new BigInteger(
								inlet_Temperature);
						String inlet_s4 = inlet_temp.toString(16);

						byte[] outlet_Temperature = new byte[] { incomingMessage[66], incomingMessage[65] };
						BigInteger outlet_temp = new BigInteger(outlet_Temperature);
						String outlet_s4 = outlet_temp.toString(16);

						Date date = new Date();

						// Save incoming inlet Calcium
						ConnectionService.db.insertAct(911, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(inlet_s, 16)) / 10, null, 1, 1);

						// Save incoming outlet Calcium
						ConnectionService.db.insertAct(912, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(outlet_s, 16)) / 10,
								null, 1, 1);

						// Save incoming inlet Potassium
						ConnectionService.db.insertAct(921, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(inlet_s1, 16)) / 10, null, 1, 1);

						// Save incoming outlet Potassium
						ConnectionService.db.insertAct( 922, dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4, null, ((double) Integer.parseInt(outlet_s1, 16)) / 10,
								null, 1, 1);

						// Save incoming inlet pH
						ConnectionService.db.insertAct(931, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(inlet_s2, 16)) / 10, null, 1, 1);

						// Save incoming outlet pH
						ConnectionService.db.insertAct( 932, dateFormat.format(date), null, 2, dateFormat.format(date),
								2, null, null, 4, null, ((double) Integer.parseInt(outlet_s2, 16)) / 10, null, 1, 1);

						// Save incoming inlet Urea
						ConnectionService.db.insertAct(941, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(inlet_s3, 16)) / 10, null, 1, 1);

						// Save incoming outlet Urea
						ConnectionService.db.insertAct( 942, dateFormat.format(date), null, 2, dateFormat.format(date), 2,
								null, null, 4, null, ((double) Integer.parseInt(outlet_s3, 16)) / 10, null, 1, 1);

						// Save incoming inlet Temperature
						ConnectionService.db.insertAct(951, dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(inlet_s4, 16)) / 10, null, 1, 1);

						// Save incoming outlet Temperature
						ConnectionService.db.insertAct( 952, dateFormat.format(date), null, 2, dateFormat.format(date), 2,
								null, null, 4, null, ((double) Integer.parseInt(outlet_s4, 16)) / 10, null, 1, 1);

						// publish Sodium,Potassium,Urea,pH
						Bundle b = new Bundle();  
						b.putString("CalciumIn", dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s, 16)) / 10 +" mmol/L");
						b.putString("CalciumOut", dateFormat.format(date)+"    "+((double) Integer.parseInt(outlet_s, 16)) / 10 +" mmol/L");
						b.putString("PotassiumIn",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s1, 16)) / 10 +" mmol/L");
						b.putString("PotassiumOut",dateFormat.format(date)+"    "+((double) Integer.parseInt(outlet_s1, 16)) / 10 +" mmol/L"); 
						b.putString("UreaIn",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s3, 16)) / 10+" mmol/L");
						b.putString("UreaOut",dateFormat.format(date)+"    "+((double) Integer.parseInt(outlet_s3, 16)) / 10+" mmol/L"); 
						b.putString("pHIn",dateFormat.format(date)+"    "+((double) Integer.parseInt(inlet_s2, 16)) / 10);
						b.putString("pHOut",dateFormat.format(date)+"    "+((double) Integer.parseInt(outlet_s2, 16)) / 10);
						Intent i2 = new Intent("nephron.mobile.application.PhysiologicalMeasurementEvent");
						i2.putExtra("physiologicalData", b);
						context.sendBroadcast(i2);
					}

				} 
				else if(unsignedByteToInt(incomingMessage[3]) == 17) {	// Physical Sensor Data
					Toast.makeText(context, "Physical Sensor Data, Size = "+incomingMessage.length, Toast.LENGTH_SHORT).show();
					if(incomingMessage.length == 52) {

						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try { 
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						createClientforStoreCommand(context, incomingMessage);

						// Get Measurements from incoming Message
						Date date = new Date();

						// Pressure FCI
						byte[] pressFCI = new byte[] { incomingMessage[10], incomingMessage[11] };
						BigInteger press_FCI = new BigInteger(pressFCI);
						String pressureFCI = press_FCI.toString(16);

						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(pressureFCI, 16)), null, 1, 1);

						// Pressure FCO
						byte[] pressFCO = new byte[] { incomingMessage[16], incomingMessage[17] };
						BigInteger press_FCO = new BigInteger(pressFCO);
						String pressureFCO = press_FCO.toString(16);

						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_FCO),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(pressureFCO, 16)), null, 1, 1);

						// Pressure BCI
						byte[] pressBCI = new byte[] { incomingMessage[22], incomingMessage[23] };
						BigInteger press_BCI = new BigInteger(pressBCI);
						String pressureBCI = press_BCI.toString(16);

						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(pressureBCI, 16)), null, 1, 1);

						// Pressure BCO
						byte[] pressBCO = new byte[] { incomingMessage[28], incomingMessage[29] };
						BigInteger press_BCO = new BigInteger(pressBCO);
						String pressureBCO = press_BCO.toString(16);

						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BCO),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(pressureBCO, 16)), null, 1, 1);

						//Temperature IN
						byte[] tempIn = new byte[] { incomingMessage[34], incomingMessage[35] };
						BigInteger temp_in = new BigInteger(tempIn);
						String temperature_In = temp_in.toString(16);
						
						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(temperature_In, 16)) / 10, null, 1, 1);

						//Temperature OUT
						byte[] tempOut = new byte[] { incomingMessage[36], incomingMessage[37] };
						BigInteger temp_Out = new BigInteger(tempOut);
						String temperature_Out = temp_Out.toString(16);
						
						ConnectionService.db.insertAct(
								MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(temperature_Out, 16)) / 10, null, 1, 1);

						// Conductivity
						byte[] condFCR = new byte[] { incomingMessage[42], incomingMessage[43] };
						BigInteger cond_FCR = new BigInteger(condFCR);
						String conductivity_FCR = cond_FCR.toString(16);
						
						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(conductivity_FCR, 16)) / 10, null, 1, 1);

						// imaginary, not used
//						byte[] condFCQ = new byte[] { incomingMessage[44], incomingMessage[45] };
//						BigInteger cond_FCQ = new BigInteger(condFCQ);
//						String conductivity_FCQ = cond_FCQ.toString(16);
//						
//						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_FCQ),
//								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
//								null, ((double) Integer.parseInt(conductivity_FCQ, 16)) / 10, null, 1, 1);

						byte[] condPT = new byte[] { incomingMessage[46], incomingMessage[47] };
						BigInteger cond_PT = new BigInteger(condPT);
						String conductivity_PT = cond_PT.toString(16);
						
						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE),
								dateFormat.format(date), null, 2, dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(conductivity_PT, 16)) / 10, null, 1, 1);
						
						
						Bundle b = new Bundle();  
						b.putString("pressureDialysate", dateFormat.format(date)+"    "+((double) Integer.parseInt(pressureFCI, 16)) / 10 +" mmHG");
						b.putString("pressureFCO", dateFormat.format(date)+"    "+((double) Integer.parseInt(pressureFCO, 16)) / 10 +" mmHG");
						b.putString("pressureBloodline", dateFormat.format(date)+"    "+((double) Integer.parseInt(pressureBCI, 16)) / 10 +" mmHG");
						b.putString("pressureBCO", dateFormat.format(date)+"    "+((double) Integer.parseInt(pressureBCO, 16)) / 10 +" mmHG");
						b.putString("temperatureBloodlineIn", dateFormat.format(date)+"    "+((double) Integer.parseInt(temperature_In, 16)) / 10 +" �C");
						b.putString("temperatureBloodlineOut", dateFormat.format(date)+"    "+((double) Integer.parseInt(temperature_Out, 16)) / 10 +" �C");
						b.putString("conductivityDialysate", dateFormat.format(date)+"    "+((double) Integer.parseInt(conductivity_FCR, 16)) / 10);
//						b.putString("conductivityFCQ", dateFormat.format(date)+"    "+((double) Integer.parseInt(conductivity_FCQ, 16)) / 10);
						b.putString("CondTempDialysate", dateFormat.format(date)+"    "+((double) Integer.parseInt(conductivity_PT, 16)) / 10);
						Intent i = new Intent("nephron.mobile.application.PhysicalsensorMeasurementEvent");
						i.putExtra("physicalSensorData", b);
						context.sendBroadcast(i);
					}

				}
 


				else if (unsignedByteToInt(incomingMessage[3]) == 22) { // Weight
					if(incomingMessage.length == 19) { 
						// send Ack
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try {
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						// Get Measurements from incoming Message 
						byte[] Weight = new byte[] { incomingMessage[6], incomingMessage[7] };
						BigInteger bi = new BigInteger(Weight);
						String s = bi.toString(16);

						byte[] WSBatteryLevel = new byte[] { incomingMessage[9], incomingMessage[10] };
						BigInteger bat = new BigInteger(WSBatteryLevel).divide(new BigInteger("10"));
						String s1 = bat.toString(16);

						byte[] WSStatus =  new byte[] { incomingMessage[11], incomingMessage[12],
														incomingMessage[13], incomingMessage[14] };
						BigInteger status = new BigInteger(WSStatus);
						String s2 = status.toString(16);



						Date date = new Date();
						// Save Weight in db
						ConnectionService.db.insertAct(181, dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(s, 16)) / 10,
								null, 1, 1);

						// Save WAKD Battery Level db
						ConnectionService.db.insertAct(183, dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(s1, 16)),
								null, 1, 1);

						// Save WSStatus in db
						ConnectionService.db.insertAct(184, dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, ((double) Integer.parseInt(s2, 16)),
								null, 1, 1);

						// publish Battery Level
						Intent i = new Intent("nephron.mobile.application.WakStatusEvent");
						i.putExtra("WSBatteryLevel",(int) Integer.parseInt(s1, 16));
						context.sendBroadcast(i);

						// publish weight,bodyfat
						Bundle b = new Bundle();
						b.putDouble("weight",((double) Integer.parseInt(s, 16)) / 10); 
						Intent i2 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent");
						i2.putExtra("weight", b);
						context.sendBroadcast(i2);

						// publish the whole Physical Measurement Message
						Intent i3 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent1");
						i3.putExtra("message",incomingMessage);
						context.sendBroadcast(i3);
					}
				}

				else if (unsignedByteToInt(incomingMessage[3]) == 27) { // EcgData Periodic, Careful, has different Intent action than ECG Stream

					Log.d("INCOMING PERIODIC ECG DATA", "PACKET LENGTH IS : " + incomingMessage.length);
					if(incomingMessage.length == 13){
						// send Ack TO MB
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try {
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						createClientforStoreCommand(context, incomingMessage);

						Date date = new Date();

						// Save Pulse
						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.HEART_RATE),
								dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, (unsignedByteToInt(incomingMessage[6])),
								null, 1, 1);

						// Save Respiration Rate
						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.RESPIRATION_RATE),
								dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, (unsignedByteToInt(incomingMessage[7])),
								null, 1, 1);

						// Save Activity Level
						ConnectionService.db.insertAct(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.ACTIVITY_LEVEL),
								dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, (unsignedByteToInt(incomingMessage[8])),
								null, 1, 1);

						// publish Physical Measurements
						Bundle b = new Bundle();
						b.putString("heartRate",dateFormat.format(date)+"    "+((double) unsignedByteToInt(incomingMessage[6]))/10 + "");
						b.putString("respirationRate",dateFormat.format(date)+"    "+((double) unsignedByteToInt(incomingMessage[7]))/10 + ""); 
						b.putString("activityLevel",dateFormat.format(date)+"    "+((double) unsignedByteToInt(incomingMessage[8]))/10 + "");
						Intent i2 = new Intent("nephron.mobile.application.ECGPeriodicMeasurementEvent");
						i2.putExtra("ecgData", b);
						context.sendBroadcast(i2);
					}
				}
				else if (unsignedByteToInt(incomingMessage[3]) == 25/*19*/) { // Blood Pressure Data

					Log.d("Blood Pressure data packet Size", String.valueOf(incomingMessage.length));
					if(incomingMessage.length == 13) {
						// send ACK TO MB after the BP Data arrived
						WakdCommandEnum command = WakdCommandEnum.ACK;
						MsgSimple ack = new MsgSimple(incomingMessage[4], command);
						try {
							ConnectionService.u.sendDatatoWAKD(ack.encode());
							Log.d(" ack Send "," ackSend = " + converter.BytesToString(ack.encode()));
						} catch (IOException e) {
							e.printStackTrace();
						}

						Date date = new Date();

						byte[] systolic = new byte[] {incomingMessage[10],incomingMessage[11],incomingMessage[12],incomingMessage[13]};
						byte[] diastolic = new byte[] {incomingMessage[14],incomingMessage[15],incomingMessage[16],incomingMessage[17]};

						BigInteger systolicbi = new BigInteger(systolic);
						String systolicS = systolicbi.toString(16);

						BigInteger diastolicbi = new BigInteger(diastolic);
						String diastolicS = diastolicbi.toString(16);

						// Save Systolic Blood Pressure
						ConnectionService.db.insertAct(234,	dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null,	4,
								null, (double) Integer.parseInt(systolicS, 16), null, 1, 1);

						// Save Diastolic Blood Pressure
						ConnectionService.db.insertAct(235, dateFormat.format(date), null, 2,
								dateFormat.format(date), 2, null, null, 4,
								null, (double) Integer.parseInt(diastolicS, 16),
								null, 1, 1);

						// publish Physical Measurements
						Bundle b = new Bundle(); 
						b.putDouble("systolic_bp", ((double) Integer.parseInt(systolicS, 16))/10);
						b.putDouble("diastolic_bp", ((double) Integer.parseInt(diastolicS, 16))/10);
						Intent i2 = new Intent("nephron.mobile.application.PhysicalMeasurementEvent2");
						i2.putExtra("blood_pressure", b);
						context.sendBroadcast(i2);
					}
				}
				else {
					Toast.makeText(context, "Unknown Message received:\n\n"+incomingMessage.toString(), Toast.LENGTH_LONG);
				}
			}
		}
		else {	// if there are unchecked Alerts display them on screen

			if (action.equals("nephron.mobile.application.AlertsReminder")) {
				alerts =   (List<AlarmsMsg>) intent.getExtras().get("value");
				for (int i = 0; i < alerts.size(); i++) {
					final int count=i;
					Intent trIntent = new Intent("android.intent.action.MAIN");
					trIntent.setClass(context, MyAlertDialog.class);
					trIntent.setAction("ALARM_FROM_RECEIVER");
					trIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					trIntent.putExtra("alertid",Integer.toString(alerts.get(count).getIdDatabase()));
					trIntent.putExtra("alertMessage",alerts.get(count).getmsgPatient());
					context.startActivity(trIntent);
				}
			}
		}

	}


	private void createClientforWAKDStatus(final Context context, final byte[] incomingMessage) {

		class SoapWAKDStatusClient implements Runnable {
			@Override
			public void run() {
				NephronSOAPClient soapClient = new NephronSOAPClient(context);
				String strCommand = DataTransformationUtils.convertByteArrayToString(incomingMessage);
				soapClient.executeStoreWAKDStatusRequest(strCommand, new Date().getTime(), null);
			}
		}
		Thread thread = new Thread(new SoapWAKDStatusClient());
		thread.start();
	}


	private void createClientforStoreCommand(final Context context, final byte[] incomingMessage) {

		class SoapWAKDStatusClient implements Runnable {
			@Override
			public void run() {
				NephronSOAPClient soapClient = new NephronSOAPClient(context);
				String strCommand = DataTransformationUtils.convertByteArrayToString(incomingMessage);
				soapClient.executeStoreCommandRequest(strCommand, new Date().getTime(), null);
			}
		}
		Thread thread = new Thread(new SoapWAKDStatusClient());
		thread.start();
	}


	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public static int unsignedByteArrayToInt(byte[] byteArray, int offset) {
		int ret = 0;
		for (int i=0; i<4 && i+offset<byteArray.length; i++) {
			ret <<= 8;
			ret |= (int)byteArray[i] & 0xFF;
		}
		return ret;
	}


}

