package nephron.mobile.application;


import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import nephron.mobile.statistics.Statistics;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class ECGMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = { "Heart Rate", "Respiration Rate", "Activity Level"};
	//	private String userSelectedMeasurements[];
	SharedPreferences myPrefs;
	String prefName;
	Bundle b;
	Intent openMeasurements;
	Button backButton;
	ArrayAdapter<String> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ecgmeasurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackButtonECGMeasurements);
		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical"))
			openMeasurements = new Intent(ECGMeasurements.this, Statistics.class);
		else
			openMeasurements = new Intent(ECGMeasurements.this, MeasurementsTable.class);

		_measurementsListView = (ListView) findViewById(R.id.ECGMeasurementsListView);

		adapter = new ArrayAdapter<String>(ECGMeasurements.this, R.layout.backgroundlayout, R.id.backtextview,	_measurementsElementArray);

		_measurementsListView.setAdapter(adapter);

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// handle the various choices
				Toast.makeText(ECGMeasurements.this, adapter.getItem(position), Toast.LENGTH_SHORT).show();
				String item = adapter.getItem(position);
				if(item.equalsIgnoreCase("Heart Rate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.HEART_RATE));
					b.putString("header1", "Daily Heart Rate Trends");
					b.putString("header2", "Weekly Heart Rate Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Respiration Rate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.RESPIRATION_RATE));
					b.putString("header1", "Daily Respiration Rate Trends");
					b.putString("header2", "Weekly Respiration Rate Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Activity Level")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.ACTIVITY_LEVEL));
					b.putString("header1", "Daily Activity Level Trends");
					b.putString("header2", "Weekly Activity Level Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				//TODO Remove when tested
				else{
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage("Unidentified Selection has been made, something has gone wrong");
					_dialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss();
						}
					});
					_dialog.show();

				}
			}
		});



		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ECGMeasurements.this.finish();
			}
		});
	}
}
