package nephron.mobile.application;

import java.io.IOException;
import java.math.BigInteger;

import nephron.mobile.application.R.drawable;
import nephron.mobile.datafunctions.ByteUtils;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class GetWeight extends Activity {

	AlertDialog.Builder passwordBuilder, bpasswordInformer, confirmation, correctWeightData;
	EditText mEditText, weightInput, bodyFatInput;
	AlertDialog passwordChecker, passwordInformer, message;
	ProgressDialog progressDialog;
	Boolean authCheck=false;
	int MessageCounter, incomingID;
	String storedPassword;
	MessageConverter converter;
	private String weightfilter = "nephron.mobile.application.PhysicalMeasurementEvent1";
	private String ackFilter = "nephron.mobile.application.ACK";

	private IncomingReceiver receiver;


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		passwordBuilder = new AlertDialog.Builder(GetWeight.this);
		confirmation = new AlertDialog.Builder(this);
		progressDialog = new ProgressDialog(getApplicationContext());
		message = new AlertDialog.Builder(this).create();
		
		LayoutInflater li = LayoutInflater.from(this);
		View correctWeight = li.inflate(R.layout.correctweight, null);
		weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
		bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
		weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		correctWeightData = new AlertDialog.Builder(this);
		
		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ackFilter);
		intentFilter.addAction(weightfilter);
		registerReceiver(receiver, intentFilter);

		View passwordView = LayoutInflater.from(GetWeight.this).inflate(R.layout.passwordchecker, null);
		mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
		passwordBuilder.setTitle("Password Warning");
		passwordBuilder.setIcon(drawable.warning);
		passwordBuilder.setView(passwordView);

		passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(passwordChecker.isShowing())
					passwordChecker.dismiss();

			}
		});

		passwordBuilder.setPositiveButton("OK", new  OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(mEditText.length() == 0) {
					authCheck = false;
					bpasswordInformer.setMessage("Empty password field, please try again.");
					passwordInformer = bpasswordInformer.create();
					passwordInformer.show();
				}
				else {
					storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
					if(mEditText.getText().toString().equals(storedPassword)) { 
						passwordChecker.dismiss();

						confirmation.setMessage("Step up on weight scale and press OK" );
						confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int which) {

								progressDialog = ProgressDialog.show(GetWeight.this, "", "Please wait...", false, true);

								new Thread(new Runnable() {
									public void run() {
										WakdCommandEnum command = WakdCommandEnum.WEIGHT_REQUEST;
										MsgSimple weightRequest;
										boolean ackArrived = false;
										MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
										for(int i=0;i<3;i++){
											weightRequest = new MsgSimple(MessageCounter, command);
											try {
												// Thread.sleep(10000); //10secs
												ConnectionService.u.sendDatatoWAKD(weightRequest.encode());
												Thread.sleep(5 * 1000);
											} catch (IOException e) {
												e.printStackTrace();
											} catch (InterruptedException e) {
												e.printStackTrace();
											} catch(Exception e) {
												e.printStackTrace();
											}


											if (incomingID == MessageCounter ) {
												ackArrived = true ;
												break;
											}										 
										}


										if(ackArrived) {
											if(progressDialog!=null) {
												if (progressDialog.isShowing()) {
													progressDialog.dismiss();
													Looper.prepare();
													progressDialog = ProgressDialog.show(GetWeight.this, "",
															"Your Command Arrived Successfully\nPlease wait for Response to come...");
													new Thread(new Runnable() {
														public void run() {
															Log.d(Functions.class.getName(),"Response");
															try {
																Thread.sleep(7 * 1000);
																if (progressDialog.isShowing()) {
																	//	Log.d("mesa sto to thread ","mesas sto to thread");
																	progressDialog.dismiss();
																	Looper.prepare();
																	message.setMessage("Nothing Happened");
																	message.setButton("OK",
																			new DialogInterface.OnClickListener() {
																		public void onClick(
																				DialogInterface dialog,
																				int which) {
																			// here you can add functions
																		}
																	});
																	message.show();
																	Looper.loop();
																}
															} catch (InterruptedException e) {
																e.printStackTrace();
															}
														}
													}).start();
													Looper.loop();
												}}	
										}
										else{
											if(progressDialog!=null){
												if (progressDialog.isShowing()) {
													progressDialog.dismiss();
												}}
											Looper.prepare();
											message.setMessage("Your Command did not\narrive to WAKD");
											message.setButton("OK",
													new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {
													// here you can add functions
												}
											});
											message.show();
											Looper.loop();
										}
									}
								}).start();
							}
						});
						confirmation.setNegativeButton("Cancel",null);
						confirmation.show();
					}
					else
					{
						bpasswordInformer.setMessage("Incorrect password, please try again.");
						passwordInformer = bpasswordInformer.create();
						passwordInformer.show();
					}
				}

			}
		});

		passwordChecker = passwordBuilder.create();
		passwordChecker.show();
	}



	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Weight case
			if(intent.getAction().equals(weightfilter)) {
				final byte[] message  =  intent.getByteArrayExtra("message");

				message[0] = ByteUtils.intToByte(1);  
				message[3] = ByteUtils.intToByte(23);
				message[5] = ByteUtils.intToByte(4);

				byte[] Weight = new byte[] { message[6], message[7] };
				BigInteger bi = new BigInteger(Weight);
				String s = bi.toString(16);

				final double weight  = ((double) Integer.parseInt(s, 16)) / 10;
				final int bodyfat  = unsignedByteToInt(message[8]);
				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}


				confirmation.setTitle("Weight Info Arrived").setMessage(
						"Current \n Weight :  "
								+ weight + " Kg \n Body Fat :"
								+ bodyfat + " %");
				confirmation.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {

						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
						Log.d(" Steilame "," Steilame me = " + MessageCounter);
						message[4] = ByteUtils.intToByte(MessageCounter) ;
						MessageConverter converter = new MessageConverter();
						Log.d(" send weight Ok "," send weight ok = " + converter.BytesToString(message));
						try {
							ConnectionService.u.sendDatatoWAKD(message);												 
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				confirmation.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						weightInput.setText(Double.toString(weight));
						bodyFatInput.setText(Integer.toString(bodyfat));
						correctWeightData.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Log.d("!!!!!!!!!!!"," weight = "+  (int) (Double.parseDouble(weightInput.getText().toString()) * 10) +" kai bodyFat " +  (int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
								System.arraycopy(ByteUtils.intToByteArray((int) (Double.parseDouble(weightInput.getText().toString()) * 10), 2), 0, message, 6, 2);
								message[8] = ByteUtils.intToByte((int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
								// WakdCommandEnum command = WakdCommandEnum.WEIGHT_DATA;
								// MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
								//MsgWeightDataOk   WeightData = new MsgWeightDataOk(MessageCounter,command, (int )weight*10,(int) bodyfat * 10, 900);
								Log.d(" correct weight "," correct weight  = " + converter.BytesToString(message));
								try { 
									ConnectionService.u.sendDatatoWAKD(message);
								} catch (IOException e) { 
									e.printStackTrace();
								}
							}
						});

						correctWeightData.setNegativeButton("No",null);
						correctWeightData.show();
					}
				});
				confirmation.show();
			}
			
			// Case of ACK
			else if(intent.getAction().equals(ackFilter)) {
				incomingID = intent.getIntExtra("messageCount", 0);
				Log.d("incoming ID","IncomingID has been set ");
			}
		}
	}



	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}




}
