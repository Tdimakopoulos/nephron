package nephron.mobile.application;

import nephron.mobile.application.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class Settings extends Activity { 

	private static final String physicalMeasElementArray[] = {
				"Calcium In", "Calcium Out",
				"Potassium In", "Potassium Out",
				"Urea In", "Urea Out",
				"pH In", "pH Out",
				"Temperature In", "Temperature Out"};
	
	private static final String physioMeasElementArray[] = { 
				"Conductivity Dialysate", "Dialysate", "Pressure Dialysate", 
				"Conductivity Temperature Dialysate", "Pressure Bloodline",
				"Temperature Bloodline Incoming",
				"Temperature Bloodline Outgoing"};

	private RadioGroup radioMeasurementGroup, vibrationRadioGroup;
	private Button changePassword, backButton;
	SharedPreferences userSettings;
	SharedPreferences.Editor prefsEditor;
	AlertDialog.Builder confirmation, bpassword, measurementsSelector;
	AlertDialog passwordDialog;
	EditText currentPassword, newPassword, verifyPassword;
	TextView physicalMeasurementsText, physioMeasurementsText;
	boolean[] physicalCheckedItems, physioCheckedItems;

	TextView myMsg;
	View passwordManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingslayout);
		
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		physicalMeasurementsText = (TextView) findViewById(R.id.PhysicalMeasSelector);
		physioMeasurementsText = (TextView) findViewById(R.id.PhysioMeasSelector);

		physicalCheckedItems = new boolean[physicalMeasElementArray.length];
		physioCheckedItems = new boolean[physioMeasElementArray.length];

		userSettings = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefsEditor = userSettings.edit();

		//***********************************************************************************************

		physicalMeasurementsText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String physicalMeasPref = userSettings.getString("physicalMeasSelected", "1111111111");
				for(int i=0; i<physicalMeasPref.length(); i++) {
					if(physicalMeasPref.charAt(i) == '1')
						physicalCheckedItems[i] = true;
					else
						physicalCheckedItems[i] = false;
				}

				measurementsSelector = new Builder(Settings.this);
				measurementsSelector.setMultiChoiceItems(physicalMeasElementArray, physicalCheckedItems, new OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if(isChecked)
							physicalCheckedItems[which] = true;
						else
							physicalCheckedItems[which] = false;
					}
				});

				measurementsSelector.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Save items here
						char[] physicalsToStore = new char[physicalMeasElementArray.length];
						for(int i=0; i<physicalCheckedItems.length; i++) {
							if(physicalCheckedItems[i])
								physicalsToStore[i]='1';
							else
								physicalsToStore[i]='0';
						}
						prefsEditor.putString("physicalMeasSelected", String.valueOf(physicalsToStore));
						prefsEditor.commit();
					}
				});

				measurementsSelector.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				measurementsSelector.create().show();
			}
		});

		//***********************************************************************************************


		physioMeasurementsText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String physioMeasPref = userSettings.getString("physioMeasSelected", "1111111");
				for(int i=0; i<physioMeasPref.length(); i++) {
					if(physioMeasPref.charAt(i) == '1')
						physioCheckedItems[i] = true;
					else
						physioCheckedItems[i] = false;
				}

				measurementsSelector = new Builder(Settings.this);
				measurementsSelector.setMultiChoiceItems(physioMeasElementArray, physioCheckedItems, new OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if(isChecked)
							physioCheckedItems[which] = true;
						else
							physioCheckedItems[which] = false;
					}
				});

				measurementsSelector.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Save items here
						char[] physioToStore = new char[7];
						for(int i=0; i<physioCheckedItems.length; i++) {
							if(physioCheckedItems[i])
								physioToStore[i]='1';
							else
								physioToStore[i]='0';
						}
						prefsEditor.putString("physioMeasSelected", String.valueOf(physioToStore));
						prefsEditor.commit();
					}
				});

				measurementsSelector.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				measurementsSelector.create().show();
			}
		});


		//***********************************************************************************************

		String presentationSelection = userSettings.getString("MeasurementPresentation", "Graphical");
		String vibrationSelection = userSettings.getString("Vibration", "Off");
//		String alarmsFrequency = userSettings.getString("AlarmsFrequency", "1");

		confirmation = new AlertDialog.Builder(this);
		radioMeasurementGroup=(RadioGroup) findViewById(R.id.MeasurementPresentationGroup);
		vibrationRadioGroup = (RadioGroup) findViewById(R.id.SelectVibrationGroup);
//		alarmFrequencyGroup = (RadioGroup) findViewById(R.id.AlarmFrequencyGroup);

		changePassword = (Button) findViewById(R.id.PasswordButton);
		backButton = (Button) findViewById(R.id.BackSettingsButton);

		// Check appropriate radio button for data presentation mode
		if(presentationSelection.equals("Graphical")) {
			radioMeasurementGroup.check(R.id.GraphicalRadioButton);
		}else{
			radioMeasurementGroup.check(R.id.TableRadioButton);
		}

		// Check appropriate radio button for vibration mode
		if(vibrationSelection.equals("Off"))
			vibrationRadioGroup.check(R.id.VibrationOffRadioButton);
		else
			vibrationRadioGroup.check(R.id.VibrationOnRadioButton);

		// Check appropriate radio button for alarms Frequency
//		if(alarmsFrequency.equals("1"))
//			alarmFrequencyGroup.check(R.id.OneMinuteRadioButton);
//		else if(alarmsFrequency.equals("2"))
//			alarmFrequencyGroup.check(R.id.TwoMinutesRadioButton);
//		else
//			if(alarmsFrequency.equals("5"))
//				alarmFrequencyGroup.check(R.id.FiveMinutesRadioButton);
//
//		alarmFrequencyGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				RadioButton radioButton = (RadioButton) findViewById(checkedId);
//				if(radioButton.getText().toString().equalsIgnoreCase("1 minute"))
//					prefsEditor.putString("AlarmsFrequency", "1");
//				else if(radioButton.getText().toString().equalsIgnoreCase("2 minutes"))
//					prefsEditor.putString("AlarmsFrequency", "2");
//				else if(radioButton.getText().toString().equalsIgnoreCase("5 minutes"))
//					prefsEditor.putString("AlarmsFrequency", "5");
//				prefsEditor.commit();
//			}
//		});

		radioMeasurementGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				RadioButton radioButton = (RadioButton) findViewById(checkedId);
				if(radioButton.getText().toString().equalsIgnoreCase("Graphical"))
					prefsEditor.putString("MeasurementPresentation", "Graphical");
				else if(radioButton.getText().toString().equalsIgnoreCase("Table"))
					prefsEditor.putString("MeasurementPresentation", "Table");
				prefsEditor.commit();
			}
		});

		vibrationRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				RadioButton radioButton = (RadioButton) findViewById(checkedId);
				if(radioButton.getText().toString().equalsIgnoreCase("Off"))
					prefsEditor.putString("Vibration", "Off");
				else if(radioButton.getText().toString().equalsIgnoreCase("On"))
					prefsEditor.putString("Vibration", "On");
				prefsEditor.commit();
			}
		});

		changePassword.setOnClickListener(new OnClickListener() {

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

			public void onClick(View arg0) {
				LayoutInflater li = LayoutInflater.from(Settings.this);
				passwordManager = li.inflate(R.layout.changepasswordlayout, null);

				bpassword = new AlertDialog.Builder(Settings.this);
				bpassword.setView(passwordManager);
				bpassword.setTitle("Password Manager");
				bpassword.setIcon(drawable.password);

				currentPassword = (EditText) passwordManager.findViewById(R.id.CurrentPassword);
				newPassword = (EditText) passwordManager.findViewById(R.id.newPassword);
				verifyPassword = (EditText) passwordManager.findViewById(R.id.verifyPassword);

				bpassword.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
						AlertDialog.Builder dialogBuilder = new Builder(Settings.this);
						dialogBuilder.setTitle("Error");
						dialogBuilder.setNeutralButton("OK", null);
						if(!currentPassword.getText().toString().equals(storedPassword)){
							dialogBuilder.setMessage("Current Password mismatch");
							dialogBuilder.show();
						}
						else {
							if((newPassword.getText().toString()).matches("") || (verifyPassword.getText().toString()).matches("")){
								dialogBuilder.setMessage("Fill in all required fields");
								dialogBuilder.show();
							}
							else {
								if(!(newPassword.getText().toString()).equals(verifyPassword.getText().toString())) {
									dialogBuilder.setMessage("New password mismatch");
									dialogBuilder.show();
								}
								else {
									ConnectionService.db.updateParameterValue("USER_PASSWORD", newPassword.getText().toString());
									dialogBuilder.setTitle("Success");
									dialogBuilder.setMessage("Password has changed");
									passwordDialog.dismiss();
									dialogBuilder.show();
								}

							}

						}

					}

				});

				bpassword.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						passwordDialog.dismiss();
					}
				});

				passwordDialog = bpassword.create();
				passwordDialog.show();

			}
		});


		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
}
