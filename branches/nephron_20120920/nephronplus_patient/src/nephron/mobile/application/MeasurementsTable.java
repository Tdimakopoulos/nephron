package nephron.mobile.application;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Display;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class MeasurementsTable extends Activity {

	private String filter = "nephron.mobile.application.PhysiologicalMeasurementEvent";
	private String filter2 = "nephron.mobile.application.ECGPeriodicMeasurementEvent";
	private String filter3 = "nephron.mobile.application.PhysicalsensorMeasurementEvent";

	private IncomingReceiver receiver;
	private ListView dailyMeasurementsListView, weeklyMeasurementsListView;
	private TextView dailyTableTextView, weeklyTableTextView;
	ArrayAdapter<String> dailyDataAdapter, weeklyDataAdapter;
	LinkedList<String> dailyMeasurements;
	List<String> weeklyMeasurements;
	LinearLayout dailyLinear, weeklyLinear;
	int code;
	String head1, head2, dailyMeasurement, weeklyMeasurement, unit="";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementstablelayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);
		registerReceiver(receiver, intentFilter);

		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated

		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		if (MeasurementsCodesEnum.getMeasurementsCodesEnum(code) != MeasurementsCodesEnum.PH_IN &&
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) != MeasurementsCodesEnum.PH_OUT) {
			unit = " mmol/L";
		}
		if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_IN  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_OUT  || 
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.COND_TEMP_DIALYSATE)
			unit = " �C";
		
		//TODO Add units for other measurements

		dailyMeasurementsListView = (ListView) findViewById(R.id.DailyMeasurementsListView);
		weeklyMeasurementsListView = (ListView) findViewById(R.id.WeeklyMeasurementsListView);
		dailyLinear = (LinearLayout) findViewById(R.id.DailyLinear);
		weeklyLinear = (LinearLayout) findViewById(R.id.WeeklyLinear);
		dailyTableTextView = (TextView) findViewById(R.id.DailyTableTextView);
		weeklyTableTextView = (TextView) findViewById(R.id.WeeklyTableTextView);
		dailyTableTextView.setText(head1);
		weeklyTableTextView.setText(head2);

		LinearLayout.LayoutParams dailyparams = (LayoutParams) dailyLinear
				.getLayoutParams();
		dailyparams.height = height / 2;
		dailyLinear.setLayoutParams(dailyparams);

		LinearLayout.LayoutParams weeklyparams = (LayoutParams) weeklyLinear.getLayoutParams();
		weeklyparams.height = height / 2;
		weeklyLinear.setLayoutParams(weeklyparams);

		dailyMeasurements = new LinkedList<String>();
		weeklyMeasurements = new ArrayList<String>();

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		if (c.getCount() == 0) {
			dailyMeasurements.addFirst("No Data Yet");
		}
		while (c.moveToNext()) {
			dailyMeasurement = c.getString(1) + "    " +   String.format("%.1f", c.getDouble(0) )  + unit;
			dailyMeasurements.addFirst(dailyMeasurement);
		}

		c.close();

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		while (c2.moveToNext()) {
			weeklyMeasurement = c2.getString(1) + "    " +  String.format("%.1f", c2.getDouble(0) )
					+ unit;
			weeklyMeasurements.add(weeklyMeasurement);
		}
		c2.close();

		dailyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				dailyMeasurements);

		dailyMeasurementsListView.setAdapter(dailyDataAdapter);

		weeklyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				weeklyMeasurements);

		weeklyMeasurementsListView.setAdapter(weeklyDataAdapter);
	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (intent.getAction().equals(filter)) {

				if (intent.hasExtra("physiologicalData")) {
					Bundle b = intent.getBundleExtra("physiologicalData");
					String CalciumIn = b.getString("CalciumIn");
					String CalciumOut = b.getString("CalciumOut");
					String PotassiumIn = b.getString("PotassiumIn");
					String PotassiumOut = b.getString("PotassiumOut");
					String UreaIn = b.getString("UreaIn");
					String UreaOut = b.getString("UreaOut");
					String pHIn = b.getString("pHIn");
					String pHOut = b.getString("pHOut");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN))
						dailyMeasurement = CalciumIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT))
						dailyMeasurement = CalciumOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN))
						dailyMeasurement = PotassiumIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT))
						dailyMeasurement = PotassiumOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN))
						dailyMeasurement = UreaIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT))
						dailyMeasurement = UreaOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN))
						dailyMeasurement = pHIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT))
						dailyMeasurement = pHOut;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}

			}
			else if(intent.getAction().equals(filter2)) {
				if(intent.hasExtra("ecgData")) {

					Bundle b = intent.getBundleExtra("ecgData");
					String heartRate = b.getString("heartRate");
					String respirationRate = b.getString("respirationRate");
					String activityLevel= b.getString("activityLevel");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.HEART_RATE))
						dailyMeasurement = heartRate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.RESPIRATION_RATE))
						dailyMeasurement = respirationRate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.ACTIVITY_LEVEL))
						dailyMeasurement = activityLevel;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}
			}
			else if(intent.getAction().equals(filter3)) {
				if(intent.hasExtra("physicalSensorData")) {

					Bundle b = intent.getBundleExtra("physicalSensorData");
					String pressureDialysate = b.getString("pressureDialysate");
					String pressureFCO = b.getString("pressureFCO");
					String pressureBloodline = b.getString("pressureBloodline");
					String pressureBCO = b.getString("pressureBCO");
					String temperatureBloodlineIn = b.getString("temperatureBloodlineIn");
					String temperatureBloodlineOut = b.getString("temperatureBloodlineOut");
					String conductivityFCR = b.getString("conductivityDialysate");
//					String conductivityFCQ = b.getString("conductivityFCQ");
					String CondTempDialysate = b.getString("CondTempDialysate");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE))
						dailyMeasurement = pressureDialysate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_FCO))
						dailyMeasurement = pressureFCO;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE))
						dailyMeasurement = pressureBloodline;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BCO))
						dailyMeasurement = pressureBCO;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN))
						dailyMeasurement = temperatureBloodlineIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT))
						dailyMeasurement = temperatureBloodlineOut;
//					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_FCQ))
//						dailyMeasurement = conductivityFCQ;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE))
						dailyMeasurement = conductivityFCR;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
						dailyMeasurement = CondTempDialysate;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}
			}
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}
