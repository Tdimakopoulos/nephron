package nephron.mobile.application;


import nephron.mobile.statistics.Statistics;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class WAKDPhysicalMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = { "Calcium In","Calcium Out", "Potassium In", "Potassium Out", "Urea In", "Urea Out", "pH In", "pH Out", "Temperature In", "Temperature Out"};
	private String userSelectedMeasurements[];
	SharedPreferences myPrefs;
	String prefName;
	Bundle b;
	Intent openMeasurements;
	Button backButton;
	ArrayAdapter<String> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackButtonMeasurements);
		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical"))
			openMeasurements = new Intent(WAKDPhysicalMeasurements.this, Statistics.class);
		else
			openMeasurements = new Intent(WAKDPhysicalMeasurements.this, MeasurementsTable.class);

		_measurementsListView = (ListView) findViewById(R.id.MeasurementsListView);

		userSelectedMeasurements = createMeasurementsArray();

		if(userSelectedMeasurements.length == 0) { 
			String[] noSelected = {"No Physical Measurement has been selected, you may change shown measurements in the Nephron+ Settings menu."};
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayoutnomeas, R.id.backtextview,	noSelected);
		}
		else
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayout, R.id.backtextview,	userSelectedMeasurements);
		
		_measurementsListView.setAdapter(adapter);

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// handle the various choices
				Toast.makeText(WAKDPhysicalMeasurements.this, adapter.getItem(position), Toast.LENGTH_SHORT).show();
				String item = adapter.getItem(position);
				if(item.equalsIgnoreCase("Calcium In")) {
					b = new Bundle();
					b.putInt("code", 911);
					b.putString("header1", "Daily Calcium In Trends");
					b.putString("header2", "Weekly Calcium In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Calcium Out")) {
					b = new Bundle();
					b.putInt("code", 912);
					b.putString("header1", "Daily Calcium Out Trends");
					b.putString("header2", "Weekly Calcium Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Potassium In")) {
					b = new Bundle();
					b.putInt("code", 921);
					b.putString("header1", "Daily Potassium In Trends");
					b.putString("header2", "Weekly Potassium In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Potassium Out")) {
					b = new Bundle();
					b.putInt("code", 922);
					b.putString("header1", "Daily Potassium Out Trends");
					b.putString("header2", "Weekly Potassium Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Urea In")) {
					b = new Bundle();
					b.putInt("code", 941);
					b.putString("header1", "Daily Urea In Trends");
					b.putString("header2", "Weekly Urea In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Urea Out")) {
					b = new Bundle();
					b.putInt("code", 942);
					b.putString("header1", "Daily Urea Out Trends");
					b.putString("header2", "Weekly Urea Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("pH In")) {
					b = new Bundle();
					b.putInt("code", 931);
					b.putString("header1", "Daily pH In Trends");
					b.putString("header2", "Weekly pH In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("pH Out")) {
					b = new Bundle();
					b.putInt("code", 932);
					b.putString("header1", "Daily pH Out Trends");
					b.putString("header2", "Weekly pH Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature In")) {
					b = new Bundle();
					b.putInt("code", 951);
					b.putString("header1", "Daily Temperature In Trends");
					b.putString("header2", "Weekly Temperature In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Out")) {
					b = new Bundle();
					b.putInt("code", 952);
					b.putString("header1", "Daily Temperature Out Trends");
					b.putString("header2", "Weekly Temperature Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else{
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage("Unidentified Selection has been made, something has gone wrong");
					_dialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss();
						}
					});
					_dialog.show();

				}
			}
		});



		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WAKDPhysicalMeasurements.this.finish();
			}
		});
	}


	private String[] createMeasurementsArray() {

		String physicalMeasPref = myPrefs.getString("physicalMeasSelected", "1111111111");

		int count = 0;
		for(int i =0; i < physicalMeasPref.length(); i++)
			if(physicalMeasPref.charAt(i) == '1')
				count++;

		String[] userMeasurements = new String[count];
		int index = 0;
		for(int i=0; i<physicalMeasPref.length(); i++) {
			if(physicalMeasPref.charAt(i) == '1'){
				userMeasurements[index] = _measurementsElementArray[i];
				index++;
			}
		}
		return userMeasurements;
	}

}
