package nephron.mobile.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class History extends Activity
{
	private ListView _historyListView;
	private static final String _historyElementArray[] = {"Alarms",
														  "Appointments",
														  "Meals",
														  "Activities"};
	
	Button backButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakstatuslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
			
		_historyListView = (ListView)findViewById(R.id.WakStatusListView);

		_historyListView.setAdapter(new ArrayAdapter<String>
		(this, R.layout.backgroundlayout, R.id.backtextview, _historyElementArray));
		
		backButton = (Button) findViewById(R.id.BackButton);
		
		// Event On Click
		_historyListView.setOnItemClickListener(new OnItemClickListener()
				{
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						// handle the various choices
				    	try
				    	{
				    		switch (position) 
				    		{
				    			case 0: // Show list of past Alarms
				    				Intent openAlarms = new Intent(History.this, Alarms.class);
				    				startActivity(openAlarms);
				    				break;
				    			
				    			case 1: // Show list of past Appointments
				    				Intent openAppointments = new Intent(History.this, Appointments.class);
				    				startActivity(openAppointments);
				    				break;
				    				
				    			case 2:	// Show list of past Meals
				    				Intent openMeals = new Intent(History.this, Meals.class);
				    				startActivity(openMeals);
				    				break;
				    				
				    			case 3:	// Show list of past Activities
 				    				Intent openActivities = new Intent(History.this, Activities.class);
				    				startActivity(openActivities);
				    				break;
				    			
				    			default:
				    				break;
				    		}
				    	}
				    	catch (Exception ex)
				    	{
				    		AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					    	_dialog.setTitle("Message");
				    		_dialog.setMessage(ex.getMessage());
				    		_dialog.setButton("OK", new DialogInterface.OnClickListener() 
				    										{   
				    											// Event of the OK button of the message box
										  						public void onClick(DialogInterface dialog, int which) 
										  						{																							
										  							dialog.dismiss(); // close dialog
										  						}
												  			});
				    		_dialog.show(); // show message box 
				    	}	
					}
				});
		
		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}
}
