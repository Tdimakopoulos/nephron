package nephron.mobile.application;

import java.util.concurrent.CopyOnWriteArrayList;

import nephron.mobile.application.alarms.AlarmsMsg;
import android.app.Application;
import de.imst.nephron.bt.BluetoothAbstractionLayer;

public class GlobalVar extends Application {

	private int messageCounter = 1;
	
	private BluetoothAbstractionLayer u;
	
	private boolean ConnectionServiceStarted = false;
	private boolean UserAuthenticated = false;
	private boolean BackendAuthenticated = false;
	private NephronApplicationStatusHandler applicationStatushandler;
	private boolean alarmWindowOpen;
	private boolean OperationInProgress = false;

	private CopyOnWriteArrayList<AlarmsMsg> newAlarmsList = new CopyOnWriteArrayList<AlarmsMsg>();
	private CopyOnWriteArrayList<AlarmsMsg> archivedAlarmsList = new CopyOnWriteArrayList<AlarmsMsg>();

	
	public int getMessageCounter() {
		return messageCounter++;
	}

	public BluetoothAbstractionLayer getBluetoothAbstractionLayer() {
		return u;
	}
	
	public void setBluetoothAbstractionLayer(BluetoothAbstractionLayer u) {
		this.u=u;
	}

	public boolean isConnectionServiceStarted() {
		return ConnectionServiceStarted;
	}

	public void setConnectionServiceStarted(boolean connectionServiceStarted) {
		ConnectionServiceStarted = connectionServiceStarted;
	}

	public boolean isUserAuthenticated() {
		return UserAuthenticated;
	}

	public void setUserAuthenticated(boolean userAuthenticated) {
		UserAuthenticated = userAuthenticated;
	}

	public boolean isBackendAuthenticated() {
		return BackendAuthenticated;
	}

	public void setBackendAuthenticated(boolean backendAuthenticated) {
		BackendAuthenticated = backendAuthenticated;
	}

	public NephronApplicationStatusHandler getApplicationStatushandler() {
		return applicationStatushandler;
	}

	public void setApplicationStatushandler(NephronApplicationStatusHandler applicationStatushandler) {
		this.applicationStatushandler = applicationStatushandler;
	}

	public boolean isAlarmWindowOpen() {
		return alarmWindowOpen;
	}

	public void setAlarmWindowOpen(boolean alarmWindowOpen) {
		this.alarmWindowOpen = alarmWindowOpen;
	}

	public CopyOnWriteArrayList<AlarmsMsg> getNewAlarmsList() {
		return newAlarmsList;
	}

	public void setNewAlarmsList(CopyOnWriteArrayList<AlarmsMsg> newAlarmsList) {
		this.newAlarmsList = newAlarmsList;
	}

	public CopyOnWriteArrayList<AlarmsMsg> getArchivedAlarmsList() {
		return archivedAlarmsList;
	}

	public void setArchivedAlarmsList(CopyOnWriteArrayList<AlarmsMsg> archivedAlarmsList) {
		this.archivedAlarmsList = archivedAlarmsList;
	}

	public boolean isOperationInProgress() {
		return OperationInProgress;
	}

	public void setOperationInProgress(boolean operationInProgress) {
		OperationInProgress = operationInProgress;
	}

}