package nephron.mobile.application;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import nephron.mobile.application.R.drawable;
import nephron.mobile.datafunctions.ByteUtils;
import nephron.mobile.datafunctions.MsgChangeStateCommand;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class WakControl extends Activity {
	Spinner opState;
	TextView currentState;
	List<String> stateOptions;
	AlertDialog message, passwordChecker, passwordInformer;
	AlertDialog.Builder confirmation,confirmationChangeState,confirmationShutDown,
	correctWeightData,incomingConfirmationChangeState, bpasswordInformer;
	Button changeOpMode, shutDown, cancel;
	Button getWeightScale;
	EditText weightInput,bodyFatInput;

	String currentWakdState = "", currentWakdOpState = "", storedPassword;
	int wakdStateTo, wakdOpStateTo, wakdStateIs=-1, wakdOpStateIs=-1;
	String selecectedStateTo;
	private String filter = "nephron.mobile.application.WakStatusEvent";
	private String filter3 = "nephron.mobile.application.ACK";
	private IncomingReceiver receiver;
	private ProgressDialog progressDialog;
	ArrayAdapter<String> dataAdapter1;
	int waitResponse, incomingID, MessageCounter;
	Double st1,st2;
	MessageConverter converter;
	protected Builder passwordBuilder;
	protected EditText mEditText;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakcontrollayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(filter);
		intentFilter.addAction(filter3);		
		registerReceiver(receiver, intentFilter);
		converter = new MessageConverter();
		currentState = (TextView) findViewById(R.id.currentState);
		opState = (Spinner) findViewById(R.id.changeTo);
		changeOpMode = (Button) findViewById(R.id.ButtonChangeOperationalMode);
		//		getWeightScale = (Button) findViewById(R.id.ButtonGetWeightScale);
		shutDown = (Button) findViewById(R.id.ButtonShutDown); 
		cancel = (Button) findViewById(R.id.BackControlButton); 


		// get correct view
		LayoutInflater li = LayoutInflater.from(this);
		View correctWeight = li.inflate(R.layout.correctweight, null);
		weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
		bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
		weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

		stateOptions = new ArrayList<String>();
		confirmation = new AlertDialog.Builder(this);  
		confirmationChangeState = new AlertDialog.Builder(this);
		correctWeightData = new AlertDialog.Builder(this);
		incomingConfirmationChangeState = new AlertDialog.Builder(this);
		correctWeightData.setView(correctWeight);
		message = new AlertDialog.Builder(this).create();
		st1= ConnectionService.db.getMeasurement(41);
		st2= ConnectionService.db.getMeasurement(42);

		if(st1!=null && st2!=null){
			wakdStateIs = st1.intValue();
			wakdOpStateIs = st2.intValue();
			setState(wakdStateIs, wakdOpStateIs);
		}


		dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateOptions);
		dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		opState.setAdapter(dataAdapter1);

		// if no state send Status Request command
		if((wakdStateIs == -1) && (wakdOpStateIs == -1) ){ 
			WakdCommandEnum command = WakdCommandEnum.STATUS_REQUEST;
			MsgSimple  statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
			try {
				ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
				Log.d(" send statusRequest "," send statusRequest cause unknown = " + converter.BytesToString(statusRequest.encode()));
			} catch (IOException e) {
				e.printStackTrace();
			}  
		}

		bpasswordInformer = new AlertDialog.Builder(WakControl.this);
		bpasswordInformer.setCancelable(true);
		bpasswordInformer.setTitle("Password Error");
		bpasswordInformer.setIcon(R.drawable.erroricon);
		bpasswordInformer.setNeutralButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(passwordInformer.isShowing()){
					passwordInformer.dismiss();
				}
			}
		});


		//	Button: Change Operation Mode	************************************
		changeOpMode.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(WakControl.this);
				View passwordView = LayoutInflater.from(WakControl.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();
					}
				});

				passwordBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								if(opState.getSelectedItem()!=null) { 
									selecectedStateTo = opState.getSelectedItem().toString();

									if (selecectedStateTo.equals("All Stopped")) {
										wakdStateTo = 1;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selecectedStateTo.equals("Maintenance")) {
										wakdStateTo = 2;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selecectedStateTo.equals("No Dialysate")) {
										wakdStateTo = 3;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selecectedStateTo.equals("Dialysis Auto")) {
										wakdStateTo = 4;
										wakdOpStateTo = 1;

									} else if (selecectedStateTo.equals("Regen1 Auto")) {
										wakdStateTo = 5;
										wakdOpStateTo = 1;

									} else if (selecectedStateTo.equals("Ultrafiltration Auto")) {
										wakdStateTo = 6;
										wakdOpStateTo = 1;

									} else if (selecectedStateTo.equals("Regen2 Auto")) {
										wakdStateTo = 7;
										wakdOpStateTo = 1;

									} else if (selecectedStateTo.equals("Dialysis Semi-Auto")) {
										wakdStateTo = 4;
										wakdOpStateTo = 2;

									} else if (selecectedStateTo.equals("Regen1 Semi-Auto")) {
										wakdStateTo = 5;
										wakdOpStateTo = 2;

									} else if (selecectedStateTo
											.equals("Ultrafiltration Semi-Auto")) {
										wakdStateTo = 6;
										wakdOpStateTo = 2;

									} else if (selecectedStateTo.equals("Regen2 Semi-Auto")) {
										wakdStateTo = 7;
										wakdOpStateTo = 2;

									}

									confirmationChangeState
									.setTitle("Change State")
									.setMessage(
											"From\n" + currentWakdState + " "
													+ currentWakdOpState + "\nTo\n"
													+ selecectedStateTo)
													.setPositiveButton("Yes", dialogClickListener)
													.setNegativeButton("No", null).show();
								}
								else {
									confirmationChangeState
									.setTitle("No state Selected")
									.setPositiveButton("Ok", null).show();
								}

							}
							else {
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}
					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}

		});


		//TODO 	REMOVE when tested that it works after its transfer to Weight.class
		//		REMOVE associated variables as well
		//	Button: Get Weight	************************************
		//		getWeightScale.setOnClickListener(new View.OnClickListener() {
		//			public void onClick(View view) {
		//
		//				confirmation.setMessage("Step up on weight scale and press OK" );
		//				confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		//					public void onClick(DialogInterface dialog,	int which) {
		//						progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");
		//
		//						new Thread(new Runnable() {
		//							public void run() {
		//								WakdCommandEnum command = WakdCommandEnum.WEIGHT_REQUEST;
		//								MsgSimple weightRequest;
		//								boolean ackArrived = false;
		//								MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
		//								for(int i=0;i<3;i++){
		//									Log.d(" Steilame "," Steilame me = " + MessageCounter);
		//									weightRequest = new MsgSimple(MessageCounter, command);
		//									Log.d(" Prospatheia Count "," Prospatheia Number = "+ i +" kai MessageCounter = " + MessageCounter + " incomingID = " + incomingID);
		//									Log.d(" send weightRequest "," send weightRequest = " + converter.BytesToString(weightRequest.encode()));
		//									try {
		//										// Thread.sleep(10000); //10secs
		//										ConnectionService.u.sendDatatoWAKD(weightRequest.encode());												 
		//										Thread.sleep(5 * 1000);
		//									} catch (IOException e) {
		//										e.printStackTrace();
		//									} catch (InterruptedException e) {
		//										e.printStackTrace();
		//									}
		//
		//
		//									if (incomingID == MessageCounter ) {
		//										ackArrived = true ;
		//										break;
		//									}										 
		//								}
		//
		//
		//								if(ackArrived) {
		//									if(progressDialog!=null){
		//										if (progressDialog.isShowing()) {
		//											progressDialog.dismiss();
		//											Looper.prepare();
		//											progressDialog = ProgressDialog.show(WakControl.this, "",
		//													"Your Command Arrived Successfully \n Please wait for Response to come...");
		//											new Thread(new Runnable() {
		//												public void run() {
		//													Log.d("sekinise to thread ","xeinise to thread");
		//													try {
		//														Thread.sleep(7 * 1000);
		//														if (progressDialog.isShowing()) {
		//															Log.d("mesa sto to thread ","mesas sto to thread");
		//															progressDialog.dismiss();
		//															Looper.prepare();
		//															message.setMessage("Nothing Happened");
		//															message.setButton("OK",
		//																	new DialogInterface.OnClickListener() {
		//																public void onClick(
		//																		DialogInterface dialog,
		//																		int which) {
		//																	// here you can add functions
		//																}
		//															});
		//															message.show();
		//															Looper.loop();
		//														}
		//													} catch (InterruptedException e) {
		//														e.printStackTrace();
		//													}
		//												}
		//											}).start();
		//											Looper.loop();
		//										}}
		//								}
		//								else{
		//									if(progressDialog!=null){
		//										if (progressDialog.isShowing()) {
		//											progressDialog.dismiss();
		//										}}
		//									Looper.prepare();
		//									message.setMessage("Your Command did not\narrive to WAKD");
		//									message.setButton("OK",
		//											new DialogInterface.OnClickListener() {
		//										public void onClick(
		//												DialogInterface dialog,
		//												int which) {
		//											// here you can add functions
		//										}
		//									});
		//									message.show();
		//									Looper.loop();
		//								}
		//							}
		//						}).start();
		//					}
		//				});
		//				confirmation.setNegativeButton("Cancel",null);
		//				confirmation.show();
		//			}
		//		});	




		//	Button: Shutdown WAKD	************************************
		shutDown.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(WakControl.this);
				View passwordView = LayoutInflater.from(WakControl.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new AlertDialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();
					}
				});


				passwordBuilder.setPositiveButton("OK", new AlertDialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.getText().toString() == null) {
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();


								/*
								 * 
								 */
								confirmation.setMessage("Do you want to Close the Device ?" );
								confirmation.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");

										new Thread(new Runnable() {
											public void run() {
												WakdCommandEnum command = WakdCommandEnum.SHUTDOWN;
												MsgSimple shutDown;
												boolean ackArrived = false;
												MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
												for(int i=0;i<3;i++){
													shutDown = new MsgSimple(MessageCounter, command);
													Log.d(" Prospatheia Count "," Prospatheia Number = "+ i +" kai MessageCounter = " + MessageCounter + " incomingID = " + incomingID);
													Log.d(" send shutDown "," send shutDown = " + converter.BytesToString(shutDown.encode()));
													try {
														ConnectionService.u.sendDatatoWAKD(shutDown.encode());												 
														Thread.sleep(5 * 1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													if (incomingID == MessageCounter ) {
														ackArrived = true ;
														break;
													}										 
												}

												if(progressDialog!=null){
													if (progressDialog.isShowing()) {
														progressDialog.dismiss();
													}
												}

												if(ackArrived){
													Looper.prepare();
													message.setMessage("Your Command Arrived Successfully");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
												else{
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											}
										}).start();
									}
								});
								confirmation.setNegativeButton("No",null);
								confirmation.show();
							}
							else {
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}
					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}
		});

		//	Button: Cancel, return from  WAKD Control Screen	************************************
		cancel.setOnClickListener(new OnClickListener() {
			//	        	@Override
			public void onClick(View v) { 
				finish();
			}

		});


	}


	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

		//		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				// send Ack
				waitResponse=1;

				Log.d("Efuge paketo me id", "wakdStateTo = " + wakdStateTo + " kai wakdOpStateTo = " + wakdOpStateTo);
				progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");
				new Thread(new Runnable() {
					public void run() {
						WakdCommandEnum command = WakdCommandEnum.CHANGE_STATE_FROM_TO;
						MsgChangeStateCommand changeState ; 
						boolean ackArrived = false;
						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
						for(int i=0;i<3;i++){
							changeState = new MsgChangeStateCommand(MessageCounter,
									command, wakdStateIs, wakdOpStateIs, wakdStateTo, wakdOpStateTo);

							Log.d(" Prospatheia Count "," Prospatheia Number = "+ i +" kai MessageCounter = " + MessageCounter + " incomingID = " + incomingID);

							Log.d(" send change State "," send change State = " + converter.BytesToString(changeState.encode()));
							try {
								ConnectionService.u.sendDatatoWAKD(changeState.encode());												 
								Thread.sleep(5 * 1000);
							} catch (IOException e) {
								e.printStackTrace();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							if (incomingID == MessageCounter ) {
								ackArrived = true ;
								break;
							}	

						}

						if(ackArrived){
							if(progressDialog!=null){
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
									Looper.prepare();
									progressDialog = ProgressDialog.show(WakControl.this, "",
											"Your Command Arrived Successfully\nPlease wait for Response to come.");
									new Thread(new Runnable() {
										public void run() {
											Log.d("sekinise to thread ","xeinise to thread");
											try {
												Thread.sleep(7 * 1000);
												if (progressDialog.isShowing()) {
													progressDialog.dismiss();
													Looper.prepare();
													message.setMessage("Nothing Happened");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
										}
									}).start();
									Looper.loop();
								}}	
						}
						else{
							if(progressDialog!=null){
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
								}}
							Looper.prepare();
							message.setMessage("Your Command did not\narrived to WAKD");
							message.setButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							message.show();
							Looper.loop();
						}
						
					}
				}).start();

				break;

			case DialogInterface.BUTTON_NEGATIVE:
				break;
			}
		}
	};


	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(filter)) {
				if (intent.hasExtra("currentWakdState")) {
					Bundle b = intent.getBundleExtra("currentWakdState");
					int newWakdStateIs = b.getInt("wakdStateIs");
					int newWakdOpStateIs = b.getInt("wakdOpStateIs");
					if(progressDialog!=null){
						if (progressDialog.isShowing()) {
							progressDialog.dismiss();
						}}  
					if(waitResponse==1){
						if (wakdStateTo == newWakdStateIs
								&& wakdOpStateTo == newWakdOpStateIs) {
							incomingConfirmationChangeState.setIcon(R.drawable.success);
							incomingConfirmationChangeState.setTitle("WAKD Response").setMessage(
									"Everything OK!\nCurrent State \n"
											+ wakdState(2, newWakdStateIs) + " "
											+ wakdState(1, newWakdOpStateIs));
							incomingConfirmationChangeState.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							stateOptions.clear();
							setState(newWakdStateIs, newWakdOpStateIs);

							dataAdapter1.notifyDataSetChanged();
						} else {
							incomingConfirmationChangeState.setIcon(R.drawable.warning);
							incomingConfirmationChangeState.setTitle("WAKD Response").setMessage(
									"Not OK!\nCurrent State \n"
											+ wakdState(2, newWakdStateIs) + " "
											+ wakdState(1, newWakdOpStateIs));
							incomingConfirmationChangeState.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							stateOptions.clear();

							setState(newWakdStateIs, newWakdOpStateIs);
							dataAdapter1.notifyDataSetChanged();
						}
						waitResponse=0;
						wakdStateIs = newWakdStateIs;
						wakdOpStateIs = newWakdOpStateIs;
						incomingConfirmationChangeState.show();
					}else{
						incomingConfirmationChangeState.setTitle("Current State").setMessage(
								"Current State info Arrived\nCurrent:"
										+ wakdState(2, newWakdStateIs) + " "
										+ wakdState(1, newWakdOpStateIs));
						incomingConfirmationChangeState.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// here you can add functions
							}
						});
						stateOptions.clear();
						setState(newWakdStateIs, newWakdOpStateIs);

						dataAdapter1.notifyDataSetChanged();
					}

					//incomingConfirmationChangeState.show();
				}
			}
			else if(intent.getAction().equals(filter3)) {
				Log.d("To iddddddd"," To id  ======== " + intent.getIntExtra("messageCount", 0));
				incomingID = intent.getIntExtra("messageCount", 0);

			}
		}

	}

	public void setState(int wakdState, int wakdOpState) {

		switch (wakdState) {
		case 1: //All Stopped
			stateOptions.add("Maintenance");
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 1);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 2: // Maintenance
			stateOptions.add("All Stopped");
			currentWakdState = wakdState(2, 2);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 3: // No Dialysate
			stateOptions.add("All Stopped");
			stateOptions.add("Dialysis Auto");
			stateOptions.add("Regen1 Auto");
			stateOptions.add("Ultrafiltration Auto");
			stateOptions.add("Regen2 Auto");
			stateOptions.add("Dialysis Semi-Auto");
			stateOptions.add("Regen1 Semi-Auto");
			stateOptions.add("Ultrafiltration Semi-Auto");
			stateOptions.add("Regen2 Semi-Auto");
			currentWakdState = wakdState(2, 3);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 4:  //Dialysis
			switch (wakdOpState) {
			case 1: // Auto
				stateOptions.add("Regen1 Auto");
				stateOptions.add("Ultrafiltration Auto");
				stateOptions.add("Regen2 Auto");
				break;
			case 2: // Semi-Auto
				stateOptions.add("Regen1 Semi-Auto");
				stateOptions.add("Ultrafiltration Semi-Auto");
				stateOptions.add("Regen2 Semi-Auto");
				break;
			}
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 4);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 5:  // Regen1
			switch (wakdOpState) {
			case 1: // Auto
				stateOptions.add("Ultrafiltration Auto");
				stateOptions.add("Regen2 Auto");
				stateOptions.add("Dialysis Auto");
				break;
			case 2: // Semi-Auto
				stateOptions.add("Ultrafiltration Semi-Auto");
				stateOptions.add("Regen2 Semi-Auto");
				stateOptions.add("Dialysis Semi-Auto");
				break;
			}
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 5);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 6:   //Ultrafiltration
			switch (wakdOpState) {
			case 1: // Auto
				stateOptions.add("Regen2 Auto");
				stateOptions.add("Dialysis Auto");
				stateOptions.add("Regen1 Auto");
				break;
			case 2: // Semi-Auto
				stateOptions.add("Regen2 Semi-Auto");
				stateOptions.add("Dialysis Semi-Auto");
				stateOptions.add("Regen1 Semi-Auto");
				break;
			}
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 6);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 7:  //Regen2
			switch (wakdOpState) {
			case 1: // Auto
				stateOptions.add("Dialysis Auto");
				stateOptions.add("Regen1 Auto");
				stateOptions.add("Ultrafiltration Auto");
				break;
			case 2: // Semi-Auto
				stateOptions.add("Dialysis Semi-Auto");
				stateOptions.add("Regen1 Semi-Auto");
				stateOptions.add("Ultrafiltration Semi-Auto");
				break;
			}
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 7);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		default:
			break;
		}

	}

	public  static String wakdState(int id, int st) {
		String state = "";
		switch (id) {
		case 1:
			switch (st) {
			case 1:
				state = "Auto";
				break;
			case 2:
				state = "Semi-Auto";
				break;
			}
			break;
		case 2:
			switch (st) {
			case 1:
				state = "All Stopped";
				break;
			case 2:
				state = "Maintenance";
				break;
			case 3:
				state = "No Dialysate";
				break;
			case 4:
				state = "Dialysis";
				break;
			case 5:
				state = "Regen1";
				break;
			case 6:
				state = "Ultrafiltration";
				break;
			case 7:
				state = "Regen2";
				break;
			}
			break;

		}
		return state;

	}

	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}
