package nephron.mobile.application;

import nephron.mobile.application.R.drawable;
import nephron.mobile.backend.NephronSOAPClient;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Questionnaire extends Activity
{

	Button saveQuestionnaireButton;
	Button cancelQuestionnaireButton;
	AlertDialog.Builder builder;

	RadioGroup dizzyGroup;
	RadioGroup nauseaGroup;
	RadioGroup headacheGroup;
	RadioGroup muscleGroup;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questionairelayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		builder = new Builder(Questionnaire.this);
		builder.setPositiveButton("OK", null);

		dizzyGroup = (RadioGroup) findViewById(R.id.DizzyQuestionGroup);
		nauseaGroup = (RadioGroup) findViewById(R.id.NauseaQuestionGroup);
		headacheGroup = (RadioGroup) findViewById(R.id.HeadacheQuestionGroup);
		muscleGroup = (RadioGroup) findViewById(R.id.MuscleCrampsQuestionGroup);

		cancelQuestionnaireButton = (Button) findViewById(R.id.CancelQuestionnairesImageButton);

		saveQuestionnaireButton = (Button) findViewById(R.id.SaveQuestionnairesImageButton);

		saveQuestionnaireButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int dizzyID = dizzyGroup.getCheckedRadioButtonId();
				int nauseaID = nauseaGroup.getCheckedRadioButtonId();
				int headacheID = headacheGroup.getCheckedRadioButtonId();
				int muscleID = dizzyGroup.getCheckedRadioButtonId();

				if(dizzyID == -1 || nauseaID == -1 || headacheID == -1 || muscleID == -1) {
					builder.setTitle("Error");
					builder.setIcon(drawable.erroricon);
					builder.setMessage("Please make sure that you have selected an answer for all questions");
					builder.create().show();
				}
				else {
					
					String response1;
					String response2;
					String response3;
					String response4;
					
					NephronSOAPClient client = new NephronSOAPClient(Questionnaire.this);
					
					StringBuilder sbuilder = new StringBuilder();

					if(((RadioButton) findViewById(dizzyID)).getText().toString().equalsIgnoreCase("Yes"))
						response1 = client.executeQuestionnaireRequest("1", "true");
					else
						response1 = client.executeQuestionnaireRequest("1", "false");

					if(((RadioButton) findViewById(nauseaID)).getText().toString().equalsIgnoreCase("Yes"))
						response2 = client.executeQuestionnaireRequest("2", "true");
					else
						response2 = client.executeQuestionnaireRequest("2", "false");

					if(((RadioButton) findViewById(headacheID)).getText().toString().equalsIgnoreCase("Yes"))
						response3 = client.executeQuestionnaireRequest("3", "true");
					else
						response3 = client.executeQuestionnaireRequest("3", "false");

					if(((RadioButton) findViewById(muscleID)).getText().toString().equalsIgnoreCase("Yes"))
						response4 = client.executeQuestionnaireRequest("4", "true");
					else
						response4 = client.executeQuestionnaireRequest("4", "false");

					if(response1!=null && response2!=null && response3!=null && response4!=null) {
						builder.setIcon(drawable.success);
						builder.setTitle("Success");
						builder.setMessage("Your answers have been submitted successfuly.");
						
						for(int i = 0; i < nauseaGroup.getChildCount(); i++){
							((RadioButton)nauseaGroup.getChildAt(i)).setEnabled(false);
						}
						
						for(int i = 0; i < dizzyGroup.getChildCount(); i++){
							((RadioButton)dizzyGroup.getChildAt(i)).setEnabled(false);
						}
						for(int i = 0; i < headacheGroup.getChildCount(); i++){
							((RadioButton)headacheGroup.getChildAt(i)).setEnabled(false);
						}
						
						for(int i = 0; i < muscleGroup.getChildCount(); i++){
							((RadioButton)muscleGroup.getChildAt(i)).setEnabled(false);
						}
						
					}
					else {
						builder.setIcon(drawable.erroricon);
						builder.setTitle("Failure");
						builder.setMessage("Some of your answers have not been submitted successfully, please try again");
					}
					builder.create().show();

				}

			}
		});

		cancelQuestionnaireButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}

}
