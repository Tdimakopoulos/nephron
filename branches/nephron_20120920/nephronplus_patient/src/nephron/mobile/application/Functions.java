package nephron.mobile.application;

import java.io.IOException;
import java.math.BigInteger;

import nephron.mobile.application.R.drawable;
import nephron.mobile.datafunctions.ByteUtils;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Functions extends Activity {

	public Functions() {
		super();
	}

	Button getWeightScaleButton, getBloodPressureButton, getECGButton, cancelButton;
	AlertDialog.Builder confirmation, correctWeightData, passwordBuilder, bpasswordInformer, userOperationInitDialog ;
	private ProgressDialog progressDialog;
	int MessageCounter, incomingID;
	MessageConverter weightConverter, ecgConverter, bpConverter;
	AlertDialog message, bpmessage;
	ECGXmlFileWriter ecgWriter;

	private String weightfilter = "nephron.mobile.application.PhysicalMeasurementEvent1";
	private String bpfilter = "nephron.mobile.application.PhysicalMeasurementEvent2";
	private String ecgfilter = "nephron.mobile.application.ECGStartStreamMeasurementEvent";
	private String ecgfilter2 = "nephron.mobile.application.ECGStopStreamMeasurementEvent";
	private String ackFilter = "nephron.mobile.application.ACK";
	private IncomingReceiver receiver;
	EditText weightInput,bodyFatInput, passwordInput;
	MessageConverter converter;
	View passwordView;

	EditText mEditText;
	Button posButton, negButton;
	Boolean authCheck=false;
	String storedPassword;

	AlertDialog passwordChecker, passwordInformer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.functionslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);


		getWeightScaleButton = (Button) findViewById(R.id.ButtonGetWeightScale);
		getBloodPressureButton = (Button) findViewById(R.id.GetBloodPressure);
		getECGButton = (Button) findViewById(R.id.GetECG);
		cancelButton = (Button) findViewById(R.id.BackFunctionsButton);
		confirmation = new AlertDialog.Builder(this);
		weightConverter = new MessageConverter();
		message = new AlertDialog.Builder(this).create();
		progressDialog = new ProgressDialog(getApplicationContext());

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ackFilter);
		intentFilter.addAction(weightfilter);
		intentFilter.addAction(bpfilter);
		intentFilter.addAction(ecgfilter);
		intentFilter.addAction(ecgfilter2);
		registerReceiver(receiver, intentFilter);

		// get correct view
		LayoutInflater li = LayoutInflater.from(this);
		View correctWeight = li.inflate(R.layout.correctweight, null);
		weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
		bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
		weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		correctWeightData = new AlertDialog.Builder(this);

		bpasswordInformer = new AlertDialog.Builder(Functions.this);
		bpasswordInformer.setCancelable(true);
		bpasswordInformer.setTitle("Password Error");
		bpasswordInformer.setIcon(R.drawable.erroricon);
		bpasswordInformer.setNeutralButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(passwordInformer.isShowing()){
					passwordInformer.dismiss();
				}
			}
		});

		//	Button: Get Weight	************************************
		getWeightScaleButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(Functions.this);
				View passwordView = LayoutInflater.from(Functions.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();

					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								confirmation.setMessage("Step up on weight scale and press OK" );
								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,	int which) {

										progressDialog = ProgressDialog.show(Functions.this, "", "Please wait...", false, true);

										new Thread(new Runnable() {
											public void run() {
												WakdCommandEnum command = WakdCommandEnum.WEIGHT_REQUEST;
												MsgSimple weightRequest;
												boolean ackArrived = false;
												MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
												for(int i=0;i<3;i++){
													weightRequest = new MsgSimple(MessageCounter, command);
													try {
														// Thread.sleep(10000); //10secs
														ConnectionService.u.sendDatatoWAKD(weightRequest.encode());
														Thread.sleep(5 * 1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													} catch(Exception e) {
														e.printStackTrace();
													}


													if (incomingID == MessageCounter ) {
														ackArrived = true ;
														break;
													}
												}


												if(ackArrived) {
													if(progressDialog!=null) {
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
															Looper.prepare();
															progressDialog = ProgressDialog.show(Functions.this, "",
																	"Your Command Arrived Successfully\nPlease wait for Response to come...");
															new Thread(new Runnable() {
																public void run() {
																	Log.d(Functions.class.getName(),"Response");
																	try {
																		Thread.sleep(7 * 1000);
																		if (progressDialog.isShowing()) {
																			//	Log.d("mesa sto to thread ","mesas sto to thread");
																			progressDialog.dismiss();
																			Looper.prepare();
																			message.setMessage("Nothing Happened");
																			message.setButton("OK",
																					new DialogInterface.OnClickListener() {
																				public void onClick(
																						DialogInterface dialog,
																						int which) {
																					// here you can add functions
																				}
																			});
																			message.show();
																			Looper.loop();
																		}
																	} catch (InterruptedException e) {
																		e.printStackTrace();
																	}
																}
															}).start();
															Looper.loop();
														}}	
												}
												else{
													if(progressDialog!=null){
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
														}}
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											}
										}).start();
									}
								});
								confirmation.setNegativeButton("Cancel",null);
								confirmation.show();
							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}

					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}
		});


		//	Button: Get Blood Pressure	****************************
		getBloodPressureButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				passwordBuilder = new AlertDialog.Builder(Functions.this);
				View passwordView = LayoutInflater.from(Functions.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();

					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								confirmation.setMessage("Please connect to your blood pressure machine. Press OK when ready.");
								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog, int which) {
										progressDialog = ProgressDialog.show(Functions.this, null,
												"Blood pressure acquisition in progress...", false, true);

										new Thread(new Runnable() {

											@Override
											public void run() {
												WakdCommandEnum bpCommand = WakdCommandEnum.BP_REQUEST;

												boolean ackArrived = false;
												MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
												for(int i=0;i<3;i++) {
													MsgSimple bloodPressureRequest = new MsgSimple(MessageCounter, bpCommand);
													Log.d("Blood Pressure Request Thread", "Sending request");

													try {
														ConnectionService.u.sendDatatoWAKD(bloodPressureRequest.encode());
														Thread.sleep(5*1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													if(incomingID == MessageCounter) {
														ackArrived = true;
														break;
													}

												}

												if(ackArrived) {
													Log.d("ACK","ACK response from MB arrived");
													if(progressDialog!=null) {
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
															Looper.prepare();
															progressDialog = ProgressDialog.show(Functions.this, "",
																	"Your command arrived successfully, please wait for response.");
															new Thread(new Runnable() {
																public void run() {
																	Log.d(Functions.class.getName(),"Response");
																	try {
																		Thread.sleep(7 * 1000);
																		if (progressDialog.isShowing()) {
																			//Log.d("mesa sto to thread ","mesas sto to thread");
																			progressDialog.dismiss();
																			Looper.prepare();
																			message.setMessage("Nothing Happened");
																			message.setButton("OK",
																					new DialogInterface.OnClickListener() {
																				public void onClick( DialogInterface dialog,
																						int which) {
																					// here you can add functions
																				}
																			});
																			message.show();
																			Looper.loop();
																		}
																	} catch (InterruptedException e) {
																		e.printStackTrace();
																	}
																}
															}).start();
															Looper.loop();
														}}

												}
												else{
													if(progressDialog!=null){
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
														}}
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
												// TODO Place code for handling the response from MB

											}
										}).start();
									}

								});
								confirmation.setNegativeButton("Cancel", null);
								AlertDialog alertDialog = confirmation.create();
								alertDialog.show();


							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}

						}

					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();

			}
		});



		getECGButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				passwordBuilder = new AlertDialog.Builder(Functions.this);
				View passwordView = LayoutInflater.from(Functions.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);
				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();

					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								confirmation.setMessage("Please connect to the ECG device. Press OK when ready.");

								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog, int which) {
										progressDialog.show(Functions.this, null, "ECG Measurement request is being sent.", false, true);

										new Thread(new Runnable() {

											@Override
											public void run() {
												boolean ackArrived = false;
												MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
												MsgSimple ecgRequest = new MsgSimple(MessageCounter, WakdCommandEnum.ECG_REQUEST);
												for(int i=0;i<3;i++) {
													try {
														ConnectionService.u.sendDatatoWAKD(ecgRequest.encode());
														Thread.sleep(5 * 1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													if (incomingID == MessageCounter ) {
														ackArrived = true ;
														break;
													}
												}

												if(ackArrived) {
													if(progressDialog!=null) {
														if(progressDialog.isShowing()) {
															progressDialog.dismiss();
															//TODO add steps when ACK has arrived
//															ecgWriter = new ECGXmlFileWriter();
															Looper.prepare();
															progressDialog = ProgressDialog.show(Functions.this, "",
																	"Waiting for data to arrive from MB", false, true);

															new Thread( new Runnable() {
																public void run() {

																}
															}).start();

														}
													}




												}
												else {
													//TODO add steps when no ACK has arrived after three attempts


												}

											}
										});

									}
								});

								confirmation.setNegativeButton("Cancel", null);
								AlertDialog alertDialog = confirmation.create();
								alertDialog.show();


							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password field, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}



					}
				});
				passwordChecker = passwordBuilder.create();
				passwordChecker.show();

			}
		});

		cancelButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Functions.this.finish();
			}
		});


	}


	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// Weight case
			if(intent.getAction().equals(weightfilter)) {
				final byte[] message  =  intent.getByteArrayExtra("message");

				message[0] = ByteUtils.intToByte(1);  
				message[3] = ByteUtils.intToByte(23);
				message[5] = ByteUtils.intToByte(4);

				byte[] Weight = new byte[] { message[6], message[7] };
				BigInteger bi = new BigInteger(Weight);
				String s = bi.toString(16);

				final double weight  = ((double) Integer.parseInt(s, 16)) / 10;
				final int bodyfat  = unsignedByteToInt(message[8]);
				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				confirmation.setTitle("Weight Info Arrived").setMessage(
						"Current \n Weight :  "
								+ weight + " Kg \n Body Fat :"
								+ bodyfat + " %");
				confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,	int which) {

						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
						Log.d(" Steilame "," Steilame me = " + MessageCounter);
						message[4] = ByteUtils.intToByte(MessageCounter) ;
						MessageConverter converter = new MessageConverter();
						Log.d(" send weight Ok "," send weight ok = " + converter.BytesToString(message));
						try {
							ConnectionService.u.sendDatatoWAKD(message);												 
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				confirmation.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int which) {
						weightInput.setText(Double.toString(weight));
						bodyFatInput.setText(Integer.toString(bodyfat));
						correctWeightData.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								Log.d("!!!!!!!!!!!"," weight = "+  (int) (Double.parseDouble(weightInput.getText().toString()) * 10) +" kai bodyFat " +  (int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
								System.arraycopy(ByteUtils.intToByteArray((int) (Double.parseDouble(weightInput.getText().toString()) * 10), 2), 0, message, 6, 2);
								message[8] = ByteUtils.intToByte((int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
								// WakdCommandEnum command = WakdCommandEnum.WEIGHT_DATA;
								// MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
								//MsgWeightDataOk   WeightData = new MsgWeightDataOk(MessageCounter,command, (int )weight*10,(int) bodyfat * 10, 900);
								Log.d(" correct weight "," correct weight  = " + converter.BytesToString(message));
								try {
									ConnectionService.u.sendDatatoWAKD(message);
								} catch (IOException e) { 
									e.printStackTrace();
								}
							}
						});

						correctWeightData.setNegativeButton("No",null);
						correctWeightData.show();
					}
				});
				confirmation.show();
			}

			// Blood Pressure case
			else if(intent.getAction().equals(bpfilter)) {
				Bundle bp_bundle = intent.getBundleExtra("blood_ressure");
				Double systolic_bp = bp_bundle.getDouble("systolic_bp");
				Double diastolic_bp = bp_bundle.getDouble("diastolic_bp");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				/*
				 *  TODO Place code to update appropriate text fields here
				 */

				bpmessage.setTitle("Blood Pressure Results");
				bpmessage.setMessage("Systolic : " +systolic_bp+
						"\nDiastolic : " + diastolic_bp);

				bpmessage.setButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage.show();

			}
			// ECG case
			else if(intent.getAction().equals(ecgfilter)) {
				if(progressDialog.isShowing()) {
					progressDialog.dismiss();
				}



			}
			// Case of ACK
			else if(intent.getAction().equals(ackFilter)) {
				incomingID = intent.getIntExtra("messageCount", 0);
				Log.d("incoming ID","IncomingID has been set ");
			}
		}
	}



	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}