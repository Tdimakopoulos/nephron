package nephron.mobile.application;

import java.util.ArrayList;
import java.util.List;

import nephron.mobile.application.alarms.AlarmsMsg;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class Alarms extends Activity 
{
	private ListView _measurementsListView;
	private MyAdapter adapter_listas;
	private List<AlarmsMsg> alarmsList;
	Cursor c;
	Button backButton;
	Handler handler;
	AlarmsReceiver receiver;
	private String alarmsFilter = "nephron.mobile.application.alarmsacked";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alarmslayout);

		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		receiver = new AlarmsReceiver();
		IntentFilter filter = new  IntentFilter(alarmsFilter);
		registerReceiver(receiver, filter);

		backButton = (Button) findViewById(R.id.BackAlarmsButton);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Alarms.this.finish();
			}
		});

		_measurementsListView = (ListView)findViewById(R.id.AlarmsListView);
		alarmsList = new ArrayList<AlarmsMsg>();
		adapter_listas = new MyAdapter(this);
		_measurementsListView.setAdapter(adapter_listas);

		c =   ConnectionService.db.getAllAlerts();

		while (c.moveToNext()) { 
			AlarmsMsg das = new AlarmsMsg(c.getInt(0),c.getInt(1),c.getString(2),c.getInt(3));
			alarmsList.add(das); 
		}
		c.close();

		ShowOrder((List<AlarmsMsg>)alarmsList);
		
		// TODO Test alarms with Maarten

	}

	public void ShowOrder(final List<AlarmsMsg> alarm) {
		this.runOnUiThread(new Runnable() {
			//			@Override
			public void run() {
				adapter_listas.addOrder(alarm);
				adapter_listas.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			//			@Override
			public void run() {
				adapter_listas.clear();
			}
		});
	}

	public class AlarmsReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals("nephron.mobile.application.alarmsacked")) {
				
				//option 1
//				Intent itemintent = new Intent(getApplicationContext(),Alarms.class);
//				((Activity) context).startActivityForResult(itemintent, 0);
//				((Activity) context).overridePendingTransition(0, 0);
//				((Activity) context).finish();
				
				//option 2
//				Alarms.this.adapter_listas.notifyDataSetChanged();
				
				ShowOrder((List<AlarmsMsg>)alarmsList);
			}
		}
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

}
