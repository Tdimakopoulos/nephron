package nephron.mobile.application;

import java.io.IOException;

import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class GetBloodPressure extends Activity {

	public GetBloodPressure() {
		super();
	}

	Button getWeightScaleButton, getBloodPressureButton, getECGButton, cancelButton;
	AlertDialog.Builder confirmation, correctWeightData, passwordBuilder, bpasswordInformer ;
	private ProgressDialog progressDialog;
	int MessageCounter, incomingID;
	MessageConverter weightConverter, ecgConverter, bpConverter;
	AlertDialog message, bpmessage;
	ECGXmlFileWriter ecgWriter;

	private String bpfilter = "nephron.mobile.application.PhysicalMeasurementEvent2";

	private String ackFilter = "nephron.mobile.application.ACK";
	private GetBPReceiver receiver;
	EditText weightInput,bodyFatInput, passwordInput;
	MessageConverter converter;
	View passwordView;

	EditText mEditText;
	Button posButton, negButton;
	Boolean authCheck=false;
	String storedPassword;

	AlertDialog passwordChecker, passwordInformer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.functionslayout);


		getBloodPressureButton = (Button) findViewById(R.id.GetBloodPressure);
		cancelButton = (Button) findViewById(R.id.BackFunctionsButton);
		confirmation = new AlertDialog.Builder(this);
		message = new AlertDialog.Builder(this).create();
		progressDialog = new ProgressDialog(getApplicationContext());

		receiver = new GetBPReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ackFilter);
		intentFilter.addAction(bpfilter);
		registerReceiver(receiver, intentFilter);

		//	Button: Get Blood Pressure	****************************
		
		confirmation.setMessage("Please connect to your blood pressure machine. Press OK when ready.");
		confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				progressDialog = ProgressDialog.show(GetBloodPressure.this, null,
						"Blood pressure acquisition in progress...", false, true);

				new Thread(new Runnable() {

					@Override
					public void run() {
						WakdCommandEnum bpCommand = WakdCommandEnum.BP_REQUEST;

						boolean ackArrived = false;
						MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
						for(int i=0;i<3;i++) {
							MsgSimple bloodPressureRequest = new MsgSimple(MessageCounter, bpCommand);
							Log.d("Blood Pressure Request Thread", "Sending request");

							try {
								ConnectionService.u.sendDatatoWAKD(bloodPressureRequest.encode());
								Thread.sleep(5*1000);
							} catch (IOException e) {
								e.printStackTrace();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							if(incomingID == MessageCounter) {
								ackArrived = true;
								break;
							}

						}

						if(ackArrived) {
							Log.d("ACK","ACK response from MB arrived");
							if(progressDialog!=null) {
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
									Looper.prepare();
									progressDialog = ProgressDialog.show(GetBloodPressure.this, "",
											"Your command arrived successfully, please wait for response.");
									new Thread(new Runnable() {
										public void run() {
											Log.d(GetBloodPressure.class.getName(),"Response");
											try {
												Thread.sleep(7 * 1000);
												if (progressDialog.isShowing()) {
													//Log.d("mesa sto to thread ","mesas sto to thread");
													progressDialog.dismiss();
													Looper.prepare();
													message.setMessage("Nothing Happened");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick( DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
										}
									}).start();
									Looper.loop();
								}}

						}
						else{
							if(progressDialog!=null){
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
								}}
							Looper.prepare();
							message.setMessage("Your Command did not\n arrive to WAKD");
							message.setButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							message.show();
							Looper.loop();
						}
						// TODO Place code for handling the response from MB

					}
				}).start();
			}

		});
		confirmation.setNegativeButton("Cancel", null);
		AlertDialog alertDialog = confirmation.create();
		alertDialog.show();


	}


	public class GetBPReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(bpfilter)) {
				Bundle bp_bundle = intent.getBundleExtra("blood_ressure");
				Double systolic_bp = bp_bundle.getDouble("systolic_bp");
				Double diastolic_bp = bp_bundle.getDouble("diastolic_bp");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				bpmessage.setTitle("Blood Pressure Results");
				bpmessage.setMessage("Systolic : " +systolic_bp+
						"\nDiastolic : " + diastolic_bp);

				bpmessage.setButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage.show();

			}
			// Case of ACK
			else if(intent.getAction().equals(ackFilter)) {
				incomingID = intent.getIntExtra("messageCount", 0);
				Log.d("incoming ID","IncomingID has been set ");
			}
		}
	}
	
	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

}