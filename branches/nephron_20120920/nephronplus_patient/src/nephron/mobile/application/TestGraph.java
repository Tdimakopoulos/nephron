package nephron.mobile.application;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TestGraph extends Activity {
	
	
	Button backButton;
	TextView DailyTrendTextView, AverageTrendTextView;
	int code;
	String head1, head2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.testgraphlayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackStatisticsButton);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				TestGraph.this.finish();
			}
		});
		
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);
		DailyTrendTextView.setText(head1);
		AverageTrendTextView.setText(head2);
		
		
//		LineGraphView graphView = new LineGraphView(TestGraph.this, "Daily "+ head1 +" trends");
		
		
	}


}
