package nephron.mobile.application;

import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class PhysicalMeasurements extends Activity {
	private ListView _wakStatusListView;
	private static final String _wakStatusElementArray[] = { "Blood Pressure Systolic: -",
		"Blood Pressure Diastolic: -",
	"Weight: -" };

	private String filter = "nephron.mobile.application.PhysicalMeasurementEvent";
	private String filter2 = "nephron.mobile.application.PhysicalMeasurementEvent2";

	private IncomingReceiver receiver;
	ArrayAdapter<String> dataAdapter;

	Button backButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakstatuslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackButton);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		registerReceiver(receiver, intentFilter);

		_wakStatusListView = (ListView) findViewById(R.id.WakStatusListView);
		dataAdapter=new ArrayAdapter<String>(this,
				R.layout.backgroundlayoutwithoutarows, R.id.backtextview,
				_wakStatusElementArray);
		_wakStatusListView.setAdapter(dataAdapter);

		Double pressure_systolic = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BLOODP_SYSTOLIC));
		Double pressure_diastolic = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BLOODP_DIASTOLIC));
		Double weight = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.WEIGHT));
		Double bodyfat = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BODY_FAT));

		if(pressure_systolic != null){
			_wakStatusElementArray[0] = "Blood Pressure Systolic: " + pressure_systolic + " �C";
		}
		if(pressure_diastolic != null){
			_wakStatusElementArray[1] = "Blood Pressure Diastolic: " + pressure_diastolic + " bpm";
		}
		if (weight != null) {
			_wakStatusElementArray[2] = "Weight: " + weight + " Kg";
		}

		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(filter)) {  
				if(intent.hasExtra("weight")) {   
					Bundle b =  intent.getBundleExtra("weight");
					double weight  = b.getDouble("weight");
					_wakStatusElementArray[2] = "Weight: " + weight + " Kg";
					dataAdapter.notifyDataSetChanged();
				}
			}
			else if(intent.getAction().equals(filter2)) {
				if(intent.hasExtra("blood_pressure")) {
					Bundle b = intent.getBundleExtra("blood_pressure");
					double bp_sys = b.getDouble("systolic_bp");
					double bp_dia = b.getDouble("diastolic_bp");
					_wakStatusElementArray[0] = "Blood Pressure Systolic: " + bp_sys + " bpm";
					_wakStatusElementArray[1] = "Blood Pressure Diastolic: " + bp_dia + " bpm";
					dataAdapter.notifyDataSetChanged();
				}
			}
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}