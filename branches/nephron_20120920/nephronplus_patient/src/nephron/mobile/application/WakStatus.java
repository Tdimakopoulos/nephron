package nephron.mobile.application;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class WakStatus extends Activity {
	private ListView _wakStatusListView;
	private static final String _wakStatusElementArray[] = { "Battery Status:",
			"Operational Status:", "WAKD Battery Level: -",
			"Weight Scale Battery Level: -",
			"Connectivity Status: Not Active",
			"Backend Connectivity Status:"};
	
	private String filter = "nephron.mobile.application.WakStatusEvent";
	private IncomingReceiver receiver;
	ArrayAdapter<String> dataAdapter;
	int wakdStateIs, wakdOpStateIs;
	Double st1,st2,weightScaleBatteryLevel;
	Button backButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakstatuslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		registerReceiver(receiver, intentFilter);
		
		backButton = (Button) findViewById(R.id.BackButton);

		_wakStatusListView = (ListView) findViewById(R.id.WakStatusListView);
		dataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundlayoutwithoutarows, R.id.backtextview,
				_wakStatusElementArray);
		_wakStatusListView.setAdapter(dataAdapter);

	   weightScaleBatteryLevel = ConnectionService.db.getMeasurement(183);
		
		if (weightScaleBatteryLevel != null) {
			_wakStatusElementArray[3] = "Weight Scale Battery Level: "
					+ weightScaleBatteryLevel + " %";
		}
		if (ConnectionService.connection == true) {
			_wakStatusElementArray[4] = "WAKD Connectivity Status: Active";
		}
		if (((GlobalVar)getApplicationContext()).isBackendAuthenticated()) {
			_wakStatusElementArray[5] = "Backend Connectivity Status: Active";
		}
		
		
		
		st1= ConnectionService.db.getMeasurement(41);
		st2= ConnectionService.db.getMeasurement(42);
 		if(st1!=null && st2!=null) {
 			wakdStateIs = st1.intValue();
 			wakdOpStateIs = st2.intValue();
 			_wakStatusElementArray[1] = "Operational Status: "	+ WakControl.wakdState(2, wakdStateIs) +" "+ WakControl.wakdState(1, wakdOpStateIs);
  		}
 		
 		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				finish();
			}
		});
 	}

	public class IncomingReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(filter)) {
				if (intent.hasExtra("WSBatteryLevel")) {
					int weightScaleBatteryStatus = intent.getExtras().getInt("WSBatteryLevel");
					_wakStatusElementArray[3] = "Weight Scale Battery Level: "
							+ weightScaleBatteryStatus + " %";
					dataAdapter.notifyDataSetChanged();
				} else if (intent.hasExtra("currentWakdState")) {
					Bundle b = intent.getBundleExtra("currentWakdState");
					int newWakdStateIs = b.getInt("wakdStateIs");
					int newWakdOpStateIs = b.getInt("wakdOpStateIs");
					_wakStatusElementArray[1] = "Operational Status: "
							+ WakControl.wakdState(2, newWakdStateIs)
							+ WakControl.wakdState(1, newWakdOpStateIs);
					dataAdapter.notifyDataSetChanged();
				} else if (intent.hasExtra("connection")) {
					_wakStatusElementArray[5] = "WAKD Connectivity Status: "
							+ intent.getExtras().getString("connection");
					dataAdapter.notifyDataSetChanged();
				} else if (intent.hasExtra("be_con_status")) {
					_wakStatusElementArray[6] = "Backend Connectivity Status: "
							+ intent.getExtras().getString("be_con_status");
					dataAdapter.notifyDataSetChanged();
				}
			}
		}

	}
	
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}