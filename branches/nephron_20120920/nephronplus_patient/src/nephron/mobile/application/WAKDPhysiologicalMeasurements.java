package nephron.mobile.application;


import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import nephron.mobile.statistics.Statistics;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class WAKDPhysiologicalMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = { "Conductivity Dialysate", "Vitamin C", "Pressure Dialysate",
		"Conductivity Temperature Dialysate", "Pressure Bloodline", "Temperature Bloodline Incoming",
	"Temperature Bloodline Outgoing"};
	SharedPreferences myPrefs;
	private String userSelectedMeasurements[];
	String prefName;
	Bundle b;
	Intent openMeasurements;
	Button backButton;
	ArrayAdapter<String> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		backButton = (Button) findViewById(R.id.BackButtonMeasurements);
		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical")) {
			openMeasurements = new Intent(WAKDPhysiologicalMeasurements.this, Statistics.class);
		} else {
			openMeasurements = new Intent(WAKDPhysiologicalMeasurements.this, MeasurementsTable.class);
		}

		_measurementsListView = (ListView) findViewById(R.id.MeasurementsListView);

		userSelectedMeasurements = createMeasurementsArray();

		if(userSelectedMeasurements.length == 0) { 
			String[] noSelected = {"No Physiological Measurement has been selected, you may change shown measurements in the Nephron+ Settings menu."};
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayoutnomeas, R.id.backtextview,	noSelected);
		}
		else
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayout, R.id.backtextview,	userSelectedMeasurements);

		_measurementsListView.setAdapter(adapter);

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// handle the various choices
				Toast.makeText(WAKDPhysiologicalMeasurements.this, adapter.getItem(position), Toast.LENGTH_SHORT).show();
				String item = adapter.getItem(position);
				if(item.equalsIgnoreCase("Conductivity Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE));
					b.putString("header1", "Daily Conductivity Dialysate Trends");
					b.putString("header2", "Weekly Conductivity Dialysate Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Vitamin C")) {
					b = new Bundle();
					b.putInt("code", 652);
					b.putString("header1", "Daily Vitamin C Trends");
					b.putString("header2", "Weekly Vitamin C Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Pressure Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE));
					b.putString("header1", "Daily Pressure Dialysate Trends");
					b.putString("header2", "Weekly Pressure Dialysate Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Conductivity Temperature Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE));
					b.putString("header1", "Daily Conductivity Temperature Dialysate Trends");
					b.putString("header2", "Weekly Conductivity Temperature Dialysate Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Pressure Bloodline")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE));
					b.putString("header1", "Daily Pressure Bloodline Trends");
					b.putString("header2", "Weekly Pressure Bloodline Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Bloodline Incoming")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN));
					b.putString("header1", "Daily Temperature Bloodline In Trends");
					b.putString("header2", "Weekly Temperature Bloodline In Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Bloodline Outgoing")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT));
					b.putString("header1", "Daily Temperature Bloodline Out Trends");
					b.putString("header2", "Weekly Temperature Bloodline Out Trends");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else{
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage("Unidentified Selection has been made, something has gone wrong");
					_dialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss();
						}
					});
					_dialog.show();
				}
				
			}
		});


		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WAKDPhysiologicalMeasurements.this.finish();
			}
		});

	}

	private String[] createMeasurementsArray() {

		String physicalMeasPref = myPrefs.getString("physioMeasSelected", "1111111");

		int count = 0;
		for(int i =0; i < physicalMeasPref.length(); i++)
			if(physicalMeasPref.charAt(i) == '1')
				count++;

		String[] userMeasurements = new String[count];
		int index = 0;
		for(int i=0; i<physicalMeasPref.length(); i++) {
			if(physicalMeasPref.charAt(i) == '1'){
				userMeasurements[index] = _measurementsElementArray[i];
				index++;
			}
		}
		return userMeasurements;
	}

}
