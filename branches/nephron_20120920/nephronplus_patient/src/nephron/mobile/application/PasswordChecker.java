package nephron.mobile.application;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordChecker extends Dialog {
	
	public EditText mEditText;
	public Button posButton, negButton;
	public Boolean authCheck=false;
	private String storedPassword;

	public PasswordChecker(final Context context) {
    	super(context);
    	View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.passwordchecker, null);
    	this.setContentView(dialogView);
    	this.mEditText = (EditText) this.findViewById(R.id.inputPassword);
//    	this.posButton = (Button) this.findViewById(R.id.PasswordCheckerOKButton);
//    	this.negButton = (Button) this.findViewById(R.id.PasswordCheckerCancelButton);
    	
    	posButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast toast = Toast.makeText(getContext(), null , Toast.LENGTH_SHORT);
				if(mEditText.getText().toString() == null) {
					authCheck = false;
					toast.setText("Empty Password Field, auth false");
					toast.show();
				}
				else {
					storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
					if(mEditText.getText().toString().equals(storedPassword)) {
						toast.setText("Non Empty Password Field, auth true");
						toast.show();
					}
					else
					{
						toast.setText("Non Empty Password Field, non matching, auth false");
						toast.show();
					}
				}
			PasswordChecker.this.dismiss();
			}
		});
    	
    	negButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				PasswordChecker.this.dismiss();
			}
		});
    }
	
	public Boolean getAuthCheck() {
		return authCheck;
	}

	public void setAuthCheck(Boolean authCheck) {
		this.authCheck = authCheck;
	}
	
	public EditText getmEditText() {
		return mEditText;
	}

	public void setmEditText(EditText mEditText) {
		this.mEditText = mEditText;
	}

	public Button getPosButton() {
		return posButton;
	}

	public void setPosButton(Button posButton) {
		this.posButton = posButton;
	}

	public Button getNegButton() {
		return negButton;
	}

	public void setNegButton(Button negButton) {
		this.negButton = negButton;
	}


}
