package nephron.demo.mobile.datafunctions;

	
public enum WAKDConfigCodeEnum {
		
		
		parameter_gender((byte) 1),					//  1: m/f
		parameter_measureTimeHours((byte) 2),			//  2: daily time for weight & bp measurement, hour
		parameter_measureTimeMinutes((byte) 3),		//  3: daily time for weight & bp measurement, minute

		parameter_wghtCtlTarget((byte) 4),			//  4: weight control target [same unit as weigth measurement on RTB]
		parameter_wghtCtlLastMeasurement((byte) 5),	//  5: weight control last measurement [same unit as weigth measurement on RTB]
		parameter_wghtCtlDefRemPerDay((byte) 6),		//  6: weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
		parameter_kCtlTarget((byte) 7),				//  7: potassium control target use FIXPOINTSHIFT for [mmol/l]
		parameter_kCtlLastMeasurement((byte) 8),		//  8: potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
		parameter_kCtlDefRemPerDay((byte) 9),			//  9: potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
		parameter_urCtlTarget((byte) 10),				//  a: urea control target use FIXPOINTSHIFT for [mmol/l]
		parameter_urCtlLastMeasurement((byte) 11),		//  b: urea control last measurement use FIXPOINTSHIFT for [mmol/l]
		parameter_urCtlDefRemPerDay((byte) 12),		//  c: urea default removal per day use FIXPOINTSHIFT for [mmol/day]
		parameter_minDialPlasFlow((byte) 13),			//  d: minimal flowrate for plasma in Dialysis mode  ml/min
		parameter_maxDialPlasFlow((byte) 14),			//  e: maximal flowrate for plasma in Dialysis mode  ml/min
		parameter_minDialVoltage((byte) 15),			//  f: minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
		parameter_maxDialVoltage((byte) 16),			// 10: maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT

		// To be configured for every state of "enumWakdStates"
		parameter_defSpeedBp_mlPmin((byte) 17),		// 11: setting speed blood pump [ml/min]
		parameter_defSpeedFp_mlPmin((byte) 18),		// 12: setting speed fluidic pump [ml/min]
		parameter_defPol_V((byte) 19),					// 13: setting voltage polarization [V]
		parameter_direction((byte) 20),				// 14: polarity of polarization voltage.
		parameter_duration_sec((byte) 21),				// 15: pre-setting for duration of "enumWakdStates"

		// WAKD parameters
//		parameter_NaAbsL((byte) 22),		    // 16: lower Na absolute threshold
//		parameter_NaAbsH((byte) 23),     		// 17: upper Na absolute threshold
//		parameter_NaTrdLT((byte) 24),			// 18: lower Na time period
//		parameter_NaTrdHT((byte) 25),			// 19: upper Na time period
//		parameter_NaTrdLPsT((byte) 26),		// 1a: lower Na trend per Specified Time threshold
//		parameter_NaTrdHPsT((byte) 27),		// 1b: upper Na trend per Specified Time threshold

		parameter_KAbsL((byte) 28),			// 1c: lower K absolute threshold
		parameter_KAbsH((byte) 29),			// 1d: upper K absolute threshold
		parameter_KAAL((byte) 30),				// 1e: lower K absolute alarm threshold
		parameter_KAAH((byte) 31),				// 1f: upper K absolute alarm threshold
		parameter_KTrdLT((byte) 32),			// 20: lower K trend low time-difference
		parameter_KTrdHT((byte) 33),			// 21: upper K trend low time-difference
		parameter_KTrdLPsT((byte) 34),			// 22: lower K trend per Specified Time threshold
		parameter_KTrdHPsT((byte) 35),			// 23: upper K trend per Specified Time threshold

		parameter_CaAbsL((byte) 36),			// 24: lower Ca absolute threshold
		parameter_CaAbsH((byte) 37),			// 25: upper Ca absolute threshold
		parameter_CaAAbsL((byte) 38),			// 26: lower K absolute alarm threshold
		parameter_CaAAbsH((byte) 39),			// 27: upper K absolute alarm threshold
		parameter_CaTrdLT((byte) 40),			// 28: lower Ca time period
		parameter_CaTrdHT((byte) 41),			// 29: upper Ca time period
		parameter_CaTrdLPsT((byte) 42),		// 2a: lower Ca trend per Specified Time threshold
		parameter_CaTrdHPsT((byte) 43),		// 2b: upper Ca trend per Specified Time threshold

//		parameter_UreaAAbsH((byte) 44),		// 2c: upper Urea absolute alarm threshold
//		parameter_UreaTrdHT((byte) 45),		// 2d: upper Urea time period
//		parameter_UreaTrdHPsT((byte) 46),		// 2e: upper Urea trend per Specified Time
//
//		parameter_CreaAbsL((byte) 47),			// 2f: lower Crea absolute threshold
//		parameter_CreaAbsH((byte) 48),			// 30: upper Crea absolute threshold
//		parameter_CreaTrdHT((byte) 49),		// 31: upper Crea time period
//		parameter_CreaTrdHPsT((byte) 50),		// 32: upper Crea trend per Specified Time
//
//		parameter_PhosAbsL((byte) 51),			// 33: lower Phos absolute threshold
//		parameter_PhosAbsH((byte) 52),			// 34: higher Phos absolute threshold
//		parameter_PhosAAbsH((byte) 53),		// 35: upper Phos absolute alarm threshold
//		parameter_PhosTrdLT((byte) 54),		// 36: lower Phos time period
//		parameter_PhosTrdHT((byte) 55),		// 37: upper Phos time period
//		parameter_PhosTrdLPsT((byte) 56),		// 38: lower Phos trend per Specified Time threshold
//		parameter_PhosTrdHPsT((byte) 57),		// 39: upper Phos trend per Specified Time
//
//		parameter_HCO3AAbsL((byte) 58),		// 3a: lower HCO3 absolute alarm threshold
//		parameter_HCO3AbsL((byte) 59),			// 3b: lower HCO3 absolute threshold
//		parameter_HCO3AbsH((byte) 60),			// 3c: upper HCO3 absolute threshold
//		parameter_HCO3TrdLT((byte) 61),		// 3d: lower HCO3 time period
//		parameter_HCO3TrdHT((byte) 62),		// 3e: upper HCO3 time period
//		parameter_HCO3TrdLPsT((byte) 63),		// 3f: lower HCO3 trend per Specified Time threshold
//		parameter_HCO3TrdHPsT((byte) 64),		// 40: upper HCO3 trend per Specified Time

		parameter_PhAAbsL((byte) 65),			// 41: lower pH absolute alarm threshold
		parameter_PhAAbsH((byte) 66),			// 42: upper pH absolute alarm threshold
		parameter_PhAbsL((byte) 67),			// 43: lower pH absolute threshold
		parameter_PhAbsH((byte) 68),			// 44: upper pH absolute threshold
		parameter_PhTrdLT((byte) 69),			// 45: lower pH time period
		parameter_PhTrdHT((byte) 70),			// 46: upper pH time period
		parameter_PhTrdLPsT((byte) 71),		// 47: lower pH trend per Specified Time threshold
		parameter_PhTrdHPsT((byte) 72),		// 48: upper pH trend per Specified Time threshold

		parameter_BPsysAbsL((byte) 73),		// 49: lower BPsys absolute threshold
		parameter_BPsysAbsH((byte) 74),		// 4a: upper BPsys absolute threshold
		parameter_BPsysAAbsH((byte) 75),		// 4b: upper BPsys absolute alarm threshold
		parameter_BPdiaAbsL((byte) 76),		// 4c: lower BPdia absolute threshold
		parameter_BPdiaAbsH((byte) 77),		// 4d: upper BPdia absolute threshold
		parameter_BPdiaAAbsH((byte) 78),		// 4e: upper BPdia absolute alarm threshold

		parameter_pumpFaccDevi((byte) 79),		// 4f: accepted flow deviation ml/min
		parameter_pumpBaccDevi((byte) 80),		// 50: accepted flow deviation ml/min

		parameter_VertDeflecitonT((byte) 81),	// 51: accepted vertical deflection threshold

		parameter_WghtAbsL((byte) 82),			// 52: lower Weight absolute threshold
		parameter_WghtAbsH((byte) 83),			// 53: upper Weight absolute threshold
		parameter_WghtTrdT((byte) 84),			// 54: Weight thrend threshold
		parameter_FDpTL((byte) 85),			// 55: Fluid Extraction Deviation Percentage Threshold Low
		parameter_FDpTH((byte) 86),			// 56: Fluid Extraction Deviation Percentage Threshold

		parameter_BPSoTH((byte) 87),			// 57: sensor 'BPSo' pressure upper threshold
		parameter_BPSoTL((byte) 88),			// 58: sensor 'BPSo' pressure lower threshold
		parameter_BPSiTH((byte) 89),			// 59: sensor 'BPSi' pressure upper threshold
		parameter_BPSiTL((byte) 90),			// 5a: sensor 'BPSi' pressure lower threshold
		parameter_FPS1TH((byte) 91),			// 5b: sensor 'FPS1' pressure upper threshold
		parameter_FPS1TL((byte) 92),			// 5c: sensor 'FPS1' pressure lower threshold
		parameter_FPS2TH((byte) 93),			// 5d: sensor 'FPS2' pressure upper threshold
		parameter_FPS2TL((byte) 94),			// 5e: sensor 'FPS2' pressure lower threshold

		parameter_BTSoTH((byte) 95),			// 5f: temperature sensor BTSo value upper threshold
		parameter_BTSoTL((byte) 96),			// 60: temperature sensor BTSo value lower threshold
		parameter_BTSiTH((byte) 97),			// 61: temperature sensor BTSi value upper threshold
		parameter_BTSiTL((byte) 98),			// 62: temperature sensor BTSi value lower threshold

		parameter_BatStatT((byte) 99);			// 63: battery status threshold

		// Expected adsorption rate calculation
		// parameters named according to Nanodialysis funciton of the sorbent unit
//		parameter_f_K((byte) 100),				// 64:
//		parameter_SCAP_K((byte) 101),			// 65:
//		parameter_P_K((byte) 102),				// 66:
//		parameter_Fref_K((byte) 103),			// 67:
//		parameter_cap_K((byte) 104),			// 68:
//		parameter_f_Ph((byte) 105),				// 69:
//		parameter_SCAP_Ph((byte) 106),			// 6a:
//		parameter_P_Ph((byte) 107),				// 6b:
//		parameter_Fref_Ph((byte) 108),			// 6c:
//		parameter_cap_Ph((byte) 109),			// 6d:
//		parameter_A_dm2((byte) 110),			// 6e:
//		parameter_CaAdr((byte) 111),			// 6f:
//		parameter_CreaAdr((byte) 112),			// 70:
//		parameter_HCO3Adr((byte) 113),			// 71:
//		parameter_wgtNa((byte) 114),			// 72: the fuzzy importance of the adsorption of this substance
//		parameter_wgtK((byte) 115),				// 73:
//		parameter_wgtCa((byte) 116),			// 74:
//		parameter_wgtUr((byte) 117),			// 75:
//		parameter_wgtCrea((byte) 118),			// 76:
//		parameter_wgtPhos((byte) 119),			// 77:
//		parameter_wgtHCO3((byte) 120),			// 78:
//		parameter_mspT((byte) 121);				// 79: set the threshold here!

		
		private final int code;

		WAKDConfigCodeEnum( int code ){
            this.code = code;
        }

        public int getCode() {
            return code;
        }
        
}