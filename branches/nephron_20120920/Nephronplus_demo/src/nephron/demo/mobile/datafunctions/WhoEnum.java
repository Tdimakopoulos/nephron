package nephron.demo.mobile.datafunctions;

public enum WhoEnum {
	UNDEFINED((byte)-1),		
	ND((byte)0),		// not defined, used whenever whoId is irrelevant (sender does not need Ack/Nack)
	MB((byte)1),		// Main Board
	RTB((byte)2),	// Real Time Board
	CB((byte)3),		// Communication Board
	SP((byte)4),		// Smart Phone
	PMB1((byte)5),	// Power Management Board 1
	PMB2((byte)6);	// Power management Board 2
	
	public static WhoEnum getWhoEnum(byte value) {
		switch (value) {
		case (byte)0:
			return ND;
		case (byte)1:
			return MB;
		case (byte)2:
			return RTB;
		case (byte)3:
			return CB;
		case (byte)4:
			return SP;
		case (byte)5:
			return PMB1;
		case (byte)6:
			return PMB2;
		default:
			return UNDEFINED;
		}
	}
	
	private byte value;

	private WhoEnum(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
}
