package nephron.demo.mobile.datafunctions;

import java.io.Serializable;


public abstract class WakdMsg implements Serializable {

	private static final long serialVersionUID = -4610547597128692152L;

	protected Header header;

	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}
	
	public abstract byte[] encode();
}
