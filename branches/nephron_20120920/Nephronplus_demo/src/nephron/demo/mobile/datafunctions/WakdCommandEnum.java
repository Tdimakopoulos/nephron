package nephron.demo.mobile.datafunctions;

public enum WakdCommandEnum {
	
	// Generic commands/data relevant for all Who.
	UNDEFINED((byte)-1),		//0	
	RESET((byte)0),			// 1 Recipient should reset itself no data attached (refer to strMessageOnly) 
	SET_TIME((byte)1),		// 3 Recipient should set Unix time according to attached data (refer to tdsetTime)
	SHUTDOWN ((byte)2),     // 2 Request to put system in safe state to turn of power in the end.
	CURRENT_STATE((byte)4),	// 4 Recipient is informed about the current state of WAKD
	ACK((byte)5), //5
	NACK((byte)6), //6
	STATUS_REQUEST((byte)7),	//7
	SYSTEM_INFO((byte)8),	//8
	// Commands/data relevant for RTB.
	// Configures actuator values for the states implemented in the RTB
	CONFIGURE_STATE((byte)9),					// 9 For RTB only: set WAKD actuators in state accroding to attached data (refer to tdActCtrl)
	CONFIGURE_PERIOD_POLARIZER_TOGGLE((byte)117),	//  (A) For RTB only: tdMsgPeriodPolarizerToggle
		
	// Command id that triggers to switch into another state. The RTB will automatically apply
	// the preconfigured actuator values (see above) whenever it changes into another state.
	CHANGE_STATE_FROM_TO((byte)11),		// (B) Request a state change from State A to B. (refer to tdWakdStateFromTo)
	
	// Command ids relevant for exchange of sensor data from RTB
	PHYSIOLOGICAL_DATA((byte)12),			// (C) Recipient gets all physiological data in one set. (refer to tdPhysiologicalData)
//	dataID_physiologicalData_K,			// D:
//	dataID_physiologicalData_Na,		// E:
//	dataID_physiologicalData_pH,		// F:
//	dataID_physiologicalData_Ur,		// 10:
	
	PHYSICAL_DATA((byte)10),				// (11) Recipient gets all physical data in one set. (refer to tdPhysicalData)

	//	dataID_actuatorData,				// 12:
	
	ALARM_RTB((byte)15),					// (13) Recipient is informed about alarm status from RTB. (refer to tdAlarms)
	
	// commands/data relevant for CB
	WEIGHT_REQUEST((byte)21),		// (15) MessageOnly command to ask CB to connect to weight scale and receive new value.
	WEIGHT_DATA((byte)22),		// (16) Recipient gets weight measurement. (refer to tdWeightData)
	WEIGHT_DATA_OK((byte)23),	//	17
	BP_REQUEST((byte)24),  		// (18)	SHOULD BE OIN ONE BLOCK (should be 24)
	BP_DATA((byte)25), /*151*/			//19	(should be 25)
	ECG_REQUEST((byte)26),/*16*/		//1A
	ECG_DATA((byte)27),		//1B
	
			//0x23
	
	ECGSTREAM_START((byte)36),	//0x24
	ECGSTREAM_STOP((byte)37),	//0x25
	
	
//	dataID_PatientProfile,			// 1C:
//	dataID_WAKDAllStateConfigure, 	// 1D: used for task FC, single state: dataID_configureState
//	dataID_StateParametersSCC,	 	// 1E: used for task SCC
//	dataID_AllStatesParametersSCC,	// 1F: used for task SCC
//	dataID_configureStateDump,		// 20: used to log the configutaion send out from FC to the RTB
//
//	dataID_CurrentParameters,		// 21: maintains IDs of parameters that current values WAKD should send to SP.
//	dataID_ActualParameters,		// 22: answer to dataID_CurrentParameters. Maintains name-value pairs of the parameters required.
	CONFIGURE_WAKD((byte)35),		// 23: Used by Smartphone (Exodus) to send new parameter data to WAKD (minboard, OFFIS)
	
	WS_ATTACH_REQUEST((byte)41),    // 29
	BP_ATTACH_REQUEST((byte)42),    // 2A
	ECG_ATTACH_REQUEST((byte)43),   // 2B
	              
	WS_ATTACHED((byte)44),          // 2C
	BP_ATTACHED((byte)45),          // 2D
	ECG_ATTACHED((byte)46),         // 2E
	                
	WS_NOT_ATTACHED((byte)47),      // 2F
	BP_NOT_ATTACHED((byte)48),      // 30
	ECG_NOT_ATTACHED((byte)49);     // 31

	
	
	public static WakdCommandEnum getWakdCommandEnum(byte value) {
		switch (value) {
		case (byte)0:
			return RESET;
		case (byte)1:
			return SET_TIME;    
		case (byte)2:
			return SHUTDOWN;
		case (byte)4:
			return CURRENT_STATE;
		case (byte)5:
			return ACK;
		case (byte)6:
			return NACK;
	 	case (byte)7:
		 	return STATUS_REQUEST;
	 	case (byte)8:
			return SYSTEM_INFO;
		case (byte)9:
			return CONFIGURE_STATE;
		case (byte)711:
			return CONFIGURE_PERIOD_POLARIZER_TOGGLE;
		case (byte)11:
			return CHANGE_STATE_FROM_TO;
		case (byte)12:
			return PHYSIOLOGICAL_DATA;
		case (byte)10:
			return PHYSICAL_DATA;
		case (byte)15:
			return ALARM_RTB;
		case (byte)21:
			return WEIGHT_REQUEST;
		case (byte)22:
			return WEIGHT_DATA;    
		case (byte)23:
			return WEIGHT_DATA_OK; 
		case (byte)14:
			return BP_REQUEST;
		case (byte)16:
			return ECG_REQUEST;
		case (byte)27:
			return ECG_DATA;
		case (byte)35:
			return CONFIGURE_WAKD;
		case (byte)36:
			return ECGSTREAM_START;
		case (byte)37:
			return ECGSTREAM_STOP;
		default:
			return UNDEFINED;
		}
	}
	
	private byte value;

	private WakdCommandEnum(byte value) {
		this.value = value;
	}

	public byte getValue() {
		return value;
	}

	public void setValue(byte value) {
		this.value = value;
	}
	
}
