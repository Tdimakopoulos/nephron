package nephron.demo.mobile.datafunctions;

public class MsgChangeStateCommand extends OutgoingWakdMsg {

	private static final long serialVersionUID = 8598583075931094670L;
	private int wakdStateFrom,wakdOpStateFrom,wakdStateTo,wakdOpStateTo;

	public MsgChangeStateCommand(int id, WakdCommandEnum command,int wakdStateFrom,int wakdOpStateFrom,int wakdStateTo,int wakdOpStateTo) {
		this.header.setId(id);
		this.header.setCommand(command);
		this.header.setSize(6);
		this.wakdStateFrom = wakdStateFrom;
		this.wakdOpStateFrom = wakdOpStateFrom;
		this.wakdStateTo = wakdStateTo;
		this.wakdOpStateTo = wakdOpStateTo;
	}

	@Override
	public byte[] encode() {
		
		byte[] bytes = new byte[10];
		bytes[0] = this.header.getRecipient().getValue();
		System.arraycopy(ByteUtils.intToByteArray(bytes.length, 2), 0, bytes, 1, 2);
		bytes[3] = this.header.getCommand().getValue();
		bytes[4] = ByteUtils.intToByte(this.header.getId());
		bytes[5] = this.header.getSender().getValue(); 
 		bytes[6] = ByteUtils.intToByte(this.wakdStateFrom);
		bytes[7] =  ByteUtils.intToByte(this.wakdOpStateFrom); 
		bytes[8] = ByteUtils.intToByte(this.wakdStateTo);
		bytes[9] =  ByteUtils.intToByte(this.wakdOpStateTo); 
		
 		return bytes;
	}

	
 
}