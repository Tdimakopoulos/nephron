package nephron.demo.mobile.statistics;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.R;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;

public class pHStatistics extends Activity
{	
	WebView dailyCreatinineWebView;
	WebView weeklyCreatinineWebView;
	ScrollView scrolview;
	String url = "file:///android_asset/dailycreatininegraph.html?";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{		
		// Show layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.creatininelayout);
		scrolview = (ScrollView)findViewById(R.id.scrollview);

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(181);
		while (c.moveToNext()) {
			url=url+"value="+c.getDouble(0)+"&"+"time="+c.getString(1)+"&";
		} 

		url= url.substring(0,url.length()-1);
		url= url.replaceAll("-", "/");
		url= url.replaceAll(" ", "%20");
		Log.d("!!PEIMENOUME DATABASE!!", url);
		c.close();
		// Load daily statistics
		dailyCreatinineWebView = (WebView)findViewById(R.id.DailyCreatinineWebView);
		dailyCreatinineWebView.getSettings().setJavaScriptEnabled(true);
		dailyCreatinineWebView.loadUrl(url);
		//dailyCreatinineWebView.loadUrl("file:///android_asset/exmlp.html?id=1&name=5");
		// Disable Scrolling by setting up an OnTouchListener to do nothing

		// Load weekly statistics
		weeklyCreatinineWebView = (WebView)findViewById(R.id.WeeklyCreatinineWebView);
		weeklyCreatinineWebView.getSettings().setJavaScriptEnabled(true);
		weeklyCreatinineWebView.loadUrl("file:///android_asset/weeklycreatininegraph.html");

		dailyCreatinineWebView.setOnTouchListener(new View.OnTouchListener() {
//			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				dailyCreatinineWebView.getParent().requestDisallowInterceptTouchEvent(true);
				//	scrolview.setEnabled(false);
				return false;
			}
		});    
	}
}