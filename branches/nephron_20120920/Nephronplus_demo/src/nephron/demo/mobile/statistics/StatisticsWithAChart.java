package nephron.demo.mobile.statistics;


import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.R;
import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.util.MathHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StatisticsWithAChart extends Activity {

	private XYMultipleSeriesDataset mDailyDataset = new XYMultipleSeriesDataset();
	private XYMultipleSeriesDataset mWeeklyDataset = new XYMultipleSeriesDataset();

	private XYMultipleSeriesRenderer mDailyMultiRenderer = new XYMultipleSeriesRenderer();
	private XYMultipleSeriesRenderer mWeeklyMultiRenderer = new XYMultipleSeriesRenderer();

	private XYSeriesRenderer mDailyRenderer = new XYSeriesRenderer();
	private XYSeriesRenderer mDailyUpperRenderer = new XYSeriesRenderer();
	private XYSeriesRenderer mDailyLowerRenderer = new XYSeriesRenderer();

	private XYSeriesRenderer mWeeklyRenderer = new XYSeriesRenderer();
	private XYSeriesRenderer mWeeklyUpperRenderer = new XYSeriesRenderer();
	private XYSeriesRenderer mWeeklyLowerRenderer = new XYSeriesRenderer();

	XYSeriesRenderer[] dailySimpleRenderers;

	XYSeriesRenderer[] weeklySimpleRenderers;

	private TimeSeries mDailyTimeSeries = new TimeSeries("Real Time");
	private TimeSeries mDailyUpperTimeSeries = new TimeSeries("Upper Limit");
	private TimeSeries mDailyLowerTimeSeries = new TimeSeries("Lower Limit");

	private TimeSeries mWeeklyTimeSeries = new TimeSeries("Daily");
	private TimeSeries mWeeklyLowerTimeSeries = new TimeSeries("Upper Limit");
	private TimeSeries mWeeklyUpperTimeSeries = new TimeSeries("Lower Limit");

	private Double lowerLimit;
	private Double upperLimit;
	private Boolean foundLimits = false;

	private GraphicalView mDailyChartView, mWeeklyChartView;

	LinearLayout dailyLayout, weeklyLayout;
	Format dailyFormatter, weeklyFormatter;

	private TextView DailyTrendTextView, AverageTrendTextView;
	private int code;
	private String head1, head2, title;
	Button backButton;

	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.statisticswithgraphlayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		

		handler = new Handler();

		dailyFormatter = new SimpleDateFormat("HH:mm");
		weeklyFormatter = new SimpleDateFormat("dd/MM");

		initializeHeader();

		backButton = (Button) findViewById(R.id.BackButton);
		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StatisticsWithAChart.this.finish();
			}
		});

		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
			title = b.getString("title");
		}
		
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText(title);

		receiver = new ConnectivityHeaderReceiver();
		IntentFilter filter = new IntentFilter("GraphEvent");
		filter.addAction("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);
		DailyTrendTextView.setText(head1);
		AverageTrendTextView.setText(head2);


		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);
		DailyTrendTextView.setText(head1);
		AverageTrendTextView.setText(head2);

		Display display = getWindowManager().getDefaultDisplay();
		int screenWidth = display.getWidth();
		LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(screenWidth, screenWidth);
		linearLayoutParams.setMargins(0, 10, 0, 10);

		dailyLayout = (LinearLayout) findViewById(R.id.dailyGraphLayout);
		dailyLayout.setLayoutParams(linearLayoutParams);

		weeklyLayout = (LinearLayout) findViewById(R.id.weeklyGraphLayout);
		weeklyLayout.setLayoutParams(linearLayoutParams);
		/*	Finished with layout	*/


		//		Steps to follow
		//		1)	Create the values for the graphs, into two arraylists

		foundLimits = findLimitsforGraph(code);

		createDataForGraph(true);
		createDataForGraph(false);

		createSimpleRenderers(true);
		createSimpleRenderers(false);

		//		4) prepare the daily multi renderer


		mDailyMultiRenderer = prepareMultiRenderer(true);

		//	This is where the min max Values for X and Y axes are created and calculateds 
		setMinMaxYValueForRenderer(true, mDailyMultiRenderer);

		setMinMaxXValueForRenderer(true, mDailyMultiRenderer);

		prepareLimitSeries(true);

		prepareListOfRenderers(true);

		addRenderersToMultiRenderer(mDailyMultiRenderer, dailySimpleRenderers);

		//		5) prepare the weekly multi renderer

		mWeeklyMultiRenderer = prepareMultiRenderer(false);

		setMinMaxYValueForRenderer(false, mWeeklyMultiRenderer);

		setMinMaxXValueForRenderer(false, mWeeklyMultiRenderer);

		prepareLimitSeries(false);

		prepareListOfRenderers(false);

		addRenderersToMultiRenderer(mWeeklyMultiRenderer, weeklySimpleRenderers);

		addSeriesToDataset(mDailyDataset, true);

		addSeriesToDataset(mWeeklyDataset, false);

	}


	@Override
	protected void onStart() {
		super.onStart();

		mWeeklyChartView = ChartFactory.getTimeChartView(this, mWeeklyDataset, mWeeklyMultiRenderer, "dd/MM");
		weeklyLayout.addView(mWeeklyChartView);

		mDailyChartView = ChartFactory.getTimeChartView(this, mDailyDataset, mDailyMultiRenderer, "HH:mm");
		dailyLayout.addView(mDailyChartView);

	}


	private void prepareLimitSeries(Boolean daily) {

		if(foundLimits) {
			
			Log.e("LIMITS", "FOUND");

			if(daily) {

//				mDailyUpperTimeSeries.add(mDailyTimeSeries.getMinX(), upperLimit);
//				mDailyUpperTimeSeries.add(mDailyTimeSeries.getMaxX(), upperLimit);
//
//				mDailyLowerTimeSeries.add(mDailyTimeSeries.getMinX(), lowerLimit);
//				mDailyLowerTimeSeries.add(mDailyTimeSeries.getMaxX(), lowerLimit);
				
				
				if(mDailyTimeSeries.getItemCount() == 1) {
					mDailyUpperTimeSeries.add(mDailyMultiRenderer.getXAxisMin(), upperLimit);
					mDailyUpperTimeSeries.add(mDailyMultiRenderer.getXAxisMax(), upperLimit);

					mDailyLowerTimeSeries.add(mDailyMultiRenderer.getXAxisMin(), lowerLimit);
					mDailyLowerTimeSeries.add(mDailyMultiRenderer.getXAxisMax(), lowerLimit);	
				}
				if(mDailyTimeSeries.getItemCount() == 2) {
					mDailyUpperTimeSeries.add(mDailyTimeSeries.getMinX(), upperLimit);
					mDailyUpperTimeSeries.add(mDailyTimeSeries.getMaxX(), upperLimit);

					mDailyLowerTimeSeries.add(mDailyTimeSeries.getMinX(), lowerLimit);
					mDailyLowerTimeSeries.add(mDailyTimeSeries.getMaxX(), lowerLimit);	
				}
				else {
					
					mDailyUpperTimeSeries.add(mDailyTimeSeries.getMinX(), upperLimit);
					mDailyUpperTimeSeries.add(mDailyTimeSeries.getMaxX(), upperLimit);

					mDailyLowerTimeSeries.add(mDailyTimeSeries.getMinX(), lowerLimit);
					mDailyLowerTimeSeries.add(mDailyTimeSeries.getMaxX(), lowerLimit);			
				}
				
				
			}
			else {
				
				if(mWeeklyTimeSeries.getItemCount() == 1) {
					mWeeklyUpperTimeSeries.add(mWeeklyMultiRenderer.getXAxisMin(), upperLimit);
					mWeeklyUpperTimeSeries.add(mWeeklyMultiRenderer.getXAxisMax(), upperLimit);

					mWeeklyLowerTimeSeries.add(mWeeklyMultiRenderer.getXAxisMin(), lowerLimit);
					mWeeklyLowerTimeSeries.add(mWeeklyMultiRenderer.getXAxisMax(), lowerLimit);	
				}
				if(mWeeklyTimeSeries.getItemCount() == 2) {
					mWeeklyUpperTimeSeries.add(mWeeklyMultiRenderer.getXAxisMin(), upperLimit);
					mWeeklyUpperTimeSeries.add(mWeeklyMultiRenderer.getXAxisMax(), upperLimit);

					mWeeklyLowerTimeSeries.add(mWeeklyMultiRenderer.getXAxisMin(), lowerLimit);
					mWeeklyLowerTimeSeries.add(mWeeklyMultiRenderer.getXAxisMax(), lowerLimit);	
				}
				else {
					
					mWeeklyUpperTimeSeries.add(mWeeklyTimeSeries.getMinX(), upperLimit);
					mWeeklyUpperTimeSeries.add(mWeeklyTimeSeries.getMaxX(), upperLimit);

					mWeeklyLowerTimeSeries.add(mWeeklyTimeSeries.getMinX(), lowerLimit);
					mWeeklyLowerTimeSeries.add(mWeeklyTimeSeries.getMaxX(), lowerLimit);			
				}
			}
		}
		else Log.e("LIMITS", "NOT FOUND");
			
	}


	private void addSeriesToDataset(XYMultipleSeriesDataset dataset, Boolean daily) {

		if(daily) {
			dataset.addSeries(mDailyTimeSeries);
			if(foundLimits) {
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {
					dataset.addSeries(mDailyUpperTimeSeries);
					dataset.addSeries(mDailyLowerTimeSeries);	
				}
				else
					dataset.addSeries(mDailyUpperTimeSeries);
			}		

		}
		else {
			dataset.addSeries(mWeeklyTimeSeries);
			if(foundLimits) {
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {

					dataset.addSeries(mWeeklyUpperTimeSeries);
					dataset.addSeries(mWeeklyLowerTimeSeries);
				}
				else
					dataset.addSeries(mWeeklyUpperTimeSeries);
			}


		}
	}


	private boolean findLimitsforGraph(int code) {

		Integer[] configCodes = decodeCodeToLimits(code);

		if(configCodes != null) {

			if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {

				Integer upperLimitID = configCodes[1];
				if(configCodes[1] == null) 
					return false;
				else {
					upperLimit = ConnectionService.db.getWAKDConfigParameter(upperLimitID);
					if(upperLimit == null)
						return false;
					else
						return true;
				}
			}
			else {
				Integer lowerLimitID = configCodes[0];
				Integer upperLimitID = configCodes[1];

				lowerLimit = ConnectionService.db.getWAKDConfigParameter(lowerLimitID);
				upperLimit = ConnectionService.db.getWAKDConfigParameter(upperLimitID);
				if(lowerLimit == null)
					return false;
				if(upperLimit == null)
					return false;

				Log.e("LIMITS FOR GRAPHS",lowerLimit+"      "+upperLimit);
				return true;
			}
		}
		else
			return false;
	}


	private void createSimpleRenderers(Boolean daily) {


		if(daily) {
			//	2) Create the simple renderers for dailys
			mDailyRenderer = prepareSimpleRenderer(StatisticsWithAChart.this.getResources().getColor(R.color.nephrongreen), 4, PointStyle.CIRCLE);
			if(foundLimits){
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {
					mDailyUpperRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
					mDailyLowerRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
				}
				else
					mDailyUpperRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
			}
		}
		else {

			//	3) Create the simple renderers for weekly

			mWeeklyRenderer = prepareSimpleRenderer(StatisticsWithAChart.this.getResources().getColor(R.color.nephrongreen), 4, PointStyle.CIRCLE);
			if(foundLimits){
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {

					mWeeklyUpperRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
					mWeeklyLowerRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
				}
				else {
					mWeeklyUpperRenderer = prepareSimpleRenderer(Color.RED, 4, PointStyle.POINT);
				}
			}
		}
	}

	private void prepareListOfRenderers(Boolean daily) {
		
		if(daily) {

			if(foundLimits){
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT))
					dailySimpleRenderers = new XYSeriesRenderer[]{mDailyRenderer, mDailyLowerRenderer, mDailyUpperRenderer};
				else
					dailySimpleRenderers = new XYSeriesRenderer[]{mDailyRenderer, mDailyUpperRenderer};
			}
			else
				dailySimpleRenderers = new XYSeriesRenderer[]{mDailyRenderer};

		}
		else {

			if(foundLimits) {
				if(code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN) || 
						code != MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT)) {
					weeklySimpleRenderers = new XYSeriesRenderer[]{mWeeklyRenderer, mWeeklyLowerRenderer, mWeeklyUpperRenderer};
				}
				else
					weeklySimpleRenderers = new XYSeriesRenderer[]{mWeeklyRenderer, mWeeklyUpperRenderer};
			}
			else
				weeklySimpleRenderers = new XYSeriesRenderer[]{mWeeklyRenderer};
		}
	}


	//FIXME Check if the absolute normal or absolute alarm threshold should be used
	private Integer[] decodeCodeToLimits(int code) {
		if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN))
			return new Integer[]{36,37};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT))
			return new Integer[]{36,37};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN))
			return new Integer[]{28,29};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT))
			return new Integer[]{28,29};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN))
			return new Integer[]{44,null};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT))
			return new Integer[]{44,null};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN))
			return new Integer[]{67,68};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT))
			return new Integer[]{67,68};

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_IN))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_OUT))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN))
			return null;

		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT))
			return null;

		else
			return  null;
	}


	private void createDataForGraph(boolean dailyMeasurements) {

		Cursor c = null;
		SimpleDateFormat format;
		if(dailyMeasurements) {
			c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
			format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
		else {
			c = ConnectionService.db.getWeeklyMeasurements(code);
			format = new SimpleDateFormat("dd/MM");
		}

		if(dailyMeasurements) {
			mDailyTimeSeries.clear();
			c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
			while (c.moveToNext()) {
				String dateString = c.getString(1);
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Double value = c.getDouble(0);
				mDailyTimeSeries.add(date, value);
				//FIXME Remove from heres
				//				if(foundLimits) {
				//					mDailyUpperTimeSeries.add(date, upperLimit);
				//					mDailyLowerTimeSeries.add(date, lowerLimit);	
				//				}

			}
			c.close();
			if(mDailyTimeSeries.getItemCount() == 0) {

				mDailyTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
				//				mDailyUpperTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
				//				mDailyLowerTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
			}
		}
		else {
			mWeeklyTimeSeries.clear();
			c = ConnectionService.db.getWeeklyMeasurements(code);
			while (c.moveToNext()) {
				String dateString = c.getString(1);
				Date date = null;
				try {
					date = format.parse(dateString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Double value = c.getDouble(0);
				mWeeklyTimeSeries.add(date, value);
				//				if(foundLimits) {
				//					mWeeklyUpperTimeSeries.add(date, upperLimit);
				//					mWeeklyLowerTimeSeries.add(date, lowerLimit);
				//				}

				//				mWeeklyUpperTimeSeries.add(date, 8);
				//				mWeeklyLowerTimeSeries.add(date, 2);
			}
			c.close();
			if(mWeeklyTimeSeries.getItemCount() == 0) {

				mWeeklyTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
				//				mWeeklyUpperTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
				//				mWeeklyLowerTimeSeries.add(MathHelper.NULL_VALUE, MathHelper.NULL_VALUE);
			}
		}

		Log.e("DAILY TIME SERIES ITEM COUNT", String.valueOf(mDailyTimeSeries.getItemCount()));
		Log.e("WEEKLY TIME SERIES ITEM COUNT", String.valueOf(mWeeklyTimeSeries.getItemCount()));
	}


	private void addRenderersToMultiRenderer(XYMultipleSeriesRenderer renderer, XYSeriesRenderer[] simpleRenderers) {
		if(simpleRenderers!=null){
			if(simpleRenderers.length>0) {
				for(int i=0; i<simpleRenderers.length; i++ ){
					if(simpleRenderers[i]!=null)
						renderer.addSeriesRenderer(simpleRenderers[i]);
				}	
			}
		}
	}


	private XYMultipleSeriesRenderer prepareMultiRenderer(Boolean daily) {

		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.WHITE);
		renderer.setGridColor(Color.BLACK);
		renderer.setAxesColor(Color.BLACK);
		renderer.setMarginsColor(Color.WHITE);
		renderer.setLabelsColor(Color.BLACK);
		renderer.setXLabelsColor(Color.BLACK);
		renderer.setYLabelsColor(0, Color.BLACK);
		renderer.setAxisTitleTextSize(10);
		renderer.setLabelsTextSize(13);
		renderer.setPanEnabled(true, false);
		renderer.setMargins(decodeCodeToMultirendererMargins(code));
		renderer.setLegendHeight(50);
		renderer.setInScroll(true);
		renderer.setShowGrid(true);
		renderer.setGridColor(Color.LTGRAY);
		renderer.setYLabelsAlign(Align.RIGHT);
		return renderer;
		
	}
	
	
	private int[] decodeCodeToMultirendererMargins(int code) {
		if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN))
			return new int[] { 20, 25, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT))
			return new int[] { 20, 25, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN))
			return new int[] { 20, 45, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN))
			return new int[] { 20, 25, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT))
			return new int[] { 20, 25, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_IN))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_OUT))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE))
			return new int[] { 20, 40, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C))
			return new int[] { 20, 45, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE))
			return new int[] { 20, 40, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE))
			return new int[] { 20, 45, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN))
			return new int[] { 20, 30, 0, 10 };
		else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT))
			return new int[] { 20, 30, 0, 10 };
		else
			return new int[] { 20, 25, 0, 10 };
	}
	
	
	
	

	private void setMinMaxYValueForRenderer(Boolean daily, XYMultipleSeriesRenderer renderer) {
		//Set the min , max for Y Axis
		renderer.setYAxisMin(calculateMinMaxForY(daily, true));
		renderer.setYAxisMax(calculateMinMaxForY(daily, false));
	}


	private void setMinMaxXValueForRenderer(Boolean daily, XYMultipleSeriesRenderer renderer) {

		//Set the minimum x for the daily graph
		if(daily){
			//Case when only one value present for the daily
			if(mDailyTimeSeries.getItemCount() == 1) {
				if(mDailyTimeSeries.getX(0) != MathHelper.NULL_VALUE) {
					renderer.setXAxisMax(mDailyTimeSeries.getMaxX() + (2*60*1000));
					renderer.setXAxisMin(mDailyTimeSeries.getMinX() - (2*60*1000));
				}
			}
			//Case when two values present for the daily
			else if(mDailyTimeSeries.getItemCount() == 2 ){
				renderer.setXAxisMax(mDailyTimeSeries.getMaxX());
				renderer.setXAxisMin(mDailyTimeSeries.getMinX());

			}
			//Case when more than two values present for the daily
			else if(mDailyTimeSeries.getItemCount() > 2 ){
				// if min and max have a distance of at least two hours
				if(mDailyTimeSeries.getMaxX()-(1200000)>mDailyTimeSeries.getMinX()) {
					renderer.setXAxisMin(mDailyTimeSeries.getMaxX()-(1200000));
					renderer.setXAxisMax(mDailyTimeSeries.getMaxX());	
				}
				// if min and max have a distance of less than two hours
				else{
					renderer.setXAxisMin(mDailyTimeSeries.getMinX());
					renderer.setXAxisMax(mDailyTimeSeries.getMaxX());
				}
			}
		}
		else {
			if(mWeeklyTimeSeries.getItemCount() == 1 ){
				if(mWeeklyTimeSeries.getX(0) != MathHelper.NULL_VALUE) {
					renderer.setXAxisMax(mWeeklyTimeSeries.getMaxX()+(24*60*60*1000));
					renderer.setXAxisMin(mWeeklyTimeSeries.getMinX()-(24*60*60*1000));
				}
			}
			else if(mWeeklyTimeSeries.getItemCount() == 2 ){
				renderer.setXAxisMax(mWeeklyTimeSeries.getMaxX()+(24*60*60*1000));
				renderer.setXAxisMin(mWeeklyTimeSeries.getMinX()-(24*60*60*1000));
			}
			else if(mWeeklyTimeSeries.getItemCount() > 2 ){
				renderer.setXAxisMax(mWeeklyTimeSeries.getMaxX()/*+(24*60*60*1000)*/);
				renderer.setXAxisMin(mWeeklyTimeSeries.getMinX()/*-(24*60*60*1000)*/);
			}
		}
	}


	private Double calculateMinMaxForY(Boolean daily, Boolean min) {
		// Y axis
		if(daily) {
			if(min) {	// Calculate min
				if(!foundLimits)
					return (mDailyTimeSeries.getMinY()*0.98);
				else {
					if(mDailyTimeSeries.getMinY() > lowerLimit)
						return (lowerLimit * 0.98);
					else
						return (mDailyTimeSeries.getMinY() *0.98);
				}
			}
			else {// Calculate max
				if(!foundLimits)
					return (mDailyTimeSeries.getMaxY()*1.02);
				else {
					if(mDailyTimeSeries.getMaxY() > upperLimit)
						return (mDailyTimeSeries.getMaxY() * 1.02);
					else
						return (upperLimit *1.02);
				}
			}
		}
		else {
			if(min) {	// Calculate min
				if(!foundLimits)
					return (mWeeklyTimeSeries.getMinY()*0.98);
				else {
					if(mWeeklyTimeSeries.getMinY() > lowerLimit)
						return (lowerLimit * 0.98);
					else
						return (mWeeklyTimeSeries.getMinY() *0.98);
				}
			}
			else {// Calculate max
				if(!foundLimits)
					return (mWeeklyTimeSeries.getMaxY()*1.02);
				else {
					if(mWeeklyTimeSeries.getMaxY() > upperLimit)
						return (mWeeklyTimeSeries.getMaxY() * 1.02);
					else
						return (upperLimit *1.02);
				}
			}

		}

	}

	private XYSeriesRenderer prepareSimpleRenderer(int color, int lineWidth, PointStyle style) {

		XYSeriesRenderer simpleRenderer = new XYSeriesRenderer();
		simpleRenderer.setColor(color);
		simpleRenderer.setLineWidth(lineWidth);
		simpleRenderer.setPointStyle(style);
		simpleRenderer.setFillPoints(true);
		return simpleRenderer;
	}

	protected void onDestroy() {
		if(receiver != null)
			unregisterReceiver(receiver);
		super.onDestroy();
	}


	private void initializeHeader() {
		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(StatisticsWithAChart.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener 
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsWithAChart.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)StatisticsWithAChart.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.getAction().equalsIgnoreCase("GraphEvent")){
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {

						//						createDataForGraph(true);
						//						mDailyDataset.removeSeries(0);
						mDailyDataset.removeSeries(mDailyTimeSeries);
						if(foundLimits) {
							mDailyDataset.removeSeries(mDailyLowerTimeSeries);
							mDailyDataset.removeSeries(mDailyUpperTimeSeries);	
						}

						//						mDailyDataset.addSeries(mDailyTimeSeries);
						//						//check for limits too
						//						createDataForGraph(false);
						//						mWeeklyDataset.removeSeries(0);
						mWeeklyDataset.removeSeries(mWeeklyTimeSeries);
						if(foundLimits) {
							mWeeklyDataset.removeSeries(mWeeklyLowerTimeSeries);
							mWeeklyDataset.removeSeries(mWeeklyUpperTimeSeries);
						}
						//						mWeeklyDataset.addSeries(mWeeklyTimeSeries);
						//						//check for limits too
						//
						//						//Set the min and max X,Y axis values
						//						//daily
						//						setMinMaxYValueForRenderer(true, mDailyMultiRenderer);
						//s						setMinMaxXValueForRenderer(true, mDailyMultiRenderer);
						//						//weekly
						//						setMinMaxYValueForRenderer(false, mWeeklyMultiRenderer);
						//						setMinMaxXValueForRenderer(false, mWeeklyMultiRenderer);


						createDataForGraph(true);
						createDataForGraph(false);

						createSimpleRenderers(true);
						createSimpleRenderers(false);



						mDailyMultiRenderer = prepareMultiRenderer(true);

						//	This is where the min max Values for X and Y axes are created and calculateds 
						setMinMaxYValueForRenderer(true, mDailyMultiRenderer);

						setMinMaxXValueForRenderer(true, mDailyMultiRenderer);

						prepareLimitSeries(true);

						prepareListOfRenderers(true);
						
						mDailyMultiRenderer.removeSeriesRenderer(mDailyRenderer);
						if(foundLimits) {
							mDailyMultiRenderer.removeSeriesRenderer(mDailyUpperRenderer);
							mDailyMultiRenderer.removeSeriesRenderer(mDailyLowerRenderer);
						}

						addRenderersToMultiRenderer(mDailyMultiRenderer, dailySimpleRenderers);

						//		5) prepare the weekly multi renderer

						mWeeklyMultiRenderer = prepareMultiRenderer(false);

						setMinMaxYValueForRenderer(false, mWeeklyMultiRenderer);

						setMinMaxXValueForRenderer(false, mWeeklyMultiRenderer);

						prepareLimitSeries(false);

						prepareListOfRenderers(false);
						
						mWeeklyMultiRenderer.removeSeriesRenderer(mWeeklyRenderer);
						if(foundLimits) {
							mWeeklyMultiRenderer.removeSeriesRenderer(mWeeklyUpperRenderer);
							mWeeklyMultiRenderer.removeSeriesRenderer(mWeeklyLowerRenderer);
						}

						addRenderersToMultiRenderer(mWeeklyMultiRenderer, weeklySimpleRenderers);

						addSeriesToDataset(mDailyDataset, true);

						addSeriesToDataset(mWeeklyDataset, false);


						dailyLayout.removeAllViews();
						mDailyChartView = ChartFactory.getTimeChartView(StatisticsWithAChart.this, mDailyDataset, mDailyMultiRenderer, "HH:mm");
						dailyLayout.addView(mDailyChartView);

						weeklyLayout.removeAllViews();
						mWeeklyChartView = ChartFactory.getTimeChartView(StatisticsWithAChart.this, mWeeklyDataset, mWeeklyMultiRenderer, "dd/MM");
						weeklyLayout.addView(mWeeklyChartView);

					}
				}, 500);

			}
			else if(intent.getAction().equalsIgnoreCase("CONTROLS_CHANGED")){

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}

				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(StatisticsWithAChart.this, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}
				}
			}
		}
	}

}