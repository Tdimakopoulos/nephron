package nephron.demo.mobile.statistics;

import nephron.demo.mobile.application.R;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class BicarbonateStatistics extends Activity
{	
	WebView dailyBicarbonateWebView;
	WebView weeklyBicarbonateWebView;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{		
		// Show layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bicarbonatelayout);
				
		// Load daily statistics
		dailyBicarbonateWebView = (WebView)findViewById(R.id.DailyBicarbonateWebView);
		dailyBicarbonateWebView.getSettings().setJavaScriptEnabled(true);
		dailyBicarbonateWebView.loadUrl("file:///android_asset/dailybicarbonategraph.html");
	
		// Load weekly statistics
		weeklyBicarbonateWebView = (WebView)findViewById(R.id.WeeklyBicarbonateWebView);
		weeklyBicarbonateWebView.getSettings().setJavaScriptEnabled(true);
		weeklyBicarbonateWebView.loadUrl("file:///android_asset/weeklybicarbonategraph.html");
	}
}
