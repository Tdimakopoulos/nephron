package nephron.demo.mobile.statistics;
 

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.R;
import nephron.demo.mobile.backend.InternetConnectivityStatus;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Statistics extends Activity {

	private WebView dailyWebView;
	private WebView weeklyWebView;
	private TextView DailyTrendTextView, AverageTrendTextView;
	private String url = "file:///android_asset/dailygraph.html?",url2="file:///android_asset/weeklygraph.html?";
	private int code, i, count;
	private String head1, head2; 
	private Handler handler;
	Button backButton;
	
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.statisticslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		initializeHeader();
		
		backButton = (Button) findViewById(R.id.BackStatisticsButton);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Statistics.this.finish();
			}
		});
		
		handler = new Handler(); 
		i = 0;
		count=0;
		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);
		DailyTrendTextView.setText(head1);
		AverageTrendTextView.setText(head2);
	
		// Load daily statistics
		dailyWebView = (WebView) findViewById(R.id.DailyWebView);
		dailyWebView.getSettings().setJavaScriptEnabled(true);
		
		// Load weekly statistics
		weeklyWebView = (WebView) findViewById(R.id.WeeklyWebView);
		weeklyWebView.getSettings().setJavaScriptEnabled(true);
		
		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		while (c.moveToNext()) {
			url = url + "value=" +  c.getDouble(0)  + "&" + "time="
					+ c.getString(1) + "&";
		}
		c.close();
		url = url.substring(0, url.length() - 1);
		url = url.replaceAll("-", "/");
		url = url.replaceAll(" ", "%20");

		// Load daily statistics
		dailyWebView.loadUrl(url);
		
		Log.e("URL", url);

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		while (c2.moveToNext()) {
			url2 = url2 + "value=" + c2.getDouble(0) + "&" + "date="
					+ c2.getString(1) + "&";
		}
		url2 = url2.substring(0, url2.length() - 1);
		c2.close();
		Log.d("!!PEIMENOUME DATABASE!!", url2);
		
		// Load weekly statistics
		weeklyWebView.loadUrl(url2);
		
		handler.post(mUpdate);
	}

	public Runnable mUpdate = new Runnable() {
		public void run() {
			url="file:///android_asset/dailygraph.html?";
			Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
			while (c.moveToNext()) {
				url = url + "value=" + c.getDouble(0) + "&" + "time="
						+ c.getString(1) + "&";
			}
			count = c.getCount();
			c.close();
			url = url.substring(0, url.length() - 1);
			url = url.replaceAll("-", "/");
			url = url.replaceAll(" ", "%20");
			// Log.d("!!PEIMENOUME DATABASE!!", url);
			if (i < count) {
	 			dailyWebView.loadUrl(url);
				i = count;
         	} 
			// Load weekly statistics
			handler.postDelayed(this, 1000);
		}
	};
	
	protected void onDestroy() {
		super.onDestroy();
		handler.removeCallbacks(mUpdate); 
	}
	
	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(Statistics.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(Statistics.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)Statistics.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(Statistics.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}

	@Override
	protected void onPause() {
		if(receiver !=null)
			unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver != null)
			registerReceiver(receiver, filter);
	}

}