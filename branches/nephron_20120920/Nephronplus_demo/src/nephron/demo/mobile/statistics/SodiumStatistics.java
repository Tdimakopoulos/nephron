package nephron.demo.mobile.statistics;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.R;
import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import android.widget.TextView;

public class SodiumStatistics extends Activity {
	// WebView dailySodiumWebView;
	// WebView weeklySodiumWebView;

	WebView dailyWebView;
	WebView weeklyWebView;
	TextView DailyTrendTextView, AverageTrendTextView;
	ScrollView scrolview;
	String url = "file:///android_asset/dailygraph.html?";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Show layout
		// super.onCreate(savedInstanceState);
		// setContentView(R.layout.sodiumlayout);

		// Load daily statistics
		// dailySodiumWebView = (WebView)findViewById(R.id.DailySodiumWebView);
		// dailySodiumWebView.getSettings().setJavaScriptEnabled(true);
		// dailySodiumWebView.loadUrl("file:///android_asset/dailysodiumgraph.html");

		// Load weekly statistics
		/*
		 * weeklySodiumWebView =
		 * (WebView)findViewById(R.id.WeeklySodiumWebView);
		 * weeklySodiumWebView.getSettings().setJavaScriptEnabled(true);
		 * weeklySodiumWebView
		 * .loadUrl("file:///android_asset/weeklysodiumgraph.html");
		 */

		// Show layout
		super.onCreate(savedInstanceState);
		setContentView(R.layout.statisticslayout);
		scrolview = (ScrollView) findViewById(R.id.scrollview);
		DailyTrendTextView = (TextView) findViewById(R.id.DailyTrendTextView);
		AverageTrendTextView = (TextView) findViewById(R.id.AverageTrendTextView);

		DailyTrendTextView.setText("Daily Creatinine Trends");
		AverageTrendTextView.setText("Weekly Creatinine Trends");

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(911);
		while (c.moveToNext()) {
			url = url + "value=" + c.getDouble(0) + "&" + "time="
					+ c.getString(1) + "&";
		}

		url = url.substring(0, url.length() - 1);
		url = url.replaceAll("-", "/");
		url = url.replaceAll(" ", "%20");
		Log.d("!!PEIMENOUME DATABASE!!", url);
		c.close();
		// Load daily statistics
		dailyWebView = (WebView) findViewById(R.id.DailyWebView);
		dailyWebView.getSettings().setJavaScriptEnabled(true);
		dailyWebView.loadUrl(url);
		// dailyCreatinineWebView.loadUrl("file:///android_asset/exmlp.html?id=1&name=5");
		// Disable Scrolling by setting up an OnTouchListener to do nothing

		// Load weekly statistics
		weeklyWebView = (WebView) findViewById(R.id.WeeklyWebView);
		weeklyWebView.getSettings().setJavaScriptEnabled(true);
		weeklyWebView.loadUrl("file:///android_asset/weeklycreatininegraph.html");

		dailyWebView.setOnTouchListener(new View.OnTouchListener() {
//			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				dailyWebView.getParent().requestDisallowInterceptTouchEvent(true);
				// scrolview.setEnabled(false);
				return false;
			}
		});

	}
}