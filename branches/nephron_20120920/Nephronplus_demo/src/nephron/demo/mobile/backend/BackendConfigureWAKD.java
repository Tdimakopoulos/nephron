package nephron.demo.mobile.backend;


import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.GlobalVar;
import nephron.demo.mobile.application.MessageConverter;
import nephron.demo.mobile.application.R;
import nephron.demo.mobile.datafunctions.MsgConfigureWAKD;
import nephron.demo.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;

public class BackendConfigureWAKD extends Activity {
	AlertDialog alert, message;
	AlertDialog.Builder alertd, messaged;
	String commandID, command, configParams;
	int MessageCounter, incomingID;
	MessageConverter converter;
	private ProgressDialog progressDialog;
	private String ackFilter = "nephron.mobile.application.ACK";
	BackendConfigureWAKDReceiver receiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		converter = new MessageConverter();
		messaged = new AlertDialog.Builder(BackendConfigureWAKD.this);

		receiver = new BackendConfigureWAKDReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(ackFilter);
		registerReceiver(receiver, filter);

		alertd = new AlertDialog.Builder(BackendConfigureWAKD.this);
		alertd.setIcon(R.drawable.warning);

		String incomingAction = getIntent().getAction();
		if(incomingAction.equalsIgnoreCase("WAKD_CONFIGURATION")) {

			// to set sto flag gia to window open tha ginetai sta onclicks tou dialog

			final Timer t = new Timer();

			Bundle extras = getIntent().getExtras();
			commandID = extras.getString("commandID");
			command = extras.getString("command");

			if(Integer.decode(command.substring(6, 8)).equals(23)) {
				configParams = command.substring(12);

				//				if(configParams.length()%5 != 0) {
				//					alertd.setTitle("WAKD Config Error");
				//					alertd.setIcon(drawable.erroricon);
				//					alertd.setMessage("incoming configuration data do not have the correct format.\n" +
				//							"Please inform your doctor.");
				//					alertd.setCancelable(false);
				//					alertd.setNeutralButton("OK", new OnClickListener() {
				//						
				//						@Override
				//						public void onClick(DialogInterface dialog, int which) {
				//							BackendConfigureWAKD.this.finish();
				//						}
				//					});
				//					alert = alertd.create();
				//					alert.show();
				//					
				//				}
				//				else {
				//					
				//				}



				alertd.setTitle("WAKD Configuration");
				alertd.setMessage("WAKD configuration has been issued by your doctor, execute now?");

				alertd.setPositiveButton("OK", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						t.cancel();
						alert.dismiss();

						//Shutdown Functionality will be placed here
						progressDialog = ProgressDialog.show(BackendConfigureWAKD.this, "", "Executing WAKD configuration,\nplease wait...");

						new Thread(new Runnable() {

							public void run() {


								WakdCommandEnum configCommand = WakdCommandEnum.CONFIGURE_WAKD;
								MsgConfigureWAKD configureWAKDMessage;
								boolean ackArrived = false;
								MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
								for(int i=0;i<3;i++){
									configureWAKDMessage = new MsgConfigureWAKD(MessageCounter, configCommand, configParams);
									try {
										ConnectionService.u.sendDatatoWAKD(configureWAKDMessage.encode());
										Thread.sleep(5 * 1000);
									} catch (IOException e) {
										e.printStackTrace();
									} catch (InterruptedException e) {
										e.printStackTrace();
									}

									if (incomingID == MessageCounter ) {
										ackArrived = true ;
										break;
									}										 
								}

								if(progressDialog!=null) {
									if (progressDialog.isShowing()) {
										progressDialog.dismiss();
									}
								}

								if(ackArrived) {
									Looper.prepare();

									// Functionality to store the WAKD Conf Params in the local database as well...
									// This will be performed only after the configuration has been acknowledged by the WAKD_CONFIGURATION
									boolean stored = decodeAndStoreConfigurationCommand(configParams);
									messaged.setCancelable(false);
									if(stored)
										messaged.setMessage("Configuration sent to WAKD Successfully.\n" +
												"A local copy has been successfully stored.");
									else
										messaged.setMessage("Configuration sent to WAKD Successfully.\n" +
												"A local copy has not been successfully stored.");
									messaged.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										public void onClick(DialogInterface dialog, int which) {
											message.dismiss();

											createClientforStoreCommand(BackendConfigureWAKD.this, commandID);

											((GlobalVar)getApplicationContext()).setOperationInProgress(false);
											BackendConfigureWAKD.this.finish();   
										}
									});
									message = messaged.create();
									message.show();
									Looper.loop();
								}
								else {
									Looper.prepare();
									messaged.setMessage("Your Command did not\narrive to WAKD");
									messaged.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										public void onClick( DialogInterface dialog, int which) {
											message.dismiss();
											((GlobalVar)getApplicationContext()).setOperationInProgress(false);
											BackendConfigureWAKD.this.finish();
										}
									});
									message = messaged.create();
									message.show();
									Looper.loop();
								}
							}
						}).start();

					}
				});

				alertd.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						t.cancel();
						((GlobalVar)getApplicationContext()).setOperationInProgress(false);
						BackendConfigureWAKD.this.finish();
					}
				});

				alert = alertd.create();

				if(this.getSharedPreferences("userSettings", Context.MODE_WORLD_READABLE).getString("Vibration", "Off").equalsIgnoreCase("On")) {
					Vibrator mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
					mVibrator.vibrate(300);
				}

				alert.show();

				t.schedule(new TimerTask() {

					public void run() {
						t.cancel();
						alert.dismiss();
						BackendConfigureWAKD.this.finish();
						((GlobalVar)getApplicationContext()).setOperationInProgress(false);
					}
				}, 5000);

			}
		}

	}



	private Boolean decodeAndStoreConfigurationCommand(String configString) {

		//		byte[] commandByteArray = DataTransformationUtils.convertStringToByteArray(configString);


		if((configString.length())%5 == 0) {

			try {

				for(int i=0;i<configString.length();i=i+10) {

					// The configuration type
					String sconfigType = configString.substring(i, i+2);
					int configType = Integer.parseInt(sconfigType,16);

					Integer configValue = Integer.parseInt(configString.substring(i+2, i+10), 16);

					//The configuration value

					Double value = ConnectionService.db.getWAKDConfigParameter(configType);
					Double dConfigValue = (double)configValue;
					Log.e("before insert", String.valueOf(dConfigValue));
					if( value != null){
						ConnectionService.db.updateWAKDConfigParamvalue(configType, (double)(configValue));
					}
					else{
						ConnectionService.db.insertWakdConfigParameter(configType, (double)(configValue));
					}
				}
				return true;
			} catch (NumberFormatException e) {
				e.printStackTrace();
				return false;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}


	//	private Boolean decodeAndStoreConfigurationCommand(String configString) {
	//
	//		byte[] commandByteArray = DataTransformationUtils.convertStringToByteArray(configString);
	//		
	//		
	//		if((commandByteArray.length)%5 == 0) {
	//			
	//			try {
	//				for(int i=0;i<commandByteArray.length;i=i+5) {
	//					
	//					// The configuration type
	//					int configType = Integer.parseInt(Byte.toString(commandByteArray[i]));
	//
	//					//The configuration value
	//					byte[] valueBytes = {0,0,0,0,commandByteArray[i+1],commandByteArray[i+2],commandByteArray[i+3],commandByteArray[i+4]};
	//					Double configValue = ByteBuffer.wrap(valueBytes).order(ByteOrder.LITTLE_ENDIAN).getDouble();
	//					Log.e("before insert", String.valueOf(configValue));
	//
	//					Double value = ConnectionService.db.getWAKDConfigParameter(configType);
	//					if( value != null){
	//						ConnectionService.db.updateWAKDConfigParamvalue(configType, configValue);
	//						Log.e("WAKD CONFIG PARAM STATUS","UPDATE");
	//					}
	//					else{
	//						ConnectionService.db.insertWakdConfigParameter(configType, configValue);
	//						Log.e("WAKD CONFIG PARAM STATUS","INSERT");
	//					}
	//				}
	//				return true;
	//			} catch (NumberFormatException e) {
	//				e.printStackTrace();
	//				return false;
	//			} catch (SQLException e) {
	//				e.printStackTrace();
	//				return false;
	//			}
	//		}
	//		return false;
	//	}


	@Override
	public void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}


	private void createClientforStoreCommand(final Context context, final String commandIDToAck) {

		class SoapCommandClient implements Runnable {
			@Override
			public void run() {
				if(InternetConnectivityStatus.getInstance(context).isOnline(context)) {
					NephronSOAPClient client = new NephronSOAPClient(BackendConfigureWAKD.this);
					String response = client.executeACKRequest(Long.valueOf(commandIDToAck));
					if( response!= null ) {
						if(response.equalsIgnoreCase("Command Updated"))
							Log.e("BackendConfigureWAKD", "Remote command successfully acked");
						else if(response.equalsIgnoreCase("Command ID does not exist"))
							Log.e("BackendConfigureWAKD", "Remote command does not exist, ack failed");
						else
							Log.e("BackendConfigureWAKD", "General error occured, unknown response");
					}
				}
			}
		}
		Thread thread = new Thread(new SoapCommandClient());
		thread.start();
	}

	public class BackendConfigureWAKDReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// ACK received from WAKD, after succcessfully receiving the config parameters
			if(intent.getAction().equals(ackFilter)) {
				incomingID = intent.getIntExtra("messageCount", 0);
			}
		}
	}

	private Double convertFiPointShift(int code, Double value){
		switch(code) {
		case 1:
			return value;
			//  1: m/f
		case 2:			//  2: daily time for weight & bp measurement, hour
			return value;
			
		case 3:		//  3: daily time for weight & bp measurement, minute
			return value;
			

		case 4:			//  4: weight control target [same unit as weigth measurement on RTB]
			return value;
			
		case 5:	//  5: weight control last measurement [same unit as weigth measurement on RTB]
			return value;
			
		case 6:		//  6: weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
			return value;
			
		case 7:				//  7: potassium control target use FIXPOINTSHIFT for [mmol/l]
			return value/10;
			
		case 8:		//  8: potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
			return value/10;
			
		case 9:			//  9: potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
			return value/10;
			
		case 10:				//  a: urea control target use FIXPOINTSHIFT for [mmol/l]
			return value/10;
			
		case 11:		//  b: urea control last measurement use FIXPOINTSHIFT for [mmol/l]
			return value/10;
			
		case 12:		//  c: urea default removal per day use FIXPOINTSHIFT for [mmol/day]
			return value/10;
			
		case 13:			//  d: minimal flowrate for plasma in Dialysis mode  ml/min
			return value;
			
		case 14:			//  e: maximal flowrate for plasma in Dialysis mode  ml/min
			return value;
			
		case 15:			//  f: minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
			return value/10;
			
		case 16:			// 10: maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
			return value/10;
			

			// To be configured for every state of "enumWakdStates"
		case 17:		// 11: setting speed blood pump [ml/min]
			return value;
			
		case 18:		// 12: setting speed fluidic pump [ml/min]
			return value;
			
		case 19:					// 13: setting voltage polarization [V]
			return value;
			
		case 20:				// 14: polarity of polarization voltage.
			return value;
			
		case 21:				// 15: pre-setting for duration of "enumWakdStates"
			return value;
			
			
			// WAKD parameters
			//		parameter_NaAbsL((byte) 22),		    // 16: lower Na absolute threshold
			//		parameter_NaAbsH((byte) :),     		// 17: upper Na absolute threshold
			//		parameter_NaTrdLT((byte) 24),			// 18: lower Na time period
			//		parameter_NaTrdHT((byte) 25),			// 19: upper Na time period
			//		parameter_NaTrdLPsT((byte) 26),		// 1a: lower Na trend per Specified Time threshold
			//		parameter_NaTrdHPsT((byte) 27),		// 1b: upper Na trend per Specified Time threshold


		case 28:			// 1c: lower K absolute threshold
			return value/10;
			
		case 29:			// 1d: upper K absolute threshold
			return value/10;
			
		case 30:				// 1e: lower K absolute alarm threshold
			return value/10;
			
		case 31:				// 1f: upper K absolute alarm threshold
			return value/10;
			
		case 32:			// 20: lower K trend low time-difference
			return value/10;
			
		case 33:			// 21: upper K trend low time-difference
			return value/10;
			
		case 34:			// 22: lower K trend per Specified Time threshold
			return value/10;
			
		case 35:			// 23: upper K trend per Specified Time threshold
			return value/10;
			
		case 36:			// 24: lower Ca absolute threshold
			return value/10;
			
		case 37:			// 25: upper Ca absolute threshold
			return value/10;
			
		case 38:			// 26: lower Ca absolute alarm threshold
			return value/10;
			
		case 39:			// 27: upper Ca absolute alarm threshold
			return value/10;
			
		case 40:			// 28: lower Ca time period
			return value/10;
			
		case 41:			// 29: upper Ca time period
			return value/10;
			
		case 42:		// 2a: lower Ca trend per Specified Time threshold
			return value/10;
			
		case 43:		// 2b: upper Ca trend per Specified Time threshold
			return value/10;
			

			//		parameter_UreaAAbsH((byte) 44),		// 2c: upper Urea absolute alarm threshold
			//		parameter_UreaTrdHT((byte) 45),		// 2d: upper Urea time period
			//		parameter_UreaTrdHPsT((byte) 46),		// 2e: upper Urea trend per Specified Time
			//
			//		parameter_CreaAbsL((byte) 47),			// 2f: lower Crea absolute threshold
			//		parameter_CreaAbsH((byte) 48),			// 30: upper Crea absolute threshold
			//		parameter_CreaTrdHT((byte) 49),		// 31: upper Crea time period
			//		parameter_CreaTrdHPsT((byte) 50),		// 32: upper Crea trend per Specified Time
			//
			//		parameter_PhosAbsL((byte) 51),			// 33: lower Phos absolute threshold
			//		parameter_PhosAbsH((byte) 52),			// 34: higher Phos absolute threshold
			//		parameter_PhosAAbsH((byte) 53),		// 35: upper Phos absolute alarm threshold
			//		parameter_PhosTrdLT((byte) 54),		// 36: lower Phos time period
			//		parameter_PhosTrdHT((byte) 55),		// 37: upper Phos time period
			//		parameter_PhosTrdLPsT((byte) 56),		// 38: lower Phos trend per Specified Time threshold
			//		parameter_PhosTrdHPsT((byte) 57),		// 39: upper Phos trend per Specified Time
			//
			//		parameter_HCO3AAbsL((byte) 58),		// 3a: lower HCO3 absolute alarm threshold
			//		parameter_HCO3AbsL((byte) 59),			// 3b: lower HCO3 absolute threshold
			//		parameter_HCO3AbsH((byte) 60),			// 3c: upper HCO3 absolute threshold
			//		parameter_HCO3TrdLT((byte) 61),		// 3d: lower HCO3 time period
			//		parameter_HCO3TrdHT((byte) 62),		// 3e: upper HCO3 time period
			//		parameter_HCO3TrdLPsT((byte) 63),		// 3f: lower HCO3 trend per Specified Time threshold
			//		parameter_HCO3TrdHPsT((byte) 64),		// 40: upper HCO3 trend per Specified Time

		case 65:			// 41: lower pH absolute alarm threshold
			return value/10;
			
		case 66:			// 42: upper pH absolute alarm threshold
			return value/10;
			
		case 67:			// 43: lower pH absolute threshold
			return value/10;
			
		case 68:			// 44: upper pH absolute threshold
			return value/10;
			
		case 69:			// 45: lower pH time period
			return value/10;
			
		case 70:			// 46: upper pH time period
			return value/10;
			
		case 71:		// 47: lower pH trend per Specified Time threshold
			return value/10;
			
		case 72:		// 48: upper pH trend per Specified Time threshold
			return value/10;
			
		case 73:		// 49: lower BPsys absolute threshold
			return value;
			
		case 74:		// 4a: upper BPsys absolute threshold
			return value;
			
		case 75:		// 4b: upper BPsys absolute alarm threshold
			return value;
			
		case 76:		// 4c: lower BPdia absolute threshold
			return value;
			
		case 77:		// 4d: upper BPdia absolute threshold
			return value;
			
		case 78:		// 4e: upper BPdia absolute alarm threshold
			return value;
			
		case 79:		// 4f: accepted flow deviation ml/min
			return value;
			
		case 80:		// 50: accepted flow deviation ml/min
			return value;
			
		case 81:	// 51: accepted vertical deflection threshold
			return value;
			
		case 82:			// 52: lower Weight absolute threshold
			return value;
			
		case 83:			// 53: upper Weight absolute threshold
			return value;
			
		case 84:			// 54: Weight trend threshold
			return value;
			
		case 85:			// 55: Fluid Extraction Deviation Percentage Threshold Low
			return value;
			
		case 86:			// 56: Fluid Extraction Deviation Percentage Threshold
			return value;
			
		case 87:			// 57: sensor 'BPSo' pressure upper threshold
			return value;
			
		case 88:			// 58: sensor 'BPSo' pressure lower threshold
			return value;
			
		case 89:			// 59: sensor 'BPSi' pressure upper threshold
			return value;
			
		case 90:			// 5a: sensor 'BPSi' pressure lower threshold
			return value;
			
		case 91:			// 5b: sensor 'FPS1' pressure upper threshold
			return value;
			
		case 92:			// 5c: sensor 'FPS1' pressure lower threshold
			return value;
			
		case 93:			// 5d: sensor 'FPS2' pressure upper threshold
			return value;
			
		case 94:			// 5e: sensor 'FPS2' pressure lower threshold
			return value;
			
		case 95:			// 5f: temperature sensor BTSo value upper threshold
			return value;
			
		case 96:			// 60: temperature sensor BTSo value lower threshold
			return value;
			
		case 97:			// 61: temperature sensor BTSi value upper threshold
			return value;
			
		case 98:			// 62: temperature sensor BTSi value lower threshold
			return value;
			
		case 99:			// 63: battery status threshold
			return value;
			

			// Expected adsorption rate calculation
			// parameters named according to Nanodialysis funciton of the sorbent unit
			//		parameter_f_K((byte) 100),				// 64:
			//		parameter_SCAP_K((byte) 101),			// 65:
			//		parameter_P_K((byte) 102),				// 66:
			//		parameter_Fref_K((byte) 103),			// 67:
			//		parameter_cap_K((byte) 104),			// 68:
			//		parameter_f_Ph((byte) 105),				// 69:
			//		parameter_SCAP_Ph((byte) 106),			// 6a:
			//		parameter_P_Ph((byte) 107),				// 6b:
			//		parameter_Fref_Ph((byte) 108),			// 6c:
			//		parameter_cap_Ph((byte) 109),			// 6d:
			//		parameter_A_dm2((byte) 110),			// 6e:
			//		parameter_CaAdr((byte) 111),			// 6f:
			//		parameter_CreaAdr((byte) 112),			// 70:
			//		parameter_HCO3Adr((byte) 113),			// 71:
			//		parameter_wgtNa((byte) 114),			// 72: the fuzzy importance of the adsorption of this substance
			//		parameter_wgtK((byte) 115),				// 73:
			//		parameter_wgtCa((byte) 116),			// 74:
			//		parameter_wgtUr((byte) 117),			// 75:
			//		parameter_wgtCrea((byte) 118),			// 76:
			//		parameter_wgtPhos((byte) 119),			// 77:
			//		parameter_wgtHCO3((byte) 120),			// 78:
			//		parameter_mspT((byte) 121);				// 79: set the threshold here!
		default:
			return value;
		}

	}





}