package nephron.demo.mobile.backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import nephron.demo.mobile.application.GlobalVar;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

public class NephronSOAPClient {

	private Context context;
	
	String serverAddress = "";

	public NephronSOAPClient(Context context) {
		this.context = context;
		serverAddress = retrieveServerAddress();
		if(serverAddress == null || serverAddress == "")
			serverAddress = "178.63.69.149:80";
		
		Log.e("SOAP CLIENT SERVER ADDRESS", serverAddress);
	}

	public String executeWeightRequest(String weightData) {

		String nameSpace = "http://weight.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/WeightManager";
		String soapAction = "http://weight.nephron.eu/WeightManager/WeightManagerNewValueReceived";
		String methodName = "WeightManagerNewValueReceived";

		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		
		SoapObject request = new SoapObject(nameSpace, methodName);

		request.addProperty("imei", deviceID);
		request.addProperty("date", String.valueOf(new Date().getTime()));
		request.addProperty("reply", weightData);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				if(response.toString().equalsIgnoreCase("OK"))
					return response.toString();
				else
					return null;
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		return null;
	}

	
	public String executeQuestionnaireRequest(String questionid, String answer) {

		String nameSpace = "http://questionary.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/QuestionaryManager";
		String soapAction = "http://questionary.nephron.eu/QuestionaryManager/QuestionaryManagerSaveQuestionReply";
		String methodName = "QuestionaryManagerSaveQuestionReply";

		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

		SoapObject request = new SoapObject(nameSpace, methodName);

		request.addProperty("questionid", questionid);
		request.addProperty("imei", deviceID);
		request.addProperty("questionarydate", String.valueOf(new Date().getTime()));
		request.addProperty("questionreply", answer);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	//TODO ADD FUNCTIONALITY FOR HANDLING IO EXCEPTIONS AND STORING COMMANDS TO GLOBALVAR ARRAY
	// CHECK HOW THIS CAN BE IMPLEMENTED AS IS RIGHT NOW
	public String executeACKRequest(Long commandID) {

		//	successful ACK returns "Command Updated"
		//	unsuccessful ACK returns "Command ID does not exist"

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/MarkACK";
		String methodName = "MarkACK";

		SoapObject request = new SoapObject(nameSpace, methodName);
		request.addProperty("commandid", commandID);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	
	public String executeUploadFileRequest(byte[] fileContents, Long length, Long date) {

		String nameSpace = "http://ws.file.nephron.eu/";
		String URL = "http://" + serverAddress + "/NephronFileAttachments/NephronECGManagerWS";
		String soapAction = "http://ws.file.nephron.eu/NephronECGManagerWS/ECGTransfer";
		String methodName = "ECGTransfer";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		
		request.addProperty("ECG", fileContents);
		
		request.addProperty("IMEI", deviceID);
		request.addProperty("Date", date);
		request.addProperty("Length", length);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
       	new MarshalBase64().register(envelope);   //serialization
       	envelope.encodingStyle = SoapEnvelope.ENC;
		
		envelope.bodyOut = request;
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}


	public String executeRetrieveCommandRequest(String imei) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/RetrieveCommand";
		String methodName = "RetrieveCommand";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				return response.toString();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}



	public String executeBackendLoginRequest(Object param) {

		String nameSpace = "http://webservices.nephron.eu/";
		String URL = "http://" + serverAddress + "/PKI-Infastructure/KeyManagerService";
		String soapAction = "http://webservices.nephron.eu/KeyManager/GetUserRole";
		String methodName = "GetUserRole";

		SoapObject request = new SoapObject(nameSpace, methodName);
		PropertyInfo attachedParam = new PropertyInfo();
		attachedParam.setName("key");
		attachedParam.setValue(param);
		attachedParam.setType(String.class);
		request.addProperty(attachedParam);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
		return null;
	}


	public String executeStoreCommandRequest(String hexCommand, long date, List<String> restArgs) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/StoreCommand";
		String methodName = "StoreCommand";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		request.addProperty("command", hexCommand);
		request.addProperty("date", date);

		if(restArgs != null) {
			if(!restArgs.isEmpty()) {
				int counter =1;
				for(String arg : restArgs) {
					request.addProperty("param" + counter, arg);
					counter++;
				}
			}
		}
		else{

			//TODO Change the webservice so that it accepts null values as well

			request.addProperty("param1", ".");
			request.addProperty("param2", ".");
			request.addProperty("param3", ".");
			request.addProperty("param4", ".");
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				return response.toString();
			}

		} catch (IOException e) {
//			((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(hexStringToByteArray(hexCommand));
			e.printStackTrace();
		} catch (XmlPullParserException e) {
//			((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(hexStringToByteArray(hexCommand));
			e.printStackTrace();
		} 
		return null;
	}

	
	public String executeStoreWAKDStatusRequest(String hexCommand, long date, List<String> restArgs) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://" + serverAddress + "/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/StoreWAKD";
		String methodName = "StoreWAKD";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		request.addProperty("wakdstatus", hexCommand);
		request.addProperty("date", date);

		if(restArgs != null) {
			if(!restArgs.isEmpty()) {
				int counter =1;
				for(String arg : restArgs) {
					request.addProperty("param"+counter, arg);
					counter++;
				}
			}
		}
		else{

			//TODO Change the webservice so that it accepts null values as well

			request.addProperty("param1", ".");
			request.addProperty("param2", ".");
			request.addProperty("param3", ".");
			request.addProperty("param4", ".");
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				// Log.d("INSIDE THE WAKD STATUS CLIENT", "FINISHED STORE WAKD STATUS REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
//			((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(hexStringToByteArray(hexCommand));
		} catch (XmlPullParserException e) {
			e.printStackTrace();
//			((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(hexStringToByteArray(hexCommand));
		}
		return null;
	}
	
	
	public String executeUploadCommandsFileContents(List<String> commandsList) {

		String nameSpace = "http://ws.connectivity.nephron.eu/";
		String URL = "http://" + serverAddress + "/ConnectivityServices/OfflineCommandQ";
		String soapAction = "http://ws.connectivity.nephron.eu/OfflineCommandQ/offlinecommandprocessing";
		String methodName = "offlinecommandprocessing";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		
		String csCommands = new String();
		for(String command :commandsList){
			csCommands = csCommands + "," + command;
		}
		
		request.addProperty("offlinecommands", csCommands);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				Log.e("UPLOAD COMMANDS RESPONSE", response.toString());
				return response.toString();
			}

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}


	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}
	
	
	private String retrieveServerAddress() {

		String filename = "serveraddress.txt";
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,filename);

		//Read text from file
		StringBuilder sbuilder = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				sbuilder.append(line);
			}
		}
		catch (IOException e) {
			Log.d("IP_FILE_HANDLER", "Error occurred while opening file");
			e.printStackTrace();
			return null;
		}
		String skey = sbuilder.toString();
		return skey;
	}
	
	
}