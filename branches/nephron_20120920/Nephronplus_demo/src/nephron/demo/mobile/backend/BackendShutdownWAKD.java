package nephron.demo.mobile.backend;


import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.GlobalVar;
import nephron.demo.mobile.application.MessageConverter;
import nephron.demo.mobile.application.R;
import nephron.demo.mobile.datafunctions.MsgSimple;
import nephron.demo.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Looper;
import android.os.Vibrator;
import android.util.Log;
import android.view.Window;

public class BackendShutdownWAKD extends Activity {
	AlertDialog alert, message;
	AlertDialog.Builder alertd, messaged;
	String commandID, command;
	int MessageCounter, incomingID;
	MessageConverter converter;
	private ProgressDialog progressDialog;
	private String ackFilter = "nephron.mobile.application.ACK";
	BackendShutdownReceiver receiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		converter = new MessageConverter();
		messaged = new AlertDialog.Builder(BackendShutdownWAKD.this);
		//		message = new AlertDialog.Builder(BackendShutdownWAKD.this).create();

		receiver = new BackendShutdownReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(ackFilter);
		registerReceiver(receiver, filter);

		alertd = new AlertDialog.Builder(BackendShutdownWAKD.this);
		alertd.setIcon(R.drawable.warning);

		String incomingAction = getIntent().getAction();
		if(incomingAction.equalsIgnoreCase("BACKEND_SHUTDOWN")) {

			// to set sto flag gia to window open tha ginetai sta onclicks tou dialog

			final Timer t = new Timer();

			Bundle extras = getIntent().getExtras();
			commandID = extras.getString("commandID");
			command = extras.getString("command");

			if(Integer.decode(command.substring(6, 8)).equals(2)) {

				alertd.setTitle("Shutdown WAKD");
				alertd.setMessage("ShutDown has been issued by your doctor, execute now?");

				alertd.setPositiveButton("OK", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						t.cancel();
						alert.dismiss();

						//Shutdown Functionality will be placed here
						progressDialog = ProgressDialog.show(BackendShutdownWAKD.this, "", "Please wait...");

						new Thread(new Runnable() {

							public void run() {

								WakdCommandEnum command = WakdCommandEnum.SHUTDOWN;
								MsgSimple shutDown;
								boolean ackArrived = false;
								MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
								for(int i=0;i<3;i++){
									shutDown = new MsgSimple(MessageCounter, command);
									try {
										ConnectionService.u.sendDatatoWAKD(shutDown.encode());
										Thread.sleep(5 * 1000);
									} catch (IOException e) {
										e.printStackTrace();
									} catch (InterruptedException e) {
										e.printStackTrace();
									}

									if (incomingID == MessageCounter ) {
										ackArrived = true ;
										break;
									}										 
								}

								if(progressDialog!=null) {
									if (progressDialog.isShowing()) {
										progressDialog.dismiss();
									}
								}

								if(ackArrived) {
									Looper.prepare();
									messaged.setMessage("Your Command Arrived Successfully.\n" +
											"Press the power button on the device.");

									messaged.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										public void onClick(DialogInterface dialog, int which) {
											message.dismiss();
											((GlobalVar)getApplicationContext()).setOperationInProgress(false);

											//TODO place function for sending ACK to backend
											NephronSOAPClient client = new NephronSOAPClient(BackendShutdownWAKD.this);
											String response = client.executeACKRequest(Long.valueOf(commandID));
											if( response!= null ) {
												if(response.equalsIgnoreCase("Command Updated"))
													Log.e("BackendShutdownWAKD", "Remote command successfully acked");
												else if(response.equalsIgnoreCase("Command ID does not exist"))
													Log.e("BackendShutdownWAKD", "Remote command does not exist, ack failed");
												else
													Log.e("BackendShutdownWAKD", "General error occured, unknown response");
											}
											//TODO Add the ability to add to the global untransmitted commands
											BackendShutdownWAKD.this.finish();
										}
									});
									message = messaged.create();
									message.show();
									Looper.loop();
								}
								else {
									Looper.prepare();
									messaged.setMessage("Your Command did not\narrive to WAKD");
									messaged.setPositiveButton("OK", new DialogInterface.OnClickListener() {

										public void onClick( DialogInterface dialog, int which) {
											message.dismiss();
											BackendShutdownWAKD.this.finish();
											((GlobalVar)getApplicationContext()).setOperationInProgress(false);
										}
									});
									message = messaged.create();
									message.show();
									Looper.loop();
								}
							}
						}).start();

					}
				});

				alertd.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						t.cancel();
						BackendShutdownWAKD.this.finish();
						((GlobalVar)getApplicationContext()).setOperationInProgress(false);
					}
				});

				alert = alertd.create();

				if(this.getSharedPreferences("userSettings", Context.MODE_WORLD_READABLE).getString("Vibration", "Off").equalsIgnoreCase("On")) {
					Vibrator mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
					mVibrator.vibrate(300);
				}

				alert.show();

				t.schedule(new TimerTask() {

					public void run() {
						t.cancel();
						alert.dismiss();
						BackendShutdownWAKD.this.finish();
						((GlobalVar)getApplicationContext()).setOperationInProgress(false);
					}
				}, 5000);
			}
		}


	}

	@Override
	public void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}

	public class BackendShutdownReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// ACK received from 
			if(intent.getAction().equals(ackFilter)) {
				incomingID = intent.getIntExtra("messageCount", 0);
			}
		}
	}

}