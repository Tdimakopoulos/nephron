package nephron.demo.mobile.application;

import java.io.IOException;
import java.math.BigInteger;

import nephron.demo.mobile.application.R.drawable;
import nephron.demo.mobile.application.utils.DataTransformationUtils;
import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.backend.NephronSOAPClient;
import nephron.demo.mobile.datafunctions.ByteUtils;
import nephron.demo.mobile.datafunctions.MsgSimple;
import nephron.demo.mobile.datafunctions.MsgWeightDataOk;
import nephron.demo.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class MedicalDevices extends Activity {

	public MedicalDevices() {
		super();
	}

	Button getWeightScaleButton, getBloodPressureButton, getECGButton, cancelButton;
	Button attachBPButton, attachWSButton, attachECGButton;
	AlertDialog.Builder confirmation, correctWeightData, passwordBuilder, bpasswordInformer, userOperationInitDialog, ecgbuilder; //FIXME The ecgBuilder should be removed when we have full functionality enabled
	private ProgressDialog progressDialog;
	int MessageCounter, incomingID;
	MessageConverter weightConverter, ecgConverter, bpConverter;
	AlertDialog message, bpmessage, ecgMessageDialog;

	Boolean weightAvailable = true;
	Boolean bpAvailable = true;
	Boolean ecgAvailable = true;

	String weightfilter = "nephron.mobile.application.PhysicalMeasurementEvent1";
	String bpfilter = "nephron.mobile.application.PhysicalMeasurementEvent2";
	String ecgfilterStart = "nephron.mobile.application.ECGStartStreamMeasurementEvent";
	String ecgfilterStop = "nephron.mobile.application.ECGStopStreamMeasurementEvent";
	String ackFilter = "nephron.mobile.application.ACK";
	String headerFilter = "CONTROLS_CHANGED";
	String attachedWSFilter = "nephron.mobile.application.AttachedWSDevice";
	String attachedBPFilter = "nephron.mobile.application.AttachedBPDevice";
	String attachedECGFilter = "nephron.mobile.application.AttachedECGDevice";
	FunctionsIncomingReceiver receiver;
	EditText weightInput,bodyFatInput, passwordInput;
	MessageConverter converter;
	View passwordView, correctWeight;

	EditText mEditText;
	Button posButton, negButton;
	Boolean authCheck=false;
	String storedPassword;

	AlertDialog passwordChecker, passwordInformer;

	IntentFilter intentFilter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	Context context;

	String ecgReturnMessage = "", ecgTitlemessage = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.functionslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Medical Devices");

		context = MedicalDevices.this;

		initializeHeader();

		getWeightScaleButton = (Button) findViewById(R.id.ButtonGetWeightScale);
		getBloodPressureButton = (Button) findViewById(R.id.GetBloodPressure);
		getECGButton = (Button) findViewById(R.id.GetECG);
		//		attachWSButton = (Button) findViewById(R.id.attachWeightScale);
		//		attachBPButton = (Button) findViewById(R.id.attachBP);
		//		attachECGButton = (Button) findViewById(R.id.attachECG);
		cancelButton = (Button) findViewById(R.id.BackButton);
		confirmation = new AlertDialog.Builder(this);
		weightConverter = new MessageConverter();
		message = new AlertDialog.Builder(this).create();
		progressDialog = new ProgressDialog(getApplicationContext());

		// get correct view
		//		LayoutInflater li = LayoutInflater.from(this);
		//		correctWeight = li.inflate(R.layout.correctweight, null);
		//		weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
		//		bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
		//		weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		//		bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		//		correctWeightData = new AlertDialog.Builder(this);

		bpasswordInformer = new AlertDialog.Builder(MedicalDevices.this);
		bpasswordInformer.setCancelable(true);
		bpasswordInformer.setTitle("Password Error");
		bpasswordInformer.setIcon(R.drawable.erroricon);
		bpasswordInformer.setNeutralButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(passwordInformer.isShowing()){
					passwordInformer.dismiss();
				}
				((GlobalVar)context.getApplicationContext()).setOperationInProgress(false);
			}
		});

		//	Button: Get Weight	************************************
		getWeightScaleButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(MedicalDevices.this);
				View passwordView = LayoutInflater.from(MedicalDevices.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning); 
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();

					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								confirmation.setMessage("Step up on weight scale and press OK" );
								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,	int which) {

										progressDialog = ProgressDialog.show(MedicalDevices.this, "", "Please wait...", false, true);

										new Thread(new Runnable() {
											public void run() {
												WakdCommandEnum command = WakdCommandEnum.WEIGHT_REQUEST;
												MsgSimple weightRequest;
												boolean ackArrived = false;
												MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
												for(int i=0;i<3;i++){
													weightRequest = new MsgSimple(MessageCounter, command);
													try {
														// Thread.sleep(10000); //10secs
														ConnectionService.u.sendDatatoWAKD(weightRequest.encode());
														Thread.sleep(5 * 1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													} catch(Exception e) {
														e.printStackTrace();
													}


													if (incomingID == MessageCounter ) {
														ackArrived = true ;
														break;
													}
												}


												if(ackArrived) {
													if(progressDialog!=null) {
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
															Looper.prepare();
															progressDialog = ProgressDialog.show(MedicalDevices.this, "",
																	"Your Command Arrived Successfully\nPlease wait for Response to come...");
															new Thread(new Runnable() {
																public void run() {
																	Log.d(MedicalDevices.class.getName(),"Response");
																	try {
																		Thread.sleep(20 * 1000);
																		if (progressDialog.isShowing()) {
																			//	Log.d("mesa sto to thread ","mesa sto to thread");
																			progressDialog.dismiss();
																			Looper.prepare();
																			message.setMessage("Nothing Happened");
																			message.setButton("OK",
																					new DialogInterface.OnClickListener() {
																				public void onClick(
																						DialogInterface dialog,
																						int which) {
																					// here you can add functions
																				}
																			});
																			message.show();
																			Looper.loop();
																		}
																	} catch (InterruptedException e) {
																		e.printStackTrace();
																	}
																}
															}).start();

															Looper.loop();
														}}	
												}
												else{
													if(progressDialog!=null){
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
														}}
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											}
										}).start();
									}
								});
								confirmation.setNegativeButton("Cancel",null);
								confirmation.show();
							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}

					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}
		});


		//	Button: Get Blood Pressure	****************************
		getBloodPressureButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				passwordBuilder = new AlertDialog.Builder(MedicalDevices.this);
				View passwordView = LayoutInflater.from(MedicalDevices.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();
					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								confirmation.setTitle("Blood Pressure Request");
								confirmation.setMessage("Please connect to your blood pressure machine. Press OK when ready.");
								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog, int which) {
										progressDialog = ProgressDialog.show(MedicalDevices.this, null,
												"Blood pressure acquisition in progress...", false, true);

										new Thread(new Runnable() {

											@Override
											public void run() {
												WakdCommandEnum bpCommand = WakdCommandEnum.BP_REQUEST;

												boolean ackArrived = false;
												MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
												for(int i=0;i<3;i++) {
													MsgSimple bloodPressureRequest = new MsgSimple(MessageCounter, bpCommand);
													Log.d("Blood Pressure Request Thread", "Sending request");

													try {
														ConnectionService.u.sendDatatoWAKD(bloodPressureRequest.encode());
														Thread.sleep(5*1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													if(incomingID == MessageCounter) {
														ackArrived = true;
														break;
													}

												}

												if(ackArrived) {
													Log.d("ACK","ACK response from MB arrived");
													if(progressDialog!=null) {
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
															Looper.prepare();
															progressDialog = ProgressDialog.show(MedicalDevices.this, "",
																	"Your command arrived successfully, please wait for response.");
															new Thread(new Runnable() {
																public void run() {
																	Log.d(MedicalDevices.class.getName(),"Response");
																	try {
																		Thread.sleep(20 * 1000);
																		if (progressDialog.isShowing()) {
																			//Log.d("mesa sto to thread ","mesas sto to thread");
																			progressDialog.dismiss();
																			Looper.prepare();
																			message.setMessage("Nothing Happened");
																			message.setButton("OK",
																					new DialogInterface.OnClickListener() {
																				public void onClick( DialogInterface dialog,
																						int which) {
																					// here you can add functions
																				}
																			});
																			message.show();
																			Looper.loop();
																		}
																	} catch (InterruptedException e) {
																		e.printStackTrace();
																	}
																}
															}).start();
															Looper.loop();
														}}

												}
												else{
													if(progressDialog!=null){
														if (progressDialog.isShowing()) {
															progressDialog.dismiss();
														}}
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
												// TODO Place code for handling the response from MB

											}
										}).start();
									}

								});
								confirmation.setNegativeButton("Cancel", null);
								AlertDialog alertDialog = confirmation.create();
								alertDialog.show();


							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}

						}

					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();

			}
		});


		getECGButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				passwordBuilder = new AlertDialog.Builder(MedicalDevices.this);
				View passwordView = LayoutInflater.from(MedicalDevices.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning); 
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();

					}
				});

				passwordBuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							authCheck = false;
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								Intent openECGLogger = new Intent(MedicalDevices.this, ECGLoggingActivity.class);
								startActivityForResult(openECGLogger, 1);

							}
							else
							{
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}

					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}
		});

		//	FIXME When we have ECG capabilities, this functionality will be enabled here
		// 	and the above will be removed
		//		getECGButton.setOnClickListener(new View.OnClickListener() {
		//
		//			public void onClick(View v) {
		//
		//				((GlobalVar)context.getApplicationContext()).setOperationInProgress(true);
		//
		//				passwordBuilder = new AlertDialog.Builder(MedicalDevices.this);
		//				View passwordView = LayoutInflater.from(MedicalDevices.this).inflate(R.layout.passwordchecker, null);
		//				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
		//				passwordBuilder.setTitle("Password Warning");
		//				passwordBuilder.setIcon(drawable.warning);
		//				passwordBuilder.setView(passwordView);
		//				passwordBuilder.setNegativeButton("Cancel", new OnClickListener() {
		//
		//					@Override
		//					public void onClick(DialogInterface dialog, int which) {
		//						if(passwordChecker.isShowing())
		//							passwordChecker.dismiss();
		//					}
		//				});
		//
		//				passwordBuilder.setPositiveButton("OK", new OnClickListener() {
		//
		//					@Override
		//					public void onClick(DialogInterface dialog, int which) {
		//
		//						if(mEditText.length() == 0) {
		//							authCheck = false;
		//							bpasswordInformer.setMessage("Empty password field, please try again.");
		//							passwordInformer = bpasswordInformer.create();
		//							passwordInformer.show();
		//						}
		//						else {
		//							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
		//							if(mEditText.getText().toString().equals(storedPassword)) {
		//								passwordChecker.dismiss();	
		//
		//								confirmation.setMessage("Please make sure that you are wearing the ECG vest. Press OK when ready.");
		//
		//								confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		//
		//									public void onClick(DialogInterface dialog, int which) {
		//
		//										//It will show this progress dialog when waiting for the initial ECG_STREAM_START_ACK
		//										progressDialog = ProgressDialog.show(MedicalDevices.this, null, "Informing the WAKD device to " +
		//												"release control of the Datalogger", false, true);
		//
		//										new Thread(new Runnable() {
		//
		//											@Override
		//											public void run() {
		//												boolean ackArrived = false;
		//												MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
		//												MsgSimple ecgStreamStartRequest = new MsgSimple(MessageCounter, WakdCommandEnum.ECGSTREAM_START);
		//												for(int i=0;i<3;i++) {
		//													try {
		//														ConnectionService.u.sendDatatoWAKD(ecgStreamStartRequest.encode());
		//														Thread.sleep(5 * 1000);
		//													} catch (IOException e) {
		//														e.printStackTrace();
		//													} catch (InterruptedException e) {
		//														e.printStackTrace();
		//													}
		//
		//													if (incomingID == MessageCounter ) {
		//														ackArrived = true;
		//														break;
		//													}
		//												}
		//
		//
		//												if(ackArrived) {
		//													// This means that the ECG Stream START has been received,
		//													// now it should take some time for the smartphone to wait for the actual release...
		//													if(progressDialog!=null) {
		//														if(progressDialog.isShowing()) {
		//															progressDialog.dismiss();
		//															Looper.prepare();
		//															progressDialog = ProgressDialog.show(MedicalDevices.this, "",
		//																	"Waiting for release of the datalogger", false, true);
		//															Thread runnable = new Thread(new Runnable() {
		//
		//																@Override
		//																public void run() {
		//
		//																	try {
		//																		// This gives time to the WAKD to release the datalogger
		//																		Thread.sleep(10000);
		//																		Log.e("ECG waiting", "sleeping");
		//																	} catch (InterruptedException e) {
		//																		e.printStackTrace();
		//																	}
		//																	if(progressDialog.isShowing())
		//																		progressDialog.dismiss();
		//																	Looper.prepare();
		//																	//FIXME
		//																	if(!ecgAvailable){
		//																		ecgAvailable = true;
		//
		//																		message.setMessage("The WAKD device reports that there is no ECG device connected. please make sure that the device is in close range and charged.");
		//																		message.setButton("OK", new OnClickListener() {
		//
		//																			@Override
		//																			public void onClick(DialogInterface dialog, int which) {
		//																				((GlobalVar)context.getApplicationContext()).setOperationInProgress(false);
		//																			}
		//																		});
		//																		message.show();
		//																		Looper.loop();
		//																	}
		//																	else {
		//																		// This is where I should handle the datalogger connection and rest functionality
		//
		//																		// Option 1: create a separate activity and implement start activity for result --> FINAL
		//																		//																		progressDialog.dismiss();
		//																		Intent openECGLogger = new Intent(MedicalDevices.this, ECGLoggingActivity.class);
		//																		startActivityForResult(openECGLogger, 1);
		//																	}																	
		//																}
		//															});
		//															runnable.start();
		//															Looper.loop();
		//														}
		//													}
		//
		//												}
		//												else {
		//													if(MedicalDevices.this.progressDialog!=null){
		//														if (MedicalDevices.this.progressDialog.isShowing()) {
		//															MedicalDevices.this.progressDialog.dismiss();
		//														}
		//													}
		//													Looper.prepare();
		//													message.setMessage("Your Command did not arrive to WAKD" +
		//															"\n");
		//													message.setButton("OK", new DialogInterface.OnClickListener() {
		//														public void onClick( DialogInterface dialog, int which) {
		//															((GlobalVar)context.getApplicationContext()).setOperationInProgress(false);
		//														}
		//													});
		//													message.show();
		//													Looper.loop();
		//												}
		//											}
		//										}).start();
		//
		//									}
		//								});
		//
		//								confirmation.setNegativeButton("Cancel", new OnClickListener() {
		//
		//									@Override
		//									public void onClick(DialogInterface dialog, int which) {
		//										((GlobalVar)context.getApplicationContext()).setOperationInProgress(false);
		//									}
		//								});
		//								AlertDialog alertDialog = confirmation.create();
		//								alertDialog.show();
		//
		//
		//							}
		//							else
		//							{
		//								bpasswordInformer.setMessage("Incorrect password field, please try again.");
		//								passwordInformer = bpasswordInformer.create();
		//								passwordInformer.show();
		//							}
		//						}
		//
		//
		//
		//					}
		//				});
		//				passwordChecker = passwordBuilder.create();
		//				passwordChecker.show();
		//
		//			}
		//		});

		cancelButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				((GlobalVar)context.getApplicationContext()).setOperationInProgress(false);
				MedicalDevices.this.finish();
			}
		});



	}




	private class FunctionsIncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation;

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(headerFilter)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}

				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}
			}

			// Weight case
			else if(intent.getAction().equals(weightfilter)) {
				final byte[] message  =  intent.getByteArrayExtra("message");

				message[0] = ByteUtils.intToByte(1);  	//sets the recipient
				message[3] = ByteUtils.intToByte(23);	//sets the weiht_dataIdOK
				message[5] = ByteUtils.intToByte(4);	//sets the SP as sender

				byte[] Weight = new byte[] { message[6], message[7] };
				BigInteger bi = new BigInteger(Weight);
				String s = bi.toString(16);

				final double weight  = ((double) Integer.parseInt(s, 16)) / 10;
				final int bodyfat  = unsignedByteToInt(message[8]);
				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				confirmation.setTitle("Weight Info Arrived").setMessage(
						"Current \n Weight :  "
								+ weight + " Kg \n Body Fat :"
								+ bodyfat + " %");
				confirmation.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,	int which) {

						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
						Log.d(" Steilame "," Steilame me = " + MessageCounter);
						message[4] = ByteUtils.intToByte(MessageCounter);
						MessageConverter converter = new MessageConverter();
						Log.d(" send weight Ok "," send weight ok = " + converter.BytesToString(message));
						try {
							ConnectionService.u.sendDatatoWAKD(message);
//							createClientforStoreWeight(MedicalDevices.this, message);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
				});
				confirmation.setNegativeButton("No", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog,	int which) {
						try {
							correctWeightData = new AlertDialog.Builder(MedicalDevices.this);
							LayoutInflater li = LayoutInflater.from(MedicalDevices.this);
							correctWeight = li.inflate(R.layout.correctweight, null);
							weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
							bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
							weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
							bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
							correctWeightData.setView(correctWeight);
							weightInput.setText(Double.toString(weight));
							bodyFatInput.setText(Integer.toString(bodyfat));
							correctWeightData.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Log.d("!!!!!!!!!!!"," weight = "+  (int) (Double.parseDouble(weightInput.getText().toString()) * 10) +" kai bodyFat " +  (int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
									System.arraycopy(ByteUtils.intToByteArray((int) (Double.parseDouble(weightInput.getText().toString()) * 10), 2), 0, message, 6, 2);
									message[8] = ByteUtils.intToByte((int) (Double.parseDouble(bodyFatInput.getText().toString()) * 10));
									WakdCommandEnum command = WakdCommandEnum.WEIGHT_DATA;
									MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
									MsgWeightDataOk weightDataOK = new MsgWeightDataOk(MessageCounter,command, (int )weight*10,(int) bodyfat * 10, 900);
									Log.d(" correct weight "," correct weight  = " + weightConverter.BytesToString(message));
									try {
										ConnectionService.u.sendDatatoWAKD(message);
//										createClientforStoreWeight(MedicalDevices.this, message);
									} catch (IOException e) {
										e.printStackTrace();
									}
								}
							});

							correctWeightData.setNegativeButton("No",null);
							correctWeightData.show();

						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}

					} 
				});
				confirmation.show();
			}

			// Blood Pressure case
			else if(intent.getAction().equals(bpfilter)) {
				Bundle bp_bundle = intent.getBundleExtra("blood_pressure");
				Double systolic_bp = bp_bundle.getDouble("systolic_bp");
				Double diastolic_bp = bp_bundle.getDouble("diastolic_bp");
				Double heartrate_bp = bp_bundle.getDouble("heartrate_bp");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				/*
				 *  TODO Place code to update appropriate text fields here
				 */

				AlertDialog.Builder bpBuilder = new Builder(MedicalDevices.this);
				bpBuilder.setTitle("Blood Pressure Results");
				bpBuilder.setMessage("Systolic : " +systolic_bp+
						"\nDiastolic : " + diastolic_bp+
						"\nHeart Rate : " + heartrate_bp);

				bpBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage = bpBuilder.create();
				bpmessage.show();

			}
			// ECG case
			else if(intent.getAction().equals(ecgfilterStop)) {
				if(progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				progressDialog = ProgressDialog.show(MedicalDevices.this, null,
						"Informing the WAKD to resume reception from the ECG Device", false, false);

				new Thread(new Runnable() {

					@Override
					public void run() {
						boolean ackArrived = false;
						MessageCounter = ((GlobalVar)getApplication()).getMessageCounter();
						MsgSimple ecgStreamStopRequest = new MsgSimple(MessageCounter, WakdCommandEnum.ECGSTREAM_STOP);
						for(int i=0;i<3;i++) {
							try {
								ConnectionService.u.sendDatatoWAKD(ecgStreamStopRequest.encode());
								Thread.sleep(5 * 1000);
							} catch (IOException e) {
								e.printStackTrace();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							if (incomingID == MessageCounter ) {
								ackArrived = true;
								break;
							}
						}

						if(ackArrived) {
							// This means that the ECG Stream START has been received,
							// now it should take some time for the smartphone to wait for the actual release...
							if(progressDialog!=null) {
								if(progressDialog.isShowing()) {
									progressDialog.dismiss();
									Looper.prepare();
									message.setMessage("The WAKD has been informed to resume periodic ECG reception");
									message.setButton("OK", new DialogInterface.OnClickListener() {
										public void onClick( DialogInterface dialog, int which) {

										}
									});
									message.show();
									Looper.loop();
								}
							}

						}
						else {
							if(MedicalDevices.this.progressDialog!=null){
								if (MedicalDevices.this.progressDialog.isShowing()) {
									MedicalDevices.this.progressDialog.dismiss();
								}
							}
							Looper.prepare();
							message.setMessage("Unable to verify that the WAKD has been informed to resume periodic ECG reception");
							message.setButton("OK", new DialogInterface.OnClickListener() {
								public void onClick( DialogInterface dialog, int which) {

								}
							});
							message.show();
							Looper.loop();
						}		
					}
				}).start();

			}
			// Case of ACK
			else if(intent.getAction().equals(ackFilter)) {
				if(intent.hasExtra("messageCount")) {
					incomingID = intent.getIntExtra("messageCount", 0);
					Log.d("incoming ID","IncomingID has been set ");
				}
			}
			else if(intent.getAction().equals(attachedECGFilter)) {
				Bundle bp_bundle = intent.getBundleExtra("status");
				Boolean attached = bp_bundle.getBoolean("attached");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				/*
				 *  TODO Place code to update appropriate text fields here
				 */

				AlertDialog.Builder bpBuilder = new Builder(MedicalDevices.this);
				bpBuilder.setTitle("ECG Attachment Results");
				if(attached)
					bpBuilder.setMessage("Attachment was successful");
				else
					bpBuilder.setMessage("Attachment failed, please repeat the process.");

				bpBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage = bpBuilder.create();
				bpmessage.show();
			}
			else if(intent.getAction().equals(attachedBPFilter)) {
				Bundle bp_bundle = intent.getBundleExtra("status");
				Boolean attached = bp_bundle.getBoolean("attached");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				/*
				 *  TODO Place code to update appropriate text fields here
				 */

				AlertDialog.Builder bpBuilder = new Builder(MedicalDevices.this);
				bpBuilder.setTitle("BP Attachment Results");
				if(attached)
					bpBuilder.setMessage("Attachment was successful");
				else
					bpBuilder.setMessage("Attachment failed, please repeat the process.");

				bpBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage = bpBuilder.create();
				bpmessage.show();
			}
			else if(intent.getAction().equals(attachedWSFilter)) {
				Bundle bp_bundle = intent.getBundleExtra("status");
				Boolean attached = bp_bundle.getBoolean("attached");

				if(progressDialog!=null){
					if (progressDialog.isShowing()) {
						progressDialog.dismiss();
					}
				}

				/*
				 *  TODO Place code to update appropriate text fields here
				 */

				AlertDialog.Builder bpBuilder = new Builder(MedicalDevices.this);
				bpBuilder.setTitle("WS Attachment Results");
				if(attached)
					bpBuilder.setMessage("Attachment was successful");
				else
					bpBuilder.setMessage("Attachment failed, please repeat the process.");

				bpBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bpmessage.dismiss();
					}
				});
				bpmessage = bpBuilder.create();
				bpmessage.show();
			}		

		}
	}


	private void initializeHeader() {

		this.receiver = new FunctionsIncomingReceiver();
		intentFilter = new IntentFilter(headerFilter);
		intentFilter.addAction(ackFilter);
//		intentFilter.addAction(weightfilter);
//		intentFilter.addAction(bpfilter);
		intentFilter.addAction(ecgfilterStart);
		intentFilter.addAction(ecgfilterStop);
		intentFilter.addAction(attachedBPFilter);
		intentFilter.addAction(attachedECGFilter);
		intentFilter.addAction(attachedWSFilter);
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(MedicalDevices.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(MedicalDevices.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)MedicalDevices.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});

	}



	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	@Override
	public void onDestroy() {
		if(ecgMessageDialog != null)
			if(ecgMessageDialog.isShowing())
				ecgMessageDialog.dismiss();
		unregisterReceiver(this.receiver);
		super.onDestroy();

	}

//	private void createClientforStoreWeight(final Context context, final byte[] incomingMessage) {
//
//		class SoapWAKDStatusClient implements Runnable {
//
//			String response;
//			@Override
//			public void run() {
//				if(InternetConnectivityStatus.getInstance(context).isOnline(context)) {
//					NephronSOAPClient soapClient = new NephronSOAPClient(context);
//					String strCommand = DataTransformationUtils.convertByteArrayToString(incomingMessage);
//					response = soapClient.executeWeightRequest(strCommand);
//					if(response != null) {
//						if(!response.equalsIgnoreCase("OK"))
//							((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(incomingMessage);
//					}
//					else
//						((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(incomingMessage);
//				}
//				else 
//					// COMMANDS ARE ADDED TO GLOBALVAR ARRAYLIST
//					((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().add(incomingMessage);
//			}
//		}
//		Thread thread = new Thread(new SoapWAKDStatusClient());
//		thread.start();
//	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if(progressDialog !=null)
			if(progressDialog.isShowing())
				progressDialog.dismiss();

		ecgReturnMessage = "";
		ecgTitlemessage = "";

		ecgbuilder = new Builder(MedicalDevices.this);

		if(data != null) {

			if (requestCode == 1) {

				if(resultCode == RESULT_OK) {

					if(data.hasExtra("result")) {
						ecgTitlemessage = "ECG Reception successful.";
						ecgbuilder.setIcon(R.drawable.success);
					}

				}
				else if (resultCode == RESULT_CANCELED) {
					ecgTitlemessage = "ECG Reception unsuccessful.";
					ecgbuilder.setIcon(R.drawable.erroricon);
					if(data.hasExtra("result")) {
						Log.i("NEPHRON", "ecg activity did not end well");
					}
				}

				if(data.hasExtra("message")) {
					ecgReturnMessage = data.getStringExtra("message");
					Log.i("NEPHRON", ecgReturnMessage);
				}

				ecgbuilder.setTitle(ecgTitlemessage);
				ecgbuilder.setMessage(ecgReturnMessage);
				ecgbuilder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						//TODO this is empty cause we just need to close the Dialog
					}
				});

				//****************************************************************************************************

				// FIXME Enable this thread when the ECG full functionality is implemented

				//				Thread ecgStreamStopIntent = new Thread(new Runnable() {
				//
				//					@Override
				//					public void run() {
				//						try {
				//							Thread.sleep(2000);
				//						} catch (InterruptedException e) {
				//							e.printStackTrace();
				//						}
				//
				//						Intent ecgStopIntent = new Intent(ecgfilterStop);
				//						sendBroadcast(ecgStopIntent);
				//					}
				//				});
				//				ecgStreamStopIntent.start();

				//****************************************************************************************************
				// FIXME Disable this here when the ECG full functionality is implemented
				ecgMessageDialog = ecgbuilder.create();
				ecgMessageDialog.show();

				//****************************************************************************************************

			}

		}
		else
			Log.e("RECEIVED RESULT", "no data was received");

	}//onActivityResult







}