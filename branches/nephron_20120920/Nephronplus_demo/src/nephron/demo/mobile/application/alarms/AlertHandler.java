package nephron.demo.mobile.application.alarms;

import java.io.Serializable;
import java.util.List;

import nephron.demo.mobile.application.ConnectionService;
import nephron.demo.mobile.application.GlobalVar;
import android.content.Context;
import android.content.Intent;

public class AlertHandler extends Thread implements Serializable {

	private static final long serialVersionUID = -3878158368732984364L;

	Context context;

	public static List<AlarmsMsg> alerts;

	public AlertHandler(Context y) {
		this.context = y;
	}

	public void run() {

		while (ConnectionService.alive) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				new Thread(new AlertHandler(context)).start();
				e.printStackTrace();
			}

			//Handler will trigger only when the dialog window is not open...
			if( !((GlobalVar)context.getApplicationContext()).isAlarmWindowOpen() ) {

				if (!((GlobalVar)context.getApplicationContext()).getNewAlarmsList().isEmpty() ) {
					((GlobalVar)context.getApplicationContext()).setAlarmWindowOpen(true);
					AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getNewAlarmsList());
					Intent alarmIntent = new Intent("android.intent.action.MAIN");
					alarmIntent.setClass(context, MyAlertDialog.class);
					alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					alarmIntent.setAction("NEW_ALARM");
					alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
					alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
					context.startActivity(alarmIntent);
				}
				else
					if(!((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList().isEmpty()) {
						((GlobalVar)context.getApplicationContext()).setAlarmWindowOpen(true);
						AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)context.getApplicationContext()).getArchivedAlarmsList());

						Intent alarmIntent = new Intent("android.intent.action.MAIN");
						alarmIntent.setClass(context, MyAlertDialog.class);
						alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						alarmIntent.setAction("ARCHIVED_ALARM");
						alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
						alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
						context.startActivity(alarmIntent);
					}
			}

		}
	}

}
