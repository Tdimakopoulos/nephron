package nephron.demo.mobile.application;

import java.io.File;
import java.util.concurrent.CopyOnWriteArrayList;

import nephron.demo.mobile.application.alarms.AlarmsMsg;
import android.app.Application;
import android.content.Intent;
import de.imst.nephron.bt.BluetoothAbstractionLayer;

public class GlobalVar extends Application {

	// Central Location for specific values
	
	// Retrieve Remote Command ASync task period (in ms)
	private long retrieveRCommandPeriod = 30000;
	
	// Rest of the Variables
	private int messageCounter = 1;
	
	private BluetoothAbstractionLayer u;
	
	private boolean ConnectionServiceStarted = false;
	
	private boolean UserAuthenticated = false;
	
	private boolean BackendAuthenticated = false;
	
	private NephronApplicationStatusHandler applicationStatushandler;
	
	private boolean alarmWindowOpen;
	
	private boolean OperationInProgress = false;
	
	private File ecgFile;

	private CopyOnWriteArrayList<AlarmsMsg> newAlarmsList = new CopyOnWriteArrayList<AlarmsMsg>();
	
	private CopyOnWriteArrayList<AlarmsMsg> archivedAlarmsList = new CopyOnWriteArrayList<AlarmsMsg>();
	
	private CopyOnWriteArrayList<byte[]> untransmittedCommandsList = new CopyOnWriteArrayList<byte[]>();
	
	public int getMessageCounter() {
		return messageCounter++;
	}
	
	public void sendIntentForBluetoothConnectionEnabled() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_ON"));
	}
	
	public void sendIntentForBluetoothConnectionDisabled() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_OFF"));
	}
	
	public void sendIntentForBluetoothConnectionTooManyPaired() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_TOO_MANY_PAIRED"));
	}
	
	public void sendIntentForBluetoothConnectionNoPaired() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_NO_PAIRED"));
	}
	

	public BluetoothAbstractionLayer getBluetoothAbstractionLayer() {
		return u;
	}
	
	public void setBluetoothAbstractionLayer(BluetoothAbstractionLayer u) {
		this.u=u;
	}

	public boolean isConnectionServiceStarted() {
		return ConnectionServiceStarted;
	}

	public void setConnectionServiceStarted(boolean connectionServiceStarted) {
		ConnectionServiceStarted = connectionServiceStarted;
	}

	public boolean isUserAuthenticated() {
		return UserAuthenticated;
	}

	public void setUserAuthenticated(boolean userAuthenticated) {
		UserAuthenticated = userAuthenticated;
	}

	public boolean isBackendAuthenticated() {
		return BackendAuthenticated;
	}

	public void setBackendAuthenticated(boolean backendAuthenticated) {
		BackendAuthenticated = backendAuthenticated;
	}

	public NephronApplicationStatusHandler getApplicationStatushandler() {
		return applicationStatushandler;
	}

	public void setApplicationStatushandler(NephronApplicationStatusHandler applicationStatushandler) {
		this.applicationStatushandler = applicationStatushandler;
	}

	public boolean isAlarmWindowOpen() {
		return alarmWindowOpen;
	}

	public void setAlarmWindowOpen(boolean alarmWindowOpen) {
		this.alarmWindowOpen = alarmWindowOpen;
	}

	public CopyOnWriteArrayList<AlarmsMsg> getNewAlarmsList() {
		return newAlarmsList;
	}

	public void setNewAlarmsList(CopyOnWriteArrayList<AlarmsMsg> newAlarmsList) {
		this.newAlarmsList = newAlarmsList;
	}

	public CopyOnWriteArrayList<AlarmsMsg> getArchivedAlarmsList() {
		return archivedAlarmsList;
	}

	public void setArchivedAlarmsList(CopyOnWriteArrayList<AlarmsMsg> archivedAlarmsList) {
		this.archivedAlarmsList = archivedAlarmsList;
	}

	public boolean isOperationInProgress() {
		return OperationInProgress;
	}

	public void setOperationInProgress(boolean operationInProgress) {
		OperationInProgress = operationInProgress;
	}

	public CopyOnWriteArrayList<byte[]> getUntransmittedCommandsList() {
		return untransmittedCommandsList;
	}

	public void setUntransmittedCommandsList(
			CopyOnWriteArrayList<byte[]> untransmittedCommandsList) {
		this.untransmittedCommandsList = untransmittedCommandsList;
	}

	public long getRetrieveRCommandPeriod() {
		return retrieveRCommandPeriod;
	}

	public void setRetrieveRCommandPeriod(long retrieveRCommandPeriod) {
		this.retrieveRCommandPeriod = retrieveRCommandPeriod;
	}

	public File getEcgFile() {
		return ecgFile;
	}

	public void setEcgFile(File ecgFile) {
		this.ecgFile = ecgFile;
	}

}