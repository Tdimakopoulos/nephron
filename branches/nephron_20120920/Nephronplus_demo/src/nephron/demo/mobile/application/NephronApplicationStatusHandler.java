package nephron.demo.mobile.application;

import android.content.Context;
import android.os.Handler;
import android.widget.Button;
import android.widget.TextView;

public class NephronApplicationStatusHandler extends Handler {

	final int CONNECTION_OK = 0;
	final int CONNECTION_ERROR = 1;
	final int BACKEND_AUTH_OK = 2;
	final int BACKEND_AUTH_ERROR = 3;

	Context context;
	TextView connectionText, backendText, userText;
	Button proceed;

	public NephronApplicationStatusHandler(Context context, TextView connectionText, TextView backendText, TextView userText, Button proceed) {
		super();
		this.context = context;
		this.connectionText = connectionText;
		this.backendText = backendText;
		this.userText = userText;
		this.proceed = proceed;
	}

	public NephronApplicationStatusHandler(Context context) {
		super();
		this.context = context;
	}

	public void handleMessage(android.os.Message msg) {

		switch (msg.what) {

		case CONNECTION_OK:
			this.connectionText.setText("CONNECTED     ");
			break;
		case CONNECTION_ERROR:
			this.connectionText.setText("DISCONNECTED");
			break;
		case BACKEND_AUTH_OK:
			this.backendText.setText("CONNECTED     ");
			break;
		case BACKEND_AUTH_ERROR:
			this.backendText.setText("DISCONNECTED");
			break;
		default:
		}
		
		ShowHideProceedButton();
	}

	public void ShowHideProceedButton() {
		boolean connection;
		connection = ((GlobalVar)(context.getApplicationContext())).isConnectionServiceStarted();
		
		if(connection == true && proceed.getVisibility() == 4) {
			proceed.setVisibility(0);
		}
	}

}