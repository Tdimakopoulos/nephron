package nephron.demo.mobile.application.alarms;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class QueueHandler {
	
	public static AlarmsMsg getLatestAlarmFromQueue(CopyOnWriteArrayList<AlarmsMsg> alarmsList) {
		AlarmsMsg latest = new AlarmsMsg(0, -1);
		
		Iterator<AlarmsMsg> it = (Iterator<AlarmsMsg>) alarmsList.iterator();
//		Log.e("", "AlarmsList size is "+alarmsList.size());
		while(it.hasNext())
	    {
			AlarmsMsg testAlarm = it.next();
			if(latest.getIdDatabase()<testAlarm.getIdDatabase()) {
				latest = testAlarm;
			}
	    }
		return latest;
	}
	
	public static boolean removeAlarmFromQueue(CopyOnWriteArrayList<AlarmsMsg> alarmsList, int alarmID) {
		Iterator<AlarmsMsg> it = (Iterator<AlarmsMsg>) alarmsList.iterator();
//		Log.e("BEFORE REMOVE", "AlarmsList size is "+alarmsList.size());
		while(it.hasNext()){
			
			AlarmsMsg msg = it.next();
			if(alarmID == msg.getIdDatabase()) {
				alarmsList.remove(msg);
//				boolean flag = alarmsList.remove(msg);
//				if(flag == true)
//					Log.e("REMOVE FROM QUEUE", "success");
//				else
//					Log.e("REMOVE FROM QUEUE", "failure");
//				Log.e("AFTER REMOVE", "AlarmsList size is "+alarmsList.size());
				return true;
			}
		}
//		Log.e("", "Now AlarmsList size is "+alarmsList.size());
		return false;
	}
	
	public static AlarmsMsg findAlarmFromQueue(CopyOnWriteArrayList<AlarmsMsg> alarmsList, int alarmID) {
		Iterator<AlarmsMsg> it = (Iterator<AlarmsMsg>) alarmsList.iterator();
		while(it.hasNext()){
			AlarmsMsg testAlarm = it.next();
			if(testAlarm.getIdDatabase() == alarmID)
				return testAlarm;
		}
		return null;
	}

}
