package nephron.demo.mobile.application;


import java.util.LinkedList;

import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MeasurementsTableAlt extends Activity {

	private String filter = "nephron.mobile.application.PhysiologicalMeasurementEvent";
	private String filter2 = "nephron.mobile.application.ECGPeriodicMeasurementEvent";
	private String filter3 = "nephron.mobile.application.PhysicalsensorMeasurementEvent";
	private String filter4 = "CONTROLS_CHANGED";

	private IncomingReceiver receiver;
	private ListView dailyMeasurementsListView, weeklyMeasurementsListView;
	private TextView dailyTableTextView, weeklyTableTextView;
	MeasurementsTableDataAdapter dailyDataAdapter, weeklyDataAdapter;
	LinkedList<MeasurementDatevaluePair> dailyMeasurements, weeklyMeasurements;
	LinearLayout dailyLinear, weeklyLinear;
	int code;
	String head1, head2, weeklyMeasurement, unit="", title;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.measurementstablelayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		initializeHeader();

		//		receiver = new IncomingReceiver();
		//		IntentFilter intentFilter = new IntentFilter(filter);
		//		intentFilter.addAction(filter2);
		//		intentFilter.addAction(filter3);
		//		registerReceiver(receiver, intentFilter);

		Display display = getWindowManager().getDefaultDisplay();
//		int width = display.getWidth(); // deprecated
//		int height = display.getHeight(); // deprecated

		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
			title = b.getString("title");
		}
		
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText(title);

		//TODO Add units for other measurements

		dailyMeasurementsListView = (ListView) findViewById(R.id.DailyMeasurementsListView);
		weeklyMeasurementsListView = (ListView) findViewById(R.id.WeeklyMeasurementsListView);
		dailyLinear = (LinearLayout) findViewById(R.id.DailyLinear);
		weeklyLinear = (LinearLayout) findViewById(R.id.WeeklyLinear);
		dailyTableTextView = (TextView) findViewById(R.id.DailyTableTextView);
		weeklyTableTextView = (TextView) findViewById(R.id.WeeklyTableTextView);
		dailyTableTextView.setText(head1);
		weeklyTableTextView.setText(head2);

//		LinearLayout.LayoutParams dailyparams = (LayoutParams) dailyLinear
//				.getLayoutParams();
//		dailyparams.height = height / 2;
//		dailyLinear.setLayoutParams(dailyparams);

//		LinearLayout.LayoutParams weeklyparams = (LayoutParams) weeklyLinear.getLayoutParams();
//		weeklyparams.height = height / 2;
//		weeklyLinear.setLayoutParams(weeklyparams);

		dailyMeasurements = new LinkedList<MeasurementDatevaluePair>();
		weeklyMeasurements = new LinkedList<MeasurementDatevaluePair>();

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		if (c.getCount() == 0) {
			dailyMeasurements.addFirst(new MeasurementDatevaluePair("No measurements so far.", null));
		}
		while (c.moveToNext()) {
			dailyMeasurements.addFirst(new MeasurementDatevaluePair(c.getString(1), c.getDouble(0)));
		}

		c.close();

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		if (c2.getCount() == 0) {
			weeklyMeasurements.addFirst(new MeasurementDatevaluePair("No measurements so far.", null));
		}
		while (c2.moveToNext()) {
			weeklyMeasurements.addFirst(new MeasurementDatevaluePair(c2.getString(1), c2.getDouble(0)));
		}
		c2.close();


		dailyDataAdapter = new MeasurementsTableDataAdapter(MeasurementsTableAlt.this, code, true, dailyMeasurements);


		dailyMeasurementsListView.setAdapter(dailyDataAdapter);

		weeklyDataAdapter = new MeasurementsTableDataAdapter(MeasurementsTableAlt.this, code, false, weeklyMeasurements);

		weeklyMeasurementsListView.setAdapter(weeklyDataAdapter);
	}

	public class IncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation;

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(filter4)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}


				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}

			}

			else if (intent.getAction().equals(filter)) {
				if(checkIncomingValue(code, true)) {
					
					Log.e("INCOMING PHYSIO DATA","MEAS ARRIVED");
					if (intent.hasExtra("physiologicalSensorData")) {
						Bundle b = intent.getBundleExtra("physiologicalSensorData");
						String dailymeasurementdate = b.getString("measurementDate");
						Double dailymeasurementvalue = null;
						if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN))
							dailymeasurementvalue = b.getDouble("CalciumIn");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT))
							dailymeasurementvalue = b.getDouble("CalciumOut");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN))
							dailymeasurementvalue = b.getDouble("PotassiumIn");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT))
							dailymeasurementvalue = b.getDouble("PotassiumOut");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C))
							dailymeasurementvalue = b.getDouble("OxidationState");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN))
							dailymeasurementvalue = b.getDouble("pHIn");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT))
							dailymeasurementvalue = b.getDouble("pHOut");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_IN))
							dailymeasurementvalue = b.getDouble("TemperatureIn");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_OUT))
							dailymeasurementvalue = b.getDouble("TemperatureOut");

						if (dailyMeasurements.getFirst().getDate().equals("No measurements so far.")) {
							dailyMeasurements.removeFirst();
						}
						dailyMeasurements.addFirst(new MeasurementDatevaluePair(dailymeasurementdate, dailymeasurementvalue));
						dailyDataAdapter.notifyDataSetChanged();
					}
					
				}
				//	PhysiologicalMeasurementEvent
				

			}
			else if(intent.getAction().equals(filter3)) {
				//	PhysicalMeasurementEvent
				if(checkIncomingValue(code, false)) {
					
					if(intent.hasExtra("physicalSensorData")) {
						Bundle b = intent.getBundleExtra("physicalSensorData");
						String dailymeasurementdate = b.getString("measurementDate");
						
						Double dailymeasurementvalue = null;

						if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE))
							dailymeasurementvalue = b.getDouble("pressureDialysate");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_FCO))
							dailymeasurementvalue = b.getDouble("pressureFCO");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE))
							dailymeasurementvalue = b.getDouble("pressureBloodline");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BCO))
							dailymeasurementvalue = b.getDouble("pressureBCO");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN))
							dailymeasurementvalue = b.getDouble("temperatureBloodlineIn");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT))
							dailymeasurementvalue = b.getDouble("temperatureBloodlineOut");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE))
							dailymeasurementvalue = b.getDouble("conductivityDialysate");
						else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
							dailymeasurementvalue = b.getDouble("CondTempDialysate");

						if (dailyMeasurements.getFirst().getDate().equals("No measurements so far.")) {
							dailyMeasurements.removeFirst();
						}
						dailyMeasurements.addFirst(new MeasurementDatevaluePair(dailymeasurementdate, dailymeasurementvalue));
						dailyDataAdapter.notifyDataSetChanged();
					}
					
				}
				
			}
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	private void initializeHeader() {

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);
		intentFilter.addAction(filter4);
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(MeasurementsTableAlt.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(MeasurementsTableAlt.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)MeasurementsTableAlt.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});
	}
	
	private boolean checkIncomingValue(int code, boolean physiological) {
		if(physiological) {
			if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C) ||code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_IN) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMPERATURE_OUT))
					return true;
		}
		else {
			if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_FCO) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BCO) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN) ||code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT) ||
					code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE) || code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
					return true;
		}
		
		return false;	
	}


}
