package nephron.demo.mobile.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import nephron.demo.mobile.application.alarms.AlertHandler;
import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MsgSimple;
import nephron.demo.mobile.datafunctions.MyDataListener;
import nephron.demo.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Nephron extends Activity {

	MyDataListener mylistener;
	AlertDialog alert;
	AlertDialog.Builder alertd;
	WakdCommandEnum command;
	MsgSimple statusRequest;
	final Context context = this;
	AlertDialog shutdownInfoDialog;
	AlertHandler alertHandler;

	AlertDialog dialog;
	Button upfile;

	//	code for header ****************
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Nephron+ Menu");
		
		initializeHeader();


		//		upfile = (Button) findViewById(R.id.uploadfile);
		//		
		//		upfile.setOnClickListener(new OnClickListener() {
		//			
		//			@Override
		//			public void onClick(View arg0) {
		//				
		//				NephronSOAPClient client = new NephronSOAPClient(context/*, nameSpace, URL, soapAction, methodName*/);
		//				String deviceID = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		//				String response = client.executeUploadFileRequest(deviceID, "String Test File for ECG FILE UPLOAD ");
		//				
		//				AlertDialog.Builder builder = new AlertDialog.Builder(context);
		//				builder.setTitle("Upload File request");
		//				builder.setMessage(response);
		//				builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
		//					
		//					@Override
		//					public void onClick(DialogInterface arg0, int arg1) {
		//						dialog.dismiss();
		//					}
		//				});
		//				dialog = builder.create();
		//				dialog.show();
		//				
		//				
		//				
		//			}
		//		});


		//deleteDatabase("nephron");

		// Start Connection Service with WAKD
		//		if(!((GlobalVar)getApplicationContext()).isConnectionServiceStarted()) {
		//			Intent start = new Intent("nephron.mobile.application.ConnectionService");
		//			this.startService(start);
		//		}

	}

	protected void onDestroy() {
		//		try {
		//			backupDatabase();
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		super.onDestroy();
		System.exit(1);
	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {

		switch (view.getId()) {
		case R.id.ButtonMeasurements: // Show list of measurements
			Intent openListOfMeasurements = new Intent(Nephron.this, MeasurementOption.class);
			startActivity(openListOfMeasurements);
			break;
		case R.id.FunctionsButton:	// Show Screen of Functions (Weight etc)
			Intent openFunctions = new Intent(Nephron.this, MedicalDevices.class);
			startActivity(openFunctions);
			break;
		case R.id.WAKStatusButton: // Show WAKD status
			Intent openWakStatus = new Intent(Nephron.this, WakStatus.class);
			startActivity(openWakStatus);
			break;
		case R.id.WAKControlButton: // Show WAKD settings
			Intent openWakControl = new Intent(Nephron.this, WakControl.class);
			startActivity(openWakControl);
			break;
		case R.id.AlarmsButton: // Show list of past Alarms
			Intent openAlarms = new Intent(Nephron.this, Alarms.class);
			startActivity(openAlarms);
			break;
//		case R.id.ScheduleButton: // Show patient schedules
//			startActivity(new Intent(Intent.ACTION_VIEW).setDataAndType(null, nephron.mobile.calendar.CalendarActivity.MIME_TYPE));
//			break;
		case R.id.QuestionnairesButton: // Show questionnaires
			Intent openQuestionnairesControl = new Intent(Nephron.this, Questionnaire.class);
			startActivity(openQuestionnairesControl);
			break;
//		case R.id.ECGLoggerButton:
//			Intent openECGLogger = new Intent(Nephron.this, ECGLoggingActivity.class);
//			startActivityForResult(openECGLogger, 1);
//			break;
//		case R.id.PatientDetailsButton: // Show questionnaires
//			Intent openPatientDetails = new Intent(Nephron.this, PatientDetails.class);
//			startActivity(openPatientDetails);
//			break;
		case R.id.SettingsButton: // Show application settings
			Intent openSettingsControl = new Intent(Nephron.this, Settings.class);
			startActivity(openSettingsControl);
			break;
//		case R.id.HistoryButton: // Show history
//			Intent openHistory = new Intent(Nephron.this, History.class);
//			startActivity(openHistory);
//			break;
		case R.id.ExitImageButton:
			finish();
		default:
			break;
		}
	}

	//Create backup of Database on Smartphone
	public static void backupDatabase() throws IOException {

		//	Open your local db as the input stream
		String inFileName = "/data/data/nephron.mobile.application/databases/nephron";
		File dbFile = new File(inFileName);
		FileInputStream fis = new FileInputStream(dbFile);

		String outFileName = Environment.getExternalStorageDirectory() + "/nephron";

		//	Open the empty db as the output stream
		OutputStream output = new FileOutputStream(outFileName);

		//	transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = fis.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}

		//	Close the streams
		output.flush();
		output.close();
		fis.close();
	}


	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(context);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});
	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				btText = (TextView) findViewById(R.id.bluetoothText);
				btImage = (ImageView) findViewById(R.id.bluetoothicon);
				//TODO ADD functionality for handling bluetooth image
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF"))
					btText.setText("Off");
				else if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON"))
					btText.setText("On");
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}

	
}
