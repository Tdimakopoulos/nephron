package nephron.demo.mobile.application;


import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;
import nephron.demo.mobile.statistics.StatisticsWithAChart;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WAKDPhysiologicalMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = 
		{"Temperature Bloodline Incoming",
		"Temperature Bloodline Outgoing",
		"Pressure Bloodline",
		"Pressure Dialysate",
		"Conductivity Dialysate",
		"Temperature Conductivity Dialysate",
		"Oxidation State"};
	
	SharedPreferences myPrefs;
	private String userSelectedMeasurements[];
	String prefName;
	Bundle b;
	Intent openMeasurements;
	Button backButton;
	ArrayAdapter<String> adapter;
	
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;
	
	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.measurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Physical Measurements");
		
		initializeHeader();

		backButton = (Button) findViewById(R.id.BackButton);
		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical")) {
			openMeasurements = new Intent(WAKDPhysiologicalMeasurements.this, StatisticsWithAChart.class);
		} else {
			openMeasurements = new Intent(WAKDPhysiologicalMeasurements.this, MeasurementsTableAlt.class);
		}

		_measurementsListView = (ListView) findViewById(R.id.MeasurementsListView);

		userSelectedMeasurements = createMeasurementsArray();

		if(userSelectedMeasurements.length == 0) { 
			String[] noSelected = {"No Physiological Measurement has been selected, you may change shown measurements in the Nephron+ Settings menu."};
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayoutnomeas, R.id.backtextview,	noSelected);
		}
		else
			adapter = new ArrayAdapter<String>(this, R.layout.backgroundlayout, R.id.backtextview,	userSelectedMeasurements);

		_measurementsListView.setAdapter(adapter);

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				String unit="";
				// handle the various choices
				String item = adapter.getItem(position);
				if(item.equalsIgnoreCase("Conductivity Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE));
					findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE));
					b.putString("header1", "Real Time Conductivity Dialysate Trends"+unit);
					b.putString("header2", "Daily Conductivity Dialysate\nTrends"+unit);
					b.putString("title", "Conductivity Dialysate");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Pressure Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE));
					b.putString("header1", "Real Time Pressure Dialysate\nTrends"+unit);
					b.putString("header2", "Daily Pressure Dialysate\nTrends"+unit);
					b.putString("title", "Pressure Dialysate");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Conductivity Dialysate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE));
					b.putString("header1", "Real Time Temperature Conductivity Dialysate Trends"+unit);
					b.putString("header2", "Daily Temperature Conductivity Dialysate Trends"+unit);
					b.putString("title", "Temperature Conductivity Dialysate");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Pressure Bloodline")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE));
					b.putString("header1", "Real Time Pressure Bloodline\n Trends"+unit);
					b.putString("header2", "Daily Pressure Bloodline\nTrends"+unit);
					b.putString("title", "Pressure Bloodline");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Bloodline Incoming")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN));
					b.putString("header1", "Real Time Temperature Bloodline\nIn Trends"+unit);
					b.putString("header2", "Daily Temperature Bloodline\nIn Trends"+unit);
					b.putString("title", "Temperature Bloodline In");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Temperature Bloodline Outgoing")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT));
					b.putString("header1", "Real Time Temperature Bloodline\nOut Trends"+unit);
					b.putString("header2", "Daily Temperature Bloodline\nOut Trends"+unit);
					b.putString("title", "Temperature Bloodline out");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Oxidation State")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C));
					unit = findUnitForeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.VITAMIN_C));
					b.putString("header1", "Real Time Oxidation State Trends"+unit);
					b.putString("header2", "Daily Oxidation State Trends"+unit);
					b.putString("title", "Oxidation State");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else{
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage("Unidentified Selection has been made, something has gone wrong");
					_dialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss();
						}
					});
					_dialog.show();
				}
				
			}
		});


		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WAKDPhysiologicalMeasurements.this.finish();
			}
		});

	}

	private String[] createMeasurementsArray() {

		String physicalMeasPref = myPrefs.getString("physicalMeasSelected", "1111111");

		int count = 0;
		for(int i =0; i < physicalMeasPref.length(); i++)
			if(physicalMeasPref.charAt(i) == '1')
				count++;

		String[] userMeasurements = new String[count];
		int index = 0;
		for(int i=0; i<physicalMeasPref.length(); i++) {
			if(physicalMeasPref.charAt(i) == '1'){
				userMeasurements[index] = _measurementsElementArray[i];
				index++;
			}
		}
		return userMeasurements;
	}
	
	
	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(WAKDPhysiologicalMeasurements.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(WAKDPhysiologicalMeasurements.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)WAKDPhysiologicalMeasurements.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(WAKDPhysiologicalMeasurements.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(receiver);
		super.onDestroy();
	}
	
	
	
	private String findUnitForeasurement(int code) {

		String unit = "";

		if (MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PH_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PH_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.VITAMIN_C) {
			unit = "";
		}
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_IN  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_OUT  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.COND_TEMP_DIALYSATE)
			unit = " (�C)";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.HEART_RATE ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.RESPIRATION_RATE)
			unit = " (bpm)";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_DIASTOLIC ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_SYSTOLIC)
			unit = " (mm/Hg)";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_BLOODLINE ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_DIALYSATE)
			unit = " (mm/Hg)";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE)
			unit = " (mS/cm)";
		else
			unit = " (mmol/L)";

		return unit;
	}
	
	

}
