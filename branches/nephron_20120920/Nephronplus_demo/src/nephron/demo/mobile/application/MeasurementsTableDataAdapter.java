package nephron.demo.mobile.application;

import java.util.LinkedList;

import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MeasurementsTableDataAdapter extends ArrayAdapter<MeasurementDatevaluePair> {

	private Context context;
	private LinkedList<MeasurementDatevaluePair> values;
	private int code;
	private LayoutInflater inflater;
	private View rowView;
	TextView value;
	TextView date;
	TextView unit;
	ImageView imageView;
	boolean daily;


	public MeasurementsTableDataAdapter(Context context, int code, boolean daily, LinkedList<MeasurementDatevaluePair> values) {
		super(context, R.layout.measurementstablerowbg, values);
		this.context = context;
		this.values = values;
		this.daily = daily;
		this.setCode(code);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if(values.size() == 0) {
			// if values are zero in length we place a custom row layout
			rowView = inflater.inflate(R.layout.backgroundmeauseremntsrow, parent, false);
			TextView infoText = (TextView) rowView.findViewById(R.id.backtextview);
			infoText.setText("No measurements so far.");

		}
		else if(values.size() == 1) {
			if(values.get(0).getValue() != null) {
				rowView = inflater.inflate(R.layout.measurementstablerowbg, parent, false);
				value = (TextView) rowView.findViewById(R.id.measurementsrowvalue);
				value.setText(String.format("%.1f", values.get(0).getValue()));
				date = (TextView) rowView.findViewById(R.id.measurementsrowdate);
				if(this.daily)
					date.setText(values.get(position).getDate().replace(" ", "\n"));
				else
					date.setText(values.get(position).getDate());
				unit = (TextView) rowView.findViewById(R.id.measurementsrowunit);
				unit.setText(findUnitFormeasurement(code));
			}
			else {
				rowView = inflater.inflate(R.layout.backgroundmeauseremntsrow, parent, false);
				TextView infoText = (TextView) rowView.findViewById(R.id.backtextview);
				infoText.setText("No measurements so far.");
			}
		}
		else {
			rowView = inflater.inflate(R.layout.measurementstablerowbg, parent, false);
			value = (TextView) rowView.findViewById(R.id.measurementsrowvalue);
			value.setText(String.format("%.1f", values.get(position).getValue()));
			date = (TextView) rowView.findViewById(R.id.measurementsrowdate);
			if(this.daily)
				date.setText(values.get(position).getDate().replace(" ", "\n"));
			else
				date.setText(values.get(position).getDate());
			unit = (TextView) rowView.findViewById(R.id.measurementsrowunit);
			unit.setText(findUnitFormeasurement(code));
			imageView = (ImageView) rowView.findViewById(R.id.measurementsrowimage);
			if(position != values.size() - 1) {
				if(values.get(position).getValue() > values.get(position+1).getValue())
					imageView.setImageResource(R.drawable.arrowup);
				else if(values.get(position).getValue() < values.get(position+1).getValue())
					imageView.setImageResource(R.drawable.arrowdown);
				else
					imageView.setImageResource(R.drawable.arrowequal);

			}
		}

		return rowView;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	private String findUnitFormeasurement(int code) {

		String unit = "";

		if (MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PH_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PH_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.VITAMIN_C) {
			unit = "";
		}
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_IN  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_OUT  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.COND_TEMP_DIALYSATE)
			unit = "�C";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.HEART_RATE ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.RESPIRATION_RATE)
			unit = "bpm";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_DIASTOLIC ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_SYSTOLIC)
			unit = "mm/Hg";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_BLOODLINE ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_DIALYSATE)
			unit = "mm/Hg";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE)
			unit = "mS/cm";
		else
			unit = "mmol/L";

		return unit;
	}


} 