package nephron.demo.mobile.application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Timer;

import nephron.demo.mobile.application.R.drawable;
import nephron.demo.mobile.backend.RetrieveBackendCommand;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class NephronCentral extends Activity {

	private Timer timer;
	final Context context = this;
	TextView connectionText, backendText, userText;
	Button proceed, exit;
	NephronApplicationStatusHandler handler;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.nephroncentrallayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		connectionText =  (TextView) findViewById(R.id.ConnectionStatusText);
		connectionText.setText(R.string.WAKDConnectionInitial);

		
		backendText = (TextView) findViewById(R.id.BackendStatusText);
		backendText.setText(R.string.BackendConnectionInitial);

		//	userText = (TextView) findViewById(R.id.UserStatusText);
		proceed = (Button) findViewById(R.id.proceeedApplicationMenu);
		exit = (Button) findViewById(R.id.ExitApplication);

		//TODO Insert code to handle Textview fields concerning connection status when the activity is reloaded

		handler = new NephronApplicationStatusHandler(NephronCentral.this, connectionText, backendText, userText, proceed);
		((GlobalVar)getApplicationContext()).setApplicationStatushandler(handler);
		
//		LostConnectivityCommandManager commandManager = new LostConnectivityCommandManager(context);
//		commandManager.start();

		exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				NephronCentral.this.finish();
				System.exit(0);
			}
		});

		proceed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setCancelable(false);
				builder.setTitle("Authorization Status");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
//						TODO Place the Retrieve Command from backend 
						//	at an appropriate position in the application
						timer = new Timer();
						timer.scheduleAtFixedRate(new RetrieveBackendCommand(NephronCentral.this), 2000, 
								((GlobalVar)getApplicationContext()).getRetrieveRCommandPeriod());
						
						Intent intent = new Intent(NephronCentral.this, Nephron.class);
						startActivity(intent);
					}
				});
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});

				if(((GlobalVar)getApplicationContext()).isConnectionServiceStarted()) {

					if(((GlobalVar)getApplicationContext()).isBackendAuthenticated()) {
						builder.setIcon(drawable.success);
						builder.setMessage("Authorization successfull,\n please proceed.");
					}
					else{
						builder.setIcon(drawable.warning);
						builder.setMessage("No Connection to Backend available, no data will be transferred, proceed at your own risk.");
					}
				}

				AlertDialog dialog = builder.create();
				dialog.show();
			}
		});
		
		

	}

	@Override
	public void onStart() {
		super.onStart();
		Thread backendLogin = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(!(((GlobalVar)getApplicationContext()).isConnectionServiceStarted())) {
					Intent start = new Intent("nephron.demo.mobile.application.ConnectionService");
					startService(start);
				}
			}
		});

		if(!(((GlobalVar)getApplicationContext()).isBackendAuthenticated())) {
			backendLogin.start();	
		}
		
	}

	protected void onDestroy() {
		super.onDestroy();
		try {
			if(timer!=null) {
				timer.cancel();
				timer.purge();
			}
			backupDatabase();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//Create backup of Database on Smartphone
	public static void backupDatabase() throws IOException {

		//	Open your local db as the input stream
		String inFileName = "/data/data/nephron.mobile.application/databases/nephron";
		File dbFile = new File(inFileName);
		FileInputStream fis = new FileInputStream(dbFile);

		String outFileName = Environment.getExternalStorageDirectory() + "/nephron";

		//	Open the empty db as the output stream
		OutputStream output = new FileOutputStream(outFileName);

		//	transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = fis.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}

		//	Close the streams
		output.flush();
		output.close();
		fis.close();
	}

}
