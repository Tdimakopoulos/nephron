package nephron.demo.mobile.application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import nephron.demo.mobile.application.R.drawable;
import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.ByteUtils;
import nephron.demo.mobile.datafunctions.MsgChangeStateCommand;
import nephron.demo.mobile.datafunctions.MsgSimple;
import nephron.demo.mobile.datafunctions.WakdCommandEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class WakControl extends Activity {
	Spinner opState;
	TextView currentState;
	List<String> stateOptions;
	AlertDialog message, passwordChecker, passwordInformer;
	AlertDialog.Builder confirmation,confirmationChangeState,confirmationShutDown,
	correctWeightData,incomingConfirmationChangeState, bpasswordInformer;
	Button changeOpMode, shutDown, cancel;
	EditText weightInput,bodyFatInput;

	String currentWakdState = "", currentWakdOpState = "", storedPassword;
	int wakdStateTo, wakdOpStateTo, wakdStateIs=-1, wakdOpStateIs=-1;
	String selectedStateTo;
	private String filter = "nephron.mobile.application.WakStatusEvent";
	private String filter2 = "CONTROLS_CHANGED";
	private String filter3 = "nephron.mobile.application.ACK";
	private IncomingReceiver receiver;
	private ProgressDialog progressDialog;
	ArrayAdapter<String> dataAdapter1;
	int waitResponse, incomingID, MessageCounter;
	Double st1,st2;
	MessageConverter converter;
	protected Builder passwordBuilder;
	protected EditText mEditText;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wakcontrollayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("WAKD Control");

		initializeHeader();

		converter = new MessageConverter();
		currentState = (TextView) findViewById(R.id.currentState);
		opState = (Spinner) findViewById(R.id.changeTo);
		changeOpMode = (Button) findViewById(R.id.ButtonChangeOperationalMode);
		shutDown = (Button) findViewById(R.id.ButtonShutDown); 
		cancel = (Button) findViewById(R.id.BackButton); 

		// get correct view
		LayoutInflater li = LayoutInflater.from(this);
		View correctWeight = li.inflate(R.layout.correctweight, null);
		weightInput = (EditText) correctWeight.findViewById(R.id.WeightUserInput);
		bodyFatInput = (EditText) correctWeight.findViewById(R.id.BodyFatUserInput);
		weightInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		bodyFatInput.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

		stateOptions = new ArrayList<String>();
		confirmation = new AlertDialog.Builder(this);
		confirmationChangeState = new AlertDialog.Builder(this);
		correctWeightData = new AlertDialog.Builder(this);
		incomingConfirmationChangeState = new AlertDialog.Builder(this);
		correctWeightData.setView(correctWeight);
		message = new AlertDialog.Builder(this).create();
		st1= ConnectionService.db.getMeasurement(41);
		st2= ConnectionService.db.getMeasurement(42);

		if(st1!=null && st2!=null){
			wakdStateIs = st1.intValue();
			wakdOpStateIs = st2.intValue();
			setState(wakdStateIs, wakdOpStateIs);
		}


		//		dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, stateOptions);
		dataAdapter1 = new ArrayAdapter<String>(this, R.layout.my_spinner_layout, stateOptions);
		dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		opState.setAdapter(dataAdapter1);

		// if no state send Status Request command
		if((wakdStateIs == -1) && (wakdOpStateIs == -1) ){ 
			WakdCommandEnum command = WakdCommandEnum.STATUS_REQUEST;
			MsgSimple  statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
			try {
				ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
				Log.d(" send statusRequest "," send statusRequest cause unknown = " + converter.BytesToString(statusRequest.encode()));
			} catch (IOException e) {
				e.printStackTrace();
			}  
		}

		bpasswordInformer = new AlertDialog.Builder(WakControl.this);
		bpasswordInformer.setCancelable(true);
		bpasswordInformer.setTitle("Password Error");
		bpasswordInformer.setIcon(R.drawable.erroricon);
		bpasswordInformer.setNeutralButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(passwordInformer.isShowing()){
					passwordInformer.dismiss();
				}
			}
		});


		//	Button: Change Operation Mode	************************************
		changeOpMode.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(WakControl.this);
				View passwordView = LayoutInflater.from(WakControl.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();
					}
				});

				passwordBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.length() == 0) {
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();

								if(opState.getSelectedItem()!=null) { 
									selectedStateTo = opState.getSelectedItem().toString();

									if (selectedStateTo.equals("All Stopped")) {
										wakdStateTo = 1;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selectedStateTo.equals("Maintenance")) {
										wakdStateTo = 2;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selectedStateTo.equals("No Dialysate")) {
										wakdStateTo = 3;
										wakdOpStateTo = wakdOpStateIs;

									} else if (selectedStateTo.equals("Dialysis Auto")) {
										wakdStateTo = 4;
										wakdOpStateTo = 1;

									} else if (selectedStateTo.equals("Regen1 Auto")) {
										wakdStateTo = 5;
										wakdOpStateTo = 1;

									} else if (selectedStateTo.equals("Ultrafiltration Auto")) {
										wakdStateTo = 6;
										wakdOpStateTo = 1;

									} else if (selectedStateTo.equals("Regen2 Auto")) {
										wakdStateTo = 7;
										wakdOpStateTo = 1;

									} else if (selectedStateTo.equals("Dialysis Semi-Auto")) {
										wakdStateTo = 4;
										wakdOpStateTo = 2;

									} else if (selectedStateTo.equals("Regen1 Semi-Auto")) {
										wakdStateTo = 5;
										wakdOpStateTo = 2;

									} else if (selectedStateTo
											.equals("Ultrafiltration Semi-Auto")) {
										wakdStateTo = 6;
										wakdOpStateTo = 2;

									} else if (selectedStateTo.equals("Regen2 Semi-Auto")) {
										wakdStateTo = 7;
										wakdOpStateTo = 2;

									}

									confirmationChangeState
									.setTitle("Change State")
									.setMessage(
											"From\n" + currentWakdState + " "
													+ currentWakdOpState + "\nTo\n"
													+ selectedStateTo)
													.setPositiveButton("Yes", dialogClickListener)
													.setNegativeButton("No", null).show();
								}
								else {
									confirmationChangeState
									.setTitle("No state Selected")
									.setPositiveButton("Ok", null).show();
								}

							}
							else {
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}
					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}

		});


		//	Button: Shutdown WAKD	************************************
		shutDown.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {

				passwordBuilder = new AlertDialog.Builder(WakControl.this);
				View passwordView = LayoutInflater.from(WakControl.this).inflate(R.layout.passwordchecker, null);
				mEditText = (EditText) passwordView.findViewById(R.id.inputPassword);
				passwordBuilder.setTitle("Password Warning");
				passwordBuilder.setIcon(drawable.warning);
				passwordBuilder.setView(passwordView);

				passwordBuilder.setNegativeButton("Cancel", new AlertDialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(passwordChecker.isShowing())
							passwordChecker.dismiss();
					}
				});


				passwordBuilder.setPositiveButton("OK", new AlertDialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(mEditText.getText().toString() == null) {
							bpasswordInformer.setMessage("Empty password field, please try again.");
							passwordInformer = bpasswordInformer.create();
							passwordInformer.show();
						}
						else {
							storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
							if(mEditText.getText().toString().equals(storedPassword)) {
								passwordChecker.dismiss();


								/*
								 * 
								 */
								confirmation.setMessage("Do you want to Close the Device ?" );
								confirmation.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");

										new Thread(new Runnable() {
											public void run() {
												WakdCommandEnum command = WakdCommandEnum.SHUTDOWN;
												MsgSimple shutDown;
												boolean ackArrived = false;
												MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
												for(int i=0;i<3;i++){
													shutDown = new MsgSimple(MessageCounter, command);
													Log.d(" Prospatheia Count "," Prospatheia Number = "+ i +" kai MessageCounter = " + MessageCounter + " incomingID = " + incomingID);
													Log.d(" send shutDown "," send shutDown = " + converter.BytesToString(shutDown.encode()));
													try {
														ConnectionService.u.sendDatatoWAKD(shutDown.encode());												 
														Thread.sleep(5 * 1000);
													} catch (IOException e) {
														e.printStackTrace();
													} catch (InterruptedException e) {
														e.printStackTrace();
													}

													if (incomingID == MessageCounter ) {
														ackArrived = true ;
														break;
													}										 
												}

												if(progressDialog!=null){
													if (progressDialog.isShowing()) {
														progressDialog.dismiss();
													}
												}

												if(ackArrived){
													Looper.prepare();
													message.setMessage("Your Command Arrived Successfully");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
												else{
													Looper.prepare();
													message.setMessage("Your Command did not\narrive to WAKD");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											}
										}).start();
									}
								});
								confirmation.setNegativeButton("No",null);
								confirmation.show();
							}
							else {
								bpasswordInformer.setMessage("Incorrect password, please try again.");
								passwordInformer = bpasswordInformer.create();
								passwordInformer.show();
							}
						}
					}
				});

				passwordChecker = passwordBuilder.create();
				passwordChecker.show();
			}
		});

		//	Button: Cancel, return from  WAKD Control Screen	************************************
		cancel.setOnClickListener(new OnClickListener() {
			//	        	@Override
			public void onClick(View v) { 
				finish();
			}

		});


	}




	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

		//		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				// send Ack
				
				waitResponse=1;

				progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");
				new Thread(new Runnable() {
					public void run() {
						WakdCommandEnum command = WakdCommandEnum.CHANGE_STATE_FROM_TO;
						MsgChangeStateCommand changeState ; 
						boolean ackArrived = false;
						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();

						createWakdStatusCommand(wakdStateTo, wakdOpStateTo );
						ackArrived = true ;
						
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						if(ackArrived){
							if(progressDialog!=null){
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
									Looper.prepare();
									progressDialog = ProgressDialog.show(WakControl.this, "",
											"Your Command Arrived Successfully\nPlease wait for Response to come.");
									new Thread(new Runnable() {
										public void run() {
											Log.d("sekinise to thread ","xeinise to thread");
											try {
												Thread.sleep(7 * 1000);
												if (progressDialog.isShowing()) {
													progressDialog.dismiss();
													Looper.prepare();
													message.setMessage("Nothing Happened");
													message.setButton("OK",
															new DialogInterface.OnClickListener() {
														public void onClick(
																DialogInterface dialog,
																int which) {
															// here you can add functions
														}
													});
													message.show();
													Looper.loop();
												}
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
										}
									}).start();
									Looper.loop();
								}}	
						}
						else{
							if(progressDialog!=null){
								if (progressDialog.isShowing()) {
									progressDialog.dismiss();
								}}
							Looper.prepare();
							message.setMessage("Your Command did not\narrived to WAKD");
							message.setButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							message.show();
							Looper.loop();
						}

					}
				}).start();

				break;

			case DialogInterface.BUTTON_NEGATIVE:
				break;
			}
		}
	};






	//	DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
	//
	//		//		@Override
	//		public void onClick(DialogInterface dialog, int which) {
	//			switch (which) {
	//			case DialogInterface.BUTTON_POSITIVE:
	//				// send Ack
	//				waitResponse=1;
	//
	//				Log.d("Efuge paketo me id", "wakdStateTo = " + wakdStateTo + " kai wakdOpStateTo = " + wakdOpStateTo);
	//				progressDialog = ProgressDialog.show(WakControl.this, "", "Please wait...");
	//				new Thread(new Runnable() {
	//					public void run() {
	//						WakdCommandEnum command = WakdCommandEnum.CHANGE_STATE_FROM_TO;
	//						MsgChangeStateCommand changeState ; 
	//						boolean ackArrived = false;
	//						MessageCounter =  ((GlobalVar) getApplication()).getMessageCounter();
	//						for(int i=0;i<3;i++){
	//							changeState = new MsgChangeStateCommand(MessageCounter,
	//									command, wakdStateIs, wakdOpStateIs, wakdStateTo, wakdOpStateTo);
	//
	//							Log.d(" Prospatheia Count "," Prospatheia Number = "+ i +" kai MessageCounter = " + MessageCounter + " incomingID = " + incomingID);
	//
	//							Log.d(" send change State "," send change State = " + converter.BytesToString(changeState.encode()));
	//							try {
	//								ConnectionService.u.sendDatatoWAKD(changeState.encode());												 
	//								Thread.sleep(5 * 1000);
	//							} catch (IOException e) {
	//								e.printStackTrace();
	//							} catch (InterruptedException e) {
	//								e.printStackTrace();
	//							}
	//
	//							if (incomingID == MessageCounter ) {
	//								ackArrived = true ;
	//								break;
	//							}	
	//
	//						}
	//
	//						if(ackArrived){
	//							if(progressDialog!=null){
	//								if (progressDialog.isShowing()) {
	//									progressDialog.dismiss();
	//									Looper.prepare();
	//									progressDialog = ProgressDialog.show(WakControl.this, "",
	//											"Your Command Arrived Successfully\nPlease wait for Response to come.");
	//									new Thread(new Runnable() {
	//										public void run() {
	//											Log.d("sekinise to thread ","xeinise to thread");
	//											try {
	//												Thread.sleep(7 * 1000);
	//												if (progressDialog.isShowing()) {
	//													progressDialog.dismiss();
	//													Looper.prepare();
	//													message.setMessage("Nothing Happened");
	//													message.setButton("OK",
	//															new DialogInterface.OnClickListener() {
	//														public void onClick(
	//																DialogInterface dialog,
	//																int which) {
	//															// here you can add functions
	//														}
	//													});
	//													message.show();
	//													Looper.loop();
	//												}
	//											} catch (InterruptedException e) {
	//												e.printStackTrace();
	//											}
	//										}
	//									}).start();
	//									Looper.loop();
	//								}}	
	//						}
	//						else{
	//							if(progressDialog!=null){
	//								if (progressDialog.isShowing()) {
	//									progressDialog.dismiss();
	//								}}
	//							Looper.prepare();
	//							message.setMessage("Your Command did not\narrived to WAKD");
	//							message.setButton("OK",
	//									new DialogInterface.OnClickListener() {
	//								public void onClick(
	//										DialogInterface dialog,
	//										int which) {
	//									// here you can add functions
	//								}
	//							});
	//							message.show();
	//							Looper.loop();
	//						}
	//
	//					}
	//				}).start();
	//
	//				break;
	//
	//			case DialogInterface.BUTTON_NEGATIVE:
	//				break;
	//			}
	//		}
	//	};


	public class IncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation;

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(filter2)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}


				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}

			}
			else if (intent.getAction().equals(filter)) {
				if (intent.hasExtra("currentWakdState")) {
					Bundle b = intent.getBundleExtra("currentWakdState");
					int newWakdStateIs = b.getInt("wakdStateIs");
					int newWakdOpStateIs = b.getInt("wakdOpStateIs");
					if(progressDialog!=null){
						if (progressDialog.isShowing()) {
							progressDialog.dismiss();
						}}
					//this part is used when the process is initiated by the SP
					if(waitResponse==1){
						if (wakdStateTo == newWakdStateIs
								&& wakdOpStateTo == newWakdOpStateIs) {
							incomingConfirmationChangeState.setIcon(R.drawable.success);
							incomingConfirmationChangeState.setTitle("WAKD Response").setMessage(
									"Everything OK!\nCurrent State \n"
											+ wakdState(2, newWakdStateIs) + " "
											+ wakdState(1, newWakdOpStateIs));
							incomingConfirmationChangeState.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							stateOptions.clear();
							setState(newWakdStateIs, newWakdOpStateIs);

							dataAdapter1.notifyDataSetChanged();
						} else {
							incomingConfirmationChangeState.setIcon(R.drawable.warning);
							incomingConfirmationChangeState.setTitle("WAKD Response").setMessage(
									"Not OK!\nCurrent State \n"
											+ wakdState(2, newWakdStateIs) + " "
											+ wakdState(1, newWakdOpStateIs));
							incomingConfirmationChangeState.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// here you can add functions
								}
							});
							stateOptions.clear();

							setState(newWakdStateIs, newWakdOpStateIs);
							dataAdapter1.notifyDataSetChanged();
						}
						waitResponse=0;
						wakdStateIs = newWakdStateIs;
						wakdOpStateIs = newWakdOpStateIs;
						incomingConfirmationChangeState.show();
					}
					else{	//this part is used when the process is initiated by the SP

						incomingConfirmationChangeState.setTitle("Current State").setMessage(
								"Current State info Arrived\nCurrent:"
										+ wakdState(2, newWakdStateIs) + " "
										+ wakdState(1, newWakdOpStateIs));
						incomingConfirmationChangeState.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// here you can add functions
							}
						});
						stateOptions.clear();
						// this sets the available options in the spinner
						setState(newWakdStateIs, newWakdOpStateIs);

						wakdStateIs = newWakdStateIs;
						wakdOpStateIs = newWakdOpStateIs;

						dataAdapter1.notifyDataSetChanged();
					}

					//incomingConfirmationChangeState.show();
				}
			}
			else if(intent.getAction().equals(filter3)) {
				Log.d("To iddddddd"," To id  ======== " + intent.getIntExtra("messageCount", 0));
				incomingID = intent.getIntExtra("messageCount", 0);

			}
		}

	}

	public void setState(int wakdState, int wakdOpState) {

		switch (wakdState) {
		case 1: //All Stopped
			stateOptions.add("Maintenance");
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 1);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 2: // Maintenance
			stateOptions.add("All Stopped");
			currentWakdState = wakdState(2, 2);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 3: // No Dialysate
			stateOptions.add("All Stopped");
			stateOptions.add("Dialysis Auto");
			stateOptions.add("Regen1 Auto");
			stateOptions.add("Ultrafiltration Auto");
			stateOptions.add("Regen2 Auto");
			stateOptions.add("Dialysis Semi-Auto");
			stateOptions.add("Regen1 Semi-Auto");
			stateOptions.add("Ultrafiltration Semi-Auto");
			stateOptions.add("Regen2 Semi-Auto");
			currentWakdState = wakdState(2, 3);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 4:  //Dialysis
			switch (wakdOpState) {
			case 1: // Auto
				stateOptions.add("Regen1 Auto");
				//				stateOptions.add("Ultrafiltration Auto");
				//				stateOptions.add("Regen2 Auto");
				break;
			case 2: // Semi-Auto
				//				stateOptions.add("Regen1 Semi-Auto");
				//				stateOptions.add("Ultrafiltration Semi-Auto");
				//				stateOptions.add("Regen2 Semi-Auto");
				break;
			}
			stateOptions.add("All Stopped");
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 4);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 5:  // Regen1
			//			switch (wakdOpState) {
			//			case 1: // Auto
			//				stateOptions.add("Ultrafiltration Auto");
			//				stateOptions.add("Regen2 Auto");
			//				stateOptions.add("Dialysis Auto");
			//				break;
			//			case 2: // Semi-Auto
			//				stateOptions.add("Ultrafiltration Semi-Auto");
			//				stateOptions.add("Regen2 Semi-Auto");
			//				stateOptions.add("Dialysis Semi-Auto");
			//				break;
			//			}
			stateOptions.add("All Stopped");
			stateOptions.add("No Dialysate");
			currentWakdState = wakdState(2, 5);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 6:   //Ultrafiltration
			//			switch (wakdOpState) {
			//			case 1: // Auto
			//				stateOptions.add("Regen2 Auto");
			//				stateOptions.add("Dialysis Auto");
			//				stateOptions.add("Regen1 Auto");
			//				break;
			//			case 2: // Semi-Auto
			//				stateOptions.add("Regen2 Semi-Auto");
			//				stateOptions.add("Dialysis Semi-Auto");
			//				stateOptions.add("Regen1 Semi-Auto");
			//				break;
			//			}
			stateOptions.add("No Dialysate");
			stateOptions.add("All Stopped");
			currentWakdState = wakdState(2, 6);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		case 7:  //Regen2
			//			switch (wakdOpState) {
			//			case 1: // Auto
			//				stateOptions.add("Dialysis Auto");
			//				stateOptions.add("Regen1 Auto");
			//				stateOptions.add("Ultrafiltration Auto");
			//				break;
			//			case 2: // Semi-Auto
			//				stateOptions.add("Dialysis Semi-Auto");
			//				stateOptions.add("Regen1 Semi-Auto");
			//				stateOptions.add("Ultrafiltration Semi-Auto");
			//				break;
			//			}
			stateOptions.add("No Dialysate");
			stateOptions.add("All Stopped");
			currentWakdState = wakdState(2, 7);
			currentWakdOpState = wakdState(1, wakdOpState);
			currentState.setText(currentWakdState + " " + currentWakdOpState);
			break;
		default:
			break;
		}

	}



	//	public void setState(int wakdState, int wakdOpState) {
	//
	//		switch (wakdState) {
	//		case 1: //All Stopped
	//			stateOptions.add("Maintenance");
	//			stateOptions.add("No Dialysate");
	//			currentWakdState = wakdState(2, 1);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 2: // Maintenance
	//			stateOptions.add("All Stopped");
	//			currentWakdState = wakdState(2, 2);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 3: // No Dialysate
	//			stateOptions.add("All Stopped");
	//			stateOptions.add("Dialysis Auto");
	//			stateOptions.add("Regen1 Auto");
	//			stateOptions.add("Ultrafiltration Auto");
	//			stateOptions.add("Regen2 Auto");
	//			stateOptions.add("Dialysis Semi-Auto");
	//			stateOptions.add("Regen1 Semi-Auto");
	//			stateOptions.add("Ultrafiltration Semi-Auto");
	//			stateOptions.add("Regen2 Semi-Auto");
	//			currentWakdState = wakdState(2, 3);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 4:  //Dialysis
	//			switch (wakdOpState) {
	//			case 1: // Auto
	//				stateOptions.add("Regen1 Auto");
	//				stateOptions.add("Ultrafiltration Auto");
	//				stateOptions.add("Regen2 Auto");
	//				break;
	//			case 2: // Semi-Auto
	//				stateOptions.add("Regen1 Semi-Auto");
	//				stateOptions.add("Ultrafiltration Semi-Auto");
	//				stateOptions.add("Regen2 Semi-Auto");
	//				break;
	//			}
	//			stateOptions.add("No Dialysate");
	//			currentWakdState = wakdState(2, 4);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 5:  // Regen1
	//			switch (wakdOpState) {
	//			case 1: // Auto
	//				stateOptions.add("Ultrafiltration Auto");
	//				stateOptions.add("Regen2 Auto");
	//				stateOptions.add("Dialysis Auto");
	//				break;
	//			case 2: // Semi-Auto
	//				stateOptions.add("Ultrafiltration Semi-Auto");
	//				stateOptions.add("Regen2 Semi-Auto");
	//				stateOptions.add("Dialysis Semi-Auto");
	//				break;
	//			}
	//			stateOptions.add("No Dialysate");
	//			currentWakdState = wakdState(2, 5);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 6:   //Ultrafiltration
	//			switch (wakdOpState) {
	//			case 1: // Auto
	//				stateOptions.add("Regen2 Auto");
	//				stateOptions.add("Dialysis Auto");
	//				stateOptions.add("Regen1 Auto");
	//				break;
	//			case 2: // Semi-Auto
	//				stateOptions.add("Regen2 Semi-Auto");
	//				stateOptions.add("Dialysis Semi-Auto");
	//				stateOptions.add("Regen1 Semi-Auto");
	//				break;
	//			}
	//			stateOptions.add("No Dialysate");
	//			currentWakdState = wakdState(2, 6);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		case 7:  //Regen2
	//			switch (wakdOpState) {
	//			case 1: // Auto
	//				stateOptions.add("Dialysis Auto");
	//				stateOptions.add("Regen1 Auto");
	//				stateOptions.add("Ultrafiltration Auto");
	//				break;
	//			case 2: // Semi-Auto
	//				stateOptions.add("Dialysis Semi-Auto");
	//				stateOptions.add("Regen1 Semi-Auto");
	//				stateOptions.add("Ultrafiltration Semi-Auto");
	//				break;
	//			}
	//			stateOptions.add("No Dialysate");
	//			currentWakdState = wakdState(2, 7);
	//			currentWakdOpState = wakdState(1, wakdOpState);
	//			currentState.setText(currentWakdState + " " + currentWakdOpState);
	//			break;
	//		default:
	//			break;
	//		}
	//
	//	}


	public  static String wakdState(int id, int st) {
		String state = "";
		switch (id) {
		case 1:
			switch (st) {
			case 1:
				state = "Auto";
				break;
			case 2:
				state = "Semi-Auto";
				break;
			}
			break;
		case 2:
			switch (st) {
			case 1:
				state = "All Stopped";
				break;
			case 2:
				state = "Maintenance";
				break;
			case 3:
				state = "No Dialysate";
				break;
			case 4:
				state = "Dialysis";
				break;
			case 5:
				state = "Regen1";
				break;
			case 6:
				state = "Ultrafiltration";
				break;
			case 7:
				state = "Regen2";
				break;
			}
			break;

		}
		return state;

	}


	private void initializeHeader() {

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);		
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(WakControl.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(WakControl.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)WakControl.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}



	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}


	private  void createWakdStatusCommand(int wakdStateTo, int wakdOpStateTo) {

		byte[] wakdStatus = new byte[8];

		wakdStatus[0] = ByteUtils.intToByte(4);
		wakdStatus[1] = ByteUtils.intToByte(0);
		wakdStatus[2] = ByteUtils.intToByte(8);
		wakdStatus[3] = ByteUtils.intToByte(4);
		wakdStatus[4] = ByteUtils.intToByte(03);	//Counter
		wakdStatus[5] = ByteUtils.intToByte(1);
		wakdStatus[6] = ByteUtils.intToByte(wakdStateTo);
		wakdStatus[7] = ByteUtils.intToByte(wakdOpStateTo);

		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", wakdStatus);
		WakControl.this.sendBroadcast(physicalIntent);
	}


}
