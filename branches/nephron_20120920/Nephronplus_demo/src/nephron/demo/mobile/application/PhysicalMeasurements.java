package nephron.demo.mobile.application;

import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PhysicalMeasurements extends Activity {
	private ListView _patientmeasListView;
	private static final String _wakStatusElementArray[] = { "Blood Pressure Systolic: -",
		"Blood Pressure Diastolic: -", "Weight: -" };

	private String filter = "nephron.mobile.application.PhysicalMeasurementEvent";
	private String filter2 = "nephron.mobile.application.PhysicalMeasurementEvent2";
	private String filter3 = "CONTROLS_CHANGED";

	IncomingReceiver receiver;
	ArrayAdapter<String> dataAdapter;
	
	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	Button backButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		setContentView(R.layout.wakstatuslayout);
		setContentView(R.layout.measurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Patient Measurements");

		backButton = (Button) findViewById(R.id.BackButton);
		
		initializeHeader();

		_patientmeasListView = (ListView) findViewById(R.id.MeasurementsListView);
		dataAdapter=new ArrayAdapter<String>(this,
				R.layout.backgroundlayoutwithoutarows, R.id.backtextview,
				_wakStatusElementArray);
		_patientmeasListView.setAdapter(dataAdapter);

		Double pressure_systolic = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BLOODP_SYSTOLIC));
		Double pressure_diastolic = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BLOODP_DIASTOLIC));
		Double weight = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.WEIGHT));
		Double bodyfat = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.BODY_FAT));

		if(pressure_systolic != null){
			_wakStatusElementArray[0] = "Blood Pressure Systolic: " + pressure_systolic + " bpm";
		}
		if(pressure_diastolic != null){
			_wakStatusElementArray[1] = "Blood Pressure Diastolic: " + pressure_diastolic + " bpm";
		}
		if (weight != null) {
			_wakStatusElementArray[2] = "Weight: " + weight + " Kg";
		}

		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				PhysicalMeasurements.this.finish();
			}
		});

	}
	
	
	private void initializeHeader() {
		
		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(PhysicalMeasurements.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(PhysicalMeasurements.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)PhysicalMeasurements.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}
	

	private class IncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;
		
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(filter)) {  
				if(intent.hasExtra("weight")) {   
					Bundle b =  intent.getBundleExtra("weight");
					double weight  = b.getDouble("weight");
					_wakStatusElementArray[2] = "Weight: " + weight + " Kg";
					dataAdapter.notifyDataSetChanged();
				}
			}
			else if(intent.getAction().equals(filter2)) {
				if(intent.hasExtra("blood_pressure")) {
					Bundle b = intent.getBundleExtra("blood_pressure");
					double bp_sys = b.getDouble("systolic_bp");
					double bp_dia = b.getDouble("diastolic_bp");
					_wakStatusElementArray[0] = "Blood Pressure Systolic: " + bp_sys + " bpm";
					_wakStatusElementArray[1] = "Blood Pressure Diastolic: " + bp_dia + " bpm";
					dataAdapter.notifyDataSetChanged();
				}
			}
			else if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}
		}
		
		

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}