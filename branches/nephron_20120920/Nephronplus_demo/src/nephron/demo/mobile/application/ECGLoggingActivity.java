package nephron.demo.mobile.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.zip.Deflater;

import nephron.demo.mobile.backend.NephronSOAPClient;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import ch.csem.sew.ISewOsDevice;
import ch.csem.sew.ISewOsDeviceCallBack;

public class ECGLoggingActivity extends Activity
{
	private final String TAG = "SewServiceTest";
	private final Boolean D = true;

	Boolean receptionAchieved = false;
	Boolean deviceConnected = false;
	Boolean transmissionStarted = false;

	Long ecgStartPausePeriod = Long.valueOf(3000);
	Long ecgReceptionDuration = Long.valueOf(120000+ecgStartPausePeriod);

	private final static int MSG_PBAR_UPDATE = 0;
	private final static int MSG_ID_LOGAPPEND	= 1;

	View progressDialogLayoutView;
	AlertDialog.Builder pdialogBuilder;
	AlertDialog pdialog;
//	ProgressBar pdialogBar;
	TextView pdialogText;
	String log_message;

	// File writing related vars
	Long ecgStartTime;
	Timer timer;
	TimerTask fileWriterTask;
	String fileName;


	CopyOnWriteArrayList<String> measurementsList;

	/*Service used to communicate with the sewViewer...*/
	private ISewOsDevice mISewOsDevice = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater= LayoutInflater.from(ECGLoggingActivity.this);
		progressDialogLayoutView = inflater.inflate(R.layout.ecgloggerprogressdialog, null);
		

//		pdialogBar = (ProgressBar) progressDialogLayoutView.findViewById(R.id.ecgProgressBar);

		pdialogText = (TextView) progressDialogLayoutView.findViewById(R.id.ecgLoggerText);

		pdialogBuilder = new AlertDialog.Builder(ECGLoggingActivity.this);
		pdialogBuilder.setView(progressDialogLayoutView);

		pdialogBuilder.setCancelable(true);
		pdialogBuilder.setTitle("ECG Logger");
		pdialogBuilder.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// Considered a failure cause the reception, although maybe started, has not actually completed
				// since the user navigates away from the Activity
				if(timer!=null) {
					timer.cancel();	// this will stop the timer and the timertask will never be executed again
					timer.purge();
				}
				eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
				prepareReturnIntent(false, "User has selected to cancel the operation while in progress.");
			}
		});
		pdialog = pdialogBuilder.create();
		pdialog.show();

		measurementsList = new CopyOnWriteArrayList<String>();

		timer = new Timer();
		fileWriterTask = new TimerTask() {

			@Override
			public void run() {
				try {
					Log.e("file writer task size of list", String.valueOf(measurementsList.size()));
					if(measurementsList.size() > 0) {
						Log.e("FILE WRITER TIMER TASK", "List contains measurements, executing...");
						commitCommandListToFile(ECGLoggingActivity.this, fileName, measurementsList);
					}

				} catch (FileNotFoundException e) {
					Log.e("FILE WRITER TIMER TASK", "FileNotFoundException occured while commiting measurements to file, terminating");
					eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);

					prepareReturnIntent(false, "FileNotFoundException occured while commiting measurements to file, terminating");
					e.printStackTrace();
				} catch (IOException e) {
					Log.e("FILE WRITER TIMER TASK", "IOException occured while commiting measurements to file, terminating");
					eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
					prepareReturnIntent(false, "IOException occured while commiting measurements to file, terminating");
					e.printStackTrace();
				}
				measurementsList.clear();
			}
		};





	}/*onCreate*/

	/*Use a handler to communicate from the call-backs to the main UI thread...*/
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage( Message msg )
		{
			switch (msg.what) {
			default:
				super.handleMessage( msg );
				break;
				//FIXME reenable this if we actually manage to handle the progress bar
//			case MSG_PBAR_UPDATE:
//				if (msg.obj!=null)
//					pdialogBar.setProgress(pdialogBar.getProgress()+(Integer)msg.obj);
//				break;
			case MSG_ID_LOGAPPEND:
				if (msg.obj!=null){
					if(pdialogText.length() != 0)
						pdialogText.append("\n");
					pdialogText.append(" -  " + (String) msg.obj);
				}
				break;
			}
		}};


		@Override
		protected void onStart() {
			super.onStart();

			fileName = checkforFileAndCreate();
			if(fileName != null) {
				if(fileName.length() > 0 ) {



					this.log_message = "ECG Storage successful.";
					mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
//					Integer barMessage = 10;
//					mHandler.sendMessage( mHandler.obtainMessage( MSG_PBAR_UPDATE, barMessage) );	
				}
			}
			else {
				this.log_message = "ECG Storage unsuccessful.";
				mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
				this.log_message = "ECG reception will terminate.";
				mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );

				// The process will terminate here because the creation of file for storing he ECG measurements has failed
				if(timer!=null) {
					timer.cancel();	// this will stop the timer and the timertask will never be executed again
					timer.purge();
				}

				prepareReturnIntent(false, "Error while creating the ECG Measurements file"); // this will terminate the activity as well
				ECGLoggingActivity.this.finish();
			}

			/*Connect to the SEWviewer service... The connection to the device will be initiated once the service is connected!*/
			if (bindService( new Intent("ch.csem.sew.viewer.DeviceCommunicationService"), mServiceConnection, Context.BIND_AUTO_CREATE )) {
				this.log_message = "Connecting to service...";
				mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
//				Integer barMessage = 10;
//				mHandler.sendMessage( mHandler.obtainMessage( MSG_PBAR_UPDATE, barMessage) );
//				Log.e("", log_message);
			}
			else {
				this.log_message = "Bind to service failed!\n";
				mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
//				Integer barMessage = 10;
//				mHandler.sendMessage( mHandler.obtainMessage( MSG_PBAR_UPDATE, barMessage) );

				Log.e("", log_message);
				// This means that the service to be bound is not found and the user should be informed of this.
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						AlertDialog dialog;
						AlertDialog.Builder builder = new Builder(ECGLoggingActivity.this);
						log_message = "Bind to ECG service has failed";
						builder.setTitle("ECG Logger");
						builder.setMessage(log_message);
						builder.setNeutralButton("OK", new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								prepareReturnIntent(false, log_message);
								ECGLoggingActivity.this.finish();
							}
						});
						dialog = builder.create();
						dialog.show();
					}
				});

			}

		}



		@Override
		protected void onStop() {
			disconnectFromDevice();
			unbindService( mServiceConnection );
			super.onStop();
		}



		@Override
		public void onDestroy()
		{
			Log.e("ON DESTROY", "ON DESTROY REACHED");

			if(pdialog != null) {
				if(pdialog.isShowing())	
					pdialog.dismiss();
			}
			super.onDestroy();
		}


		private ServiceConnection mServiceConnection = new ServiceConnection() {
			@Override
			public void onServiceConnected(ComponentName name, IBinder service)
			{
				mISewOsDevice = ISewOsDevice.Stub.asInterface(service);
				if (D){
					Log.e(TAG,"Connected "+mISewOsDevice);
					String log_message = "Connected to service";
					Log.e("", log_message);
					mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
//					Integer barMessage = 10;
//					mHandler.sendMessage( mHandler.obtainMessage( MSG_PBAR_UPDATE, barMessage) );
				}
				try {
					final String defdevicename = mISewOsDevice.getDefaultDeviceName();

					if (defdevicename!=null)

						/* Define the call-backs used to receive the feed-back from the receiving service of sewViwer*/
						/* Note that these call-backs are running from a different thread than the main UI and therefore
						 * cannot directly modify elements of the UI!*/
						mISewOsDevice.registerCallBack( new ISewOsDeviceCallBack.Stub() {
							@Override
							public void onDeviceFirmwareInfo(String platform, int firmware_version, int firmware_revision, int firmware_buildnumber)
									throws RemoteException {

								if(deviceConnected == false) {
									String log_message = "Connected to ECG Device";
									mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
//									Integer barMessage = 10;
//									mHandler.sendMessage( mHandler.obtainMessage( MSG_PBAR_UPDATE, barMessage) );
//									Log.e("ECGLoggingActivity", log_message);

									deviceConnected = true;

									timer.scheduleAtFixedRate(fileWriterTask, 2000, 4000);
									ecgStartTime = new Date().getTime();

								}

							}

							@Override
							public void onBluetoothDisconnected() throws RemoteException {
								String log_message = "Bluetooth disconnected!" ;
								Log.e("", log_message);
								mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ) );
							}

							@Override
							public void onDeviceIdentification(byte[] deviceid)	throws RemoteException {}

							@Override
							public void onBluetoothConnectionFailed() throws RemoteException {
								String log_message = "Bluetooth connection failed!";
								Log.e("ECGLoggingActivity", log_message);
								mHandler.sendMessage( mHandler.obtainMessage( MSG_ID_LOGAPPEND, log_message ));
								if(timer!=null) {
									timer.cancel();	// this will stop the timer and the timertask will never be executed again
									timer.purge();
								}
								eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
								prepareReturnIntent(false, "Bluetooth connection failed!");
							}

							@Override
							public void onWriteAnnotationResult(int result)	throws RemoteException {}

							@Override
							public void onValueProcessedUpdated(String type, double time__seconds, float value) throws RemoteException {}

							@Override
							public void onValueContinuousUpdated(String type, double time__seconds, float value) throws RemoteException {

								if(type.equalsIgnoreCase("SIG_ECG")) {
									if(Long.valueOf(new Date().getTime()) > (ecgStartTime + ecgReceptionDuration)) {
										timer.cancel();	// this will stop the timer and the timertask will never be executed again
										timer.purge();

										// 2) 	Collect the remaining values from the measurementsList and append them to file
										receptionAchieved = true;

										if(!transmissionStarted) {
											transmissionStarted = true;
											try {
												commitCommandListToFile(ECGLoggingActivity.this, fileName, measurementsList);
											} catch (FileNotFoundException e) {
												e.printStackTrace();
											} catch (IOException e) {
												e.printStackTrace();
											}

											Thread compressTransmit = new Thread(new Runnable() {

												@Override
												public void run() {
													log_message = compressAndTransmitFile();
													prepareReturnIntent(receptionAchieved, log_message);
													ECGLoggingActivity.this.finish();
												}
											});

											compressTransmit.start();
										}
									}
									else {
										if(Long.valueOf(new Date().getTime()) > (ecgStartTime + ecgStartPausePeriod))
											measurementsList.add(time__seconds + "$" + value);
										else
											Log.e("ECG STREAM","VALUE NOT ADDED");
									}

								}
							}


						} );
					/*end of new ISewOsDeviceCallBack.Stub*/


					/*Show only the HR and ECG quality signal*/
					mISewOsDevice.setEnableContinuousSignals(true);
					mISewOsDevice.filterChannelEnableAll( true );
					//	mISewOsDevice.filterChannelEnable( "ALG_ECG_HR", true );
					//	mISewOsDevice.filterChannelEnable( "ALG_ECG_Q", true );
					/*Other valid signal names are: 
					 * ECG, RESP, ALG_ECG_HRV, ALG_ACTIVITY_CLASS, ALG_ACTIVITY_PACE, ALG_RESPIRATION_BR*/

					/*Connect to the default device of the sewViewer application*/
					mISewOsDevice.connectToDevice(null);

					/*Connection isn't always successful, so a flag has to be checked again after some time has elapsed
					in order to terminate the activity*/

					Thread deviceConnectionChecker = new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Thread.sleep(10000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							if(deviceConnected)
								Log.e("Device Connection Thread", "Device has been checked to be connected");
							else {
								Log.e("Device Connection Thread", "Device is not responsive, something has gone wrong");
								if(timer!=null) {
									timer.cancel();	// this will stop the timer and the timertask will never be executed again
									timer.purge();
								}
								eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
								prepareReturnIntent(false, "Device is not responding, something has gone wrong. You will have to repeat the ECG measurement process.");
							}

						}
					});
					deviceConnectionChecker.start();

				} catch (RemoteException e) {
					if (D)
						Log.d(TAG,"Remote Exception "+e);
					eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
					prepareReturnIntent(false, "Error while connecting to the device.");
					e.printStackTrace();
				}
			}

			@Override
			public void onServiceDisconnected(ComponentName name)
			{
				mISewOsDevice = null;
				ECGLoggingActivity.this.finish();
			}
		};

		private String checkforFileAndCreate() {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String fileDate = dateFormat.format(new Date());
			String fileSeparator = System.getProperty("file.separator");
			String commandsDir = Environment.getExternalStorageDirectory() + fileSeparator + "Nephron" + fileSeparator + "ECGData";
			File commandsDirectory = new File(commandsDir);
			String fileName = commandsDirectory + fileSeparator + "ECGDiagramData_"+ fileDate +".txt";
			File commandsFile = new File(fileName);

			if(!commandsDirectory.exists()) {
				commandsDirectory.mkdirs();
			}

			if(!commandsFile.exists()) {

				try {
					commandsFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("DIRECTORY STRUCTURE ","EXCEPTION IN CREATE NEW FILE");
					return null;
				}
			}

			if(commandsFile.exists()) {
				((GlobalVar)ECGLoggingActivity.this.getApplicationContext()).setEcgFile(commandsFile);
				return fileName;
			}
			else 
				return null;
		}


		private void commitCommandListToFile(Context ctx, String fileName, CopyOnWriteArrayList<String> ecgvalues) throws IOException, FileNotFoundException  {

			FileOutputStream fOut;
			File commandsFile = new File(fileName);

			fOut = new FileOutputStream(commandsFile, true);

			OutputStreamWriter osw = new OutputStreamWriter(fOut);
			String newLine = System.getProperty("line.separator");

			for(String value : ecgvalues) {
				osw.write(value+newLine);
				osw.flush();
			}
			osw.close();
		}

		public boolean eraseECGFileFromFilesystem(String commandsFileName, Context ctx) {

			File commandsFile = new File(commandsFileName);
			boolean deleted = false;
			if(commandsFile.exists()) {

				deleted = commandsFile.delete();
				if(deleted)
					Log.d("Erase from filesystem", "SUCCESS");
				else
					Log.d("Erase from filesystem", "FAILURE");
			}
			return deleted;
		}

		public void eraseECGFileContents(String commandsFileName, Context ctx) {

			File commandsFile = new File(commandsFileName);

			if(commandsFile.exists()) {

				FileOutputStream fOut;
				try {
					fOut = new FileOutputStream(commandsFile, false);
					OutputStreamWriter osw = new OutputStreamWriter(fOut);
					osw.write("");
					osw.flush();
					osw.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		private void disconnectFromDevice() {

			if (mISewOsDevice != null) {
				try {
					mISewOsDevice.disconnectFromDevice();
					if (D)
						Log.d(TAG,"Disconnecting from device.");
					log_message = "Disconnecting from device." ;
					mHandler.sendMessage( mHandler.obtainMessage(MSG_ID_LOGAPPEND, log_message) );

				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

		}

		private void prepareReturnIntent(Boolean state, String message) {
			((GlobalVar)ECGLoggingActivity.this.getApplicationContext()).setEcgFile(null);
			Intent returnIntent = new Intent();
			returnIntent.putExtra("result", state);
			returnIntent.putExtra("message", message);
			if(state)
				setResult(RESULT_OK, returnIntent);
			else
				setResult(RESULT_CANCELED, returnIntent);
			ECGLoggingActivity.this.finish();
		}

		//***********************************************************************************************************

		private String compressAndTransmitFile() {

			String fileContentsAsString = new String();
			Long zippedContentsLength = Long.valueOf(0);
			Long contentsLength = Long.valueOf(0);
			byte[] container;

			try {
				fileContentsAsString = readFileContents(fileName);
			} catch (IOException e) {
				e.printStackTrace();
				return "Error while reading ECG Measurements from file.";
			}

			if(fileContentsAsString != null) {
				if(fileContentsAsString.length() > 0 ) {
					contentsLength = (long) fileContentsAsString.getBytes().length;
					Log.d("FILE CONTENTS LENGTH", String.valueOf(fileContentsAsString.length()));
					container = new byte[fileContentsAsString.getBytes().length];

					try {
						zippedContentsLength = Long.valueOf(compressString(fileContentsAsString, container, fileContentsAsString.length()));
						Log.d("ZIPPED FILE CONTENTS LENGTH", String.valueOf(zippedContentsLength));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						return "Error while compressing the ECG Measurements file before transmission.";
					}


					if(zippedContentsLength > 0) {

						String response = null;
						try {
							NephronSOAPClient client = new NephronSOAPClient(ECGLoggingActivity.this);
							response = client.executeUploadFileRequest(container, contentsLength, new Date().getTime());
						} catch (Exception e) {
							e.printStackTrace();
							return "Error while uploading the ECG Measurements file to backend.";
						}

						if(response!=null) {

							if(response.equalsIgnoreCase("Saved !!")){
								eraseECGFileFromFilesystem(fileName, ECGLoggingActivity.this);
								return "ECG Measurements have been uploaded successfully.";
							}
							else
								return "Error while saving the ECG Measurements file to backend.";

						}

					}
				}
			}
			else
				return "Reading from file produced a null string";

			return "Unknown error has occured while trying to upload the ECG Measurements file.";
		}

		//***********************************************************************************************************

		private String readFileContents( String fileName ) throws IOException {

			File ecgFile = new File(fileName);
			BufferedReader reader = new BufferedReader( new FileReader (ecgFile));
			String         line = null;
			StringBuilder  stringBuilder = new StringBuilder();
			String         ls = System.getProperty("line.separator");

			while( ( line = reader.readLine() ) != null ) {
				stringBuilder.append( line );
				stringBuilder.append( ls );
			}
			reader.close();

			return stringBuilder.toString();
		}

		//***********************************************************************************************************

		public static int compressString(String data, byte[] output, int len) throws UnsupportedEncodingException {
			Deflater deflater = new Deflater();
			deflater.setInput(data.getBytes("UTF-8"));
			deflater.finish();
			return deflater.deflate(output);
		}

		//***********************************************************************************************************


}