package nephron.demo.mobile.application;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import nephron.demo.mobile.backend.InternetConnectivityStatus;
import nephron.demo.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class MeasurementsTable extends Activity {

	private String filter = "nephron.mobile.application.PhysiologicalMeasurementEvent";
	private String filter2 = "nephron.mobile.application.ECGPeriodicMeasurementEvent";
	private String filter3 = "nephron.mobile.application.PhysicalsensorMeasurementEvent";
	private String filter4 = "CONTROLS_CHANGED";

	private IncomingReceiver receiver;
	private ListView dailyMeasurementsListView, weeklyMeasurementsListView;
	private TextView dailyTableTextView, weeklyTableTextView;
	ArrayAdapter<String> dailyDataAdapter, weeklyDataAdapter;
	LinkedList<String> dailyMeasurements;
	List<String> weeklyMeasurements;
	LinearLayout dailyLinear, weeklyLinear;
	int code;
	String head1, head2, dailyMeasurement, weeklyMeasurement, unit="";

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.measurementstablelayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		initializeHeader();

		//		receiver = new IncomingReceiver();
		//		IntentFilter intentFilter = new IntentFilter(filter);
		//		intentFilter.addAction(filter2);
		//		intentFilter.addAction(filter3);
		//		registerReceiver(receiver, intentFilter);

		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated

		Intent startingIntent = getIntent();
		if (startingIntent != null) {
			Bundle b = startingIntent.getBundleExtra("values");
			if (b != null)
				code = b.getInt("code");
			head1 = b.getString("header1");
			head2 = b.getString("header2");
		}

		if (MeasurementsCodesEnum.getMeasurementsCodesEnum(code) != MeasurementsCodesEnum.PH_IN &&
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) != MeasurementsCodesEnum.PH_OUT &&
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) != MeasurementsCodesEnum.VITAMIN_C) {
			unit = " mmol/L";
		}
		if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_IN ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMPERATURE_OUT ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_IN  ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.TEMP_BLOODLINE_OUT  || 
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.COND_TEMP_DIALYSATE)
			unit = " �C";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_DIASTOLIC ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.BLOODP_SYSTOLIC)
			unit = " mmHg";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_BLOODLINE ||
				MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.PRESSURE_DIALYSATE)
			unit = " Pa";
		else if(MeasurementsCodesEnum.getMeasurementsCodesEnum(code) == MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE)
			unit = " mS/cm";
		else
			unit = "";

		dailyMeasurementsListView = (ListView) findViewById(R.id.DailyMeasurementsListView);
		weeklyMeasurementsListView = (ListView) findViewById(R.id.WeeklyMeasurementsListView);
		dailyLinear = (LinearLayout) findViewById(R.id.DailyLinear);
		weeklyLinear = (LinearLayout) findViewById(R.id.WeeklyLinear);
		dailyTableTextView = (TextView) findViewById(R.id.DailyTableTextView);
		weeklyTableTextView = (TextView) findViewById(R.id.WeeklyTableTextView);
		dailyTableTextView.setText(head1);
		weeklyTableTextView.setText(head2);

		LinearLayout.LayoutParams dailyparams = (LayoutParams) dailyLinear
				.getLayoutParams();
		dailyparams.height = height / 2;
		dailyLinear.setLayoutParams(dailyparams);

		LinearLayout.LayoutParams weeklyparams = (LayoutParams) weeklyLinear.getLayoutParams();
		weeklyparams.height = height / 2;
		weeklyLinear.setLayoutParams(weeklyparams);

		dailyMeasurements = new LinkedList<String>();
		weeklyMeasurements = new ArrayList<String>();

		Cursor c = ConnectionService.db.getTodayMeasurementsUntilNow(code);
		if (c.getCount() == 0) {
			dailyMeasurements.addFirst("No Data Yet");
		}
		while (c.moveToNext()) {
			dailyMeasurement = c.getString(1) + "    " +   String.format("%.1f", c.getDouble(0) )  + unit;
			dailyMeasurements.addFirst(dailyMeasurement);
		}

		c.close();

		Cursor c2 = ConnectionService.db.getWeeklyMeasurements(code);
		while (c2.moveToNext()) {
			weeklyMeasurement = c2.getString(1) + "    " +  String.format("%.1f", c2.getDouble(0) )
					+ unit;
			weeklyMeasurements.add(weeklyMeasurement);
		}
		c2.close();
		
		
		dailyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				dailyMeasurements);

		dailyMeasurementsListView.setAdapter(dailyDataAdapter);

		weeklyDataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundmeauseremntsrow, R.id.backtextview,
				weeklyMeasurements);

		weeklyMeasurementsListView.setAdapter(weeklyDataAdapter);
	}

	public class IncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation;

		@Override
		public void onReceive(Context context, Intent intent) {

			if(intent.getAction().equalsIgnoreCase(filter4)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}


				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}

			}

			else if (intent.getAction().equals(filter)) {
				
				Log.e("INCOMING PHYSIO DATA","MEAS ARRIVED");
				if (intent.hasExtra("physiologicalData")) {
					Bundle b = intent.getBundleExtra("physiologicalData");
					String CalciumIn = b.getString("CalciumIn");
					String CalciumOut = b.getString("CalciumOut");
					String PotassiumIn = b.getString("PotassiumIn");
					String PotassiumOut = b.getString("PotassiumOut");
					String UreaIn = b.getString("UreaIn");
					String UreaOut = b.getString("UreaOut");
					String pHIn = b.getString("pHIn");
					String pHOut = b.getString("pHOut");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_IN))
						dailyMeasurement = CalciumIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CALCIUM_OUT))
						dailyMeasurement = CalciumOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_IN))
						dailyMeasurement = PotassiumIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.POTASSIUM_OUT))
						dailyMeasurement = PotassiumOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_IN))
						dailyMeasurement = UreaIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.UREA_OUT))
						dailyMeasurement = UreaOut;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_IN))
						dailyMeasurement = pHIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PH_OUT))
						dailyMeasurement = pHOut;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}

			}
			else if(intent.getAction().equals(filter2)) {
				if(intent.hasExtra("ecgData")) {

					Bundle b = intent.getBundleExtra("ecgData");
					String heartRate = b.getString("heartRate");
					String respirationRate = b.getString("respirationRate");
					String activityLevel= b.getString("activityLevel");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.HEART_RATE))
						dailyMeasurement = heartRate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.RESPIRATION_RATE))
						dailyMeasurement = respirationRate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.ACTIVITY_LEVEL))
						dailyMeasurement = activityLevel;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}
			}
			else if(intent.getAction().equals(filter3)) {
				Log.e("INCOMING PHYSIO DATA","MEAS ARRIVED");
				if(intent.hasExtra("physiologicalSensorData")) {
					Bundle b = intent.getBundleExtra("physicalSensorData");
					String pressureDialysate = b.getString("pressureDialysate");
					String pressureFCO = b.getString("pressureFCO");
					String pressureBloodline = b.getString("pressureBloodline");
					String pressureBCO = b.getString("pressureBCO");
					String temperatureBloodlineIn = b.getString("temperatureBloodlineIn");
					String temperatureBloodlineOut = b.getString("temperatureBloodlineOut");
					String conductivityFCR = b.getString("conductivityDialysate");
					//	String conductivityFCQ = b.getString("conductivityFCQ");
					String CondTempDialysate = b.getString("CondTempDialysate");

					if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_DIALYSATE))
						dailyMeasurement = pressureDialysate;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_FCO))
						dailyMeasurement = pressureFCO;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BLOODLINE))
						dailyMeasurement = pressureBloodline;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.PRESSURE_BCO))
						dailyMeasurement = pressureBCO;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_IN))
						dailyMeasurement = temperatureBloodlineIn;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT))
						dailyMeasurement = temperatureBloodlineOut;
					//					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_FCQ))
					//						dailyMeasurement = conductivityFCQ;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.CONDUCTIVITY_DIALYSATE))
						dailyMeasurement = conductivityFCR;
					else if(code == MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.COND_TEMP_DIALYSATE))
						dailyMeasurement = CondTempDialysate;

					if (dailyMeasurements.getFirst().equals("No Data Yet")) {
						dailyMeasurements.removeFirst();
					}
					dailyMeasurements.addFirst(dailyMeasurement);
					dailyDataAdapter.notifyDataSetChanged();
				}
			}
		}

	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	private void initializeHeader() {

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);
		intentFilter.addAction(filter4);
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(MeasurementsTable.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(MeasurementsTable.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)MeasurementsTable.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}

}
