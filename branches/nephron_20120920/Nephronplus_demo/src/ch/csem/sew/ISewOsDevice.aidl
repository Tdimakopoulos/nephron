/* ---------------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2013          CSEM S.A.            CH-2002 Neuchatel
 * ALL RIGHTS RESERVED
 * ---------------------------------------------------------------------------------------------------------------------
 * Any copy of this software must include both the above
 * copyright notice of CSEM SA and this paragraph.  
 * This software is made available AS IS,
 * and CSEM SA AND THE AUTHORS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER
 * PROVISION CONTAINED HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM
 * THE SOFTWARE OR ITS USE IS EXPRESSLY DISCLAIMED, WHETHER ARISING IN
 * CONTRACT, TORT (INCLUDING NEGLIGENCE) OR STRICT LIABILITY, EVEN IF
 * CSEM SA OR THE AUTHORS ARE ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * ---------------------------------------------------------------------------------------------------------------------
 */

package ch.csem.sew;

import ch.csem.sew.ISewOsDeviceCallBack;

interface ISewOsDevice
{
	/**
	 * Register a class with call-back methods which are called from the service once a value changes.
	 */
	void registerCallBack( in ISewOsDeviceCallBack callback );
	
	/**
	 * Unregister the call-back class.
	 */
	void unregisterCallBack( in ISewOsDeviceCallBack callback );
	
	/**
	 * Get the Bluetooth Device Address of the last device which has been successfully connected by the SEWviewer.
	 */
	String getDefaultDeviceAddress();
	
	/**
	 * Get the Bluetooth friendly name of the last device which has been successfully connected by the SEWviewer.
	 */
	String getDefaultDeviceName();
	
	/**
	 * Start a device connection.
	 *
	 * @param deviceAddress string with the BD-Address of the target device. If null, the last successfully connected device will be used.
	 */
	boolean connectToDevice( in String deviceAddress );
	
	/**
	 * Start a device disconnection.
	 */
	void disconnectFromDevice();

	/**
	 * Get the recording state of the device.
	 *
	 * @return true if the device is recording
	 */
	boolean isRecording();
	
	/**
	 * Set the recording mode of the device.
	 * 
	 * @param doRecord	true to activate the recording functions.
	 */
	void setRecording( in boolean doRecord );
	
	/**
	 * Set the streaming state of the device. (Enabled by default when starting the connection!)
	 */
	void setStreaming( in boolean doStream );
	
	/**
	 * Write an annotation string into the data-stream stored into the device memory.
	 */
	boolean	writeAnnotation( in String annotation );
	
	/**
	 * Enable or disable the filter for all channels
	 *
	 * @param do_enable_all	Set to true to enable all channels, false to disable all.
	 */
	void filterChannelEnableAll( in boolean do_enable_all );
	
	/**
	 * Enable or disable a specific channel.
	 *
	 * @param channel	String with the unique identification name of the channel.
	 * @param do_enable	true to enable the channel, false otherwise.
	 * @return true if the channel name has been found.
	 */
	boolean filterChannelEnable( in String channel, in boolean do_enable );
	
	/**
	 * Enable (or disable) the transmission of physical channels, which may be resource consuming.
	 * Even if the channels are not filtered out, if EnableContinuousSignals is not set, the device won't send these channel!
	 */
	void setEnableContinuousSignals( in boolean do_enable );
}
