/* ---------------------------------------------------------------------------------------------------------------------
 * Copyright (C) 2013          CSEM S.A.            CH-2002 Neuchatel
 * ALL RIGHTS RESERVED
 * ---------------------------------------------------------------------------------------------------------------------
 * Any copy of this software must include both the above
 * copyright notice of CSEM SA and this paragraph.  
 * This software is made available AS IS,
 * and CSEM SA AND THE AUTHORS DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED,
 * INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE, AND NOTWITHSTANDING ANY OTHER
 * PROVISION CONTAINED HEREIN, ANY LIABILITY FOR DAMAGES RESULTING FROM
 * THE SOFTWARE OR ITS USE IS EXPRESSLY DISCLAIMED, WHETHER ARISING IN
 * CONTRACT, TORT (INCLUDING NEGLIGENCE) OR STRICT LIABILITY, EVEN IF
 * CSEM SA OR THE AUTHORS ARE ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * ---------------------------------------------------------------------------------------------------------------------
 */

package ch.csem.sew;

interface ISewOsDeviceCallBack
{
	/**
	 * Call-back used once a connection has been established to report the platform and firmware version.
	 */
	void onDeviceFirmwareInfo( in String platform, in int firmware_version, in int firmware_revision, in int firmware_buildnumber );
	
	/**
	 * Call-back used to report the device unique identification number.
	 */
	void onDeviceIdentification( in byte[] deviceid );
	
	/**
	 * Call-back used to report that the connection attempt has failed.
	 */
	void onBluetoothConnectionFailed();
	
	/**
	 * Call-back used to report that the communication has been terminated.
	 */
	void onBluetoothDisconnected();

	/**
	 * Call-back used once a writeAnnotation has been used.
	 *
	 * @param result	0 if no error is reported, -1 if not supported by the target, 1 if an error occurred.
	 */
	void onWriteAnnotationResult( in int result );

	/**
	 * Call-back used if a processed (or non-continuous) signal has been updated.
	 *
	 * @param type	String indicating the signal type (following the enum Unique Channel Identificator).
	 * @param time__seconds	Time-stamp of the signal in seconds (unix-time).
	 * @param value	Signal value
	 */
	void onValueProcessedUpdated( in String type, in double time__seconds, in float value );

	/**
	 * Call-back used if a physical signal has been updated. May be called very often!
	 *
	 * @param type	String indicating the signal type (following the enum Unique Channel Identificator).
	 * @param time__seconds	Time-stamp of the signal in seconds (unix-time).
	 * @param value	Signal value
	 */
	void onValueContinuousUpdated( in String type, in double time__seconds, in float value );
}
