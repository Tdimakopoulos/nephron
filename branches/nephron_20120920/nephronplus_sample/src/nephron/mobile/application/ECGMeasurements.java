package nephron.mobile.application;


import nephron.mobile.backend.InternetConnectivityStatus;
import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import nephron.mobile.statistics.StatisticsWithAChart;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ECGMeasurements extends Activity // extends ListActivity
{
	private ListView _measurementsListView;
	private static final String _measurementsElementArray[] = { "Heart Rate", "Respiration Rate", "Activity Level"};
	//	private String userSelectedMeasurements[];
	SharedPreferences myPrefs;
	String prefName;
	Bundle b;
	Intent openMeasurements;
	Button backButton;
	ArrayAdapter<String> adapter;
	
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.ecgmeasurementslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("ECG Measurements");
		
		initializeHeader();

		backButton = (Button) findViewById(R.id.BackButton);
		// Get Saved Settings Graphical or Table presentation
		myPrefs = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefName = myPrefs.getString("MeasurementPresentation", "Graphical");

		if (prefName.equals("Graphical"))
			openMeasurements = new Intent(ECGMeasurements.this, StatisticsWithAChart.class);
		else
			openMeasurements = new Intent(ECGMeasurements.this, MeasurementsTableAlt.class);

		_measurementsListView = (ListView) findViewById(R.id.ECGMeasurementsListView);

		adapter = new ArrayAdapter<String>(ECGMeasurements.this, R.layout.backgroundlayout, R.id.backtextview,	_measurementsElementArray);

		_measurementsListView.setAdapter(adapter);

		// Event On Click
		_measurementsListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// handle the various choices
				Toast.makeText(ECGMeasurements.this, adapter.getItem(position), Toast.LENGTH_SHORT).show();
				String item = adapter.getItem(position);
				if(item.equalsIgnoreCase("Heart Rate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.HEART_RATE));
					b.putString("header1", "Real Time Heart Rate Trends");
					b.putString("header2", "Daily Heart Rate Trends");
					b.putString("title", "Heart Rate");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Respiration Rate")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.RESPIRATION_RATE));
					b.putString("header1", "Real Time Respiration Rate Trends");
					b.putString("header2", "Daily Respiration Rate Trends");
					b.putString("title", "Respiration Rate");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				else if(item.equalsIgnoreCase("Activity Level")) {
					b = new Bundle();
					b.putInt("code", MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.ACTIVITY_LEVEL));
					b.putString("header1", "Real Time Activity Level Trends");
					b.putString("header2", "Daily Activity Level Trends");
					b.putString("title", "Activity Level");
					openMeasurements.putExtra("values", b);
					startActivity(openMeasurements);
				}
				//TODO Remove when tested
				else{
					AlertDialog _dialog = new AlertDialog.Builder(parent.getContext()).create();
					_dialog.setTitle("Message");
					_dialog.setMessage("Unidentified Selection has been made, something has gone wrong");
					_dialog.setButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int which) {
							dialog.dismiss();
						}
					});
					_dialog.show();

				}
			}
		});



		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ECGMeasurements.this.finish();
			}
		});
	}
	
	
	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(ECGMeasurements.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(ECGMeasurements.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)ECGMeasurements.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(ECGMeasurements.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}

	@Override
	protected void onPause() {
		if(receiver !=null)
			unregisterReceiver(receiver);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(receiver != null)
			registerReceiver(receiver, filter);
	}
	
	
}
