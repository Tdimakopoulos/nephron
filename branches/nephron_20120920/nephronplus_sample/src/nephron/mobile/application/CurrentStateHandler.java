package nephron.mobile.application;

import java.io.IOException;

import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.Service;
import android.content.Intent;
import android.util.Log;

public class CurrentStateHandler extends Thread {

	Service service;  
	WakdCommandEnum command;
	MsgSimple statusRequest;

	public CurrentStateHandler(Service y) {
		this.service = y;
	}

	public void run() {
		
		//	This is a thread that is checking for WAKD status every
		//	10 seconds. Whenever the transmision of data is successful,
		//	the thread will issue an intent to inform that the BT connection is on
		WakdCommandEnum command = WakdCommandEnum.CHANGE_STATE_FROM_TO;
		while (ConnectionService.alive) {

			command = WakdCommandEnum.STATUS_REQUEST;
			statusRequest = new MsgSimple(((GlobalVar) service.getApplication()).getMessageCounter(), command);
			try {
				Thread.sleep(10000); //10secs
				ConnectionService.u.sendDatatoWAKD(statusRequest.encode());
				sendIntentForBluetoothConnectionEnabled();
			} catch (IOException e) {
				Log.e("CURRENT STATE HANDLER", e.getMessage());
				sendIntentForBluetoothConnectionDisabled();
				e.printStackTrace();
			} catch (InterruptedException e) {
				new CurrentStateHandler(service).start();
				this.destroy();
				e.printStackTrace();
			}
		}
	}
	
	
	private void sendIntentForBluetoothConnectionEnabled() {
		service.sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_OFF"));
	}

	private void sendIntentForBluetoothConnectionDisabled() {
		service.sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_OFF"));
	}
	

}
