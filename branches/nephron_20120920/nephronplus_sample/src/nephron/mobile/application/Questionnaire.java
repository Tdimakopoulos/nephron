package nephron.mobile.application;

import nephron.mobile.application.R.drawable;
import nephron.mobile.backend.InternetConnectivityStatus;
import nephron.mobile.backend.NephronSOAPClient;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Questionnaire extends Activity
{

	Button saveQuestionnaireButton;
	Button cancelQuestionnaireButton;
	AlertDialog.Builder builder;
	AlertDialog dialog;

	RadioGroup dizzyGroup;
	RadioGroup nauseaGroup;
	RadioGroup headacheGroup;
	RadioGroup muscleGroup;
	ProgressDialog pdialog;
	
	int dizzyID, nauseaID, headacheID, muscleID;

	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.questionairelayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Questionnaires");

		initializeHeader();

		builder = new Builder(Questionnaire.this);
		builder.setPositiveButton("OK", null);

		dizzyGroup = (RadioGroup) findViewById(R.id.DizzyQuestionGroup);
		nauseaGroup = (RadioGroup) findViewById(R.id.NauseaQuestionGroup);
		headacheGroup = (RadioGroup) findViewById(R.id.HeadacheQuestionGroup);
		muscleGroup = (RadioGroup) findViewById(R.id.MuscleCrampsQuestionGroup);

		cancelQuestionnaireButton = (Button) findViewById(R.id.BackButton);

		saveQuestionnaireButton = (Button) findViewById(R.id.proceedButton);

		saveQuestionnaireButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				dizzyID = dizzyGroup.getCheckedRadioButtonId();
				nauseaID = nauseaGroup.getCheckedRadioButtonId();
				headacheID = headacheGroup.getCheckedRadioButtonId();
				muscleID = muscleGroup.getCheckedRadioButtonId();

				if(dizzyID == -1 || nauseaID == -1 || headacheID == -1 || muscleID == -1) {
					builder.setTitle("Error");
					builder.setIcon(drawable.erroricon);
					builder.setMessage("Please make sure that you have selected an answer for all questions");
					builder.create().show();
				}
				else {
					
					pdialog = ProgressDialog.show(Questionnaire.this, "Questionnaire", "Sending your answers to the server, please wait...",  false, true);
					
					Thread saveAnswers = new Thread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stubs
							
							
							String response1;
							String response2;
							String response3;
							String response4;

							NephronSOAPClient client = new NephronSOAPClient(Questionnaire.this);

							if(((RadioButton) findViewById(dizzyID)).getText().toString().equalsIgnoreCase("Yes"))
								response1 = client.executeQuestionnaireRequest("1", "true");
							else
								response1 = client.executeQuestionnaireRequest("1", "false");

							if(((RadioButton) findViewById(nauseaID)).getText().toString().equalsIgnoreCase("Yes"))
								response2 = client.executeQuestionnaireRequest("2", "true");
							else
								response2 = client.executeQuestionnaireRequest("2", "false");

							if(((RadioButton) findViewById(headacheID)).getText().toString().equalsIgnoreCase("Yes"))
								response3 = client.executeQuestionnaireRequest("3", "true");
							else
								response3 = client.executeQuestionnaireRequest("3", "false");

							if(((RadioButton) findViewById(muscleID)).getText().toString().equalsIgnoreCase("Yes"))
								response4 = client.executeQuestionnaireRequest("4", "true");
							else
								response4 = client.executeQuestionnaireRequest("4", "false");

							if(response1 != null && response2 != null && response3 != null && response4 != null) {
								
								builder.setIcon(drawable.success);
								builder.setTitle("Success");
								builder.setMessage("Your answers have been submitted successfuly.");
								builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										Questionnaire.this.finish();

									}
								});


//								for(int i = 0; i < nauseaGroup.getChildCount(); i++){
//									((RadioButton)nauseaGroup.getChildAt(i)).setEnabled(false);
//								}
//
//								for(int i = 0; i < dizzyGroup.getChildCount(); i++){
//									((RadioButton)dizzyGroup.getChildAt(i)).setEnabled(false);
//								}
//								for(int i = 0; i < headacheGroup.getChildCount(); i++){
//									((RadioButton)headacheGroup.getChildAt(i)).setEnabled(false);
//								}
//
//								for(int i = 0; i < muscleGroup.getChildCount(); i++){
//									((RadioButton)muscleGroup.getChildAt(i)).setEnabled(false);
//								}

							}
							else {
								builder.setIcon(drawable.erroricon);
								builder.setTitle("Failure");
								builder.setMessage("Some of your answers have not been submitted successfully, please try again");
								builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										Questionnaire.this.finish();

									}
								});
							}
							
							if (pdialog != null) {
								if(pdialog.isShowing())
									pdialog.dismiss();
							}
							Looper.prepare();
							dialog = builder.create();
							dialog.show();
							Looper.loop();
						}
					});
					saveAnswers.start();
					
				}

			}
		});

		cancelQuestionnaireButton.setOnClickListener( new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});
	}


	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(Questionnaire.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(Questionnaire.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)Questionnaire.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(Questionnaire.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}


	@Override
	protected void onDestroy() {
		if (pdialog != null) {
			if(pdialog.isShowing())
				pdialog.dismiss();
		}
		if(dialog != null) {
			if (dialog.isShowing())
				dialog.dismiss();
		}
		unregisterReceiver(receiver);
		super.onDestroy();
	}




}
