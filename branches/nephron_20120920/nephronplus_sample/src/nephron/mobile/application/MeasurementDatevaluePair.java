package nephron.mobile.application;

public class MeasurementDatevaluePair {
	
	private String date;
	private Double value;
	
	
	public MeasurementDatevaluePair(String date, Double value) {
		this.setDate(date);
		this.setValue(value);
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public Double getValue() {
		return value;
	}


	public void setValue(Double value) {
		this.value = value;
	}

}
