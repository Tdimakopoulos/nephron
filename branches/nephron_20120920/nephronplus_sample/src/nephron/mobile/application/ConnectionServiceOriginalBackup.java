//TODO Original Version
package nephron.mobile.application;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import nephron.mobile.application.alarms.AlarmsMsg;
import nephron.mobile.application.alarms.AlertHandler;
import nephron.mobile.backend.SPLogin;
import nephron.mobile.datafunctions.DBAdapter;
import nephron.mobile.datafunctions.MsgSimple;
import nephron.mobile.datafunctions.MyDataListener;
import nephron.mobile.datafunctions.WakdCommandEnum;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import de.imst.nephron.bt.BluetoothAbstractionLayer;

public class ConnectionServiceOriginalBackup extends Service {
	
	//TODO bluetooth WAS static only, NOW IS public static, there was error in the shutdowntask
	public static BluetoothAbstractionLayer u;
	static boolean connection = false;
	AlertDialog alert;
	AlertDialog.Builder alertd;
	WakdCommandEnum command;
	MsgSimple statusRequest;
	MyDataListener mylistener;
	Thread alertHandler,currentStateHandler;
	public static DBAdapter db;
	public static boolean alive;
	
	NephronApplicationStatusHandler handler;
	
	final int CONNECTION_OK = 0;
	final int CONNECTION_ERROR = 1;
	Runnable loginRunnable;
	Thread backendLogin;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	// Start Service
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Service Started","Service Started");
		
		loginRunnable = new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				SPLogin login = new SPLogin(handler, getApplicationContext());
	    		login.run();
			}
		};
		
		backendLogin = new Thread(loginRunnable);
		
		//create db
		db = new DBAdapter(this);
        db.open();
        
        String storedPassword = ConnectionServiceOriginalBackup.db.getParameterValue("USER_PASSWORD");
		if(storedPassword == null) {
			ConnectionServiceOriginalBackup.db.insertParameter("USER_PASSWORD", "1234");
		}
        
        alive = true;
		u = new BluetoothAbstractionLayer();
		
		mylistener = new MyDataListener(this);
		u.setDataListener(mylistener);
		
		
		currentStateHandler = new CurrentStateHandler(this);
		
		try {
			u.init(); 
		    connection = true;
		    
		    sendIntentForBluetoothConnectionEnabled();
			 
		    //if connection established Send STATUS_REQUEST to WAKD
	        command = WakdCommandEnum.STATUS_REQUEST;
		    statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
			try {
				// Thread.sleep(10000); //10secs
				u.sendDatatoWAKD(statusRequest.encode());
			} catch (IOException e) {
//				connection = 
				sendIntentForBluetoothConnectionDisabled();
				e.printStackTrace();
			}  
			
			Log.e("****** CONNECTION SERVICE ******", "HI FRANK, I JUST MANAGED TO MAKE A CONNECTION");
		  
   		// ask for Current State Every 10 sec
		// currentStateHandler.start();
 			 
		} catch (IOException e) {

			//if Bluetooth not Activated open it
			if (e.getMessage() == "Bluetooth not Activated!") {
				Intent infoDialog = new Intent(ConnectionServiceOriginalBackup.this,infoDialog.class);
				infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				infoDialog.putExtra("BluetoothMessage","Your Bluetooth is not Activated!");
	    	    startActivity(infoDialog);    
			}
			else if(e.getMessage().equalsIgnoreCase("Device has no Bluetooth Adapter!")) {
				Intent infoDialog = new Intent(ConnectionServiceOriginalBackup.this,infoDialog.class);
				infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				infoDialog.putExtra("BluetoothMessage","Device has no Bluetooth Adapter!");
	    	    startActivity(infoDialog);
			}
//			else if (e.getMessage() == "Service discovery failed") {
//
//				 connectionhandler=new ConnectionHandler();
//				 connectionhandler.start();
//				sendIntentForBluetoothConnectionDisabled();
//			}

			e.printStackTrace();
		}
 	
		
		// if there is no connection try to connect every 2 sec
		if (!connection) {
			final Timer t = new Timer();
			t.schedule(new TimerTask() {
				public void run() {
					Looper.prepare();
					int connectionAttempt = 0;
					while (!connection) {
						Log.d("!!!!NO CONNECTION!!!!!!!!","!NO CONNECTION!!!!!!!!!");
						try {
							Thread.sleep(2000);
							u.init();
							connection = true;
							//This intent will update the WAKD Status Screen accordingly
							Intent i = new Intent("nephron.mobile.application.WakStatusEvent");
							i.putExtra("connection","Active");
					    	sendBroadcast(i);
					    	
					    	sendIntentForBluetoothConnectionEnabled();
					     
					    	//if connection established Send STATUS_REQUEST to WAKD
					        command = WakdCommandEnum.STATUS_REQUEST;
							statusRequest = new MsgSimple(((GlobalVar) getApplication()).getMessageCounter(), command);
							try {
								// Thread.sleep(10000); //10secs
								u.sendDatatoWAKD(statusRequest.encode());
							} catch (IOException e) {
								sendIntentForBluetoothConnectionDisabled();
								e.printStackTrace();
							}
							
							Log.d("!!!!YESSSSSSSSSSSS!!!!!!!!","!!!!!!!!YESSSSSSSSSSSS!!!!!!!!!");
							//Show dialog if connection established
//							Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
//							infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
//							infoDialog.putExtra("message","Connection Established with WAKD!!");
//				    	    startActivity(infoDialog);
				    	    
				    	    ((GlobalVar)getApplicationContext()).setConnectionServiceStarted(true);
				    	    handler = ((GlobalVar) getApplication()).getApplicationStatushandler();
				    	    handler.sendEmptyMessage(CONNECTION_OK);
				    	    backendLogin.start();
				    	    
				    		// ask for Current State Every 10 sec 
						 	// currentStateHandler.start();
				    	    
						} catch (IOException e) {
							//Sow dialog if connection not established after 4 attempts
							if(connectionAttempt == 4){
//								Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
//								infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
//								infoDialog.putExtra("message","Connection failed to WAKD!!");
//					    	    startActivity(infoDialog);
					    	    
					    	    ((GlobalVar)getApplicationContext()).setConnectionServiceStarted(false);
					    	    handler = ((GlobalVar) getApplication()).getApplicationStatushandler();
					    	    handler.sendEmptyMessage(CONNECTION_ERROR);
					    	    backendLogin.start();
					    	    
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						connectionAttempt++;
					}

					// t.cancel();
				}
			}, 2000);
		}else {
			//Show dialog if connection established
//			Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
//			infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
//			infoDialog.putExtra("message","Connection Established with WAKD!!");
//    	    startActivity(infoDialog);
    	    
    	    ((GlobalVar)getApplicationContext()).setConnectionServiceStarted(true);
    	    handler = ((GlobalVar) getApplication()).getApplicationStatushandler();
    	    handler.sendEmptyMessage(CONNECTION_OK);
    	    backendLogin.start();
		}
     
		// Set dataListener from WAKD
		mylistener = new MyDataListener(this);
		u.setDataListener(mylistener);
		
		Cursor c = ConnectionServiceOriginalBackup.db.getUncheckedAlerts();
		while(c.moveToNext()) {
			((GlobalVar)getApplicationContext()).getArchivedAlarmsList().
						add(new AlarmsMsg(c.getInt(1), c.getInt(0)));
		}
		c.close();
		
		// Start thread to check for unchecked alerts
		alertHandler = new AlertHandler(getApplicationContext());
		alertHandler.start();
		
		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("Service stopped","Service stopped");
		alive = false;
		connection = false;
        db.close();
	}
	
	private void sendIntentForBluetoothConnectionEnabled() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_ON"));
	}
	
	private void sendIntentForBluetoothConnectionDisabled() {
		sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("bluetooth_state", "BT_OFF"));
	}

}
