package nephron.mobile.application;

import java.util.ArrayList;
import java.util.List;

import nephron.mobile.application.alarms.AlarmsMsg;
import nephron.mobile.backend.InternetConnectivityStatus;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Alarms extends Activity 
{
	private ListView _measurementsListView;
	private MyAdapter adapter_listas;
	private List<AlarmsMsg> alarmsList;
	Cursor c;
	Button backButton;
	Handler handler;
	AlarmsReceiver receiver;
	private String alarmsFilter = "nephron.mobile.application.alarmsacked";
	private String incomingAlarmFilter = "nephron.mobile.application.alarmarrived";
	private String headerFilter = "CONTROLS_CHANGED";

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{	
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alarmslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Alarms");

		initializeHeader();

		backButton = (Button) findViewById(R.id.BackButton);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Alarms.this.finish();
			}
		});

		_measurementsListView = (ListView)findViewById(R.id.AlarmsListView);
		alarmsList = new ArrayList<AlarmsMsg>();
		adapter_listas = new MyAdapter(this);
		_measurementsListView.setAdapter(adapter_listas);

		fillListAndShow();

	}

	public void ShowOrder(final List<AlarmsMsg> alarm) {
		this.runOnUiThread(new Runnable() {
			//	@Override
			public void run() {
				adapter_listas.addOrder(alarm);
				adapter_listas.notifyDataSetChanged();
			}
		});
	}

	public void ClearOrder() {
		this.runOnUiThread(new Runnable() {
			//	@Override
			public void run() {
				adapter_listas.clear();
			}
		});
	}

	public class AlarmsReceiver extends BroadcastReceiver {
		
		Animation batteryAnimation;

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(alarmsFilter) || intent.getAction().equals(incomingAlarmFilter)) {
				alarmsList.clear();
				fillListAndShow();
			}
			else if(intent.getAction().equals(headerFilter)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}


				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}

			}
		}
	}

	public void fillListAndShow() {
		c =   ConnectionService.db.getAllAlerts();
		while (c.moveToNext())
			alarmsList.add(new AlarmsMsg(c.getInt(0),c.getInt(1),c.getString(2),c.getInt(3)));
		c.close();
		ShowOrder((List<AlarmsMsg>)alarmsList);
	}


	@Override
	public void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}
	

	private void initializeHeader() {
		
		receiver = new AlarmsReceiver();
		IntentFilter filter = new IntentFilter(alarmsFilter);
		filter.addAction(headerFilter);
		filter.addAction(incomingAlarmFilter);
		registerReceiver(receiver, filter);
		

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(Alarms.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(Alarms.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)Alarms.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}

}
