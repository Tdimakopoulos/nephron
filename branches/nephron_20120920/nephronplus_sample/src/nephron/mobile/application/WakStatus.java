package nephron.mobile.application;

import nephron.mobile.backend.InternetConnectivityStatus;
import nephron.mobile.datafunctions.MeasurementsCodesEnum;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class WakStatus extends Activity {
	private ListView _wakStatusListView;
	private static final String _wakStatusElementArray[] = 
		{	"Control target Weight: -",
			"Battery Status: -",
			"Current Status: -" ,
			"Operational Status: -",
			"Bloodline Outlet Temp : -"};
	
	private String filter = "nephron.mobile.application.WakStatusEvent";
	private String filter2 = "CONTROLS_CHANGED";
	private String filter3 = "nephron.mobile.application.PhysicalsensorMeasurementEvent";
	private IncomingReceiver receiver;
	ArrayAdapter<String> dataAdapter;
	int wakdStateIs, wakdOpStateIs;
	Double st1,st2;
	Button backButton;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText, activityTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wakstatuslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("WAKD Status");

		initializeHeader();

		backButton = (Button) findViewById(R.id.BackButton);

		_wakStatusListView = (ListView) findViewById(R.id.WakStatusListView);
		dataAdapter = new ArrayAdapter<String>(this,
				R.layout.backgroundlayoutwithoutarows, R.id.backtextview,
				_wakStatusElementArray);
		_wakStatusListView.setAdapter(dataAdapter);


		Double BOT = ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.TEMP_BLOODLINE_OUT));
		if(BOT != null) {
			_wakStatusElementArray[4] = "Bloodline Outlet Temp. : " + String.valueOf(BOT) + " �C";
		}
		
		st1= ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.WAKD_CURRENT_STATE));
		st2= ConnectionService.db.getMeasurement(MeasurementsCodesEnum.MeasurementEnum(MeasurementsCodesEnum.WAKD_CUR_OP_STATE));
		if(st1!=null && st2!=null) {
			wakdStateIs = st1.intValue();
			wakdOpStateIs = st2.intValue();
			_wakStatusElementArray[2] = "Current Status: " + WakControl.wakdState(2, wakdStateIs);
			_wakStatusElementArray[3] = "Operational Status: " + WakControl.wakdState(1, wakdOpStateIs);
		}

		backButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	private void initializeHeader() {

		receiver = new IncomingReceiver();
		IntentFilter intentFilter = new IntentFilter(filter);
		intentFilter.addAction(filter2);
		intentFilter.addAction(filter3);
		registerReceiver(receiver, intentFilter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(WakStatus.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(WakStatus.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)WakStatus.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}

	public class IncomingReceiver extends BroadcastReceiver {

		Animation batteryAnimation;
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			Log.e("INCOMING MESSAGE", intent.getAction());

			if(intent.getAction().equalsIgnoreCase(filter2)) {

				if(intent.hasExtra("wifi_state")) {
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
						TextView wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("Off");
						wifiText.setTextColor(Color.parseColor("#b85c2f"));
					}
					else
						if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
							wifiText = (TextView) findViewById(R.id.wifiText);
							wifiText.setText("On");
							wifiText.setTextColor(Color.parseColor("#2D9C3E"));
						}
				}

				if(intent.hasExtra("bluetooth_state")) {
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("Off");
					}
					else
						if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
							btText = (TextView) findViewById(R.id.bluetoothText);
							btText.setText("On");
						}
				}


				if(intent.hasExtra("battery_level")) {
					batteryAnimation = AnimationUtils.loadAnimation(context, R.anim.battery_animation);

					Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
					
					_wakStatusElementArray[1] = "Battery Status: " + String.valueOf(batteryLevel) + "%";
					dataAdapter.notifyDataSetChanged();
					
					
					batteryText = (TextView) findViewById(R.id.batteryText);
					batteryImage = (ImageView) findViewById(R.id.batteryicon);

					if(batteryLevel != null && batteryLevel != -1) {
						batteryText.setText(batteryLevel.toString()+"%");
						if(batteryLevel>20) {
							batteryImage.clearAnimation();
							batteryText.setTextColor(Color.parseColor("#2D9C3E"));
							if(batteryLevel<=100 && batteryLevel>80)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
							else
								if(batteryLevel<=80 && batteryLevel>60)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
								else
									if(batteryLevel<=60 && batteryLevel>40)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
									else
										if(batteryLevel<=40 && batteryLevel>20)
											batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
						}
						else {
							batteryText.setTextColor(Color.parseColor("#b85c2f"));
							if(batteryLevel<=20 && batteryLevel>10) {
								batteryImage.clearAnimation();
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
							}
							else {
								batteryImage.startAnimation(batteryAnimation);
								if(batteryLevel<=10 && batteryLevel>5)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
								else
									if(batteryLevel<=5 && batteryLevel>=0)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
							}
						}
					}

				}

			}
			else if (intent.getAction().equals(filter)) {
				if (intent.hasExtra("currentWakdState")) {
					Bundle b = intent.getBundleExtra("currentWakdState");
					int newWakdStateIs = b.getInt("wakdStateIs");
					int newWakdOpStateIs = b.getInt("wakdOpStateIs");
					_wakStatusElementArray[2] = "Current Status: " + WakControl.wakdState(2, newWakdStateIs);
					_wakStatusElementArray[3] = "Operational Status: " + WakControl.wakdState(1, newWakdOpStateIs);
					dataAdapter.notifyDataSetChanged();
				}
			}
			else if (intent.getAction().equals(filter3)) {
				if (intent.hasExtra("physicalSensorData")) {
					Bundle b = intent.getBundleExtra("physicalSensorData");
					Double temp = b.getDouble("temperatureBloodlineOut");
					if(temp!=null) {
						_wakStatusElementArray[4] = "Bloodline Outlet Temp. : " + String.valueOf(temp) + " �C";
						dataAdapter.notifyDataSetChanged();	
					}
				}
			}
		}
	}

	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}
}