package nephron.mobile.application;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import nephron.mobile.backend.InternetConnectivityStatus;
import nephron.mobile.calendar.CalendarActivity;
import nephron.mobile.calendar.CalendarView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

public class ScheduleDetails extends Activity
{
	CalendarView mView = null;
	AlertDialog.Builder setScheduleEvent,alertDialog;
	private TimePickerDialog timePickDialog = null;
	EditText timeSelection,userDetails;
	String timeSelectionInput,userDetailsInput;
	LayoutInflater li;
	View setsSheduleventView;
    SimpleDateFormat dateFormat;
	private ListView eventsListView;  
	ArrayAdapter<String> eventListAdapter;
	private LinkedList<String> eventsList;
	Cursor c;
	
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.scheduledetailslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		initializeHeader();
		
		mView = (CalendarView)findViewById(R.id.calendar);
	    dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    
	    eventsListView = (ListView)findViewById(R.id.EventsListView);
	    eventsList = new LinkedList<String>();
	   
	   
	    c =   ConnectionService.db.getEvents(CalendarActivity.dateForDB);
	     while (c.moveToNext()) { 
 	    	eventsList.add(c.getString(0) + " " + c.getString(1)); 
		}
		c.close();
		
		  eventListAdapter = new ArrayAdapter<String>(this,R.layout.backgroundmeauseremntsrow, R.id.backtextview,eventsList);
		  eventsListView.setAdapter(eventListAdapter);
		
		TextView dateText = (TextView)findViewById(R.id.DateTitleTextBox);
		dateText.setText (CalendarActivity.date); 
		
	    li = LayoutInflater.from(this);  
	    
	    alertDialog =  new AlertDialog.Builder(this);
	    alertDialog.setTitle("Alert").setMessage(" Please Insert Correct data ");
	    alertDialog.setPositiveButton("OK",null);
	}
	
	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.NewEventImageButton: // New Event
			setsSheduleventView = li.inflate(R.layout.setschedulevent, null);
			timeSelection = (EditText) setsSheduleventView.findViewById(R.id.UserTimeInput);
			userDetails = (EditText) setsSheduleventView.findViewById(R.id.UserDetailsInput);
		  	timeSelection.setOnClickListener(new OnClickListener() {
//		 		@Override
			    public void onClick(View v) {
					timePickDialog = new TimePickerDialog(v.getContext(),
							new TimePickHandler(), 8, 00, true);
		 			  timePickDialog.show();
			    }
			});
			setScheduleEvent = new AlertDialog.Builder(this);
			setScheduleEvent.setView(setsSheduleventView);
			setScheduleEvent.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							timeSelectionInput = timeSelection.getText().toString();
							userDetailsInput = userDetails.getText().toString();
							Log.d(" Test!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
							
	 						 if(timeSelectionInput.equals("--:--") || userDetailsInput.length() < 1){
								 alertDialog.show();
							 }else{
									Log.d(" Ok!!!!! "," timeSelection = "+ timeSelectionInput + " kai userDetails = " + userDetailsInput);
									Date date = new Date();
									// Insert Event into the db 
									ConnectionService.db.insertScheduledTask(0, dateFormat.format(date), null, 2,
											dateFormat.format(date), 2, null, null, 4,
											null,  0,
											null, 1, 1, CalendarActivity.dateForDB+" "+timeSelectionInput, userDetailsInput);  
									finish();
									Intent scheduleDetails = new Intent(ScheduleDetails.this, ScheduleDetails.class);
									startActivity(scheduleDetails);
									overridePendingTransition(0, 0);
							 }
	 		 			}
					});
			
			setScheduleEvent.setNegativeButton("No",null);
			setScheduleEvent.show();		
			break;
		case R.id.CancelImageButton: // Cancel
			finish();
			break;
		default:
			break;
		}
	}
	
    private class TimePickHandler implements OnTimeSetListener {
//        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        	timeSelection.setText((hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" + (minute < 10 ? "0" + minute : minute) );
            timePickDialog.hide();
            
        }
    }
    
    private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(ScheduleDetails.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(ScheduleDetails.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)ScheduleDetails.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(ScheduleDetails.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}

			}

		}

	}
    
 
}
 