package nephron.mobile.application.alarms;


import java.util.Timer;
import java.util.TimerTask;

import nephron.mobile.application.ConnectionService;
import nephron.mobile.application.GlobalVar;
import nephron.mobile.application.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;

public class MyAlertDialog extends Activity {
	AlertDialog alert;
	AlertDialog.Builder alertd;
	String alertid, alertMessage;
	boolean newAlarm = true;
	Timer t;
	Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = getApplicationContext();

		alertd = new AlertDialog.Builder(MyAlertDialog.this);
		alertd.setIcon(R.drawable.warning);

		t = new Timer();

		String incomingAction = getIntent().getAction();
		if(incomingAction.equalsIgnoreCase("NEW_ALARM")) {

			newAlarm = true;
			Bundle extras = getIntent().getExtras();
			alertid = extras.getString("alertid");
			alertMessage = extras.getString("alertMessage");

			alertd.setTitle("ALARM");
			alertd.setMessage(alertMessage);

			alertd.setPositiveButton("Accept", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					t.cancel();
					ConnectionService.db.updateAlertStatus(Integer.parseInt(alertid), 2);
					QueueHandler.removeAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList(), Integer.valueOf(alertid));

					sendIntentToUpdateAlarms();
					
					sendIntentForNextAlarm();
					MyAlertDialog.this.finish();
				}
			});

			alertd.setNegativeButton("Cancel", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					t.cancel();
					AlarmsMsg temMsg = QueueHandler.findAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList(), Integer.valueOf(alertid));
					QueueHandler.removeAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList(), Integer.valueOf(alertid));
					((GlobalVar)getApplicationContext()).getArchivedAlarmsList().add(temMsg);
					((GlobalVar)getApplicationContext()).setAlarmWindowOpen(false);
					MyAlertDialog.this.finish();
				}
			});


			alert = alertd.create();
		}
		else
			if(incomingAction.equalsIgnoreCase("ARCHIVED_ALARM")) {

				newAlarm = false;

				Bundle extras = getIntent().getExtras();
				alertid = extras.getString("alertid");
				alertMessage = extras.getString("alertMessage");

				alertd.setTitle("ALARM");
				alertd.setMessage(alertMessage);

				alertd.setPositiveButton("Accept",	new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						t.cancel();
						ConnectionService.db.updateAlertStatus(Integer.parseInt(alertid), 2);
						QueueHandler.removeAlarmFromQueue(((GlobalVar)getApplicationContext()).getArchivedAlarmsList(), Integer.valueOf(alertid));
						// TODO CHECK update alarms mechanism
						sendIntentToUpdateAlarms();
						
						sendIntentForNextAlarm();
						MyAlertDialog.this.finish();
					}
				});

				alertd.setNegativeButton("Cancel", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						t.cancel();
						((GlobalVar)getApplicationContext()).setAlarmWindowOpen(false);
						MyAlertDialog.this.finish();
					}
				});

				alert = alertd.create();

			}
	}


	@Override
	public void onStart() {
		super.onStart();

		if(alert!=null) {
			
			if(context.getSharedPreferences("userSettings", Context.MODE_WORLD_READABLE).getString("Vibration", "Off").equalsIgnoreCase("On")) {
				Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				mVibrator.vibrate(new long[]{500,300,500,300,500}, 1);
			}
			
			alert.show();
			if(newAlarm) {
				t.schedule(new TimerTask() {
					public void run() {
						alert.dismiss();
						AlarmsMsg temMsg = QueueHandler.findAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList(), Integer.valueOf(alertid));
						QueueHandler.removeAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList(), Integer.valueOf(alertid));
						((GlobalVar)getApplicationContext()).getArchivedAlarmsList().add(temMsg);
						((GlobalVar)getApplicationContext()).setAlarmWindowOpen(false);
						overridePendingTransition(0, 0);
						t.cancel();
						MyAlertDialog.this.finish();
					}
				}, 5000);
			}
			else {

				t.schedule(new TimerTask() {
					public void run() {
						alert.dismiss();
						((GlobalVar)getApplicationContext()).setAlarmWindowOpen(false);
						overridePendingTransition(0, 0);
						t.cancel();
						MyAlertDialog.this.finish();
					}
				}, 5000);

			}
		}
		else
			Log.e("Inside HANDLER", "*****************ALERT IS NULL FOR SOME REASON*****************");
	}

	private void sendIntentToUpdateAlarms() {

		Intent updateAlarms = new Intent("nephron.mobile.application.alarmsacked");
		MyAlertDialog.this.sendBroadcast(updateAlarms);
	}

	private void sendIntentForNextAlarm() {

		Intent alarmIntent = new Intent("android.intent.action.MAIN");
		alarmIntent.setClass(MyAlertDialog.this, MyAlertDialog.class);
		alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		if (!((GlobalVar)getApplicationContext()).getNewAlarmsList().isEmpty()) {
			AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)getApplicationContext()).getNewAlarmsList());
			alarmIntent.setAction("NEW_ALARM");
			alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
			alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
			startActivity(alarmIntent);
		}
		else {
			if (!((GlobalVar)getApplicationContext()).getArchivedAlarmsList().isEmpty()) {
				AlarmsMsg alarm_msg = QueueHandler.getLatestAlarmFromQueue(((GlobalVar)getApplicationContext()).getArchivedAlarmsList());
				alarmIntent.setAction("ARCHIVED_ALARM");
				alarmIntent.putExtra("alertid",Integer.toString(alarm_msg.getIdDatabase()));
				alarmIntent.putExtra("alertMessage",alarm_msg.getmsgPatient());
				startActivity(alarmIntent);
			}
			else {
				((GlobalVar)getApplicationContext()).setAlarmWindowOpen(false);
			}
		}

		MyAlertDialog.this.finish();
	}


	public void onDestroy() {
		super.onDestroy();
	}

}