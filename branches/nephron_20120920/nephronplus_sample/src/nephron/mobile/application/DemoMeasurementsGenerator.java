package nephron.mobile.application;

import java.util.Random;

import nephron.mobile.datafunctions.ByteUtils;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class DemoMeasurementsGenerator implements Runnable {

	private Context context;
	byte[] wakdStatus = new byte[8];
	Random rand;
	int messageCounter = 0;
	static String intentAction = "nephron.mobile.application.DataArrived";

	public DemoMeasurementsGenerator(Context context) {
		super();
		this.context = context;
	}


	@Override
	public void run() {

		rand = new Random();

		createWakdStatusCommand();	//run only the first time the 

		while (true) {
			
			createBatteryLevel();
			delay(1000);
			createWeightmeasurements();
			delay(1000);
			createECGmeasurements();
			delay(1000);
			createPhysicalMeasurements();
			delay(1000);
			createPhysiologicalMeasurements();
			delay(1000);
			createBPmeasurements();
			delay(3000);

		}
	}
	
	private void delay(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {}
		
		
	}




	private  void createWakdStatusCommand() {

		messageCounter = messageCounter + 1;

		wakdStatus[0] = ByteUtils.intToByte(4);
		wakdStatus[1] = ByteUtils.intToByte(0);
		wakdStatus[2] = ByteUtils.intToByte(8);
		wakdStatus[3] = ByteUtils.intToByte(4);
		wakdStatus[4] = ByteUtils.intToByte(messageCounter);	//Counter
		wakdStatus[5] = ByteUtils.intToByte(1);
		wakdStatus[6] = ByteUtils.intToByte(4);
		wakdStatus[7] = ByteUtils.intToByte(1);
		
		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", wakdStatus);
		context.sendBroadcast(physicalIntent);
	}
	
	private  void createECGmeasurements() {

		messageCounter = messageCounter + 1;
		
		byte[] ecg = new byte[13];

		ecg[0] = ByteUtils.intToByte(4);
		ecg[1] = ByteUtils.intToByte(0);
		ecg[2] = ByteUtils.intToByte(13);
		ecg[3] = ByteUtils.intToByte(27);
		ecg[4] = ByteUtils.intToByte(messageCounter);	//Counter
		ecg[5] = ByteUtils.intToByte(1);
		ecg[6] = ByteUtils.intToByte(random(70,90));
		ecg[7] = ByteUtils.intToByte(random(12,20));
		ecg[8] = ByteUtils.intToByte(random(5,9));
		ecg[9] = ByteUtils.intToByte(1);
		ecg[10] = ByteUtils.intToByte(4);
		ecg[11] = ByteUtils.intToByte(1);
		ecg[12] = ByteUtils.intToByte(1);
		
		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", ecg);
		context.sendBroadcast(physicalIntent);
	}
	
	private  void createBPmeasurements() {

		messageCounter = messageCounter + 1;
		
		byte[] bp = new byte[13];

		bp[0] = ByteUtils.intToByte(4);
		bp[1] = ByteUtils.intToByte(0);
		bp[2] = ByteUtils.intToByte(13);
		bp[3] = ByteUtils.intToByte(25);
		bp[4] = ByteUtils.intToByte(messageCounter);	//Counter
		bp[5] = ByteUtils.intToByte(1);
		bp[6] = ByteUtils.intToByte(random(90,130));
		bp[7] = ByteUtils.intToByte(random(60,100));
		bp[8] = ByteUtils.intToByte(0);
		bp[9] = ByteUtils.intToByte(0);
		bp[10] = ByteUtils.intToByte(0);
		bp[11] = ByteUtils.intToByte(0);
		bp[12] = ByteUtils.intToByte(0);
		
		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", bp);
		context.sendBroadcast(physicalIntent);
	}
	
	
	private  void createWeightmeasurements() {

		messageCounter = messageCounter + 1;
		
		byte[] weight = new byte[19];

		weight[0] = ByteUtils.intToByte(4);
		weight[1] = ByteUtils.intToByte(0);
		weight[2] = ByteUtils.intToByte(13);
		weight[3] = ByteUtils.intToByte(22);
		weight[4] = ByteUtils.intToByte(messageCounter);	//Counter
		weight[5] = ByteUtils.intToByte(1);
		int weightValue = random(785, 805);
		byte[] weightValueByte = ByteUtils.intToByteArray(weightValue, 2);
		
		
		weight[6] = weightValueByte[0];
		weight[7] = weightValueByte[1];
		weight[8] = ByteUtils.intToByte(0);
		weight[9] = ByteUtils.intToByte(0);
		weight[10] = ByteUtils.intToByte(0);
		weight[11] = ByteUtils.intToByte(0);
		weight[12] = ByteUtils.intToByte(0);
		weight[13] = ByteUtils.intToByte(0);
		weight[14] = ByteUtils.intToByte(0);
		weight[15] = ByteUtils.intToByte(0);
		weight[16] = ByteUtils.intToByte(0);
		weight[17] = ByteUtils.intToByte(0);
		weight[18] = ByteUtils.intToByte(0);
		
		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", weight);
		context.sendBroadcast(physicalIntent);
	}


	private void createPhysicalMeasurements() {

		byte[] physical = new byte[52];

		messageCounter = messageCounter + 1;

		// Create header first
		physical[0] = ByteUtils.intToByte(4);
		physical[1] = ByteUtils.intToByte(0);
		physical[2] = ByteUtils.intToByte(8);
		physical[3] = ByteUtils.intToByte(17);
		physical[4] = ByteUtils.intToByte(messageCounter);	//Counter
		physical[5] = ByteUtils.intToByte(1);
		// End of header


		physical[6] = ByteUtils.intToByte(0);
		physical[7] = ByteUtils.intToByte(0);
		physical[8] = ByteUtils.intToByte(0);
		physical[9] = ByteUtils.intToByte(0);

		//***************************************************************
		//Pressure Dialysate --> 450 	+/-50
		//***************************************************************
		int pressureDialysate = random(4000, 5000);
		byte[] pressureDialByte = ByteUtils.intToByteArray(pressureDialysate, 2);
		physical[10] = pressureDialByte[0];
		physical[11] = pressureDialByte[1];
		//***************************************************************
		physical[12] = ByteUtils.intToByte(0);
		physical[13] = ByteUtils.intToByte(0);
		physical[14] = ByteUtils.intToByte(0);
		physical[15] = ByteUtils.intToByte(0);
		physical[16] = ByteUtils.intToByte(0);
		physical[17] = ByteUtils.intToByte(0);

		physical[18] = ByteUtils.intToByte(0);
		physical[19] = ByteUtils.intToByte(0);
		physical[20] = ByteUtils.intToByte(0);
		physical[21] = ByteUtils.intToByte(0);

		//***************************************************************
		//Pressure Bloodline --> 150 +/-20
		//***************************************************************
		int pressureBloodline = random(1300, 1700);
		byte[] pressureBloodByte = ByteUtils.intToByteArray(pressureBloodline, 2);
		physical[22] = pressureBloodByte[0];
		physical[23] = pressureBloodByte[1];
		//***************************************************************

		physical[24] = ByteUtils.intToByte(0);
		physical[25] = ByteUtils.intToByte(0);
		physical[26] = ByteUtils.intToByte(0);
		physical[27] = ByteUtils.intToByte(0);
		physical[28] = ByteUtils.intToByte(0);
		physical[29] = ByteUtils.intToByte(0);
		physical[30] = ByteUtils.intToByte(0);
		physical[31] = ByteUtils.intToByte(0);
		physical[32] = ByteUtils.intToByte(0);
		physical[33] = ByteUtils.intToByte(0);


		int tempBloodlineIn = random(340, 360);
		byte[] tempBloodlineInByte = ByteUtils.intToByteArray(tempBloodlineIn, 2);
		physical[34] = tempBloodlineInByte[0];
		physical[35] = tempBloodlineInByte[1];


		int tempBloodlineOut = random(360, 380);
		byte[] tempBloodlineOutByte = ByteUtils.intToByteArray(tempBloodlineOut, 2);
		physical[36] = tempBloodlineOutByte[0];
		physical[37] = tempBloodlineOutByte[1];



		physical[38] = ByteUtils.intToByte(0);
		physical[39] = ByteUtils.intToByte(0);
		physical[40] = ByteUtils.intToByte(0);
		physical[41] = ByteUtils.intToByte(0);



		int conductivity = random(143, 145);
		byte[] conductivityByte = ByteUtils.intToByteArray(conductivity, 2);
		physical[42] = conductivityByte[0];
		physical[43] = conductivityByte[1];



		physical[44] = ByteUtils.intToByte(1);
		physical[45] = ByteUtils.intToByte(1);

		int conductivityTD = random(355, 375);
		byte[] conductivityTDByte = ByteUtils.intToByteArray(conductivityTD, 2);
		physical[46] = conductivityTDByte[0];
		physical[47] = conductivityTDByte[1];



		physical[48] = ByteUtils.intToByte(0);
		physical[49] = ByteUtils.intToByte(0);
		physical[50] = ByteUtils.intToByte(0);
		physical[51] = ByteUtils.intToByte(0);



		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", physical);
		context.sendBroadcast(physicalIntent);

	}


	private void createPhysiologicalMeasurements() {

		byte[] physiological = new byte[70];

		messageCounter = messageCounter + 1;

		// Create header first
		physiological[0] = ByteUtils.intToByte(4);
		physiological[1] = ByteUtils.intToByte(0);
		physiological[2] = ByteUtils.intToByte(8);
		physiological[3] = ByteUtils.intToByte(12);
		physiological[4] = ByteUtils.intToByte(messageCounter);	//Counter
		physiological[5] = ByteUtils.intToByte(1);
		// End of header


		physiological[6] = ByteUtils.intToByte(0);
		physiological[7] = ByteUtils.intToByte(0);
		physiological[8] = ByteUtils.intToByte(0);
		physiological[9] = ByteUtils.intToByte(0);


		int CalciumIn = random(14,18);
		byte[] CalciumInByte = ByteUtils.intToByteArray(CalciumIn, 2);
		physiological[10] = CalciumInByte[0];
		physiological[11] = CalciumInByte[1];

		physiological[12] = ByteUtils.intToByte(0);
		physiological[13] = ByteUtils.intToByte(0);
		physiological[14] = ByteUtils.intToByte(0);
		physiological[15] = ByteUtils.intToByte(0);


		int potassiumIn = random(46,50 );
		byte[] potassiumInByte = ByteUtils.intToByteArray(potassiumIn, 2);
		physiological[16] = potassiumInByte[0];
		physiological[17] = potassiumInByte[1];


		physiological[18] = ByteUtils.intToByte(0);
		physiological[19] = ByteUtils.intToByte(0);
		physiological[20] = ByteUtils.intToByte(0);
		physiological[21] = ByteUtils.intToByte(0);

		int phIn = random(72, 76);
		byte[] phInByte = ByteUtils.intToByteArray(phIn, 2);
		physiological[22] = phInByte[0];
		physiological[23] = phInByte[1];


		physiological[24] = ByteUtils.intToByte(0);
		physiological[25] = ByteUtils.intToByte(0);
		physiological[26] = ByteUtils.intToByte(0);
		physiological[27] = ByteUtils.intToByte(0);



		int oxidation = random(1500, 1900);
		byte[] oxidationByte = ByteUtils.intToByteArray(oxidation, 2);
		physiological[28] = oxidationByte[0];
		physiological[29] = oxidationByte[1];

		physiological[30] = ByteUtils.intToByte(0);
		physiological[31] = ByteUtils.intToByte(0);
		physiological[32] = ByteUtils.intToByte(0);
		physiological[33] = ByteUtils.intToByte(0);

		int tempIn = random(340, 360 );
		byte[] tempInByte = ByteUtils.intToByteArray(tempIn, 2);
		physiological[34] = tempInByte[0];
		physiological[35] = tempInByte[1];
		physiological[36] = ByteUtils.intToByte(0);
		physiological[37] = ByteUtils.intToByte(0);
		physiological[38] = ByteUtils.intToByte(0);
		physiological[39] = ByteUtils.intToByte(0);

		int CalciumOut = random(14, 18);
		byte[] CalciumOutByte = ByteUtils.intToByteArray(CalciumOut, 2);
		physiological[40] = CalciumOutByte[0];
		physiological[41] = CalciumOutByte[1];

		physiological[42] = ByteUtils.intToByte(0);
		physiological[43] = ByteUtils.intToByte(0);
		physiological[44] = ByteUtils.intToByte(0);
		physiological[45] = ByteUtils.intToByte(0);


		int potassiumOut = random(42, 46);
		byte[] potassiumOutByte = ByteUtils.intToByteArray(potassiumOut, 2);
		physiological[46] = potassiumOutByte[0];
		physiological[47] = potassiumOutByte[1];

		physiological[48] = ByteUtils.intToByte(0);
		physiological[49] = ByteUtils.intToByte(0);
		physiological[50] = ByteUtils.intToByte(0);
		physiological[51] = ByteUtils.intToByte(0);


		int phOut = random(73, 77);
		byte[] phOutByte = ByteUtils.intToByteArray(phOut, 2);
		physiological[52] = phOutByte[0];
		physiological[53] = phOutByte[1];


		physiological[54] = ByteUtils.intToByte(0);
		physiological[55] = ByteUtils.intToByte(0);
		physiological[56] = ByteUtils.intToByte(0);
		physiological[57] = ByteUtils.intToByte(0);
		physiological[58] = ByteUtils.intToByte(0);
		physiological[59] = ByteUtils.intToByte(0);
		physiological[60] = ByteUtils.intToByte(0);
		physiological[61] = ByteUtils.intToByte(0);
		physiological[62] = ByteUtils.intToByte(0);
		physiological[63] = ByteUtils.intToByte(0);

		int tempOut = random(360, 380);
		byte[] tempOutByte = ByteUtils.intToByteArray(tempOut, 2);
		physiological[64] = tempOutByte[0];
		physiological[65] = tempOutByte[1];

		physiological[66] = ByteUtils.intToByte(0);
		physiological[67] = ByteUtils.intToByte(0);
		physiological[68] = ByteUtils.intToByte(0);
		physiological[69] = ByteUtils.intToByte(0);


		Intent physicalIntent = new Intent("nephron.mobile.application.DataArrived");
		physicalIntent.putExtra("value", physiological);
		context.sendBroadcast(physicalIntent);

	}


	private void createBatteryLevel() {

		int counter = random(40, 80);
		context.sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("battery_level", Long.valueOf(String.valueOf(counter))));
	}


	private int random(int min, int max) {

		return rand.nextInt((max - min) + 1) + min;

	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}


}
