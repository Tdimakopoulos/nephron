package nephron.mobile.application;

import nephron.mobile.application.R.drawable;
import nephron.mobile.backend.InternetConnectivityStatus;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class Settings extends Activity { 

	private static final String physioMeasElementArray[] = {
				"Calcium In", 
				"Calcium Out",
				"Potassium In",
				"Potassium Out",
				"pH In",
				"pH Out",
				"Temperature Dialysate In",
				"Temperature Dialysate Out"};
	
	private static final String physicalMeasElementArray[] = 
		{"Temperature Bloodline Incoming",
		"Temperature Bloodline Outgoing",
		"Pressure Bloodline",
		"Pressure Dialysate",
		"Conductivity Dialysate",
		"Temperature Conductivity Dialysate",
		"Oxidation State"};

	private RadioGroup radioMeasurementGroup/*, vibrationRadioGroup*/;
	private Button changePassword, backButton;
	SharedPreferences userSettings;
	SharedPreferences.Editor prefsEditor;
	AlertDialog.Builder confirmation, bpassword, measurementsSelector;
	AlertDialog passwordDialog;
	EditText currentPassword, newPassword, verifyPassword;
	TextView physicalMeasurementsText, physioMeasurementsText, activityTitle;
	boolean[] physicalCheckedItems, physioCheckedItems;

	TextView myMsg;
	View passwordManager;
	
	ConnectivityHeaderReceiver receiver;
	IntentFilter filter;

	ImageView wifiImage, batteryImage, btImage;
	TextView batteryText, btText, wifiText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settingslayout);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		activityTitle = (TextView) findViewById(R.id.activitytitle);
		activityTitle.setText("Settings");
		
		initializeHeader();

		physicalMeasurementsText = (TextView) findViewById(R.id.PhysicalMeasSelector);
		physioMeasurementsText = (TextView) findViewById(R.id.PhysioMeasSelector);

		physicalCheckedItems = new boolean[physicalMeasElementArray.length];
		physioCheckedItems = new boolean[physioMeasElementArray.length];

		userSettings = this.getSharedPreferences("userSettings", MODE_WORLD_READABLE);
		prefsEditor = userSettings.edit();

		//***********************************************************************************************

		physicalMeasurementsText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String physicalMeasPref = userSettings.getString("physicalMeasSelected", "1111111");
				for(int i=0; i<physicalMeasPref.length(); i++) {
					if(physicalMeasPref.charAt(i) == '1')
						physicalCheckedItems[i] = true;
					else
						physicalCheckedItems[i] = false;
				}

				measurementsSelector = new Builder(Settings.this);
				measurementsSelector.setMultiChoiceItems(physicalMeasElementArray, physicalCheckedItems, new OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if(isChecked)
							physicalCheckedItems[which] = true;
						else
							physicalCheckedItems[which] = false;
					}
				});

				measurementsSelector.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Save items here
						char[] physicalsToStore = new char[physicalMeasElementArray.length];
						for(int i=0; i<physicalCheckedItems.length; i++) {
							if(physicalCheckedItems[i])
								physicalsToStore[i]='1';
							else
								physicalsToStore[i]='0';
						}
						prefsEditor.putString("physicalMeasSelected", String.valueOf(physicalsToStore));
						prefsEditor.commit();
					}
				});

				measurementsSelector.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				measurementsSelector.create().show();
			}
		});

		//***********************************************************************************************


		physioMeasurementsText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				String physioMeasPref = userSettings.getString("physioMeasSelected", "11111111");
				for(int i=0; i<physioMeasPref.length(); i++) {
					if(physioMeasPref.charAt(i) == '1')
						physioCheckedItems[i] = true;
					else
						physioCheckedItems[i] = false;
				}

				measurementsSelector = new Builder(Settings.this);
				measurementsSelector.setMultiChoiceItems(physioMeasElementArray, physioCheckedItems, new OnMultiChoiceClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which, boolean isChecked) {
						if(isChecked)
							physioCheckedItems[which] = true;
						else
							physioCheckedItems[which] = false;
					}
				});

				measurementsSelector.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Save items here
						char[] physioToStore = new char[physioMeasElementArray.length];
						for(int i=0; i<physioCheckedItems.length; i++) {
							if(physioCheckedItems[i])
								physioToStore[i]='1';
							else
								physioToStore[i]='0';
						}
						prefsEditor.putString("physioMeasSelected", String.valueOf(physioToStore));
						prefsEditor.commit();
					}
				});

				measurementsSelector.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				measurementsSelector.create().show();
			}
		});


		//***********************************************************************************************

		String presentationSelection = userSettings.getString("MeasurementPresentation", "Graphical");
		String vibrationSelection = userSettings.getString("Vibration", "Off");
//		String alarmsFrequency = userSettings.getString("AlarmsFrequency", "1");

		confirmation = new AlertDialog.Builder(this);
		radioMeasurementGroup=(RadioGroup) findViewById(R.id.MeasurementPresentationGroup);
//		vibrationRadioGroup = (RadioGroup) findViewById(R.id.SelectVibrationGroup);
//		alarmFrequencyGroup = (RadioGroup) findViewById(R.id.AlarmFrequencyGroup);

		changePassword = (Button) findViewById(R.id.PasswordButton);
		backButton = (Button) findViewById(R.id.BackButton);

		// Check appropriate radio button for data presentation mode
		if(presentationSelection.equals("Graphical")) {
			radioMeasurementGroup.check(R.id.GraphicalRadioButton);
		}else{
			radioMeasurementGroup.check(R.id.TableRadioButton);
		}

		// Check appropriate radio button for vibration mode
//		if(vibrationSelection.equals("Off"))
//			vibrationRadioGroup.check(R.id.VibrationOffRadioButton);
//		else
//			vibrationRadioGroup.check(R.id.VibrationOnRadioButton);
		
		
		
		

		// Check appropriate radio button for alarms Frequency
//		if(alarmsFrequency.equals("1"))
//			alarmFrequencyGroup.check(R.id.OneMinuteRadioButton);
//		else if(alarmsFrequency.equals("2"))
//			alarmFrequencyGroup.check(R.id.TwoMinutesRadioButton);
//		else
//			if(alarmsFrequency.equals("5"))
//				alarmFrequencyGroup.check(R.id.FiveMinutesRadioButton);
//
//		alarmFrequencyGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				RadioButton radioButton = (RadioButton) findViewById(checkedId);
//				if(radioButton.getText().toString().equalsIgnoreCase("1 minute"))
//					prefsEditor.putString("AlarmsFrequency", "1");
//				else if(radioButton.getText().toString().equalsIgnoreCase("2 minutes"))
//					prefsEditor.putString("AlarmsFrequency", "2");
//				else if(radioButton.getText().toString().equalsIgnoreCase("5 minutes"))
//					prefsEditor.putString("AlarmsFrequency", "5");
//				prefsEditor.commit();
//			}
//		});

		radioMeasurementGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				RadioButton radioButton = (RadioButton) findViewById(checkedId);
				if(radioButton.getText().toString().equalsIgnoreCase("Graphical"))
					prefsEditor.putString("MeasurementPresentation", "Graphical");
				else if(radioButton.getText().toString().equalsIgnoreCase("Table"))
					prefsEditor.putString("MeasurementPresentation", "Table");
				prefsEditor.commit();
			}
		});

//		vibrationRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				RadioButton radioButton = (RadioButton) findViewById(checkedId);
//				if(radioButton.getText().toString().equalsIgnoreCase("Off"))
//					prefsEditor.putString("Vibration", "Off");
//				else if(radioButton.getText().toString().equalsIgnoreCase("On"))
//					prefsEditor.putString("Vibration", "On");
//				prefsEditor.commit();
//			}
//		});

		changePassword.setOnClickListener(new OnClickListener() {

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

			public void onClick(View arg0) {
				LayoutInflater li = LayoutInflater.from(Settings.this);
				passwordManager = li.inflate(R.layout.changepasswordlayout, null);

				bpassword = new AlertDialog.Builder(Settings.this);
				bpassword.setView(passwordManager);
				bpassword.setTitle("Password Manager");
				bpassword.setIcon(drawable.password);

				currentPassword = (EditText) passwordManager.findViewById(R.id.CurrentPassword);
				newPassword = (EditText) passwordManager.findViewById(R.id.newPassword);
				verifyPassword = (EditText) passwordManager.findViewById(R.id.verifyPassword);

				bpassword.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String storedPassword = ConnectionService.db.getParameterValue("USER_PASSWORD");
						AlertDialog.Builder dialogBuilder = new Builder(Settings.this);
						dialogBuilder.setTitle("Error");
						dialogBuilder.setNeutralButton("OK", null);
						if(!currentPassword.getText().toString().equals(storedPassword)){
							dialogBuilder.setMessage("Current Password mismatch");
							dialogBuilder.show();
						}
						else {
							if((newPassword.getText().toString()).matches("") || (verifyPassword.getText().toString()).matches("")){
								dialogBuilder.setMessage("Fill in all required fields");
								dialogBuilder.show();
							}
							else {
								if(!(newPassword.getText().toString()).equals(verifyPassword.getText().toString())) {
									dialogBuilder.setMessage("New password mismatch");
									dialogBuilder.show();
								}
								else {
									ConnectionService.db.updateParameterValue("USER_PASSWORD", newPassword.getText().toString());
									dialogBuilder.setTitle("Success");
									dialogBuilder.setMessage("Password has changed");
									passwordDialog.dismiss();
									dialogBuilder.show();
								}

							}

						}

					}

				});

				bpassword.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						passwordDialog.dismiss();
					}
				});

				passwordDialog = bpassword.create();
				passwordDialog.show();

			}
		});


		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}
	
	
	private void initializeHeader() {

		receiver = new ConnectivityHeaderReceiver();
		filter = new IntentFilter("CONTROLS_CHANGED");
		registerReceiver(receiver, filter);

		//Set the Wifitext by checking connectivity and broadcast inner intent
		InternetConnectivityStatus internetStatus = new InternetConnectivityStatus();
		internetStatus.isOnline(Settings.this);

		//Edit Bluetooth state
		btText = (TextView) findViewById(R.id.bluetoothText);
		if(ConnectionService.alive)
			btText.setText("On");
		else
			btText.setText("Off");


		// Edit battery Image and set onclickListener
		batteryImage = (ImageView) findViewById(R.id.batteryicon);
		batteryImage.setOnClickListener( new OnClickListener() {

			@Override
			public void onClick(View v) {
				String batteryText = null;
				try {
					AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
					builder.setCancelable(true);
					TextView batteryTextView = (TextView) findViewById(R.id.batteryText);
					batteryText = batteryTextView.getText().toString();
					batteryText = batteryText.subSequence(0, batteryText.length()-1).toString();
					if (Long.valueOf(batteryText) <= 20 && Long.valueOf(batteryText) > 5)
						builder.setMessage("Your battery is running low, please charge your device.");
					else if (Long.valueOf(batteryText) <= 5)
						builder.setMessage("Your battery is depleted, please charge your device.");
					else
						builder.setMessage("Your battery levels are good.");
					builder.setNeutralButton("Close", null);
					builder.create().show();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		wifiImage = (ImageView) findViewById(R.id.wifiicon);
		wifiImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				WifiManager wifiManager = (WifiManager)Settings.this.getSystemService(Context.WIFI_SERVICE);
				if(wifiManager.isWifiEnabled() ) {
					wifiManager.setWifiEnabled(false);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_OFF"));
				}
				else {
					wifiManager.setWifiEnabled(true);
					sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("wifi_state", "WIFI_ON"));
				}
			}
		});


	}


	//	code for header ****************
	private class ConnectivityHeaderReceiver extends BroadcastReceiver {

		Animation batteryAnimation ;

		@Override
		public void onReceive(Context arg0, Intent intent) {
			if(intent.hasExtra("wifi_state")) {
				if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_OFF")) {
					TextView wifiText = (TextView) findViewById(R.id.wifiText);
					wifiText.setText("Off");
					wifiText.setTextColor(Color.parseColor("#b85c2f"));
				}
				else
					if(intent.getStringExtra("wifi_state").equalsIgnoreCase("WIFI_ON")) {
						wifiText = (TextView) findViewById(R.id.wifiText);
						wifiText.setText("On");
						wifiText.setTextColor(Color.parseColor("#2D9C3E"));
					}
			}

			if(intent.hasExtra("bluetooth_state")) {
				if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_OFF")) {
					btText = (TextView) findViewById(R.id.bluetoothText);
					btText.setText("Off");
				}
				else
					if(intent.getStringExtra("bluetooth_state").equalsIgnoreCase("BT_ON")) {
						btText = (TextView) findViewById(R.id.bluetoothText);
						btText.setText("On");
					}
			}


			if(intent.hasExtra("battery_level")) {
				batteryAnimation = AnimationUtils.loadAnimation(Settings.this, R.anim.battery_animation);

				Long batteryLevel = intent.getLongExtra("battery_level", Long.valueOf("-1"));
				batteryText = (TextView) findViewById(R.id.batteryText);
				batteryImage = (ImageView) findViewById(R.id.batteryicon);

				if(batteryLevel != null && batteryLevel != -1) {
					batteryText.setText(batteryLevel.toString()+"%");
					if(batteryLevel>20) {
						batteryImage.clearAnimation();
						batteryText.setTextColor(Color.parseColor("#2D9C3E"));
						if(batteryLevel<=100 && batteryLevel>80)
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_100);
						else
							if(batteryLevel<=80 && batteryLevel>60)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_80);
							else
								if(batteryLevel<=60 && batteryLevel>40)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_60);
								else
									if(batteryLevel<=40 && batteryLevel>20)
										batteryImage.setBackgroundResource(R.drawable.battery_horizontal_40);	
					}
					else {
						batteryText.setTextColor(Color.parseColor("#b85c2f"));
						if(batteryLevel<=20 && batteryLevel>10) {
							batteryImage.clearAnimation();
							batteryImage.setBackgroundResource(R.drawable.battery_horizontal_20);
						}
						else {
							batteryImage.startAnimation(batteryAnimation);
							if(batteryLevel<=10 && batteryLevel>5)
								batteryImage.setBackgroundResource(R.drawable.battery_horizontal_10);
							else
								if(batteryLevel<=5 && batteryLevel>=0)
									batteryImage.setBackgroundResource(R.drawable.battery_horizontal_0);
						}
					}
				}
			}
		}
	}


	@Override
	protected void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}
	
	
	
	
}
