package nephron.mobile.application;


import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class MessageConverter {

	public MessageConverter(){}
	
 	public String BytesToString(byte[] a){
        String result = new String(Hex.encodeHex(a)); 
		return result;//HexToString(result); 
  }   
 	
	public byte[] StringToBytes(String a){
		  byte[] data = null;
	        char[] send=(a).toCharArray();
	     
				try {
					data=Hex.decodeHex(send);
				} catch (DecoderException e) {
					e.printStackTrace();
				} 
		return data;
}  
	
	
       public String stringToHex(String str) { 
	    	  char[] chars = str.toCharArray();
	    	  StringBuffer strBuffer = new StringBuffer();
	    	  for (int i = 0; i < chars.length; i++) {
	    	    strBuffer.append(Integer.toHexString((int) chars[i]));
	    	  }
	    	  return strBuffer.toString();
	    	  }
	    
	    
	    public String HexToString(String hex){
	    	 
	  	  StringBuilder sb = new StringBuilder();
	  	  StringBuilder temp = new StringBuilder();
	   
	  	  //49204c6f7665204a617661 split into two characters 49, 20, 4c...
	  	  for( int i=0; i<hex.length()-1; i+=2 ){
	   
	  	      //grab the hex in pairs
	  	      String output = hex.substring(i, (i + 2));
	  	      //convert hex to decimal
	  	      int decimal = Integer.parseInt(output, 16);
	  	      //convert the decimal to character
	  	      sb.append((char)decimal);
	   
	  	      temp.append(decimal);
	  	  }
	  	  System.out.println("Decimal : " + temp.toString());
	   
	  	  return sb.toString();
	    }
	    
}
