package nephron.mobile.datafunctions;

import nephron.mobile.application.utils.DataTransformationUtils;

public class MsgConfigureWAKD extends OutgoingWakdMsg {

	private static final long serialVersionUID = 8598583075931094670L;
	
	private byte[] configParams;

	public MsgConfigureWAKD(int id, WakdCommandEnum command, String configuration) {
		this.header.setId(id);
		this.header.setCommand(command);
		int size = (configuration.length() / 2) + 6;
		this.header.setSize(size);
		this.setConfigParams(DataTransformationUtils.convertStringToByteArray(configuration));
	}
	
	

	@Override
	public byte[] encode() {
		byte[] headerBytes = new byte[6];
		headerBytes[0] = this.header.getRecipient().getValue();
		System.arraycopy(ByteUtils.intToByteArray(this.header.getSize(), 2), 0, headerBytes, 1, 2);
		headerBytes[3] = this.header.getCommand().getValue();
		headerBytes[4] = ByteUtils.intToByte(this.header.getId());
		headerBytes[5] = this.header.getSender().getValue();
		int commandSize = (this.getConfigParams().length) + 6;
		byte[] command = new byte[commandSize];
		System.arraycopy(headerBytes, 0, command, 0, headerBytes.length);
		System.arraycopy(this.getConfigParams(), 0, command, headerBytes.length, this.getConfigParams().length);
		return command;
	}

	public byte[] getConfigParams() {
		return configParams;
	}


	public void setConfigParams(byte[] configParams) {
		this.configParams = configParams;
	}

}