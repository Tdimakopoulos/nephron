package nephron.mobile.datafunctions;

public class MsgWeightDataOk extends OutgoingWakdMsg {

	private static final long serialVersionUID = 8598583075931094670L;
	
	private int weight, bodyFat, WSBatteryLevel;

	public MsgWeightDataOk(int id, WakdCommandEnum command, int weight, int bodyFat, int WSBatteryLevel) {
		this.header.setId(id);
		this.header.setCommand(command);
		this.header.setSize(6);
		this.weight = weight;
		this.bodyFat = bodyFat;
		this.WSBatteryLevel = WSBatteryLevel;
	}
	

	@Override
	public byte[] encode() {

		byte[] bytes = new byte[15];
		bytes[0] = this.header.getRecipient().getValue();
		System.arraycopy(ByteUtils.intToByteArray(bytes.length, 2), 0, bytes, 1, 2);
		bytes[3] = this.header.getCommand().getValue();
		bytes[4] = ByteUtils.intToByte(this.header.getId());
		bytes[5] = this.header.getSender().getValue();
		System.arraycopy(ByteUtils.intToByteArray(weight, 2), 0, bytes, 6, 2);
		bytes[8] = ByteUtils.intToByte(this.bodyFat);
		System.arraycopy(ByteUtils.intToByteArray(WSBatteryLevel, 2), 0, bytes, 9, 2);
		bytes[11] = ByteUtils.intToByte(1);
		bytes[12] = ByteUtils.intToByte(2);
		bytes[13] = ByteUtils.intToByte(3);
		bytes[14] = ByteUtils.intToByte(4);
		
		return bytes;
	}

}