package nephron.mobile.datafunctions;
 

public abstract class OutgoingWakdMsg extends WakdMsg {

	private static final long serialVersionUID = 3601616369525064685L;
	
	protected OutgoingWakdMsg() {
		this.header = new Header();
		this.header.setSender(WhoEnum.SP);
		this.header.setRecipient(WhoEnum.MB);
	}

}

