package nephron.mobile.datafunctions;

public enum MeasurementsCodesEnum {

	UNDEFINED,

	//****************   REST MEASUREMENTS   ****************
	WAKD_CURRENT_STATE,
	WAKD_CUR_OP_STATE,

	//****************   WEIGHT   ****************
	WEIGHT,
	BODY_FAT,
	WS_BATTERY_LEVEL,
	WSSTATUS,

	//****************   ECG   ****************
	HEART_RATE,
	RESPIRATION_RATE ,
	ACTIVITY_LEVEL,

	//****************   BLOOD PRESSURE   ****************
	BLOODP_SYSTOLIC,
	BLOODP_DIASTOLIC,

	//****************   WAKD PHYSICAL MEASUREMENTS   ****************   
	CALCIUM_IN,
	CALCIUM_OUT,

	POTASSIUM_IN,
	POTASSIUM_OUT,

	PH_IN,
	PH_OUT,

	UREA_IN,
	UREA_OUT,

	TEMPERATURE_IN,
	TEMPERATURE_OUT,

	//****************   WAKD PHYSIOLOGICAL MEASUREMENTS   ****************
	CONDUCTIVITY_DIALYSATE,
	VITAMIN_C,
	PRESSURE_DIALYSATE,
	COND_TEMP_DIALYSATE,
	PRESSURE_BLOODLINE,
	TEMP_BLOODLINE_IN,
	TEMP_BLOODLINE_OUT,

	//****************   WAKD PHYSICAL SENSOR DATA MEASUREMENTS   ****************
	PRESSURE_FCO,	// NOT EXPLAINED YET
//	PRESSURE_BCI,	// NOT EXPLAINED YET
	PRESSURE_BCO,	// NOT EXPLAINED YET
//	TEMPERATURE_IN_PHY_SENSOR,	// NOT EXPLAINED YET
//	TEMPERATURE_OUT_PHY_SENSOR,
//	CONDUCTIVITY_FCQ,
//	CONDUCTIVITY_PT
	;


	public static MeasurementsCodesEnum getMeasurementsCodesEnum(int value) {

		switch (value) {

		// PHYSICAL SENSOR MEASUREMENTS
		case (int)302:
			return PRESSURE_FCO;
//		case (int)303:
//			return PRESSURE_BCI;
		case (int)304:
			return PRESSURE_BCO;
//		case (int)305:
//			return TEMPERATURE_IN_PHY_SENSOR;
//		case (int)306:
//			return TEMPERATURE_OUT_PHY_SENSOR;
//		case (int)308:
//			return CONDUCTIVITY_FCQ;
//		case (int)309:
//			return CONDUCTIVITY_PT;

		//	REST MEASUREMENTS
		case (int)41:
			return WAKD_CURRENT_STATE;
		case (int)42:
			return WAKD_CUR_OP_STATE;

		//	WEIGHT
		case (int)181:
			return WEIGHT;
		case (int)182:
			return BODY_FAT;
		case (int)183:
			return WS_BATTERY_LEVEL;
		case (int)184:
			return WSSTATUS;

		//	ECG
		case (int)231:
			return HEART_RATE;
		case (int)232:
			return RESPIRATION_RATE;
		case (int)233:
			return ACTIVITY_LEVEL;

		//BLOOD PRESSURE
		case (int)234:
			return BLOODP_SYSTOLIC;
		case (int)235:
			return BLOODP_DIASTOLIC;

		// WAKD PHYSICAL MEASUREMENTS
		case (int)911:
			return CALCIUM_IN;
		case (int)912:
			return CALCIUM_OUT;
		case (int)921:
			return POTASSIUM_IN;
		case (int)922:
			return POTASSIUM_OUT;
		case (int)931:
			return PH_IN;
		case (int)932:
			return PH_OUT;
		case (int)941:
			return UREA_IN;
		case (int)942:
			return UREA_OUT;
		case (int)951:
			return TEMPERATURE_IN;
		case (int)952:
			return TEMPERATURE_OUT;

		// WAKD PHYSIOLOGICAL MEASUREMENTS
		case (int)651:
			return CONDUCTIVITY_DIALYSATE;
		case (int)652:
			return VITAMIN_C;
		case (int)653:
			return PRESSURE_DIALYSATE;
		case (int)654:
			return COND_TEMP_DIALYSATE;
		case (int)655:
			return PRESSURE_BLOODLINE;
		case (int)656:
			return TEMP_BLOODLINE_IN;
		case (int)657:
			return TEMP_BLOODLINE_OUT;

		//TODO OTHER MEASUREMENTS TO BE DEFINED

		default:
			return UNDEFINED;
		}
	}

	public static int MeasurementEnum(MeasurementsCodesEnum value)
	{

		switch (value) {

		//	PHYSICAL SENSOR MEASUREMENTS
		case PRESSURE_FCO:
			return 302;
//		case PRESSURE_BCI:
//			return 303;
		case PRESSURE_BCO:
			return 304;
//		case TEMPERATURE_IN_PHY_SENSOR:
//			return 305;
//		case TEMPERATURE_OUT_PHY_SENSOR:
//			return 306;
//		case CONDUCTIVITY_FCQ:
//			return 308;
//		case CONDUCTIVITY_PT:
//			return 309;

		//	REST MEASUREMENTS
		case WAKD_CURRENT_STATE:
			return 41;
		case WAKD_CUR_OP_STATE:
			return 42;

		//	WEIGHT
		case WEIGHT:
			return 181;
		case BODY_FAT:
			return 182;
		case WS_BATTERY_LEVEL:
			return 183;
		case WSSTATUS:
			return 184;

		//	ECG
		case HEART_RATE:
			return 231;
		case RESPIRATION_RATE:
			return 232;
		case ACTIVITY_LEVEL:
			return 233;

		//BLOOD PRESSURE
		case BLOODP_SYSTOLIC:
			return 234;
		case BLOODP_DIASTOLIC:
			return 235;

		// WAKD PHYSICAL MEASUREMENTS
		case CALCIUM_IN:
			return 911;
		case CALCIUM_OUT:
			return 912;
		case POTASSIUM_IN:
			return 921;
		case POTASSIUM_OUT:
			return 922;
		case PH_IN:
			return 931;
		case PH_OUT:
			return 932;
		case UREA_IN:
			return 941;
		case UREA_OUT:
			return 942;
		case TEMPERATURE_IN:
			return 951;
		case TEMPERATURE_OUT:
			return 952;

		// WAKD PHYSIOLOGICAL MEASUREMENTS
		case CONDUCTIVITY_DIALYSATE:
			return 651;
		case VITAMIN_C:
			return 652;
		case PRESSURE_DIALYSATE:
			return 653;
		case COND_TEMP_DIALYSATE:
			return 654;
		case PRESSURE_BLOODLINE:
			return 655;
		case TEMP_BLOODLINE_IN:
			return 656;
		case TEMP_BLOODLINE_OUT:
			return 657;
		default:
			return 0;
		}
		
	}
}