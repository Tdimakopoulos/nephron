package nephron.mobile.backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import nephron.mobile.application.GlobalVar;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class SPLogin {

	public Handler handler;
	static boolean loginStatus = false;

	Handler mHandler;
	String key;
	Context context;

	final int BACKEND_AUTH_OK = 2;
	final int BACKEND_AUTH_ERROR = 3;

	public SPLogin(Handler handler, Context context) {
		this.mHandler = handler;
		this.context = context;
		this.key = retrieveKey();
	}

	public void run() {
		
		NephronSOAPClient sClient = new NephronSOAPClient(context);
		String response = sClient.executeBackendLoginRequest(key);
		Message msg = new Message();
		msg.setTarget(mHandler);
		Intent loginIntent = new Intent("nephron.mobile.application.WakStatusEvent");
		if(response!=null) {
			if(response.equals("NO_USER_FOUND")) {
				msg.what = BACKEND_AUTH_ERROR;
				((GlobalVar)context.getApplicationContext()).setBackendAuthenticated(false);
				loginIntent.putExtra("be_con_status", "Active, not logged in");
			}
			else {
				msg.what = BACKEND_AUTH_OK;
				((GlobalVar)context.getApplicationContext()).setBackendAuthenticated(true);
				loginIntent.putExtra("be_con_status", "Active, logged in");
			}
		}
		else {
			msg.what = BACKEND_AUTH_ERROR;
			((GlobalVar)context.getApplicationContext()).setBackendAuthenticated(false);
			loginIntent.putExtra("be_con_status", "Active, not logged in");
		}
		msg.sendToTarget();
		context.sendBroadcast(loginIntent);
	}

	private String retrieveKey() {

		String filename = "key.pki";
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,filename);

		//Read text from file
		StringBuilder sbuilder = new StringBuilder();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				sbuilder.append(line);
			}
		}
		catch (IOException e) {
			Log.d("PKI_FILE_HANDLER", "Error occurred while opening file");
			e.printStackTrace();
		}
		
		String skey = sbuilder.toString();
		
		return skey;
	}

}
