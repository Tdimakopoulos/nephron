package nephron.mobile.backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.zip.Deflater;

import nephron.mobile.application.GlobalVar;
import nephron.mobile.application.alarms.AlarmsMsg;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;


public class LostConnectivityCommandManager extends Thread implements Serializable {

	private static final long serialVersionUID = -3878158368732984364L;

	Context context;
	InternetConnectivityStatus istatus = new InternetConnectivityStatus();
	NephronSOAPClient soapClient;
	int counter;

	public static List<AlarmsMsg> alerts;

	public LostConnectivityCommandManager(Context y) {
		this.context = y;
		istatus = new InternetConnectivityStatus();

	}

	public void run() {

		soapClient = new NephronSOAPClient(context);
		counter = 20;

		while (true) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}


			//FIXME REMOVE THIS WHEN APPLICATION HAS BATTERY INFO AVAILABLE
			/**********************************************************************************************************************/
			counter = 0 + (int)(Math.random() * ((30 - 0) + 1));
			context.sendBroadcast(new Intent("CONTROLS_CHANGED").putExtra("battery_level", Long.valueOf(String.valueOf(counter))));
			/**********************************************************************************************************************/


			String fileName = checkforFileAndCreate();
			File commandsFile = new File(fileName);
			
			// Check to see for commands in memory that have not been committed
			if(!((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().isEmpty()) {

				// If there are untransmitted commands append them to the file
				commitCommandListToFile(context, 
						((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList());
				Log.d("UNTRANSMITTED COMMANDS LIST", "UNTRANSMITTED COMMANDS AVAILABLE, WRITTEN TO FILE");
				// Exception occurs in the above only statement. If it does occur, 
				// then the command array will not be emptied
				((GlobalVar)context.getApplicationContext()).getUntransmittedCommandsList().clear();
				Log.d("UNTRANSMITTED COMMANDS LIST", "UNTRANSMITTED COMMANDS AVAILABLE, CLEARED");
			}
			else
				Log.d("UNTRANSMITTED COMMANDS LIST", "NO UNTRANSMITTED COMMANDS AVAILABLE");

			if(istatus.isOnline(context)) {

				// Check for untransmitted commands present in the file
				// transmit them.
				List<String> commandsList = convertFileToListForTransmission(commandsFile);
				if(commandsList != null && !commandsList.isEmpty()) {

					String response = soapClient.executeUploadCommandsFileContents(commandsList);
					if(response != null) {

						if(response.equalsIgnoreCase("OK"))
							// the commands list has been transmitted and received successfully at the backend
							eraseFileContents(commandsFile, context);
						else
							Log.e("SOAP UPLOAD FILE COMMANDS", "NULL OR NO RESPONSE FROM CLIENT");
					}

				}


				//Implement the mechanism for uploading several files
				String fileSeparator = System.getProperty("file.separator");
				String commandsDir = Environment.getExternalStorageDirectory() + fileSeparator + "Nephron" + fileSeparator + "ECGData";
				File ecgDir = new File(commandsDir);
				if(ecgDir.exists()) {

					SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");

					if(ecgDir.isDirectory()) {
						File[] filesList = ecgDir.listFiles();
						if(filesList.length> 0) {
							for(File file : filesList) {
								String fileName2Edit = file.getName();

								File currentECGFile = ((GlobalVar)context.getApplicationContext()).getEcgFile();
								if(currentECGFile!= null) {
									if(currentECGFile.getName().equalsIgnoreCase(fileName2Edit))
										continue;
								}
								fileName2Edit = fileName2Edit.replace("ECGDiagramData_", "");
								fileName2Edit = fileName2Edit.replace(".txt", "");
								try {
									Long fileDate = format.parse(fileName2Edit).getTime();
									compressAndTransmitFile(file, fileDate);
								} catch (ParseException e) {
									e.printStackTrace();
									Log.e("LOST CONNECTIVITY COMMAND MANAGER","Error while parsing date from file");
								}
							}
						}
					}
				}
			}
		}
	}

	//***********************************************************************************************************

	public String checkforFileAndCreate() {
		String fileSeparator = System.getProperty("file.separator");
		String commandsDir = Environment.getExternalStorageDirectory() + fileSeparator + "Nephron";
		File commandsDirectory = new File(commandsDir);
		File commandsFile = new File(commandsDirectory, "commandsList.txt");

		if(!commandsDirectory.exists()) {
			commandsDirectory.mkdirs();
		}

		if(!commandsFile.exists()) {
			Log.e("DIRECTORY STRUCTURE ","COMMANDS FILE NOT PRESENT");

			try {
				commandsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				Log.e("DIRECTORY STRUCTURE ","EXCEPTION IN CREATE NEW FILE");
			}
		}

		if(commandsFile.exists())
			return commandsDir + fileSeparator + "commandsList.txt";
		else 
			return null;
	}

	//***********************************************************************************************************

	private void commitCommandToFile(Context ctx, String command)
			throws FileNotFoundException, IOException {

		String fileSeparator = System.getProperty("file.separator");
		String commandsFileName = Environment.getExternalStorageDirectory() + fileSeparator + "Nephron" + fileSeparator + "commandsList.txt";
		File commandsFile = new File(commandsFileName);
		FileOutputStream fOut = new FileOutputStream(commandsFile, true);
		OutputStreamWriter osw = new OutputStreamWriter(fOut);
		String newLine = System.getProperty("line.separator");
		Log.d("****** COMMAND FROM SINGLE WRITER*******", command);
		osw.write(command+newLine);
		osw.flush();
		osw.close();			            
	}

	//***********************************************************************************************************

	private void commitCommandListToFile(Context ctx, CopyOnWriteArrayList<byte[]> commandsList)  {

		String fileSeparator = System.getProperty("file.separator");

		try {
			FileOutputStream fOut;
			File commandsFile = new File(Environment.getExternalStorageDirectory()+
					fileSeparator + "Nephron" + fileSeparator + "commandsList.txt");

			fOut = new FileOutputStream(commandsFile, true);

			OutputStreamWriter osw = new OutputStreamWriter(fOut);
			String newLine = System.getProperty("line.separator");

			for(byte[] command : commandsList) {
				String commandStr = byteArray2HexString(command);
				Log.d("****** COMMAND FROM MULTIPLE WRITER*******", commandStr);
				osw.write(commandStr+newLine);
				osw.flush();
			}
			osw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	//***********************************************************************************************************

	public List<String> convertFileToListForTransmission(File file) {

		List<String> commandsList = new ArrayList<String>();
		try {
			FileReader reader = new FileReader(file);
			BufferedReader in = new BufferedReader(reader);
			while (in.ready()) {
				String command = in.readLine();
				commandsList.add(command);
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch 
			e.printStackTrace();
			return null;
		}
		if(commandsList.isEmpty())
			Log.e("convertFileToListForTransmission", "THE LIST IS EMPTY");
		else
			Log.e("convertFileToListForTransmission", "NON EMPTY LIST");
		return commandsList;
	}

	//***********************************************************************************************************

	public void eraseFileContents(File commandsFile, Context ctx) {

		if(commandsFile.exists()) {

			FileOutputStream fOut;
			try {
				fOut = new FileOutputStream(commandsFile, false);
				OutputStreamWriter osw = new OutputStreamWriter(fOut);
				osw.write("");
				osw.flush();
				osw.close();
				Log.e("THE COMMANDS FILE HAS BEEN ERASED", "THE COMMANDS FILE HAS BEEN ERASED");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//***********************************************************************************************************

	public boolean eraseECGFileFromFilesystem(File commandsFile, Context ctx) {

		if(commandsFile.exists()) 
			return commandsFile.delete();
		else
			return false;

	}

	//***********************************************************************************************************

	private String readFileContents(File ecgFile) throws IOException {

		BufferedReader reader = new BufferedReader( new FileReader (ecgFile));
		String         line = null;
		StringBuilder  stringBuilder = new StringBuilder();
		String         ls = System.getProperty("line.separator");

		while( ( line = reader.readLine() ) != null ) {
			stringBuilder.append( line );
			stringBuilder.append( ls );
		}

		return stringBuilder.toString();
	}

	//***********************************************************************************************************

	private String compressAndTransmitFile(File ecgFile, Long fileDate) {

		String fileContentsAsString = new String();
		Long zippedContentsLength = Long.valueOf(0);
		Long contentsLength = Long.valueOf(0);
		byte[] container;

		try {
			fileContentsAsString = readFileContents(ecgFile);
		} catch (IOException e) {
			e.printStackTrace();
			return "Error while reading ECG Measurements from file.";
		}

		if(fileContentsAsString != null) {
			if(fileContentsAsString.length() > 0 ) {
				contentsLength = (long) fileContentsAsString.getBytes().length;
				Log.d("FILE CONTENTS LENGTH", String.valueOf(fileContentsAsString.length()));
				container = new byte[fileContentsAsString.getBytes().length];

				try {
					zippedContentsLength = Long.valueOf(compressString(fileContentsAsString, container, fileContentsAsString.length()));
					Log.d("ZIPPED FILE CONTENTS LENGTH", String.valueOf(zippedContentsLength));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					return "Error while compressing the ECG Measurements file before transmission.";
				}


				if(zippedContentsLength > 0) {

					String response = null;
					try {
						NephronSOAPClient client = new NephronSOAPClient(context);
						response = client.executeUploadFileRequest(container, contentsLength, fileDate);
					} catch (Exception e) {
						e.printStackTrace();
						return "Error while uploading the ECG Measurements file to backend.";
					}

					if(response!=null) {

						if(response.equalsIgnoreCase("Saved !!")){
							eraseECGFileFromFilesystem(ecgFile, context);
							Log.e("ECG TRANSMISSION", "ECG Measurements have been uploaded successfully.");
							return "ECG Measurements have been uploaded successfully.";
						}
						else {
							//TODO Failure, file could not be uploaded
							Log.e("ECG TRANSMISSION", "Error while saving the ECG Measurements file to backend.");
							return "Error while saving the ECG Measurements file to backend.";
						}

					}

				}
			}
		}
		else
			return "Reading from file produced a null string";

		return "Unknown error has occured while trying to upload the ECG Measurements file.";
	}

	//***********************************************************************************************************

	public static int compressString(String data, byte[] output, int len) throws UnsupportedEncodingException {
		Deflater deflater = new Deflater();
		deflater.setInput(data.getBytes("UTF-8"));
		deflater.finish();
		return deflater.deflate(output);
	}

	//***********************************************************************************************************

	public static String byteArray2HexString(byte[] ba) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < ba.length; i++) {
			int hbits = (ba[i] & 0x000000f0) >> 4;
		int lbits = ba[i] & 0x0000000f;

		sb.append("" + hexChars[hbits] + hexChars[lbits]);
		}
		return sb.toString();
	}

	//***********************************************************************************************************

	private static char[] hexChars = {
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
	};


}
