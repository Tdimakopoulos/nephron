package nephron.mobile.backend;

import nephron.mobile.application.R.drawable;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class BackendInteractionHandler extends Handler {

	final int BACKEND_SHUTDOWN = 1;
	final int WAKD_CONFIGURATION = 2;

	private AlertDialog alertDialog;
	private AlertDialog.Builder dialogBuilder;
	Bundle bundle;
	Context context;

	public BackendInteractionHandler(Context context) {
		super();
		this.context = context;
		this.dialogBuilder = new AlertDialog.Builder(this.context);
	}

	public void handleMessage(android.os.Message msg) {

		if(alertDialog != null) {
			if(alertDialog.isShowing()){
				alertDialog.dismiss();
			}
		}

		switch (msg.what) {

		case BACKEND_SHUTDOWN:
			bundle = msg.getData();
			if(bundle.containsKey("commandID") && bundle.containsKey("command")) {

				Intent shutdownIntent = new Intent();
				shutdownIntent.setClass(context, BackendShutdownWAKD.class);
				shutdownIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				shutdownIntent.setAction("BACKEND_SHUTDOWN");
				shutdownIntent.putExtras(bundle);
				context.startActivity(shutdownIntent);
			}
			break;
			
		case WAKD_CONFIGURATION:
			bundle = msg.getData();
			if(bundle.containsKey("commandID") && bundle.containsKey("command")) {

				Intent configureWAKDIntent = new Intent();
				configureWAKDIntent.setClass(context, BackendConfigureWAKD.class);
				configureWAKDIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				configureWAKDIntent.setAction("WAKD_CONFIGURATION");
				configureWAKDIntent.putExtras(bundle);
				context.startActivity(configureWAKDIntent);
			}
			break;
			
		default:
			dialogBuilder.setTitle("General Backend Info");
			dialogBuilder.setIcon(drawable.erroricon);
			dialogBuilder.setMessage("Unknown Event occured");
			dialogBuilder.setPositiveButton("OK", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			break;
		}

		alertDialog = dialogBuilder.create();
		alertDialog.show();
	};

}
