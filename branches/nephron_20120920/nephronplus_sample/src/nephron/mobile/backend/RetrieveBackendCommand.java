package nephron.mobile.backend;

import java.util.TimerTask;

import nephron.mobile.application.GlobalVar;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;

public class RetrieveBackendCommand extends TimerTask {

	Context context;
	BackendInteractionHandler bhandler;
	final int BACKEND_SHUTDOWN = 1;
	final int BACKEND_CONFIGUREWAKD =2;

	public RetrieveBackendCommand(Context context) {
		super();
		this.context = context;
		this.bhandler = new BackendInteractionHandler(this.context);
	}

	@Override
	public void run() {
		
		Log.e("RETRIEVEBACKENCOMMAND","RUNNING");
		NephronSOAPClient soapClient = new NephronSOAPClient(context);
		String commandResponse = soapClient.executeRetrieveCommandRequest(((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId());
		
//		commandResponse = "2100:010010230204	2400000087	250000009B";

		if(commandResponse != null) {
			
			Log.e("RESPONSE",commandResponse);
			
			Message msg = null;
			
			try {
				msg = decodeBackendCommand(commandResponse);
			} catch (Exception e) {
				Log.e("Retrieve backendCommand in Client", "Exception while decoding the command");
			}
			
			if(msg != null) {
				msg.setTarget(bhandler);
				if(!((GlobalVar)context.getApplicationContext()).isOperationInProgress()) {
					((GlobalVar)context.getApplicationContext()).setOperationInProgress(true);
					msg.sendToTarget();
				}
			}
			else
				Log.e("RetrieveBackendCommand", "Unable to determine incoming command");
		}
	}


	private Message decodeBackendCommand(String retrievedCommand) {

		int sepIndex = retrievedCommand.indexOf(":") + 1;
		String commandID = retrievedCommand.substring(0, retrievedCommand.indexOf(":"));
		String command = retrievedCommand.substring(sepIndex, retrievedCommand.length());
		Bundle b = new Bundle();
		b.putString("commandID", commandID);
		b.putString("command", command);
		
		Message msg = new Message();
		CharSequence hexCommand = command.subSequence(6, 8);
		if( hexCommand.toString().equalsIgnoreCase("02") )
			msg.what = bhandler.BACKEND_SHUTDOWN;
		else if(hexCommand.toString().equalsIgnoreCase("23"))
			msg.what = bhandler.WAKD_CONFIGURATION;
		else
			return null;
		msg.setData(b);
		return msg;
	}

}