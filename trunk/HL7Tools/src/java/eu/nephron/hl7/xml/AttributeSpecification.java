/*
 * AttributeSpecification.java : A HL72XML specification component class.
 */

package eu.nephron.hl7.xml;

import eu.nephron.hl7.XMLUtils.AttributeMap;
import eu.nephron.hl7.message.HL7Message;
import java.util.ArrayList;

import org.w3c.dom.Node;




class AttributeSpecification extends HL72XMLSpecificationItem {

   AttributeSpecification(String desginatorStr, String nameStr, String valueStr) {
      super(desginatorStr, nameStr, valueStr);
   } // AttributeSpecification constructor

   AttributeSpecification(Node node) {
      super(node);
   } // AttributeSpecification constructor

   protected String toXMLString() {
      String argV = "";
      if (!hasName()) return "";
      if (hasValue()) argV = value();
      return toXMLStringA(argV);
   } // toXMLString


   private String toXMLStringA(String str) {
      if (!hasName()) return "";

      return new StringBuffer(name())
              .append("=\"")
              .append(str)
              .append("\"").toString();
   } // toXMLString

   protected String toXMLString(HL7Message msg) {
      return toXMLStringA(getContent(msg));
   } // toXMLString


   static AttributeMap attributeMap(ArrayList<AttributeSpecification> attributes, HL7Message msg) {
      if (attributes == null || attributes.isEmpty()) return null;
      AttributeMap map = new AttributeMap();
      for (AttributeSpecification spec : attributes) map.add(spec.name(), spec.getContent(msg));
      return map;
   } // attributeMap


} // AttributeSpecification
