/*
 *  ElementSpecification.java : A HL72XML specification component class.
 */

package eu.nephron.hl7.xml;

import eu.nephron.hl7.XMLUtils.AttributeMap;
import eu.nephron.hl7.XMLUtils.XMLUtils;
import eu.nephron.hl7.message.HL7Message;
import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



class ElementSpecification extends HL72XMLSpecificationItem {
   ArrayList<AttributeSpecification>   attributes;
   ArrayList<ElementSpecification>     subElements;

   ElementSpecification(String desginatorStr, String nameStr, String valueStr) {
      super(desginatorStr, nameStr, valueStr);
   } // ElementSpecification constructor

   ElementSpecification(Node node) {
      super(node);

      if (node.hasChildNodes()) {
         NodeList kids = node.getChildNodes();
         int numKids = kids.getLength();
         for (int index = 0; index < numKids; ++index) parseChildNode(kids.item(index));
      } // if
   } // ElementSpecification constructor


   private void parseChildNode(Node node) {
      if (node == null) return;

      String nodeName = node.getNodeName();

      if (nodeName.equalsIgnoreCase(HL72XMLSpecificationItem.NAME_ELEMENT)) {
         addSubElement(new ElementSpecification(node));
      } else if (nodeName.equalsIgnoreCase(HL72XMLSpecificationItem.NAME_ATTRIBUTE)) {
         addAttribute(new AttributeSpecification(node));
      } // if - else if
   } // parseChildNode


   private void addSubElement(ElementSpecification elementSpec) {
      if (elementSpec == null) return;
      if (subElements == null) subElements = new ArrayList<ElementSpecification>();
      subElements.add(elementSpec);
   } // addSubElement


   private void addAttribute(AttributeSpecification attributeSpec) {
      if (attributeSpec == null) return;
      if (attributes == null) attributes = new ArrayList<AttributeSpecification>();
      attributes.add(attributeSpec);
   } // addAttribute


   private String toXMLString(String content, AttributeMap attribs) {
      return XMLUtils.elementString(name(), attribs, content);
   } // toXMLString


   String toXMLString(HL7Message msg) {
      String contentStr = hasSubElements()
                           ? toSubElementalContentString(msg)
                           : getContent(msg);

      AttributeMap attr = hasAttributes()
                           ? AttributeSpecification.attributeMap(attributes, msg)
                           : null;
      return toXMLString(contentStr, attr);
   } // toXMLString


   private boolean hasAttributes() {
      return attributes != null && !attributes.isEmpty();
   } // hasAttributes

   private boolean hasSubElements() {
      return subElements != null && !subElements.isEmpty();
   } // hasSubElements

   private String toSubElementalContentString(HL7Message msg) {
      if (!hasSubElements()) return "";

      StringBuilder contentBuilder = new StringBuilder();
      for (ElementSpecification element : subElements) {
         contentBuilder.append(element.toXMLString(msg));
      } // for

      return contentBuilder.toString();
   } // toSubElementalContentString

} // ElementSpecification
