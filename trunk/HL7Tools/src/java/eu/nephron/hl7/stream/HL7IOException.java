/*
 *  HL7IOException.java : An exception class for HL7 message streams.
 *
 *
 */

package eu.nephron.hl7.stream;

import java.io.IOException;
import java.net.SocketException;

/**
 * An exception class for HL7 message streams.
 * 
 */
public class HL7IOException extends IOException {
   /**
    * No error.
    */
   public static final int    NO_ERROR = 0;
   /**
    * Unspecified (or unknown) error.
    */
   public static final int    UNSPECIFIED_ERROR = 1;
   /**
    * HL7 transaction was rejected with a NAck message.
    */
   public static final int    HL7_NACK = 501;
   /**
    * The stream was closed at the time of the call.
    */
   public static final int    STREAM_CLOSED = 502;
   /**
    * The stream was in an inconsistent state at the time of the call.
    */
   public static final int    INCONSISTENT_STATE = 503;
   /**
    * The call causing this exception requested an operation which is
    * inconsistent with the characteristics of the context stream.
    */
   public static final int    INAPPROPRIATE_OPERATION = 504;
   /**
    * An attempt was made to operate on a HL7 message which had no type defined.
    */
   public static final int    NO_MSG_TYPE = 505;
   /**
    * An attempt was made to operate on a null message reference.
    */
   public static final int    NULL_MSG = 506;
   /**
    * An attempt was made to operate on an incorrectly identified HL7 message.
    */
   public static final int    WRONG_CTLID = 507;
   /**
    * An operation was attempted on a message that was not a valid HL7 message.
    */
   public static final int    NOT_VALID_MSG = 508;
   /**
    * An attempt was made to operate on a null stream reference.
    */
   public static final int    NULL_STREAM = 511;
   /**
    * An attempt was made to perform an operation inconsistent with the access
    * characteristics of the context stream. ie; read from a writer or write to
    * a reader.
    */
   public static final int    INAPPROPRIATE_DIRECTIVE = 514;
   /**
    * A URI parameter was not interperable within the context of the stream.
    */
   public static final int    UNINTERPERABLE_URI = 521;
   /**
    * The file towards which the offending operation was directed, was not found.
    */
   public static final int    FILE_NOT_FOUND = 701;
   /**
    * A file i/o error occurred.
    */
   public static final int    FILE_IO_ERROR = 702;
   /**
    * A specified host was not found.
    */
   public static final int    UNKNOWN_HOST = 901;
   /**
    * A general IO exception was thrown.
    */
   public static final int    IO_EXCEPTION = 1001;
   int                        errorType = NO_ERROR;


   /**
    * Creates a new exception having the argument message.
    * @param msg
    */
   public HL7IOException(String msg) {
      super(msg);
      errorType = HL7IOException.UNSPECIFIED_ERROR;
   } // HL7IOException


   /**
    * Creates a new exception having the argument message, which is associated
    * with the argument throwable cause.
    * @param msg
    * @param thrown
    */
   public HL7IOException(String msg, Throwable thrown) {
      super(msg, thrown);
      errorType = HL7IOException.UNSPECIFIED_ERROR;
   } // HL7IOException


   /**
    * Creates a new exception having the argument message and error type code.
    * @param msg
    * @param errorType
    */
   public HL7IOException(String msg, int errType) {
      super(msg);
      errorType = errType;
   } // HL7IOException


   /**
    * Creates a new exception having the argument message and error type code,
    * which is associated with the argument throwable cause.
    * @param msg
    * @param errType
    * @param thrown
    */
   public HL7IOException(String msg, int errType, Throwable thrown) {
      super(msg, thrown);
      errorType = errType;
   } // HL7IOException


   /**
    * Access to the exception's error type code.
    * @return the exception's error type code.
    */
   public int errorType() {
      return errorType;
   } // errorType


   public boolean isNetworkProblem() {
      return isSocketProblem(getCause());
   } // isNetworkProblem

   private static boolean isSocketProblem(Throwable cause) {
      if (cause == null) return false;
      if (cause instanceof SocketException) return true;
      return (isSocketProblem(cause.getCause()));
   } // isSocketProblem
   
} // HL7IOException
