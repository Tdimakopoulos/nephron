/*
 *  HL7FileWriter.java : A file writer class for HL7 message streams.
 *                       Writes or appends HL7 (v.2.x) transaction messages to a file.
 *
 *
 */

package eu.nephron.hl7.stream;


import eu.nephron.hl7.message.HL7Message;
import java.io.*;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 * Writes HL7Message objects to a file.
 */
public abstract class HL7FileWriter extends HL7StreamBase implements HL7Stream {
   private boolean   isAppender = false;
   File              file;
   BufferedWriter    writer;

   /**
    * Creates a new file writer object using the argument File object.
    * @param hl7File
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public HL7FileWriter(File hl7File) throws HL7IOException {
         this.initialize(hl7File);
   } // HL7FileWriter


   /**
    * Creates a new file writer object using the argument file name string.
    * @param hl7FileName
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public HL7FileWriter(String hl7FileName) throws HL7IOException {
      if (hl7FileName == null || hl7FileName.isEmpty()) {
         throw new IllegalArgumentException("HL7FileWriter(): file name not specified.");
      } // if

      this.initialize(new File(hl7FileName));
   } // HL7FileReader


   /**
    * Creates a new file writer object using the argument file URI.
    * @param fileURI
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public HL7FileWriter(URI fileURI) throws HL7IOException {
      HL7StreamURI streamURI = new HL7StreamURI(fileURI);

      if (!streamURI.canWriteFiles()) {
         throw new IllegalArgumentException("HL7FileWriter(" 
                                          + fileURI.toString()
                                          + "):URI cannot write files.");
      } // if

      if (streamURI.isFileAppenderURI()) {
         isAppender = true;
      } // if

      initialize(new File(streamURI.fileURIOf()) );
   } // HL7FileWriter


   // * Construction support.
   private void initialize(File hl7File) throws HL7IOException {
      this.mediaType = HL7Stream.FILE_TYPE;
      this.directive = HL7FileWriter.WRITER;
      if (this.isAppender == true) {
         this.directive = HL7Stream.APPENDER;
      } // if

      if (hl7File == null) {
         throw new HL7IOException("HL7FileWriter():File is null.");
      } // if

      this.file = hl7File;
   } // initialize


   /**
    * Opens the context file writer, as a writer.
    * @return a boolean success indicator. true if the operation is successful,
    * otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean open() throws HL7IOException {
      return this.open(this.isAppender);
   } // open


   /**
    * Opens the context file writer, as a writer, or as an appender.
    * @param append a boolean flag indicating whether the writer stream is to be
    * opened as a writer (false) or as an appender (true);
    * @return a boolean success indicator. true if the operation is successful,
    * otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean open(boolean append) throws HL7IOException {
      if (file == null) {
         throw new HL7IOException( "HL7FileWriter.open: file name not specified." );
      } // if

      try {
         if (!file.exists()) createFile(file);
         writer = new BufferedWriter(new FileWriter(file, append) );
      } catch (IOException ioEx) {
         throw new HL7IOException(  "HL7FileWriter.open:IOException:"
                                 +  ioEx.getMessage(), ioEx);
      } // try - catch

      statusValue = HL7Stream.OPEN;
      return true;
   } // openWriter


   /**
    * Writes the argument HL7 message string to the context stream.
    * @param hl7Msg
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public void writeMsg(String hl7Msg) throws HL7IOException {
      if (this.writer == null || !this.isOpen()) {
         this.open();
      } // if
      
      if (this.writer == null) {
         throw new HL7IOException(  "HL7FileWriter.writeMsg: Null writer.",
                                    HL7IOException.INCONSISTENT_STATE);
      } // if

      try {
         // FileWriter always assumes default encoding is OK!
         this.writer.write( hl7Msg + "\n" );
         this.writer.flush();
      } catch (IOException ioEx) {
         throw new HL7IOException(  "HL7FileWriter.writeMsg: IOException", ioEx);

      } // try - catch
   } // writeMsg


   /**
    * Writes the argument HL7 message string to the context stream,
    * opening it using the argument file name string if the stream is not open
    * at the time of the call.
    * @param fileName
    * @param hl7Msg
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
  public void writeMsg(String fileName, String hl7Msg) throws HL7IOException {
      if (this.writer == null && this.statusValue == HL7FileWriter.UNINITIALIZED) {
         if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("HL7FileWriter.writeMsg: file not specified.");
         } // if

         this.initialize(new File(fileName));
         this.open();
      } // if

      this.writeMsg(hl7Msg);
   } // writeMsg


   /**
    * Write the argument HL7Message to the stream
    * @param msg
    * @return a boolean success indicator. true if the operation is successful,
    * otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean write(HL7Message msg) {
        try {
            this.writeMsg(msg.toString());
        } catch (HL7IOException ex) {
            //Logger.getLogger(HL7FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
      return true;
   } // write


   /**
    * This method always throws a INAPPROPRIATE_OPERATION HL7IOException.
    * @return true if the operation succeeded, otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public HL7Message read()  {
        try {
            throw new HL7IOException( "HL7FileWriter.read:Innapropriate operation.",
                                       HL7IOException.INAPPROPRIATE_OPERATION);
        } catch (HL7IOException ex) {
            //Logger.getLogger(HL7FileWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
   } // read


   /**
    * Closes the context stream writer.
    * @return true if the operation succeeded, otherwise false.
    * @throws eu.nephron.HL7Stream.HL7IOException
    */
   public boolean close()throws HL7IOException {
      if (!this.isOpen()) {
         return true;
      } // if

      try {
         this.writer.close();
      } catch (IOException ioEx) {
         throw new HL7IOException("HL7FileWriter.close:IOException", ioEx);
      } // try - catch

      this.statusValue = HL7FileWriter.CLOSED;
      return true;
   } // close

   private void createFile(File file) throws IOException {
      if (file.exists()) return;
      File parent = file.getParentFile();
      if (!parent.exists()) parent.mkdirs();
      file.createNewFile();
   } // createFile

} // HL7FileWriter
