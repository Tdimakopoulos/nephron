/*
 *
 *  HL7ElementLevel.java : A class of bounded ordered HL7 hierarchy levels.
 *
 */

package eu.nephron.hl7.message;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author tomd
 */
class HL7ElementLevel implements Iterator {
   public static final int SEGMENT      = 1,
                           FIELD        = 2,
                           REPETITION   = 3,
                           COMPONENT    = 4,
                           SUBCOMPONENT = 5;
   private int value;

   public HL7ElementLevel(int level) {
      _set(level);
   } // HL7ElementLevel
   
   private void _set(int level) {
      if (level < HL7ElementLevel.SEGMENT || level > HL7ElementLevel.SUBCOMPONENT) {
         throw new IllegalArgumentException("Illegal level:" + Integer.toString(level));
      } // if

      value = level;
   } // set

   void set(int level) { _set(level); }

   int get() {
      return value;
   } // get

   public boolean hasNext() {
      if (value > COMPONENT) return false;
      return true;
   } // hasNext

   public HL7ElementLevel next() throws NoSuchElementException {
      if (hasNext()) return new HL7ElementLevel(value + 1);
      throw new NoSuchElementException();
   } // next

   public void remove() {
      throw new UnsupportedOperationException("Not supported.");
   } // remove

   static HL7Element newElementAt(HL7ElementLevel level) {
      switch (level.value) {
         case SEGMENT      : return new HL7Segment();
         case FIELD        : return new HL7Field();
         case REPETITION   : return new HL7FieldRepetition();
         case COMPONENT    : return new HL7Component();
         case SUBCOMPONENT : return new HL7SubComponent();
         default: throw new IllegalArgumentException("No such HL7 element level:"
                                                   + Integer.toString(level.value));
      } // switch
   } // newElementAt

} // HL7ElementLevel

