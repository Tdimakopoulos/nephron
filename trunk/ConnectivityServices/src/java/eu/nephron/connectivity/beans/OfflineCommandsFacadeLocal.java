/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.connectivity.beans;

import eu.nephron.connectivity.database.OfflineCommands;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tom
 */
@Local
public interface OfflineCommandsFacadeLocal {

    void create(OfflineCommands offlineCommands);

    void edit(OfflineCommands offlineCommands);

    void remove(OfflineCommands offlineCommands);

    OfflineCommands find(Object id);

    List<OfflineCommands> findAll();

    List<OfflineCommands> findRange(int[] range);

    int count();
    
}
