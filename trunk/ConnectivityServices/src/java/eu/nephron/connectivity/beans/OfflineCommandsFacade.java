/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.connectivity.beans;

import eu.nephron.connectivity.database.OfflineCommands;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tom
 */
@Stateless
public class OfflineCommandsFacade extends AbstractFacade<OfflineCommands> implements OfflineCommandsFacadeLocal {
    @PersistenceContext(unitName = "ConnectivityServicesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OfflineCommandsFacade() {
        super(OfflineCommands.class);
    }
    
}
