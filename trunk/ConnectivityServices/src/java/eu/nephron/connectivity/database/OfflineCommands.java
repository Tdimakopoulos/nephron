/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.connectivity.database;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tom
 */
@Entity
public class OfflineCommands implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String IMEI;
    private String command;
    private String sorder;
    private String sdate;
    private String info1;
    private int ifield1;
    private int ifield2;
    private int ifield3;
    private long lfield1;
    private long lfield2;
    private long lfield3;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfflineCommands)) {
            return false;
        }
        OfflineCommands other = (OfflineCommands) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.connectivity.database.OfflineCommands[ id=" + id + " ]";
    }

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @return the sorder
     */
    public String getSorder() {
        return sorder;
    }

    /**
     * @param sorder the sorder to set
     */
    public void setSorder(String sorder) {
        this.sorder = sorder;
    }

    /**
     * @return the sdate
     */
    public String getSdate() {
        return sdate;
    }

    /**
     * @param sdate the sdate to set
     */
    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    /**
     * @return the info1
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * @param info1 the info1 to set
     */
    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    /**
     * @return the ifield1
     */
    public int getIfield1() {
        return ifield1;
    }

    /**
     * @param ifield1 the ifield1 to set
     */
    public void setIfield1(int ifield1) {
        this.ifield1 = ifield1;
    }

    /**
     * @return the ifield2
     */
    public int getIfield2() {
        return ifield2;
    }

    /**
     * @param ifield2 the ifield2 to set
     */
    public void setIfield2(int ifield2) {
        this.ifield2 = ifield2;
    }

    /**
     * @return the ifield3
     */
    public int getIfield3() {
        return ifield3;
    }

    /**
     * @param ifield3 the ifield3 to set
     */
    public void setIfield3(int ifield3) {
        this.ifield3 = ifield3;
    }

    /**
     * @return the lfield1
     */
    public long getLfield1() {
        return lfield1;
    }

    /**
     * @param lfield1 the lfield1 to set
     */
    public void setLfield1(long lfield1) {
        this.lfield1 = lfield1;
    }

    /**
     * @return the lfield2
     */
    public long getLfield2() {
        return lfield2;
    }

    /**
     * @param lfield2 the lfield2 to set
     */
    public void setLfield2(long lfield2) {
        this.lfield2 = lfield2;
    }

    /**
     * @return the lfield3
     */
    public long getLfield3() {
        return lfield3;
    }

    /**
     * @param lfield3 the lfield3 to set
     */
    public void setLfield3(long lfield3) {
        this.lfield3 = lfield3;
    }
    
}
