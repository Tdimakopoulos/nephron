/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.connectivity.ws;

import eu.nephron.connectivity.beans.OfflineCommandsFacadeLocal;
import eu.nephron.connectivity.database.OfflineCommands;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tom
 */
@WebService(serviceName = "OfflineCommandQ")
public class OfflineCommandQ {
    @EJB
    private OfflineCommandsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "offlinecommandprocessing")
    public String createwithstringasarray(@WebParam(name = "offlinecommands") String offlineCommandsarrays,
    @WebParam(name = "imei") String imei) {
        
        ArrayList<String> offlineCommandsarray = new  ArrayList<String>(Arrays.asList(offlineCommandsarrays.split(",")));
        
        for(int i=0;i<offlineCommandsarray.size();i++)
        {
            OfflineCommands offlineCommand= new OfflineCommands();
            offlineCommand.setCommand(offlineCommandsarray.get(i));
            offlineCommand.setIMEI(imei);
            offlineCommand.setIfield1(i);
            offlineCommand.setIfield2(i);
            offlineCommand.setIfield3(i);
            offlineCommand.setInfo1("N/A");
            offlineCommand.setLfield1(i);
            offlineCommand.setLfield2(i);
            offlineCommand.setLfield3(i);
            offlineCommand.setSdate((new Date()).toString());
            offlineCommand.setSorder(String.valueOf(i));
            ejbRef.create(offlineCommand);
        }
        return "ok";
    }
    
    @WebMethod(operationName = "createwitharray")
    public String createwitharray(@WebParam(name = "offlineCommandsarray") List<OfflineCommands> offlineCommandsarray) {
        for(int i=0;i<offlineCommandsarray.size();i++)
            ejbRef.create(offlineCommandsarray.get(i));
        return "ok";
    }
    
    @WebMethod(operationName = "create")
    public String create(@WebParam(name = "offlineCommands") OfflineCommands offlineCommands) {
        ejbRef.create(offlineCommands);
        return "ok";
    }

    @WebMethod(operationName = "edit")
    public String edit(@WebParam(name = "offlineCommands") OfflineCommands offlineCommands) {
        ejbRef.edit(offlineCommands);
        return "ok";
    }

    @WebMethod(operationName = "remove")
    public String remove(@WebParam(name = "offlineCommands") OfflineCommands offlineCommands) {
        ejbRef.remove(offlineCommands);
        return "ok";
    }

    @WebMethod(operationName = "find")
    public OfflineCommands find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<OfflineCommands> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<OfflineCommands> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
