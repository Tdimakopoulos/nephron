/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakdstatus;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;


/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class wakdstatusbean {

    private String outcomestatus;
    private String batterystatus;
    private String currentstatus;
    private String opstatus;
    private String temperature;
    private String scalebattery;

    public static byte[] convertStringToByteArray(String s) {

        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Creates a new instance of wakdstatusbean
     */
    public wakdstatusbean() {

       
    }

    /**
     * @return the outcomestatus
     */
    public String getOutcomestatus() {
        return outcomestatus;
    }

    /**
     * @param outcomestatus the outcomestatus to set
     */
    public void setOutcomestatus(String outcomestatus) {
        this.outcomestatus = outcomestatus;
    }

    /**
     * @return the batterystatus
     */
    public String getBatterystatus() {
        return batterystatus;
    }

    /**
     * @param batterystatus the batterystatus to set
     */
    public void setBatterystatus(String batterystatus) {
        this.batterystatus = batterystatus;
    }

    /**
     * @return the currentstatus
     */
    public String getCurrentstatus() {
        return currentstatus;
    }

    /**
     * @param currentstatus the currentstatus to set
     */
    public void setCurrentstatus(String currentstatus) {
        this.currentstatus = currentstatus;
    }

    /**
     * @return the opstatus
     */
    public String getOpstatus() {
        return opstatus;
    }

    /**
     * @param opstatus the opstatus to set
     */
    public void setOpstatus(String opstatus) {
        this.opstatus = opstatus;
    }

    /**
     * @return the temperature
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the scalebattery
     */
    public String getScalebattery() {
        return scalebattery;
    }

    /**
     * @param scalebattery the scalebattery to set
     */
    public void setScalebattery(String scalebattery) {
        this.scalebattery = scalebattery;
    }

    private String retrieveWAKDStatus(java.lang.String imei) {
        return "----";
    }

    public void shutdownWAKDListener(ActionEvent actionEvent) {
       
    }

    public String shutdown() {
      return null;
    }

    private void storePhoneCommand(java.lang.String imei, java.lang.String command, java.lang.Long ddate, java.lang.String param1, java.lang.String param2, java.lang.String param3, java.lang.String param4) {
        
    }
}
