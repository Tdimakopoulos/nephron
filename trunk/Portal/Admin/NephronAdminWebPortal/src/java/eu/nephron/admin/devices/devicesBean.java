/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.devices;

import eu.nephron.model.DevicesModel;
import eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7_Service;
import eu.nephron.webservices.KeyManagerService;
import eu.nephron.ws.medent.device.Device;
import eu.nephron.ws.medent.device.DeviceManagerSWS_Service;
import eu.nephron.ws.role.Role_Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class devicesBean {

    private List<DevicesModel> pList = new ArrayList<DevicesModel>();
    private DevicesModel selectedDevice;
    private String sznameadd;
    private String szserialadd;
    private String sztypeadd;
    private String number;
    private String numberfilter;

    /**
     * Creates a new instance of devicesBean
     */
    public devicesBean() {
        java.util.List<eu.nephron.ws.medent.device.Device> pFindAll = findAllDevice();
        for (int i = 0; i < pFindAll.size(); i++) {
            DevicesModel item = new DevicesModel();
            item.setIquantity(pFindAll.get(i).getQuantity());
            item.setSerial(pFindAll.get(i).getSerialNumber());
            item.setdID(pFindAll.get(i).getId());
            item.setSzCode(pFindAll.get(i).getCode());
            item.setSzName(pFindAll.get(i).getName());
            pList.add(item);

        }
    }

    public void clearAndReload() {
        pList.clear();
        java.util.List<eu.nephron.ws.medent.device.Device> pFindAll = findAllDevice();
        for (int i = 0; i < pFindAll.size(); i++) {
            DevicesModel item = new DevicesModel();
            item.setIquantity(pFindAll.get(i).getQuantity());
            item.setSerial(pFindAll.get(i).getSerialNumber());
            item.setdID(pFindAll.get(i).getId());

            item.setSzCode(pFindAll.get(i).getCode());
            item.setSzName(pFindAll.get(i).getName());

            System.out.print(" ---> " + numberfilter);
            
            if (numberfilter.equalsIgnoreCase("2")) {
                if (pFindAll.get(i).getCode().equalsIgnoreCase("SP")) {
                    System.out.print(" ---> " + numberfilter + " " + pFindAll.get(i).getCode());
                    pList.add(item);
                }
            }

            if (numberfilter.equalsIgnoreCase("1")) {
                if (pFindAll.get(i).getCode().equalsIgnoreCase("WAKD")) {
                    System.out.print(" ---> " + numberfilter + " " + pFindAll.get(i).getCode());
                    pList.add(item);
                }
            }

        }
    }

    private void refresh() {
        pList.clear();
        java.util.List<eu.nephron.ws.medent.device.Device> pFindAll = findAllDevice();
        for (int i = 0; i < pFindAll.size(); i++) {
            DevicesModel item = new DevicesModel();
            item.setIquantity(pFindAll.get(i).getQuantity());
            item.setSerial(pFindAll.get(i).getSerialNumber());
            item.setdID(pFindAll.get(i).getId());
            item.setSzCode(pFindAll.get(i).getCode());
            item.setSzName(pFindAll.get(i).getName());
            pList.add(item);

        }
    }

    private Long locate(String name) {
        Long pfindid = null;
        pList.clear();
        java.util.List<eu.nephron.ws.medent.device.Device> pFindAll = findAllDevice();
        for (int i = 0; i < pFindAll.size(); i++) {
            DevicesModel item = new DevicesModel();
            item.setIquantity(pFindAll.get(i).getQuantity());
            item.setSerial(pFindAll.get(i).getSerialNumber());
            if (name.equalsIgnoreCase(pFindAll.get(i).getSerialNumber())) {
                pfindid = pFindAll.get(i).getId();
            }
            item.setdID(pFindAll.get(i).getId());
            item.setSzCode(pFindAll.get(i).getCode());
            item.setSzName(pFindAll.get(i).getName());
            pList.add(item);

        }
        return pfindid;
    }

    public String canceldoctor() {


        return "Equipments?faces-redirect=true";
    }

    public String deletedevice() {
        removeDevice(selectedDevice.getdID());
        refresh();
        return "Equipments?faces-redirect=true";
    }

    public String editdevice() {
        System.out.println(selectedDevice.getSzName() + "  " + selectedDevice.getSerial() + "  " + selectedDevice.getIrole());
        sznameadd = selectedDevice.getSzName();
        szserialadd = selectedDevice.getSerial();
        if (selectedDevice.getIrole() == 1) {
            number = "1";
        }
        if (selectedDevice.getIrole() == 2) {
            number = "2";
        }
        System.out.println(sznameadd + "  " + szserialadd + " " + sztypeadd + "  " + number);
        return "Edit_Device?faces-redirect=true";
    }

    public String adddevice() {

        sznameadd = "";
        szserialadd = "";
        number.valueOf(0);
        return "Add_Device?faces-redirect=true";
    }

    public String editdeviceindb() {
        System.out.println(sznameadd + "  " + szserialadd + " " + sztypeadd + "  " + number);
        removeDevice(selectedDevice.getdID());
        if (number.equalsIgnoreCase("1")) {
            //sp

            createSPADVANCE(sznameadd, szserialadd);
        }
        if (number.equalsIgnoreCase("2")) {
            //wakd
            createWAKDADVANCE(sznameadd, szserialadd);
        }
        sznameadd = "";
        szserialadd = "";
        number.valueOf(0);
        refresh();
        return "Equipments?faces-redirect=true";
    }

    public String adddeviceindb() {
        System.out.println(sznameadd + "  " + szserialadd + " " + sztypeadd + "  " + number);
        if (number.equalsIgnoreCase("1")) {
            //sp
            createSPADVANCE(sznameadd, szserialadd);
            Long lid = locate(szserialadd);
            generateUserKeyPair(szserialadd, "SmartPhone", "patient", lid);

        }
        if (number.equalsIgnoreCase("2")) {
            //wakd
            createWAKDADVANCE(sznameadd, szserialadd);
            refresh();
        }
        sznameadd = "";
        szserialadd = "";
        number.valueOf(0);

        return "Equipments?faces-redirect=true";
    }

    /**
     * @return the pList
     */
    public List<DevicesModel> getpList() {
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<DevicesModel> pList) {
        this.pList = pList;
    }

    /**
     * @return the selectedDevice
     */
    public DevicesModel getSelectedDevice() {
        return selectedDevice;
    }

    /**
     * @param selectedDevice the selectedDevice to set
     */
    public void setSelectedDevice(DevicesModel selectedDevice) {
        this.selectedDevice = selectedDevice;
    }

    private java.util.List<eu.nephron.ws.medent.device.Device> findAllDevice() {
        DeviceManagerSWS_Service service = new DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        return port.findAllDevice();
    }

    /**
     * @return the sznameadd
     */
    public String getSznameadd() {
        return sznameadd;
    }

    /**
     * @param sznameadd the sznameadd to set
     */
    public void setSznameadd(String sznameadd) {
        this.sznameadd = sznameadd;
    }

    /**
     * @return the szserialadd
     */
    public String getSzserialadd() {
        return szserialadd;
    }

    /**
     * @param szserialadd the szserialadd to set
     */
    public void setSzserialadd(String szserialadd) {
        this.szserialadd = szserialadd;
    }

    /**
     * @return the sztypeadd
     */
    public String getSztypeadd() {
        return sztypeadd;
    }

    /**
     * @param sztypeadd the sztypeadd to set
     */
    public void setSztypeadd(String sztypeadd) {
        this.sztypeadd = sztypeadd;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    private void createDevice(eu.nephron.ws.medent.device.Device entity) {
        DeviceManagerSWS_Service service = new DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        port.createDevice(entity);
    }

    private java.util.List<eu.nephron.ws.role.Role_Type> findAllRole() {
        Role_Service service = new Role_Service();
        eu.nephron.ws.role.Role port = service.getRolePort();
        return port.findAllRole();
    }

    private void createSPADVANCE(java.lang.String arg0, java.lang.String arg1) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service = new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createSPADVANCE(arg0, arg1);
    }

    private void createWAKDADVANCE(java.lang.String arg0, java.lang.String arg1) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service = new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createWAKDADVANCE(arg0, arg1);
    }

    private void removeDevice(Long entityid) {
        DeviceManagerSWS_Service service = new DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        port.removeDevice(findDevice(entityid));
    }

    private Device findDevice(java.lang.Object id) {
        DeviceManagerSWS_Service service = new DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        return port.findDevice(id);
    }

    private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2, arg3);
    }

    /**
     * @return the numberfilter
     */
    public String getNumberfilter() {
        return numberfilter;
    }

    /**
     * @param numberfilter the numberfilter to set
     */
    public void setNumberfilter(String numberfilter) {
        this.numberfilter = numberfilter;
    }
}
