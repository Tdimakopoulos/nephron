/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dashboard.patient;


import eu.nephron.helper.entityhelper;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class PatientTableBeanSumup {
    

    private List<DSSResults> sumups;
    private DSSResults selectedsumup;
    private DSSResults[] selectedsumups;
    private String patientname;
    String previousid="NA";
    
    /**
     * Creates a new instance of TableBeanSumup
     */
    public PatientTableBeanSumup() {
        sumups = new ArrayList<DSSResults>();
        populatesumups(sumups);
    }

    private void checkforrefresh()
    {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
            previousid=puserid;
        }else
        {
            if(previousid.equalsIgnoreCase(puserid))
            {}else
            {
                previousid=puserid;
                sumups = new ArrayList<DSSResults>();
                populatesumups(sumups);
            }
        }
    }
    private void populatesumups(List<DSSResults> list) {
//        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
//        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
//        if(previousid.equalsIgnoreCase("NA"))
//        {
//        previousid=puserid;
//        }
//        entityhelper phelper = new entityhelper();
//        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
//        patientname=phelper.GetPatientName(Long.parseLong(puserid));
//        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);


    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        checkforrefresh();
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the selectedsumup
     */
    public DSSResults getSelectedsumup() {
        checkforrefresh();
        return selectedsumup;
    }

    /**
     * @param selectedsumup the selectedsumup to set
     */
    public void setSelectedsumup(DSSResults selectedsumup) {
        this.selectedsumup = selectedsumup;
    }

    /**
     * @return the selectedsumups
     */
    public DSSResults[] getSelectedsumups() {
        checkforrefresh();
        return selectedsumups;
    }

    /**
     * @param selectedsumups the selectedsumups to set
     */
    public void setSelectedsumups(DSSResults[] selectedsumups) {
        this.selectedsumups = selectedsumups;
    }

  


    /**
     * @return the patientname
     */
    public String getPatientname() {
        checkforrefresh();
        return patientname;
    }

    /**
     * @param patientname the patientname to set
     */
    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }
}
