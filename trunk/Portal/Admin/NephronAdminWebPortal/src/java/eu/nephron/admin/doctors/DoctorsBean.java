/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.doctors;

import eu.nephron.credentials.ws.CredentialsWS_Service;
import eu.nephron.credentials.ws.Credentialsdb;
import eu.nephron.helper.AdminEntityHelper;
import eu.nephron.model.Doctors;
import eu.nephron.soap.ws.AddressManager_Service;
import eu.nephron.soap.ws.ContactInfoManager_Service;
import eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import eu.nephron.webservices.KeyManagerService;
import eu.nephron.ws.medicalentity.Person_Service;
import eu.nephron.ws.medicalentity.Person_Type;
import eu.nephron.ws.medicalentity.Role;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class DoctorsBean {
    private String code;
    private String name;
    private String administrativeArea;
    private String city;
    private String street;
    private String streetNo;
    private String floor;
    private String postCode;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fatherName;
    private int maritalStatus;
    private int educationalLevel;
    private String phone;
    private String companyPhone;
    private String mobile;
    private String companyMobile;
    private String email;
    private String companyEmail;
    private List<Doctors> pList = new ArrayList<Doctors>();
    private String doctoridforedit;
    private Doctors selectedDoctor;
    private String username;
    private String password;
    private Long pdid;
    private Long icreid;

    public String deletedoctor() {

        removePerson(findPerson(selectedDoctor.getId()));
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String editcredoctor() {
        long ifind = Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", getDoctoridforedit());
        System.out.println("***** Doctor ID For Edit :  " + getDoctoridforedit());
        setPdid(selectedDoctor.getId());
        setUsername("");
        setPassword("");
        setIcreid(new Long(-1));
        java.util.List<eu.nephron.credentials.ws.Credentialsdb> pfind = findAll();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getDoctorid() == getPdid()) {
                setUsername(pfind.get(i).getUsername());
                setPassword(pfind.get(i).getPassword());
                setPdid(pfind.get(i).getDoctorid());
                setIcreid(pfind.get(i).getId());
            }
        }
        return "Credentials?faces-redirect=true";
    }

    public String editdoctor() {
        long ifind = Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", getDoctoridforedit());
        System.out.println("***** Doctor ID For Edit :  " + getDoctoridforedit());
        //from list to values
        code = selectedDoctor.getCode();
        name = selectedDoctor.getName();
        administrativeArea = selectedDoctor.getAdministrativeArea();
        city = selectedDoctor.getCity();
        street = selectedDoctor.getStreet();
        streetNo = selectedDoctor.getStreetNo();
        floor = selectedDoctor.getFloor();
        postCode = selectedDoctor.getPostCode();
        title = selectedDoctor.getTitle();
        firstName = selectedDoctor.getFirstName();
        middleName = selectedDoctor.getMiddleName();
        lastName = selectedDoctor.getLastName();
        fatherName = selectedDoctor.getFatherName();
        maritalStatus = selectedDoctor.getMaritalStatus();
        educationalLevel = selectedDoctor.getEducationalLevel();
        phone = selectedDoctor.getPhone();
        companyPhone = selectedDoctor.getCompanyPhone();
        mobile = selectedDoctor.getMobile();
        companyMobile = selectedDoctor.getCompanyMobile();
        email = selectedDoctor.getEmail();
        companyEmail = selectedDoctor.getCompanyEmail();



        return "Edit_Doctor?faces-redirect=true";
    }

    public String adddoctor() {
        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "Add_Doctor?faces-redirect=true";
    }

    public String savecreadddoctor() {
        //  private String username;
    //private String password;
    //private Long pdid;
    //private Long icreid;
    if(icreid==-1)
    {
        eu.nephron.credentials.ws.Credentialsdb  credentialsdb= new Credentialsdb();
        credentialsdb.setDoctorid(pdid);
        credentialsdb.setUsername(username);
        credentialsdb.setPassword(password);
    create(credentialsdb);
    }else
    {
        remove(find(icreid));
        eu.nephron.credentials.ws.Credentialsdb  credentialsdb= new Credentialsdb();
        credentialsdb.setDoctorid(pdid);
        credentialsdb.setUsername(username);
        credentialsdb.setPassword(password);
    create(credentialsdb);
        
    }
        return "DoctorPage?faces-redirect=true";
    }
    
    private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
        KeyManagerService service = new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2, arg3);
    }

    public String saveadddoctor() {
        String szaddressinfoid = createAddressInfo(code, city, street, streetNo, postCode, administrativeArea, floor, name);
        String szpersoinfoid = createPersonalInfo(title, firstName, lastName, fatherName, middleName, educationalLevel);
        String szcontactinfoid = createContactInfo(phone, mobile, email, companyPhone, companyMobile, companyEmail);
        createDoctorWithIDs(Long.parseLong(szaddressinfoid), Long.parseLong(szcontactinfoid), Long.parseLong(szpersoinfoid));
        generateUserKeyPair(firstName + " " + lastName, "Doctor", "Doctor", Long.valueOf(-1));
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String canceldoctor() {

        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "DoctorPage?faces-redirect=true";
    }

    public String saveeditdoctor() {
        eu.nephron.ws.medicalentity.Person_Type pdoctor = findPerson(selectedDoctor.getId());

        String szaddressinfoid = createAddressInfo(code, city, street, streetNo, postCode, administrativeArea, floor, name);
        String szpersoinfoid = createPersonalInfo(title, firstName, lastName, fatherName, middleName, educationalLevel);
        String szcontactinfoid = createContactInfo(phone, mobile, email, companyPhone, companyMobile, companyEmail);
        //createDoctorWithIDs(Long.parseLong(szaddressinfoid),Long.parseLong(szcontactinfoid),Long.parseLong(szpersoinfoid));
        pdoctor.setContactInfo(Long.parseLong(szcontactinfoid));
        pdoctor.setPersonalInfo(Long.parseLong(szpersoinfoid));
        pdoctor.setAddress(Long.parseLong(szaddressinfoid));
        editPerson(pdoctor);
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String editpdoctor() {
        long ifind = Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", getDoctoridforedit());
        System.out.println("***** Doctor ID For pEdit :  " + getDoctoridforedit());
        return "Edit_Doctor?faces-redirect=true";
    }

    public void refresh() {
        pList.clear();
        java.util.List<eu.nephron.ws.medicalentity.Person_Type> pWSList = findAllPerson();
        for (int i = 0; i < pWSList.size(); i++) {
            String name = pWSList.get(i).getName();
            Long lAddress = pWSList.get(i).getAddress();
            String szCode = pWSList.get(i).getCode();
            Long lContactInfo = pWSList.get(i).getContactInfo();
            int iGenter = pWSList.get(i).getGender();
            Long lID = pWSList.get(i).getId();
            Long lPersonalInfo = pWSList.get(i).getPersonalInfo();
//            int iQuantity = pWSList.get(i).getQuantity();
            Role pRole = pWSList.get(i).getRole();
            // System.out.println("***** Role "+pRole.getCode());
            if (pRole.getCode().equalsIgnoreCase("DOC")) {
                Doctors pDoctor = new Doctors();
                pDoctor.setId(lID);
                java.util.List<eu.nephron.soap.ws.Address> pListAddress = AdminEntityHelper.findAddressByID(lAddress);
                java.util.List<eu.nephron.soap.ws.ContactInfo> pListContact = AdminEntityHelper.findContactInfoByID(lContactInfo);
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);

                pDoctor.setFirstName(pListPersonalInfo.get(0).getFirstName());
                pDoctor.setLastName(pListPersonalInfo.get(0).getLastName());
                pDoctor.setMiddleName(pListPersonalInfo.get(0).getMiddleName());
                pDoctor.setAddresscode(pListAddress.get(0).getFloor());
                pDoctor.setAdministrativeArea(pListAddress.get(0).getAdministrativeArea());
                pDoctor.setCity(pListAddress.get(0).getCity());
                pDoctor.setCode(pListAddress.get(0).getCode());
                pDoctor.setCompanyEmail(pListContact.get(0).getCompanyEmail());
                pDoctor.setCompanyMobile(pListContact.get(0).getCompanyMobile());
                pDoctor.setCompanyPhone(pListContact.get(0).getCompanyPhone());
                pDoctor.setEducationalLevel(pListPersonalInfo.get(0).getEducationalLevel());
                pDoctor.setEmail(pListContact.get(0).getEmail());
                pDoctor.setFatherName(pListPersonalInfo.get(0).getFatherName());
                pDoctor.setFloor(pListAddress.get(0).getFloor());
                pDoctor.setMaritalStatus(pListPersonalInfo.get(0).getMaritalStatus());
                pDoctor.setMobile(pListContact.get(0).getMobile());
                pDoctor.setPhone(pListContact.get(0).getPhone());
                pDoctor.setPostCode(pListAddress.get(0).getPostCode());
                pDoctor.setStreet(pListAddress.get(0).getStreet());
                pDoctor.setStreetNo(pListAddress.get(0).getStreetNo());
                pDoctor.setTitle(pListPersonalInfo.get(0).getTitle());
                pList.add(pDoctor);

            }
        }
    }

    /**
     * Creates a new instance of DoctorsBean
     */
    public DoctorsBean() {
        java.util.List<eu.nephron.ws.medicalentity.Person_Type> pWSList = findAllPerson();
        for (int i = 0; i < pWSList.size(); i++) {
            String name = pWSList.get(i).getName();
            Long lAddress = pWSList.get(i).getAddress();
            String szCode = pWSList.get(i).getCode();
            Long lContactInfo = pWSList.get(i).getContactInfo();
            int iGenter = pWSList.get(i).getGender();
            Long lID = pWSList.get(i).getId();
            Long lPersonalInfo = pWSList.get(i).getPersonalInfo();
//            int iQuantity = pWSList.get(i).getQuantity();
            Role pRole = pWSList.get(i).getRole();
            // System.out.println("***** Role "+pRole.getCode());
            if (pRole.getCode().equalsIgnoreCase("DOC")) {
                Doctors pDoctor = new Doctors();
                pDoctor.setId(lID);
                java.util.List<eu.nephron.soap.ws.Address> pListAddress = AdminEntityHelper.findAddressByID(lAddress);
                java.util.List<eu.nephron.soap.ws.ContactInfo> pListContact = AdminEntityHelper.findContactInfoByID(lContactInfo);
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);

                pDoctor.setFirstName(pListPersonalInfo.get(0).getFirstName());
                pDoctor.setLastName(pListPersonalInfo.get(0).getLastName());
                pDoctor.setMiddleName(pListPersonalInfo.get(0).getMiddleName());
                pDoctor.setAddresscode(pListAddress.get(0).getFloor());
                pDoctor.setAdministrativeArea(pListAddress.get(0).getAdministrativeArea());
                pDoctor.setCity(pListAddress.get(0).getCity());
                pDoctor.setCode(pListAddress.get(0).getCode());
                pDoctor.setCompanyEmail(pListContact.get(0).getCompanyEmail());
                pDoctor.setCompanyMobile(pListContact.get(0).getCompanyMobile());
                pDoctor.setCompanyPhone(pListContact.get(0).getCompanyPhone());
                pDoctor.setEducationalLevel(pListPersonalInfo.get(0).getEducationalLevel());
                pDoctor.setEmail(pListContact.get(0).getEmail());
                pDoctor.setFatherName(pListPersonalInfo.get(0).getFatherName());
                pDoctor.setFloor(pListAddress.get(0).getFloor());
                pDoctor.setMaritalStatus(pListPersonalInfo.get(0).getMaritalStatus());
                pDoctor.setMobile(pListContact.get(0).getMobile());
                pDoctor.setPhone(pListContact.get(0).getPhone());
                pDoctor.setPostCode(pListAddress.get(0).getPostCode());
                pDoctor.setStreet(pListAddress.get(0).getStreet());
                pDoctor.setStreetNo(pListAddress.get(0).getStreetNo());
                pDoctor.setTitle(pListPersonalInfo.get(0).getTitle());
                pList.add(pDoctor);

            }
        }


    }

    private java.util.List<eu.nephron.ws.medicalentity.Person_Type> findAllPerson() {
        eu.nephron.ws.medicalentity.Person_Service service = new eu.nephron.ws.medicalentity.Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        return port.findAllPerson();
    }

    /**
     * @return the pList
     */
    public List<Doctors> getpList() {
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<Doctors> pList) {
        this.pList = pList;
    }

    /**
     * @return the doctoridforedit
     */
    public String getDoctoridforedit() {
        return doctoridforedit;
    }

    /**
     * @param doctoridforedit the doctoridforedit to set
     */
    public void setDoctoridforedit(String doctoridforedit) {
        this.doctoridforedit = doctoridforedit;
    }

    /**
     * @return the selectedDoctor
     */
    public Doctors getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(Doctors selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

//    public void editDoctorDetails() {
//        System.out.println("***** Selected Doctor ID :  " + selectedDoctor.getId());
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", selectedDoctor.getId().toString());
//    }
//
//    public String editDoctorDetails2() {
//        System.out.println("***** Selected Doctor ID :  " + selectedDoctor.getId());
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", selectedDoctor.getId().toString());
//        return "DashBoard?faces-redirect=true";
//    }
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the administrativeArea
     */
    public String getAdministrativeArea() {
        return administrativeArea;
    }

    /**
     * @param administrativeArea the administrativeArea to set
     */
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetNo
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo the streetNo to set
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the maritalStatus
     */
    public int getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the educationalLevel
     */
    public int getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * @param educationalLevel the educationalLevel to set
     */
    public void setEducationalLevel(int educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the companyMobile
     */
    public String getCompanyMobile() {
        return companyMobile;
    }

    /**
     * @param companyMobile the companyMobile to set
     */
    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the companyEmail
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * @param companyEmail the companyEmail to set
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    private String createAddressInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7) {
        AddressManager_Service service = new AddressManager_Service();
        eu.nephron.soap.ws.AddressManager port = service.getAddressManagerPort();
        return port.createAddressInfo(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    private String createPersonalInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, int arg5) {
        PersonalInfoManager_Service service = new PersonalInfoManager_Service();
        eu.nephron.soap.ws.PersonalInfoManager port = service.getPersonalInfoManagerPort();
        return port.createPersonalInfo(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    private String createContactInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) {
        ContactInfoManager_Service service = new ContactInfoManager_Service();
        eu.nephron.soap.ws.ContactInfoManager port = service.getContactInfoManagerPort();
        return port.createContactInfo(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    private void createDoctorWithIDs(java.lang.Long arg0, java.lang.Long arg1, java.lang.Long arg2) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service = new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createDoctorWithIDs(arg0, arg1, arg2);
    }

    private void removePerson(eu.nephron.ws.medicalentity.Person_Type entity) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        port.removePerson(entity);
    }

    private Person_Type findPerson(java.lang.Object id) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        return port.findPerson(id);
    }

    private void editPerson(eu.nephron.ws.medicalentity.Person_Type entity) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        port.editPerson(entity);
    }

    private java.util.List<eu.nephron.credentials.ws.Credentialsdb> findAll() {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        return port.findAll();
    }

    private void remove(eu.nephron.credentials.ws.Credentialsdb credentialsdb) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        port.remove(credentialsdb);
    }

    private void create(eu.nephron.credentials.ws.Credentialsdb credentialsdb) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        port.create(credentialsdb);
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the pdid
     */
    public Long getPdid() {
        return pdid;
    }

    /**
     * @param pdid the pdid to set
     */
    public void setPdid(Long pdid) {
        this.pdid = pdid;
    }

    /**
     * @return the icreid
     */
    public Long getIcreid() {
        return icreid;
    }

    /**
     * @param icreid the icreid to set
     */
    public void setIcreid(Long icreid) {
        this.icreid = icreid;
    }

    private Credentialsdb find(java.lang.Object id) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        return port.find(id);
    }
}
