/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.helper;

/**
 *
 * @author tdim
 */
public class AdminEntityHelper {

    public static java.util.List<eu.nephron.soap.ws.Address> findAddressByID(java.lang.Long arg0) {
        eu.nephron.soap.ws.AddressManager_Service service = new eu.nephron.soap.ws.AddressManager_Service();
        eu.nephron.soap.ws.AddressManager port = service.getAddressManagerPort();
        return port.findAddressByID(arg0);
    }

    public static java.util.List<eu.nephron.soap.ws.ContactInfo> findContactInfoByID(java.lang.Long arg0) {
        eu.nephron.soap.ws.ContactInfoManager_Service service = new eu.nephron.soap.ws.ContactInfoManager_Service();
        eu.nephron.soap.ws.ContactInfoManager port = service.getContactInfoManagerPort();
        return port.findContactInfoByID(arg0);
    }

    public static java.util.List<eu.nephron.soap.ws.PersonalInfo> findPersonalInfoByID(java.lang.Long arg0) {
        eu.nephron.soap.ws.PersonalInfoManager_Service service = new eu.nephron.soap.ws.PersonalInfoManager_Service();
        eu.nephron.soap.ws.PersonalInfoManager port = service.getPersonalInfoManagerPort();
        return port.findPersonalInfoByID(arg0);
    }
    
    
}
