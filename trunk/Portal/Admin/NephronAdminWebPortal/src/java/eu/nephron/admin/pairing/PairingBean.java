/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.pairing;

import eu.nephron.admin.patients.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import eu.nephron.helper.AdminEntityHelper;
import eu.nephron.model.Doctors;
import eu.nephron.soap.ws.AddressManager_Service;
import eu.nephron.soap.ws.ContactInfoManager_Service;
import eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import eu.nephron.soap.ws.commandmanager.ActServiceManager_Service;
import eu.nephron.webservices.KeyManagerService;
import eu.nephron.ws.medent.device.DeviceManagerSWS_Service;
import eu.nephron.ws.medicalentity.Person_Service;
import eu.nephron.ws.medicalentity.Person_Type;
import eu.nephron.ws.medicalentity.Role;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class PairingBean {
    

    
    
    
    
    
    

    private String code;
    private String name;
    private String administrativeArea;
    private String city;
    private String street;
    private String streetNo;
    private String floor;
    private String postCode;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fatherName;
    private int maritalStatus;
    private int educationalLevel;
    private String phone;
    private String companyPhone;
    private String mobile;
    private String companyMobile;
    private String email;
    private String companyEmail;
    private List<Doctors> pList = new ArrayList<Doctors>();
    private String doctoridforedit;
    private Doctors selectedDoctor;
    private Map<String,String> wakds = new HashMap<String, String>();
    private Map<String,String> mobiles = new HashMap<String, String>();
    private Map<String,String> doctors = new HashMap<String, String>();
    private String wakdl;
    private String mobilel;
    private String doctorl;
    boolean badd=false;
    
    private Map<String,String> wakds1 = new HashMap<String, String>();
    private Map<String,String> mobiles1 = new HashMap<String, String>();
    private Map<String,String> doctors1 = new HashMap<String, String>();
    private String wakdl1;
    private String mobilel1;
    private String doctorl1;
    /**
     * Creates a new instance of PatientsBean
     */
    public PairingBean() {
        FirstLoad();
    }
private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
        KeyManagerService service= new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2, arg3);
    }
    public String deletedoctor() {
    
        removePerson(findPerson(selectedDoctor.getId()));
        deleteConnections(Long.valueOf(selectedDoctor.getId()),"WS");
        refresh();
        return "Pairing?faces-redirect=true";
    }
    
    public String editdoctor() {
//        long ifind = Long.parseLong(getDoctoridforedit());
        //      FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", getDoctoridforedit());
        //    System.out.println("***** Doctor ID For Edit :  " + getDoctoridforedit());
        //from list to values
        code = selectedDoctor.getCode();
        name = selectedDoctor.getName();
        administrativeArea = selectedDoctor.getAdministrativeArea();
        city = selectedDoctor.getCity();
        street = selectedDoctor.getStreet();
        streetNo = selectedDoctor.getStreetNo();
        floor = selectedDoctor.getFloor();
        postCode = selectedDoctor.getPostCode();
        title = selectedDoctor.getTitle();
        firstName = selectedDoctor.getFirstName();
        middleName = selectedDoctor.getMiddleName();
        lastName = selectedDoctor.getLastName();
        fatherName = selectedDoctor.getFatherName();
        maritalStatus = selectedDoctor.getMaritalStatus();
        educationalLevel = selectedDoctor.getEducationalLevel();
        phone = selectedDoctor.getPhone();
        companyPhone = selectedDoctor.getCompanyPhone();
        mobile = selectedDoctor.getMobile();
        companyMobile = selectedDoctor.getCompanyMobile();
        email = selectedDoctor.getEmail();
        companyEmail = selectedDoctor.getCompanyEmail();



        return "Edit_Patient?faces-redirect=true";
    }

    public String adddoctor() {
        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "Add_Patient?faces-redirect=true";
    }

    public String saveadddoctor() {
        String szaddressinfoid = createAddressInfo(code, city, street, streetNo, postCode, administrativeArea, floor, name);
        String szpersoinfoid = createPersonalInfo(title, firstName, lastName, fatherName, middleName, educationalLevel);
        String szcontactinfoid = createContactInfo(phone, mobile, email, companyPhone, companyMobile, companyEmail);
        createPatientWithIDs(Long.parseLong(szaddressinfoid), Long.parseLong(szcontactinfoid), Long.parseLong(szpersoinfoid));
        generateUserKeyPair(firstName+" "+lastName,"Patient","Patient",Long.valueOf(-1));
        refresh();
        return "Pairing?faces-redirect=true";
    }

    public String canceldoctor() {
    
    code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "Pairing?faces-redirect=true";
    }
    
    public String saveeditdoctor() {
        eu.nephron.ws.medicalentity.Person_Type pdoctor = findPerson(selectedDoctor.getId());

        String szaddressinfoid = createAddressInfo(code, city, street, streetNo, postCode, administrativeArea, floor, name);
        String szpersoinfoid = createPersonalInfo(title, firstName, lastName, fatherName, middleName, educationalLevel);
        String szcontactinfoid = createContactInfo(phone, mobile, email, companyPhone, companyMobile, companyEmail);
        //createDoctorWithIDs(Long.parseLong(szaddressinfoid),Long.parseLong(szcontactinfoid),Long.parseLong(szpersoinfoid));
        pdoctor.setContactInfo(Long.parseLong(szcontactinfoid));
        pdoctor.setPersonalInfo(Long.parseLong(szpersoinfoid));
        pdoctor.setAddress(Long.parseLong(szaddressinfoid));
        editPerson(pdoctor);
        refresh();
        return "Pairing?faces-redirect=true";
    }

     public String saveeditassociations() {
        
        if(badd==true)
        {
            System.out.println(" make associations ID : " + selectedDoctor.getId());
            createAssociationActPatientAndSmartphoneandWakd(selectedDoctor.getId(),Long.valueOf(mobilel),Long.valueOf(wakdl));
            createAssociationActPatientAndDoctor(Long.valueOf(doctorl),selectedDoctor.getId());
        }else
        {
            System.out.println(" delete associations ID : " + selectedDoctor.getId());
            deleteConnections(Long.valueOf(selectedDoctor.getId()),"WS");
            System.out.println(" make associations ID : " + selectedDoctor.getId());
            createAssociationActPatientAndSmartphoneandWakd(selectedDoctor.getId(),Long.valueOf(mobilel),Long.valueOf(wakdl));
            createAssociationActPatientAndDoctor(Long.valueOf(doctorl),selectedDoctor.getId());
        }
        return "Pairing?faces-redirect=true";
    }
     
    //device associations
    public String editpdevice() {
        System.out.println(" ID : " + selectedDoctor.getId());

        wakdl="0";
        mobilel="0";
        doctorl="0";
        badd=true;
        try {
            
            List<Object> pList = findConnections(selectedDoctor.getId(), "WS");
            
            
            java.util.List<eu.nephron.ws.medent.device.Device> pDevices=findAllDevice();
            for(int i=0;i<pDevices.size();i++)
            {
                long id=pDevices.get(i).getId();
                String serialnumber=pDevices.get(i).getSerialNumber();
                String dcode=pDevices.get(i).getCode();
                if(dcode.equalsIgnoreCase("WAKD"))
                {
                    wakds.put(serialnumber,String.valueOf(id));
                    for (int i2 = 0; i2 < pList.size(); i2++) {
                        if ((Long)pList.get(i2)==id)
                        {
                            
                            wakdl=String.valueOf(id);
                            System.out.println(" Find selected wakd "+wakdl);
                            badd=false;
                        }
                    }
                }
                if(dcode.equalsIgnoreCase("SP"))
                {
                    mobiles.put(serialnumber,String.valueOf(id));
                    for (int i2 = 0; i2 < pList.size(); i2++) {
                        if ((Long)pList.get(i2)==id)
                        {
                            
                            mobilel=String.valueOf(id);
                            System.out.println(" Find selected Mobile "+mobilel);
                            badd=false;
                        }
                    }
                }

                
            }
            
            for (int i = 0; i < pList.size(); i++) {
                //System.out.println("--> " + i + " - " + pList.get(i));
                String idss=String.valueOf((Long)pList.get(i));
                if (wakdl.equalsIgnoreCase(idss))
                {}else if (mobilel.equalsIgnoreCase(idss))
                {}else{
                    System.out.println(" Find selected Doctor "+doctorl);
                    doctorl=idss;badd=false;}
            }
            if((Long)pList.get(0)==-1)
            {
                badd=true;
            }else{badd=false;}
        } finally {
            return "Edit_PAss?faces-redirect=true";
        }
    }

    /**************************************************************************************************************************/
    
    public String GetDoctorName()
    {
        for (Entry<String,String> entry : doctors1.entrySet())
        {
          if (entry.getValue().equalsIgnoreCase(doctorl1))  
              return entry.getKey();
        }
        return "N/A";    
    }
    
    public String GetWAKD()
    {
        for (Entry<String,String> entry : wakds1.entrySet())
        {
          if (entry.getValue().equalsIgnoreCase(wakdl1))  
              return entry.getKey();
        }
        return "N/A";    
    }
    
    public String GetSP()
    {
        for (Entry<String,String> entry : mobiles1.entrySet())
        {
          if (entry.getValue().equalsIgnoreCase(mobilel1))  
              return entry.getKey();
        }
        return "N/A";    
    }
    
     public String ppdevice(Long ID) {
        System.out.println(" ID : " + ID);

        wakdl1="0";
        mobilel1="0";
        doctorl1="0";
        //badd=true;
        try {
            
            List<Object> pList = findConnections(ID, "WS");
            
            
            java.util.List<eu.nephron.ws.medent.device.Device> pDevices=findAllDevice();
            for(int i=0;i<pDevices.size();i++)
            {
                long id=pDevices.get(i).getId();
                String serialnumber=pDevices.get(i).getSerialNumber();
                String dcode=pDevices.get(i).getCode();
                if(dcode.equalsIgnoreCase("WAKD"))
                {
                    wakds1.put(serialnumber,String.valueOf(id));
                    for (int i2 = 0; i2 < pList.size(); i2++) {
                        if ((Long)pList.get(i2)==id)
                        {
                            System.out.println(" Find selected wakd");
                            wakdl1=String.valueOf(id);
                            
                           // badd=false;
                        }
                    }
                }
                if(dcode.equalsIgnoreCase("SP"))
                {
                    mobiles1.put(serialnumber,String.valueOf(id));
                    for (int i2 = 0; i2 < pList.size(); i2++) {
                        if ((Long)pList.get(i2)==id)
                        {
                            System.out.println(" Find selected Mobile");
                            mobilel1=String.valueOf(id);
                            //badd=false;
                        }
                    }
                }

                
            }
            
            for (int i = 0; i < pList.size(); i++) {
                System.out.println("--> " + i + " - " + pList.get(i));
                String idss=String.valueOf((Long)pList.get(i));
                if (wakdl1.equalsIgnoreCase(idss))
                {}else if (mobilel1.equalsIgnoreCase(idss))
                {}else{doctorl1=idss;}
            }
            
        } finally {
            return "ok";
        }
    }
    
    /****************************************************************************************************************************/
    //doctor associations
    public String editpdoctor() {

        return "Edit_PAss?faces-redirect=true";
    }

    public void refresh() {
        pList.clear();
        java.util.List<eu.nephron.ws.medicalentity.Person_Type> pWSList = findAllPerson();
        doctors.clear();
        for (int i = 0; i < pWSList.size(); i++) {
            String name = pWSList.get(i).getName();
            Long lAddress = pWSList.get(i).getAddress();
            String szCode = pWSList.get(i).getCode();
            Long lContactInfo = pWSList.get(i).getContactInfo();
            int iGenter = pWSList.get(i).getGender();
            Long lID = pWSList.get(i).getId();
            Long lPersonalInfo = pWSList.get(i).getPersonalInfo();
//            int iQuantity = pWSList.get(i).getQuantity();
            Role pRole = pWSList.get(i).getRole();
             if(pRole.getCode().equalsIgnoreCase("DOC"))
                {
                    java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo0 = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);
                    doctors.put(pListPersonalInfo0.get(0).getFirstName()+" "+pListPersonalInfo0.get(0).getLastName(),String.valueOf(lID));
                    doctors1.put(pListPersonalInfo0.get(0).getFirstName()+" "+pListPersonalInfo0.get(0).getLastName(),String.valueOf(lID));
                }
            if (pRole.getCode().equalsIgnoreCase("P")) {
                Doctors pDoctor = new Doctors();
                pDoctor.setId(lID);
                //java.util.List<eu.nephron.soap.ws.Address> pListAddress = AdminEntityHelper.findAddressByID(lAddress);
                //java.util.List<eu.nephron.soap.ws.ContactInfo> pListContact = AdminEntityHelper.findContactInfoByID(lContactInfo);
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);

                pDoctor.setFirstName(pListPersonalInfo.get(0).getFirstName()+"  "+pListPersonalInfo.get(0).getLastName());
                ppdevice(lID);
                pDoctor.setLastName(GetDoctorName());
                pDoctor.setMiddleName(GetSP());
                pDoctor.setAddresscode(GetWAKD());
//                pDoctor.setAdministrativeArea(pListAddress.get(0).getAdministrativeArea());
//                pDoctor.setCity(pListAddress.get(0).getCity());
//                pDoctor.setCode(pListAddress.get(0).getCode());
//                pDoctor.setCompanyEmail(pListContact.get(0).getCompanyEmail());
//                pDoctor.setCompanyMobile(pListContact.get(0).getCompanyMobile());
//                pDoctor.setCompanyPhone(pListContact.get(0).getCompanyPhone());
//                pDoctor.setEducationalLevel(pListPersonalInfo.get(0).getEducationalLevel());
//                pDoctor.setEmail(pListContact.get(0).getEmail());
//                pDoctor.setFatherName(pListPersonalInfo.get(0).getFatherName());
//                pDoctor.setFloor(pListAddress.get(0).getFloor());
//                pDoctor.setMaritalStatus(pListPersonalInfo.get(0).getMaritalStatus());
//                pDoctor.setMobile(pListContact.get(0).getMobile());
//                pDoctor.setPhone(pListContact.get(0).getPhone());
//                pDoctor.setPostCode(pListAddress.get(0).getPostCode());
//                pDoctor.setStreet(pListAddress.get(0).getStreet());
//                pDoctor.setStreetNo(pListAddress.get(0).getStreetNo());
//                pDoctor.setTitle(pListPersonalInfo.get(0).getTitle());
                pList.add(pDoctor);

            }
        }
    }

    /**
     * Creates a new instance of DoctorsBean
     */
    public void FirstLoad() {
        java.util.List<eu.nephron.ws.medicalentity.Person_Type> pWSList = findAllPerson();
        doctors.clear();
        for (int i = 0; i < pWSList.size(); i++) {
            String name = pWSList.get(i).getName();
            Long lAddress = pWSList.get(i).getAddress();
            String szCode = pWSList.get(i).getCode();
            Long lContactInfo = pWSList.get(i).getContactInfo();
            int iGenter = pWSList.get(i).getGender();
            Long lID = pWSList.get(i).getId();
            Long lPersonalInfo = pWSList.get(i).getPersonalInfo();
//            int iQuantity = pWSList.get(i).getQuantity();
            Role pRole = pWSList.get(i).getRole();
            // System.out.println("***** Role "+pRole.getCode());
 if(pRole.getCode().equalsIgnoreCase("DOC"))
                {
                    java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo0 = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);
                    doctors.put(pListPersonalInfo0.get(0).getFirstName()+" "+pListPersonalInfo0.get(0).getLastName(),String.valueOf(lID));
                    doctors1.put(pListPersonalInfo0.get(0).getFirstName()+" "+pListPersonalInfo0.get(0).getLastName(),String.valueOf(lID));
                }
            if (pRole.getCode().equalsIgnoreCase("P")) {
                Doctors pDoctor = new Doctors();
                pDoctor.setId(lID);
                //java.util.List<eu.nephron.soap.ws.Address> pListAddress = AdminEntityHelper.findAddressByID(lAddress);
                //java.util.List<eu.nephron.soap.ws.ContactInfo> pListContact = AdminEntityHelper.findContactInfoByID(lContactInfo);
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pListPersonalInfo = AdminEntityHelper.findPersonalInfoByID(lPersonalInfo);

                pDoctor.setFirstName(pListPersonalInfo.get(0).getFirstName()+" "+pListPersonalInfo.get(0).getLastName());
                ppdevice(lID);
                pDoctor.setLastName(GetDoctorName());
                pDoctor.setMiddleName(GetSP());
                pDoctor.setAddresscode(GetWAKD());
//                pDoctor.setAdministrativeArea(pListAddress.get(0).getAdministrativeArea());
//                pDoctor.setCity(pListAddress.get(0).getCity());
//                pDoctor.setCode(pListAddress.get(0).getCode());
//                pDoctor.setCompanyEmail(pListContact.get(0).getCompanyEmail());
//                pDoctor.setCompanyMobile(pListContact.get(0).getCompanyMobile());
//                pDoctor.setCompanyPhone(pListContact.get(0).getCompanyPhone());
//                pDoctor.setEducationalLevel(pListPersonalInfo.get(0).getEducationalLevel());
//                pDoctor.setEmail(pListContact.get(0).getEmail());
//                pDoctor.setFatherName(pListPersonalInfo.get(0).getFatherName());
//                pDoctor.setFloor(pListAddress.get(0).getFloor());
//                pDoctor.setMaritalStatus(pListPersonalInfo.get(0).getMaritalStatus());
//                pDoctor.setMobile(pListContact.get(0).getMobile());
//                pDoctor.setPhone(pListContact.get(0).getPhone());
//                pDoctor.setPostCode(pListAddress.get(0).getPostCode());
//                pDoctor.setStreet(pListAddress.get(0).getStreet());
//                pDoctor.setStreetNo(pListAddress.get(0).getStreetNo());
//                pDoctor.setTitle(pListPersonalInfo.get(0).getTitle());
                pList.add(pDoctor);

            }
        }


    }

    private java.util.List<eu.nephron.ws.medicalentity.Person_Type> findAllPerson() {
        eu.nephron.ws.medicalentity.Person_Service service = new eu.nephron.ws.medicalentity.Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        return port.findAllPerson();
    }

    /**
     * @return the pList
     */
    public List<Doctors> getpList() {
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<Doctors> pList) {
        this.pList = pList;
    }

    /**
     * @return the doctoridforedit
     */
    public String getDoctoridforedit() {
        return doctoridforedit;
    }

    /**
     * @param doctoridforedit the doctoridforedit to set
     */
    public void setDoctoridforedit(String doctoridforedit) {
        this.doctoridforedit = doctoridforedit;
    }

    /**
     * @return the selectedDoctor
     */
    public Doctors getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(Doctors selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

//    public void editDoctorDetails() {
//        System.out.println("***** Selected Doctor ID :  " + selectedDoctor.getId());
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", selectedDoctor.getId().toString());
//    }
//
//    public String editDoctorDetails2() {
//        System.out.println("***** Selected Doctor ID :  " + selectedDoctor.getId());
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", selectedDoctor.getId().toString());
//        return "DashBoard?faces-redirect=true";
//    }
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the administrativeArea
     */
    public String getAdministrativeArea() {
        return administrativeArea;
    }

    /**
     * @param administrativeArea the administrativeArea to set
     */
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetNo
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo the streetNo to set
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the maritalStatus
     */
    public int getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the educationalLevel
     */
    public int getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * @param educationalLevel the educationalLevel to set
     */
    public void setEducationalLevel(int educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the companyMobile
     */
    public String getCompanyMobile() {
        return companyMobile;
    }

    /**
     * @param companyMobile the companyMobile to set
     */
    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the companyEmail
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * @param companyEmail the companyEmail to set
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    private String createAddressInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5, java.lang.String arg6, java.lang.String arg7) {
        AddressManager_Service service = new AddressManager_Service();
        eu.nephron.soap.ws.AddressManager port = service.getAddressManagerPort();
        return port.createAddressInfo(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
    }

    private String createPersonalInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, int arg5) {
        PersonalInfoManager_Service service = new PersonalInfoManager_Service();
        eu.nephron.soap.ws.PersonalInfoManager port = service.getPersonalInfoManagerPort();
        return port.createPersonalInfo(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    private String createContactInfo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.String arg3, java.lang.String arg4, java.lang.String arg5) {
        ContactInfoManager_Service service = new ContactInfoManager_Service();
        eu.nephron.soap.ws.ContactInfoManager port = service.getContactInfoManagerPort();
        return port.createContactInfo(arg0, arg1, arg2, arg3, arg4, arg5);
    }

    private void createDoctorWithIDs(java.lang.Long arg0, java.lang.Long arg1, java.lang.Long arg2) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service = new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createDoctorWithIDs(arg0, arg1, arg2);
    }

    private void removePerson(eu.nephron.ws.medicalentity.Person_Type entity) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        port.removePerson(entity);
    }

    private Person_Type findPerson(java.lang.Object id) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        return port.findPerson(id);
    }

    private void editPerson(eu.nephron.ws.medicalentity.Person_Type entity) {
        Person_Service service = new Person_Service();
        eu.nephron.ws.medicalentity.Person port = service.getPersonPort();
        port.editPerson(entity);
    }

    private void createPatientWithIDs(java.lang.Long arg0, java.lang.Long arg1, java.lang.Long arg2) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service = new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createPatientWithIDs(arg0, arg1, arg2);
    }

    private void createAssociationAct(java.util.List<java.lang.Long> arg0, java.lang.String arg1) {
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        port.createAssociationAct(arg0, arg1);
    }

    private void updateAssociationAct(java.lang.Long arg0, java.util.List<java.lang.Long> arg1, java.lang.String arg2) {
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        port.updateAssociationAct(arg0, arg1, arg2);
    }

    private java.util.List<java.lang.Object> findConnections(java.lang.Long arg0, java.lang.String arg1) {
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        return port.findConnections(arg0, arg1);
    }

    /**
     * @return the wakds
     */
    public Map<String,String> getWakds() {
        return wakds;
    }

    /**
     * @param wakds the wakds to set
     */
    public void setWakds(Map<String,String> wakds) {
        this.wakds = wakds;
    }

    /**
     * @return the mobiles
     */
    public Map<String,String> getMobiles() {
        return mobiles;
    }

    /**
     * @param mobiles the mobiles to set
     */
    public void setMobiles(Map<String,String> mobiles) {
        this.mobiles = mobiles;
    }

    /**
     * @return the doctors
     */
    public Map<String,String> getDoctors() {
        return doctors;
    }

    /**
     * @param doctors the doctors to set
     */
    public void setDoctors(Map<String,String> doctors) {
        this.doctors = doctors;
    }

    /**
     * @return the wakdl
     */
    public String getWakdl() {
        return wakdl;
    }

    /**
     * @param wakdl the wakdl to set
     */
    public void setWakdl(String wakdl) {
        this.wakdl = wakdl;
    }

    /**
     * @return the mobilel
     */
    public String getMobilel() {
        return mobilel;
    }

    /**
     * @param mobilel the mobilel to set
     */
    public void setMobilel(String mobilel) {
        this.mobilel = mobilel;
    }

    /**
     * @return the doctorl
     */
    public String getDoctorl() {
        return doctorl;
    }

    /**
     * @param doctorl the doctorl to set
     */
    public void setDoctorl(String doctorl) {
        this.doctorl = doctorl;
    }

    private java.util.List<eu.nephron.ws.medent.device.Device> findAllDevice() {
        DeviceManagerSWS_Service service=new DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        return port.findAllDevice();
    }

    private void createAssociationAct_1(java.util.List<java.lang.Long> arg0, java.lang.String arg1) {
        ActServiceManager_Service service=new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        port.createAssociationAct(arg0, arg1);
    }

    private void createAssociationActPatientAndDoctor(java.lang.Long arg0, java.lang.Long arg1) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service=new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createAssociationActPatientAndDoctor(arg0, arg1);
    }

    private void createAssociationActPatientAndSmartphoneandWakd(java.lang.Long arg0, java.lang.Long arg1, java.lang.Long arg2) {
        DatabaseTesterAndFillerPersonalNephronHL7_Service service=new DatabaseTesterAndFillerPersonalNephronHL7_Service();
        eu.nephron.soap.ws.DatabaseTesterAndFillerPersonalNephronHL7 port = service.getDatabaseTesterAndFillerPersonalNephronHL7Port();
        port.createAssociationActPatientAndSmartphoneandWakd(arg0, arg1, arg2);
    }

    private String deleteConnections(java.lang.Long arg0, java.lang.String arg1) {
        ActServiceManager_Service service=new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        return port.deleteConnections(arg0, arg1);
    }
}
