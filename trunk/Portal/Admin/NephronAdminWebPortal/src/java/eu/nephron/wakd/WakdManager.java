/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
//import eu.nephron.phonemanager.soap.ManagePhoneMessages_Service;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class WakdManager {

    /**
     * Creates a new instance of WakdManager
     */
    public WakdManager() {
    }

    public String shutdown() {

        spsCreateShutdown("800", "test reason");
        return "DashBoard_Patient?faces-redirect=true";
    }

    private void spsCreateShutdown(java.lang.String imei, java.lang.String reason) {
        // ManagePhoneMessages_Service service=new ManagePhoneMessages_Service();
        //eu.nephron.phonemanager.soap.ManagePhoneMessages port = service.getManagePhoneMessagesPort();
        //port.spsCreateShutdown(imei, reason);
    }
}
