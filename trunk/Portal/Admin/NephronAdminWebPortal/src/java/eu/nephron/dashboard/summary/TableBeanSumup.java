/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dashboard.summary;

import eu.nephron.model.DSSResults;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class TableBeanSumup {
    

    private List<DSSResults> sumups;
    private DSSResults selectedsumup;
    private DSSResults[] selectedsumups;

    /**
     * Creates a new instance of TableBeanSumup
     */
    public TableBeanSumup() {
        sumups = new ArrayList<DSSResults>();
        populatesumups(sumups);
    }

    private void populatesumups(List<DSSResults> list) {

    
    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the selectedsumup
     */
    public DSSResults getSelectedsumup() {
        return selectedsumup;
    }

    /**
     * @param selectedsumup the selectedsumup to set
     */
    public void setSelectedsumup(DSSResults selectedsumup) {
        this.selectedsumup = selectedsumup;
    }

    /**
     * @return the selectedsumups
     */
    public DSSResults[] getSelectedsumups() {
        return selectedsumups;
    }

    /**
     * @param selectedsumups the selectedsumups to set
     */
    public void setSelectedsumups(DSSResults[] selectedsumups) {
        this.selectedsumups = selectedsumups;
    }

    

    
}
