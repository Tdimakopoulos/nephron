/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.conversions;

import java.io.UnsupportedEncodingException;

/**
 *
 * @author tdim
 */
public class ByteUtils {

    /**
     * to mimic sizeof(short) = short length in bytes = 12 (=16bit)
     */
    public static final int SIZEOF_SHORT = 2;
    /**
     * to mimic sizeof(int) = int length in bytes = 4 (=32bit)
     */
    public static final int SIZEOF_INT = 4;
    /**
     * to mimic sizeof(long) = long length in bytes = 8 (=64bit)
     */
    public static final int SIZEOF_LONG = 8;
    /**
     * bit length of byte
     */
    public static final int BITS_BYTE = 8;
    /**
     * bit length of short
     */
    public static final int BITS_SHORT = BITS_BYTE * SIZEOF_SHORT;
    /**
     * bit length of int
     */
    public static final int BITS_INT = BITS_BYTE * SIZEOF_INT;
    /**
     * bit length of long
     */
    public static final int BITS_LONG = BITS_BYTE * SIZEOF_LONG;
    /**
     * mask a byte in an int
     */
    public static final int MASK_BYTE = 0xFF;

    protected ByteUtils() {
    }

    /**
     * Converts the SIZEOF_LONG bytes starting at off within b to a long value.
     * <br />NOTE: the conversion treats the leftmost byte as the lowest byte of
     * the resulting long value
     *
     * @param b a byte[] containing bytes to be converted
     * @param off the offset where to find the bytes to be converted
     * @return a long value
     * @throws IndexOutOfBoundsException if an index violation occurs
     */
    public static long toLong(byte[] b, int off) {
        long r = 0;
        for (int i = SIZEOF_LONG - 1; i >= 0; i--) {
            r |= (((long) b[ off + i]) & MASK_BYTE) << (i * BITS_BYTE);
        }
        return r;
    }

    /**
     * Converts the SIZEOF_INT bytes starting at off within b to an int value.
     * <br />NOTE: the conversion treats the leftmost byte as the lowest byte of
     * the resulting int value
     *
     * @param b a byte[] containing bytes to be converted
     * @param off the offset where to find the bytes to be converted
     * @return an int value
     * @throws IndexOutOfBoundsException if an index violation occurs
     */
    public static int toInt(byte[] b, int off) {
        int r = 0;
        for (int i = SIZEOF_INT - 1; i >= 0; i--) {
            r |= ((int) (b[ off + i] & MASK_BYTE)) << (i * BITS_BYTE);
        }
        return r;
    }

    /**
     * Converts a long value to SIZEOF_LONG bytes stored in b starting at off.
     * <br />NOTE: the conversion stores the lowest byte of the long value as
     * the leftmost byte within the sequence
     *
     * @param val a long value to be split up into bytes
     * @param b a byte[] to be written to
     * @param off the offset where to start writing within b
     * @throws IndexOutOfBoundsException if an index violation occurs
     */
    public static void toBytes(long val, byte[] b, int off) {
        for (int i = 0; i < SIZEOF_LONG; i++, val >>= BITS_BYTE) {
            b[ off + i] = (byte) (val & MASK_BYTE);
        }
    }

    /**
     * Converts an int value to SIZEOF_INT bytes stored in b starting at off.
     * <br />NOTE: the conversion stores the lowest byte of the int value as the
     * leftmost byte within the sequence
     *
     * @param val an int value to be split up into bytes
     * @param b a byte[] to be written to
     * @param off the offset where to start writing within b
     * @throws IndexOutOfBoundsException if an index violation occurs
     */
    public static void toBytes(int val, byte[] b, int off) {
        for (int i = 0; i < SIZEOF_INT; i++, val >>= BITS_BYTE) {
            b[ off + i] = (byte) (val & MASK_BYTE);
        }
    }

    /**
     * Converts a byte value to a hexadecimal String.
     *
     * @param b a byte value
     * @return a hexadecimal (upper-case-based) String representation of the
     * byte value
     */
    public static String toHexString(byte b) {
        int len = 2;
        byte[] dig = new byte[len];
        dig[ 0] = (byte) ((b & 0xF0) >> 4);
        dig[ 1] = (byte) (b & 0x0F);
        for (int i = 0; i < len; i++) {
            dig[ i] += 10 > dig[ i] ? 48 : 55;
        }
        return new String(dig);
    }

    public static String toHexString(byte[] b) {
        if (null == b) {
            return null;
        }
        int len = b.length;
        byte[] hex = new byte[len << 1];
        for (int i = 0, j = 0; i < len; i++, j += 2) {
            hex[ j] = (byte) ((b[ i] & 0xF0) >> 4);
            hex[ j] += 10 > hex[ j] ? 48 : 55;
            hex[ j + 1] = (byte) (b[ i] & 0x0F);
            hex[ j + 1] += 10 > hex[ j + 1] ? 48 : 55;
        }
        return new String(hex);
    }

    /**
     * Parses a number from a string. Finds the first recognizable base-10
     * number (integer or floating point) in the string and returns it as a
     * Number.
     *
     * @param string String to parse
     * @return first recognizable number
     * @exception NumberFormatException if no recognizable number is found
     */
    public static Number toNumber(String s)
            throws NumberFormatException {
        // parsing states
        int INT = 0;
        int FRAC = 1;
        int EXP = 2;
        int p = 0;
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                int start = i;
                int end = ++i;
                int state = INT;
                if (start > 0 && s.charAt(start - 1) == '.') {
                    --start;
                    state = FRAC;
                }
                if (start > 0 && s.charAt(start - 1) == '-') {
                    --start;
                }
                boolean atEnd = false;
                while (!atEnd && i < s.length()) {
                    switch (s.charAt(i)) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            end = ++i;
                            break;
                        case '.':
                            if (state == INT) {
                                state = FRAC;
                                ++i;
                            } else {
                                atEnd = true;
                            }
                            break;
                        case 'e':
                        case 'E':
                            state = EXP;
                            ++i;
                            if (i < s.length() && ((c = s.charAt(i)) == '+' || c == '-')) {
                                ++i;
                            }
                            break;
                        default:
                            atEnd = true;
                    }
                }
                String num = s.substring(start, end);
                try {
                    if (state == INT) {
                        return new Integer(num);
                    } else {
                        return new Double(num);
                    }
                } catch (NumberFormatException e) {
                    throw new RuntimeException("internal error: " + e);
                }
            }
        }
        throw new NumberFormatException(s);
    }

    public static String[] toStrings(Object[] o) {
        if (null == o) {
            return null;
        }
        int len = o.length;
        String[] s = new String[len];
        for (int i = 0; i < len; i++) {
            if (null != o[ i]) {
                s[ i] = o[ i].toString();
            }
        }
        return s;
    }

    /**
     * converts to int with radix 10 and default 0
     */
    public static int toInt(Object o) {
        return toInt(o, 10, 0);
    }

    /**
     * Convets an Object to an int value using radix 10. <br />This is a wrapper
     * for
     * <code>{@link #toInt( Object, int ) toInt( o, 10 )}</code>.
     *
     * @param o Object to be converted to integer
     * @return the int value represented by o or 0 if conversion fails.
     */
    public static int toInt(Object o, int dflt) {
        return toInt(o, 10, dflt);
    }

    /**
     * Converts any String representation to an int value using a given radix.
     * <br />For
     * <code>null</code> it returns
     * <code>0</code>.<br /> For Objects instanceof Number it returns the
     * Object's
     * <code>intValue()</code>. For all other Objects it uses the
     * <code>toString()</code> method to get an appropriate String
     * representation and parses this String using
     * <code>Integer.parseInt</code>. If conversion fails it returns
     * <code>0</code>.
     *
     * @param o Object to be converted to integer
     * @param radix the radix used for conversion
     * @return the int value represented by <code>o</code> or <code>0</code> if
     * conversion fails.
     */
    public static int toInt(Object o, int radix, int dflt) {
        if (null == o) // shortcut without exception
        {
            return dflt;
        }
        if (o instanceof Number) {
            return ((Number) o).intValue();
        }
        try {
            return Integer.parseInt(o.toString().trim(), radix);
        } catch (Exception e) {
            return dflt;
        }
    }

    public static long toLong(Object o) {
        return toLong(o, 10, 0);
    }

    public static long toLong(Object o, int dflt) {
        return toLong(o, 10, dflt);
    }

    public static long toLong(Object o, int radix, int dflt) {
        if (null == o) // shortcut without exception
        {
            return dflt;
        }
        if (o instanceof Number) {
            return ((Number) o).longValue();
        }
        try {
            return Long.parseLong(o.toString().trim(), radix);
        } catch (Exception e) {
            return dflt;
        }
    }

    /**
     * Converts a byte array to short value. Equivalent to
     * byteArrayToShort(paRawBytes, 0, pbBigEndian);
     *
     * @param paRawBytes the byte array
     * @param pbBigEndian true if the bytes are in Big-endian order; false
     * otherwise
     *
     * @return short representation of the bytes
     */
    public static short byteArrayToShort(byte[] paRawBytes, boolean pbBigEndian) {
        return byteArrayToShort(paRawBytes, 0, pbBigEndian);
    }

    /**
     * Converts a portion of a byte array with given offset to short value.
     *
     * @param paRawBytes the byte array
     * @param piOffset offset in the original array to start reading bytes from
     * @param pbBigEndian true if the bytes are in Big-endian order; false
     * otherwise
     *
     * @return short representation of the bytes
     */
    public static short byteArrayToShort(byte[] paRawBytes, int piOffset, boolean pbBigEndian) {
        int iRetVal = -1;


        if (paRawBytes.length < piOffset + 2) {
            return -1;
        }

        int iLow;
        int iHigh;

        if (pbBigEndian) {
            iLow = paRawBytes[piOffset + 1];
            iHigh = paRawBytes[piOffset + 0];
        } else {
            iLow = paRawBytes[piOffset + 0];
            iHigh = paRawBytes[piOffset + 1];
        }


        iRetVal = (iHigh << 8) | (0xFF & iLow);

        return (short) iRetVal;
    }

    /**
     * Converts a byte array to int value. Equivalent to
     * intArrayToShort(paRawBytes, 0, pbBigEndian);
     *
     * @param paRawBytes the byte array
     * @param pbBigEndian true if the bytes are in Big-endian order; false
     * otherwise
     *
     * @return int representation of the bytes
     */
    public static int byteArrayToInt(byte[] paRawBytes, boolean pbBigEndian) {
        return byteArrayToInt(paRawBytes, 0, pbBigEndian);
    }

    /**
     * Converts a portion of a byte array with given offset to int value.
     *
     * @param paRawBytes the byte array
     * @param piOffset offset in the original array to start reading bytes from
     * @param pbBigEndian true if the bytes are in Big-endian order; false
     * otherwise
     *
     * @return int representation of the bytes
     */
    public static int byteArrayToInt(byte[] paRawBytes, int piOffset, boolean pbBigEndian) {
        int iRetVal = -1;

        if (paRawBytes.length < piOffset + 4) {
            return iRetVal;
        }

        int iLowest;
        int iLow;
        int iMid;
        int iHigh;

        if (pbBigEndian) {
            iLowest = paRawBytes[piOffset + 3];
            iLow = paRawBytes[piOffset + 2];
            iMid = paRawBytes[piOffset + 1];
            iHigh = paRawBytes[piOffset + 0];
        } else {
            iLowest = paRawBytes[piOffset + 0];
            iLow = paRawBytes[piOffset + 1];
            iMid = paRawBytes[piOffset + 2];
            iHigh = paRawBytes[piOffset + 3];
        }

        // Merge four bytes to form a 32-bit int value.
        iRetVal = (iHigh << 24) | (iMid << 16) | (iLow << 8) | (0xFF & iLowest);

        return iRetVal;
    }

    /**
     * Converts an int value to a byte array.
     *
     * @param piValueToConvert the original integer
     * @param pbBigEndian true if the bytes are in Big-endian order; false
     * otherwise
     *
     * @return byte[] representation of the int
     */
    public static byte[] intToByteArray(int piValueToConvert, boolean pbBigEndian) {
        byte[] aRetVal = new byte[4];

        byte iLowest;
        byte iLow;
        byte iMid;
        byte iHigh;

        iLowest = (byte) (piValueToConvert & 0xFF);
        iLow = (byte) ((piValueToConvert >> 8) & 0xFF);
        iMid = (byte) ((piValueToConvert >> 16) & 0xFF);
        iHigh = (byte) ((piValueToConvert >> 24) & 0xFF);

        if (pbBigEndian) {
            aRetVal[3] = iLowest;
            aRetVal[2] = iLow;
            aRetVal[1] = iMid;
            aRetVal[0] = iHigh;
        } else {
            aRetVal[0] = iLowest;
            aRetVal[1] = iLow;
            aRetVal[2] = iMid;
            aRetVal[3] = iHigh;
        }

        return aRetVal;
    }

    /**
     * Converts a byte array to String value. Cleans up non-word characters
     * along the way.
     *
     * Equivalent to byteArrayToString(paRawBytes, 0, paRawBytes.length);
     *
     * @param paRawBytes the byte array, non-UNICODE
     *
     * @return UNICODE String representation of the bytes
     */
    public static String byteArrayToString(byte[] paRawBytes) {
        return byteArrayToString(paRawBytes, 0, paRawBytes.length);
    }

    /**
     * Converts a portion of a byte array to String value. Cleans up non-word
     * characters along the way.
     *
     * @param paRawBytes the byte array, non-UNICODE
     * @param piOffset offset in the original array to start reading bytes from
     * @param piLength how many bytes of the array paramter to interpret as
     * String
     *
     * @return UNICODE String representation of the bytes with trailing garbage
     * stripped; "" if array length is less than piOffset + piLength; "" if the
     * generatied string begins with garbage
     */
    public static String byteArrayToString(byte[] paRawBytes, int piOffset, int piLength) {
        if (paRawBytes.length < piOffset + piLength) {
            return "";
        }

        String oBeautifulString = new String(paRawBytes, piOffset, piLength);
        int i = 0;

        if (oBeautifulString.matches("^\\W") == true) {
            oBeautifulString = "";
        } else {
            for (i = piOffset; i < piOffset + piLength; i++) {
                if (paRawBytes[i] < 32 || paRawBytes[i] > 128) {
                    break;
                }
            }

            oBeautifulString = oBeautifulString.substring(0, i - piOffset);
        }

        return oBeautifulString;
    }

    /**
     * Converts a String value to a byte array in US-ASCII charset.
     *
     * Equivalent to stringToByteArray(pstrStringToConvert, "US-ASCII");
     *
     * @param pstrStringToConvert the original string
     *
     * @return null-terminated byte[] representation of the String
     */
    public static byte[] stringToByteArray(String pstrStringToConvert) {
        return stringToByteArray(pstrStringToConvert, "US-ASCII");
    }

    /**
     * Attempts to convert a String value to a byte array in specified charset.
     * If the charset is invalid, returns plain byte-representation of the host
     * environment.
     *
     * @param pstrStringToConvert the original string
     * @param pstrCharSet characted set to assume for the original string
     *
     * @return null-terminated byte[] representation of the String
     */
    public static byte[] stringToByteArray(String pstrStringToConvert, String pstrCharSet) {
        byte[] aRecordData = null;

        try {
            aRecordData = (pstrStringToConvert + '\0').getBytes(pstrCharSet);
        } catch (UnsupportedEncodingException e) {
            System.err.println("WARNING: " + e);
            aRecordData = (pstrStringToConvert + '\0').getBytes();
        }

        return aRecordData;
    }
}
