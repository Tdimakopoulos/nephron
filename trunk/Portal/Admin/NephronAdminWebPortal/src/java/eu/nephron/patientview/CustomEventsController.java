/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientview;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;


/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class CustomEventsController implements Serializable {

//    private List<Timeline> timelines;
//    private TimelineEvent selectedEvent;
    private String eventStyle = "box";
    private String axisPosition = "bottom";
    private boolean showNavigation = true;
String previousid="NA";

    

    private void checkforrefresh()
    {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
            previousid=puserid;
        }else
        {
            if(previousid.equalsIgnoreCase(puserid))
            {}else
            {
                previousid=puserid;
                LoadData();
            }
        }
    }
    
    private void LoadData()
    {
        
    }
    public CustomEventsController() {
        LoadData();
    }

//    public void onEventSelect(SelectEvent event) {
//        selectedEvent = (TimelineEvent) event.getObject();
//    }
//
//    public List<Timeline> getTimelines() {
//        checkforrefresh();
//        return timelines;
//    }
//
//    public TimelineEvent getSelectedEvent() {
//        return selectedEvent;
//    }

    /**
     * @return the eventStyle
     */
    public String getEventStyle() {
        return eventStyle;
    }

    /**
     * @param eventStyle the eventStyle to set
     */
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    /**
     * @return the axisPosition
     */
    public String getAxisPosition() {
        return axisPosition;
    }

    /**
     * @param axisPosition the axisPosition to set
     */
    public void setAxisPosition(String axisPosition) {
        this.axisPosition = axisPosition;
    }

    /**
     * @return the showNavigation
     */
    public boolean isShowNavigation() {
        return showNavigation;
    }

    /**
     * @param showNavigation the showNavigation to set
     */
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }
}
