/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.charts;

import eu.nephron.model.Sensors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import org.primefaces.model.chart.CartesianChartModel;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class ChartBean implements Serializable {

    private CartesianChartModel linearModel;
    private CartesianChartModel linearModelsodium;
    private CartesianChartModel linearModelpotasium;
    private CartesianChartModel linearModelurea;
    private CartesianChartModel linearModelph;
    private List<Sensors> sensors;
    private CartesianChartModel categoryModel;
String previousid="NA";

  private boolean checkforrefresh()
    {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
            previousid=puserid;
            return true;
        }else
        {
            if(previousid.equalsIgnoreCase(puserid))
            {
            return false;
            }else
            {
                previousid=puserid;
                //sumups = new ArrayList<DSSResults>();
                //populatesumups(sumups);
                return true;
            }
        }
    }
  
    public ChartBean() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        if(previousid.equalsIgnoreCase("NA"))
        {
        previousid=puserid;
        }
        sensors = new ArrayList<Sensors>();
        createLinearModel();
        createCategoryModel();
    }

    public CartesianChartModel getLinearModel() {
        return linearModel;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void updatereadings() {
        System.err.println("Updating Readings Called");
        createLinearModel();
        createCategoryModel();
    }

    private void createLinearModel() {
        
    }

    private void createCategoryModel() {
        //Not used yet
    }

    /**
     * @return the linearModelsodium
     */
    public CartesianChartModel getLinearModelsodium() {
        return linearModelsodium;
    }

    /**
     * @param linearModelsodium the linearModelsodium to set
     */
    public void setLinearModelsodium(CartesianChartModel linearModelsodium) {
        this.linearModelsodium = linearModelsodium;
    }

    /**
     * @return the linearModelpotasium
     */
    public CartesianChartModel getLinearModelpotasium() {
        return linearModelpotasium;
    }

    /**
     * @param linearModelpotasium the linearModelpotasium to set
     */
    public void setLinearModelpotasium(CartesianChartModel linearModelpotasium) {
        this.linearModelpotasium = linearModelpotasium;
    }

    /**
     * @return the linearModelurea
     */
    public CartesianChartModel getLinearModelurea() {
        return linearModelurea;
    }

    /**
     * @param linearModelurea the linearModelurea to set
     */
    public void setLinearModelurea(CartesianChartModel linearModelurea) {
        this.linearModelurea = linearModelurea;
    }

    /**
     * @return the linearModelph
     */
    public CartesianChartModel getLinearModelph() {
        return linearModelph;
    }

    /**
     * @param linearModelph the linearModelph to set
     */
    public void setLinearModelph(CartesianChartModel linearModelph) {
        this.linearModelph = linearModelph;
    }

    /**
     * @return the sensors
     */
    public List<Sensors> getSensors() {

        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }

    
}
