/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.helper;

import eu.nephron.model.Patient;
import eu.nephron.patientsecurityws.SecurePatientConnection_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import eu.nephron.soap.ws.commandmanager.ActServiceManager_Service;
import eu.nephron.ws.medent.device.Device;

import java.util.List;

/**
 *
 * @author tdim
 */
public class entityhelper {

    public static void main(String[] args) {
//        entityhelper pp = new entityhelper();
//
//        System.out.println("ID=" + pp.GetPatientsPhoneID(new Long(3)));
//        System.out.println("Phone IMEI=" + pp.GetPatientsPhoneIMEIPP(new Long(1)));
//        System.out.println("Phone IMEI=" + pp.GetPatientsPhoneIMEI(new Long(1)));
//        System.out.println("WAKD Serial=" + pp.GetPatientsWAKDSERIAL(new Long(2)));
//        System.out.println("Name=" + pp.GetPatientName(new Long(3)));
        
        

    }

    public String GetPatientName(Long pID) {

        Patient pdd = findAll(pID);

        return pdd.getFirstName() + " " + pdd.getFatherName() + " " + pdd.getLastName();
    }

    public Patient findAll(Long pID) {
        Patient ppa = new Patient();

        java.util.List<eu.nephron.patientsecurityws.Patientconnections> pfind = findAll_1();
        for (int i = 0; i < pfind.size(); i++) {
            System.out.println("ID For Check =" + pfind.get(i).getIdpatient());
            if (pfind.get(i).getIdpatient() == pID) {
                PersonalInfoManager_Service service2 = new PersonalInfoManager_Service();
                eu.nephron.soap.ws.PersonalInfoManager port2 = service2.getPersonalInfoManagerPort();
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pp = port2.findPersonalInfoByID(pfind.get(i).getIdpersonnal());
                ppa.setFirstName(pp.get(0).getFirstName());
                ppa.setLastName(pp.get(0).getLastName());
                ppa.setFatherName(pp.get(0).getFatherName());
                ppa.setAddressid(pfind.get(i).getIdaddress());
                ppa.setPersonalinfoid(pfind.get(i).getIdpersonnal());
                ppa.setContactinfoid(pfind.get(i).getIdcontact());
            }
        }
        return ppa;
    }

    private java.util.List<eu.nephron.patientsecurityws.Patientconnections> findAll_1() {
        SecurePatientConnection_Service service = new SecurePatientConnection_Service();
        eu.nephron.patientsecurityws.SecurePatientConnection port = service.getSecurePatientConnectionPort();
        return port.findAllSecurePatientConnection();
    }

    private java.util.List<java.lang.Object> findConnections2(java.lang.Long arg0, java.lang.String arg1) {
        System.out.print("Calling WS to find connections");
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        return port.findConnections(arg0, arg1);
    }

    

    public String GetTextForType(int i) {
        if (i == 1) {
            return "SmartPhone";
        }
        if (i == 2) {
            return "WAKD";
        }
        if (i == 3) {
            return "Doctor";
        }
        if (i == 4) {
            return "Patient";
        }
        return "N/A";
    }

//    public boolean CheckIfPhone(Long ID) {
//        if (findMedicalEntity(ID).getCode().equalsIgnoreCase("SP")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public boolean CheckIfWAKD(Long ID) {
//        if (findMedicalEntity(ID).getCode().equalsIgnoreCase("WAKD")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public boolean CheckIfDOC(Long ID) {
//        if (findMedicalEntity(ID).getCode().equalsIgnoreCase("DOC")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public boolean CheckIfPATIENT(Long ID) {
//        if (findMedicalEntity(ID).getCode().equalsIgnoreCase("P")) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    public int FindType(Long ID) {
//        String Type = findMedicalEntity(ID).getCode();
//        if (Type.equalsIgnoreCase("WAKD")) {
//            return 2;
//        }
//        if (Type.equalsIgnoreCase("P")) {
//            return 4;
//        }
//        if (Type.equalsIgnoreCase("DOC")) {
//            return 3;
//        }
//        if (Type.equalsIgnoreCase("SP")) {
//            return 1;
//        }
//        return -1;
//
//    }

    public List<Object> GetPatientForDoctor(Long DoctorID) {
        return findConnections(DoctorID, "WS");

    }

//    public Long GetPatientsPhoneID(Long PatientID) {
//        List<Object> pFind = findConnections(PatientID, "WS");
//        for (int i = 0; i < pFind.size(); i++) {
//            if (FindType((Long) pFind.get(i)) == 1) {
//                return (Long) pFind.get(i);
//            }
//        }
//        return new Long(0);
//
//    }

    public String GetPatientsPhoneIMEI(Long PatientID) {

        return findDevice(PatientID).getSerialNumber();

    }

    public String GetPatientsPhoneIMEIPP(Long PatientID) {

        return findDevice(PatientID).getSerialNumber();

    }

//    public String GetPatientsPhoneIMEIV2(Long PatientID) {
//
//        return findDevice(GetPatientsPhoneID(PatientID)).getSerialNumber();
//
//    }

    public String GetPatientsWAKDSERIAL(Long PatientID) {

        return findDevice(PatientID).getSerialNumber();

    }

    private java.util.List<java.lang.Object> findConnections(java.lang.Long arg0, java.lang.String arg1) {
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        return port.findConnections(arg0, arg1);
    }

//    private static MedicalEntity_Type findMedicalEntity(java.lang.Object id) {
//        eu.nephron.ws.medicalentity.MedicalEntity_Service service = new eu.nephron.ws.medicalentity.MedicalEntity_Service();
//        eu.nephron.ws.medicalentity.MedicalEntity port = service.getMedicalEntityPort();
//        return port.findMedicalEntity(id);
//    }

    private static Device findDevice(java.lang.Object id) {
        eu.nephron.ws.medent.device.DeviceManagerSWS_Service service = new eu.nephron.ws.medent.device.DeviceManagerSWS_Service();
        eu.nephron.ws.medent.device.DeviceManagerSWS port = service.getDeviceManagerSWSPort();
        return port.findDevice(id);
    }

    
}
