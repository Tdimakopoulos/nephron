/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.ScheduleEntrySelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@SessionScoped
public class ScheduleController {

    private ScheduleModel eventModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private Date appdate;
    private String appduration;
    private String appdesc;
    private String msmedication;//string
    private String msquantity;//int
    private Date msfrom;//long
    private Date msto;//long
    private String msevery;//int/long
    private String msrecurrence;//int
    private String description;//string

    public void savemedlistener(ActionEvent actionEvent) {
        
    }

    public void saveapplistener(ActionEvent actionEvent) {
        
    }

    public ScheduleController() {
        

    }

    public Date getRandomDate(Date base) {
        Calendar date = Calendar.getInstance();
        date.setTime(base);
        date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1);        //set random day of month

        return date.getTime();
    }

    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar;
    }

    private Date previousDay8Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 8);

        return t.getTime();
    }

    private Date previousDay11Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date today1Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 1);

        return t.getTime();
    }

    private Date theDayAfter3Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 2);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    private Date today6Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 6);

        return t.getTime();
    }

    private Date nextDay9Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 9);

        return t.getTime();
    }

    private Date nextDay11Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date fourDaysLater3pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 4);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    private Date normalizeDate(Date dinit) {
        Calendar t = Calendar.getInstance();
        t.setTime(dinit);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE));


        return t.getTime();
    }

    public void addEvent(ActionEvent actionEvent) {
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ScheduleEvent eventtoadd = new DefaultScheduleEvent(event.getTitle(), normalizeDate(event.getStartDate()), normalizeDate(event.getEndDate()));
        if (event.getId() == null) {
//            eu.nephron.soap.ws.ScheduledAct entity= new ScheduledAct();
//            entity.setText(event.getTitle());
//            GregorianCalendar gc = new GregorianCalendar();
//            gc.setTimeInMillis(event.getStartDate().getTime());
//            entity.setActivityDate(df.newXMLGregorianCalendar(gc));
//            gc.setTimeInMillis(event.getEndDate().getTime());
//            entity.setEffectiveDate(df.newXMLGregorianCalendar(gc));
//            scheduledActcreate(entity);
            eventModel.addEvent(eventtoadd);
        } else {
            eventModel.deleteEvent(event);
            eventModel.addEvent(eventtoadd);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(ScheduleEntrySelectEvent selectEvent) {
        event = selectEvent.getScheduleEvent();
    }

    public void onDateSelect(DateSelectEvent selectEvent) {

        event = new DefaultScheduleEvent("", selectEvent.getDate(), selectEvent.getDate());

    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * @return the appdate
     */
    public Date getAppdate() {
        return appdate;
    }

    /**
     * @param appdate the appdate to set
     */
    public void setAppdate(Date appdate) {
        this.appdate = appdate;
    }

    /**
     * @return the appduration
     */
    public String getAppduration() {
        return appduration;
    }

    /**
     * @param appduration the appduration to set
     */
    public void setAppduration(String appduration) {
        this.appduration = appduration;
    }

    /**
     * @return the appdesc
     */
    public String getAppdesc() {
        return appdesc;
    }

    /**
     * @param appdesc the appdesc to set
     */
    public void setAppdesc(String appdesc) {
        this.appdesc = appdesc;
    }

    

    /**
     * @return the msmedication
     */
    public String getMsmedication() {
        return msmedication;
    }

    /**
     * @param msmedication the msmedication to set
     */
    public void setMsmedication(String msmedication) {
        this.msmedication = msmedication;
    }

    /**
     * @return the msquantity
     */
    public String getMsquantity() {
        return msquantity;
    }

    /**
     * @param msquantity the msquantity to set
     */
    public void setMsquantity(String msquantity) {
        this.msquantity = msquantity;
    }

    /**
     * @return the msfrom
     */
    public Date getMsfrom() {
        return msfrom;
    }

    /**
     * @param msfrom the msfrom to set
     */
    public void setMsfrom(Date msfrom) {
        this.msfrom = msfrom;
    }

    /**
     * @return the msto
     */
    public Date getMsto() {
        return msto;
    }

    /**
     * @param msto the msto to set
     */
    public void setMsto(Date msto) {
        this.msto = msto;
    }

    /**
     * @return the msevery
     */
    public String getMsevery() {
        return msevery;
    }

    /**
     * @param msevery the msevery to set
     */
    public void setMsevery(String msevery) {
        this.msevery = msevery;
    }

    /**
     * @return the msrecurrence
     */
    public String getMsrecurrence() {
        return msrecurrence;
    }

    /**
     * @param msrecurrence the msrecurrence to set
     */
    public void setMsrecurrence(String msrecurrence) {
        this.msrecurrence = msrecurrence;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    
}
