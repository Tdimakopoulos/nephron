/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.session;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Thomas
 */
@ManagedBean
@ApplicationScoped
public class LoginManager {

    private String username;
    private String password;
    private String userid;
    private int irole;

    /**
     * Creates a new instance of LoginManager
     */
    public LoginManager() {
    }

    /**
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the irole
     */
    public int getIrole() {
        return irole;
    }

    /**
     * @param irole the irole to set
     */
    public void setIrole(int irole) {
        this.irole = irole;
    }



    public String login() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Checking Username and Password"));
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage msg = null;
        if (this.username.equalsIgnoreCase("admin"))
            if (this.password.equalsIgnoreCase("cb1312ef"))
                return "DashBoard?faces-redirect=true";
        
        return "index?faces-redirect=true";
        
    }

    public String logout() {
        username = "";
        password = "";
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index?faces-redirect=true";
    }
}
