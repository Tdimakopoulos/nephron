/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.pkimanager;

import eu.nephron.webservices.KeyManagerService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class SecurityManager {
    

    /**
     * Creates a new instance of SecurityManager
     */
    public SecurityManager() {
    }

    private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        KeyManagerService service= new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2);
    }

    private java.util.List<eu.nephron.webservices.KeyDB> queryAllUsers() {
        KeyManagerService service= new KeyManagerService();
        eu.nephron.webservices.KeyManager port = service.getKeyManagerPort();
        return port.queryAllUsers();
    }
    
}
