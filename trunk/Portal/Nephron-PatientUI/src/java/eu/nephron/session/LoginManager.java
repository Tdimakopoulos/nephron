/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.session;

import eu.nephron.credentials.patient.ws.CredentialsPatientWS_Service;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class LoginManager {

    private String username;
    private String password;
    private String userid;
    private int irole;

    /**
     * Creates a new instance of LoginManager
     */
    public LoginManager() {
    }

    /**
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userid
     */
    public String getUserid() {
        return userid;
    }

    /**
     * @param userid the userid to set
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * @return the irole
     */
    public int getIrole() {
        return irole;
    }

    /**
     * @param irole the irole to set
     */
    public void setIrole(int irole) {
        this.irole = irole;
    }

    public String login() {


        
        
        Long lPatientID = new Long(0);
        java.util.List<eu.nephron.credentials.patient.ws.Credentialspatientsdb> pfind = findAll();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getUsername().equalsIgnoreCase(username)) {
                if (pfind.get(i).getPassword().equalsIgnoreCase(password)) {
                    //puserid = String.valueOf(pfind.get(i).getDoctorid());
                    lPatientID = pfind.get(i).getPatientid();
                }
            }

        }

        
        



        if (lPatientID > -1) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PatientIMEI", "352912056975154");
            return "DashBoard?faces-redirect=true";
        } else {
            username = "";
            password = "";
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            return "index?faces-redirect=true";
        }
    }

    public String logout() {
        username = "";
        password = "";
        //FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index?faces-redirect=true";
    }

    private java.util.List<eu.nephron.credentials.patient.ws.Credentialspatientsdb> findAll() {
        CredentialsPatientWS_Service service = new CredentialsPatientWS_Service();
        eu.nephron.credentials.patient.ws.CredentialsPatientWS port = service.getCredentialsPatientWSPort();
        return port.findAll();
    }
}
