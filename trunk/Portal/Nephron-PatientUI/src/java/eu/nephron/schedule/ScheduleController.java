/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.schedule;

import eu.nephron.schedule.webservices.ScheduleActAndTaskManager_Service;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;
import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class ScheduleController {
  

     private ScheduleModel eventModel;  
      
    private ScheduleModel lazyEventModel;  
  
    private ScheduleEvent event = new DefaultScheduleEvent();  
      
    private String theme;  
  
    public ScheduleController() {  
        eventModel = new DefaultScheduleModel();
        java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> pfind = findAllScheduleActAndTaskManager();
        String PhoneIMEI =(String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PatientIMEI");
        

        for (int i = 0; i < pfind.size(); i++) {
            if (PhoneIMEI.equalsIgnoreCase(pfind.get(i).getImei())) {
                String MoreDesc = "";

                if (pfind.get(i).getItype() == 1) {
                    MoreDesc = "Appointment";
                }
                if (pfind.get(i).getItype() == 2) {
                    MoreDesc = "Medication Schedule";
                }

                
                //MoreDesc = MoreDesc + " " + pHelper.GetPatientName(pfind.get(i).getPatientid()) + " ";
                Date datefrom = new Date();
                Date dateto = new Date();
                datefrom.setTime(pfind.get(i).getDateap());
                dateto.setTime(pfind.get(i).getDateap() + (60 * 60 * 1000));
                ScheduleEvent eventtoadd = null;
                if (pfind.get(i).getItype() == 1) {
                    eventtoadd = new DefaultScheduleEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom, dateto, "emp1");
                }
                if (pfind.get(i).getItype() == 2) {
                    eventtoadd = new DefaultScheduleEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom, dateto, "emp2");
                }
                eventModel.addEvent(eventtoadd);
            }
        }  
         
    }  
      
    public void addEvent(ActionEvent actionEvent) {  
        if(getEvent().getId() == null)  
            getEventModel().addEvent(getEvent());  
        else  
            getEventModel().updateEvent(getEvent());  
          
        setEvent(new DefaultScheduleEvent());  
    }  
      
    public void onEventSelect(SelectEvent selectEvent) {  
        setEvent((ScheduleEvent) selectEvent.getObject());  
    }  
      
    public void onDateSelect(DateSelectEvent selectEvent) {  
    //    event = new DefaultScheduleEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());  
    }  
      
    public void onEventMove(ScheduleEntryMoveEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());  
          
        addMessage(message);  
    }  
      
    public void onEventResize(ScheduleEntryResizeEvent event) {  
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());  
          
        addMessage(message);  
    }  
      
    private void addMessage(FacesMessage message) {  
        FacesContext.getCurrentInstance().addMessage(null, message);  
    }  

    /**
     * @return the eventModel
     */
    public ScheduleModel getEventModel() {
        return eventModel;
    }

    /**
     * @param eventModel the eventModel to set
     */
    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }

    /**
     * @return the lazyEventModel
     */
    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }

    /**
     * @param lazyEventModel the lazyEventModel to set
     */
    public void setLazyEventModel(ScheduleModel lazyEventModel) {
        this.lazyEventModel = lazyEventModel;
    }

    /**
     * @return the event
     */
    public ScheduleEvent getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    /**
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * @param theme the theme to set
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    private java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> findAllScheduleActAndTaskManager() {
        ScheduleActAndTaskManager_Service service= new ScheduleActAndTaskManager_Service();
        eu.nephron.schedule.webservices.ScheduleActAndTaskManager port = service.getScheduleActAndTaskManagerPort();
        return port.findAllScheduleActAndTaskManager();
    }
}
