/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.quest;

import eu.nephron.questionary.QuestionaryManager_Service;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;
import org.primefaces.context.RequestContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class questmanager {
    

    private String s1="0";
    private String s2="0";
    private String s3="0";
    private String s4="0";
    
    /**
     * Creates a new instance of questmanager
     */
    public questmanager() {
    }

    public void selectMenuListener() {
    System.out.println("-------- >> " + s1+s2+s3+s4); // here null coming
    
}
    
   
    
    public String save2()
    {
        String PhoneIMEI =(String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PatientIMEI");
        System.out.print("Save"+s1+s2+s3+s4);
        questionaryManagerSaveQuestionReply("1",PhoneIMEI,String.valueOf(new Date().getTime()),s1);
        questionaryManagerSaveQuestionReply("2",PhoneIMEI,String.valueOf(new Date().getTime()),s2);
        questionaryManagerSaveQuestionReply("3",PhoneIMEI,String.valueOf(new Date().getTime()),s3);
        questionaryManagerSaveQuestionReply("4",PhoneIMEI,String.valueOf(new Date().getTime()),s4);
        return "DashBoard?faces-redirect=true";

        
    }
    
     
     
    private String questionaryManagerSaveQuestionReply(java.lang.String questionid, java.lang.String imei, java.lang.String questionarydate, java.lang.String questionreply) {
        QuestionaryManager_Service service=new QuestionaryManager_Service();
        eu.nephron.questionary.QuestionaryManager port = service.getQuestionaryManagerPort();
        return port.questionaryManagerSaveQuestionReply(questionid, imei, questionarydate, questionreply);
    }

    /**
     * @return the s1
     */
    public String getS1() {
        return s1;
    }

    /**
     * @param s1 the s1 to set
     */
    public void setS1(String s1) {
        this.s1 = s1;
    }

    /**
     * @return the s2
     */
    public String getS2() {
        return s2;
    }

    /**
     * @param s2 the s2 to set
     */
    public void setS2(String s2) {
        this.s2 = s2;
    }

    /**
     * @return the s3
     */
    public String getS3() {
        return s3;
    }

    /**
     * @param s3 the s3 to set
     */
    public void setS3(String s3) {
        this.s3 = s3;
    }

    /**
     * @return the s4
     */
    public String getS4() {
        return s4;
    }

    /**
     * @param s4 the s4 to set
     */
    public void setS4(String s4) {
        this.s4 = s4;
    }
    
}
