/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.alarms;

import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import eu.nephron.secure.ws.WAKDAlertsManager_Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class beanalarm {
    
    

    private List<AlertsModel> alertslist=new ArrayList<AlertsModel>();
    /**
     * Creates a new instance of beanalarm
     */
    public beanalarm() {
        
        String pimei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PatientIMEI");
        
        
        java.util.List<eu.nephron.secure.ws.WakdAlerts> palist=findWAKDAlertsManagerWithIMEI(pimei);
        
        for(int i=0;i<palist.size();i++)
        {
        AlertsModel item = new AlertsModel();
        item.setSeverity("Critical");
        item.setMessage(palist.get(i).getMsgPatient());
        item.setdDate(palist.get(i).getDdate().toString());
        alertslist.add(item);
        }
        
    }

    /**
     * @return the alertslist
     */
    public List<AlertsModel> getAlertslist() {
        return alertslist;
    }

    /**
     * @param alertslist the alertslist to set
     */
    public void setAlertslist(List<AlertsModel> alertslist) {
        this.alertslist = alertslist;
    }

    private java.util.List<eu.nephron.secure.ws.WakdAlerts> findWAKDAlertsManagerWithIMEI(java.lang.String imei) {
        WAKDAlertsManager_Service service=new WAKDAlertsManager_Service();
        eu.nephron.secure.ws.WAKDAlertsManager port = service.getWAKDAlertsManagerPort();
        return port.findWAKDAlertsManagerWithIMEI(imei);
    }
}
