/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

/**
 *
 * @author tdim
 */
public class AlertsModel {

    private Long ID;
    private String patient;
    private String severity;
    private String dDate;
    private String Check;
    private String message;

    /**
     * @return the ID
     */
    public Long getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(Long ID) {
        this.ID = ID;
    }

    /**
     * @return the patient
     */
    public String getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(String patient) {
        this.patient = patient;
    }

    /**
     * @return the dDate
     */
    public String getdDate() {
        return dDate;
    }

    /**
     * @param dDate the dDate to set
     */
    public void setdDate(String dDate) {
        this.dDate = dDate;
    }

    /**
     * @return the Check
     */
    public String getCheck() {
        return Check;
    }

    /**
     * @param Check the Check to set
     */
    public void setCheck(String Check) {
        this.Check = Check;
    }

   

    /**
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
