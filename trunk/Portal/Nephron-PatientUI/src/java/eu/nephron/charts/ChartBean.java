/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.charts;

import eu.nephron.dateutils.dateutils;
import eu.nephron.mestypes.mestypes;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
//@SessionScoped
@RequestScoped
public class ChartBean {

    private CartesianChartModel categoryModel;
    private CartesianChartModel categoryModelweekly;
    
    private String number="Inlet Uria";

    public void showhide() {

    }

    public ChartBean() {
        createCategoryModel();
    }





    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void createCategoryModel() {
        mestypes ptypes=new mestypes();
        if(ptypes.GetType(number)==-1)
        {}
        else
        {
        String pimei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PatientIMEI");
        dateutils pdate = new dateutils();
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pmes = findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(pimei, ptypes.GetType(number), pdate.PreviousDay0pm().getTime());

        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pmesweek = findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(pimei, ptypes.GetType(number), pdate.p20DaysAgo0pm().getTime());//SevenDaysAgo0pm().getTime());
        System.out.println(pdate.PreviousDay0pm().getTime());
        System.out.println(pdate.SevenDaysAgo0pm().getTime());
        categoryModel = new CartesianChartModel();
        categoryModelweekly= new CartesianChartModel();
        
        Date pdate2 = new Date();
        
        ChartSeries mesr = new ChartSeries();
        mesr.setLabel("Daily");

        ChartSeries mesrweek = new ChartSeries();
        mesrweek.setLabel("Weekly");

        for (int i = 0; i < pmes.size(); i++) {
            pdate2.setTime(pmesweek.get(i).getDate());
            String Show = "";
            DateFormat df=new SimpleDateFormat("MM/dd/yyyy");
            Show = df.format(pdate2);
            mesr.set(Show, pmes.get(i).getValue());
        }


        

        for (int i = 0; i < pmesweek.size(); i++) {
            pdate2.setTime(pmesweek.get(i).getDate());
            String Show = "";
            DateFormat df=new SimpleDateFormat("MM/dd/yyyy");
            Show = df.format(pdate2);

                mesrweek.set(Show, pmesweek.get(i).getValue());
                
        }



        if (pmes.size() != 0) {
            categoryModel.addSeries(mesr);
        }else
        {
            mesr.set("0", 0);
            categoryModel.addSeries(mesr);
        }

        if (pmesweek.size() != 0) {
            categoryModelweekly.addSeries(mesrweek);
        }else
        {
            mesrweek.set("0", 0);
            categoryModelweekly.addSeries(mesr);
        }
        }
    }


    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }


    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(java.lang.String imei, java.lang.Long arg1, java.lang.Long arg2) {
        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
        return port.findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(imei, arg1, arg2);
    }

    /**
     * @return the categoryModelweekly
     */
    public CartesianChartModel getCategoryModelweekly() {
        return categoryModelweekly;
    }

    /**
     * @param categoryModelweekly the categoryModelweekly to set
     */
    public void setCategoryModelweekly(CartesianChartModel categoryModelweekly) {
        this.categoryModelweekly = categoryModelweekly;
    }
}

