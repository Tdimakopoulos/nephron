/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.style;

import eu.nephron.web.settings.websettings;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SlideEndEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class styleBean {

    private int number1 = 14;
    private int number2 = 14;
    private int number3 = 14;
    private int number4 = 14;
    private int number5 = 14;
    private String size1 = String.valueOf(number1) + "px";
    private String size2 = String.valueOf(number2) + "px";
    private String size3 = String.valueOf(number3) + "px";
    private String size4 = String.valueOf(number4) + "px";
    private String size5 = String.valueOf(number5) + "px";

    public void onSlideEnd(SlideEndEvent event) {
        FacesMessage msg = new FacesMessage("Text Size Updated ", "Value: " + event.getValue());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * Creates a new instance of styleBean
     */
    public styleBean() {
        try {
            websettings pset = new websettings();
            number1 = Integer.valueOf(pset.getLevel1());
            number2 = Integer.valueOf(pset.getLevel2());
            number3 = Integer.valueOf(pset.getLevel3());
            number4 = Integer.valueOf(pset.getLevel4());
            number5 = Integer.valueOf(pset.getLevel5());

            size1 = String.valueOf(number1) + "px";
            size2 = String.valueOf(number2) + "px";
            size3 = String.valueOf(number3) + "px";
            size4 = String.valueOf(number4) + "px";
            size5 = String.valueOf(number5) + "px";
        } catch (IOException ex) {
            Logger.getLogger(styleBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the size1
     */
    public String getSize1() {
        return String.valueOf(number1) + "px";
    }

    /**
     * @param size1 the size1 to set
     */
    public void setSize1(String size1) {
        this.size1 = size1;
    }

    /**
     * @return the size2
     */
    public String getSize2() {
        return String.valueOf(number2) + "px";
    }

    /**
     * @param size2 the size2 to set
     */
    public void setSize2(String size2) {
        this.size2 = size2;
    }

    /**
     * @return the size3
     */
    public String getSize3() {
        return String.valueOf(number3) + "px";
    }

    /**
     * @param size3 the size3 to set
     */
    public void setSize3(String size3) {
        this.size3 = size3;
    }

    /**
     * @return the size4
     */
    public String getSize4() {
        return String.valueOf(number4) + "px";
    }

    /**
     * @param size4 the size4 to set
     */
    public void setSize4(String size4) {
        this.size4 = size4;
    }

    /**
     * @return the size5
     */
    public String getSize5() {
        return String.valueOf(number5) + "px";
    }

    /**
     * @param size5 the size5 to set
     */
    public void setSize5(String size5) {
        this.size5 = size5;
    }

    /**
     * @return the number1
     */
    public int getNumber1() {
        return number1;
    }

    /**
     * @param number1 the number1 to set
     */
    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    /**
     * @return the number2
     */
    public int getNumber2() {
        return number2;
    }

    /**
     * @param number2 the number2 to set
     */
    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    /**
     * @return the number3
     */
    public int getNumber3() {
        return number3;
    }

    /**
     * @param number3 the number3 to set
     */
    public void setNumber3(int number3) {
        this.number3 = number3;
    }

    /**
     * @return the number4
     */
    public int getNumber4() {
        return number4;
    }

    /**
     * @param number4 the number4 to set
     */
    public void setNumber4(int number4) {
        this.number4 = number4;
    }

    /**
     * @return the number5
     */
    public int getNumber5() {
        return number5;
    }

    /**
     * @param number5 the number5 to set
     */
    public void setNumber5(int number5) {
        this.number5 = number5;
    }

    public void savestyle(ActionEvent actionEvent) {
        websettings pset;
        try {
            pset = new websettings();
            pset.setLevel1(String.valueOf(number1));
            pset.setLevel2(String.valueOf(number2));
            pset.setLevel3(String.valueOf(number3));
            pset.setLevel4(String.valueOf(number4));
            pset.setLevel5(String.valueOf(number5));
            pset.saveChangesAsXML();
        } catch (IOException ex) {
            Logger.getLogger(styleBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
