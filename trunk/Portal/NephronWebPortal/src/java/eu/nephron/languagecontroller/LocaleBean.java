/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.languagecontroller;

import eu.nephron.themes.TabBean;
import eu.nephron.web.settings.websettings;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@ApplicationScoped
public class LocaleBean {

    private String language;

    /**
     * Creates a new instance of LocaleBean
     */
    public LocaleBean() {
        try {
            websettings pset= new websettings();
            language=pset.getLocale();
        } catch (IOException ex) {
            Logger.getLogger(TabBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}
