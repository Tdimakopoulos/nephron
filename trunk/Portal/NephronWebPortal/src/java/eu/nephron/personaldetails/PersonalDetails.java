/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.personaldetails;

import eu.nephron.helper.entityhelper;
import eu.nephron.model.Patient;
import eu.nephron.soap.ws.AddressManager_Service;
import eu.nephron.soap.ws.ContactInfoManager_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class PersonalDetails {

    private int ilevel;
    private String fathername;
    private String firstname;
    private String lastname;
    private int imstatus;
    private String middlename;
    private String title;
    private String companyemail;
    private String companymobile;
    private String companyphone;
    private String email;
    private String mobile;
    private String phone;
    private String administrativearea;
    private String city;
    private String code;
    private String floor;
    private String postcode;
    private String street;
    private String streetno;

    private String patienteducationlevel;
    private String patientmaritalstatus;
    
//     CreateVoc("Education Level Text","EDULEVEL","ELEMENTARY","ENUM","1");
//        CreateVoc("Education Level Text","EDULEVEL","HIGH_SCOOL","ENUM","2");
//        CreateVoc("Education Level Text","EDULEVEL","COLLEGE","ENUM","3");
//        CreateVoc("Education Level Text","EDULEVEL","UNIVERSITY","ENUM","4");
//        CreateVoc("Education Level Text","EDULEVEL","MASTER","ENUM","5");
//        CreateVoc("Education Level Text","EDULEVEL","PHD","ENUM","6");
//        CreateVoc("Education Level Text","EDULEVEL","UNDEFINED","ENUM","7");
//        
//        
//        CreateVoc("Gender Text","GENDER","MALE","ENUM","1");
//        CreateVoc("Gender Text","GENDER","FEMALE","ENUM","2");
//        CreateVoc("Gender Text","GENDER","HERMAPHRODITE","ENUM","3");
//        CreateVoc("Education Level Text","EDULEVEL","UNDEFINED","ENUM","4");
//        
//        CreateVoc("Marital Status Text","MARITALSTATUS","SINGLE","ENUM","1");
//        CreateVoc("Marital Status Text","MARITALSTATUS","MARRIED","ENUM","2");
//        CreateVoc("Marital Status Text","MARITALSTATUS","DIVORCED","ENUM","3");
//        CreateVoc("Marital Status Text","MARITALSTATUS","UNDEFINED","ENUM","4");
    
    
    
    public PersonalDetails() {
//        entityhelper phelper = new entityhelper();
//        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
//        Patient pdetails = phelper.findAll(Long.parseLong(puserid));
//
//        List<eu.nephron.soap.ws.PersonalInfo> ppInfo = findPersonalInfoByID(pdetails.getPersonalinfoid());
//
//        ilevel = ppInfo.get(0).getEducationalLevel();
//        
//        if (ilevel==1)patienteducationlevel="ELEMENTARY";
//        if (ilevel==2)patienteducationlevel="HIGH_SCOOL";
//        if (ilevel==3)patienteducationlevel="COLLEGE";
//        if (ilevel==4)patienteducationlevel="UNIVERSITY";
//        if (ilevel==5)patienteducationlevel="MASTER";
//        if (ilevel==6)patienteducationlevel="PHD";
//        if (ilevel==7)patienteducationlevel="UNDEFINED";
//        
//        fathername = ppInfo.get(0).getFatherName();
//        firstname = ppInfo.get(0).getFirstName();
//        lastname = ppInfo.get(0).getLastName();
//        imstatus = ppInfo.get(0).getMaritalStatus();
//        
//        if (imstatus==1)patientmaritalstatus="SINGLE";
//        if (imstatus==2)patientmaritalstatus="MARRIED";
//        if (imstatus==3)patientmaritalstatus="DIVORCED";
//        if (imstatus==4)patientmaritalstatus="UNDEFINED";
//        
//        middlename = ppInfo.get(0).getMiddleName();
//        title = ppInfo.get(0).getTitle();
//
//        List<eu.nephron.soap.ws.ContactInfo> pcInfo = findContactInfoByID(pdetails.getContactinfoid());
//        companyemail = pcInfo.get(0).getCompanyEmail();
//        companymobile = pcInfo.get(0).getCompanyMobile();
//        companyphone = pcInfo.get(0).getCompanyPhone();
//        email = pcInfo.get(0).getEmail();
//        mobile = pcInfo.get(0).getMobile();
//        phone = pcInfo.get(0).getPhone();
//
//        List<eu.nephron.soap.ws.Address> paInfo = findAddressByID(pdetails.getAddressid());
//
//        administrativearea = paInfo.get(0).getAdministrativeArea();
//        city = paInfo.get(0).getCity();
//        code = paInfo.get(0).getCode();
//        floor = paInfo.get(0).getFloor();
//        postcode = paInfo.get(0).getPostCode();
//        street = paInfo.get(0).getStreet();
//        streetno = paInfo.get(0).getStreetNo();
    }

    private java.util.List<eu.nephron.soap.ws.PersonalInfo> findPersonalInfoByID(java.lang.Long arg0) {
        PersonalInfoManager_Service service = new PersonalInfoManager_Service();
        eu.nephron.soap.ws.PersonalInfoManager port = service.getPersonalInfoManagerPort();
        return port.findPersonalInfoByID(arg0);
    }

    private java.util.List<eu.nephron.soap.ws.ContactInfo> findContactInfoByID(java.lang.Long arg0) {
        ContactInfoManager_Service service = new ContactInfoManager_Service();
        eu.nephron.soap.ws.ContactInfoManager port = service.getContactInfoManagerPort();
        return port.findContactInfoByID(arg0);
    }

    private java.util.List<eu.nephron.soap.ws.Address> findAddressByID(java.lang.Long arg0) {
        AddressManager_Service service = new AddressManager_Service();
        eu.nephron.soap.ws.AddressManager port = service.getAddressManagerPort();
        return port.findAddressByID(arg0);
    }

    /**
     * @return the ilevel
     */
    public int getIlevel() {
        return ilevel;
    }

    /**
     * @param ilevel the ilevel to set
     */
    public void setIlevel(int ilevel) {
        this.ilevel = ilevel;
    }

    /**
     * @return the fathername
     */
    public String getFathername() {
        return fathername;
    }

    /**
     * @param fathername the fathername to set
     */
    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the imstatus
     */
    public int getImstatus() {
        return imstatus;
    }

    /**
     * @param imstatus the imstatus to set
     */
    public void setImstatus(int imstatus) {
        this.imstatus = imstatus;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the companyemail
     */
    public String getCompanyemail() {
        return companyemail;
    }

    /**
     * @param companyemail the companyemail to set
     */
    public void setCompanyemail(String companyemail) {
        this.companyemail = companyemail;
    }

    /**
     * @return the companymobile
     */
    public String getCompanymobile() {
        return companymobile;
    }

    /**
     * @param companymobile the companymobile to set
     */
    public void setCompanymobile(String companymobile) {
        this.companymobile = companymobile;
    }

    /**
     * @return the companyphone
     */
    public String getCompanyphone() {
        return companyphone;
    }

    /**
     * @param companyphone the companyphone to set
     */
    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the administrativearea
     */
    public String getAdministrativearea() {
        return administrativearea;
    }

    /**
     * @param administrativearea the administrativearea to set
     */
    public void setAdministrativearea(String administrativearea) {
        this.administrativearea = administrativearea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetno
     */
    public String getStreetno() {
        return streetno;
    }

    /**
     * @param streetno the streetno to set
     */
    public void setStreetno(String streetno) {
        this.streetno = streetno;
    }

    /**
     * @return the patienteducationlevel
     */
    public String getPatienteducationlevel() {
        return patienteducationlevel;
    }

    /**
     * @param patienteducationlevel the patienteducationlevel to set
     */
    public void setPatienteducationlevel(String patienteducationlevel) {
        this.patienteducationlevel = patienteducationlevel;
    }

    /**
     * @return the patientmaritalstatus
     */
    public String getPatientmaritalstatus() {
        return patientmaritalstatus;
    }

    /**
     * @param patientmaritalstatus the patientmaritalstatus to set
     */
    public void setPatientmaritalstatus(String patientmaritalstatus) {
        this.patientmaritalstatus = patientmaritalstatus;
    }
}
