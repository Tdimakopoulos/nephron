/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.themes;

import eu.nephron.web.settings.websettings;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class TabBean {

    private String effect = "slide";

    /**
     * Creates a new instance of TabBean
     */
    public TabBean() {
        try {
            websettings pset= new websettings();
            //effect=pset.getTabEffect();
        } catch (IOException ex) {
            Logger.getLogger(TabBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the effect
     */
    public String getEffect() {
        return effect;
    }

    /**
     * @param effect the effect to set
     */
    public void setEffect(String effect) {
        this.effect = effect;
    }
}
