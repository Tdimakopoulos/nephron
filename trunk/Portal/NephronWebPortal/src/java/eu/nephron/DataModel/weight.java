/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.DataModel;

/**
 *
 * @author tdim
 */
public class weight {
    private String weight;
    private String imei;
    private String bodyfat;
    private String wsbattery;
    private String tbd;
    private Long wtime;

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the bodyfat
     */
    public String getBodyfat() {
        return bodyfat;
    }

    /**
     * @param bodyfat the bodyfat to set
     */
    public void setBodyfat(String bodyfat) {
        this.bodyfat = bodyfat;
    }

    /**
     * @return the wsbattery
     */
    public String getWsbattery() {
        return wsbattery;
    }

    /**
     * @param wsbattery the wsbattery to set
     */
    public void setWsbattery(String wsbattery) {
        this.wsbattery = wsbattery;
    }

    /**
     * @return the tbd
     */
    public String getTbd() {
        return tbd;
    }

    /**
     * @param tbd the tbd to set
     */
    public void setTbd(String tbd) {
        this.tbd = tbd;
    }

    /**
     * @return the wtime
     */
    public Long getWtime() {
        return wtime;
    }

    /**
     * @param wtime the wtime to set
     */
    public void setWtime(Long wtime) {
        this.wtime = wtime;
    }
    
}
