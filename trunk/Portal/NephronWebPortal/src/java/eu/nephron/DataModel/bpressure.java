/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.DataModel;

/**
 *
 * @author tdim
 */
public class bpressure {
    private String imei;
    private String systolic;
    private String diastolic;
    private String heartrate;
    private Long bptime;

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the systolic
     */
    public String getSystolic() {
        return systolic;
    }

    /**
     * @param systolic the systolic to set
     */
    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    /**
     * @return the diastolic
     */
    public String getDiastolic() {
        return diastolic;
    }

    /**
     * @param diastolic the diastolic to set
     */
    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }

    /**
     * @return the heartrate
     */
    public String getHeartrate() {
        return heartrate;
    }

    /**
     * @param heartrate the heartrate to set
     */
    public void setHeartrate(String heartrate) {
        this.heartrate = heartrate;
    }

    /**
     * @return the bptime
     */
    public Long getBptime() {
        return bptime;
    }

    /**
     * @param bptime the bptime to set
     */
    public void setBptime(Long bptime) {
        this.bptime = bptime;
    }
    
}
