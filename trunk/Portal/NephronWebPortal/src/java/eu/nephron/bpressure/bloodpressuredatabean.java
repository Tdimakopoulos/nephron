/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bpressure;

import eu.nephron.DataModel.bpressure;
import eu.nephron.bloodpressure.datavalues.ws.Bloodpressurevaluesws;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class bloodpressuredatabean {
    
    
//bloodpressuredatabean
    private List<bpressure> sensors;
    private List<bpressure> sensors2;
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    /**
     * Creates a new instance of bloodpressuredatabean
     */
    public bloodpressuredatabean() {
        java.util.List<eu.nephron.bloodpressure.datavalues.ws.Bloodpressurevalues> pList=findAll();
        
         String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        
        sensors = new ArrayList<bpressure>();
        sensors2 = new ArrayList<bpressure>();
        
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        for(int i=0;i<pList.size();i++) 
        {
            if(pList.get(i).getImei().equalsIgnoreCase(PhoneIMEI))
            {
                bpressure item= new bpressure();
                item.setBptime(pList.get(i).getBptime());
                item.setDiastolic(pList.get(i).getDiastolic());
                item.setHeartrate(pList.get(i).getHeartrate());
                item.setSystolic(pList.get(i).getSystolic());
                item.setImei(pList.get(i).getImei());
                sensors.add(item);
                sensors2.add(item);
            }
            
        }
    }

    public void UpdateRecords(ActionEvent actionEvent) {
        sensors.clear();
        for (int i = 0; i < sensors2.size(); i++) {
            Date date = new Date();
            Long dtime = sensors2.get(i).getBptime();
            date.setTime(dtime);
            if (date.getTime() > dateFromM.getTime()) {
                if (date.getTime() < dateToM.getTime()) {
                    sensors.add(sensors2.get(i));
                }
            }
        }
    }
    /**
     * @return the sensors
     */
    public List<bpressure> getSensors() {
        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<bpressure> sensors) {
        this.sensors = sensors;
    }

    private java.util.List<eu.nephron.bloodpressure.datavalues.ws.Bloodpressurevalues> findAll() {
        Bloodpressurevaluesws service= new Bloodpressurevaluesws();
        eu.nephron.bloodpressure.datavalues.ws.NewWebService port = service.getNewWebServicePort();
        return port.findAll();
    }
}
