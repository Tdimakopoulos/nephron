/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

import eu.nephron.mestypes.mestypes;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class Sensors implements Serializable {

    private String type;
    private double value;
    private Date time;
    private String typename;
    

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * @return the typename
     */
    public String getTypename() {
//        typename="Unknown";
//        if (type.equalsIgnoreCase("8"))
//            return "Sodium";
//        if (type.equalsIgnoreCase("7"))
//            return "Potasium";
//        if (type.equalsIgnoreCase("10"))
//            return "Urea";
//        if (type.equalsIgnoreCase("6"))
//            return "Ph";
        mestypes ptypes= new mestypes();
        return ptypes.GetName(Long.parseLong(type));
        //return typename;
    }

    /**
     * @param typename the typename to set
     */
    public void setTypename(String typename) {
        this.typename = typename;
    }
}
