/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.schedule;

import eu.nephron.helper.entityhelper;
import eu.nephron.schedule.webservices.ScheduleActAndTaskManager_Service;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.ws.WebServiceRef;

import org.primefaces.event.DateSelectEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.ScheduleEntrySelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean
@SessionScoped
public class ScheduleController {

    private ScheduleModel eventModel;
    private ScheduleEvent event = new DefaultScheduleEvent();
    private Date appdate;
    private String appduration;
    private String appdesc;
    private String msmedication;//string
    private String msquantity;//int
    private Date msfrom;//long
    private Date msto;//long
    private String msevery;//int/long
    private String msrecurrence;//int
    private String description;//string

    public void savemedlistener(ActionEvent actionEvent) {
        System.err.println("***************************** Medication Scheduler Called ************************************");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Saving Appointment"));
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        entityhelper phelper = new entityhelper();
        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);
        System.err.println("Appointment Schedule --> " + "User ID " + puserid + " Phone IMEI " + phelper.GetPatientsPhoneIMEIPP(PhoneID) + " Doctor ID " + duserid);
        Date ddate = new Date();
        eu.nephron.schedule.webservices.ScheduleActAndTask pscheduleActAndTask = new eu.nephron.schedule.webservices.ScheduleActAndTask();

        pscheduleActAndTask.setImei(PhoneIMEI);
        pscheduleActAndTask.setPatientid(Long.parseLong(puserid));
        pscheduleActAndTask.setDoctorid(Long.parseLong(duserid));
        pscheduleActAndTask.setItype(2);
        pscheduleActAndTask.setSdesc(description);
        pscheduleActAndTask.setLl1(msto.getTime());
        pscheduleActAndTask.setI1(Integer.parseInt(msevery));
        pscheduleActAndTask.setText1(msmedication);
        pscheduleActAndTask.setDateap(msfrom.getTime());
        pscheduleActAndTask.setDurationhours(Long.parseLong(msevery));
        pscheduleActAndTask.setText2(msrecurrence);
        pscheduleActAndTask.setLl2(Long.parseLong(msquantity));

        createScheduleActAndTaskManager(pscheduleActAndTask);
    }

    public void saveapplistener(ActionEvent actionEvent) {
        System.err.println("***************************** Appointment Scheduler Called ************************************");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Please Wait", "Saving Appointment"));
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        entityhelper phelper = new entityhelper();
        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);
        System.err.println("Appointment Schedule --> " + "User ID " + puserid + " Phone IMEI " + phelper.GetPatientsPhoneIMEIPP(PhoneID) + " Doctor ID " + duserid);
        Date ddate = new Date();
        eu.nephron.schedule.webservices.ScheduleActAndTask pscheduleActAndTask = new eu.nephron.schedule.webservices.ScheduleActAndTask();
        pscheduleActAndTask.setDateap(appdate.getTime());
        pscheduleActAndTask.setDurationhours(Long.parseLong(appduration));
        pscheduleActAndTask.setImei(PhoneIMEI);
        pscheduleActAndTask.setPatientid(Long.parseLong(puserid));
        pscheduleActAndTask.setDoctorid(Long.parseLong(duserid));
        pscheduleActAndTask.setItype(1);
        pscheduleActAndTask.setSdesc(appdesc);

        createScheduleActAndTaskManager(pscheduleActAndTask);
    }

    public ScheduleController() {
        eventModel = new DefaultScheduleModel();
        java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> pfind = findAllScheduleActAndTaskManager();
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        Long loginDoctorID = Long.parseLong(duserid);

        for (int i = 0; i < pfind.size(); i++) {
            if (loginDoctorID == pfind.get(i).getDoctorid()) {
                String MoreDesc = "";

                if (pfind.get(i).getItype() == 1) {
                    MoreDesc = "Appointment";
                }
                if (pfind.get(i).getItype() == 2) {
                    MoreDesc = "Medication Schedule";
                }

                entityhelper pHelper = new entityhelper();
                MoreDesc = MoreDesc + " " + pHelper.GetPatientName(pfind.get(i).getPatientid()) + " ";
                Date datefrom = new Date();
                Date dateto = new Date();
                datefrom.setTime(pfind.get(i).getDateap());
                dateto.setTime(pfind.get(i).getDateap() + (60 * 60 * 1000));
                ScheduleEvent eventtoadd = null;
                if (pfind.get(i).getItype() == 1) {
                    eventtoadd = new DefaultScheduleEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom, dateto, "emp1");
                }
                if (pfind.get(i).getItype() == 2) {
                    eventtoadd = new DefaultScheduleEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom, dateto, "emp2");
                }
                eventModel.addEvent(eventtoadd);
            }
        }

    }

    public Date getRandomDate(Date base) {
        Calendar date = Calendar.getInstance();
        date.setTime(base);
        date.add(Calendar.DATE, ((int) (Math.random() * 30)) + 1);        //set random day of month

        return date.getTime();
    }

    public Date getInitialDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar.getTime();
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    private Calendar today() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);

        return calendar;
    }

    private Date previousDay8Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 8);

        return t.getTime();
    }

    private Date previousDay11Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) - 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date today1Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 1);

        return t.getTime();
    }

    private Date theDayAfter3Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 2);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    private Date today6Pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.HOUR, 6);

        return t.getTime();
    }

    private Date nextDay9Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 9);

        return t.getTime();
    }

    private Date nextDay11Am() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.AM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 1);
        t.set(Calendar.HOUR, 11);

        return t.getTime();
    }

    private Date fourDaysLater3pm() {
        Calendar t = (Calendar) today().clone();
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE) + 4);
        t.set(Calendar.HOUR, 3);

        return t.getTime();
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    private Date normalizeDate(Date dinit) {
        Calendar t = Calendar.getInstance();
        t.setTime(dinit);
        t.set(Calendar.AM_PM, Calendar.PM);
        t.set(Calendar.DATE, t.get(Calendar.DATE));


        return t.getTime();
    }

    public void addEvent(ActionEvent actionEvent) {
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
        }

        ScheduleEvent eventtoadd = new DefaultScheduleEvent(event.getTitle(), normalizeDate(event.getStartDate()), normalizeDate(event.getEndDate()));
        if (event.getId() == null) {
//            eu.nephron.soap.ws.ScheduledAct entity= new ScheduledAct();
//            entity.setText(event.getTitle());
//            GregorianCalendar gc = new GregorianCalendar();
//            gc.setTimeInMillis(event.getStartDate().getTime());
//            entity.setActivityDate(df.newXMLGregorianCalendar(gc));
//            gc.setTimeInMillis(event.getEndDate().getTime());
//            entity.setEffectiveDate(df.newXMLGregorianCalendar(gc));
//            scheduledActcreate(entity);
            eventModel.addEvent(eventtoadd);
        } else {
            eventModel.deleteEvent(event);
            eventModel.addEvent(eventtoadd);
        }

        event = new DefaultScheduleEvent();
    }

    public void onEventSelect(ScheduleEntrySelectEvent selectEvent) {
        event = selectEvent.getScheduleEvent();
    }

    public void onDateSelect(DateSelectEvent selectEvent) {

        event = new DefaultScheduleEvent("", selectEvent.getDate(), selectEvent.getDate());

    }

    public void onEventMove(ScheduleEntryMoveEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * @return the appdate
     */
    public Date getAppdate() {
        return appdate;
    }

    /**
     * @param appdate the appdate to set
     */
    public void setAppdate(Date appdate) {
        this.appdate = appdate;
    }

    /**
     * @return the appduration
     */
    public String getAppduration() {
        return appduration;
    }

    /**
     * @param appduration the appduration to set
     */
    public void setAppduration(String appduration) {
        this.appduration = appduration;
    }

    /**
     * @return the appdesc
     */
    public String getAppdesc() {
        return appdesc;
    }

    /**
     * @param appdesc the appdesc to set
     */
    public void setAppdesc(String appdesc) {
        this.appdesc = appdesc;
    }

    private void createScheduleActAndTaskManager(eu.nephron.schedule.webservices.ScheduleActAndTask scheduleActAndTask) {
        ScheduleActAndTaskManager_Service service = new ScheduleActAndTaskManager_Service();
        eu.nephron.schedule.webservices.ScheduleActAndTaskManager port = service.getScheduleActAndTaskManagerPort();
        port.createScheduleActAndTaskManager(scheduleActAndTask);
    }

    /**
     * @return the msmedication
     */
    public String getMsmedication() {
        return msmedication;
    }

    /**
     * @param msmedication the msmedication to set
     */
    public void setMsmedication(String msmedication) {
        this.msmedication = msmedication;
    }

    /**
     * @return the msquantity
     */
    public String getMsquantity() {
        return msquantity;
    }

    /**
     * @param msquantity the msquantity to set
     */
    public void setMsquantity(String msquantity) {
        this.msquantity = msquantity;
    }

    /**
     * @return the msfrom
     */
    public Date getMsfrom() {
        return msfrom;
    }

    /**
     * @param msfrom the msfrom to set
     */
    public void setMsfrom(Date msfrom) {
        this.msfrom = msfrom;
    }

    /**
     * @return the msto
     */
    public Date getMsto() {
        return msto;
    }

    /**
     * @param msto the msto to set
     */
    public void setMsto(Date msto) {
        this.msto = msto;
    }

    /**
     * @return the msevery
     */
    public String getMsevery() {
        return msevery;
    }

    /**
     * @param msevery the msevery to set
     */
    public void setMsevery(String msevery) {
        this.msevery = msevery;
    }

    /**
     * @return the msrecurrence
     */
    public String getMsrecurrence() {
        return msrecurrence;
    }

    /**
     * @param msrecurrence the msrecurrence to set
     */
    public void setMsrecurrence(String msrecurrence) {
        this.msrecurrence = msrecurrence;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    private java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> findAllScheduleActAndTaskManager() {
        ScheduleActAndTaskManager_Service service = new ScheduleActAndTaskManager_Service();
        eu.nephron.schedule.webservices.ScheduleActAndTaskManager port = service.getScheduleActAndTaskManagerPort();
        return port.findAllScheduleActAndTaskManager();
    }
}
