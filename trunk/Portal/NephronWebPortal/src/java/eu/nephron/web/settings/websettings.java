/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.web.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class websettings {

    String szSettingsPath = "c:\\websettings\\";
    String szSettingsPathUnix = "/home/exodus/websettings/";
    private String Level1;
    private String Level2;
    private String Level3;
    private String Level4;
    private String Level5;
    private String Locale;
    private String Theme;
    private String TabEffect;

    public websettings() throws IOException {
        FileInputStream fis = null;
        try {
            //Reading properties file in Java example
            Properties props = new Properties();
            fis = new FileInputStream(GetPathAndFilename());
            //loading properites from properties file
            props.loadFromXML(fis);
            //reading proeprty
            Level1 = props.getProperty("sizelevel1");
            //System.out.println("sizelevel1: " + Level1);

            Level2 = props.getProperty("sizelevel2");
            //System.out.println("sizelevel2: " + Level2);

            Level3 = props.getProperty("sizelevel3");
            //System.out.println("sizelevel3: " + Level3);

            Level4 = props.getProperty("sizelevel4");
            //System.out.println("sizelevel4: " + Level4);

            Level5 = props.getProperty("sizelevel5");
            //System.out.println("sizelevel5: " + Level5);

            Locale = props.getProperty("Locale");
            //System.out.println("Locale: " + Locale);

            Theme = props.getProperty("Theme");
            //System.out.println("Theme: " + Theme);

            TabEffect = props.getProperty("TabEffect");
            //System.out.println("TabEffect: " + TabEffect);

        } catch (FileNotFoundException ex) {
            //Logger.getLogger(websettings.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                //Logger.getLogger(websettings.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }

    public void saveChangesAsXML() {
        try {
            Properties props = new Properties();
            props.setProperty("sizelevel1", Level1);
            props.setProperty("sizelevel2", Level2);
            props.setProperty("sizelevel3", Level3);
            props.setProperty("sizelevel4", Level4);
            props.setProperty("sizelevel5", Level5);
            props.setProperty("Locale", Locale);
            props.setProperty("Theme", Theme);
            props.setProperty("TabEffect", TabEffect);
            File f = new File(GetPathAndFilename());
            OutputStream out = new FileOutputStream(f);
            props.storeToXML(out, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String GetPathAndFilename() {
        if (isUnix()) {
            return szSettingsPathUnix + "properties.xml";
        } else {
            return szSettingsPath + "properties.xml";
        }
    }

    private boolean isUnix() {
        if (File.separatorChar == '/') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the Level1
     */
    public String getLevel1() {
        return Level1;
    }

    /**
     * @param Level1 the Level1 to set
     */
    public void setLevel1(String Level1) {
        this.Level1 = Level1;
    }

    /**
     * @return the Level2
     */
    public String getLevel2() {
        return Level2;
    }

    /**
     * @param Level2 the Level2 to set
     */
    public void setLevel2(String Level2) {
        this.Level2 = Level2;
    }

    /**
     * @return the Level3
     */
    public String getLevel3() {
        return Level3;
    }

    /**
     * @param Level3 the Level3 to set
     */
    public void setLevel3(String Level3) {
        this.Level3 = Level3;
    }

    /**
     * @return the Level4
     */
    public String getLevel4() {
        return Level4;
    }

    /**
     * @param Level4 the Level4 to set
     */
    public void setLevel4(String Level4) {
        this.Level4 = Level4;
    }

    /**
     * @return the Level5
     */
    public String getLevel5() {
        return Level5;
    }

    /**
     * @param Level5 the Level5 to set
     */
    public void setLevel5(String Level5) {
        this.Level5 = Level5;
    }

    /**
     * @return the Locale
     */
    public String getLocale() {
        return Locale;
    }

    /**
     * @param Locale the Locale to set
     */
    public void setLocale(String Locale) {
        this.Locale = Locale;
    }

    /**
     * @return the Theme
     */
    public String getTheme() {
        return Theme;
    }

    /**
     * @param Theme the Theme to set
     */
    public void setTheme(String Theme) {
        this.Theme = Theme;
    }

    /**
     * @return the TabEffect
     */
    public String getTabEffect() {
        return TabEffect;
    }

    /**
     * @param TabEffect the TabEffect to set
     */
    public void setTabEffect(String TabEffect) {
        this.TabEffect = TabEffect;
    }
}
