/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientview;

import eu.nephron.helper.entityhelper;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import eu.nephron.schedule.webservices.ScheduleActAndTaskManager_Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.model.timeline.DefaultTimeLine;
import org.primefaces.extensions.model.timeline.DefaultTimelineEvent;
import org.primefaces.extensions.model.timeline.Timeline;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class CustomEventsController implements Serializable {

    private List<Timeline> timelines;
    private TimelineEvent selectedEvent;
    private String eventStyle = "box";
    private String axisPosition = "bottom";
    private boolean showNavigation = true;
    String previousid="NA";

    private java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> findAllScheduleActAndTaskManager() {
        ScheduleActAndTaskManager_Service service = new ScheduleActAndTaskManager_Service();
        eu.nephron.schedule.webservices.ScheduleActAndTaskManager port = service.getScheduleActAndTaskManagerPort();
        return port.findAllScheduleActAndTaskManager();
    }

    private void checkforrefresh()
    {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
            previousid=puserid;
        }else
        {
            if(previousid.equalsIgnoreCase(puserid))
            {}else
            {
                previousid=puserid;
                LoadData();
            }
        }
    }
    
    private void LoadData()
    {
        timelines = new ArrayList<Timeline>();
        Calendar cal = Calendar.getInstance();
        Date ddate = new Date();
        cal.setTime(ddate);
        Timeline timeline = new DefaultTimeLine("customEventTimeline", "Nephron Timeline");
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        
        entityhelper phelper = new entityhelper();
        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);

        java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> pfind = findAllScheduleActAndTaskManager();
        Long loginDoctorID = Long.parseLong(duserid);
        for (int i = 0; i < pfind.size(); i++) {
            if (loginDoctorID == pfind.get(i).getDoctorid()) {
                String MoreDesc = "";

                if (pfind.get(i).getItype() == 1) {
                    MoreDesc = "Appointment";
                }
                if (pfind.get(i).getItype() == 2) {
                    MoreDesc = "Medication Schedule";
                }

                entityhelper pHelper = new entityhelper();
                MoreDesc = MoreDesc + " " + pHelper.GetPatientName(pfind.get(i).getPatientid()) + " ";
                Date datefrom = new Date();
                Date dateto = new Date();
                datefrom.setTime(pfind.get(i).getDateap());
                dateto.setTime(pfind.get(i).getDateap() + (60 * 60 * 1000));
                ScheduleEvent eventtoadd = null;
                if (pfind.get(i).getItype() == 1) {
                    TimelineEvent timelineEvent = new DefaultTimelineEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom);
                    timelineEvent.setStyleClass("green");
                    timeline.addEvent(timelineEvent);


                }

                if (pfind.get(i).getItype() == 2) {
                    TimelineEvent timelineEvent = new DefaultTimelineEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom);
                    timelineEvent.setStyleClass("blue");
                    timeline.addEvent(timelineEvent);

                }
            }
        }

        entityhelper pEntityHelper = new entityhelper();
        List<AlertsModel> palerts = pEntityHelper.GetAlertsForIMEI2(PhoneIMEI, Long.parseLong(puserid));


        for (int i = 0; i < palerts.size(); i++) {
            String title = palerts.get(i).getMessage();
            Date ddated = new Date();
            ddated.setTime(Long.parseLong(palerts.get(i).getdDate()));
            TimelineEvent timelineEvent = new DefaultTimelineEvent(title, ddated);
            timelineEvent.setStyleClass("red");
            timeline.addEvent(timelineEvent);
        }

        timelines.add(timeline);
    }
    public CustomEventsController() {
        LoadData();
    }

    public void onEventSelect(SelectEvent event) {
        selectedEvent = (TimelineEvent) event.getObject();
    }

    public List<Timeline> getTimelines() {
        checkforrefresh();
        return timelines;
    }

    public TimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * @return the eventStyle
     */
    public String getEventStyle() {
        return eventStyle;
    }

    /**
     * @param eventStyle the eventStyle to set
     */
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    /**
     * @return the axisPosition
     */
    public String getAxisPosition() {
        return axisPosition;
    }

    /**
     * @param axisPosition the axisPosition to set
     */
    public void setAxisPosition(String axisPosition) {
        this.axisPosition = axisPosition;
    }

    /**
     * @return the showNavigation
     */
    public boolean isShowNavigation() {
        return showNavigation;
    }

    /**
     * @param showNavigation the showNavigation to set
     */
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }
}
