/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.weight;

import javax.faces.bean.SessionScoped;
import eu.nephron.DataModel.weight;
import eu.nephron.helper.entityhelper;
import eu.nephron.weightdatavalues.ws.WeightDataValuesWS_Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;


/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class weightdatabean {


private List<weight> sensors;
private List<weight> sensors2;
private Date dateFromM = new Date();
    private Date dateToM = new Date();
    /**
     * Creates a new instance of weightdatabean
     */
    public weightdatabean() {
         String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        
        sensors = new ArrayList<weight>();
        sensors2 = new ArrayList<weight>();
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> pList=findAll();
        for(int i=0;i<pList.size();i++) 
        {
            if(pList.get(i).getImei().equalsIgnoreCase(PhoneIMEI))
            {
                weight item=new weight();
                item.setBodyfat(pList.get(i).getBodyfat());
                item.setImei(PhoneIMEI);
                item.setWeight(pList.get(i).getWeight());
                item.setWtime(pList.get(i).getWtime());
                sensors.add(item);
                sensors2.add(item);
            }
            
        }
    }
    
    public void UpdateRecords(ActionEvent actionEvent) {
        sensors.clear();
        for (int i = 0; i < sensors2.size(); i++) {
            Date date = new Date();
            Long dtime = sensors2.get(i).getWtime();
            date.setTime(dtime);
            if (date.getTime() > dateFromM.getTime()) {
                if (date.getTime() < dateToM.getTime()) {
                    sensors.add(sensors2.get(i));
                }
            }
        }
    }
    
     private java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> findAll() {
        
        WeightDataValuesWS_Service service= new WeightDataValuesWS_Service();
        eu.nephron.weightdatavalues.ws.WeightDataValuesWS port = service.getWeightDataValuesWSPort();
        return port.findAll();
    }

    /**
     * @return the sensors
     */
    public List<weight> getSensors() {
        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<weight> sensors) {
        this.sensors = sensors;
    }

    private java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> findWAKDMeasurmentsManagerWithIMEI(java.lang.String imei) {
                WeightDataValuesWS_Service service= new WeightDataValuesWS_Service();
        eu.nephron.weightdatavalues.ws.WeightDataValuesWS port = service.getWeightDataValuesWSPort();
        return port.findWAKDMeasurmentsManagerWithIMEI(imei);
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }
}
