/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.weight;

import eu.nephron.DataModel.weight;
import eu.nephron.helper.entityhelper;
import eu.nephron.model.Sensors;
import eu.nephron.weightdatavalues.ws.WeightDataValuesWS_Service;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@RequestScoped
public class weightData {
    
    private List<weight> sensors;

    /**
     * Creates a new instance of weightData
     */
    public weightData() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        
        
        entityhelper phelper = new entityhelper();
        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);
        System.err.println("Phone IMEI for charts : "+PhoneIMEI);
        System.err.println("User ID " + puserid + " Phone IMEI " + PhoneIMEI);
        java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> pList=findAll();
        for(int i=0;i<pList.size();i++) 
        {
            if(pList.get(i).getImei().equalsIgnoreCase(PhoneIMEI))
            {
                weight item=new weight();
                item.setBodyfat(pList.get(i).getBodyfat());
                item.setImei(PhoneIMEI);
                item.setWeight(pList.get(i).getWeight());
                item.setWtime(pList.get(i).getWtime());
                sensors.add(item);
            }
            
        }
        
    }

    private java.util.List<eu.nephron.weightdatavalues.ws.Weightdatavalues> findAll() {
        
        WeightDataValuesWS_Service service= new WeightDataValuesWS_Service();
        eu.nephron.weightdatavalues.ws.WeightDataValuesWS port = service.getWeightDataValuesWSPort();
        return port.findAll();
    }

    /**
     * @return the sensors
     */
    public List<weight> getSensors() {
        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<weight> sensors) {
        this.sensors = sensors;
    }
}
