/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.application.bean;

import eu.nephron.helper.entityhelper;
import eu.nephron.logic.CompleteTreeModel;
import eu.nephron.logic.MedicalDevicesModel;
import eu.nephron.logic.NephronLogicWS_Service;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Patient;
import eu.nephron.patientsecurityws.SecurePatientConnection_Service;
import eu.nephron.schedule.webservices.ScheduleActAndTaskManager_Service;
import eu.nephron.secure.ws.WAKDAlertsManager_Service;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
import eu.nephron.soap.ws.AddressManager_Service;
import eu.nephron.soap.ws.ContactInfoManager_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;
import org.primefaces.extensions.model.timeline.DefaultTimeLine;
import org.primefaces.extensions.model.timeline.DefaultTimelineEvent;
import org.primefaces.extensions.model.timeline.Timeline;
import org.primefaces.extensions.model.timeline.TimelineEvent;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class BasicApplicationBean {

    //Variables for Storage
    java.util.List<eu.nephron.secure.ws.WakdMeasurments> pMeasurments = new ArrayList();
    java.util.List<eu.nephron.secure.ws.WakdAlerts> pAlerts = new ArrayList();
    java.util.List<eu.nephron.patientsecurityws.Patientconnections> pfind = new ArrayList();
    CompleteTreeModel pTreeModel = new CompleteTreeModel();
    private Long DoctorIDFromWeb = new Long(3);
    String PKIKey = "WS";
    //Bean Variables for Data Tables
    private List<Patient> filteredPatient;
    private List<Patient> Patients;
    private Patient selectedPatient;
    private Patient[] selectedPatients;
    private String useridforedit;
    private List<DSSResults> sumups;
    // bean variables for personal details view of patient
    private int ilevel;
    private String fathername;
    private String firstname;
    private String lastname;
    private int imstatus;
    private String middlename;
    private String title;
    private String companyemail;
    private String companymobile;
    private String companyphone;
    private String email;
    private String mobile;
    private String phone;
    private String administrativearea;
    private String city;
    private String code;
    private String floor;
    private String postcode;
    private String street;
    private String streetno;
    private String patienteducationlevel;
    private String patientmaritalstatus;
    //Time Line vars
    private List<Timeline> timelines;
    private TimelineEvent selectedEvent;
    private String eventStyle = "box";
    private String axisPosition = "bottom";
    private boolean showNavigation = true;
    //Vars for alerts
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private List<DSSResults> sumups2;
    private List<DSSResults> sumupsoriginal;

    /**
     * Creates a new instance of BasicApplicationBean basicApplicationBean
     */
    public BasicApplicationBean() {
        //DUserID
        pTreeModel = getCompleteStructure(DoctorIDFromWeb, PKIKey);
        pMeasurments = returnAllMeasurmentsForLastDay();
        pAlerts = returnAllAlertsForLast7Days();
        pfind = findAllSecurePatientConnection();
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        DoctorIDFromWeb = Long.parseLong(duserid);
        Patients = new ArrayList<Patient>();
        sumups = new ArrayList<DSSResults>();
        sumups2 = new ArrayList<DSSResults>();
        sumupsoriginal = new ArrayList<DSSResults>();
        LoadPatients();
        LoadAlerts();
    }

    public String LoadAlertsPage() {
        String imei = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        sumups2.clear();
        sumupsoriginal.clear();
        List<AlertsModel> palerts = new ArrayList<AlertsModel>();
        java.util.List<eu.nephron.secure.ws.WakdAlerts> pWAlerts = findWAKDAlertsManagerWithIMEI(imei);
        for (int i = 0; i < pWAlerts.size(); i++) {
            AlertsModel pitem = new AlertsModel();
            pitem.setID(pWAlerts.get(i).getId());
            pitem.setPatient(GetPatientName(Long.parseLong(puserid)));
            pitem.setMessage(pWAlerts.get(i).getMsgDoctor());
            pitem.setCheck("Alert");
            Date pp;
            pp = new Date();
            pp.setTime(pWAlerts.get(i).getDdate());
            pitem.setdDate(pp.toString());
            pitem.setDdatel(pWAlerts.get(i).getDdate());
            palerts.add(pitem);
        }
        for (int i = 0; i < palerts.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(palerts.get(i).getID());
            pNew.setRulename(palerts.get(i).getCheck());
            pNew.setRuleresults(palerts.get(i).getPatient());
            pNew.setRuleresults1(palerts.get(i).getMessage());
            pNew.setRuleresults2(palerts.get(i).getdDate());
            pNew.setRuleresults3(String.valueOf(palerts.get(i).getDdatel()));
            sumups2.add(pNew);
            sumupsoriginal.add(pNew);
        }
        return "Alerts?faces-redirect=true";
    }
    
    /////////////////////////////////////////////////////////////////////////////////
    //Alerts

    public void UpdateRecords(ActionEvent actionEvent) {
        sumups2.clear();
        for (int i = 0; i < sumupsoriginal.size(); i++) {
            Date date = new Date();
            Long dtime = Long.parseLong(sumupsoriginal.get(i).getRuleresults3());
            date.setTime(dtime);
            if (date.getTime() > dateFromM.getTime()) {
                if (date.getTime() < dateToM.getTime()) {
                    sumups2.add(sumupsoriginal.get(i));
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////
    // Load Data

    private void LoadPatients() {
        Patients.clear();
        for (int i = 0; i < pTreeModel.getPPatients().size(); i++) {
            Patient pentry = new Patient();
            pentry.setId(pTreeModel.getPPatients().get(i).getPatientID());
            Patient pFindPatient = findPatientDetails(pTreeModel.getPPatients().get(i).getPatientID());
            pentry.setFirstName(pFindPatient.getFirstName());
            pentry.setLastName(pFindPatient.getLastName());
            pentry.setFatherName(pFindPatient.getFatherName());
            Patients.add(pentry);
        }
    }

    private void LoadAlerts() {
        List<AlertsModel> palerts = new ArrayList<AlertsModel>();
        sumups.clear();
        for (int i = 0; i < pTreeModel.getPPatients().size(); i++) {
            for (int i2 = 0; i2 < pTreeModel.getPPatients().get(i).getPMedicalDevices().size(); i2++) {
                MedicalDevicesModel pDevice = pTreeModel.getPPatients().get(i).getPMedicalDevices().get(i2);
                if (pDevice.getType().equalsIgnoreCase("SP")) {
                    for (int idd = 0; idd < pAlerts.size(); idd++) {
                        if (pAlerts.get(idd).getIMEI().equalsIgnoreCase(pDevice.getIMEI())) {
                            DSSResults pentry = new DSSResults();
                            AlertsModel pitem = new AlertsModel();
                            pitem.setID(pAlerts.get(idd).getId());
                            pitem.setPatient(GetPatientName(pTreeModel.getPPatients().get(i).getPatientID()));
                            pitem.setMessage(pAlerts.get(idd).getMsgDoctor());
                            pitem.setCheck("Alert");
                            Date pp;
                            pp = new Date();
                            pp.setTime(pAlerts.get(idd).getDdate());
                            pitem.setdDate(pp.toString());
                            palerts.add(pitem);
                        }
                    }
                }
            }
        }

        for (int i = 0; i < palerts.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(palerts.get(i).getID());
            pNew.setRulename(palerts.get(i).getCheck());
            pNew.setRuleresults(palerts.get(i).getPatient());
            pNew.setRuleresults1(palerts.get(i).getMessage());
            pNew.setRuleresults2(palerts.get(i).getdDate());
            sumups.add(pNew);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // Get patient details
    private String GetPatientName(Long pID) {
        String patientname = "";
        for (int i = 0; i < Patients.size(); i++) {
            if (Patients.get(i).getId() == pID) {
                patientname = Patients.get(i).getFirstName() + " " + Patients.get(i).getLastName();
            }
        }
        return patientname;
    }
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    //Manage edit for patient, load patient information dashboard
    public String edituser() {
        long ifind = Long.parseLong(getUseridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", getUseridforedit());
        LoadPatientPersonalInfodetails(ifind);
        LoadTimeLine(ifind);
        return "DashBoard_Patient?faces-redirect=true";
    }

    /**
     * @return the useridforedit
     */
    public String getUseridforedit() {
        return useridforedit;
    }

    /**
     * @param useridforedit the useridforedit to set
     */
    public void setUseridforedit(String useridforedit) {
        this.useridforedit = useridforedit;
    }

    private void LoadPatientPersonalInfodetails(Long lfind) {
        System.out.println("-----> Load Info for patient : s1");
        Long pid = null;
        Long cid = null;
        Long aid = null;
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getIdpatient() == lfind) {
                pid = pfind.get(i).getIdpersonnal();
                cid = pfind.get(i).getIdcontact();
                aid = pfind.get(i).getIdaddress();
            }
        }
        System.out.println("-----> Load Info for patient : s2");
        List<eu.nephron.soap.ws.PersonalInfo> ppInfo = findPersonalInfoByID(pid);
        System.out.println("-----> Load Info for patient : s3");

        ilevel = ppInfo.get(0).getEducationalLevel();

        if (ilevel == 1) {
            patienteducationlevel = "ELEMENTARY";
        }
        if (ilevel == 2) {
            patienteducationlevel = "HIGH_SCOOL";
        }
        if (ilevel == 3) {
            patienteducationlevel = "COLLEGE";
        }
        if (ilevel == 4) {
            patienteducationlevel = "UNIVERSITY";
        }
        if (ilevel == 5) {
            patienteducationlevel = "MASTER";
        }
        if (ilevel == 6) {
            patienteducationlevel = "PHD";
        }
        if (ilevel == 7) {
            patienteducationlevel = "UNDEFINED";
        }

        fathername = ppInfo.get(0).getFatherName();
        firstname = ppInfo.get(0).getFirstName();
        lastname = ppInfo.get(0).getLastName();
        imstatus = ppInfo.get(0).getMaritalStatus();

        if (imstatus == 1) {
            patientmaritalstatus = "SINGLE";
        }
        if (imstatus == 2) {
            patientmaritalstatus = "MARRIED";
        }
        if (imstatus == 3) {
            patientmaritalstatus = "DIVORCED";
        }
        if (imstatus == 4) {
            patientmaritalstatus = "UNDEFINED";
        }

        middlename = ppInfo.get(0).getMiddleName();
        title = ppInfo.get(0).getTitle();

        List<eu.nephron.soap.ws.ContactInfo> pcInfo = findContactInfoByID(cid);
        companyemail = pcInfo.get(0).getCompanyEmail();
        companymobile = pcInfo.get(0).getCompanyMobile();
        companyphone = pcInfo.get(0).getCompanyPhone();
        email = pcInfo.get(0).getEmail();
        mobile = pcInfo.get(0).getMobile();
        phone = pcInfo.get(0).getPhone();

        List<eu.nephron.soap.ws.Address> paInfo = findAddressByID(aid);

        administrativearea = paInfo.get(0).getAdministrativeArea();
        city = paInfo.get(0).getCity();
        code = paInfo.get(0).getCode();
        floor = paInfo.get(0).getFloor();
        postcode = paInfo.get(0).getPostCode();
        street = paInfo.get(0).getStreet();
        streetno = paInfo.get(0).getStreetNo();
    }
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    // Private Helper Functions
    private Patient findPatientDetails(Long pID) {
        Patient ppa = new Patient();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getIdpatient() == pID) {
                PersonalInfoManager_Service service2 = new PersonalInfoManager_Service();
                eu.nephron.soap.ws.PersonalInfoManager port2 = service2.getPersonalInfoManagerPort();
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pp = port2.findPersonalInfoByID(pfind.get(i).getIdpersonnal());
                ppa.setFirstName(pp.get(0).getFirstName());
                ppa.setLastName(pp.get(0).getLastName());
                ppa.setFatherName(pp.get(0).getFatherName());
            }
        }
        return ppa;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /**
     * @return the DoctorIDFromWeb
     */
    public Long getDoctorIDFromWeb() {
        return DoctorIDFromWeb;
    }

    /**
     * @param DoctorIDFromWeb the DoctorIDFromWeb to set
     */
    public void setDoctorIDFromWeb(Long DoctorIDFromWeb) {
        this.DoctorIDFromWeb = DoctorIDFromWeb;
    }

    /**
     * @return the filteredPatient
     */
    public List<Patient> getFilteredPatient() {
        return filteredPatient;
    }

    /**
     * @param filteredPatient the filteredPatient to set
     */
    public void setFilteredPatient(List<Patient> filteredPatient) {
        this.filteredPatient = filteredPatient;
    }

    /**
     * @return the Patients
     */
    public List<Patient> getPatients() {
        return Patients;
    }

    /**
     * @param Patients the Patients to set
     */
    public void setPatients(List<Patient> Patients) {
        this.Patients = Patients;
    }

    /**
     * @return the selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * @param selectedPatient the selectedPatient to set
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * @return the selectedPatients
     */
    public Patient[] getSelectedPatients() {
        return selectedPatients;
    }

    /**
     * @param selectedPatients the selectedPatients to set
     */
    public void setSelectedPatients(Patient[] selectedPatients) {
        this.selectedPatients = selectedPatients;
    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the ilevel
     */
    public int getIlevel() {
        return ilevel;
    }

    /**
     * @param ilevel the ilevel to set
     */
    public void setIlevel(int ilevel) {
        this.ilevel = ilevel;
    }

    /**
     * @return the fathername
     */
    public String getFathername() {
        return fathername;
    }

    /**
     * @param fathername the fathername to set
     */
    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the imstatus
     */
    public int getImstatus() {
        return imstatus;
    }

    /**
     * @param imstatus the imstatus to set
     */
    public void setImstatus(int imstatus) {
        this.imstatus = imstatus;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the companyemail
     */
    public String getCompanyemail() {
        return companyemail;
    }

    /**
     * @param companyemail the companyemail to set
     */
    public void setCompanyemail(String companyemail) {
        this.companyemail = companyemail;
    }

    /**
     * @return the companymobile
     */
    public String getCompanymobile() {
        return companymobile;
    }

    /**
     * @param companymobile the companymobile to set
     */
    public void setCompanymobile(String companymobile) {
        this.companymobile = companymobile;
    }

    /**
     * @return the companyphone
     */
    public String getCompanyphone() {
        return companyphone;
    }

    /**
     * @param companyphone the companyphone to set
     */
    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the administrativearea
     */
    public String getAdministrativearea() {
        return administrativearea;
    }

    /**
     * @param administrativearea the administrativearea to set
     */
    public void setAdministrativearea(String administrativearea) {
        this.administrativearea = administrativearea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetno
     */
    public String getStreetno() {
        return streetno;
    }

    /**
     * @param streetno the streetno to set
     */
    public void setStreetno(String streetno) {
        this.streetno = streetno;
    }

    /**
     * @return the patienteducationlevel
     */
    public String getPatienteducationlevel() {
        return patienteducationlevel;
    }

    /**
     * @param patienteducationlevel the patienteducationlevel to set
     */
    public void setPatienteducationlevel(String patienteducationlevel) {
        this.patienteducationlevel = patienteducationlevel;
    }

    /**
     * @return the patientmaritalstatus
     */
    public String getPatientmaritalstatus() {
        return patientmaritalstatus;
    }

    /**
     * @param patientmaritalstatus the patientmaritalstatus to set
     */
    public void setPatientmaritalstatus(String patientmaritalstatus) {
        this.patientmaritalstatus = patientmaritalstatus;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    private String GetPatientsPhoneIMEI(Long patientID) {
        String pret = null;
        for (int i = 0; i < this.pTreeModel.getPPatients().size(); i++) {
            if (pTreeModel.getPPatients().get(i).getPatientID() == patientID) {
                for (int i1 = 0; i1 < pTreeModel.getPPatients().get(i).getPMedicalDevices().size(); i1++) {
                    if (pTreeModel.getPPatients().get(i).getPMedicalDevices().get(i1).getType().equalsIgnoreCase("SP")) {
                        pret = pTreeModel.getPPatients().get(i).getPMedicalDevices().get(i1).getIMEI();
                    }
                }
            }
        }
        return pret;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Timeline model functions

    private void LoadTimeLine(Long PatientID) {
        setTimelines(new ArrayList<Timeline>());
        Calendar cal = Calendar.getInstance();
        Date ddate = new Date();
        cal.setTime(ddate);
        Timeline timeline = new DefaultTimeLine("customEventTimeline", "Nephron Timeline");
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");



        String PhoneIMEI = GetPatientsPhoneIMEI(Long.parseLong(puserid));
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PHONEIMEI", PhoneIMEI);

        java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> pfind = findAllScheduleActAndTaskManager();
        Long loginDoctorID = Long.parseLong(duserid);
        for (int i = 0; i < pfind.size(); i++) {
            if (loginDoctorID == pfind.get(i).getDoctorid()) {
                String MoreDesc = "";

                if (pfind.get(i).getItype() == 1) {
                    MoreDesc = "Appointment";
                }
                if (pfind.get(i).getItype() == 2) {
                    MoreDesc = "Medication Schedule";
                }

                //      entityhelper pHelper = new entityhelper();
                MoreDesc = MoreDesc + " " + this.GetPatientName(pfind.get(i).getPatientid()) + " ";
                Date datefrom = new Date();
                Date dateto = new Date();
                datefrom.setTime(pfind.get(i).getDateap());
                dateto.setTime(pfind.get(i).getDateap() + (60 * 60 * 1000));
                ScheduleEvent eventtoadd = null;
                if (pfind.get(i).getItype() == 1) {
                    TimelineEvent timelineEvent = new DefaultTimelineEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom);
                    timelineEvent.setStyleClass("green");
                    timeline.addEvent(timelineEvent);


                }

                if (pfind.get(i).getItype() == 2) {
                    TimelineEvent timelineEvent = new DefaultTimelineEvent(MoreDesc + pfind.get(i).getSdesc(), datefrom);
                    timelineEvent.setStyleClass("blue");
                    timeline.addEvent(timelineEvent);

                }
            }
        }

        entityhelper pEntityHelper = new entityhelper();
        List<AlertsModel> palerts = pEntityHelper.GetAlertsForIMEI2(PhoneIMEI, Long.parseLong(puserid));


        for (int i = 0; i < palerts.size(); i++) {
            String title = palerts.get(i).getMessage();
            Date ddated = new Date();
            ddated.setTime(Long.parseLong(palerts.get(i).getdDate()));
            TimelineEvent timelineEvent = new DefaultTimelineEvent(title, ddated);
            timelineEvent.setStyleClass("red");
            timeline.addEvent(timelineEvent);
        }

        getTimelines().add(timeline);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Web Service Calls
    private java.util.List<eu.nephron.soap.ws.PersonalInfo> findPersonalInfoByID(java.lang.Long arg0) {
        PersonalInfoManager_Service service = new PersonalInfoManager_Service();
        eu.nephron.soap.ws.PersonalInfoManager port = service.getPersonalInfoManagerPort();
        return port.findPersonalInfoByID(arg0);
    }

    private java.util.List<eu.nephron.soap.ws.ContactInfo> findContactInfoByID(java.lang.Long arg0) {
        ContactInfoManager_Service service = new ContactInfoManager_Service();
        eu.nephron.soap.ws.ContactInfoManager port = service.getContactInfoManagerPort();
        return port.findContactInfoByID(arg0);
    }

    private java.util.List<eu.nephron.soap.ws.Address> findAddressByID(java.lang.Long arg0) {
        AddressManager_Service service = new AddressManager_Service();
        eu.nephron.soap.ws.AddressManager port = service.getAddressManagerPort();
        return port.findAddressByID(arg0);
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> returnAllMeasurmentsForLastDay() {
        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
        return port.returnAllMeasurmentsForLastDay();
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> returnAllMeasurmentsForLastDayAndType(java.lang.Long arg0) {
        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
        return port.returnAllMeasurmentsForLastDayAndType(arg0);
    }

    private java.util.List<eu.nephron.secure.ws.WakdAlerts> returnAllAlertsForLast7Days() {
        WAKDAlertsManager_Service service_1 = new WAKDAlertsManager_Service();
        eu.nephron.secure.ws.WAKDAlertsManager port = service_1.getWAKDAlertsManagerPort();
        return port.returnAllAlertsForLast7Days();
    }

    private CompleteTreeModel getCompleteStructure(java.lang.Long arg0, java.lang.String arg1) {
        NephronLogicWS_Service service_2 = new NephronLogicWS_Service();
        eu.nephron.logic.NephronLogicWS port = service_2.getNephronLogicWSPort();
        return port.getCompleteStructure(arg0, arg1);
    }

    private java.util.List<eu.nephron.patientsecurityws.Patientconnections> findAllSecurePatientConnection() {
        SecurePatientConnection_Service service = new SecurePatientConnection_Service();
        eu.nephron.patientsecurityws.SecurePatientConnection port = service.getSecurePatientConnectionPort();
        return port.findAllSecurePatientConnection();
    }

    private java.util.List<eu.nephron.schedule.webservices.ScheduleActAndTask> findAllScheduleActAndTaskManager() {
        ScheduleActAndTaskManager_Service service = new ScheduleActAndTaskManager_Service();
        eu.nephron.schedule.webservices.ScheduleActAndTaskManager port = service.getScheduleActAndTaskManagerPort();
        return port.findAllScheduleActAndTaskManager();
    }

    /**
     * @return the timelines
     */
    public List<Timeline> getTimelines() {
        return timelines;
    }

    /**
     * @param timelines the timelines to set
     */
    public void setTimelines(List<Timeline> timelines) {
        this.timelines = timelines;
    }

    /**
     * @return the selectedEvent
     */
    public TimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * @param selectedEvent the selectedEvent to set
     */
    public void setSelectedEvent(TimelineEvent selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    /**
     * @return the eventStyle
     */
    public String getEventStyle() {
        return eventStyle;
    }

    /**
     * @param eventStyle the eventStyle to set
     */
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    /**
     * @return the axisPosition
     */
    public String getAxisPosition() {
        return axisPosition;
    }

    /**
     * @param axisPosition the axisPosition to set
     */
    public void setAxisPosition(String axisPosition) {
        this.axisPosition = axisPosition;
    }

    /**
     * @return the showNavigation
     */
    public boolean isShowNavigation() {
        return showNavigation;
    }

    /**
     * @param showNavigation the showNavigation to set
     */
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the sumups2
     */
    public List<DSSResults> getSumups2() {
        return sumups2;
    }

    /**
     * @param sumups2 the sumups2 to set
     */
    public void setSumups2(List<DSSResults> sumups2) {
        this.sumups2 = sumups2;
    }

    private java.util.List<eu.nephron.secure.ws.WakdAlerts> findWAKDAlertsManagerWithIMEI(java.lang.String imei) {
        WAKDAlertsManager_Service service = new WAKDAlertsManager_Service();
        eu.nephron.secure.ws.WAKDAlertsManager port = service.getWAKDAlertsManagerPort();
        return port.findWAKDAlertsManagerWithIMEI(imei);
    }
}
