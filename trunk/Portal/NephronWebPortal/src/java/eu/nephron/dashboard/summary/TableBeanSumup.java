/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dashboard.summary;

import eu.nephron.dss.server.ws.DSSServerRuleResultsWS_Service;
import eu.nephron.dss.ws.DSSWSManager_Service;
import eu.nephron.helper.entityhelper;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Patient;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.WebServiceRef;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.Dispatch;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Service;
import java.io.StringReader;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class TableBeanSumup {
    

    private List<DSSResults> sumups;
    private DSSResults selectedsumup;
    private DSSResults[] selectedsumups;

    /**
     * Creates a new instance of TableBeanSumup
     */
    public TableBeanSumup() {
        sumups = new ArrayList<DSSResults>();
        populatesumups(sumups);
    }

    private void populatesumups(List<DSSResults> list) {

//        java.util.List<eu.nephron.dss.server.ws.DssServerRuleResults> pResults = dssServerRuleResultsfindAll();
//        for (int i = 0; i < pResults.size(); i++) {
//            DSSResults pNew = new DSSResults();
//            pNew.setId(pResults.get(i).getId());
//            pNew.setRulename(pResults.get(i).getRulename());
//            pNew.setRuleresults(pResults.get(i).getRuleresults());
//            pNew.setRuleresults1(pResults.get(i).getRuleresults1());
//            pNew.setRuleresults2(pResults.get(i).getRuleresults2());
//            list.add(pNew);
//        }
        entityhelper pEntityHelper = new entityhelper();
        List<AlertsModel> palerts = pEntityHelper.GetAlertsForDoctor(new Long(3));
        for (int i = 0; i < palerts.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(palerts.get(i).getID());
            pNew.setRulename(palerts.get(i).getCheck());
            pNew.setRuleresults(palerts.get(i).getPatient());
            pNew.setRuleresults1(palerts.get(i).getMessage());
            pNew.setRuleresults2(palerts.get(i).getdDate());
            list.add(pNew);
        }
    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the selectedsumup
     */
    public DSSResults getSelectedsumup() {
        return selectedsumup;
    }

    /**
     * @param selectedsumup the selectedsumup to set
     */
    public void setSelectedsumup(DSSResults selectedsumup) {
        this.selectedsumup = selectedsumup;
    }

    /**
     * @return the selectedsumups
     */
    public DSSResults[] getSelectedsumups() {
        return selectedsumups;
    }

    /**
     * @param selectedsumups the selectedsumups to set
     */
    public void setSelectedsumups(DSSResults[] selectedsumups) {
        this.selectedsumups = selectedsumups;
    }

    

    private java.util.List<eu.nephron.dss.server.ws.DssServerRuleResults> dssServerRuleResultsfindAll() {
        
//     DSSServerRuleResultsWS_Service service=new DSSServerRuleResultsWS_Service();
  //      eu.nephron.dss.server.ws.DSSServerRuleResultsWS port = service.getDSSServerRuleResultsWSPort();
    //    return port.dssServerRuleResultsfindAll();
        return null;
    }
}
