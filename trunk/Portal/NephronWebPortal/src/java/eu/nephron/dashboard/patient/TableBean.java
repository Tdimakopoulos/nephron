package eu.nephron.dashboard.patient;

import eu.nephron.model.Patient;
import eu.nephron.patientsecurityws.SecurePatientConnection;
import eu.nephron.patientsecurityws.SecurePatientConnection_Service;
import eu.nephron.soap.ws.PersonalInfoManager_Service;
import eu.nephron.soap.ws.commandmanager.ActServiceManager_Service;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

@ManagedBean
@SessionScoped
public class TableBean implements Serializable {

    private List<Patient> filteredPatient;
    private List<Patient> Patients;
    private Patient selectedPatient;
    private Patient[] selectedPatients;
    private String useridforedit;

    public TableBean() {
        Patients = new ArrayList<Patient>();
        populatePatient(Patients);
    }

    private void populatePatient(List<Patient> list) {

        java.util.List<Object> pFind = findConnections(new Long(3), "");

        for (int i = 0; i < pFind.size(); i++) {

            Patient pNew2 = new Patient();
            Patient pdd = findAll((Long) pFind.get(i));
            pNew2.setId((Long) pFind.get(i));
            pNew2.setFatherName(pdd.getFatherName());
            pNew2.setFirstName(pdd.getFirstName());
            pNew2.setLastName(pdd.getLastName());
            list.add(pNew2);
        }
    }

    /**
     * @return the filteredPatient
     */
    public List<Patient> getFilteredPatient() {
        return filteredPatient;
    }

    /**
     * @param filteredPatient the filteredPatient to set
     */
    public void setFilteredPatient(List<Patient> filteredPatient) {
        this.filteredPatient = filteredPatient;
    }

    /**
     * @return the Patients
     */
    public List<Patient> getPatients() {
        return Patients;
    }

    /**
     * @param Patients the Patients to set
     */
    public void setPatients(List<Patient> Patients) {
        this.Patients = Patients;
    }

    /**
     * @return the selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * @param selectedPatient the selectedPatient to set
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * @return the selectedPatients
     */
    public Patient[] getSelectedPatients() {
        return selectedPatients;
    }

    /**
     * @param selectedPatients the selectedPatients to set
     */
    public void setSelectedPatients(Patient[] selectedPatients) {
        this.selectedPatients = selectedPatients;
    }

    private Patient findAll(Long pID) {
        Patient ppa = new Patient();

        java.util.List<eu.nephron.patientsecurityws.Patientconnections> pfind = findAll_1();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getIdpatient() == pID) {
                PersonalInfoManager_Service service2 = new PersonalInfoManager_Service();
                eu.nephron.soap.ws.PersonalInfoManager port2 = service2.getPersonalInfoManagerPort();
                java.util.List<eu.nephron.soap.ws.PersonalInfo> pp = port2.findPersonalInfoByID(pfind.get(i).getIdpersonnal());
                ppa.setFirstName(pp.get(0).getFirstName());
                ppa.setLastName(pp.get(0).getLastName());
                ppa.setFatherName(pp.get(0).getFatherName());
            }
        }
        return ppa;
    }

    public String edituser() {
        long ifind = Long.parseLong(getUseridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", getUseridforedit());
        return "DashBoard_Patient?faces-redirect=true";
    }

    /**
     * @return the useridforedit
     */
    public String getUseridforedit() {
        return useridforedit;
    }

    /**
     * @param useridforedit the useridforedit to set
     */
    public void setUseridforedit(String useridforedit) {
        this.useridforedit = useridforedit;
    }

    private java.util.List<eu.nephron.patientsecurityws.Patientconnections> findAll_1() {
        SecurePatientConnection_Service service = new SecurePatientConnection_Service();
        eu.nephron.patientsecurityws.SecurePatientConnection port = service.getSecurePatientConnectionPort();
        return port.findAllSecurePatientConnection();
    }

    private java.util.List<java.lang.Object> findConnections(java.lang.Long arg0, java.lang.String arg1) {
        System.out.print("Calling WS to find connections");
        ActServiceManager_Service service = new ActServiceManager_Service();
        eu.nephron.soap.ws.commandmanager.ActServiceManager port = service.getActServiceManagerPort();
        return port.findConnections(arg0, arg1);
    }
}
