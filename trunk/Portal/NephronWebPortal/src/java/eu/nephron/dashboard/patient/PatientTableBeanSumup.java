/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dashboard.patient;

import eu.nephron.dss.server.ws.DSSServerRuleResultsWS_Service;
import eu.nephron.helper.entityhelper;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class PatientTableBeanSumup {
    

    private List<DSSResults> sumups;
    private DSSResults selectedsumup;
    private DSSResults[] selectedsumups;
    private String patientname;
    String previousid="NA";
    
    /**
     * Creates a new instance of TableBeanSumup
     */
    public PatientTableBeanSumup() {
        sumups = new ArrayList<DSSResults>();
        populatesumups(sumups);
    }

    private void checkforrefresh()
    {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
            previousid=puserid;
        }else
        {
            if(previousid.equalsIgnoreCase(puserid))
            {}else
            {
                previousid=puserid;
                sumups = new ArrayList<DSSResults>();
                populatesumups(sumups);
            }
        }
    }
    private void populatesumups(List<DSSResults> list) {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        if(previousid.equalsIgnoreCase("NA"))
        {
        previousid=puserid;
        }
        entityhelper phelper = new entityhelper();
        Long PhoneID = phelper.GetPatientsPhoneID(Long.parseLong(puserid));
        patientname=phelper.GetPatientName(Long.parseLong(puserid));
        String PhoneIMEI = phelper.GetPatientsPhoneIMEI(PhoneID);

        //java.util.List<eu.nephron.dss.ws.DssRuleResults> pResults = findAll();
         java.util.List<eu.nephron.dss.server.ws.DssServerRuleResults> pResults = dssServerRuleResultsfindAll();
        for (int i = 0; i < pResults.size(); i++) {
            if (pResults.get(i).getRuleresults3() == null) {
            } else {
                if (pResults.get(i).getRuleresults3().equalsIgnoreCase(PhoneIMEI)) {
                    DSSResults pNew = new DSSResults();
                    pNew.setId(pResults.get(i).getId());
                    pNew.setRulename(pResults.get(i).getRulename());
                    pNew.setRuleresults(pResults.get(i).getRuleresults());
                    pNew.setRuleresults1(pResults.get(i).getRuleresults1());
                    pNew.setRuleresults2(pResults.get(i).getRuleresults2());
                    list.add(pNew);
                }
            }
        }
        entityhelper pEntityHelper = new entityhelper();
        List<AlertsModel> palerts = pEntityHelper.GetAlertsForIMEI(PhoneIMEI, Long.parseLong(puserid));
        for (int i = 0; i < palerts.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(palerts.get(i).getID());
            pNew.setRulename(palerts.get(i).getCheck());
            pNew.setRuleresults(palerts.get(i).getPatient());
            pNew.setRuleresults1(palerts.get(i).getMessage());
            pNew.setRuleresults2(palerts.get(i).getdDate());
            list.add(pNew);
        }
    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        checkforrefresh();
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the selectedsumup
     */
    public DSSResults getSelectedsumup() {
        checkforrefresh();
        return selectedsumup;
    }

    /**
     * @param selectedsumup the selectedsumup to set
     */
    public void setSelectedsumup(DSSResults selectedsumup) {
        this.selectedsumup = selectedsumup;
    }

    /**
     * @return the selectedsumups
     */
    public DSSResults[] getSelectedsumups() {
        checkforrefresh();
        return selectedsumups;
    }

    /**
     * @param selectedsumups the selectedsumups to set
     */
    public void setSelectedsumups(DSSResults[] selectedsumups) {
        this.selectedsumups = selectedsumups;
    }

  

    private java.util.List<eu.nephron.dss.server.ws.DssServerRuleResults> dssServerRuleResultsfindAll() {
        DSSServerRuleResultsWS_Service service=new DSSServerRuleResultsWS_Service();
        eu.nephron.dss.server.ws.DSSServerRuleResultsWS port = service.getDSSServerRuleResultsWSPort();
        return port.dssServerRuleResultsfindAll();
    }

    /**
     * @return the patientname
     */
    public String getPatientname() {
        checkforrefresh();
        return patientname;
    }

    /**
     * @param patientname the patientname to set
     */
    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }
}
