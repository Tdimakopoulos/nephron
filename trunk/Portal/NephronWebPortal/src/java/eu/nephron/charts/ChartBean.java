/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.charts;

import eu.nephron.mestypes.mestypes;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Sensors;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class ChartBean implements Serializable {

    private CartesianChartModel linearModel;
    private CartesianChartModel linearModelsodium;
    private CartesianChartModel linearModelpotasium;
    private CartesianChartModel linearModelurea;
    private CartesianChartModel linearModelph;
    private List<Sensors> sensors;
    private List<Sensors> sensorsOriginal;
    private CartesianChartModel categoryModel;
    String previousid = "NA";
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private String number;

    public void UpdateRecords(ActionEvent actionEvent) {
        sensors.clear();

        mestypes ptypes = new mestypes();
        for (int i = 0; i < sensorsOriginal.size(); i++) {
            String typecomp = String.valueOf(ptypes.GetType(number));
            String typecomp1 = sensorsOriginal.get(i).getType();
            Date date = sensorsOriginal.get(i).getTime();
            if (typecomp1.equalsIgnoreCase(typecomp)) {
                if (date.getTime() > dateFromM.getTime()) {
                    if (date.getTime() < dateToM.getTime()) {
                        sensors.add(sensorsOriginal.get(i));
                    }
                }
            } else {
            }
        }

    }

    public ChartBean() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        if (previousid.equalsIgnoreCase("NA")) {
            previousid = puserid;
        }
        sensors = new ArrayList<Sensors>();
        sensorsOriginal = new ArrayList<Sensors>();
        createLinearModel();
        createCategoryModel();
    }

    public CartesianChartModel getLinearModel() {
        return linearModel;
    }

    public CartesianChartModel getCategoryModel() {
        return categoryModel;
    }

    public void updatereadings() {
        System.err.println("Updating Readings Called");
        createLinearModel();
        createCategoryModel();
    }

    private void createLinearModel() {

        linearModel = new CartesianChartModel();
        linearModelsodium = new CartesianChartModel();
        linearModelpotasium = new CartesianChartModel();
        linearModelurea = new CartesianChartModel();
        linearModelph = new CartesianChartModel();

        categoryModel = new CartesianChartModel();
        ChartSeries psodium = new ChartSeries();
        ChartSeries ppotasium = new ChartSeries();
        ChartSeries purea = new ChartSeries();
        ChartSeries pph = new ChartSeries();

        psodium.setLabel("Sodium NA+");
        ppotasium.setLabel("Potasium K+");
        purea.setLabel("Urea");
        pph.setLabel("PH");

        LineChartSeries sodium = new LineChartSeries();
        LineChartSeries potasium = new LineChartSeries();
        LineChartSeries urea = new LineChartSeries();
        LineChartSeries ph = new LineChartSeries();
        sodium.setLabel("Sodium NA+");
        potasium.setLabel("Potasium K+");
        urea.setLabel("Urea");
        ph.setLabel("PH");
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findAllWAKDMeasurmentsManager();
        sensors.clear();
        sensorsOriginal.clear();
        for (int i = 0; i < pReturn.size(); i++) {
            Sensors psensor = new Sensors();
            Date dd = new Date();
            dd.setTime(pReturn.get(i).getDate());
            psensor.setTime(dd);
            psensor.setType(pReturn.get(i).getType().toString());
            psensor.setValue(pReturn.get(i).getValue());

            sensors.add(psensor);
            sensorsOriginal.add(psensor);
        }

        for (int i = 0; i < pReturn.size(); i++) {

            if (pReturn.get(i).getType() == 8) {

                sodium.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
                psodium.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
            }
            if (pReturn.get(i).getType() == 7) {

                potasium.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
                ppotasium.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
            }
            if (pReturn.get(i).getType() == 10) {

                urea.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
                purea.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
            }
            if (pReturn.get(i).getType() == 6) {

                ph.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
                pph.set(pReturn.get(i).getValue(), pReturn.get(i).getValue());
            }

        }
        linearModel.addSeries(sodium);
        linearModel.addSeries(potasium);
        linearModel.addSeries(urea);
        linearModel.addSeries(ph);
        linearModelsodium.addSeries(sodium);
        linearModelpotasium.addSeries(potasium);
        linearModelurea.addSeries(urea);
        linearModelph.addSeries(ph);
        categoryModel.addSeries(psodium);
        categoryModel.addSeries(ppotasium);
        categoryModel.addSeries(purea);
        categoryModel.addSeries(pph);
        // }
    }

    private void createCategoryModel() {
        //Not used yet
    }

    /**
     * @return the linearModelsodium
     */
    public CartesianChartModel getLinearModelsodium() {
        return linearModelsodium;
    }

    /**
     * @param linearModelsodium the linearModelsodium to set
     */
    public void setLinearModelsodium(CartesianChartModel linearModelsodium) {
        this.linearModelsodium = linearModelsodium;
    }

    /**
     * @return the linearModelpotasium
     */
    public CartesianChartModel getLinearModelpotasium() {
        return linearModelpotasium;
    }

    /**
     * @param linearModelpotasium the linearModelpotasium to set
     */
    public void setLinearModelpotasium(CartesianChartModel linearModelpotasium) {
        this.linearModelpotasium = linearModelpotasium;
    }

    /**
     * @return the linearModelurea
     */
    public CartesianChartModel getLinearModelurea() {
        return linearModelurea;
    }

    /**
     * @param linearModelurea the linearModelurea to set
     */
    public void setLinearModelurea(CartesianChartModel linearModelurea) {
        this.linearModelurea = linearModelurea;
    }

    /**
     * @return the linearModelph
     */
    public CartesianChartModel getLinearModelph() {
        return linearModelph;
    }

    /**
     * @param linearModelph the linearModelph to set
     */
    public void setLinearModelph(CartesianChartModel linearModelph) {
        this.linearModelph = linearModelph;
    }

    /**
     * @return the sensors
     */
    public List<Sensors> getSensors() {

        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }

    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findAllWAKDMeasurmentsManager() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");

        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pListToReturn = port.findAllWAKDMeasurmentsManager();
        for (int i = 0; i < pListToReturn.size(); i++) {
            if (pListToReturn.get(i).getIMEI().equalsIgnoreCase(PhoneIMEI)) {
            } else {
                pListToReturn.remove(i);
            }

        }

        return pListToReturn;
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }
}
