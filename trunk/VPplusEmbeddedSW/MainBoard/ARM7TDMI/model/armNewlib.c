/*
 * Copyright (c) 2005-2011 Imperas Software Ltd., www.imperas.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <string.h>

#ifdef __linux__
	#include <stdio.h>
#endif

// VMI area includes
#include "vmi/vmiMessage.h"
#include "vmi/vmiOSAttrs.h"
#include "vmi/vmiOSLib.h"
#include "vmi/vmiRt.h"
#include "vmi/vmiVersion.h"

#ifndef __linux__
	#include <sys/types.h>
 	#include <sys/stat.h>
	#include <fcntl.h>
#endif

#define DBG(_CAT, _FMT, ...)

//
// Message prefix
//
#define CPU_PREFIX "ARM_NEWLIB_"

//
// Get native endianness
//
#ifdef HOST_IS_BIG_ENDIAN
    #define ENDIAN_NATIVE MEM_ENDIAN_BIG
#else
    #define ENDIAN_NATIVE MEM_ENDIAN_LITTLE
#endif

//
// ARM/NewLib file modes
//
#define ARM_NEWLIB_S_IFDIR  0x4000
#define ARM_NEWLIB_S_IFREG  0x8000

#if _WIN32
    #define OPEN_MODE 0600
#else
    #define OPEN_MODE 0644
#endif

//
// ARM/NewLib offset of errno entry in struct _reent
//
#define ARM_ERRNO_OFFSET 0

//
// This defines the number of file descriptors for this processor
//
#define FILE_DES_NUM 128

#define REG_ARG_NUM  3


////////////////////////////////////////////////////////////////////////////////
// TYPES
////////////////////////////////////////////////////////////////////////////////

typedef struct vmiosObjectS {

    // first three argument registers (standard ABI)
    vmiRegInfoCP args[REG_ARG_NUM];

    // return register (standard ABI)
    vmiRegInfoCP resultReg;

    // __impure_ptr address and domain
    Addr       impurePtrAddr;
    memDomainP impurePtrDomain;

    // file descriptor table
    Int32 fileDescriptors[FILE_DES_NUM];

} vmiosObject;


////////////////////////////////////////////////////////////////////////////////
// CONSTRUCTOR & DESTRUCTOR
////////////////////////////////////////////////////////////////////////////////

#define ERRNO_REF "_impure_ptr"

//
// Constructor
//
static VMIOS_CONSTRUCTOR_FN(constructor) {

    Int32 i;

    // first three argument registers
    object->args[0] = vmiosGetRegDesc(processor, "r0");
    object->args[1] = vmiosGetRegDesc(processor, "r1");
    object->args[2] = vmiosGetRegDesc(processor, "r2");

    // return register (standard ABI)
    object->resultReg = vmiosGetRegDesc(processor, "r0");

    // _impure_ptr address and domain
    object->impurePtrDomain = vmirtAddressLookup(
        processor, ERRNO_REF, &object->impurePtrAddr
    );

    // initialise stdin, stderr and stdout
    object->fileDescriptors[0] = vmiosGetStdin(processor);
    object->fileDescriptors[1] = vmiosGetStdout(processor);
    object->fileDescriptors[2] = vmiosGetStderr(processor);

    // initialise remaining file descriptors
    for(i=3; i<FILE_DES_NUM; i++) {
        object->fileDescriptors[i] = -1;
    }
}

//
// Destructor
//
static VMIOS_DESTRUCTOR_FN(destructor) {
}


////////////////////////////////////////////////////////////////////////////////
// UTILITIES
////////////////////////////////////////////////////////////////////////////////

//
// Map from model-specific file descriptor to simulated one
//
static Int32 mapFileDescriptor(
    vmiProcessorP processor,
    vmiosObjectP  object,
    Uns32         i
) {
    if(i>=FILE_DES_NUM) {
        return -1;
    } else {
        return object->fileDescriptors[i];
    }
}

//
// Return the next available file descriptor
//
static Int32 newFileDescriptor(vmiosObjectP object, const char *context) {

    Int32 i;

    // find and return the first unused file descriptor
    for(i=0; i<FILE_DES_NUM; i++) {
        if(object->fileDescriptors[i]==-1) {
            return i;
        }
    }

    vmiMessage("P", "ARM_NEWLIB_TMOF",
        "Too many open files in %s - semihosting supports up to %u",
        context, FILE_DES_NUM
    );

    return -1;
}

//
// Byte swap the passed value
//
inline static Uns32 swap4(Uns32 value) {
    return SWAP_4_BYTE(value);
}

//
// Read a function argument using the standard ABI
//
static void getArg(
    vmiProcessorP processor,
    vmiosObjectP  object,
    Uns32         index,
    void         *result
) {
    if(index>=REG_ARG_NUM) {

        vmiMessage("P", "ARM_NEWLIB_ANS",
            "No more than %u function arguments supported",
            REG_ARG_NUM
        );

        vmirtFinish(-1);

    } else {

        vmiosRegRead(processor, object->args[index], result);
    }
}

//
// Write data in the stat buffer 'buf' back to simulation space at address
// 'bufAddr'
//
static void transcribeStatData(
    vmiProcessorP processor,
    Uns32         bufAddr,
    vmiosStatBufP buf
) {
    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    memEndian  endian = vmirtGetProcessorDataEndian(processor);

    // arm/newlib stat structure
    struct {
        Uns16 st_dev;
        Uns16 st_ino;
        Uns32 st_mode;
        Uns16 st_nlink;
        Uns16 st_uid;
        Uns16 st_gid;
        Uns16 st_rdev;
        Uns32 st_size;
        Uns32 st_atime;
        Uns32 st_spare1;
        Uns32 st_mtime;
        Uns32 st_spare2;
        Uns32 st_ctime;
        Uns32 st_spare3;
        Uns32 st_blksize;
        Uns32 st_blocks;
        Uns32 st_spare4[2];
    } simStatStruct = {0};

    // convert from host mode to arm mode
    // - host S_IFDIR converts to arm ARM_NEWLIB_S_IFDIR
    // - host S_IFREG converts to arm ARM_NEWLIB_S_IFREG
    simStatStruct.st_mode = (
        (buf->mode & 0x1ff) |
        ((buf->mode & VMIOS_S_IFDIR) ? ARM_NEWLIB_S_IFDIR : 0) |
        ((buf->mode & VMIOS_S_IFREG) ? ARM_NEWLIB_S_IFREG : 0)
    );

    // extract remaining basic fields
    simStatStruct.st_size    = (Uns32)buf->size;
    simStatStruct.st_atime   = buf->atime;
    simStatStruct.st_ctime   = buf->ctime;
    simStatStruct.st_mtime   = buf->mtime;
    simStatStruct.st_blocks  = buf->blocks;
    simStatStruct.st_blksize = buf->blksize;

    // swap endianness if required
    if(endian != ENDIAN_NATIVE) {
        simStatStruct.st_mode    = swap4(simStatStruct.st_mode);
        simStatStruct.st_size    = swap4(simStatStruct.st_size);
        simStatStruct.st_blocks  = swap4(simStatStruct.st_blocks);
        simStatStruct.st_blksize = swap4(simStatStruct.st_blksize);
        simStatStruct.st_atime   = swap4(simStatStruct.st_atime);
        simStatStruct.st_ctime   = swap4(simStatStruct.st_ctime);
        simStatStruct.st_mtime   = swap4(simStatStruct.st_mtime);
    }

    // write stat struct into arm data domain
    vmirtWriteNByteDomain(
        domain, bufAddr, &simStatStruct, sizeof(simStatStruct), 0, False
    );
}

//
// Write data in the time buffer 'buf' back to simulation space at address
// 'bufAddr'
//
static void transcribeTimeData(
    vmiProcessorP processor,
    Uns32         bufAddr,
    vmiosTimeBufP buf
) {
    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    memEndian  endian = vmirtGetProcessorDataEndian(processor);

    // swap endianness if required
    if(endian != ENDIAN_NATIVE) {
        buf->sec  = swap4(buf->sec);
        buf->usec = swap4(buf->usec);
    }

    // write values into arm data domain
    vmirtWriteNByteDomain(
        domain, bufAddr, buf, sizeof(*buf), 0, False
    );
}

//
// Write data in the times buffer 'buf' back to simulation space at address
// 'bufAddr'
//
static void transcribeTmsData(
    vmiProcessorP processor,
    Uns32         bufAddr,
    Uns32         iCount
) {
    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    memEndian  endian = vmirtGetProcessorDataEndian(processor);

    // arm/newlib tms structure
    struct {
        Uns32 utime;
        Uns32 stime;
        Uns32 cutime;
        Uns32 cstime;
    } simTmsStruct = {0};

    // We only care about the utime field.
    simTmsStruct.utime = iCount;

    // swap endianness if required
    if(endian != ENDIAN_NATIVE) {
        simTmsStruct.utime = swap4(simTmsStruct.utime);
    }

    // write tms struct into arm data domain
    vmirtWriteNByteDomain(
        domain, bufAddr, &simTmsStruct, sizeof(simTmsStruct), 0, False
    );
}

//
// Split function result into result (0/-1) and errno (-result)
//
static void setErrnoAndResult(
    vmiProcessorP processor,
    vmiosObjectP  object,
    Int32         result,
    const char   *context
) {
    if(!object->impurePtrDomain) {
        static Bool reported = False;

        if (!reported) {

            vmiMessage("W", "ARM_NEWLIB_ICF",
                "Interception of '%s' will not update 'errno' "
                "(application does not appear to be compiled with newlib "
                "or has missing symbols)",
                context
            );
            reported = True;
        }
        // vmirtFinish(-1);

    } else if(result<0) {

        memDomainP domain = object->impurePtrDomain;
        memEndian  endian = vmirtGetProcessorDataEndian(processor);
        Int32      errnum = -result;
        Uns32      impurePtrAddr;

        result = -1;

        // swap errnum endianness if required
        if(endian != ENDIAN_NATIVE) {
            errnum = swap4(errnum);
        }

        // read __impure_ptr value
        vmirtReadNByteDomain(
            domain, object->impurePtrAddr, &impurePtrAddr,
            sizeof(impurePtrAddr), 0, False
        );

        // swap errnum address endianness if required
        if(endian != ENDIAN_NATIVE) {
            impurePtrAddr = swap4(impurePtrAddr);
        }

        // write back errnum
        vmirtWriteNByteDomain(
            domain, impurePtrAddr+ARM_ERRNO_OFFSET, &errnum,
            sizeof(errnum), 0, False
        );
    }

    vmiosRegWrite(processor, object->resultReg, &result);
}


////////////////////////////////////////////////////////////////////////////////
// INTERCEPT CALLBACKS
////////////////////////////////////////////////////////////////////////////////

//
// __syscall (int number, ...)
//
static VMIOS_INTERCEPT_FN(syscallCB) {

    Uns32 code;

    getArg(processor, object, 0, &code);

    vmiMessage("F", CPU_PREFIX "UNSUPPORTED", "__syscall with code %d", code);
}

//
// _exit
//
static VMIOS_INTERCEPT_FN(exitCB) {
    vmirtExit(processor);
}

//
// __get_memtop()
//
static VMIOS_INTERCEPT_FN(getMemTopCB) {
    const Uns32 hiMem = 0x00000000;
    vmiosRegWrite(processor, object->resultReg, &hiMem);
}

//
// _open (const char *buf, int flags, int mode)
//
static VMIOS_INTERCEPT_FN(openCB) {

    Uns32 pathnameAddr;
    Int32 flags;
    Int32 mode;

    // obtain function arguments
    getArg(processor, object, 0, &pathnameAddr);
    getArg(processor, object, 1, &flags);
    getArg(processor, object, 2, &mode);

    // get file name from data domain
    memDomainP  domain   = vmirtGetProcessorDataDomain(processor);
    const char *pathname = vmirtGetString(domain, pathnameAddr);

    // map across flags
    Int32 vmiosFlags = (
        (flags & 0x003) |
        ((flags & 0x200) ? VMIOS_O_CREAT  : 0) |
        ((flags & 0x008) ? VMIOS_O_APPEND : 0) |
        ((flags & 0x400) ? VMIOS_O_TRUNC  : 0)
    );

    // implement open
    Int32 result = vmiosOpen(processor, pathname, vmiosFlags, mode);

    // save file descriptor in simulated descriptor table if successful
    if(result>=0) {
        Int32 fdMap = newFileDescriptor(object, context);
        object->fileDescriptors[fdMap] = result;
        result = fdMap;
    }

    // return result
    setErrnoAndResult(processor, object, result, context);
}

//
// _close (int fd)
//
static VMIOS_INTERCEPT_FN(closeCB) {

    Int32 fd;

    // obtain function arguments
    getArg(processor, object, 0, &fd);

    // implement close
    Int32 fdMap  = mapFileDescriptor(processor, object, fd);
    Int32 result = vmiosClose(processor, fdMap);

    // null out the semihosted file descriptor if success
    if(!result) {
        object->fileDescriptors[fd] = -1;
    }

    // return result
    setErrnoAndResult(processor, object, result, context);
}

//
// remove (const char *buf)
//
static VMIOS_INTERCEPT_FN(removeCB) {

	//	vmiMessage("I", "DEBUG", "Inside intercepted remove!\n");

    Uns32 FileName;
    
    // obtain function arguments
    getArg(processor, object, 0, &FileName);

    // get file name from data domain
    memDomainP  domain   = vmirtGetProcessorDataDomain(processor);
    const char *fn = vmirtGetString(domain, FileName);

		Int32 result = vmiosUnlink(processor, fn);

    // return result
    setErrnoAndResult(processor, object, result, context);
}

//
// rename (const char *buf, const char *buf)
//
static VMIOS_INTERCEPT_FN(renameCB) {

	// vmiMessage("I", "DEBUG", "Inside intercepted rename!\n");

	Uns32 FileNameFrom;
	Uns32 FileNameTo;
	#ifndef __linux__
	Int32 fdMapFrom;
	Int32 fdMapTo;
	#endif
	Int32 mode;
    
	// obtain function arguments
	getArg(processor, object, 0, &FileNameFrom);
	getArg(processor, object, 1, &FileNameTo);
	getArg(processor, object, 2, &mode);
	
	// get file name from data domain
	memDomainP  domain   = vmirtGetProcessorDataDomain(processor);
	const char *fnFrom = vmirtGetString(domain, FileNameFrom);
	const char *fnTo   = vmirtGetString(domain, FileNameTo);
	
	#ifdef __linux__
	Int32 result = rename(fnFrom, fnTo);
	#endif
	// This does not work! Grrrrrr. Well, to get this over with I just
  // implement a 'stupid' rename here that creates the file new, copies
  // the old into it and after deletes the old.
  // This is waisted performance, but it is host performance not in the simulation.
  
//	// Open the two files
//	vmiMessage("I", "RENAME", "Opening From-File");
//  // Int32 resultFrom = vmiosOpen(processor, fnFrom, VMIOS_O_RDONLY, mode);
//	Int32 resultFrom = open(fnFrom,O_RDONLY);
//	if(resultFrom>=0) { // is same as:	if(resultFrom != -1) {
//		vmiMessage("I", "RENAME", "From-File is open");
//		//fdMapFrom = newFileDescriptor(object, context);
//		//object->fileDescriptors[fdMapFrom] = resultFrom;
//		//resultFrom = fdMapFrom;
//	}
//	vmiMessage("I", "RENAME", "Opening To-File");
//  // Int32 resultTo   = vmiosOpen(processor, fnTo, (VMIOS_O_WRONLY|VMIOS_O_CREAT), 700);
//	Int32 resultTo = open(fnTo, O_WRONLY|O_CREAT);
//	if(resultTo>=0) { // 	if(resultTo != -1) {
//		vmiMessage("I", "RENAME", "To-File is open");
//		//fdMapTo = newFileDescriptor(object, context);
//		//object->fileDescriptors[fdMapTo] = resultTo;
//		//resultTo = fdMapTo;
//	}
//
//	// Copy one file into the other
//	vmiMessage("I", "RENAME", "Copying bytes from to");
//	char buf[]="U";
//	//Int32 resultRead = vmiosRead(processor, fdMapFrom, domain, buf, 1);
//	while (read(resultFrom, &buf, 1))
//		{
//			//printf("%s", buf);
//			write(resultTo, &buf, 1);
//		}
//
//  // Close the two files // implement close
//	vmiMessage("I", "RENAME", "Closing From-File");
//	//fdMapFrom  = mapFileDescriptor(processor, object, resultFrom);
//  //resultFrom = vmiosClose(processor, fdMapFrom);
//	//if(!resultFrom) {
//	if (close(resultFrom)==0){
//		vmiMessage("I", "RENAME", "Closed");
//		//object->fileDescriptors[fdMapFrom] = -1;
//	}
//	vmiMessage("I", "RENAME", "Closing To-File");
//	//fdMapTo  = mapFileDescriptor(processor, object, resultTo);
//	//resultTo   = vmiosClose(processor, fdMapTo  );
// 	//if(!resultTo) {
//	if(close(resultTo)==0){
//		vmiMessage("I", "RENAME", "Closed");
//		//object->fileDescriptors[fdMapTo] = -1;
//	}
	#ifndef __linux__
	// remove the from file
	vmiMessage("I", "RENAME", "Deleting From-File");
	Int32 resultDelete = vmiosUnlink(processor, fnFrom);
	if(!resultDelete) {
		vmiMessage("I", "RENAME", "Deleted");
	}

 
	Int32 result = 0;
	#endif
    // return result
    setErrnoAndResult(processor, object, result, context);
}


//
// _read (int fd, void *buf, size_t count)
//
static VMIOS_INTERCEPT_FN(readCB) {

    Int32 fd;
    Uns32 buf;
    Uns32 count;

    // obtain function arguments
    getArg(processor, object, 0, &fd);
    getArg(processor, object, 1, &buf);
    getArg(processor, object, 2, &count);

    // implement read
    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    Int32      fdMap  = mapFileDescriptor(processor, object, fd);
    Int32      result = vmiosRead(processor, fdMap, domain, buf, count);

    // return result
    setErrnoAndResult(processor, object, result, context);
}

//
// _write (int fd, void *buf, size_t count)
//
static VMIOS_INTERCEPT_FN(writeCB) {

    Int32 fd;
    Uns32 buf;
    Uns32 count;

    // obtain function arguments
    getArg(processor, object, 0, &fd);
    getArg(processor, object, 1, &buf);
    getArg(processor, object, 2, &count);

    // implement write
    memDomainP domain = vmirtGetProcessorDataDomain(processor);
    Int32      fdMap  = mapFileDescriptor(processor, object, fd);
    Int32      result = vmiosWrite(processor, fdMap, domain, buf, count);

    // return result
    setErrnoAndResult(processor, object, result, context);

}

//
// _lseek (int fildes, off_t offset, int whence)
//
static VMIOS_INTERCEPT_FN(lseekCB) {

    Int32 fildes;
    Int32 offset;
    Int32 whence;

    // obtain function arguments
    getArg(processor, object, 0, &fildes);
    getArg(processor, object, 1, &offset);
    getArg(processor, object, 2, &whence);

    // implement lseek
    Int32 fdMap  = mapFileDescriptor(processor, object, fildes);
    Int32 result = vmiosLSeek(processor, fdMap, offset, whence);

    // return result
    setErrnoAndResult(processor, object, result, context);
}

//
// _fstat (int filedes, struct stat *buf)
//
static VMIOS_INTERCEPT_FN(fstatCB) {

    Int32 filedes;
    Uns32 bufAddr;

    // obtain function arguments
    getArg(processor, object, 0, &filedes);
    getArg(processor, object, 1, &bufAddr);

    // implement fstat
    vmiosStatBuf statBuf = {0};
    Int32        fdMap   = mapFileDescriptor(processor, object, filedes);
    Uns32        result  = vmiosFStat(processor, fdMap, &statBuf);

    // write back results
    transcribeStatData(processor, bufAddr, &statBuf);
    setErrnoAndResult(processor, object, result, context);
}

//
// _stat (const char *file_name, struct stat *buf)
//
static VMIOS_INTERCEPT_FN(statCB) {

    Uns32 file_nameAddr;
    Uns32 bufAddr;

    // obtain function arguments
    getArg(processor, object, 0, &file_nameAddr);
    getArg(processor, object, 1, &bufAddr);

    // get file name from data domain
    memDomainP  domain    = vmirtGetProcessorDataDomain(processor);
    const char *file_name = vmirtGetString(domain, file_nameAddr);

    // implement stat
    vmiosStatBuf statBuf = {0};
    Uns32        result  = vmiosStat(processor, file_name, &statBuf);

    // write back results
    transcribeStatData(processor, bufAddr, &statBuf);
    setErrnoAndResult(processor, object, result, context);
}

//
// gettimeofday (struct timeval *tv, struct timezone *tz)
//
static VMIOS_INTERCEPT_FN(gettimeofdayCB)
{
    Uns32 tvAddr;

    // obtain function arguments
    getArg(processor, object, 0, &tvAddr);

    // implement gettimeofday
    vmiosTimeBuf timeBuf = {0};
    Int32        result  = vmiosGetTimeOfDay(processor, &timeBuf);

    // write back results
    transcribeTimeData(processor, tvAddr, &timeBuf);
    setErrnoAndResult(processor, object, result, context);
}

//
// times (struct tms *buf)
//
static VMIOS_INTERCEPT_FN(timesCB) {

    Uns32 bufAddr;
    Int32 result = 0;

    // obtain function arguments
    getArg(processor, object, 0, &bufAddr);

    // we will return a time based on the number of instructions executed
    // within this simulation
    result = vmirtGetICount(processor);
    transcribeTmsData(processor, bufAddr, result);

    // write back results
    setErrnoAndResult(processor, object, result, context);
}

//
// initialise_monitor_handles
//
static VMIOS_INTERCEPT_FN(initMonHandlesCB) {
    // ignored
}


////////////////////////////////////////////////////////////////////////////////
// INTERCEPT ATTRIBUTES
////////////////////////////////////////////////////////////////////////////////

vmiosAttr modelAttrs = {

    ////////////////////////////////////////////////////////////////////////
    // VERSION
    ////////////////////////////////////////////////////////////////////////

    .versionString  = VMI_VERSION,            // version string (THIS MUST BE FIRST)
    .modelType      = VMI_INTERCEPT_LIBRARY,  // type
    .packageName    = "Newlib",               // description
    .objectSize     = sizeof(vmiosObject),    // size in bytes of OSS object

    ////////////////////////////////////////////////////////////////////////
    // CONSTRUCTOR/DESTRUCTOR ROUTINES
    ////////////////////////////////////////////////////////////////////////

    .constructorCB  = constructor,            // object constructor
    .destructorCB   = destructor,             // object destructor

    ////////////////////////////////////////////////////////////////////////
    // INSTRUCTION INTERCEPT ROUTINES
    ////////////////////////////////////////////////////////////////////////

    .morphCB        = 0,                      // morph callback
    .nextPCCB       = 0,                      // get next instruction address
    .disCB          = 0,                      // disassemble instruction

    ////////////////////////////////////////////////////////////////////////
    // ADDRESS INTERCEPT DEFINITIONS
    ////////////////////////////////////////////////////////////////////////
    {
        // ---------------------------- ------- ------ -----------------
        // Name                         Address Opaque Callback
        // ---------------------------- ------- ------ -----------------
        { "__syscall",                  0,      True,  syscallCB        },
        { "_exit",                      0,      True,  exitCB           },
        { "_open",                      0,      True,  openCB           },
        { "_close",                     0,      True,  closeCB          },
        { "remove",                     0,      True,  removeCB         },
        { "rename",                     0,      True,  renameCB         },
        { "_read",                      0,      True,  readCB           },
        { "_write",                     0,      True,  writeCB          },
        { "__get_memtop",               0,      True,  getMemTopCB      },
        { "_lseek",                     0,      True,  lseekCB          },
        { "_fstat",                     0,      True,  fstatCB          },
        { "_stat",                      0,      True,  statCB           },
        { "_gettimeofday",              0,      True,  gettimeofdayCB   },
        { "times",                      0,      True,  timesCB          },
        { "initialise_monitor_handles", 0,      True,  initMonHandlesCB },
        {  0                                                            },
    }
};
