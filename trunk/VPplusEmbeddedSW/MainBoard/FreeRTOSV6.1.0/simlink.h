/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
 
#include "nephron.h"

#define NumberActuatorSensor 100

#define SIMLINKBASE 0x80010000
// Even though defined here, the addresses might not be
// available! This depends on the preprocessor variable
// "NumberActuatorSensor" defined in simLink.h during VP compilation.
// e.g. if this variable is 10 only ACTUATOR0-9 and SENSOR0-9 will be
// accessible.

#define IRQ0VECTADDR SIMLINKBASE
#define IRQ1VECTADDR (SIMLINKBASE+4)
#define IRQ2VECTADDR (SIMLINKBASE+8)
#define IRQ3VECTADDR (SIMLINKBASE+12)

#define ACTUATOR(n) (SIMLINKBASE + (8*(n)+16) )
#define SENSOR(n)   (SIMLINKBASE + (8*(n)+20) )

void setActuatorInSimulink (unsigned int actuatorAddress, const unsigned int actuatorFixpointShift, actuatorType value);
