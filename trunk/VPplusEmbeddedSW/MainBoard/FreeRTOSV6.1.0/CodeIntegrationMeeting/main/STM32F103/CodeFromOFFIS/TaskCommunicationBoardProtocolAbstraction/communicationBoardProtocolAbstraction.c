/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "main.h"
	#include "communicationBoardProtocolAbstraction.h"
	#include "apiWakdTime.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\main\\main.h"
	#include "communicationBoardProtocolAbstraction.h"
	#include "..\\apiWakdTime.h"
#endif

#define PREFIX "OFFIS FreeRTOS task CBPA"

// #define DEBUG_CBPA

#ifdef VP_SIMULATION
	// Dummy functionality for functions getData and putData. The dummies operate with the
	// simlink interface instead of a real serial interface buffer!
	#include "DummyCode/getData.h"
#endif

/*-----------------------------------------------------------*/

uint8_t sendAck(tdHeader *pHeader);

uint8_t heartBeatCBPA = 0;

void tskCBPA( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting Communication Board Protocol Abstraction task!");

	// On system reset, the communication board abstraction task sends one last message to the HW.
	// After sending the msg the task waits for the ACK on this to arrive and then terminates.
	//
	uint8_t resetWaitingForAck	= 0;
	uint8_t resetMsgCount		= 0;

	xQueueHandle *qhCBPAin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhCBPAin;
	xQueueHandle *qhDISPin    = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhREMINDERin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhREMINDERin;

 	tdQtoken Qtoken;
	Qtoken.command = command_DefaultError;
	Qtoken.pData   = NULL;
	
	// every msg sent receives a new number so that we can identify which Ack belongs to which msg.
	uint8_t msgCount = 0;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	for( ;; ) {
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every CBPA_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatCBPA++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif
		if ( xQueueReceive( qhCBPAin, &Qtoken, CBPA_PERIOD) == pdPASS )
		{
			#ifdef DEBUG_CBPA
				taskMessage("I", PREFIX, "id: %d in CBPA queue!", Qtoken.command);
			#endif

			switch (Qtoken.command) {
				// ##############################################################################
				case command_UseAtachedMsgHeader: {
				// ##############################################################################
					#ifdef DEBUG_CBPA
						taskMessage("I", PREFIX, "id: 'command_UseAtachedMsgHeader'");
					#endif
					// This message comes from some other task of the WAKD. Forward it to CB.
					// The msg could be one that we expect to receive an
					// ACK as an answer from the recipient. Or our msg itself is an ACK for
					// the recipient. We do not expect an ACK on an ACK, so we only register
					// the first type of msg in our reminder task.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case (dataID_ack):{
						// ##############################################################################
							// this msg is an ACK, we do not expect an answer and must not change msgCount
							#if defined DEBUG_CBPA || defined SEQ4
								taskMessage("I", PREFIX, "Calling 'putData()'.");
							#endif
							if ( putData(Qtoken.pData) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Call of function putData() failed!");
								errMsg(errmsg_putDataFailed);
							}
							sFree(&(Qtoken.pData));
							break;
						}
						case (dataID_nack):{
						// ##############################################################################
							// this msg is an NACK, we do not expect an answer and must not change msgCount
							#if defined DEBUG_CBPA || defined SEQ4
								taskMessage("I", PREFIX, "Calling 'putData()'.");
							#endif
							if ( putData(Qtoken.pData) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Call of function putData() failed!");
								errMsg(errmsg_putDataFailed);
							}
							sFree(&(Qtoken.pData));
							break;
						}
						default:{
						// ##############################################################################
							// This is not an ACK, so it needs a counter assigned for the
							// ACK we expect to be sent back from recipient.
							((tdMsgOnly *)(Qtoken.pData))->header.msgCount = ++msgCount;
							if (dataId == dataID_reset || dataId == dataID_shutdown)
							{	// If the msg is a command to reset, we have to remember that the system is
								// going down and what on msgAck we are waiting for. Than we will terminate this
								// task.
								#if defined DEBUG_CBPA || defined SEQ0
									taskMessage("I", PREFIX, "Starting shutdown of task.");
								#endif
								resetWaitingForAck	= 1;	// 1: did send reset to CB first time. Waiting for ACK
								resetMsgCount		= msgCount;
							}
							#if defined DEBUG_CBPA || defined SEQ0 || defined SEQ1 || defined SEQ5 || defined SEQ6 || defined SEQ11 || defined SEQ12
								taskMessage("I", PREFIX, "Calling 'putData()'.");
							#endif
							if ( putData(Qtoken.pData) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Call of function putData() failed!");
								errMsg(errmsg_putDataFailed);
							}
							// Register msg for Ack to arrive from recipient"
							Qtoken.command = command_WaitOnMsgCBPA;
							// Qtoken.pData 	// remains unchanged
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_WaitOnMsg' with cnt: %d for %s.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount, dataIdToString(((tdMsgOnly *)(Qtoken.pData))->header.dataId));
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_WaitOnMsg'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree((void *)&(Qtoken.pData));
							}
							break;
						}
					}
					break;
				}
				// ##############################################################################
				case command_WakeupCall: {
				// ##############################################################################
					#ifdef DEBUG_CBPA
						taskMessage("I", PREFIX, "id: 'command_WakeupCall'");
					#endif
					// We did send a message for which we are missing an ACK. Time to resend.
					#if defined DEBUG_CBPA || defined SEQ1
						taskMessage("I", PREFIX, "Re-sending 'command_WaitOnMsg'.");
					#endif
					if ( putData((void *) Qtoken.pData) )
					{	// msg could not be forwarded to communication board.
						errMsg(errmsg_putDataFailed);
					}
					// again register msg for Ack to arrive from recipient"
					Qtoken.command = command_WaitOnMsgCBPA;
					// Qtoken.pData   = STAYS THE SAME
					if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
					{	// CBPA reminder queue did not accept data. Free message and send error.
						errMsg(errmsg_QueueOfReminderTaskFull);
						sFree(&(Qtoken.pData));
						#ifndef VP_SIMULATION
							// Even though this is an unlikely event and already an error (big problem) by itself
							// it also contains a possible memory leak. If this msg was listed by the reminder Task before
							// it did create a list entry for this msg. In the line above we freed the MSG but not the list
							// entry in the reminder task. Right now this potential leak is not being covered!
							#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
						#endif
					}
					if (   ((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_reset
						|| ((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_shutdown )
					{
						// We have to repeat the sending of a reset command to the communication board.
						// After the third time we just give up and close down this task anyway.
						resetWaitingForAck++; // Still waiting for ACK
						if (resetWaitingForAck > 2)
						{	// Give up waiting for ACK. Shut down without response from CB
							#if defined DEBUG_CBPA || defined SEQ0
								taskMessage("I", PREFIX, "No ack from Communication Board on reset. Giving up and shutting down task.");
							#endif
							((tdAllHandles *) pvParameters)->allTaskHandles.handleCBPA = NULL;
							vTaskDelete(NULL);	// Shut down and wait forever.
							vTaskSuspend(NULL); // Never run again (until after reset)
						}
					}
					break;
				}
				// ##############################################################################
				case command_BufferFromCBavailable: {
				// ##############################################################################
					#ifdef DEBUG_CBPA
						taskMessage("I", PREFIX, "id: 'command_BufferFromCBavailable'");
					#endif
					
					#ifdef VP_SIMULATION
					// read value of irqVectAddr to know what the IRQ stands for; what action has to be taken?
					// In the real HW this information is part of the msg header in the transmitted buffer.
					// Our Simulink model does not generate this header though. So this is a workaroung implementation
					// that will have to be enhanced in the release code for the main board.
					extern uint32_t dummy_WhatDataToPullCB;

					switch(dummy_WhatDataToPullCB){
						case (1):{// Command change into state wakdStates_AllStopped
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_AllStopped;			// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (2):{// Command change into state wakdStates_Dialysis
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];			// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;			// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_Dialysis;			// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  		// toWakdOpState = stay in current state
							break;
						}
						case (3):{// Command change into state wakdStates_Regen1
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_Regen1;				// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (4):{// Command change into state wakdStates_Ultrafiltration
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_Ultrafiltration;		// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (5):{// Command change into state wakdStates_Regen2
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_Regen2;				// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (6):{// Command change into state wakdStates_Maintenance
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_Maintenance;			// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (7):{// Command change into state wakdStates_NoDialysate
							uartBufferCBin[0]   = 0x01;	// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;	// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;	// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;	// header.dataId = dataID_changeWAKDStateFromTo
							uartBufferCBin[4]   = ++uartBufferCBin[4];				// header.msgCount
							uartBufferCBin[5]   = whoId_SP;						// header.issuedBy = SP
							uartBufferCBin[6]   = staticBufferStateRTB;				// fromWakdStateDummy = global state variable to be always correct!
							uartBufferCBin[7]   = staticBufferOperationalStateRTB;			// fromWakdOpState    = Dummy global state variable to be always correct!
							uartBufferCBin[8]   = wakdStates_NoDialysate;			// toWakdState
							uartBufferCBin[9]   = staticBufferOperationalStateRTB;  			// toWakdOpState = stay in current state
							break;
						}
						case (8):{// CB did receive weight data
							#if defined SEQ2
								taskMessage("I", PREFIX_CB, "Weight scale event. Inform dispatcher task.");
							#endif
							// Receive sensor data from Simulink environment
							uint16_t Weight   = *(REG32 SENSOR_WEIGHTSCALE);	// weight in 0.1 kg
							// Stuffing the bytes into UART buffer
							uartBufferCBin[0]   = whoId_MB;						// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;							// header.msgSize = 15 bytes (most significant byte)
							uartBufferCBin[2]   = 15;							// header.msgSize = 15 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_weightData;			// header.dataId = dataID_weightData
							uartBufferCBin[4]   = ++uartBufferCBin[4];			// header.msgCount
							uartBufferCBin[5]   = whoId_CB;						// header.issuedBy = CB
							// Payload
							uartBufferCBin[6]   = *(((char *)(&Weight))+1);	// take second byte of Weight
							uartBufferCBin[7]   = *(((char *)(&Weight))+0);	// take first byte of weight
							uartBufferCBin[8]   = 173; 						// body fat percentage in 0.1%
							uartBufferCBin[9]   = 0;	 						// WSBatteryLevel; ????? percentage in *1 % ?????
							uartBufferCBin[10]  = 100;	 					// WSBatteryLevel; ????? percentage in *1 % ?????
							uartBufferCBin[11]  = 1;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[12]  = 2;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[13]  = 3;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[14]  = 4;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							break;
						}
						case (9):{
							// Incoming request to connect to weight scale
							#if defined SEQ1
								taskMessage("I", PREFIX_CB, "Request to connect to weight scale. Inform dispatcher task.");
							#endif
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = 6 bytes (most significant byte)
							uartBufferCBin[2]   = 0x06;					// header.msgSize = 6 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_weightRequest;	// header.dataId = dataID_weightRequest
							uartBufferCBin[4]   = ++uartBufferCBin[4];		// header.msgCount
							uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
							break;
						}
						case (10):{
							// User (patient) sends acknowledged weight measure
							#if defined SEQ3
								taskMessage("I", PREFIX_CB, "Patient sends Ack for weight. Inform dispatcher task.");
							#endif
							// Receive sensor data from Simulink environment
							uint16_t Weight   = *(REG32 SENSOR_WEIGHTSCALE);	// weight in 0.1 kg
							// Stuffing the bytes into UART buffer
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = ?? bytes (most significant byte)
							uartBufferCBin[2]   = 15;						// header.msgSize = ?? bytes (least significant byte)
							uartBufferCBin[3]   = dataID_weightDataOK;	// header.dataId = dataID_weightData
							uartBufferCBin[4]   = ++uartBufferCBin[4];		// header.msgCount
							uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
							// Payload
							uartBufferCBin[6]   = *(((char *)(&Weight))+1);	// take second byte of Weight
							uartBufferCBin[7]   = *(((char *)(&Weight))+0);	// take first byte of weight
							uartBufferCBin[8]   = 173; 						// body fat percentage in 0.1%
							uartBufferCBin[9]   = 0;	 						// WSBatteryLevel; ????? percentage in *1 % ?????
							uartBufferCBin[10]  = 100;	 					// WSBatteryLevel; ????? percentage in *1 % ?????
							uartBufferCBin[11]  = 1;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[12]  = 2;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[13]  = 3;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							uartBufferCBin[14]  = 4;							// WSStatus !!!!!!!!!!!!!!!!!to be defined??
							break;
						}
						case (11):{
							// Smart Phone request an update on WAKD status.
							#if defined SEQ5
								taskMessage("I", PREFIX_CB, "Smart Phone requests an update on WAKD status.");
							#endif
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = 6 bytes (most significant byte)
							uartBufferCBin[2]   = 0x06;					// header.msgSize = 6 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_statusRequest;	// header.dataId = dataID_statusRequest
							uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
							uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
							break;
						}
						case (12):{
							// Smart Phone requests a change of state. Prepare UART buffer for getData() to pick up
							#if defined SEQ4
								taskMessage("I", PREFIX_CB, "Smart Phone requests a change of state.");
							#endif
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = 10 bytes (most significant byte)
							uartBufferCBin[2]   = 0x0a;					// header.msgSize = 10 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_changeWAKDStateFromTo;
							uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
							uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
							uartBufferCBin[6]   = *(REG32 SENSOR_FROMSTATE);	// fromWakdState
							uartBufferCBin[7]   = *(REG32 SENSOR_FROMOPSTATE);	// fromWakdOpState = wakdStatesOS_Automatic
							uartBufferCBin[8]   = *(REG32 SENSOR_TOSTATE);		// toWakdState = wakdStates_AllStopped
							uartBufferCBin[9]   = *(REG32 SENSOR_TOOPSTATE);	// toWakdOpState = wakdStatesOS_Automatic
							break;
						} 
						case (13):{
							// Incoming request to connect to Blood Pressure Device
							// #if defined SEQ1
								// taskMessage("I", PREFIX_CB, "Request to connect to blood pressure device. Inform dispatcher task.");
							// #endif
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = 6 bytes (most significant byte)
							uartBufferCBin[2]   = 0x06;					// header.msgSize = 6 bytes (least significant byte)
							uartBufferCBin[3]   = dataID_bpRequest;		// header.dataId = dataID_weightRequest
							uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
							uartBufferCBin[5]   = whoId_SP;				// header.issuedBy = SP
							break;
						}
					case (14):{
							// Blood pressure device sends sends bpData
							// #if defined SEQ4
								// taskMessage("I", PREFIX_CB, "Blood pressure device sends blood pressure data.");
							// #endif
							// Receive sensor data from Simulink environment

							// uint8_t BloodPressSys = *(REG32 SENSOR_BLOODPRESYS);	// bloodpressure systolic
							// uint8_t BloodPressDia = *(REG32 SENSOR_BLOODPREDIA);	// bloodpressure diastolic
							// uint8_t BloodPressHR  = *(REG32 SENSOR_BLOODPREHR);		// bloodpressure (device own measurement) heart rate
							// Stuffing the bytes into UART buffer
							uartBufferCBin[0]   = whoId_MB;				// header.recipientId = MB
							uartBufferCBin[1]   = 0x00;					// header.msgSize = ?? bytes (most significant byte)
							uartBufferCBin[2]   = 0x09;					// header.msgSize = ?? bytes (least significant byte)
							uartBufferCBin[3]   = dataID_bpData;		// header.dataId = dataID_bpData
							uartBufferCBin[4]   = ++uartBufferCBin[4];	// header.msgCount
							uartBufferCBin[5]   = whoId_CB;				// header.issuedBy = CB
							// Payload
							uartBufferCBin[6]   = *(REG32 SENSOR_BLOODPRESYS);	// take the byte of BloodPressSys
							uartBufferCBin[7]   = *(REG32 SENSOR_BLOODPREDIA);	// take the byte of BloodPressDia
							uartBufferCBin[8]   = *(REG32 SENSOR_BLOODPREHR);		// take the byte of BloodPressHR
							// uartBufferCBin[6]   = *(((char *)(&BloodPressSys)));	// take the byte of BloodPressSys
							// uartBufferCBin[7]   = *(((char *)(&BloodPressDia)));	// take the byte of BloodPressDia
							// uartBufferCBin[8]   = *(((char *)(&BloodPressHR)));		// take the byte of BloodPressHR
							break;
						}	// */	
					}
				#endif
					
					
					// Look at buffer header information.
					tdDataId dataId = readDataId(whoId_CB);
					switch (dataId) {
						case dataID_ack:{
						// ##############################################################################
				// ** STEP 1: Receive Ack Buffer
							tdMsgOnly *pAck = NULL;
							pAck = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
							if ( pAck == NULL )
							{	// unable to allocate memory
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ1 || defined SEQ4 || defined SEQ5
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: ACK.");
							#endif
							if ( getData(whoId_CB, (void *) pAck) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pAck);
								break;
							}
				// ** STEP 2: Forward Ack Buffer to Reminder to delist the according msg
							Qtoken.command = command_AckOnMsgCBPA;
							Qtoken.pData   = (void *) pAck;
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_AckOnMsgCBPA' with cnt: %d.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount);
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_AckOnMsgCBPA'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree(&(Qtoken.pData));
							}
				// ** STEP 3: OPtional - if this is an ACK on reset, we now shut down this task
							if (resetWaitingForAck > 0)
							{	// The system is going down. Is this ACK the one for system reset?
								if (pAck->header.msgCount == resetMsgCount)
								{	// This is the ACK we where waiting for on our reset.
									#if defined DEBUG_CBPA || defined SEQ0
										taskMessage("I", PREFIX, "Communication Board acknowledged reset. Task is shutting down.");
									#endif
									((tdAllHandles *) pvParameters)->allTaskHandles.handleCBPA = NULL;
									vTaskDelete(NULL);	// Shut down and wait forever.
									vTaskSuspend(NULL); // Never run again (until after reset)
								}
							}
							break;
						}
						case dataID_nack:{
						// ##############################################################################
							// There is a problem with one of our CBPA messages we sent earlier.
							taskMessage("W", PREFIX, "Communication Board sends negative acknowledge!!");
							errMsg(errmsg_nackFromCB);
				// ** STEP 1: Receive NACK Buffer
							tdMsgOnly *pNack = NULL;
							pNack = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
							if ( pNack == NULL )
							{	// unable to allocate memory
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ1 || defined SEQ4 || defined SEQ5
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: NACK.");
							#endif
							if ( getData(whoId_CB, (void *) pNack) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pNack);
								break;
							}
				// ** STEP 2: Forward NACK Buffer to Reminder to delist the according msg
							Qtoken.command = command_AckOnMsgCBPA;	// For the Reminder Task it is the same if ACK or NACK arrived. Response is there, so delist from reminder.
							Qtoken.pData   = (void *) pNack;
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_AckOnMsgCBPA' with cnt: %d.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount);
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_AckOnMsgCBPA'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;
						}

						case dataID_CurrentParameters:{
						// ##############################################################################
							// SEQ12:
							// The phone likes to be informed about current configuration parameters.
				// ** STEP 1: Receive Buffer
							uint16_t dataSize = readDataSize(whoId_CB);
printf("Data size is: %d\n", dataSize);
							#if defined DEBUG_CBPA || defined SEQ12
								taskMessage("I", PREFIX, "Calling 'readDataSize()=%d (msgHeader=%d) to receive dataID_CurrentParameters.", dataSize, sizeof(tdMsgOnly));
							#endif
							void *pCurrentParametersRequest = NULL;
							pCurrentParametersRequest = pvPortMalloc(dataSize);
							if ( pCurrentParametersRequest == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ12
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: dataID_CurrentParameters.");
							#endif
							if ( getData(whoId_CB, pCurrentParametersRequest) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pCurrentParametersRequest);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ12
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_CurrentParameters.");
							#endif
							if (sendAck(&(((tdMsgOnly*)pCurrentParametersRequest)->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pCurrentParametersRequest);
								break;
							}
				// ** STEP 3: Forward parameter request to data storage via dispatcher
							#if defined DEBUG_CBPA || defined SEQ12
								taskMessage("I", PREFIX, "Forwarding 'dataID_CurrentParameters' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pCurrentParametersRequest;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pCurrentParametersRequest);
							}
							break;
						}

						case dataID_ConfigureParameters:{
						// ##############################################################################
							// SEQ13: The phone wants to reprogram some parameters of the WAKD
				// ** STEP 1: Receive Buffer
							uint16_t dataSize = readDataSize(whoId_CB);
							#if defined DEBUG_CBPA || defined SEQ13
								taskMessage("I", PREFIX, "Calling 'readDataSize()=%d (msgHeader=%d) to receive dataID_ConfigureParameters.", dataSize, sizeof(tdMsgOnly));
							#endif
							void *pConfigureParameters = NULL;
							pConfigureParameters = pvPortMalloc(dataSize);
							if ( pConfigureParameters == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ13
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: dataID_ConfigureParameters.");
							#endif
							if ( getData(whoId_CB, pConfigureParameters) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pConfigureParameters);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ13
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_ConfigureParameters.");
							#endif
							if (sendAck(&(((tdMsgOnly*)pConfigureParameters)->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pConfigureParameters);
								break;
							}
				// ** STEP 3: Forward parameter request to data storage via dispatcher
							#if defined DEBUG_CBPA || defined SEQ13
								taskMessage("I", PREFIX, "Forwarding 'dataID_ConfigureParameters' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pConfigureParameters;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pConfigureParameters);
							}
							break;
						}

						case dataID_weightRequest:{
						// ##############################################################################
							// SEQ1: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgOnly *pWeightRequest = NULL;
							pWeightRequest = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
							if ( pWeightRequest == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: weightRequest.");
							#endif
							if ( getData(whoId_CB, (void *) pWeightRequest) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pWeightRequest);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_weightRequest.");
							#endif
							if (sendAck(&(pWeightRequest->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pWeightRequest);
								break;
							}
				// ** STEP 3: Forward weight request to communication board
							// CB should go into listening mode.
							pWeightRequest->header.issuedBy 	= whoId_MB; // reuse msg from phone, but send from mainboard now
							pWeightRequest->header.recipientId	= whoId_CB;
							pWeightRequest->header.msgCount = ++msgCount;
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'putData() to forward 'dataID_weightRequest' to CB.");
							#endif
							if ( putData((void *) pWeightRequest) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Unable to write buffer to CB UART!");
								errMsg(errmsg_putDataFailed);
							}
				// ** STEP 4: Register msg for Ack to arrive from recipient"
							Qtoken.command = command_WaitOnMsgCBPA;
							Qtoken.pData   = (void *) pWeightRequest;
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_WaitOnMsg' with cnt: %d.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount);
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_WaitOnMsg'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree((void *)&pWeightRequest);
							}
							break;
						}
						case dataID_weightData: {
						// ##############################################################################
							// SEQ2: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgWeightData *pMsgWeightData = NULL;
							pMsgWeightData = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData));
							if ( pMsgWeightData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ2
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: weightData.");
							#endif
							if ( getData(whoId_CB, (void *) pMsgWeightData) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgWeightData);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ2
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_weightData.");
							#endif
							if (sendAck(&(pMsgWeightData->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pMsgWeightData);
								break;
							}
				// ** STEP 3: Duplicate Buffer
							// The patient can be reached using two interfaces: WAKD UI or smartphone.
							// Each recipient receives his own buffer to work with
							tdMsgWeightData *pMsgWeightDataSP = NULL;
							tdMsgWeightData *pMsgWeightDataUI = NULL;
							pMsgWeightDataSP = pMsgWeightData; // The smartphone receives the original
							pMsgWeightDataUI = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData));
							memcpy(pMsgWeightDataUI, pMsgWeightData, sizeof(tdMsgWeightData)); // UI gets the copy
				// ** STEP 4: Forward buffer to smartphone
							// Weight measure received. Send to Patient so that he can Ack the value.
							pMsgWeightDataSP->header.issuedBy    = whoId_MB;
							pMsgWeightDataSP->header.recipientId = whoId_SP;
							pMsgWeightDataSP->header.msgCount    = ++msgCount;
							#if defined DEBUG_CBPA || defined SEQ2
								taskMessage("I", PREFIX, "Forwarding 'dataID_weightData' to CB (SP).");
							#endif
							if ( putData((void *) pMsgWeightDataSP) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Unable to write buffer to CB UART!");
								errMsg(errmsg_putDataFailed);
								sFree((void *)&pMsgWeightDataSP);
							}
				// ** STEP 5: Register msg for Ack to arrive from recipient"
							Qtoken.command = command_WaitOnMsgCBPA;
							Qtoken.pData   = (void *) pMsgWeightDataSP;
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_WaitOnMsg' with cnt: %d.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount);
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_WaitOnMsg'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree((void *)&pMsgWeightDataSP);
							}
				// ** STEP 6: Forward buffer to Task User Interface (via DISP)
							#if defined DEBUG_CBPA || defined SEQ2
								taskMessage("I", PREFIX, "Forwarding 'dataID_weightData' to Task UI (via Disp).");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgWeightDataUI;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_DataWeightSensorRead'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgWeightDataUI);
							}
							break;
						}
						case dataID_weightDataOK:{
						// ##############################################################################
							// SEQ3: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgWeightData *pMsgWeightDataOK = NULL;
							pMsgWeightDataOK = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData));
							if ( pMsgWeightDataOK == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: weightDataOK.");
							#endif
							pMsgWeightDataOK->weightData.TimeStamp = getTime();
							if ( getData(whoId_CB, (void *) pMsgWeightDataOK) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgWeightDataOK);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_weightDataOK.");
							#endif
							if (sendAck(&(pMsgWeightDataOK->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pMsgWeightDataOK);
								break;
							}
				// ** STEP3: Forward buffer to dispatcher task
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Forwarding 'dataID_weightDataOK' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgWeightDataOK;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgWeightDataOK);
							}
							break;
						}
						case dataID_bpRequest:{
						// ##############################################################################
							// SEQ1: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgOnly *pBpRequest = NULL;
							pBpRequest = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
							if ( pBpRequest == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: weightRequest.");
							#endif
							if ( getData(whoId_CB, (void *) pBpRequest) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pBpRequest);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_bpRequest.");
							#endif
							if (sendAck(&(pBpRequest->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pBpRequest);
								break;
							}
				// ** STEP 3: Forward weight request to communication board
							// CB should go into listening mode.
							pBpRequest->header.issuedBy 	= whoId_MB; // reuse msg from phone, but send from mainboard now
							pBpRequest->header.recipientId	= whoId_CB;
							pBpRequest->header.msgCount = ++msgCount;
							#if defined DEBUG_CBPA || defined SEQ1
								taskMessage("I", PREFIX, "Calling 'putData() to forward 'dataID_weightRequest' to CB.");
							#endif
							if ( putData((void *) pBpRequest) )
							{	// msg could not be forwarded to communication board.
								taskMessage("E", PREFIX, "Unable to write buffer to CB UART!");
								errMsg(errmsg_putDataFailed);
							}
				// ** STEP 4: Register msg for Ack to arrive from recipient"
							Qtoken.command = command_WaitOnMsgCBPA;
							Qtoken.pData   = (void *) pBpRequest;
							#if defined DEBUG_CBPA
								taskMessage("I", PREFIX, "Sending 'command_WaitOnMsg' with cnt: %d.", ((tdMsgOnly *)(Qtoken.pData))->header.msgCount);
							#endif
							if ( xQueueSend( qhREMINDERin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// CBPA reminder queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Reminder did not accept 'command_WaitOnMsg'!");
								errMsg(errmsg_QueueOfReminderTaskFull);
								sFree((void *)&pBpRequest);
							}
							break;
						}		
						case dataID_bpData:{
						// ##############################################################################
							// SEQ3: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgBpData *pMsgBpData = NULL;
							pMsgBpData = (tdMsgBpData *) pvPortMalloc(sizeof(tdMsgBpData));
							if ( pMsgBpData == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: BpData.");
							#endif
							pMsgBpData->bpData.TimeStamp = getTime();
							if ( getData(whoId_CB, (void *) pMsgBpData) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgBpData);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_bpData.");
							#endif
							if (sendAck(&(pMsgBpData->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pMsgBpData);
								break;
							}
				// ** STEP3: Forward buffer to dispatcher task
							#if defined DEBUG_CBPA || defined SEQ3
								taskMessage("I", PREFIX, "Forwarding 'dataID_bpData' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgBpData;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgBpData);
							}
							break;
						}						
						case dataID_changeWAKDStateFromTo: {
						// ##############################################################################
							// SEQ4: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgWakdStateFromTo *pMsgWakdStateFromTo = NULL;
							pMsgWakdStateFromTo = (tdMsgWakdStateFromTo *) pvPortMalloc(sizeof(tdMsgWakdStateFromTo)); // Will be freed by receiver task!
							if ( pMsgWakdStateFromTo == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ4
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: changeWAKDStateFromTo.");
							#endif
							if (getData(whoId_CB, (void *) pMsgWakdStateFromTo) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgWakdStateFromTo);
								break;
							}
				// ** STEP  2: Ack Buffer
							// We do not send the ACK here. The Task Flow Control will process this message.
							// If it is able to comply, it will return the ACK, otherwise it will generate NACK.
				// ** STEP3: Forward buffer to dispatcher task
							#if defined DEBUG_CBPA || defined SEQ4
								taskMessage("I", PREFIX, "Forwarding 'dataID_changeWAKDStateFromTo' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgWakdStateFromTo;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgWakdStateFromTo);
							}
							break;
						}
						case dataID_statusRequest:{
						// ##############################################################################
							// SEQ5: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgOnly *pMsgOnly = NULL;
							pMsgOnly = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
							if ( pMsgOnly == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ5
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: statusRequest.");
							#endif
							if (getData(whoId_CB, (void *) pMsgOnly) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgOnly);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ5
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_statusRequest.");
							#endif
							if (sendAck(&(pMsgOnly->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pMsgOnly);
								break;
							}
				// ** STEP3: Forward buffer to dispatcher task
							// This msg came from SP to MB. We now bring it to RTB -> from MB to RTB.
							// We need to exchange recipient and sender.
							pMsgOnly->header.issuedBy = whoId_MB;
							pMsgOnly->header.recipientId = whoId_RTB;
							#if defined DEBUG_CBPA || defined SEQ5
								taskMessage("I", PREFIX, "Forwarding 'dataID_statusRequest' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgOnly;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgOnly);
							}
							break;
						}
						case dataID_shutdown:{
						// ##############################################################################
							// SEQ==: http://is.gd/IJSmbK
				// ** STEP 1: Receive Buffer
							tdMsgOnly *pMsgOnly = NULL;
							pMsgOnly = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
							if ( pMsgOnly == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
								break;
							}
							#if defined DEBUG_CBPA || defined SEQ00
								taskMessage("I", PREFIX, "Calling 'getData()' to copy buffer from UART: shutdown.");
							#endif
							if (getData(whoId_CB, (void *) pMsgOnly) )
							{	// unable to read buffer from serial interface
								taskMessage("E", PREFIX, "Unable to read buffer from CB UART!");
								errMsg(errmsg_getDataFailed);
								sFree((void *)&pMsgOnly);
								break;
							}
				// ** STEP  2: Ack Buffer
							#if defined DEBUG_CBPA || defined SEQ5
								taskMessage("I", PREFIX, "Calling 'putData() to send Ack for received dataID_shutdown.");
							#endif
							if (sendAck(&(pMsgOnly->header)))
							{	// error in execution of sending Ack. Stop here.
								taskMessage("E", PREFIX, "Unable to send ACK msg to CB UART!");
								sFree((void *)&pMsgOnly);
								break;
							}
				// ** STEP3: Forward buffer to dispatcher task
							// This msg came from SP to MB. We now bring it to RTB -> from MB to RTB.
							// We need to exchange recipient and sender.
							pMsgOnly->header.issuedBy = whoId_MB;
							pMsgOnly->header.recipientId = whoId_RTB;
							#if defined DEBUG_CBPA || defined SEQ00
								taskMessage("I", PREFIX, "Forwarding 'dataID_shutdown' to Task Dispatcher.");
							#endif
							Qtoken.command = command_UseAtachedMsgHeader; // By looking into pData, recipient will know what to do with this.
							Qtoken.pData   = (void *) pMsgOnly;
							if ( xQueueSend( qhDISPin, &Qtoken, CBPA_BLOCKING) != pdPASS )
							{	// DISP queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_UseAtachedMsgHeader'!");
								errMsg(errmsg_QueueOfDispatcherTaskFull);
								sFree((void *)&pMsgOnly);
							}
							break;
						}
						default : {
						// ##############################################################################
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				// ##############################################################################
				default: {
				// ##############################################################################
					// The following function covers all possible commands with one big switch.
					// The reason for this implementation is as follows. The function switch does
					// not contain a 'default' so that compilation will generate a warning, whenever
					// a new command should have been forgotten to be coded.
					defaultCommandHandling(PREFIX, Qtoken.command);
					break;
				}
			}
		}
	}
}

uint8_t sendAck(tdHeader *pHeader)
{
	uint8_t err = 0;
	tdMsgOnly *pAck = NULL;
	pAck = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
	if ( pAck == NULL )
	{	// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
		err = 1;
	}
	pAck->header.dataId      = dataID_ack;
	pAck->header.issuedBy    = whoId_MB;
	pAck->header.msgCount    = pHeader->msgCount; // Acknowledge sent msg number
	pAck->header.msgSize     = sizeof(tdMsgOnly);
	pAck->header.recipientId = pHeader->issuedBy; // Acknowledge back to sender
	if ( putData((void *) pAck) )
	{	// msg could not be forwarded to communication board.
		errMsg(errmsg_putDataFailed);
		err = 1;
	}
	sFree((void *)&pAck);
	return(err);
}
