/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifdef LINUX
	#include "watchDog.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "watchDog.h"
#endif

#define PREFIX "OFFIS FreeRTOS task WD"

extern uint8_t heartBeatCBPA;
extern uint8_t heartBeatDISP;
extern uint8_t heartBeatRTBPA;
extern uint8_t heartBeatUI;
extern uint8_t heartBeatDS;
extern uint8_t heartBeatSCC;
extern uint8_t heartBeatFC;
//extern uint8_t heartBeatPMBPA0
//extern uint8_t heartBeatPMBPA1

// On system reset, dispatcher task will forward reset request to the attached HW: CB, RTB, PMB1 and PMB2.
// After sending the msg the task waits for the responsible abstraction tasks to transmit this request and than
// terminate. Dispatcher needs to wait for all these tasks to terminate.
// This flag marks with a value != 0 that the system is going down. This is also an important flag for the
// watchdog task. If system is going down, some tasks might have terminated already while other are still
// busy finishing. Since the already terminated tasks are no longer alive, watch dog would hard reset the system
// on detection. So system shut down is a special case for watch dog to know. In this case it should only check
// for dispatcher task to be still functional. Dispatcher is the last task to shout down. Before it does it
// will kill this watch dog task as a last action ... thereby triggering the HW reset.
extern uint8_t resetWaitingForTasksShutdown;

void tskWD( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting Watch Dog task with hook %d!", ((tdAllHandles *) pvParameters)->allTaskHandles.handleWD);

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
		// xPortGetFreeHeapSize only works with Heap_1 or Heap_2. We are using Heap_3 now.
		// So this functionality is no longer available.
		// // For SCC we also have a look at the heap that is still available
		// unsigned short heapLowWaterMark = xPortGetFreeHeapSize();
		// taskMessage("I", PREFIX, "Heap left to use is: %d", heapLowWaterMark);
	#endif

	// The dummy watchdog implemented in the Nephron HW simulation is counting 1 ms.
	// Setting wdCount to 2100 will thereby trigger watch dog after 2.1 second.
	*(REG32 wdCount) = 2100;
	// Any value for wdControl larger 0 will turn on watchdog.
	*(REG32 wdControl) = 1;

	// Initializing heart beat control variables. Any task of the Nephron+
	// application periodically increments its heart beat counter. Should it fail
	// to do so we assume that the task or scheduler crashed and and this
	// watchdog task will reset the entire system.

	uint8_t heartBeatCBPA_old  = heartBeatCBPA;
	uint8_t heartBeatDISP_old  = heartBeatDISP;
	uint8_t heartBeatRTBPA_old = heartBeatRTBPA;
	uint8_t heartBeatUI_old    = heartBeatUI;
	uint8_t heartBeatDS_old    = heartBeatDS;
	uint8_t heartBeatSCC_old   = heartBeatSCC;
	uint8_t heartBeatFC_old   = heartBeatFC;

	for( ;; ) {

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
			// xPortGetFreeHeapSize only works with Heap_1 or Heap_2. We are using Heap_3 now.
			// So this functionality is no longer available.
			// For SCC we also have a look at the heap that is still available
			// unsigned short heapLowWaterMarkNew = xPortGetFreeHeapSize();
			// if (heapLowWaterMark > heapLowWaterMarkNew) {
//				heapLowWaterMark = heapLowWaterMarkNew;
//				taskMessage("I", PREFIX, "Heap left to use is: %d", heapLowWaterMark);
//				if (heapLowWaterMark == 0)
//				{
//					taskMessage("E", PREFIX, "OUT OFF HEAP!!");
//				} else if (heapLowWaterMark < 32)
//				{
//					taskMessage("W", PREFIX, "Heap left to use is: %d", heapLowWaterMark);
//				}
//	}
		#endif

		vTaskDelay(2000/portTICK_RATE_MS);

		// Setting wdCount to 2100 will trigger watch dog after 2.1 seconds.
		// This has to be done again BEFORE the timer ran out. Otherwise HW reset is executed!
		// 2.1 seconds seems to be a very long time period if e.g. the system freezes and a bubble
		// is detected. But the real-time board autonomously stops the pumps without interaction
		// of the main board and just informs the mainboard.
		*(REG32 wdCount) = 2100;

		if (heartBeatDISP != heartBeatDISP_old)
		{	// heart beat of DISP task detected: GOOD
			// taskMessage("I", PREFIX, "DISP task is alive.");
			heartBeatDISP_old = heartBeatDISP;
		} else {
			taskMessage("E", PREFIX, "DISP task died! Restarting the system.");
			*(REG32 wdCount) = 0;
		}

		if (resetWaitingForTasksShutdown)
		{	// System is going down. Do not check any other task but dispatcher
			taskMessage("I", PREFIX, "System is going down!");
		} else {
			// System in normal operation (not going down) check all tasks
			if (heartBeatCBPA != heartBeatCBPA_old)
			{	// heart beat of CBPA task detected: GOOD
				// taskMessage("I", PREFIX, "CBPA task is alive.");
				heartBeatCBPA_old = heartBeatCBPA;
			} else {
				taskMessage("E", PREFIX, "CBPA task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
			if (heartBeatRTBPA != heartBeatRTBPA_old)
			{	// heart beat of RTBPA task detected: GOOD
				// taskMessage("I", PREFIX, "RTBPA task is alive.");
				heartBeatRTBPA_old = heartBeatRTBPA;
			} else {
				taskMessage("E", PREFIX, "RTBPA task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
			if (heartBeatUI != heartBeatUI_old)
			{	// heart beat of UI task detected: GOOD
				// taskMessage("I", PREFIX, "UI task is alive.");
				heartBeatUI_old = heartBeatUI;
			} else {
				taskMessage("E", PREFIX, "UI task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
			if (heartBeatDS != heartBeatDS_old)
			{	// heart beat of DS task detected: GOOD
				// taskMessage("I", PREFIX, "DS task is alive.");
				heartBeatDS_old = heartBeatDS;
			} else {
				taskMessage("E", PREFIX, "DS task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
			if (heartBeatSCC != heartBeatSCC_old)
			{	// heart beat of SCC task detected: GOOD
				// taskMessage("I", PREFIX, "SCC task is alive.");
				heartBeatSCC_old = heartBeatSCC;
			} else {
				taskMessage("E", PREFIX, "SCC task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
			if (heartBeatFC != heartBeatFC_old)
			{	// heart beat of FC task detected: GOOD
				// taskMessage("I", PREFIX, "FC task is alive.");
				heartBeatFC_old = heartBeatFC;
			} else {
				taskMessage("E", PREFIX, "FC task died! Restarting the system.");
				*(REG32 wdCount) = 0;
			}
		}


	}
}
