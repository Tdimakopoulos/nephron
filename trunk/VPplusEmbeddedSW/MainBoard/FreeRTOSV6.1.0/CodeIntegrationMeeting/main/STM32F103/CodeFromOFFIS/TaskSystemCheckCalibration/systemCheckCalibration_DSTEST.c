/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "systemCheckCalibration.h"

#include "unified_delete.h"
#include "filenames.h"
#include "csvparselib.h"

#define PREFIX "OFFIS FreeRTOS task SCC"

uint8_t getDataFromDS(tdDataId dataId2get, void *pDtaStructure, xQueueHandle *pQH);

uint8_t heartBeatSCC = 0;

void tskSCC( void *pvParameters )
{
	taskMessage("I", PREFIX, "Starting System Check and Calibration task!");
	
	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif
  
	// tdAllQueueHandles *pAllQueueHandles = &(((tdAllHandles *) (pvParameters))->allQueueHandles);
	xQueueHandle *qhDISPin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhSCCin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhSCCin;
 	tdQtoken Qtoken;
	
	// test hier	
	
	deleteFiles(FILE_PHYSIOLOGICALDATA, 371630, 5);
	deleteFiles(FILE_PHYSICALDATA, 371633, 5);
	deleteFiles(FILE_ACTUATORDATA, 371636, 5);
	deleteFiles(FILE_WEIGHTDATA, 371639, 5);
	deleteFiles(FILE_BPDATA, 371642, 5);
	deleteFiles(FILE_ECGDATA, 371644, 5);		

	int numberOfErrors = 0;
	int currentOkay = 0;
	char errorString[] ="____________________";
	int stringPos=0;
	{
		// tdMsgPhysiologicalData 
		// write test 
		
		tdMsgPhysiologicalData *WriteTestpMsgPhysiologicalData = NULL;
		WriteTestpMsgPhysiologicalData = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
		if ( WriteTestpMsgPhysiologicalData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
			
		tdPhysiologicalData	*pDb_1 = NULL;
		pDb_1 = (tdPhysiologicalData *) pvPortMalloc(sizeof(tdPhysiologicalData)); // Will be freed by receiver task!
		if ( pDb_1 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_1->TimeStamp = 1337891870;
		pDb_1->ECPDataI.Sodium = 43;
		pDb_1->ECPDataI.msOffset_Sodium = 44;
		pDb_1->ECPDataO.Sodium = 45;
		pDb_1->ECPDataO.msOffset_Sodium = 46;
		pDb_1->ECPDataI.Potassium = 47;
		pDb_1->ECPDataI.msOffset_Potassium = 48;
		pDb_1->ECPDataO.Potassium = 49;
		pDb_1->ECPDataO.msOffset_Potassium = 50;
		pDb_1->ECPDataI.Urea = 51;
		pDb_1->ECPDataI.msOffset_Urea = 52;
		pDb_1->ECPDataO.Urea = 53;
		pDb_1->ECPDataO.msOffset_Urea = 54;
		pDb_1->ECPDataI.pH = 55;
		pDb_1->ECPDataI.msOffset_pH = 56;
		pDb_1->ECPDataO.pH = 57;
		pDb_1->ECPDataO.msOffset_pH = 58;
		pDb_1->ECPDataI.Temperature = 59;
		pDb_1->ECPDataI.msOffset_Temperature = 60;
		pDb_1->ECPDataO.Temperature = 61;
		pDb_1->ECPDataO.msOffset_Temperature = 62;
		pDb_1->currentState = wakdStates_Regen1;
		
		memcpy(&(WriteTestpMsgPhysiologicalData->physiologicalData), pDb_1, sizeof(tdPhysiologicalData));
		WriteTestpMsgPhysiologicalData->header.dataId = dataID_physiologicalData;

		#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Forwarding TEST 'dataID_physiologicalData' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgPhysiologicalData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physiologicalData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&WriteTestpMsgPhysiologicalData);
		}
		
		// read test 
		
		tdMsgPhysiologicalData *ReadTestpMsgPhysiologicalData = NULL;
		ReadTestpMsgPhysiologicalData = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
		if ( ReadTestpMsgPhysiologicalData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgPhysiologicalData;
		ReadTestpMsgPhysiologicalData->header.dataId = dataID_physiologicalData;
		ReadTestpMsgPhysiologicalData->header.issuedBy = whoId_MB;
		
		ReadTestpMsgPhysiologicalData->physiologicalData.TimeStamp = pDb_1->TimeStamp;

		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physiologicalData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgPhysiologicalData);
		}
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X 
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_physiologicalData))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		
		if(pDb_1->currentState != ReadTestpMsgPhysiologicalData->physiologicalData.currentState || pDb_1->ECPDataI.msOffset_Urea != ReadTestpMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Urea || pDb_1->ECPDataI.Sodium != ReadTestpMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium || pDb_1->ECPDataI.pH != ReadTestpMsgPhysiologicalData->physiologicalData.ECPDataI.pH )
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_physiologicalData: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&ReadTestpMsgPhysiologicalData);
		sFree((void *)&pDb_1);
		#ifdef DEBUG_STACK
			taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
		#endif
	} 
	{
		// tdPhysicalsensorData 
		// write test 
		
		tdMsgPhysicalsensorData *WriteTestpMsgPhysicalsensorData = NULL;
		WriteTestpMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
		if ( WriteTestpMsgPhysicalsensorData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		#if defined DEBUG_RTBPA || defined SEQ6
			taskMessage("I", PREFIX, "Copy static buffer from UART: PhysicalsensorData.");
		#endif
		
		tdPhysicalsensorData	*pDb_2 = NULL;
		pDb_2 = (tdPhysicalsensorData *) pvPortMalloc(sizeof(tdPhysicalsensorData)); // Will be freed by receiver task!
		if ( pDb_2 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_2->TimeStamp = 1337891870;
		pDb_2->PressureFCI.Pressure = 44;
		pDb_2->PressureFCI.msOffset_Pressure = 45;
		pDb_2->PressureFCO.Pressure = 46;
		pDb_2->PressureFCO.msOffset_Pressure = 47;
		pDb_2->PressureBCI.Pressure = 48;
		pDb_2->PressureBCI.msOffset_Pressure = 49;
		pDb_2->PressureBCO.Pressure = 50;
		pDb_2->PressureBCO.msOffset_Pressure = 51;
		pDb_2->TemperatureInOut.TemperatureInlet_Value = 52;
		pDb_2->TemperatureInOut.TemperatureOutlet_Value = 53;
		pDb_2->TemperatureInOut.msOffset_Temperature = 54;
		pDb_2->CSENS1Data.Cond_FCR = 55;
		pDb_2->CSENS1Data.Cond_FCQ = 56;
		pDb_2->CSENS1Data.Cond_PT1000 = 57;
		pDb_2->CSENS1Data.msOffset_Cond = 58;

		
		memcpy(&(WriteTestpMsgPhysicalsensorData->physicalsensorData), pDb_2, sizeof(tdPhysicalsensorData));
		WriteTestpMsgPhysicalsensorData->header.dataId = dataID_physicalData;

		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_physicalData' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgPhysicalsensorData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physicalData'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&WriteTestpMsgPhysicalsensorData);

		}
		
		// read test
		tdMsgPhysicalsensorData *ReadTestpMsgPhysicalsensorData = NULL;
		ReadTestpMsgPhysicalsensorData = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
		if ( ReadTestpMsgPhysicalsensorData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgPhysicalsensorData;

		ReadTestpMsgPhysicalsensorData->header.dataId = dataID_physicalData;
		ReadTestpMsgPhysicalsensorData->header.issuedBy = whoId_MB;
		ReadTestpMsgPhysicalsensorData->physicalsensorData.TimeStamp = pDb_2->TimeStamp;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_physicalData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgPhysicalsensorData);
		}
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X 
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_physicalData))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(pDb_2->PressureBCO.Pressure != ReadTestpMsgPhysicalsensorData->physicalsensorData.PressureBCO.Pressure || pDb_2->CSENS1Data.Cond_FCQ != ReadTestpMsgPhysicalsensorData->physicalsensorData.CSENS1Data.Cond_FCQ || pDb_2->TemperatureInOut.msOffset_Temperature != ReadTestpMsgPhysicalsensorData->physicalsensorData.TemperatureInOut.msOffset_Temperature || pDb_2->PressureFCI.msOffset_Pressure != ReadTestpMsgPhysicalsensorData->physicalsensorData.PressureFCI.msOffset_Pressure )
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_physicalData: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&ReadTestpMsgPhysicalsensorData);	
		sFree((void *)&pDb_2);
		#ifdef DEBUG_STACK
			taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
		#endif
	} 
	{
		// tdActuatorData 
		// write test 
		
		tdMsgActuatorData *WriteTestpMsgActuatorData = NULL;
		WriteTestpMsgActuatorData = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
		if ( WriteTestpMsgActuatorData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		tdActuatorData	*pDb_3 = NULL;
		pDb_3 = (tdActuatorData *) pvPortMalloc(sizeof(tdActuatorData)); // Will be freed by receiver task!
		if ( pDb_3 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_3->TimeStamp = 1337891870;
		pDb_3->MultiSwitchBIData.SwitchONnOFF = 50;
		pDb_3->MultiSwitchBIData.Position = 51;
		pDb_3->MultiSwitchBIData.msOffset_Position = 52;
		pDb_3->MultiSwitchBOData.SwitchONnOFF = 53;
		pDb_3->MultiSwitchBOData.Position = 54;
		pDb_3->MultiSwitchBOData.msOffset_Position = 55;
		pDb_3->MultiSwitchBUData.SwitchONnOFF = 56;
		pDb_3->MultiSwitchBUData.Position = 57;
		pDb_3->MultiSwitchBUData.msOffset_Position = 58;
		pDb_3->BLPumpData.SwitchONnOFF = 59;
		pDb_3->BLPumpData.Direction = 60;
		pDb_3->BLPumpData.msOffset_Direction = 61;
		pDb_3->BLPumpData.Speed = 62;
		pDb_3->BLPumpData.msOffset_Speed = 63;
		pDb_3->BLPumpData.FlowReference = 64;
		pDb_3->BLPumpData.msOffset_FlowReference = 65;
		pDb_3->BLPumpData.Flow = 66;
		pDb_3->BLPumpData.msOffset_Flow = 67;
		pDb_3->BLPumpData.Current = 68;
		pDb_3->BLPumpData.msOffset_Current = 69;
		pDb_3->FLPumpData.SwitchONnOFF = 70;
		pDb_3->FLPumpData.Direction = 71;
		pDb_3->FLPumpData.msOffset_Direction = 72;
		pDb_3->FLPumpData.Speed = 73;
		pDb_3->FLPumpData.msOffset_Speed = 74;
		pDb_3->FLPumpData.FlowReference = 75;
		pDb_3->FLPumpData.msOffset_FlowReference = 76;
		pDb_3->FLPumpData.Flow = 77;
		pDb_3->FLPumpData.msOffset_Flow = 78;
		pDb_3->FLPumpData.Current = 79;
		pDb_3->FLPumpData.msOffset_Current = 80;
		pDb_3->PolarizationData.SwitchONnOFF = 81;
		pDb_3->PolarizationData.Direction = 82;
		pDb_3->PolarizationData.msOffset_Direction = 83;
		pDb_3->PolarizationData.VoltageReference = 84;
		pDb_3->PolarizationData.msOffset_VoltageReference = 85;
		pDb_3->PolarizationData.Voltage = 86;
		pDb_3->PolarizationData.msOffset_Voltage = 87;
		
		memcpy(&(WriteTestpMsgActuatorData->actuatorData), pDb_3, sizeof(tdActuatorData));
		WriteTestpMsgActuatorData->header.dataId = dataID_actuatorData;

		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_actuatorData' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgActuatorData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_actuatorData'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&WriteTestpMsgActuatorData);

		}
		
		// read test 
		tdMsgActuatorData *ReadTestpMsgActuatorData = NULL;
		ReadTestpMsgActuatorData = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
		if ( ReadTestpMsgActuatorData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgActuatorData;
		// memcpy(&(ReadTestpMsgActuatorData->actuatorData), &db_3_test, sizeof(tdActuatorData));
		ReadTestpMsgActuatorData->actuatorData.TimeStamp = pDb_3->TimeStamp;
		ReadTestpMsgActuatorData->header.dataId = dataID_actuatorData;
		ReadTestpMsgActuatorData->header.issuedBy = whoId_MB;

		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_actuatorData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgActuatorData);
		}
		taskMessage("I", PREFIX, "sent command_RequestDatafromDS");
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_actuatorData))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 		
		if(pDb_3->FLPumpData.msOffset_FlowReference != ReadTestpMsgActuatorData->actuatorData.FLPumpData.msOffset_FlowReference || pDb_3->MultiSwitchBUData.SwitchONnOFF != ReadTestpMsgActuatorData->actuatorData.MultiSwitchBUData.SwitchONnOFF || pDb_3->PolarizationData.msOffset_Direction != ReadTestpMsgActuatorData->actuatorData.PolarizationData.msOffset_Direction || pDb_3->BLPumpData.Flow != ReadTestpMsgActuatorData->actuatorData.BLPumpData.Flow )
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		
		stringPos= stringPos+1;
		taskMessage("I", PREFIX, "Comparing dataID_actuatorData: %s", currentOkay ? "OK" : "DIFFER");
		// sFree((void *)&WriteTestpMsgActuatorData);
		sFree((void *)&ReadTestpMsgActuatorData);
		sFree((void *)&pDb_3);
	}  // */
	{
		// tdWeightMeasure 
		// write test 
	
		tdMsgWeightData *WriteTestpMsgWeightData = NULL;
		WriteTestpMsgWeightData = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData)); // Will be freed by receiver task!
		if ( WriteTestpMsgWeightData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		
		tdWeightMeasure	*pDb_4 = NULL;
		pDb_4 = (tdWeightMeasure *) pvPortMalloc(sizeof(tdWeightMeasure)); // Will be freed by receiver task!
		if ( pDb_4 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_4->Weight = 337;
		pDb_4->BodyFat = 35;
		pDb_4->WSBatteryLevel = 334;
		pDb_4->WSStatus = 333;
		pDb_4->TimeStamp = 1337891870;

		memcpy(&(WriteTestpMsgWeightData->weightData), pDb_4, sizeof(tdWeightMeasure));
		WriteTestpMsgWeightData->header.dataId = dataID_weightDataOK;
			

		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_weightDataOK' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgWeightData;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_weightDataOK'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&WriteTestpMsgWeightData);
		}
	
		// read test 
		tdMsgWeightData *ReadTestpMsgWeightData = NULL;
		ReadTestpMsgWeightData = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData)); // Will be freed by receiver task!
		if ( ReadTestpMsgWeightData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgWeightData;
		ReadTestpMsgWeightData->weightData.TimeStamp=pDb_4->TimeStamp;
		ReadTestpMsgWeightData->header.dataId = dataID_weightDataOK;
		ReadTestpMsgWeightData->header.issuedBy = whoId_MB;

		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_weightDataOK'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgWeightData);
		}
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_weightDataOK))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(pDb_4->Weight != ReadTestpMsgWeightData->weightData.Weight || pDb_4->BodyFat != ReadTestpMsgWeightData->weightData.BodyFat || pDb_4->WSBatteryLevel != ReadTestpMsgWeightData->weightData.WSBatteryLevel || pDb_4->WSStatus != ReadTestpMsgWeightData->weightData.WSStatus || pDb_4->TimeStamp != ReadTestpMsgWeightData->weightData.TimeStamp)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_weightDataOK: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&ReadTestpMsgWeightData);
		sFree((void *)&pDb_4);
	} // */
	{
		// tdBpData 
		// write test 
		
		tdMsgBpData *WriteTestpMsgBpData = NULL;
		WriteTestpMsgBpData = (tdMsgBpData *) pvPortMalloc(sizeof(tdMsgBpData)); // Will be freed by receiver task!
		if ( WriteTestpMsgBpData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		#if defined DEBUG_RTBPA || defined SEQ6
			taskMessage("I", PREFIX, "Copy static buffer from UART: BpData.");
		#endif
		
		tdBpData *pDb_5 = NULL;
		pDb_5 = (tdBpData *) pvPortMalloc(sizeof(tdBpData)); // Will be freed by receiver task!
		if ( pDb_5 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_5->systolic=220;
		pDb_5->diastolic=25;
		pDb_5->heartRate=42;
		pDb_5->TimeStamp=1337891870;
		
		memcpy(&(WriteTestpMsgBpData->bpData), pDb_5, sizeof(tdBpData));
		WriteTestpMsgBpData->header.dataId = dataID_bpData;

		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_bpData' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgBpData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_bpData'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&WriteTestpMsgBpData);
		}

		// read test 
		tdMsgBpData *ReadTestpMsgBpData = NULL;
		ReadTestpMsgBpData = (tdMsgBpData *) pvPortMalloc(sizeof(tdMsgBpData)); // Will be freed by receiver task!
		if ( ReadTestpMsgBpData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgBpData;
		ReadTestpMsgBpData->header.dataId = dataID_bpData;
		ReadTestpMsgBpData->header.issuedBy = whoId_MB;
		ReadTestpMsgBpData->bpData.TimeStamp = pDb_5->TimeStamp;

		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_bpData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgBpData);
		}

		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X 
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_bpData))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(pDb_5->systolic != ReadTestpMsgBpData->bpData.systolic || pDb_5->diastolic != ReadTestpMsgBpData->bpData.diastolic || pDb_5->heartRate != ReadTestpMsgBpData->bpData.heartRate || pDb_5->TimeStamp != ReadTestpMsgBpData->bpData.TimeStamp)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_bpData: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&ReadTestpMsgBpData);
		sFree((void *)&pDb_5);
	} // */
	{
		// tdEcgData 
		// write test 
		
		tdMsgEcgData *WriteTestpMsgEcgData = NULL;
		WriteTestpMsgEcgData = (tdMsgEcgData *) pvPortMalloc(sizeof(tdMsgEcgData)); // Will be freed by receiver task!
		if ( WriteTestpMsgEcgData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		tdEcgData	*pDb_6 = NULL;
		pDb_6 = (tdEcgData *) pvPortMalloc(sizeof(tdEcgData)); // Will be freed by receiver task!
		if ( pDb_6 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_6->heartRate=44;
		pDb_6->respirationRate=12;
		pDb_6->TimeStamp = 1337891870;
		
		memcpy(&(WriteTestpMsgEcgData->ecgData), pDb_6, sizeof(tdEcgData));
		WriteTestpMsgEcgData->header.dataId = dataID_EcgData;

		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_EcgData' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) WriteTestpMsgEcgData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_EcgData'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&WriteTestpMsgEcgData);

		}
		
		// read test 
		tdMsgEcgData *ReadTestpMsgEcgData = NULL;
		ReadTestpMsgEcgData = (tdMsgEcgData *) pvPortMalloc(sizeof(tdMsgEcgData)); // Will be freed by receiver task!
		if ( ReadTestpMsgEcgData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) ReadTestpMsgEcgData;
		// memcpy(&(ReadTestpMsgEcgData->ecgData), &db_6_test, sizeof(tdEcgData));
		
		ReadTestpMsgEcgData->ecgData.TimeStamp = pDb_6->TimeStamp;
		ReadTestpMsgEcgData->header.dataId = dataID_EcgData;
		ReadTestpMsgEcgData->header.issuedBy = whoId_MB;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_EcgData'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&ReadTestpMsgEcgData);
		}	
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_EcgData))
		{
			#if defined DEBUG_SCC || defined SEQ6
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(pDb_6->heartRate != ReadTestpMsgEcgData->ecgData.heartRate ||pDb_6->respirationRate != ReadTestpMsgEcgData->ecgData.respirationRate || pDb_6->TimeStamp != ReadTestpMsgEcgData->ecgData.TimeStamp)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_EcgData: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&ReadTestpMsgEcgData);
		sFree((void *)&pDb_6);
		#ifdef DEBUG_STACK
			taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
		#endif
	} // */


	{
		// tdWAKDAllStateConfigure 
		
		FILE* inFile_sconf = fopen(FILE_WAKDALLSTATECONFIGURE, "rb");
		FILE* outFile_sconf = fopen("tempfileDSTEST_W_allstateconf", "wb");
		int ch;
		while( (ch = fgetc(inFile_sconf)) != EOF) fputc(ch, outFile_sconf);
		fclose(inFile_sconf);
		fclose(outFile_sconf);

		FILE* inFile_pp = fopen(FILE_PATIENTPROFILE, "rb");
		FILE* outFile_pp = fopen("tempfileDSTEST_Patprof", "wb");
		while( (ch = fgetc(inFile_pp)) != EOF) fputc(ch, outFile_pp);
		fclose(inFile_pp);
		fclose(outFile_pp);

		
		// write test 
		tdMsgWAKDAllStateConfigure *pWriteTestWAKDAllStateConfigure = NULL;
		pWriteTestWAKDAllStateConfigure = (tdMsgWAKDAllStateConfigure *) pvPortMalloc(sizeof(tdMsgWAKDAllStateConfigure)); // Will be freed by receiver task!
		if ( pWriteTestWAKDAllStateConfigure == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}

		// tdMsgPatientProfileData *pWriteTestWAKDAllStateConfigurePP = NULL;
		// pWriteTestWAKDAllStateConfigurePP = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData)); // Will be freed by receiver task!
		// if ( pWriteTestWAKDAllStateConfigurePP == NULL )
		// {	// unable to allocate memory
			// taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			// errMsg(errmsg_pvPortMallocFailed);
		// }

		tdWAKDAllStateConfigure	*pDb_WAKDAllStateConfigure = NULL;
		pDb_WAKDAllStateConfigure = (tdWAKDAllStateConfigure *) pvPortMalloc(sizeof(tdWAKDAllStateConfigure)); // Will be freed by receiver task!
		if ( pDb_WAKDAllStateConfigure == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_WAKDAllStateConfigure->stateAllStoppedConfig.defSpeedBp_mlPmin=0;
		pDb_WAKDAllStateConfigure->stateAllStoppedConfig.defSpeedFp_mlPmin=0;
		pDb_WAKDAllStateConfigure->stateAllStoppedConfig.defPol_V=0;
		pDb_WAKDAllStateConfigure->stateAllStoppedConfig.direction=1;
		pDb_WAKDAllStateConfigure->stateAllStoppedConfig.duration_sec=0;
		pDb_WAKDAllStateConfigure->stateDialysisConfig.defSpeedBp_mlPmin=100;
		pDb_WAKDAllStateConfigure->stateDialysisConfig.defSpeedFp_mlPmin=50;
		pDb_WAKDAllStateConfigure->stateDialysisConfig.defPol_V=4;
		pDb_WAKDAllStateConfigure->stateDialysisConfig.direction=1;
		pDb_WAKDAllStateConfigure->stateDialysisConfig.duration_sec=5*60*60; 
		pDb_WAKDAllStateConfigure->stateRegen1Config.defSpeedBp_mlPmin=10;
		pDb_WAKDAllStateConfigure->stateRegen1Config.defSpeedFp_mlPmin=50;
		pDb_WAKDAllStateConfigure->stateRegen1Config.defPol_V=4;
		pDb_WAKDAllStateConfigure->stateRegen1Config.direction=1;
		pDb_WAKDAllStateConfigure->stateRegen1Config.duration_sec=5*60;  // 5 Minutes
		pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.defSpeedBp_mlPmin=11;
		pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.defSpeedFp_mlPmin=21;
		pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.defPol_V=0;
		pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.direction=1;
		pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.duration_sec=30*60; ;  // 30 Minutes
		pDb_WAKDAllStateConfigure->stateRegen2Config.defSpeedBp_mlPmin=10;
		pDb_WAKDAllStateConfigure->stateRegen2Config.defSpeedFp_mlPmin=50;
		pDb_WAKDAllStateConfigure->stateRegen2Config.defPol_V=4;
		pDb_WAKDAllStateConfigure->stateRegen2Config.direction=0;
		pDb_WAKDAllStateConfigure->stateRegen2Config.duration_sec=5*60; ;  // 5 Minutes
		pDb_WAKDAllStateConfigure->stateMaintenanceConfig.defSpeedBp_mlPmin=0;
		pDb_WAKDAllStateConfigure->stateMaintenanceConfig.defSpeedFp_mlPmin=0;
		pDb_WAKDAllStateConfigure->stateMaintenanceConfig.defPol_V=0;
		pDb_WAKDAllStateConfigure->stateMaintenanceConfig.direction=0;
		pDb_WAKDAllStateConfigure->stateMaintenanceConfig.duration_sec=0; 
		pDb_WAKDAllStateConfigure->stateNoDialysateConfig.defSpeedBp_mlPmin=100;
		pDb_WAKDAllStateConfigure->stateNoDialysateConfig.defSpeedFp_mlPmin=0;
		pDb_WAKDAllStateConfigure->stateNoDialysateConfig.defPol_V=0;
		pDb_WAKDAllStateConfigure->stateNoDialysateConfig.direction=1;
		pDb_WAKDAllStateConfigure->stateNoDialysateConfig.duration_sec=0; 
		// now ALWAYS! call unified_write for patientProfile, since it is member of WAKDAllStateConfigure
		strcpy(pDb_WAKDAllStateConfigure->patientProfile.name,"Erika Mustermann");
		pDb_WAKDAllStateConfigure->patientProfile.gender='f';
		pDb_WAKDAllStateConfigure->patientProfile.measureTimeHours=8;
		pDb_WAKDAllStateConfigure->patientProfile.measureTimeMinutes=33;
		pDb_WAKDAllStateConfigure->patientProfile.wghtCtlTarget=75000; //75 Kg
		pDb_WAKDAllStateConfigure->patientProfile.wghtCtlLastMeasurement=75200;
		pDb_WAKDAllStateConfigure->patientProfile.wghtCtlDefRemPerDay=1200;
		pDb_WAKDAllStateConfigure->patientProfile.kCtlTarget=4.1;
		pDb_WAKDAllStateConfigure->patientProfile.kCtlLastMeasurement=4.3;
		pDb_WAKDAllStateConfigure->patientProfile.kCtlDefRemPerDay=6; // mmols (absolute) to be removed each day
		pDb_WAKDAllStateConfigure->patientProfile.urCtlTarget=10;
		pDb_WAKDAllStateConfigure->patientProfile.urCtlLastMeasurement=12;
		pDb_WAKDAllStateConfigure->patientProfile.urCtlDefRemPerDay=60;
		pDb_WAKDAllStateConfigure->patientProfile.minDialPlasFlow = 2;
		pDb_WAKDAllStateConfigure->patientProfile.maxDialPlasFlow = 70;
		pDb_WAKDAllStateConfigure->patientProfile.minDialVoltage = 1.5;
		pDb_WAKDAllStateConfigure->patientProfile.maxDialVoltage = 4.0;

		memcpy(&(pWriteTestWAKDAllStateConfigure->WAKDAllStateConfigure), pDb_WAKDAllStateConfigure, sizeof(tdWAKDAllStateConfigure));
		// memcpy(&(pWriteTestWAKDAllStateConfigurePP->patientProfile), pDb_WAKDAllStateConfigure->patientProfile, sizeof(tdWAKDAllStateConfigure));
		pWriteTestWAKDAllStateConfigure->header.dataId = dataID_WAKDAllStateConfigure; //dataID_configureState;
		pWriteTestWAKDAllStateConfigure->header.recipientId = whoId_MB;
		pWriteTestWAKDAllStateConfigure->header.issuedBy = whoId_MB;
		// pWriteTestWAKDAllStateConfigurePP->header.dataId = dataID_PatientProfile; //dataID_configureState;
		// pWriteTestWAKDAllStateConfigurePP->header.recipientId = whoId_MB;
		// pWriteTestWAKDAllStateConfigurePP->header.issuedBy = whoId_MB;
		
		#if defined DEBUG_SCC
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_WAKDAllStateConfigure' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) pWriteTestWAKDAllStateConfigure;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_WAKDAllStateConfigure'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&pWriteTestWAKDAllStateConfigure);
		}
		// #if defined DEBUG_SCC
		// taskMessage("D", PREFIX, "Forwarding TEST 'dataID_PatientProfile' to Dispatcher Task.");
		// #endif
		// Qtoken.command	= command_UseAtachedMsgHeader;
		// Qtoken.pData	= (void *) pWriteTestWAKDAllStateConfigurePP;
		// if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		// {	// DISP queue did not accept data. Free message and send error.
		// taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_PatientProfile'!");
		// errMsg(errmsg_QueueOfDispatcherTaskFull);
		// sFree((void *)&pWriteTestWAKDAllStateConfigure);
		// }
		
		// read test 
		tdMsgWAKDAllStateConfigure *pReadTestWAKDAllStateConfigure = NULL;
		pReadTestWAKDAllStateConfigure = (tdMsgWAKDAllStateConfigure *) pvPortMalloc(sizeof(tdMsgWAKDAllStateConfigure)); // Will be freed by receiver task!
		if ( pReadTestWAKDAllStateConfigure == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) pReadTestWAKDAllStateConfigure;
		
		pReadTestWAKDAllStateConfigure->header.dataId = dataID_WAKDAllStateConfigure;
		pReadTestWAKDAllStateConfigure->header.issuedBy = whoId_MB;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_WAKDAllStateConfigure'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pReadTestWAKDAllStateConfigure);
		}	
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_WAKDAllStateConfigure))
		{
			#if defined DEBUG_SCC
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
			
		// read test  PART2 for PatientProfile
	/*	{
			tdMsgPatientProfileData *pReadTestWAKDAllStateConfigurePP = NULL;
			pReadTestWAKDAllStateConfigurePP = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData)); // Will be freed by receiver task!
			if ( pReadTestWAKDAllStateConfigurePP == NULL )
			{	// unable to allocate memory
				taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
				errMsg(errmsg_pvPortMallocFailed);
			}
			
			Qtoken.command	= command_RequestDatafromDS;
			Qtoken.pData	= (void *) pReadTestWAKDAllStateConfigurePP;
			
			pReadTestWAKDAllStateConfigurePP->header.dataId = dataID_PatientProfile;
			pReadTestWAKDAllStateConfigurePP->header.issuedBy = whoId_MB;
			
			if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
			{	// DISP queue did not accept data. Free message and send error.
				taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_PatientProfile'!");
				errMsg(errmsg_QueueOfDispatcherTaskFull);
				sFree((void *)&pReadTestWAKDAllStateConfigurePP);
			}	
			
			//	loop while nothing is in the queue or something is in the queue 
			//	and the command differs from command_RequestDatafromDSReady
			//	and the dataId differs from dataID_PatientProfile  
			while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
					|| (Qtoken.command != command_RequestDatafromDSReady)
					|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_PatientProfile))
			{
				#if defined DEBUG_SCC
				taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
				#endif
				sFree(&(Qtoken.pData));
				++heartBeatSCC;
			}
		} */
		
		// comparing results 
		if(	pDb_WAKDAllStateConfigure->stateDialysisConfig.defSpeedFp_mlPmin != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.stateDialysisConfig.defSpeedFp_mlPmin ||
			pDb_WAKDAllStateConfigure->stateUltrafiltrationConfig.defPol_V != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.stateUltrafiltrationConfig.defPol_V   ||
			pDb_WAKDAllStateConfigure->stateRegen1Config.defSpeedFp_mlPmin != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.stateRegen1Config.defSpeedFp_mlPmin ||
			pDb_WAKDAllStateConfigure->stateRegen2Config.direction != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.stateRegen2Config.direction ||
			pDb_WAKDAllStateConfigure->stateNoDialysateConfig.defSpeedBp_mlPmin != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.stateNoDialysateConfig.defSpeedBp_mlPmin ||
			pDb_WAKDAllStateConfigure->patientProfile.urCtlDefRemPerDay != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.patientProfile.urCtlDefRemPerDay ||
			pDb_WAKDAllStateConfigure->patientProfile.minDialVoltage != pReadTestWAKDAllStateConfigure->WAKDAllStateConfigure.patientProfile.minDialVoltage	)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_WAKDAllStateConfigure: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&pReadTestWAKDAllStateConfigure);
		sFree((void *)&pDb_WAKDAllStateConfigure);
		
		inFile_sconf = fopen("tempfileDSTEST_W_allstateconf", "rb");
		outFile_sconf = fopen(FILE_WAKDALLSTATECONFIGURE, "wb");
		//int ch;
		while( (ch = fgetc(inFile_sconf)) != EOF) fputc(ch, outFile_sconf);
		fclose(inFile_sconf);
		fclose(outFile_sconf);
		remove( "tempfileDSTEST_W_allstateconf");
		
		inFile_pp = fopen("tempfileDSTEST_Patprof", "rb");
		outFile_pp = fopen(FILE_PATIENTPROFILE, "wb");
		// int ch;
		while( (ch = fgetc(inFile_pp)) != EOF) fputc(ch, outFile_pp);
		fclose(inFile_pp);
		fclose(outFile_pp);
		remove( "tempfileDSTEST_Patprof");
	} // */
	{
		FILE* inFile = fopen(FILE_WAKDALLSTATECONFIGURE, "rb");
		FILE* outFile = fopen("tempfileDSTEST_wakdAstates", "wb");
		int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);
		
		// tdWAKDStateConfigurationParameters
		// write test 
		tdMsgWAKDStateConfigurationParameters *pWriteTestWAKDStateConfigurationParameters = NULL;
		pWriteTestWAKDStateConfigurationParameters = (tdMsgWAKDStateConfigurationParameters *) pvPortMalloc(sizeof(tdMsgWAKDStateConfigurationParameters)); // Will be freed by receiver task!
		if ( pWriteTestWAKDStateConfigurationParameters == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}

		tdWAKDStateConfigurationParameters	*pDb_WAKDStateConfigurationParameters = NULL;
		pDb_WAKDStateConfigurationParameters = (tdWAKDStateConfigurationParameters *) pvPortMalloc(sizeof(tdWAKDStateConfigurationParameters)); // Will be freed by receiver task!
		if ( pDb_WAKDStateConfigurationParameters == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_WAKDStateConfigurationParameters->thisState = wakdStates_AllStopped;
		pDb_WAKDStateConfigurationParameters->defSpeedBp_mlPmin=0;
		pDb_WAKDStateConfigurationParameters->defSpeedFp_mlPmin=0;
		pDb_WAKDStateConfigurationParameters->defPol_V=0;
		pDb_WAKDStateConfigurationParameters->direction=1;
		pDb_WAKDStateConfigurationParameters->duration_sec=0;
		
		memcpy(&(pWriteTestWAKDStateConfigurationParameters->WAKDStateConfigurationParameters), pDb_WAKDStateConfigurationParameters, sizeof(tdWAKDStateConfigurationParameters));
		pWriteTestWAKDStateConfigurationParameters->header.dataId = dataID_configureState; //dataID_configureState;
		pWriteTestWAKDStateConfigurationParameters->header.recipientId = whoId_MB;
		pWriteTestWAKDStateConfigurationParameters->header.issuedBy = whoId_MB;
		
		#if defined DEBUG_SCC 
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_configureState' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) pWriteTestWAKDStateConfigurationParameters;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_configureState'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&pWriteTestWAKDStateConfigurationParameters);
		}
		
		// read test 
		tdMsgWAKDStateConfigurationParameters *pReadTestMsgWAKDStateConfigurationParameters = NULL;
		pReadTestMsgWAKDStateConfigurationParameters = (tdMsgWAKDStateConfigurationParameters *) pvPortMalloc(sizeof(tdMsgWAKDStateConfigurationParameters)); // Will be freed by receiver task!
		if ( pReadTestMsgWAKDStateConfigurationParameters == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) pReadTestMsgWAKDStateConfigurationParameters;
		
		pReadTestMsgWAKDStateConfigurationParameters->header.dataId = dataID_configureState;
		pReadTestMsgWAKDStateConfigurationParameters->header.issuedBy = whoId_MB;
		pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState  = pDb_WAKDStateConfigurationParameters->thisState;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_RequestDatafromDS' with 'dataID_configureState'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pReadTestMsgWAKDStateConfigurationParameters);
		}	
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_configureState))
		{
			#if defined DEBUG_SCC
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(	pDb_WAKDStateConfigurationParameters->thisState != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.thisState ||
			pDb_WAKDStateConfigurationParameters->defSpeedBp_mlPmin != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin  ||
			pDb_WAKDStateConfigurationParameters->defSpeedFp_mlPmin != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin ||
			pDb_WAKDStateConfigurationParameters->defPol_V != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V ||
			pDb_WAKDStateConfigurationParameters->direction != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction ||
			pDb_WAKDStateConfigurationParameters->duration_sec != pReadTestMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_configureState: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&pReadTestMsgWAKDStateConfigurationParameters);
		sFree((void *)&pDb_WAKDStateConfigurationParameters);

		inFile = fopen("tempfileDSTEST_wakdAstates", "rb");
		outFile = fopen(FILE_WAKDALLSTATECONFIGURE, "wb");
		// int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);
		remove( "tempfileDSTEST_wakdAstates");

	} // */
	{
		FILE* inFile = fopen(FILE_PATIENTPROFILE, "rb");
		FILE* outFile = fopen("tempfileDSTEST_paatient", "wb");
		int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);
		// tdPatientProfile
		// write test 
		tdMsgPatientProfileData *pWriteTestMsgPatientProfileData = NULL;
		pWriteTestMsgPatientProfileData = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData)); // Will be freed by receiver task!
		if ( pWriteTestMsgPatientProfileData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}

		tdPatientProfile	*pDb_PatientProfile = NULL;
		pDb_PatientProfile = (tdPatientProfile *) pvPortMalloc(sizeof(tdPatientProfile)); // Will be freed by receiver task!
		if ( pDb_PatientProfile == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		strcpy(pDb_PatientProfile->name,"Erika Mustermann");
		pDb_PatientProfile->gender='f';
		pDb_PatientProfile->measureTimeHours=8;
		pDb_PatientProfile->measureTimeMinutes=33;
		pDb_PatientProfile->wghtCtlTarget=75000; //75 Kg
		pDb_PatientProfile->wghtCtlLastMeasurement=75200;
		pDb_PatientProfile->wghtCtlDefRemPerDay=1200;
		pDb_PatientProfile->kCtlTarget=4.1;
		pDb_PatientProfile->kCtlLastMeasurement=4.3;
		pDb_PatientProfile->kCtlDefRemPerDay=6; // mmols (absolute) to be removed each day
		pDb_PatientProfile->urCtlTarget=10;
		pDb_PatientProfile->urCtlLastMeasurement=12;
		pDb_PatientProfile->urCtlDefRemPerDay=60;
		pDb_PatientProfile->minDialPlasFlow = 2;
		pDb_PatientProfile->maxDialPlasFlow = 70;
		pDb_PatientProfile->minDialVoltage = 1.5;
		pDb_PatientProfile->maxDialVoltage = 4.0;

		memcpy(&(pWriteTestMsgPatientProfileData->patientProfile), pDb_PatientProfile, sizeof(tdPatientProfile));
		pWriteTestMsgPatientProfileData->header.dataId = dataID_PatientProfile; 
		pWriteTestMsgPatientProfileData->header.recipientId = whoId_MB;
		pWriteTestMsgPatientProfileData->header.issuedBy = whoId_MB;
		
		#if defined DEBUG_SCC 
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_PatientProfile' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) pWriteTestMsgPatientProfileData;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_PatientProfile'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&pWriteTestMsgPatientProfileData);
		}
		
		// read test 
		tdMsgPatientProfileData *pReadTestMsgPatientProfileData = NULL;
		pReadTestMsgPatientProfileData = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData)); // Will be freed by receiver task!
		if ( pReadTestMsgPatientProfileData == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) pReadTestMsgPatientProfileData;
		
		pReadTestMsgPatientProfileData->header.dataId = dataID_PatientProfile;
		pReadTestMsgPatientProfileData->header.issuedBy = whoId_MB;
		
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_RequestDatafromDS' with 'dataID_configureState'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pReadTestMsgPatientProfileData);
		}	
		
		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X  
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_PatientProfile))
		{
			#if defined DEBUG_SCC
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(	strcmp(pDb_PatientProfile->name,pReadTestMsgPatientProfileData->patientProfile.name) ||// returns 0 if both strings are equal!
		pDb_PatientProfile->gender != pReadTestMsgPatientProfileData->patientProfile.gender ||
		pDb_PatientProfile->measureTimeHours != pReadTestMsgPatientProfileData->patientProfile.measureTimeHours ||
		pDb_PatientProfile->measureTimeMinutes != pReadTestMsgPatientProfileData->patientProfile.measureTimeMinutes ||
		pDb_PatientProfile->wghtCtlTarget != pReadTestMsgPatientProfileData->patientProfile.wghtCtlTarget ||
		pDb_PatientProfile->wghtCtlLastMeasurement != pReadTestMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement ||
		pDb_PatientProfile->wghtCtlDefRemPerDay != pReadTestMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay ||
		pDb_PatientProfile->kCtlTarget != pReadTestMsgPatientProfileData->patientProfile.kCtlTarget ||
		pDb_PatientProfile->kCtlLastMeasurement != pReadTestMsgPatientProfileData->patientProfile.kCtlLastMeasurement ||
		pDb_PatientProfile->kCtlDefRemPerDay != pReadTestMsgPatientProfileData->patientProfile.kCtlDefRemPerDay ||
		pDb_PatientProfile->urCtlTarget != pReadTestMsgPatientProfileData->patientProfile.urCtlTarget ||
		pDb_PatientProfile->urCtlLastMeasurement != pReadTestMsgPatientProfileData->patientProfile.urCtlLastMeasurement ||
		pDb_PatientProfile->urCtlDefRemPerDay != pReadTestMsgPatientProfileData->patientProfile.urCtlDefRemPerDay ||
		pDb_PatientProfile->minDialPlasFlow  != pReadTestMsgPatientProfileData->patientProfile.minDialPlasFlow ||
		pDb_PatientProfile->maxDialPlasFlow  != pReadTestMsgPatientProfileData->patientProfile.maxDialPlasFlow ||
		pDb_PatientProfile->minDialVoltage  != pReadTestMsgPatientProfileData->patientProfile.minDialVoltage ||
		pDb_PatientProfile->maxDialVoltage  != pReadTestMsgPatientProfileData->patientProfile.maxDialVoltage)
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_PatientProfile: %s", currentOkay ? "OK" : "DIFFER");
		sFree((void *)&pReadTestMsgPatientProfileData);
		sFree((void *)&pDb_PatientProfile);
		
		inFile = fopen("tempfileDSTEST_paatient", "rb");
		outFile = fopen(FILE_PATIENTPROFILE, "wb");
		// int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);
		remove( "tempfileDSTEST_paatient");
	} // */	
	{
		//taskMessage("I", PREFIX, "not to change config, copy file to 'tempfileDSTEST', do the test and re-copy file again!");
		FILE* inFile = fopen(FILE_ALLSTATESPARAMETERSSCC, "rb");
		FILE* outFile = fopen("tempfileDSTEST_allScc", "wb");
		int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);

		tdMsgParametersSCC *pWriteTestMsgParametersSCC = NULL;
		pWriteTestMsgParametersSCC = (tdMsgParametersSCC *) pvPortMalloc(sizeof(tdMsgParametersSCC)); // Will be freed by receiver task!
		if ( pWriteTestMsgParametersSCC == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		tdParametersSCC	*pDb_9 = NULL;
		pDb_9 = (tdParametersSCC *) pvPortMalloc(sizeof(tdParametersSCC)); // Will be freed by receiver task!
		if ( pDb_9 == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		pDb_9->thisState=wakdStates_AllStopped;
	// for parametersSCC_stateAllStopped
		pDb_9->NaAbsL=0.1;     // lower Na absolute threshold
		pDb_9->NaAbsH=123;     // upper Na absolute threshold
		pDb_9->NaTrdT=0;      // actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->NaTrdLPsT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->NaTrdHPsT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
		
		pDb_9->KAbsL=0.1;      // lower K absolute threshold
		pDb_9->KAbsH=999;        // upper K absolute threshold
		pDb_9->KAAL=0.1;         // lower K absolute alarm threshold
		pDb_9->KAAH=999;         // upper K absolute alarm threshold
		pDb_9->KTrdT=0;       // actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->KTrdLPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->KTrdHPsT=0;     // actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->CaAbsL=0.1;    // lower Ca absolute threshold
		pDb_9->CaAbsH=123;     // upper Ca absolute threshold
		pDb_9->CaAAbsL=0.1;      // lower K absolute alarm threshold
		pDb_9->CaAAbsH=999;      // upper K absolute alarm threshold
		pDb_9->CaTrdT=0;       //actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->CaTrdLPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->CaTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
		
		pDb_9->UreaAAbsH=100;   // upper Urea absolute alarm threshold
		pDb_9->UreaTrdT=0;    // actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->UreaTrdHPsT=0; // actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->CreaAbsL=0.1;  	// lower Crea absolute threshold
		pDb_9->CreaAbsH=999;   // upper Crea absolute threshold
		pDb_9->CreaTrdT=0;  	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->CreaTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->PhosAbsL=0.1;   // lower Phos absolute threshold
		pDb_9->PhosAbsH=999;   // higher Phos absolute threshold
		pDb_9->PhosAAbsH=999;    // upper Phos absolute alarm threshold
		pDb_9->PhosTrdT=0;    	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->PhosTrdLPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->PhosTrdHPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->HCO3AAbsL=0.1;   // lower HCO3 absolute alarm threshold
		pDb_9->HCO3AbsL=0.1;    // lower HCO3 absolute threshold
		pDb_9->HCO3AbsH=999;    // upper HCO3 absolute threshold
		pDb_9->HCO3TrdT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->HCO3TrdLPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->HCO3TrdHPsT=0;	// actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->PhAAbsL=0.1;    // lower pH absolute alarm threshold
		pDb_9->PhAAbsH=10;    // upper pH absolute alarm threshold
		pDb_9->PhAbsL=3;    // lower pH absolute threshold
		pDb_9->PhAbsH=9;    // upper pH absolute threshold
		pDb_9->PhTrdT=0;		// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->PhTrdLPsT=0;		// actual opinion: TrndAnalysis not to be done in this state anyway
		pDb_9->PhTrdHPsT=0; 	// actual opinion: TrndAnalysis not to be done in this state anyway

		pDb_9->BPsysAbsL=70;    // lower BPsys absolute threshold
		pDb_9->BPsysAbsH=140;  // upper BPsys absolute threshold
		pDb_9->BPsysAAbsH=50; // upper BPsys absolute alarm threshold
		pDb_9->BPdiaAbsL=40;  // lower BPdia absolute threshold
		pDb_9->BPdiaAbsH=100;  // upper BPdia absolute threshold    
		pDb_9->BPdiaAAbsH=120; // upper BPdia absolute alarm threshold

		pDb_9->pumpFaccDevi=10;	// accepted flow deviation ml/min
		pDb_9->pumpBaccDevi=10;	// accepted flow deviation ml/min

		pDb_9->VertDeflecitonT=200;	// accepted vertical deflection threshold [? no information about units or values so far) [? no information about units or values so far)

		pDb_9->WghtAbsL=20;     // lower Weight absolute threshold
		pDb_9->WghtAbsH=100;   // upper Weight absolute threshold
		pDb_9->WghtTrdT=200;   // Weight thrend threshold
		pDb_9->FDpTL=90;    // Fluid Extraction Deviation Percentage Threshold Low(decrease)
		pDb_9->FDpTH=90;    // Fluid Extraction Deviation Percentage Threshold High(increase)

		pDb_9->BPSoTH=50;    // sensor 'BPSo' pressure upper threshold
		pDb_9->BPSoTL=1;       // sensor 'BPSo' pressure lower threshold
		pDb_9->BPSiTH=50;    // sensor 'BPSi' pressure upper threshold
		pDb_9->BPSiTL=1;       // sensor 'BPSi' pressure lower threshold
		pDb_9->FPSoTH=50;    // sensor 'FPSo' pressure upper threshold
		pDb_9->FPSoTL=1;       // sensor 'FPSo' pressure lower threshold
		pDb_9->FPSTH=50;    // sensor 'FPS' pressure upper threshold
		pDb_9->FPSTL=1;       // sensor 'FPS' pressure lower threshold

		pDb_9->BTSoTH=123;     // temperature sensor BTSo value upper threshold
		pDb_9->BTSoTL=5;      // temperature sensor BTSo value lower threshold
		pDb_9->BTSiTH=50;     // temperature sensor BTSi value upper threshold
		pDb_9->BTSiTL=5;      // temperature sensor BTSi value lower threshold

		pDb_9->BatStatT=10;   //battery status threshold

		// Expected adsorption rate calculation
		pDb_9->f_K=2;
		pDb_9->SCAP_K=2;
		pDb_9->P_K=2;
		pDb_9->Fref_K=2;
		pDb_9->cap_K=2;
		pDb_9->f_Ph=2;
		pDb_9->SCAP_Ph=2;
		pDb_9->P_Ph=2;
		pDb_9->Fref_Ph=2;
		pDb_9->cap_Ph=2;
		pDb_9->A_dm2=123;

		// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
		// KAdr=ADR_K_exp;
		pDb_9->CaAdr=2;
		// UrAdr=ADR_Ur_exp;
		pDb_9->CreaAdr=2;
		// PhosAdr=ADR_Phos_exp;
		pDb_9->HCO3Adr=2;
		
		pDb_9->wgtNa=1; // the fuzzy importance of the adsorption of this substance
		pDb_9->wgtK=1;
		pDb_9->wgtCa=1;
		pDb_9->wgtUr=1;
		pDb_9->wgtCrea=1;
		pDb_9->wgtPhos=1;
		pDb_9->wgtHCO3=1;
		pDb_9->mspT=1; // set the threshold here!	
	
		memcpy(&(pWriteTestMsgParametersSCC->parametersSCC), pDb_9, sizeof(tdParametersSCC));
		pWriteTestMsgParametersSCC->header.dataId = dataID_StateParametersSCC;
		pWriteTestMsgParametersSCC->header.recipientId = whoId_MB;
		pWriteTestMsgParametersSCC->header.issuedBy = whoId_MB;
		
		#if defined DEBUG_SCC || defined SEQ6
		taskMessage("D", PREFIX, "Forwarding TEST 'dataID_StateParametersSCC' to Dispatcher Task.");
		#endif
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) pWriteTestMsgParametersSCC;
		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
		taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_StateParametersSCC'!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree((void *)&pWriteTestMsgParametersSCC);
		}

		// read test 
		tdMsgParametersSCC *pReadTestMsgParametersSCC = NULL;
		pReadTestMsgParametersSCC = (tdMsgParametersSCC *) pvPortMalloc(sizeof(tdMsgParametersSCC)); // Will be freed by receiver task!
		if ( pReadTestMsgParametersSCC == NULL )
		{	// unable to allocate memory
			taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
			errMsg(errmsg_pvPortMallocFailed);
		}
		
		Qtoken.command	= command_RequestDatafromDS;
		Qtoken.pData	= (void *) pReadTestMsgParametersSCC;
		pReadTestMsgParametersSCC->header.dataId = dataID_StateParametersSCC;
		pReadTestMsgParametersSCC->header.issuedBy = whoId_MB;
		pReadTestMsgParametersSCC->parametersSCC.thisState = pDb_9->thisState;

		if ( xQueueSend( qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// DISP queue did not accept data. Free message and send error.
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'dataID_StateParametersSCC'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pReadTestMsgParametersSCC);
		}

		//	loop while nothing is in the queue or something is in the queue 
		//	and the command differs from command_RequestDatafromDSReady
		//	and the dataId differs from dataID_X 
		while((xQueueReceive( qhSCCin, &Qtoken, SCC_PERIOD) != pdPASS)
				|| (Qtoken.command != command_RequestDatafromDSReady)
				|| (((tdMsgSystemInfo *)(Qtoken.pData))->header.dataId != dataID_StateParametersSCC))
		{
			#if defined DEBUG_SCC 
			taskMessage("D", PREFIX, "Not received right answear while waiting for DSReady, increasing heartBeat");
			#endif
			sFree(&(Qtoken.pData));
			++heartBeatSCC;
		}
		
		// comparing results 
		if(pDb_9->NaAbsH != pReadTestMsgParametersSCC->parametersSCC.NaAbsH || pDb_9->CaAbsH != pReadTestMsgParametersSCC->parametersSCC.CaAbsH ||pDb_9->BTSoTH != pReadTestMsgParametersSCC->parametersSCC.BTSoTH ||pDb_9->A_dm2 != pReadTestMsgParametersSCC->parametersSCC.A_dm2 )
		{
			++numberOfErrors;
			currentOkay = 0;
			errorString[stringPos]= 'E';
		}
		else
		{
			currentOkay = 1;
			errorString[stringPos]= 'O';
		}
		stringPos= stringPos+1;
		
		taskMessage("I", PREFIX, "Comparing dataID_StateParametersSCC: %s", currentOkay ? "OK" : "DIFFER");
		// sFree((void *)&WriteTestpMsgBpData);
		sFree((void *)&pReadTestMsgParametersSCC);
		sFree((void *)&pDb_9);	
		
		inFile = fopen("tempfileDSTEST_allScc", "rb");
		outFile = fopen(FILE_ALLSTATESPARAMETERSSCC, "wb");
		// int ch;
		while( (ch = fgetc(inFile)) != EOF) fputc(ch, outFile);
		fclose(inFile);
		fclose(outFile);
		remove( "tempfileDSTEST_allScc");
	}

	
	deleteFiles(FILE_PHYSIOLOGICALDATA, 371630, 5);
	deleteFiles(FILE_PHYSICALDATA, 371633, 5);
	deleteFiles(FILE_ACTUATORDATA, 371636, 5);
	deleteFiles(FILE_WEIGHTDATA, 371639, 5);
	deleteFiles(FILE_BPDATA, 371642, 5);
	deleteFiles(FILE_ECGDATA, 371644, 5);	
	
	taskMessage("I", PREFIX, "Test completed with %i errors.",numberOfErrors);
	stringPos=0;
	taskMessage("I", PREFIX, "dataID_physiologicalData: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_physicalData: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_actuatorData: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_weightDataOK: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_bpData: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_EcgData: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_WAKDAllStateConfigure: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_configureState: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_PatientProfile: %c", errorString[stringPos]); stringPos= stringPos+1;
	taskMessage("I", PREFIX, "dataID_StateParametersSCC: %c", errorString[stringPos]); stringPos= stringPos+1;


// end of TESTING DS!!!
	
}
