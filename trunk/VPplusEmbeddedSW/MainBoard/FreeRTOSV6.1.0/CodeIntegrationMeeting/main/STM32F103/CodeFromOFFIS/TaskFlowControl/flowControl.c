/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "flowControl.h"
//#include "simlink.h"

#ifdef LINUX
	#include "ioHelperFunc.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\ioHelperFunc.h"
#endif

#define PREFIX "OFFIS FreeRTOS task FC"

// #define DEBUG_FC

/*-----------------------------------------------------------*/

uint8_t heartBeatFC = 0;

void tskFC( void *pvParameters )
{

	taskMessage("I", PREFIX, "Starting Flow Control task!");
	
	xQueueHandle *qhDISPin = ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
	xQueueHandle *qhFCin   = ((tdAllHandles *) pvParameters)->allQueueHandles.qhFCin;
	
 	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	Qtoken.pData	= NULL;

	// tdWAKDStateConfigurationParameters *pWAKDStateConfigure_toReconfig = NULL;
	// tdPatientProfile *pPatientProfile_toReconfig=NULL;
	// tdFCConfigPointerStruct FCConfigPointerStruct_toReconfig;

        vTaskDelay( (115*4*10) / portTICK_RATE_MS );
        heartBeatFC++;

	
	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif
	
	// Variables that measure the time periods for dialysis and regen cycles.
	tdUnixTimeType extractionEvery_sec=(6*60*60), timeNow=0;

/*
 * *****************************************************************************
 * ******************** Initialize Flow Control ********************************
 * *****************************************************************************
 */
	// At startup we need to initialize the parameters of flowControl.
	// This includes values like blood pump speed and polarization voltage.
	// These values are configured and stored on the Mainboard SD card. Data Storage
	// task handles these values. Hand over pointer to parameter struct so that it
	// can write initial values.

	tdMsgCurrentWAKDstateIs *pCurrentState = NULL;
		
	tdWAKDStateConfigurationParameters WAKDStateConfigure;
	tdPatientProfile patientProfile;
	tdFCConfigPointerStruct FCConfigPointerStruct;
	FCConfigPointerStruct.pWAKDStateConfigure = &WAKDStateConfigure;
	FCConfigPointerStruct.pPatientProfile = &patientProfile;
	
	tdWAKDOS_AutomaticDone WAKDOS_AutomaticDone = {0,0,0,0};
	
	// here we store the times necessary to change states and 
	// (all of them) for waterExtractionController()
	tdWAKDStatesTimes WAKDStatesTimes = {0,0,0,0,0,0};
	uint8_t requestCounter = 0;
	
	// keep in mind which state we are in, to know if it changed!
	tdWakdStates WAKDStateRemember; 
	
// 
//*****************************************************************************
//******************** Initialize the RTB *************************************
//*****************************************************************************
//

	// The values that have just been received from data storage to configure this task, need to be used
	// to configure the states of the RTB.
	sensorType lostGramsOneUltrafiltrationSession=0;
	sensorType extractionsPERday=0;
	uint16_t ultrafiltrationFlow=1;
	uint16_t ultrafiltrationDuration=1;
	
	#ifndef HWBUILD
	while (WAKDStatesTimes.DialysisDuration==0 || WAKDStatesTimes.Regen1Duration==0
			|| WAKDStatesTimes.UltrafiltrationDuration==0 || WAKDStatesTimes.Regen2Duration==0)
	{
		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_Maintenance;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);

		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_NoDialysate;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);
		
		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_Dialysis;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);

		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_Regen1;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);

		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_Ultrafiltration;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);
		// set frequency for water extraction for first time
		lostGramsOneUltrafiltrationSession = ((sensorType)WAKDStateConfigure.defSpeedFp_mlPmin / (float)FIXPOINTSHIFT_PUMP_FLOW)  * ((sensorType) WAKDStateConfigure.duration_sec) / 60.0;  // H2O: g=ml
		extractionsPERday = (1000.0* (sensorType) (patientProfile.wghtCtlDefRemPerDay / (float)FIXPOINTSHIFT_WEIGHTSCALE)) / (lostGramsOneUltrafiltrationSession) ;
		extractionEvery_sec = (tdUnixTimeType)(86400/extractionsPERday); // 86400 seconds is one day
		#if defined DEBUG_FC
			taskMessage("I", PREFIX, "Patient should loose %f Kg of weight per day.", ( ((float)patientProfile.wghtCtlDefRemPerDay) /(float)FIXPOINTSHIFT_WEIGHTSCALE) );
			taskMessage("I", PREFIX, "Excreted grams per ultrafiltration session: %f. ", lostGramsOneUltrafiltrationSession);
			taskMessage("I", PREFIX, "This computes to %f extraction cycles per day.", extractionsPERday);
			taskMessage("I", PREFIX, "This computes to one cycle every %d s.", extractionEvery_sec);
		#endif
		
		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_Regen2;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);

		FCConfigPointerStruct.pWAKDStateConfigure->thisState = wakdStates_AllStopped;
		getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);
		
                

	// Idea behind it: E.g. error on SD card and the reading of csv-files didnt work - then we should not continue as stae-changing will not work properly
	}  // if not at least the times in WAKDStatesTimes were set (not 0 anymore), this indicates something went wrong -> repeat while()
	#endif
        heartBeatFC++;

	// New weight measurements are only accepted if 20 hours are between two measurements.
	// At initialization there is no 
    tdUnixTimeType oldTimestamp_Chem	= 0;
    tdUnixTimeType oldTimestamp_Flow	= 0;
	tdUnixTimeType oldTimestamp_Wgt		= 0;
	
	uint8_t extracDone = 0;	// Number of completed extractions. None at the beginning! 
	// do not confuse extracDone with tdWAKDOS_AutomaticDone, as tdWAKDOS_AutomaticDone must be set back to 0 everytime the AutomaticOS is entered
    
    sensorType K_mmol_adsorbed_thisADphase	= 0.0;
    sensorType Ur_mmol_adsorbed_thisADphase	= 0.0;
    sensorType ADRK_mmolPmin_preset			= (patientProfile.kCtlDefRemPerDay/ ((float) FIXPOINTSHIFT_K)) /(24.0*60.0); // 0.03125 mmolPmin
    sensorType ADRUr_mmolPmin_preset		= (patientProfile.urCtlDefRemPerDay/ ((float) FIXPOINTSHIFT_K)) /(24.0*60.0); // 400mmolPday
    sensorType deviatSum_K					= 0.0;
    sensorType deviatSum_Ur_mmolPl			= 0.0;
	sensorType actFlow_mlPmin				= 0.0;
	sensorType ADRK_mmolPmin_now            = 0.0;
	sensorType ADRUr_mmolPmin_now			= 0.0;
	


	// During excretion sensors are in direct contact with patient body fluid.
	// Store last values as patient measurement. Have the following pointer mark it:
	tdMsgPhysiologicalData *pExcreationPhysiologicalReadout = NULL;
	// same to remember last sensor reading in dialysis state
	tdMsgPhysiologicalData *pDialysisPhysiologicalReadout 	= NULL;

// 
// *****************************************************************************
// ********* Change to state 'wakdStates_AllStopped'|'wakdStatesOS_Automatic'*****
// *****************************************************************************
//
	// The current state is unknown! Ask RTB what it is (most likely undefined).
	// Receive the current state from the real time board (should be 'wakdStates_AllStopped'|'wakdStatesOS_Automatic')
	// First send out the status request to RTB
	tdMsgOnly *pStatusRequest = NULL;
	pStatusRequest = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
	if (pStatusRequest==NULL){// unable to allocate memory
		taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	} else {
		pStatusRequest->header.dataId		= dataID_statusRequest;
		pStatusRequest->header.issuedBy		= whoId_MB;
		pStatusRequest->header.recipientId	= whoId_RTB;
		Qtoken.command	= command_UseAtachedMsgHeader;
		Qtoken.pData	= (void *) pStatusRequest;
		#if defined DEBUG_FC || defined SEQ0
			taskMessage("I", PREFIX, "Sending 'dataID_statusRequest' to DISP for RTB.");
		#endif
		if ( xQueueSend( qhDISPin, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full for too long time. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of DISP did not accept 'dataID_statusRequest'!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
		} else {
			// Receive answer to status request from RTB
			while (pCurrentState==NULL)
			{
				if ( xQueueReceive( qhFCin, &Qtoken, FC_PERIOD) == pdPASS )
				{	// Check if we received status
					if (Qtoken.command == command_UseAtachedMsgHeader)
					{
						tdMsgOnly *pMsgOnly = (tdMsgOnly *) (Qtoken.pData);
						if (pMsgOnly->header.dataId==dataID_currentWAKDstateIs)
						{
							pCurrentState = (tdMsgCurrentWAKDstateIs *) (Qtoken.pData);
							#if defined DEBUG_FC || defined SEQ0
								taskMessage("I", PREFIX, "FC is in state '%s'|'%s'.", stateIdToString(pCurrentState->wakdStateIs), opStateIdToString(pCurrentState->wakdOpStateIs));
							#endif
							FCConfigPointerStruct.pWAKDStateConfigure->thisState = pCurrentState->wakdStateIs;
							getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);
							#if defined DEBUG_FC || defined SEQ0
								taskMessage("I", PREFIX, "FC reconfiguration for new state %s done.", stateIdToString(WAKDStateConfigure.thisState));
							#endif
							// ((void *)&pMsgOnly); // Must not be freed!! pCurrentState is pointing to this!!
						} else {
							sFree(&(Qtoken.pData));
							taskMessage("E", PREFIX, "Message to arrive should be 'dataID_currentWAKDstateIs'.");
							errMsg(errmsg_ErrFlowControl);
						}
					} else {
						sFree(&(Qtoken.pData));
						taskMessage("E", PREFIX, "Message to arrive should be 'dataID_currentWAKDstateIs'.");
						errMsg(errmsg_ErrFlowControl);
					}
				}
			}
		}
	}
	// Making sure that RTB is in state AllStopped|Automatic
	if (!((pCurrentState->wakdStateIs==wakdStates_AllStopped)&&(pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic)))
	{	// change into the safe state AllStopped
		if (changeIntoStateRequest(wakdStates_AllStopped, wakdStatesOS_Automatic, pCurrentState, qhDISPin))
		{	// Changing WAKD state has failed!
			taskMessage("E", PREFIX, "Changing into new state has failed: 'wakdStates_AllStopped'.");
			errMsg(errmsg_ErrFlowControl);
		} else {
			while (!((pCurrentState->wakdStateIs == wakdStates_AllStopped)&&(pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic)))
			{
				if ( xQueueReceive( qhFCin, &Qtoken, FC_PERIOD) == pdPASS )
				{	// Check if we received Acknowledge
					if (Qtoken.command == command_UseAtachedMsgHeader)
					{
						tdMsgOnly *pMsgOnly = (tdMsgOnly *) (Qtoken.pData);
						if (pMsgOnly->header.dataId==dataID_currentWAKDstateIs)
						{	// new current state arrived. Delete old one.
							sFree((void *)&pCurrentState);
							pCurrentState = (tdMsgCurrentWAKDstateIs *) (Qtoken.pData);
							#if defined DEBUG_FC || defined SEQ0
								taskMessage("I", PREFIX, "FC is in state '%s'|'%s'.", stateIdToString(pCurrentState->wakdStateIs), opStateIdToString(pCurrentState->wakdOpStateIs));
							#endif
							FCConfigPointerStruct.pWAKDStateConfigure->thisState = pCurrentState->wakdStateIs;
							getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 1);
							#if defined DEBUG_FC || defined SEQ0
								taskMessage("I", PREFIX, "and FC reconfiguration for new state %s done.", stateIdToString(WAKDStateConfigure.thisState));
							#endif
							WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
						} else {
							sFree(&(Qtoken.pData));
							taskMessage("E", PREFIX, "Message to arrive should be 'dataID_currentWAKDstateIs'.");
							errMsg(errmsg_ErrFlowControl);
						}
					} else {
						sFree(&(Qtoken.pData));
						taskMessage("E", PREFIX, "Message to arrive should be 'dataID_currentWAKDstateIs'.");
						errMsg(errmsg_ErrFlowControl);
					}
				}
			}
		}
	}
// 
// *****************************************************************************
// ************ DONE and READY -> start normal operation of FC *****************
// *****************************************************************************
//

	for( ;; ) {
		// tasks in FreeRTOS run forever. They are scheduled by the OS, interrupted and continued.
		// We start with infinite loop.

		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every FC_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatFC++;
		
	
		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif


		// Watch Task's input queue for commands to come in.
		// If nothing happens in queue, the task will wake up anyway
		// after period to give note that it is unusually quiet!
		//
		if ( xQueueReceive( qhFCin, &Qtoken, FC_PERIOD) == pdPASS ) {
			#ifdef DEBUG_FC
				taskMessage("I", PREFIX, "Data in Flow Control queue!");
			#endif

			// The following incoming messages are treated the same no matter what current state we are in.
			// So we handle them outside the "switch wakdStateIs"
			if ( (Qtoken.command == command_UseAtachedMsgHeader) && (((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_currentWAKDstateIs) ){
				// RTB informed us about its current state. Remember this. Discard our old knowledge and remember the new.
				
				if (pCurrentState->wakdStateIs != ((tdMsgCurrentWAKDstateIs *) Qtoken.pData)->wakdStateIs)
				{ // the state has changed, so remmeber the time to switch out of it again
					WAKDStatesTimes.timelastStateChange = getTime();
					if (pDialysisPhysiologicalReadout!=NULL)
					{	// Discard previous chemical dialysis-state values so these 'old' values won't be used when entering dialysis again.
						sFree((void *)&pDialysisPhysiologicalReadout);
					} else {
						// First time we get PhysiologicalSensor Data. Nothing to discard.
					}
				}
				
				sFree((void *)&pCurrentState);
				pCurrentState = (tdMsgCurrentWAKDstateIs *) Qtoken.pData;
											
				#if defined DEBUG_FC || defined SEQ4
					taskMessage("I", PREFIX, "FC was informed about current state: '%s'|'%s'.", stateIdToString(pCurrentState->wakdStateIs), opStateIdToString(pCurrentState->wakdOpStateIs));
				#endif
				FCConfigPointerStruct.pWAKDStateConfigure->thisState = pCurrentState->wakdStateIs;
				getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 0);

			} else if ( (Qtoken.command == command_UseAtachedMsgHeader) && (((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_systemInfo) ) {
				#if defined DEBUG_FC || defined SEQ11
					taskMessage("I", PREFIX, "Received SCC message: '%s'.", infoMsgToString(((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum) );
				#endif
				interpretSCCmessage ( ((tdMsgSystemInfo *)(Qtoken.pData))->msgEnum,  qhDISPin);
				sFree((void *)&(Qtoken.pData));
			} else if ( (Qtoken.command == command_UseAtachedMsgHeader) && (((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_weightDataOK) ) {
				tdMsgWeightData *pMsgWeightData = NULL;
				pMsgWeightData = (tdMsgWeightData *) (Qtoken.pData);
				#ifdef DEBUG_FC
					taskMessage("I", PREFIX, "Received weight measurement: %f).", (((float)pMsgWeightData->weightData.Weight) / ((float)FIXPOINTSHIFT_WEIGHTSCALE)) );
				#endif
				waterExtractionController(&pMsgWeightData->weightData, &ultrafiltrationFlow, &ultrafiltrationDuration, &patientProfile,
					&extracDone, &extractionsPERday, &oldTimestamp_Wgt, &extractionEvery_sec, &WAKDStatesTimes, qhDISPin);
	
				sFree((void *)&pMsgWeightData);
			} else if ( (Qtoken.command == command_ConfigurationChanged) ) {
				#if defined DEBUG_FC || defined SEQ13
					taskMessage("I", PREFIX, "Received messege command_ConfigurationChanged - doing getStateParametersFromDS() for new configuration to DS " );
				#endif			
				getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 0);
				
			} else {
			// These following incoming messages require different actions depending on the current state. So
			// we want to handle them in a switch across the WAKD-states.
				switch (pCurrentState->wakdStateIs) {
					case (wakdStates_Dialysis): {
						switch (Qtoken.command) {
							case (command_UseAtachedMsgHeader): {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case (dataID_physicalData): {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received physical data. prFCI: %f. Discarding, not needed now.", (float)(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE) )  ) ;
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case (dataID_actuatorData): {
										// Flow control needs to know the flow of dialysate. Read this value from data.
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received actuator data.");
										#endif
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										if (pDialysisPhysiologicalReadout!=NULL ) {// only if already this data was received
											keepTrackAdsorptions(&ADRK_mmolPmin_now, &ADRUr_mmolPmin_now, &K_mmol_adsorbed_thisADphase, &Ur_mmol_adsorbed_thisADphase, pDialysisPhysiologicalReadout, &actFlow_mlPmin, pActuatorData->actuatorData.TimeStamp, &oldTimestamp_Flow);

										}
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received and processed ActuatData; new DialFlow %f ml per minute.", actFlow_mlPmin);
										#endif
										break;
									}
									case (dataID_physiologicalData): {
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data received - Processing Data.");
										#endif

										// Store last physiological-sensors measurement>>
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) Qtoken.pData;
										if (pDialysisPhysiologicalReadout!=NULL) {
											// Discard previous values and remember new
											sFree((void *)&pDialysisPhysiologicalReadout);
										} else {
											// First time we get PhysiologicalSensor Data. Nothing to discard.
										}
										// Remeber last physiological-sensors measurement
										pDialysisPhysiologicalReadout = pPhysiologicalSensorReadout;

										// Compute new flow rate for dialysate and voltage for polarizer.
										if (compTimeIsSmaller(oldTimestamp_Chem, pPhysiologicalSensorReadout->physiologicalData.TimeStamp)) {
											// Mode is Adsorption/decomposition
											// Adsorption Rate Controller%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
											// takes the actual K+ concentration, calculates the actual adsorption
											// rate, compares it to the preset adsorption rate, lowers/increases
											// pump-speed based on discrepancy between preset and actual adsorption rate

											dialysisState_ComputeNewPumpspeed(&ADRK_mmolPmin_preset, &ADRK_mmolPmin_now, &deviatSum_K, pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium, &WAKDStateConfigure, &patientProfile);
	
											// Decomposition Rate Controller %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
											dialysisState_ComputeNewVolt(&ADRUr_mmolPmin_preset, &ADRUr_mmolPmin_now, &deviatSum_Ur_mmolPl, pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Urea, &WAKDStateConfigure, &patientProfile);

											#ifdef DEBUG_FC
												taskMessage("I", PREFIX, "Time:           %i",pPhysiologicalSensorReadout->physiologicalData.TimeStamp); // ->physiologicalData.TimeStamp is tdUnixTimeType
												taskMessage("I", PREFIX, "K ECPo:         %i",pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium);
												taskMessage("I", PREFIX, "K ECPi:         %i",pPhysiologicalSensorReadout->physiologicalData.ECPDataI.Potassium);
												taskMessage("I", PREFIX, "actFlow_mlPmin: %i",actFlow_mlPmin);
											#endif
											oldTimestamp_Chem = pPhysiologicalSensorReadout->physiologicalData.TimeStamp;
											// Send values to Dispatcher to inform the rest of the WAKD
											if (sendWakdStateConfig(wakdStates_Dialysis, &WAKDStateConfigure, qhDISPin))
											{	// configuring WAKD state has failed!
												taskMessage("E", PREFIX, "Configuration of WAKD state failed 'wakdStates_Dialysis'.");
												errMsg(errmsg_ConfiguringWakdStateFailed);
											}
										}
										break;
									}
									case (dataID_changeWAKDStateFromTo):{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#if defined DEBUG_FC || defined SEQ4 || defined SEQ11
											taskMessage("I", PREFIX, "Change State (now: Dialysis) - Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
											(pWakdStateFromTo->toWakdState == wakdStates_AllStopped || 	// only these transitions are defined to be possible!
											pWakdStateFromTo->toWakdState == wakdStates_Regen1 ||		// if GUI want to go to another state, if has to make it in several steps (not directly). But this is handeled by GUI, for safety reasons
											pWakdStateFromTo->toWakdState == wakdStates_NoDialysate)) {
											// current state and request to change from state are equal. Request is valid!
											// Inform RTB to change into new state
											// sendCurrentState(pCurrentState, &Qtoken, qhDISPin);
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {
											// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("W", PREFIX, "(Dialy) Changing State not possible. Outdated or not allowed transition. Allowed: Regen1, NoDialysate, AllStopped!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									}
									default:{
										defaultIdHandling(PREFIX, dataId);
									break;
									}
								}
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						}
						// check if it is time to leave this state
						timeNow = getTime();
						// taskMessage("I", PREFIX, "Entered dialysis at %d. Duration is %d. Time is now %d.", WAKDStatesTimes.timelastStateChange, WAKDStatesTimes.DialysisDuration, timeNow);
						if (pCurrentState->wakdOpStateIs==wakdStatesOS_Semiautomatic && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.DialysisDuration) ) { //if semiautomatic and enough time passed -> fall back to wakdStates_NoDialysate
							// We are in Semiautomatic mode. Always return to state 'wakdStates_NoDialysate'.
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "Changing State: 'wakdStates_NoDialysate'|'wakdStatesOS_Semiautomatic'.");
							#endif
							// Inform WAKD tasks about new current state.
							if (changeIntoStateRequest(wakdStates_NoDialysate, wakdStatesOS_Semiautomatic, pCurrentState, qhDISPin))
							{	// Changing WAKD state has failed!
								taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
								errMsg(errmsg_ErrFlowControl);
							} else {
								WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
							} // of if(changeIntoStateRequest...
						} else {// Is it time to start regeneration and excretion cycle? If so, inform patient.
							// We are in Automatic mode. Proceed to next state if it is the time.
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "exPday:%d, exeverysec:%d, now:%d, OS_AutomaticCycleStart:%d |DUR R1%d UF%d R2%d |DONE R1%d UF%d R2%d DI%d",
									(int)extractionsPERday, extractionEvery_sec, timeNow, WAKDStatesTimes.OS_AutomaticCycleStart,
									WAKDStatesTimes.Regen1Duration, WAKDStatesTimes.UltrafiltrationDuration, WAKDStatesTimes.Regen2Duration,
									WAKDOS_AutomaticDone.Regen1Count, WAKDOS_AutomaticDone.UltrafiltrationCount, WAKDOS_AutomaticDone.Regen2Count, WAKDOS_AutomaticDone.DialysisCount
								);
								taskMessage("I", PREFIX, "will change state when %d is larger than %d? -> true?: %d",
									(timeNow-WAKDStatesTimes.OS_AutomaticCycleStart),
									(int)(	(extractionEvery_sec-WAKDStatesTimes.Regen1Duration-WAKDStatesTimes.UltrafiltrationDuration-WAKDStatesTimes.Regen2Duration)*(WAKDOS_AutomaticDone.DialysisCount+1)
										+ WAKDOS_AutomaticDone.Regen1Count * WAKDStatesTimes.Regen1Duration
										+ WAKDOS_AutomaticDone.UltrafiltrationCount * WAKDStatesTimes.UltrafiltrationDuration
										+ WAKDOS_AutomaticDone.Regen2Count * WAKDStatesTimes.Regen2Duration ),
									(int)((timeNow-WAKDStatesTimes.OS_AutomaticCycleStart) >
										(extractionEvery_sec-WAKDStatesTimes.Regen1Duration-WAKDStatesTimes.UltrafiltrationDuration-WAKDStatesTimes.Regen2Duration)*(WAKDOS_AutomaticDone.DialysisCount+1)
										+ WAKDOS_AutomaticDone.Regen1Count * WAKDStatesTimes.Regen1Duration
										+ WAKDOS_AutomaticDone.UltrafiltrationCount * WAKDStatesTimes.UltrafiltrationDuration
										+ WAKDOS_AutomaticDone.Regen2Count * WAKDStatesTimes.Regen2Duration )
								);
							#endif

							if ( pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic && (extractionsPERday>0) && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.DialysisDuration))
							{	// It is time to start a regeneration cycle. Inform Patient to attach regen & disposal bags.
								// Once the patient Ack with OK, GUI will initiate changing state to DialysisToRegen1.
								WAKDOS_AutomaticDone.DialysisCount =WAKDOS_AutomaticDone.DialysisCount+1;
								// Remind patient to connect the bags!
								Qtoken.command	= command_InformGuiConnectBag;	// ToBeDone: the GUI-Task needs to assure the bag is connected and then send dataID_changeWAKDStateFromTo(->excreation) to FC
								Qtoken.pData	= (void *) wakdStates_Regen1;	// Once the GUI has OK from patient it should request change into this state.
								#ifdef DEBUG_FC
									taskMessage("I", PREFIX, "Sending 'command_InformGuiConnectBag' to dispatcher task.");
								#endif
								if ( xQueueSend( qhDISPin, &Qtoken, FC_BLOCKING) != pdPASS )
								{	// Seemingly the queue is full. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of Dispatcher did not accept data. Message is lost!!!");
									errMsg(errmsg_QueueOfDispatcherTaskFull);
								} // if ( xQueueSend( qhDISPin ...
							} //if ( currentState.wakdOpStateIs==wakdStatesOS_Automatic...
						} // if (currentState.wakdOpStateIs==wakdStatesOS_Semiautomatic...
						break;
					} // of case wakdStates_Dialysis: */
					case (wakdStates_Regen1): {
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)));
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate. Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received and processed ActuatData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data received. K+O %f. not needed now. Discarding.", ( ((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)) );
										#endif
										sFree((void *)&pPhysiologicalSensorReadout);
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change (now: Regen1) to %d.", ((tdMsgWakdStateFromTo *)(Qtoken.pData))->toWakdState);
										#endif
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: Reg1)- Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
										(pWakdStateFromTo->toWakdState == wakdStates_AllStopped || 	// only these transitions are defined to be possible from here
										pWakdStateFromTo->toWakdState == wakdStates_NoDialysate)) {
											// current state and request to change from state are equal. Request is valid!
											// The new state requires update of actuator values
											// setActuatorValues(currentState, &actuators, &parametersFC);
											// Inform WAKD tasks about new current state.
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {
											// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("I", PREFIX, "(Reg1) Changing State not possible. Outdated or not allowed transition. Allowed: Ultrafiltration, NoDialysate, AllStopped!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									}
									default:{
										defaultIdHandling(PREFIX, dataId);
									break;
									}
								}
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						}	// for switch (Qtoken.command) {
						// check if it is time to leave this state
						timeNow = getTime();
						if (pCurrentState->wakdOpStateIs==wakdStatesOS_Semiautomatic && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.Regen1Duration) ) { //if semiautomatic and enough time passed -> fall back to wakdStates_NoDialysate
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "Changing State.");
							#endif
							// The new state requires update of actuator values
							// setActuatorValues(currentState, &actuators, &parametersFC);
							// Inform WAKD tasks about new current state.
							if (changeIntoStateRequest(wakdStates_NoDialysate, wakdStatesOS_Semiautomatic, pCurrentState, qhDISPin))
							{	// Changing WAKD state has failed!
								taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
								errMsg(errmsg_ErrFlowControl);
							} else {
								WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
							} // of if(changeIntoStateRequest...
						} else {// in Automatic mode proceed to next Operational State

							if ( pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic && (extractionsPERday>0) && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.Regen1Duration))
							{
								WAKDOS_AutomaticDone.Regen1Count = WAKDOS_AutomaticDone.Regen1Count +1;
								// It is time to start an ultrafiltration cycle.
								#ifdef DEBUG_FC
									taskMessage("I", PREFIX, "Changing State to Ultrafiltration.");
								#endif
								// Inform WAKD tasks about new current state.
								if (changeIntoStateRequest(wakdStates_Ultrafiltration, pCurrentState->wakdOpStateIs, pCurrentState, qhDISPin))
								{	// Changing WAKD state has failed!
									taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
									errMsg(errmsg_ErrFlowControl);
								} else {
									WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
								} // of if(changeIntoStateRequest...
							} // of if ( currentState.wakdOpStateIs==wakdStatesOS_Automatic
						} // of if (currentState.wakdOpStateIs==wakdStatesOS_Semiautomatic
						break;
					} // for case wakdStates_Regen1:
					case wakdStates_Ultrafiltration: {
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)) );
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate. Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved and processed ActuatData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// During excreation sensors are in direct contact with patient body fluid.
										// Store last values as patient measurement.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										if (pExcreationPhysiologicalReadout!=NULL) {
											// Discard previous values to be able to remember new
											sFree((void *)&pExcreationPhysiologicalReadout);
										}
										else {
											// First time we do patient measurement. Nothing to discard.
										}
										pExcreationPhysiologicalReadout=pPhysiologicalSensorReadout;
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data recieverd - stored Values. K+O: %f", ( ((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)));
										#endif
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: Ufiltration) - Request to %d", pWakdStateFromTo->toWakdState);
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
											(pWakdStateFromTo->toWakdState == wakdStates_AllStopped || 	// only these transitions are defined to be possible!
											pWakdStateFromTo->toWakdState == wakdStates_NoDialysate)) {
											// current state and request to change from state are equal. Request ist valid!
											// The new state requires update of actuator values
											// setActuatorValues(currentState, &actuators, &parametersFC);
											// Inform WAKD tasks about new current state.
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {
											// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("I", PREFIX, "(Ufilt) Changing State not possible. Outdated or not allowed transition. Allowed: Regen2, NoDialysate, AllStopped!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									}
									default:{
										defaultIdHandling(PREFIX, dataId);
									break;
									}
								}
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						}
						// Regeneration phase one done? If Semiautomatic mode, then fall back to All_Stopped
						timeNow = getTime();
						if (pCurrentState->wakdOpStateIs==wakdStatesOS_Semiautomatic && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.UltrafiltrationDuration) ) { //if semiautomatic and enough time passed -> fall back to wakdStates_NoDialysate
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "Changing State.");
							#endif
							// The new state requires update of actuator values
							// setActuatorValues(currentState, &actuators, &parametersFC);
							// Inform WAKD tasks about new current state.
							if (changeIntoStateRequest(wakdStates_NoDialysate, wakdStatesOS_Semiautomatic, pCurrentState, qhDISPin))
							{	// Changing WAKD state has failed!
								taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
								errMsg(errmsg_ErrFlowControl);
							} else {
								WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
							} // of if(changeIntoStateRequest...
						} else {// in Automatic mode process all chemical values as patient data and proceed to next Operational State

							if ( pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic && (extractionsPERday>0) && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.UltrafiltrationDuration))
							{
									//time to regnerate if extraction finished
								WAKDOS_AutomaticDone.UltrafiltrationCount = WAKDOS_AutomaticDone.UltrafiltrationCount +1;
								// Completed extraction. We have to remember the number of extractions for function: waterExtractionController
								extracDone=extracDone+1;
								// During extraction the outlet sensors are in direct contact with patient fluid
								// So measuring now gives "real" patient values.
								// Take the last K-concentration value (assuming this is the concentration in the patient!)
								sensorType K_2removAdd_mmol     = ( (((sensorType)pExcreationPhysiologicalReadout->physiologicalData.ECPDataO.Potassium) / ((sensorType) FIXPOINTSHIFT_K))
									- (patientProfile.kCtlTarget/ ((sensorType) FIXPOINTSHIFT_K)) )*0.6* (patientProfile.wghtCtlTarget/(sensorType)FIXPOINTSHIFT_WEIGHTSCALE); // assuming water is 60% of weight

								// printf("************************ K_2removAdd_mmol%f = (ecpoK%f-cK_target%f)*0.6*patientWeightTarget_g%d\n", K_2removAdd_mmol, pExcreationPhysiologicalReadout->physiologicalData.ECPDataO.Potassium, (patientProfile.kCtlTarget/ ((float) FIXPOINTSHIFT_K)), (int)(patientProfile.wghtCtlTarget/ (float)FIXPOINTSHIFT_WEIGHTSCALE) );
								ADRK_mmolPmin_preset= (K_mmol_adsorbed_thisADphase + K_2removAdd_mmol) /
									((sensorType)(extractionEvery_sec-WAKDStatesTimes.Regen1Duration-WAKDStatesTimes.Regen2Duration-WAKDStatesTimes.UltrafiltrationDuration)/60);

								// printf("************************ ADRK_mmolPmin_preset%f = (K_mmol_adsorbed_thisADphase%f + K_2removAdd_mmol%f) / ((extractionEvery_sec%d-regenRPDur_sec%d-regenNCDur_sec%d-extrDur_sec%d)/60);\n",ADRK_mmolPmin_preset, K_mmol_adsorbed_thisADphase, K_2removAdd_mmol, (int)extractionEvery_sec, (int)WAKDStatesTimes.Regen1Duration, (int)WAKDStatesTimes.Regen2Duration, (int)WAKDStatesTimes.UltrafiltrationDuration);
								// deviatSum_K=0; If you don't do this the Pump will start again
								// faster when going to Dialysis mode again!
								// One might:         subtract K_2removWatEx_mmol from ADRK_mmolPmin_preset

								// take the last Ur-concentration value (assuming this is the
								// concentration in the patient!
								// use it to calculate new target value for decomposition for the
								// next adsorption-phase
								sensorType Ur_2removAdd_mmol = ( ((sensorType) (pExcreationPhysiologicalReadout->physiologicalData.ECPDataO.Urea - patientProfile.urCtlTarget)) / ((sensorType)FIXPOINTSHIFT_UREA) )
											* 0.6 * ((sensorType)patientProfile.wghtCtlTarget/(float)FIXPOINTSHIFT_WEIGHTSCALE); // assuming water is 60% of weight
								// sensorType Ur_2removFluidEx_mmol = parametersFC.cUr_target*((parametersFC.extr_mlPmin/1000)*(parametersFC.extrDur_sec/60));
								ADRUr_mmolPmin_preset= (Ur_mmol_adsorbed_thisADphase + Ur_2removAdd_mmol) / (((sensorType)(extractionEvery_sec - WAKDStatesTimes.Regen1Duration-WAKDStatesTimes.Regen2Duration-WAKDStatesTimes.UltrafiltrationDuration))/60);
								// deviatSum_Ur_mmolPl=0;
								// One might:         subtract Ur_2removFluidEx_mmol from ADRUr_mmolPmin_preset

								K_mmol_adsorbed_thisADphase=0;
								Ur_mmol_adsorbed_thisADphase=0;

								// at last update the patient profile and store it
								patientProfile.kCtlLastMeasurement = (uint32_t) (((sensorType)pExcreationPhysiologicalReadout->physiologicalData.ECPDataO.Potassium) / ((sensorType) FIXPOINTSHIFT_K));
								patientProfile.kCtlDefRemPerDay = (uint32_t) ((ADRK_mmolPmin_preset * FIXPOINTSHIFT_K) * 24.0*60.0);
								patientProfile.urCtlLastMeasurement = (uint32_t) (((sensorType)pExcreationPhysiologicalReadout->physiologicalData.ECPDataO.Urea) / ((sensorType)FIXPOINTSHIFT_UREA));
								patientProfile.urCtlDefRemPerDay = (uint32_t) ((ADRUr_mmolPmin_preset * FIXPOINTSHIFT_K) * 24.0*60.0);


								// It is time to start second phase of regeneration cycle (no polarization current).
								#ifdef DEBUG_FC
									taskMessage("I", PREFIX, "Changing State to Regen2.");
								#endif
								// setActuatorValues(currentState, &actuators, &parametersFC);
								// Inform WAKD tasks about new current state.
								if (changeIntoStateRequest(wakdStates_Regen2, pCurrentState->wakdOpStateIs, pCurrentState, qhDISPin))
								{	// Changing WAKD state has failed!
									taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_Regen2), opStateIdToString(pCurrentState->wakdOpStateIs));
									errMsg(errmsg_ErrFlowControl);
								} else {
									WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
								} // of if(changeIntoStateRequest...
							} // of if ( currentState.wakdOpStateIs==wakdStatesOS_Automatic
						} // of if (currentState.wakdOpStateIs==wakdStatesOS_Semiautomatic
						break;
					} // for case wakdStates_Ultrafiltration: */
					case wakdStates_Regen2: {
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)));
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate. Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received and processed ActuatData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data received. K+O %f. not needed now. Discarding.", (((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)));
										#endif
										sFree((void *)&pPhysiologicalSensorReadout);
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: Reg2) - Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
										(pWakdStateFromTo->toWakdState == wakdStates_AllStopped || 	// only these transitions are defined to be possible!
										pWakdStateFromTo->toWakdState == wakdStates_NoDialysate)) {
											// current state and request to change from state are equal. Request ist valid!
											// Inform WAKD tasks about new current state.
											//sendCurrentState(pCurrentState, &Qtoken, qhDISPin);
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("I", PREFIX, "(Reg2) Changing State not possible. Outdated or not allowed transition. Allowed: Dialysis, NoDialysate, AllStopped!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									}
									default:{
										defaultIdHandling(PREFIX, dataId);
										break;
									}
								}
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						}
						// check if its time to change states
						timeNow = getTime();
						if (pCurrentState->wakdOpStateIs==wakdStatesOS_Semiautomatic && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.Regen2Duration) ) { //if semiautomatic and enough time passed -> fall back to wakdStates_NoDialysate
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "Changing State.");
							#endif
							// Inform WAKD tasks about new current state.
							if (changeIntoStateRequest(wakdStates_NoDialysate, wakdStatesOS_Semiautomatic, pCurrentState, qhDISPin))
							{	// Changing WAKD state has failed!
								taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
								errMsg(errmsg_ErrFlowControl);
 							} else {
								WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
							} // of if(changeIntoStateRequest...
						} else {// in Automatic mode proceed to next Operational State

							if ( pCurrentState->wakdOpStateIs==wakdStatesOS_Automatic && (extractionsPERday>0) && timeNow>=(WAKDStatesTimes.timelastStateChange+WAKDStatesTimes.Regen2Duration))
							{
								WAKDOS_AutomaticDone.Regen2Count = WAKDOS_AutomaticDone.Regen2Count +1;
								#ifdef DEBUG_FC
									taskMessage("I", PREFIX, "Changing State to Dialysis.");
								#endif
								// Inform WAKD tasks about new current state.
								if (changeIntoStateRequest(wakdStates_Dialysis, wakdStatesOS_Automatic, pCurrentState, qhDISPin))
								{	// Changing WAKD state has failed!
									taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(wakdStates_NoDialysate), opStateIdToString(pCurrentState->wakdOpStateIs));
									errMsg(errmsg_ErrFlowControl);
								} else {
									WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
								} // of if(changeIntoStateRequest...
								Qtoken.command	= command_InformGuiDisconnectBag;
								Qtoken.pData	= NULL;
								#ifdef DEBUG_FC
									taskMessage("I", PREFIX, "Sending 'command_InformGuiDisconnectBag' to dispatcher task.");
								#endif
								if ( xQueueSend(qhDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
								{	// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of DISP did not accept 'command_InformGuiDisconnectBag'!");
									errMsg(errmsg_QueueOfDispatcherTaskFull);
								}
							} // of if ( currentState.wakdOpStateIs==wakdStatesOS_Automatic...
						} // of if (currentState.wakdOpStateIs==wakdStatesOS_Semiautomatic
						break;
					} // for case wakdStates_Regen2
					case wakdStates_AllStopped: {
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)));
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate. (should be 0 here) Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Received and processed ActuatorData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data received. K+O %f. not needed now. Discarding.", (((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)));
										#endif
										sFree((void *)&pPhysiologicalSensorReadout);
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: AllStop) - Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
											(pWakdStateFromTo->toWakdState == wakdStates_NoDialysate 	|| 	// only these transitions are defined to be possible!
											pWakdStateFromTo->toWakdState == wakdStates_Maintenance)) {
											// current state and request to change from state are equal. Request ist valid!
											// Inform WAKD tasks about new current state.
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("W", PREFIX, "(AllStop) Changing State not possible. Outdated or not allowed transition. Allowed: NoDialysate, Maintenance!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									} // case command_ChangeWAKDStateFromTo:
									default: {
										defaultIdHandling(PREFIX, dataId);
										break;
									}
								} // of switch (dataId)
								break;
							// never ever leave this case automatically, but just by user-interface request (handeled above)
							} // for case command_UseAtachedMsgHeader:
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						}
						break;
					} // for case wakdStates_AllStopped:
					case wakdStates_NoDialysate:{
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)));
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate (should be = 0 here). Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved and processed ActuatData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data recieverd. K+O %f. not needed now. Discarding.", (((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)));
										#endif
										sFree((void *)&pPhysiologicalSensorReadout);
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: NoDialys) - Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if ( (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState ) && 
											(pWakdStateFromTo->toWakdState != wakdStates_Maintenance) // from NoDialysate you can change to anything but Maintenence!
										) {
											// current state and request to change from state are equal. Request is valid!
											// Inform WAKD tasks about new current state.
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
												if (pWakdStateFromTo->toWakdOpState == wakdStatesOS_Automatic && ( pWakdStateFromTo->toWakdState ==wakdStates_Dialysis || pWakdStateFromTo->toWakdState ==wakdStates_Regen1 || pWakdStateFromTo->toWakdState ==wakdStates_Ultrafiltration || pWakdStateFromTo->toWakdState ==wakdStates_Regen2 ) )
												{ //if we begin an automatic cycle, remember the time and set counters back
													WAKDStatesTimes.OS_AutomaticCycleStart = getTime();
													WAKDOS_AutomaticDone.DialysisCount =0;
													WAKDOS_AutomaticDone.Regen1Count =0;
													WAKDOS_AutomaticDone.UltrafiltrationCount =0;
													WAKDOS_AutomaticDone.Regen2Count =0;
													#ifdef DEBUG_FC
														taskMessage("I", PREFIX, "Starting automatic cycle at time %d", WAKDStatesTimes.OS_AutomaticCycleStart);
													#endif
												}
											} // of if (changeIntoStateRequest(pWakdSta...
										} else {// Wakd state changed in the meantime. State change request is outdated, discard!
											taskMessage("W", PREFIX, "(NoDial) Changing State not possible. Request is outdated!");
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									}
									default:{
										defaultIdHandling(PREFIX, dataId);
										break;
									}
								} // for switch (dataId)
								break;
							} // case command_UseAtachedMsgHeader
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
							}
						} // for switch (Qtoken.command)
						break;
					} // for case wakdStates_NoDialysate:{
					case wakdStates_Maintenance: {
						switch (Qtoken.command) {
							case command_UseAtachedMsgHeader: {
								tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
								switch (dataId){
									case dataID_physicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysicalsensorData *pPhysicalsensorData = NULL;
										pPhysicalsensorData = (tdMsgPhysicalsensorData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved physical data. prFCI: %f. Discarding, not needed now.",(((float)pPhysicalsensorData->physicalsensorData.PressureFCI.Pressure) / ((float)FIXPOINTSHIFT_PRESSURE)));
										#endif
										sFree((void *)&pPhysicalsensorData);
										break;
									}
									case dataID_actuatorData: {
										// Flow control needs to know the flow of dialysate (should be = 0 here). Read this value from data.
										tdMsgActuatorData *pActuatorData = NULL;
										pActuatorData = (tdMsgActuatorData *) (Qtoken.pData);
										actFlow_mlPmin = ((sensorType)pActuatorData->actuatorData.FLPumpData.Flow) / ((sensorType)FIXPOINTSHIFT_PUMP_FLOW);
										sFree((void *)&pActuatorData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Recieved and processed ActuatData; DialFlow %f.", actFlow_mlPmin);
										#endif
										break;
									}
									case dataID_physiologicalData: {
										// Not needed in this state. Discard.
										tdMsgPhysiologicalData *pPhysiologicalSensorReadout = NULL;
										pPhysiologicalSensorReadout = (tdMsgPhysiologicalData *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Physiological Data recieverd. K+O %f. not needed now. Discarding.", (((float)pPhysiologicalSensorReadout->physiologicalData.ECPDataO.Potassium) / ((float) FIXPOINTSHIFT_K)));
										#endif
										sFree((void *)&pPhysiologicalSensorReadout);
										break;
									}
									case dataID_changeWAKDStateFromTo:{
										// Inter task communication using queues opens possible race conditions.
										// e.g. the UI could request a change of state from A to B. But during
										// transport of message through queues FC already switched from A to C.
										// When message now reaches here we must not change from C to B since this
										// was not the idea of the original message!
										tdMsgWakdStateFromTo *pWakdStateFromTo = NULL;
										pWakdStateFromTo = (tdMsgWakdStateFromTo *) (Qtoken.pData);
										#ifdef DEBUG_FC
											taskMessage("I", PREFIX, "Change State (now: Maint) - Request to %s", stateIdToString(pWakdStateFromTo->toWakdState));
										#endif
										if (pCurrentState->wakdStateIs == pWakdStateFromTo->fromWakdState && pCurrentState->wakdOpStateIs == pWakdStateFromTo->fromWakdOpState &&
											(pWakdStateFromTo->toWakdState == wakdStates_AllStopped)) { // only possible to change into All stopped
											// current state and request to change from state are equal. Request ist valid!
											// Inform WAKD tasks about new current state.
											if (changeIntoStateRequest(pWakdStateFromTo->toWakdState, pWakdStateFromTo->toWakdOpState, pCurrentState, qhDISPin))
											{	// Changing WAKD state has failed!
												taskMessage("E", PREFIX, "Changing state has failed: '%s'|'%s'.", stateIdToString(pWakdStateFromTo->toWakdState), opStateIdToString(pWakdStateFromTo->toWakdOpState));
												errMsg(errmsg_ErrFlowControl);
												// Return NACK to sender of MSG
												if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendNack);
												}
											} else {
												// Return ACK to sender of MSG
												if (sendACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
												{	// Could not even send NACK
													errMsg(errmsg_ErrSendAck);
												}
												WAKDStatesTimes.timelastStateChange = getTime(); // remember the time of state change, to be able to get out of it after preset time
											}
										} else {// Wakd state changed in the meantime. State change request is outdated, discard!
											#if defined DEBUG_FC || defined SEQ4
												taskMessage("W", PREFIX, "(Maint) Changing State not possible. Outdated or not allowed transition. Allowed: AllStopped!");
											#endif
											// Return NACK to sender of MSG
											if (sendNACK((tdMsgOnly *) (Qtoken.pData), qhDISPin))
											{	// Could not even send NACK
												errMsg(errmsg_ErrSendNack);
											}
										}
										// processed, discard data
										sFree((void *)&pWakdStateFromTo);
										break;
									} // case command_ChangeWAKDStateFromTo:
									default: {
										defaultIdHandling(PREFIX, dataId);
										break;
									}
								} // for switch (dataId)
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
							// never ever leave this case automatically, but just by user-interface request (handeled above)
						} // for switch (Qtoken.command)
						break;
					} // for case wakdStates_Maintenance:
					case (wakdStates_UndefinedInitializing): {
						// Whenever FC requested a change of state, the request will take some
						// time to travel to the real time board and be executed by it. The response
						// will be a msg "dataID_currentStateis". In between this time period, from request to
						// "stateIs" the current state is undefined and we end up here.
						
						if ( (getTime() - WAKDStatesTimes.timelastStateChange) > 5 ) 
						{ // after 5 seconds repeat the request

							if ( requestCounter >3) 
							{ // if repeated for 3 times without sucess, indicate error and restart!
								tdMsgOnly *pMsgReset = NULL;	// message to be sent 
								pMsgReset   = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
								if ( pMsgReset == NULL )
								{	// unable to allocate memory
									taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
									errMsg(errmsg_pvPortMallocFailed);
								} else {
									pMsgReset->header.dataId		= dataID_reset;
									pMsgReset->header.issuedBy		= whoId_MB;
									pMsgReset->header.msgSize		= 6;
									pMsgReset->header.recipientId	= whoId_MB;
									Qtoken.command = command_UseAtachedMsgHeader;
									Qtoken.pData   = (void *) pMsgReset;
									#if defined DEBUG_FC | defined SEQ0 | defined SEQ00
										taskMessage("I", PREFIX, "Stuck in wakdStates_UndefinedInitializing and waiting for configuraiton for too long.Initializing Reset by notifying dispatcher.");
									#endif
									if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
									{	// FC queue did not accept data. Free message and send error.
										taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_reset'!");
										errMsg(errmsg_QueueOfFlowControlTaskFull);
										sFree((void *)&pMsgReset);
									}
								}
							}
							requestCounter = requestCounter+1;
							#ifdef DEBUG_FC
								taskMessage("I", PREFIX, "Stuck in wakdStates_UndefinedInitializing and waiting for configuraiton for too long. Repeating Request");
							#endif
							getStateParametersFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &extractionEvery_sec, &WAKDStatesTimes, &WAKDStateRemember, pCurrentState, qhFCin, 0);
						}

						switch (Qtoken.command) {
							// The only messages we want to receive when we are in undefined state
							// are:
							// 1: command_InitializeFCReady after -> then set 
							// * so we can continue FC normal operation
							// 2: a "dataID_currentWAKDstateIs" which defines our current state.
							// * this message is already intercepted outside this switch above
							// * in the code at: "if ( (Qtoken.command == command_UseAtachedMsgHeader) && (((tdMsgOnly *)(Qtoken.pData))->header.dataId == dataID_currentWAKDstateIs) ){"
							// * So the only thing we can do here is delete all incoming messages with attached data
							// * until our desired msg has arrived ... and is intercepted above.
							case (command_InitializeFCReady): {
//								#if defined DEBUG_FC
								taskMessage("I", PREFIX, "Recieved reconfiguration. Now leaving wakdStates_UndefinedInitializing");
//								#endif
								requestCounter = 0;
								pCurrentState->wakdStateIs = FCConfigPointerStruct.pWAKDStateConfigure->thisState;
								processRecievedFCConfigFromDS(&FCConfigPointerStruct, qhDISPin, &ultrafiltrationFlow, &ultrafiltrationDuration, &WAKDStatesTimes, &extractionEvery_sec, &WAKDStateRemember);
								// DO NOT: WAKDStatesTimes.timelastStateChange = getTime() as we do not do this only when state-changed, but also when only configuration changed!
								break;
							}
							case (command_UseAtachedMsgHeader): {
								#if defined DEBUG_FC
								taskMessage("I", PREFIX, "Discarding 'command id %d' while being in undefined state (probably in between a state change).", Qtoken.command);
								#endif
								sFree(&(Qtoken.pData));
								break;
							}
							default:{
								defaultCommandHandling(PREFIX, Qtoken.command);
								break;
							}
						} // for switch (Qtoken.command)
						break;
					}
					default: {
						taskMessage("F", PREFIX, "task flow control is in an undefined state!");
						errMsg(errmsg_FlowControlStateIsUnknown);
						break;
					}
				} // for switch (pCurrentState->wakdStateIs)
			}
		} else {
			// We now waited for FC_PERIOD and nothing arrived in input queue.
			// But we should be rather busy. Issue a warning
			taskMessage("I", PREFIX, "Timeout on Flow Control Task queue. Keep waiting.");
		}
	}

}


//################### waterExtractionController ############################################################################
// *pWeightSensorReadout  	: contains weight measurement and it's timestamp
// *pParametersFC			: is the parametrization of the controlalgorithm (like preset of weight to loose)
// pExtracDone				: extractions done since last weight measurement (at least 20h ago)
// pExtractionsPERday		: computed number of extractions each day
// pG_toLoosePERday			: computed weight(grams) to loose per day
//
// ******************************************************************************************************************
// should be called everytime a new scale measurement arrives!
//####################################################################################################################>>
void waterExtractionController(tdWeightMeasure *pWeightMeasure, uint16_t *pUfiltFlow, 
	uint16_t *pUfiltDuration, tdPatientProfile *pPatientProfile,
	unsigned char *pExtracDone, sensorType *pExtractionsPERday, 
	tdUnixTimeType *poldTimestamp_Wgt, tdUnixTimeType *pExtractionEvery_sec, 
	tdWAKDStatesTimes *pWAKDStatesTimes, 
	xQueueHandle *pQueueHandle)
{
	// Initialize on first call of this function.
	static unsigned char  initialize   = 1;
	static int overweight_g = 0; // also negative (under-weight) numbers must be allowed
		
	
	if (initialize) {
		overweight_g = (pPatientProfile->wghtCtlDefRemPerDay / (float)FIXPOINTSHIFT_WEIGHTSCALE ) * 1000;	//FIXPOINTSHITFT into Kgrams, then multiply by 1000
		initialize   = 0;
	}
	// Water Extraction Controller
	if (pWeightMeasure->TimeStamp > *poldTimestamp_Wgt) {
		// calculate new frequency for water extaction
		tdUnixTimeType timeDelta = pWeightMeasure->TimeStamp - (*poldTimestamp_Wgt); 

		if ( timeDelta <= (20*60*60)) {
			taskMessage("I", PREFIX, "Rejecting weight measurement: at least 20 hours between two measurements!");
			#ifndef VP_SIMULATION
				// Patient has to be informed about this by sending an info to the GUI task
				// Needs to be implemented here!
				#warning(Not compiling for Virtual Platform (VP_SIMULATION not defined). This part needs implementation for real HW!)
			#endif
		}else { 
			// sufficient time has passed since last measurement
			overweight_g  = (int) ( ((float)pWeightMeasure->Weight - (float)pPatientProfile->wghtCtlTarget) *1000 / ((float)FIXPOINTSHIFT_WEIGHTSCALE));
					// *1000 converting into grams!
			// water to extract = extracted last period + overweight
			uint16_t mlPerExtraction     = (uint16_t)( ((float) *pUfiltFlow / (float)FIXPOINTSHIFT_PUMP_FLOW) * (((float) *pUfiltDuration) / 60));
			pPatientProfile->wghtCtlDefRemPerDay =  (uint32_t) ( ((unsigned short)((float)(*pExtracDone) * (float)mlPerExtraction)) 
										+ ((unsigned short)(86400*(int)overweight_g/(int)timeDelta)) ) /1000;	// 86400sec/day
			taskMessage("E", PREFIX, "now the new patient profile shall be saved in the DS!");
			
			*pExtractionsPERday = ( (1000.0 * (float)pPatientProfile->wghtCtlDefRemPerDay) / (float)FIXPOINTSHIFT_WEIGHTSCALE) / (float)mlPerExtraction ; // H2O: g=ml
			if (*pExtractionsPERday <=0) {
				*pExtractionsPERday   = 0;
				*pExtractionEvery_sec = 86400;
			} else {
				*pExtractionEvery_sec = 86400/(*pExtractionsPERday);
			}
			#ifdef DEBUG_FC
				taskMessage("I", PREFIX, "waterExtractionController calculated new ExtractionsPERday %i ",*pExtractionsPERday);
			#endif
					
			*poldTimestamp_Wgt = pWeightMeasure->TimeStamp;
			(*pExtracDone)=0; //important to set this counter back
			
			// put the weight measurement into the patient profile
			pPatientProfile->wghtCtlLastMeasurement = (uint32_t) pWeightMeasure->Weight;
			// and (together with new wghtCtlDefRemPerDay) make a copy and send it to datastorage
			storeFCDatainDS(pPatientProfile, pQueueHandle);
			
			if (pWAKDStatesTimes->DialysisDuration>0 && pWAKDStatesTimes->Regen1Duration>0 && pWAKDStatesTimes->UltrafiltrationDuration && pWAKDStatesTimes->Regen2Duration)
			{ //only when all the times were already initialized perform the following check: 
			// compare if needed time for the regen1&2&uf is smaller than actual whole operaiton Cycle (defined by UF-period)
				// int minDialsysisDuration= pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration; 
				// time of Dialysis should be at least just as long as all other modes together (so at least half of the time should be dialysis)
				int minDialsysisDuration= 80; // Alternative for Debugging: 80 Seconds of Dialysis are assumed as absolute minumim!
				if ( (pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration + (80)) > *pExtractionEvery_sec )
				if ( (pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration + (minDialsysisDuration)) > *pExtractionEvery_sec )
				{ 
					*pExtractionEvery_sec = pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration + (minDialsysisDuration);
					#ifdef DEBUG_FC
					taskMessage("E", PREFIX, "Unable to perform frequency of Ultrafiltration in the defined time for Operational Modes! Reducing frequency for now - but user has to change parametrizaiton!" );
					taskMessage("I", PREFIX, "extractionEvery_sec is now %d", *pExtractionEvery_sec);
					#endif
					errMsg(errmsg_ConfiguredStateTimesMismatch);
				}
			}
		} //if ( timeDelta <= (20*60*60))
	}
} //################### waterExtractionController ############################################################################<<


//################### dialysisState_ComputeNewVolt ############################################################################
// *variable  	: reason
//
// ******************************************************************************************************************
// takes the actual urea concnetrations, calculates the acutal decomposition
// rate, compares it to the preset decomposition rate, lowers/increases
// voltage based on discrepancy between preset and actual decomposition rate
// ******************************************************************************************************************
// should be called everytime new chemicals concentrations measurements arrives while DIALYSIS!
//####################################################################################################################>>
void dialysisState_ComputeNewVolt(sensorType *ADRUr_mmolPmin_preset,  
	sensorType *ADRUr_mmolPmin_now, 
	sensorType *deviatSum_Ur_mmolPl, 
	uint16_t ECPO_Ur, 
	tdWAKDStateConfigurationParameters *pWAKDStateConfigure, 
	tdPatientProfile *pPatientProfile)
{
	int16_t deviat_Ur_mmolPmin = (ADRUr_mmolPmin_preset-ADRUr_mmolPmin_now);
	*deviatSum_Ur_mmolPl = (*deviatSum_Ur_mmolPl) + deviat_Ur_mmolPmin;

	static unsigned int  ECPO_Ur_tmp=0;

	const uint8_t Kp_Ur=12;
	const uint8_t Ki_Ur=0;

	uint16_t defPol_tmp = pWAKDStateConfigure->defPol_V;


	pWAKDStateConfigure->defPol_V = (uint16_t) (  ((Kp_Ur * deviat_Ur_mmolPmin) + (Ki_Ur * (*deviatSum_Ur_mmolPl) )) * FIXPOINTSHIFT_POLARIZ_VOLT  );
	
	if (pWAKDStateConfigure->defPol_V > pPatientProfile->maxDialVoltage) { //patientProfile.Voltages and stateDialysisConfig are all uint16
		pWAKDStateConfigure->defPol_V = pPatientProfile->maxDialVoltage;
	}  else if (pWAKDStateConfigure->defPol_V < pPatientProfile->minDialVoltage) {
		pWAKDStateConfigure->defPol_V = pPatientProfile->minDialVoltage;
	}


	if ((ECPO_Ur > ECPO_Ur_tmp) && ((pWAKDStateConfigure->defPol_V)<defPol_tmp)) {
		#ifdef DEBUG_FC
			taskMessage("I", PREFIX, "ECPO_Ur %i",ECPO_Ur);
			taskMessage("I", PREFIX, "            > ECPO_Ur_tmp %i",ECPO_Ur_tmp);
			taskMessage("I", PREFIX, "Volt setting %i",pWAKDStateConfigure->defPol_V);
			taskMessage("I", PREFIX, "            > defPol_tmp %i",defPol_tmp);
			taskMessage("I", PREFIX, "Hindering decrease of actuator-Voltage due to concentration increasing");
		#endif
		pWAKDStateConfigure->defPol_V  = defPol_tmp;
	}

	#ifdef DEBUG_FC
		taskMessage("I", PREFIX, "Voltage for decomposition now: %i", (int) pWAKDStateConfigure->defPol_V);
	#endif

	ECPO_Ur_tmp = ECPO_Ur;

//add some error handling here ? if bad things happen during the calculation, division by 0 like?

}//################### dialysisState_ComputeNewVolt ############################################################################<<


//################### dialysisState_ComputeNewPumpspeed ############################################################################
// *ADRK_mmolPmin_preset  		: adsorption preset
// *ADRK_mmolPmin_now  			: actual adsorption rate
// ECPI_K_mmolPl, ECPO_K_mmolPl : concentraiton measuremetns
// ******************************************************************************************************************
// takes the actual adsorption rate
// compares it to the preset adsorption rate, lowers/increases
// pump-speed based on discrepancy between preset and actual adsoption rate
// ******************************************************************************************************************
// should be called everytime new chemicals concentrations measurements arrives while DIALYSIS!
//####################################################################################################################>>
void dialysisState_ComputeNewPumpspeed(sensorType *ADRK_mmolPmin_preset,  
	sensorType *ADRK_mmolPmin_now, 
	sensorType *deviatSum_K, 
	uint16_t ECPO_K_mmolPl,  // 'o' is out of the HFD, an therefore before the ADF
	//uint16_t ECPI_K_mmolPl, // parameter needed to be displayed for debuging
	tdWAKDStateConfigurationParameters *pWAKDStateConfigure, 
	tdPatientProfile *pPatientProfile)
{
	//ADRK_mmolPmin_nowO=ADRK_mmolPmin_now;
	sensorType deviat_K_mmolPmin = (sensorType) (*ADRK_mmolPmin_preset) - (*ADRK_mmolPmin_now);	 //FIXPOINTSHITFT (make it sure) for the inputs so here only we have real floats
	*deviatSum_K  = (sensorType) ((*deviatSum_K) + deviat_K_mmolPmin);

	static sensorType  ECPI_K_tmp=0; // this variable value needs to survive in between the funciton calls 

	const uint8_t Kp_K = 190; // 10
	const uint8_t Ki_K = 45;  // 60

	sensorType plasFlowP = (sensorType) (Kp_K * deviat_K_mmolPmin);
	sensorType plasFlowI = (sensorType) (Ki_K * (*deviatSum_K));
	if (plasFlowI > 70.0) {
		(*deviatSum_K) = 70/Ki_K; // ****this ceil-s I-controller
	}

	uint16_t defSpeed_tmp;
	defSpeed_tmp = (uint16_t) (pWAKDStateConfigure->defSpeedFp_mlPmin) ;

	pWAKDStateConfigure->defSpeedFp_mlPmin = (uint16_t) round((plasFlowI + plasFlowP) * (float)FIXPOINTSHIFT_PUMP_FLOW);
		
	if (pWAKDStateConfigure->defSpeedFp_mlPmin > pPatientProfile->maxDialPlasFlow) {
		pWAKDStateConfigure->defSpeedFp_mlPmin = pPatientProfile->maxDialPlasFlow;		//patientProfile Flow values are uint16_t
	}  else if (pWAKDStateConfigure->defSpeedFp_mlPmin < pPatientProfile->minDialPlasFlow ) { 
		pWAKDStateConfigure->defSpeedFp_mlPmin = pPatientProfile->minDialPlasFlow;
	}

	#ifdef DEBUG_FC
		//taskMessage("I", PREFIX, ""ECPI_K_mmolPl %i || ECPO_K_mmolPl %i, 'o' is out of HFD", ECPI_K_mmolPl, ECPO_K_mmolPl);
		taskMessage("I", PREFIX, "max: %i", pPatientProfile->maxDialPlasFlow);
		taskMessage("I", PREFIX, "min: %i", pPatientProfile->minDialPlasFlow);
		taskMessage("I", PREFIX, "Pump P: %f", plasFlowP);
		taskMessage("I", PREFIX, "Pump I: %f", plasFlowI);
		taskMessage("I", PREFIX, "SUM I: %f", (plasFlowI + plasFlowP));
		taskMessage("I", PREFIX, "Speed of Dialysate Pump now: %i ml per minute", (int) ((float)pWAKDStateConfigure->defSpeedFp_mlPmin / (float)FIXPOINTSHIFT_PUMP_FLOW) ) ;
	#endif

	// ECPI_K_tmp = ECPI_K_mmolPl;
} //################### dialysisState_ComputeNewPumpspeed ############################################################################<<



//################### keepTrackAdsorptions ############################################################################
// *ADRK_mmolPmin_now  		    : actual adsorption Rate
// *K_mmol_adsorbed_thisADphase : already adsorbed this phase
// *...
//
// ******************************************************************************************************************
// takes the actual K+ concentrations, calculates the acutal adsorption
// rate, compares it to the preset adsorption rate, lowers/increases
// pump-speed based on discrepancy between preset and actual adsoption rate
// ******************************************************************************************************************
// should be called everytime new chemicals concentrations measurements arrives while DIALYSIS!
//####################################################################################################################>>
void keepTrackAdsorptions(sensorType *ADRK_mmolPmin_now, sensorType *ADRUr_mmolPmin_now, sensorType *K_mmol_adsorbed_thisADphase, sensorType *Ur_mmol_adsorbed_thisADphase, tdMsgPhysiologicalData *pDialysisPhysiologicalReadout, sensorType *actFlow_mlPmin, tdUnixTimeType newTimestamp_Flow, tdUnixTimeType *oldTimestamp_Flow)
 // computing online the amount of adsorbed UREA and K+
{
	/*
	This should happen EVERYTIME a new pumprate is recieved! to not miss some (e.g. manual) changes of pumpspeed -> therefore changes in adsorptionrate(=pumpspeed*concentrationDifference)
	neverteless : we assume that for every computation of "K_mmol_adsorbed_thisADphase" the chemical values are the same as the last read out, since no higher frequent information is available.
	*/
	*ADRK_mmolPmin_now = ( 
	(  ((sensorType)(pDialysisPhysiologicalReadout->physiologicalData.ECPDataO.Potassium)) - ((sensorType)(pDialysisPhysiologicalReadout->physiologicalData.ECPDataI.Potassium))  )
		/ ((sensorType) FIXPOINTSHIFT_K) // O is out of the HFD (in the ADF) and I the other one
		* ((*actFlow_mlPmin) /1000.0)  );
	
	*ADRUr_mmolPmin_now =( 
	( ((sensorType)(pDialysisPhysiologicalReadout->physiologicalData.ECPDataO.Urea))
		- ((sensorType)(pDialysisPhysiologicalReadout->physiologicalData.ECPDataI.Urea)) )
		/ ((sensorType) FIXPOINTSHIFT_UREA) 
		*  ((*actFlow_mlPmin) /1000)   ); 
	
	
	if (*oldTimestamp_Flow> 0.0) {
		// only if one measurement has been taken in the past, it makes sense to compute the adsorbed amount (=integral over time)
		// assuming that 4Byte float does handle upt to +3.4*10^38 this shall not cause violations
		*K_mmol_adsorbed_thisADphase = *K_mmol_adsorbed_thisADphase 
			+ ( ((*ADRK_mmolPmin_now) * 
					(  ((sensorType)newTimestamp_Flow)   - ((sensorType)(*oldTimestamp_Flow)) )  )/ 60.0 ); // 60 converts from sec to minutes!
		*Ur_mmol_adsorbed_thisADphase = *Ur_mmol_adsorbed_thisADphase 
			+ ( ((*ADRUr_mmolPmin_now) *
				   (  ((sensorType)newTimestamp_Flow)  -  ((sensorType)(*oldTimestamp_Flow)) )  )/ 60.0) ; 
			
	} else {
		*K_mmol_adsorbed_thisADphase = 0.0;
		*Ur_mmol_adsorbed_thisADphase = 0.0;
	}
	*oldTimestamp_Flow=newTimestamp_Flow;
}


//################### getStateParametersFromDS ############################################################################
// call getStateParametersFromDS() to initialyse the RTB at the beginning and everytime we recieved dataID_currentWAKDstateIs
// waitforDS: lets the task wait until ready! do this when starting up FC, cause we cannot do anything meaningful until initialization of all states is done!
//####################################################################################################################>>
void getStateParametersFromDS(
		tdFCConfigPointerStruct *pFCConfigPointerStruct,
		xQueueHandle *pQueueHandle,
		uint16_t *pUfiltFlow,
		uint16_t *pUfiltDuration,
		tdUnixTimeType *pextractionEvery_sec,
		tdWAKDStatesTimes *pWAKDStatesTimes,
		tdWakdStates *pWAKDStateRemember,
		tdMsgCurrentWAKDstateIs *pCurrentState,
		xQueueHandle *pQHFC,
		unsigned char waitforDS)
{
	tdQtoken Qtoken;
	// xTaskHandle *handleFC   = ((tdAllHandles *) pvParameters)->allTaskHandles.handleFC;
	#if defined DEBUG_FC || defined SEQ0 || defined SEQ13
		taskMessage("I", PREFIX, "starting getStateParametersFromDS()");
	#endif

	if (pFCConfigPointerStruct->pWAKDStateConfigure->thisState != wakdStates_UndefinedInitializing) {


		// this flag is set when doing the initialization of the task (at the beginning)
		Qtoken.command	= command_InitializeFC;
		Qtoken.pData	= (void*) pFCConfigPointerStruct;
		#if defined DEBUG_FC || defined SEQ0 || defined SEQ13
			taskMessage("I", PREFIX, "Sending 'command_InitializeFC' to obtain PatientProfile and StateConfiguration for %s.",stateIdToString(pFCConfigPointerStruct->pWAKDStateConfigure->thisState));
		#endif
		if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full. Do something accordingly!
			taskMessage("E", PREFIX, "Queue of Dispatcher did not accept 'command_InitializeFC'.");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree(&(Qtoken.pData));
		}	
		
		if (waitforDS) {
			// this flag is set=1 when doing the initialization of the task (at the beginning)
			// do not do this in some reconfiguration during runtime of FC, because it will erase all incoming messages!
			
			#if defined DEBUG_FC || defined SEQ0
			taskMessage("I", PREFIX, "FC in busy loop - waiting for right reponse from DS.");
			#endif
			
			int gotTheResponse=0;
			while (!gotTheResponse) {
				if ( xQueueReceive( pQHFC, &Qtoken, FC_PERIOD) == pdPASS )
				{
					if (Qtoken.command == command_InitializeFCReady)
					{
						gotTheResponse=1; // this is just to leave the while()
												
						processRecievedFCConfigFromDS(pFCConfigPointerStruct, pQueueHandle, pUfiltFlow, pUfiltDuration, pWAKDStatesTimes, pextractionEvery_sec, pWAKDStateRemember);
					} else {
					
						sFree(&(Qtoken.pData));
						taskMessage("E", PREFIX, "getStateParametersFromDS() Message to arrive should be 'command_InitializeFCReady'.");
						errMsg(errmsg_ErrFlowControl);
					} // of if (Qtoken.command ==...
				} // of if ( xQueueReceive( pQHFC, ...
			} // of while...
				
		} else {
			#if defined DEBUG_FC || defined SEQ0 || defined SEQ13
				taskMessage("I", PREFIX, "getStateParametersFromDS() putting FC into wakdStates_UndefinedInitializing - will be left when command_InitializeFCReady recieved from DS.");
			#endif
			// change FC's local assumpiton of which state we are in into UndefinedInitializing 
			// pCurrentState->wakdStateIs is malloced by dispatcher for FC exclusively.
			pCurrentState->wakdStateIs = wakdStates_UndefinedInitializing; 
			pWAKDStatesTimes->timelastStateChange = getTime(); // this information needed to change the operational states
		} // of if (waitforDS)...
			
		
		
	} else{ 
		taskMessage("I", PREFIX, "Skipping FC reconfiguration, since we are in 'wakdStates_UndefinedInitializing'");
	}
}//################### getStateParametersFromDS ############################################################################<<



//################### storeFCDatainDS ############################################################################
// *pPatientProfile  		    : data to be stored
//####################################################################################################################>>
void storeFCDatainDS(tdPatientProfile *pPatientProfile, xQueueHandle *pQueueHandle) {
	
	tdQtoken Qtoken;
	tdMsgPatientProfileData *pMsgPatientProfileData = NULL;
	pMsgPatientProfileData = (tdMsgPatientProfileData *) pvPortMalloc(sizeof(tdMsgPatientProfileData));
	pMsgPatientProfileData->header.dataId	= dataID_PatientProfile;
	pMsgPatientProfileData->header.issuedBy	= whoId_MB;
	pMsgPatientProfileData->header.recipientId	= whoId_RTB;
	pMsgPatientProfileData->patientProfile = *pPatientProfile;
	
	Qtoken.command	= command_UseAtachedMsgHeader;
	Qtoken.pData	= (void *) pMsgPatientProfileData;			

	if ( xQueueSend( pQueueHandle, &Qtoken, FC_BLOCKING) != pdPASS )
	{	// Seemingly the queue is full for too long time. Do something accordingly!
		taskMessage("E", PREFIX, "Queue of DISP did not accept 'command_UseAtachedMsgHeader' with tdMsgPatientProfileData!");
		errMsg(errmsg_QueueOfDispatcherTaskFull);
		sFree(&(Qtoken.pData));
	};
} //################### storeFCDatainDS ############################################################################<<




//################### processRecievedFCConfigFromDS ################################################################
// check what configuration we have just got and process it
//####################################################################################################################>>
void processRecievedFCConfigFromDS(	
	tdFCConfigPointerStruct *pFCConfigPointerStruct,
	xQueueHandle *pQHDISPin,
	uint16_t *pUfiltFlow,
	uint16_t *pUfiltDuration,
	tdWAKDStatesTimes *pWAKDStatesTimes,
	tdUnixTimeType *pextractionEvery_sec,
	tdWakdStates *pWAKDStateRemember)
	{
	#if defined DEBUG_FC
	taskMessage("I", PREFIX, "got the 'command_InitializeFCReady'. now inside processRecievedFCConfigFromDS() recieved FCConfig from DS");
	#endif

	// if we changed (and configured) state, remember the time this happened to be able to change out of this state in time
	pWAKDStatesTimes->timelastStateChange = getTime(); // this information needed to change the operational states
			
	*pWAKDStateRemember = pFCConfigPointerStruct->pWAKDStateConfigure->thisState;
	switch (pFCConfigPointerStruct->pWAKDStateConfigure->thisState) {
		case (wakdStates_Dialysis): {
			pWAKDStatesTimes->DialysisDuration = pFCConfigPointerStruct->pWAKDStateConfigure->duration_sec;
			break;
		}
		case (wakdStates_Regen1): {
			pWAKDStatesTimes->Regen1Duration = pFCConfigPointerStruct->pWAKDStateConfigure->duration_sec;
			break;
		}
		case (wakdStates_Ultrafiltration): {
			pWAKDStatesTimes->UltrafiltrationDuration = pFCConfigPointerStruct->pWAKDStateConfigure->duration_sec;
			*pUfiltFlow= (sensorType) pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedFp_mlPmin / (float)FIXPOINTSHIFT_PUMP_FLOW;
			*pUfiltDuration= pFCConfigPointerStruct->pWAKDStateConfigure->duration_sec;
			break;
		}
		case (wakdStates_Regen2): {
			pWAKDStatesTimes->Regen2Duration = pFCConfigPointerStruct->pWAKDStateConfigure->duration_sec;
			break;
		}	
		case (wakdStates_UndefinedInitializing): {
			// there is no configuration for this state! so if we recieve some, this is an error!
			taskMessage("E", PREFIX, "Recieved configuration for wakdStates_UndefinedInitializing - this is most likely an error!");
			break;
		}	
		case (wakdStates_AllStopped): {
			if (pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedBp_mlPmin!=0 
				|| pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedFp_mlPmin!=0 
				|| pFCConfigPointerStruct->pWAKDStateConfigure->defPol_V!=0) {
				// set all the parameters back to zero
				pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedBp_mlPmin = 0;
				pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedFp_mlPmin = 0;
				pFCConfigPointerStruct->pWAKDStateConfigure->defPol_V = 0;
				taskMessage("E", PREFIX, "Configuration of WAKD state wakdStates_AllStopped, actuator presets were ignored and assumed to be '0'. The presets MUST  be changed via the user interface");
			}
			break;
		}
		case (wakdStates_Maintenance): {
			break;
		}
		case (wakdStates_NoDialysate): {
			if (pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedFp_mlPmin!=0 
				|| pFCConfigPointerStruct->pWAKDStateConfigure->defPol_V!=0) {
				// set all the parameters back to zero
				pFCConfigPointerStruct->pWAKDStateConfigure->defSpeedFp_mlPmin = 0;
				pFCConfigPointerStruct->pWAKDStateConfigure->defPol_V = 0;
				taskMessage("E", PREFIX, "Configuration of WAKD state wakdStates_NoDialysate, actuator presets were ignored and assumed to be '0'. The presets MUST  be changed via the user interface");
			}
			break;
		}
	} // of switch (pFCConfigPointerStruct->pWAKDStateConfigure->thisState)
	
		if (pWAKDStatesTimes->DialysisDuration>0 && pWAKDStatesTimes->Regen1Duration>0 && pWAKDStatesTimes->UltrafiltrationDuration && pWAKDStatesTimes->Regen2Duration)
		{ //only when all the times were already initialized perform the following check: 
		// compare if needed time for the regen1&2&uf is smaller than actual whole Operaiton Cycle (defined by UF-period)
			if ( (pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration + (80))  > *pextractionEvery_sec )
			{ // 80 Seconds of Dialysis are assumed as absolute minumim!
				*pextractionEvery_sec = pWAKDStatesTimes->Regen1Duration + pWAKDStatesTimes->UltrafiltrationDuration + pWAKDStatesTimes->Regen2Duration + (80);
				taskMessage("E", PREFIX, "Unable to perform frequency of Ultrafiltration in the defined time for Operational Modes! Reducing frequency for now - but user has to change parametrizaiton!" );
				taskMessage("E", PREFIX, "extractionEvery_sec is now %d", *pextractionEvery_sec);
				errMsg(errmsg_ConfiguredStateTimesMismatch);
			}
		}
	
	// and finally also send the new settings to the RTB
	if (sendWakdStateConfig(pFCConfigPointerStruct->pWAKDStateConfigure->thisState, pFCConfigPointerStruct->pWAKDStateConfigure, pQHDISPin))
	{	// configuring WAKD state has failed!
		taskMessage("E", PREFIX, "Configuration of WAKD state failed '%s'.", stateIdToString(pFCConfigPointerStruct->pWAKDStateConfigure->thisState) );
		errMsg(errmsg_ConfiguringWakdStateFailed);
	}

}//################### processRecievedFCConfigFromDS ################################################################<<



















