/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef INTERFACES_H
#define INTERFACES_H 
 
// Handy definitions from CSEM.
#include "global.h"
// Definitions mostly relevant between Smartphone and WAKD
#include "SPinterfaces.h"

// For easier overview on the large file, the enumeration types have been
// moved into their own file.
#include "enums.h"

#define sensorType    float
#define actuatorType  float

// Below: general definitions for all
 
// Unix time, or POSIX time, is a system for describing points in time, defined
// as the number of seconds elapsed since midnight Coordinated Universal Time (UTC)
// of January 1, 1970, not counting leap seconds.
typedef uint32_t tdUnixTimeType;

// Resolution of "tdUnixTimeType" is 1 second. This is not sufficient for sensor sampling
// in the range of ms. RTB is using a second relative time that "counts" ms.
// in the range of ms. RTB gives aditionally a OFFSET from the tdUnixTimeType in ms.
typedef uint32_t tdMsTimeType;
 
// Definitions to declare the data structures comming from the sensors.
#include "sensors.h"

// Data transfered follows the "Network Byte Order" standard:
// The order in which the bytes of a multi-byte number are transmitted on a
// network - most significant byte first (as in "big-endian" storage). This may or
// may not match the order in which numbers are normally stored in memory for a
// particular processor. 

// Any data transmission requires this structure as a header.
// For asynchronous communication (e.g. SP to MB) it is necessary to Ack/Nack a request from sender.
// Sender includes a rising (NOT '0'!!) "id" number, and when receiver Ack/Nack the message, he will include this
// same number, so that sender knows to which message it refers. "issuedBy" identifies the sender of the id.
// The id '0' is to be interpreted as "no id issued"!
typedef struct strHeader
{
	uint8_t		recipientId;	// Byte 0 of all messages
	uint16_t    msgSize;		// Byte 1 and 2 size
	uint8_t 	dataId;         // Byte 3
	uint8_t		msgCount;		// Byte 4: rising number (NOT '0') of messages sent
	uint8_t		issuedBy;		// Byte 5: instance that issued the id and waits for an Ack/Nack.
} tdHeader;

// Message only without any data attached to it. The dataId in the header contains the complete
// information as e.g. commands like "execute reset" (dataId=dataID_reset)
typedef struct strMsgOnly	// e.g. request to RTB to perform reset
{
	tdHeader	header;			// e.g. myvar.header.dataId = dataID_reset
} tdMsgOnly;

// Message to communicate e.g. enum-mesages produced by the decision trees.
typedef struct strEnumMsg	// e.g. request to RTB to perform reset
{
	tdHeader	header;			// e.g. myvar.header.dataId = dataID_reset
	uint16_t	msgEnum;
} tdMsgSystemInfo;


// Struct for data package to adjust the timer counter of recipient. dataId must always have the value of "dataID_setTime"
// The recipient will use "TimeStamp" to adjust its internal absolute clock.
typedef struct strMsgSetTimeDate
{
	tdHeader		header;		// !! ALWAYS for this struct: myvar.header.dataId = dataID_setTime;
	tdUnixTimeType	TimeStamp;	// Seconds counted after January 1, 1970 (POSIX time)
} tdMsgSetTimeDate;

// Structure to inform remainder of the System (GUI, SP, Clinic Server) what the WAKD state is.
typedef struct strMsgCurrentWAKDstateIs
{
	tdHeader	header;			// !! ALWAYS for this struct: myvar.header.dataId = dataID_CurrentWAKDstateIs
	uint8_t		wakdStateIs;
	uint8_t		wakdOpStateIs;
} tdMsgCurrentWAKDstateIs;

// Required type for message to request FC to make a state change. Since it is possible
// that from the time of sending the message to FC until it reaches FC it could have happened
// that FC already switched into another state, it is required that the request defines a clear
// from-to pair.
// e.g. FC is in state A
// GUI sends "change from A to B"
// FC changes due to another event from A to C
// GUI message reaches FC. Message has to be discarded, since it is now obsolete.
// If we did not discard the message, we would have changed from C to B instead from A to B as intended!
typedef struct strMsgWakdStateFromTo
{
	tdHeader	header;			// !! ALWAYS for this struct: myvar.header.dataId = dataID_ChangeWAKDStateFromTo
	uint8_t		fromWakdState;
	uint8_t 	fromWakdOpState;
	uint8_t 	toWakdState;
	uint8_t		toWakdOpState;
} tdMsgWakdStateFromTo;

// Struct with all actuator values. Only valid recipient of this data package would be the
// Real Time Board. RTB will set all WAKD actuators of a state according to the values included.

typedef struct strMsgConfigureState
{
	tdHeader			header;				// !! ALWAYS: myvar.header.dataId = dataID_configureState
	tdWakdStates		stateToConfigure;
        tdActCtrl			actCtrl;
} tdMsgConfigureState;

// Struct to transmit weight measure from weight scale. Weight is measured in 100 grams!
typedef struct strMsgWeightData
{
	tdHeader		header;			// !! ALWAYS: myvar.header.dataId = dataID_weightData;
	tdWeightMeasure	weightData;
} tdMsgWeightData;

typedef struct strMsgEcgData
{
	tdHeader		header;
	tdEcgData		ecgData;
} tdMsgEcgData;

typedef struct strMsgBpData
{
	tdHeader		header;
	tdBpData		bpData;
} tdMsgBpData;

typedef struct strPatientProfile 	// keeping the values here as floats, when comparing to actual measuremetns, need to appy the Fixpointshift, first!
{
	char		name[50];				// if you change it here, change it in patient_read.c too
	char		gender;					// m/f
	uint8_t		measureTimeHours;		// daily time for weight & bp measurement, hour
	uint8_t		measureTimeMinutes;		// daily time for weight & bp measurement, minute
	uint32_t	wghtCtlTarget;			// weight control target [same unit as weigth measurement on RTB]
	uint32_t	wghtCtlLastMeasurement;	// weight control last measurement [same unit as weigth measurement on RTB]
	uint32_t	wghtCtlDefRemPerDay;	// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
	uint32_t	kCtlTarget;				// potassium control target use FIXPOINTSHIFT for [mmol/l]
	uint32_t	kCtlLastMeasurement;	// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
	uint32_t	kCtlDefRemPerDay;		// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
	uint32_t	urCtlTarget;			// urea control target use FIXPOINTSHIFT for [mmol/l]
	uint32_t	urCtlLastMeasurement;	// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
	uint32_t	urCtlDefRemPerDay;		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
	uint16_t  	minDialPlasFlow;		// minimal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
	uint16_t  	maxDialPlasFlow;		// maximal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
	uint16_t	minDialVoltage;			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
	uint16_t	maxDialVoltage;			// maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
} tdPatientProfile;

typedef struct strMsgPatientProfileData
{
	tdHeader		header;
	tdPatientProfile patientProfile;
} tdMsgPatientProfileData;

typedef struct strParametersSCC	// keeping the values here as floats, when comparing to actual measuremetns, need to appy the Fixpointshift, first!
{
	tdWakdStates   thisState;			// identifies which state is configured here	
  	sensorType NaAbsL;     // lower Na absolute threshold
	sensorType NaAbsH;     // upper Na absolute threshold
	sensorType NaTrdLT;			// lower Na time period
	sensorType NaTrdHT;			// upper Na time period
	sensorType NaTrdLPsT;   // lower Na trend per Specified Time threshold
	sensorType NaTrdHPsT;    // upper Na trend per Specified Time threshold
	
	sensorType KAbsL;      // lower K absolute threshold
	sensorType KAbsH;        // upper K absolute threshold
	sensorType KAAL;         // lower K absolute alarm threshold
	sensorType KAAH;         // upper K absolute alarm threshold
	sensorType KTrdLT;			// lower K trend low time-difference
	sensorType KTrdHT;			// upper K trend low time-difference
	sensorType KTrdLPsT;     // lower K trend per Specified Time threshold
	sensorType KTrdHPsT;     // upper K trend per Specified Time threshold

	sensorType CaAbsL;    // lower Ca absolute threshold
	sensorType CaAbsH;     // upper Ca absolute threshold
	sensorType CaAAbsL;      // lower K absolute alarm threshold
	sensorType CaAAbsH;      // upper K absolute alarm threshold
	sensorType CaTrdLT;			// lower Ca time period
	sensorType CaTrdHT;			// upper Ca time period
	sensorType CaTrdLPsT; // lower Ca trend per Specified Time threshold
	sensorType CaTrdHPsT; // upper Ca trend per Specified Time threshold
	
	sensorType UreaAAbsH;   // upper Urea absolute alarm threshold
	sensorType UreaTrdT;    // upper Urea time period
	sensorType UreaTrdHPsT; // upper Urea trend per Specified Time threshold

	sensorType CreaAbsL;  // lower Crea absolute threshold
	sensorType CreaAbsH;   // upper Crea absolute threshold
	sensorType CreaTrdT;  // trend Crea time period
	sensorType CreaTrdHPsT;  // upper Crea trend per Specified Time threshold

	sensorType PhosAbsL;   // lower Phos absolute threshold
	sensorType PhosAbsH;   // higher Phos absolute threshold
	sensorType PhosAAbsH;    // upper Phos absolute alarm threshold
	sensorType PhosTrdLT;		// lower Phos time period
	sensorType PhosTrdHT;		// upper Phos time period
	sensorType PhosTrdLPsT;// lower Phos trend per Specified Time threshold
	sensorType PhosTrdHPsT;// upper Phos trend per Specified Time threshold

	sensorType HCO3AAbsL;   // lower HCO3 absolute alarm threshold
	sensorType HCO3AbsL;    // lower HCO3 absolute threshold
	sensorType HCO3AbsH;    // upper HCO3 absolute threshold
	sensorType HCO3TrdLT;		// lower HCO3 time period
	sensorType HCO3TrdHT;		// upper HCO3 time period
	sensorType HCO3TrdLPsT; // lower HCO3 trend per Specified Time threshold
	sensorType HCO3TrdHPsT;  // upper HCO3 trend per Specified Time threshold

	sensorType PhAAbsL;    // lower pH absolute alarm threshold
	sensorType PhAAbsH;    // upper pH absolute alarm threshold
	sensorType PhAbsL;    // lower pH absolute threshold
	sensorType PhAbsH;    // upper pH absolute threshold
	sensorType PhTrdLT;			// lower pH time period
	sensorType PhTrdHT;			// upper pH time period
	sensorType PhTrdLPsT; // lower pH trend per Specified Time threshold
	sensorType PhTrdHPsT; // upper pH trend per Specified Time threshold

	sensorType BPsysAbsL;   // lower BPsys absolute threshold
	sensorType BPsysAbsH;  // upper BPsys absolute threshold
	sensorType BPsysAAbsH; // upper BPsys absolute alarm threshold
	sensorType BPdiaAbsL;   // lower BPdia absolute threshold
	sensorType BPdiaAbsH;  // upper BPdia absolute threshold    
	sensorType BPdiaAAbsH; // upper BPdia absolute alarm threshold

	sensorType pumpFaccDevi;	// accepted flow deviation ml/min
	sensorType pumpBaccDevi;	// accepted flow deviation ml/min

	sensorType VertDeflecitonT;	// accepted vertical deflection threshold

	sensorType WghtAbsL;     // lower Weight absolute threshold
	sensorType WghtAbsH;   // upper Weight absolute threshold
	sensorType WghtTrdT;   // Weight thrend threshold (from day to day)
	sensorType FDpTL;    // Fluid Extraction Deviation Percentage Threshold Low
	sensorType FDpTH;    // Fluid Extraction Deviation Percentage Threshold High

	sensorType BPSoTH;     // sensor 'BPSo' pressure upper threshold
	sensorType BPSoTL;       // sensor 'BPSo' pressure lower threshold
	sensorType BPSiTH;     // sensor 'BPSi' pressure upper threshold
	sensorType BPSiTL;       // sensor 'BPSi' pressure lower threshold
	sensorType FPSoTH;     // sensor 'FPS1' pressure upper threshold
	sensorType FPSoTL;       // sensor 'FPS1' pressure lower threshold
	sensorType FPSTH;     // sensor 'FPS2' pressure upper threshold
	sensorType FPSTL;       // sensor 'FPS2' pressure lower threshold

	sensorType BTSoTH;     // temperature sensor BTSo value upper threshold
	sensorType BTSoTL;       // temperature sensor BTSo value lower threshold
	sensorType BTSiTH;      // temperature sensor BTSi value upper threshold
	sensorType BTSiTL;       // temperature sensor BTSi value lower threshold

    sensorType BatStatT;   //battery status threshold

	// Expected adsorption rate calculation
	// parameters named according to Nanodialysis funciton of the sorbend unit 
	sensorType f_K;	
	sensorType SCAP_K;
	sensorType P_K;
	sensorType Fref_K;
	sensorType cap_K;
	sensorType f_Ph;
	sensorType SCAP_Ph;
	sensorType P_Ph;
	sensorType Fref_Ph;
	sensorType cap_Ph;
	sensorType A_dm2;

	// NaAdr=-ADR_K_exp; %respective EXPECTED adsorption rate (cin-cout)/cin when measured Adsortion Rate decreses to 10% of this value, an alarm is issued!
	// KAdr=ADR_K_exp;
	sensorType CaAdr;
	// UrAdr=ADR_Ur_exp;
	sensorType CreaAdr;
	// PhosAdr=ADR_Phos_exp;
	sensorType HCO3Adr;
    
	sensorType wgtNa; // the fuzzy importance of the adsorption of this substance
	sensorType wgtK;
	sensorType wgtCa;
	sensorType wgtUr;
	sensorType wgtCrea;
	sensorType wgtPhos;
	sensorType wgtHCO3;
	sensorType mspT; // set the threshold here!	
	
} tdParametersSCC;

typedef struct strMsgParametersSCC
{
	tdHeader		header;
	tdParametersSCC parametersSCC;
} tdMsgParametersSCC;

typedef struct strMsgPeriodPolarizerToggle
{
	tdHeader		header;			// dataID is always: dataID_configurePeriodPolarizerToggle
	uint8_t			togglePeriod;
} tdMsgPeriodPolarizerToggle;

// NAME VALUE PAIRS for WAKD parameter configuration
typedef struct strParameterValue {
	tdParameter	parameterName;
	uint32_t	parameetrValue; // biggest type. Will also transport uint16_t etc. Necessary overhead.
} tdParameterValue;

// typedef struct strMsgPhysicalData
// {
	// tdHeader					header;				// !! ALWAYS: myvar.header.dataId = dataID_physicalData;
	// tdUnixTimeType				TimeStamp;			// Seconds counted after January 1, 1970 (POSIX time)
	// uint8_t						statusECPI;			// !!!!!!!!!!!!!!!!!!!!!!!!!!!to be defined??
	// uint8_t						statusECPO;			// !!!!!!!!!!!!!!!!!!!!!!!!!!!to be defined??
	// tdPressureSensorData 		PressureFCI;
    // tdPressureSensorData		PressureFCO;
    // tdPressureSensorData		PressureBCI;
    // tdPressureSensorData		PressureBCO;
    // tdTemperatureSensorData		TemperatureInOut;
    // tdConductivitySensorData	CSENS1Data;
    // tdMultiSwitchData			MultiSwitchBIData;
    // tdMultiSwitchData			MultiSwitchBOData;
    // tdMultiSwitchData			MultiSwitchBUData;	// changed "L" to "U" according to figures.
    // tdPumpData					BLPumpData;
    // tdPumpData					FLPumpData;
	// tdPolarizationData 			PolarizationData;	
// } tdMsgPhysicalData;

// putData selects and controlls the correct HW-interface according to the selected recipient as defined in "whoId".
// The first byte of the data package is pointed at by "startData". Similar to memcopy, putData transfers the
// number ("lengthData") of bytes through the HW-interface to the recipient. lengthData cannot be 0!
// Calling of putData is blocking. Return value of putData is 0 if transmission was successfull. If not successfull
// putData returns 1 as signal for an error during transmission.
// e.g. uint8_t error = putData(&actuatorSet);
// The HW-intzerface (UART) to Real Time Bord will be used to transmit actuatorSet.
// At the beginning of each Msg-type there will always be the header (tdHeader) containing, sender, size, etc.
uint8_t putData(void *startData);

// getData pulls a data block from the HW-interface buffer according to the selected sender by "whoId".
// Location of the buffer is shared knowledge between HW-interface API and TASKs. Caller of the function is
// responsible to provide pointer to sufficient storage location starting at "startData" with the size of
// "lengthData". getData will then copy "lengthDat" of Bytes to the location of "startData". If successfull,
// getData will return 0, 1 if error occured.
// Calling getData will also signal the API that the HW-interface buffer that was previously blocked can now
// be reused and overwritten gain.
uint8_t getData(tdWhoId whoId, void *startData);

// struct that defines one field in any message queue between Free RTOS tasks on Main Board
typedef struct strQtoken
{
  tdCommand command;
  void      *pData;
} tdQtoken;

// *****************************************************************************************
// *****************************************************************************************
// **       ########     ##############    #########                                      **
// **       ###   ###    ##############    ####    ##                                     **
// **       ###    ##         ####         ####    ###                                    **
// **       #########         ####         #########                                      **
// **       #####             ####         #########                                      **
// **       ### ##            ####         ####    ###                                    **
// **       ###  ##           ####         ####     ##                                    **
// **       ###   ##          ####         ##########                                     **
// *****************************************************************************************
// *****************************************************************************************
typedef struct strDevicesStatus
{
    tdUnixTimeType	TimeStamp;			// Seconds counted after January 1, 1970 (POSIX time)
    uint32_t            statusECPI;
    uint32_t            statusECPO;
    uint32_t            statusBPSI;
    uint32_t            statusBPSO;
    uint32_t            statusFPSI;
    uint32_t            statusFPSO;
    uint32_t            statusBTS;
    uint32_t            statusDCS;
    uint32_t            statusBLPUMP;
    uint32_t            statusFLPUMP;
    uint32_t            statusMFSI;
    uint32_t            statusMFSO;
    uint32_t            statusMFSBL;
    uint32_t            statusPOLAR;
    uint32_t            statusRTMCB;
}tdDevicesStatus;

typedef struct strMsgDeviceStatus
{
  tdHeader  header;
  tdDevicesStatus devicesStatus;
} tdMsgDeviceStatus;


typedef struct strPhysicalsensorData
{
	tdUnixTimeType				TimeStamp;			// Seconds counted after January 1, 1970 (POSIX time)
	tdPressureSensorData 		PressureFCI;
    tdPressureSensorData		PressureFCO;
    tdPressureSensorData		PressureBCI;
    tdPressureSensorData		PressureBCO;
    tdTemperatureSensorData		TemperatureInOut;
    tdConductivitySensorData	CSENS1Data;
} tdPhysicalsensorData;

typedef struct strMsgPhysicalsensorData
{
	tdHeader				header;	// !! ALWAYS: myvar.header.dataId = dataID_physicalData;
	tdPhysicalsensorData	physicalsensorData;
} tdMsgPhysicalsensorData;

// Struct that contains all physiological sensor readouts.
 // The main structs (below) contain one absolute time stamp in Unix time, counted in seconds since
 // 1970.
 // The sub-structs inside contain individual relative times in ms for each measure in reference to this absolute time.
 // "I" stands for "inlet" and "O" stands for "outlet" in reference to the highflux dyalyser.
typedef struct strPhysiologicalData
{
	tdUnixTimeType	TimeStamp;		// Seconds counted after January 1, 1970 (POSIX time)
	tdECPData		ECPDataI;		// ECP flowing from Adsorbent Unit into Dialyser
	tdECPData		ECPDataO;		// ECP flowing from Dialyser into Adsorbent Unit
	tdWakdStates	currentState;
} tdPhysiologicalData;

typedef struct strMsgPhysiologicalData
{
	tdHeader			header;				// !! ALWAYS: myvar.header.dataId = dataID_physiologicalData;
	tdPhysiologicalData physiologicalData;
} tdMsgPhysiologicalData;

typedef struct strActuatorData
{
	tdUnixTimeType				TimeStamp;		// Seconds counted after January 1, 1970 (POSIX time)
    tdMultiSwitchData			MultiSwitchBIData;
    tdMultiSwitchData			MultiSwitchBOData;
    tdMultiSwitchData			MultiSwitchBUData;	// changed "L" to "U" according to figures.
    tdPumpData					BLPumpData;
    tdPumpData					FLPumpData;
	tdPolarizationData 			PolarizationData;	
} tdActuatorData;

typedef struct strMsgActuatorData
{
	tdHeader			header;				// !! ALWAYS: myvar.header.dataId = dataID_actuatorData;
	tdActuatorData	actuatorData;
} tdMsgActuatorData;


// -----------------------------------------------------------------------------------
// ALARMS
// ----------------------------------------
// ALARMS STATUS
// bit0 = BABD
// bit1 = BLD
// bit2 = DABD
// bit3 = FLD
// typedef struct strMsgAlarms
// {
	// tdHeader		header;			// !! ALWAYS: myvar.header.dataId = dataID_alarmRTB;
	// tdUnixTimeType	TimeStamp;		// Seconds counted after January 1, 1970 (POSIX time)
	// uint32_t 		Status;
// } tdMsgAlarms;

typedef struct strAlarms
{
	tdUnixTimeType	TimeStamp;		// Seconds counted after January 1, 1970 (POSIX time)
	uint32_t 		Status;
} tdAlarms;

typedef struct strMsgAlarms
{
  tdHeader  header;
  tdAlarms  alarms;
} tdMsgAlarms;


// -----------------------------------------------------------------------------------
// From typedefsNephron.h
// ----------------------------------------

// typedef struct strWeightSensorReadout
// {
	// tdUnixTimeType TimeStamp;	// FreeRTOS time: one Tick equals 10 ms
	// sensorType patientWeight_g;		// measurement of the patients bodyweight in gramms
// } tdWeightSensorReadout;

typedef struct strBloodpressure
{
	tdUnixTimeType TimeStamp;	// FreeRTOS time: one Tick equals 10 ms
	sensorType BPsys;
	sensorType BPdia;
} tdBloodpressure;

typedef struct strHeartrate
{
	tdUnixTimeType TimeStamp;	// FreeRTOS time: one Tick equals 10 ms
	sensorType Heartrate;
} tdHeartrate;


typedef struct strWAKDStateConfigurationParameters
{
	uint8_t			thisState;			// identifies which state is configured here
	uint16_t		defSpeedBp_mlPmin;	// setting speed blood pump [ml/min]
	uint16_t		defSpeedFp_mlPmin;	// setting speed fluidic pump [ml/min]	(=excreation speed in UF-state) use NO! FIXPOINTSHIFT_PUMP_FLOW
	uint16_t		defPol_V;			// setting polarization [V]#
	uint8_t			direction;
	uint32_t		duration_sec;		// pre-setting for duaration of this operational state
} tdWAKDStateConfigurationParameters;

typedef struct strFCConfigPointerStruct
{
	tdPatientProfile *pPatientProfile;
	tdWAKDStateConfigurationParameters *pWAKDStateConfigure;
} tdFCConfigPointerStruct;

typedef struct strWAKDAllStateConfigure
{
	tdWAKDStateConfigurationParameters stateAllStoppedConfig;			// configuration of respective state
	tdWAKDStateConfigurationParameters stateDialysisConfig;			// configuration of respective state
	tdWAKDStateConfigurationParameters stateRegen1Config;				// configuration of respective state
	tdWAKDStateConfigurationParameters stateUltrafiltrationConfig;	// configuration of respective state
	tdWAKDStateConfigurationParameters stateRegen2Config;				// configuration of respective state
	tdWAKDStateConfigurationParameters stateMaintenanceConfig;		// configuration of respective state
	tdWAKDStateConfigurationParameters stateNoDialysateConfig;		// configuration of respective state
	tdPatientProfile 	 patientProfile;				// patient profile (valid in all states)
} tdWAKDAllStateConfigure;

typedef struct strMsgWAKDAllStateConfigure
{
	tdHeader					header;
	tdWAKDAllStateConfigure 	WAKDAllStateConfigure;
} tdMsgWAKDAllStateConfigure;

typedef struct strMsgWAKDStateConfigurationParameters
{
	tdHeader							header;
	tdWAKDStateConfigurationParameters 	WAKDStateConfigurationParameters;
} tdMsgWAKDStateConfigurationParameters;

typedef struct strParametersSCCAllStates
{
	tdParametersSCC parametersSCC_stateAllStopped;		// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateDialysis;		// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateRegen1;			// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateUltrafiltration;	// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateRegen2;			// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateMaintenance;		// systemCheckAndCalibration thresold values
	tdParametersSCC parametersSCC_stateNoDialysate;		// systemCheckAndCalibration thresold values
} tdAllStatesParametersSCC;

typedef struct strWAKDStatesTimes 
{
	tdUnixTimeType DialysisDuration;
	tdUnixTimeType Regen1Duration;
	tdUnixTimeType UltrafiltrationDuration;
	tdUnixTimeType Regen2Duration;
	tdUnixTimeType timelastStateChange;
	tdUnixTimeType OS_AutomaticCycleStart;
} tdWAKDStatesTimes;

typedef struct strWAKDOS_AutomaticDone
{
	uint8_t DialysisCount;
	uint8_t Regen1Count;
	uint8_t UltrafiltrationCount;
	uint8_t Regen2Count;
} tdWAKDOS_AutomaticDone;

#endif
