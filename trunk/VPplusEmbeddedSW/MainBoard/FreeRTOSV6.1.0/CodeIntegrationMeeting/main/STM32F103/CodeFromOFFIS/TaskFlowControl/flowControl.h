/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef FLOWCONTROL_H
#define FLOWCONTROL_H

#ifdef LINUX
	#include "nephron.h"
	#include "typedefsNephron.h"
	#include "fcHelperFunc.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\nephron.h"
	#include "..\\typedefsNephron.h"
	#include ".\\fcHelperFunc.h"
#endif

void tskFC( void *pvParameters );

// void waterExtractionController(tdWeightMeasure *pWeightMeasure, tdWAKDAllStateConfigure *pWAKDAllStateConfigure, unsigned char *pExtracDone, sensorType *pExtractionsPERday, unsigned short *pG_toLoosePERday, tdUnixTimeType *poldTimestamp_Wgt, tdUnixTimeType *pExtractionEvery_sec);
void waterExtractionController(tdWeightMeasure *pWeightMeasure, uint16_t *pUfiltFlow, 
	uint16_t *pUfiltDuration, tdPatientProfile *pPatientProfile,
	unsigned char *pExtracDone, sensorType *pExtractionsPERday, 
	tdUnixTimeType *poldTimestamp_Wgt, tdUnixTimeType *pExtractionEvery_sec, 
	tdWAKDStatesTimes *pWAKDStatesTimes, 
	xQueueHandle *pQueueHandle);
 

//void sendCurrentState(tdMsgCurrentWAKDstateIs *currentState, tdQtoken *pQtoken, tdAllQueueHandles *pAllQueueHandles);
void sendCurrentState(tdMsgCurrentWAKDstateIs *pCurrentState, tdQtoken *pQtoken, xQueueHandle *pQueueHandle);

void dialysisState_ComputeNewVolt(sensorType *ADRUr_mmolPmin_preset,  sensorType *ADRUr_mmolPmin_now, sensorType *deviatSum_Ur_mmolPl, uint16_t ECPI_Ur, tdWAKDStateConfigurationParameters *pWAKDStateConfigure, tdPatientProfile *pPatientProfile);

void dialysisState_ComputeNewPumpspeed(sensorType *ADRK_mmolPmin_preset,  sensorType *ADRK_mmolPmin_now, sensorType *deviatSum_K, uint16_t ECPI_K_mmolPl, tdWAKDStateConfigurationParameters *pWAKDStateConfigure, tdPatientProfile *pPatientProfile);

void keepTrackAdsorptions(sensorType *ADRK_mmolPmin_now, sensorType *ADRUr_mmolPmin_now, sensorType *K_mmol_adsorbed_thisADphase, sensorType *Ur_mmol_adsorbed_thisADphase, tdMsgPhysiologicalData *pDialysisPhysiologicalReadout, sensorType *actFlow_mlPmin, portTickType newTimestamp_Flow, portTickType *oldTimestamp_Flow);

void getStateParametersFromDS(
		tdFCConfigPointerStruct *pFCConfigPointerStruct,
		xQueueHandle *pQueueHandle,
		uint16_t *pUfiltFlow,
		uint16_t *pUfiltDuration,
		tdUnixTimeType *pextractionEvery_sec,
		tdWAKDStatesTimes *pWAKDStatesTimes,
		tdWakdStates *pWAKDStateRemember,
		tdMsgCurrentWAKDstateIs *pCurrentState,
		xQueueHandle *pQHFC,
		unsigned char waitforDS);
		
void processRecievedFCConfigFromDS(	
	tdFCConfigPointerStruct *pFCConfigPointerStruct,
	xQueueHandle *pQHDISPin,
	uint16_t *pUfiltFlow,
	uint16_t *pUfiltDuration,
	tdWAKDStatesTimes *pWAKDStatesTimes,
	tdUnixTimeType *pextractionEvery_sec,
	tdWakdStates *pWAKDStateRemember);


void storeFCDatainDS(tdPatientProfile *pPatientProfile, xQueueHandle *pQueueHandle);


#warning remove testdebug_embeddedSysModel 
	typedef struct strPatientModel{
		float CUr_in;
		float CUr_ex;
		float G_Na;
		float G_Ur;
		float G_K;
		float CNa;
		float CK_in;
		float CK_ex;
	} tdPatientModel;
	
	void CalcControlleraction4Model_ADRpresetbased(float *ADRK_mmolPmin_preset, uint16_t ECPI_K_mmolPl, uint16_t ECPO_K_mmolPl);
	void CalcControlleraction4Model_bloodConcBased(
			float ecpiK, //concentration before the ADFilter  
			float ecpoK, //concentration behind the ADFilter  
			float *pCBloodInHFD2beComputed, // to be estiated by function dialysisState_ComputeNewPumpspeed_pBlood()
			tdWAKDStateConfigurationParameters *pWAKDStateDialysisConfigure4Model,
			tdPatientProfile *pPatientProfile4Model);
		
	void CalcActuatorsOnPatientInfluence4Model(tdPatientModel *pPatientModel,
	// now in patientModel: float *pCUr_in, float *pCUr_ex, float *pCNa, float *pCK_in, float *pCK_ex,	
	// now in patientModel: float *pG_KNa, float *pG_KUr, float *pG_KK,
		int TbaseSim, 
		float sorKMass, float elArea, float HFD_surface, uint16_t *pPlasmaFlow, uint16_t *pBloodFlow, 
		tdWakdStates *pWAKDState,
		float *pAdsod_K, int newADF,
	// ECP "I" stands for "inlet" and "O" stands for "outlet" in reference to the highflux filter 
		uint16_t *pEC_Uvolt, float EC_pH, float ecpiK_4Init, float *cK_PlasOutADF2ausprobieren,
	// finally these are the output values, i.e. the infleunce on the patient...
		float *pJUrea_mmolPmin,float *pJK_mmolPmin,float *pJNa_mmolPmin	);
		
	void CalcPatientDevelopment4Model(float *pCUr_in, float *pCUr_ex, float *pG_KNa, float *pG_KUr, float *pG_KK, float *pCNa, float *pCK_in, float *pCK_ex, int TbaseSim);
	
	void dialysisState_ComputeNewPumpspeed_pBlood(
		float ecpiK, //concentration before the ADFilter
		float ecpoK, //concentration behind the ADFilter
		float *pcorrectFactor_EffHFD,
		float *pEff_lastComputed, //but should be without the correction factor
		float *cBloodInHFD2beComputed,
		tdWAKDStateConfigurationParameters *pWAKDStateConfigure, 
		tdPatientProfile *pPatientProfile);
	
void testdebug_embeddedSysModel(tdUnixTimeType seconds2Predict, 
		float ecpiK_atBeginning,  // out of ADF and in to HFD
		float ecpoK_atBeginning, 
		float CK_exAtBeginning, 
		float CK_inAtBeginning, 
		float CUr_exAtBeginning, 
		float CUr_inAtBeginning, 
		float CNa_AtBeginning, 
		tdWAKDStateConfigurationParameters *pWAKStateDialysisConfigure_atBeginning,
		float *pAdsod_K_atBeginning,
		tdPatientProfile *pPatientProfile,
		int *heartBeatFC
	) ;
		
	void dummy2initialize_beforeEstimation(float Kex,float bloodFlow,float plasmaFlow,
		float *ecpiK, float *ecpoK);

#warning remove testdebug_embeddedSysModel END

	


#endif
