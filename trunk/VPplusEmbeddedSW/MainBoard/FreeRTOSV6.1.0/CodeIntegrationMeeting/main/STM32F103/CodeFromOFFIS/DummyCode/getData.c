/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany

 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
 
/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "getData.h"
  
//extern char uartBufferCBin[SIZEUARTBUFFERCB];
//extern char uartBufferCBout[SIZEUARTBUFFERCB];

// allocating static DATA Buffer for real time board
extern tdPhysiologicalData 		staticBufferPhysiologicalData;
extern tdPhysicalsensorData		staticBufferPhysicalsensorData;
extern tdDevicesStatus			staticBufferDevicesStatusData;
extern tdActuatorData			staticBufferActuatorData;
extern tdWakdStates				staticBufferStateRTB;
extern tdWakdOperationalState	staticBufferOperationalStateRTB;

uint8_t valves_sequence = 0;

void printfDataId(tdDataId dataId);

void printfWhoId(tdWhoId whoId);

// Whenever putData transfers a msg successfully, the following variables will be set.
// This is to simulate the ACK of the recipient of this msg. This is all
// dummy code here and will be replaced by CSEM's put & get.

typedef struct strDummyAckList
{
	// tdMsgOnly 			dummyMsg;
	tdMsgWeightData			dummyMsg;		// Oversized, if we send just a dummy ack. But does not harm
	struct strDummyAckList *pNextInList;
} tdDummyMsgList;

static tdDummyMsgList *pDummyMsgListCBPA = NULL;
//static tdAckDummy ackDummyRTBPA;



// getData pulls a data block from the HW-interface buffer according to the selected sender by "whoId".
// Location of the buffer is shared knowledge between HW-interface API and TASKs. Caller of the function is
// responsible to provide pointer to sufficient storage location starting at "startData" with the size of
// "lengthData". getData will then copy "lengthDat" of Bytes to the location of "startData". If successful,
// getData will return 0, 1 if error occurred.
// Calling getData will also signal the API that the HW-interface buffer that was previously blocked can now
// be reused and overwritten gain.

// Helper function to get first byte of buffer from serial interface and interpret it as dataId
tdDataId readDataId(tdWhoId whoId)
{
	tdDataId id = 0;
	switch (whoId) {
		case whoId_RTB: {
                #if defined VP_SIMULATION
			printf("\n********** ERROR in readDataId function! for RTB use function id=decodeMsg() instead! **************\n\n");
                        #endif
			break;
		}
		case whoId_CB: {
			if (pDummyMsgListCBPA != NULL)
			{	// Send an Ack on a previously send msg.
				id = pDummyMsgListCBPA->dummyMsg.header.dataId;
				// id = dataID_ack;
			} else {
				// This was caused by ISR. Let's see what is transmitting to us.
				id = uartBufferCBin[3];
			}
			break;
		}
		default: {
			// unknown case! Error.
			#if defined VP_SIMULATION
			printf("\n********** ERROR in readDataId function! (whoId not known!) **************\n\n");
                        #endif
			break;
		}
	}
	return(id);
}

// Helper function to extract the size of a received message. Message received size
// must be 6 bytes minimum to hold at least a message header. If less than 6 bytes is
// received this is an error and function will return 0 Bytes.
uint16_t readDataSize(tdWhoId whoId)
{
	uint16_t dataSize = 0;
	switch (whoId) {
		case whoId_RTB: {
			#if defined VP_SIMULATION
				printf("\n********** ERROR in readDataSize function! for RTB use function decodeMsg() instead! **************\n\n");
			#endif
			break;
		}
		case whoId_CB: {
			dataSize = ((uartBufferCBin[1]<<8) + uartBufferCBin[2]);
			break;
		}
		default: {
			// unknown case! Error.
			#if defined VP_SIMULATION
				printf("\n********** ERROR in readDataSize function! (whoId not known!) **************\n\n");
			#endif
			break;
		}
	}
	if (dataSize < 6)
	{	// Message is too small to contain a Message header. This must be an error.
		return(0);
	} else {
		// Unfortunately the internal sizes of structures are larger than the network structure
		// due to alignment and padding. So we have to recompute this manually.
		dataSize = dataSize-6+sizeof(tdMsgOnly);
		return(dataSize);
	}
}

extern uint8_t newStaticBufferPhysiologicalData;
extern uint8_t newStaticBufferPhysicalData;
extern uint8_t newStaticBufferStatusData;
extern uint8_t newStaticBufferActuatorData;
extern uint8_t newStaticBufferStatesRTB;

#ifdef VP_SIMULATION
	tdDataId decodeMsg()
	{
		tdDataId id = dataID_undefined;

		if (newStaticBufferPhysiologicalData)
		{
			id = dataID_physiologicalData;
			// dummy decoding was successful. Usually here in this function
			// CSEM would put the code to take the Physiological values from the UART
			// into the static data structure. The final version
			// of decodeMsg() will be implemented later by CSEM.
			staticBufferPhysiologicalData.TimeStamp							= (tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferPhysiologicalData.ECPDataI.Sodium					= (uint16_t)		*(REG32 SENSOR_ECPI_NA);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Sodium			= 0;
			staticBufferPhysiologicalData.ECPDataI.Potassium				= (uint16_t)		*(REG32 SENSOR_ECPI_K);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Potassium		= 0;
			staticBufferPhysiologicalData.ECPDataI.pH						= (uint16_t)		*(REG32 SENSOR_ECPI_PH);
			staticBufferPhysiologicalData.ECPDataI.msOffset_pH				= 0;
			staticBufferPhysiologicalData.ECPDataI.Urea						= (uint16_t)		*(REG32 SENSOR_ECPI_UREA);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Urea			= 0;
			staticBufferPhysiologicalData.ECPDataI.Temperature				= (uint16_t)		*(REG32 SENSOR_ECPI_TEMPERATURE);
			staticBufferPhysiologicalData.ECPDataI.msOffset_Temperature	= 0;
//			staticBufferPhysiologicalData.ECPDataI.Status					= 0;
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Status			= 0;
//			staticBufferPhysiologicalData.ECPDataI.Phosphate				= (uint16_t)		*(REG32 SENSOR_ECPI_H2PO4);
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Phosphate		= 0;
//			staticBufferPhysiologicalData.ECPDataI.Creatinine				= (uint16_t)		*(REG32 SENSOR_ECPI_C4H9N3O2);
//			staticBufferPhysiologicalData.ECPDataI.msOffset_Creatinine		= 0;
			staticBufferPhysiologicalData.ECPDataO.Sodium					= (uint16_t)		*(REG32 SENSOR_ECPO_NA);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Sodium			= 0;
			staticBufferPhysiologicalData.ECPDataO.Potassium				= (uint16_t)		*(REG32 SENSOR_ECPO_K);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Potassium		= 0;
			staticBufferPhysiologicalData.ECPDataO.pH						= (uint16_t)		*(REG32 SENSOR_ECPO_PH);
			staticBufferPhysiologicalData.ECPDataO.msOffset_pH				= 0;
			staticBufferPhysiologicalData.ECPDataO.Urea						= (uint16_t)		*(REG32 SENSOR_ECPO_UREA);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Urea			= 0;
			staticBufferPhysiologicalData.ECPDataO.Temperature				= (uint16_t)		*(REG32 SENSOR_ECPO_TEMPERATURE);
			staticBufferPhysiologicalData.ECPDataO.msOffset_Temperature	= 0;
//			staticBufferPhysiologicalData.ECPDataO.Status					= 0;
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Status			= 0;
//			staticBufferPhysiologicalData.ECPDataO.Phosphate				= (uint16_t)		*(REG32 SENSOR_ECPO_H2PO4);
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Phosphate		= 0;
//			staticBufferPhysiologicalData.ECPDataO.Creatinine				= (uint16_t)		*(REG32 SENSOR_ECPO_C4H9N3O2);
//			staticBufferPhysiologicalData.ECPDataO.msOffset_Creatinine		= 0;
			newStaticBufferPhysiologicalData = 0;
		} else if (newStaticBufferPhysicalData) {
			id = dataID_physicalData;
			staticBufferPhysicalsensorData.TimeStamp								= (tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_WAKD);
//			staticBufferPhysicalsensorData.statusECPI								= (uint8_t)			*(REG32 SENSOR_ECPI_STAT);
//			staticBufferPhysicalsensorData.statusECPO								= (uint8_t)			*(REG32 SENSOR_ECPO_STAT);
			staticBufferPhysicalsensorData.PressureFCI.Pressure						= (uint16_t)		*(REG32 SENSOR_FPSI_FLUIDPRESSI);
			staticBufferPhysicalsensorData.PressureFCI.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCI.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCI.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureFCO.Pressure						= (uint16_t)		*(REG32 SENSOR_FPSO_FLUIDPRESSO);
			staticBufferPhysicalsensorData.PressureFCO.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCO.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureFCO.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureBCI.Pressure						= (uint16_t)		*(REG32 SENSOR_BPSI_BLOODPRESI);
			staticBufferPhysicalsensorData.PressureBCI.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCI.Status						= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCI.msOffset_Status				= (tdMsTimeType) 	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.PressureBCO.Pressure						= (uint16_t)		*(REG32 SENSOR_BPSO_BLOODPRESO);
			staticBufferPhysicalsensorData.PressureBCO.msOffset_Pressure			= (tdMsTimeType) 	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCO.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.PressureBCO.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.TemperatureInOut.TemperatureInlet_Value	= (uint16_t)		*(REG32 SENSOR_BTSI_TEMPERATURE);
			staticBufferPhysicalsensorData.TemperatureInOut.TemperatureOutlet_Value	= (uint16_t)		*(REG32 SENSOR_BTSO_TEMPERATURE);
			staticBufferPhysicalsensorData.TemperatureInOut.msOffset_Temperature	= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.TemperatureInOut.Status					= (uint32_t) 		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.TemperatureInOut.msOffset_Status		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.CSENS1Data.Cond_FCR						= (uint16_t)		*(REG32 SENSOR_DCS_CONDUCT_FCR);
			staticBufferPhysicalsensorData.CSENS1Data.Cond_FCQ						= (uint16_t)		*(REG32 SENSOR_DCS_CONDUCT_FCQ);
			staticBufferPhysicalsensorData.CSENS1Data.Cond_PT1000					= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferPhysicalsensorData.CSENS1Data.msOffset_Cond				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.CSENS1Data.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferPhysicalsensorData.CSENS1Data.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			newStaticBufferPhysicalData = 0;
		} else if (newStaticBufferStatusData) {
			id = dataID_statusRTB;
			staticBufferDevicesStatusData.TimeStamp					= 		(tdUnixTimeType)	*(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferDevicesStatusData.statusECPI				= 		(uint32_t)			*(REG32 SENSOR_ECPISTAT);
			staticBufferDevicesStatusData.statusECPO				= 		(uint32_t)			*(REG32 SENSOR_ECPOSTAT);
			staticBufferDevicesStatusData.statusBPSI				= 		(uint32_t)			*(REG32 SENSOR_BPSISTAT);
			staticBufferDevicesStatusData.statusBPSO				= 		(uint32_t)			*(REG32 SENSOR_BPSOSTAT);
			staticBufferDevicesStatusData.statusFPSI				= 		(uint32_t)			*(REG32 SENSOR_FPSISTAT);
			staticBufferDevicesStatusData.statusFPSO				= 		(uint32_t)			*(REG32 SENSOR_FPSOSTAT);
			staticBufferDevicesStatusData.statusBTS					= 		(uint32_t)			*(REG32 SENSOR_BTSSTAT);
			staticBufferDevicesStatusData.statusDCS					= 		(uint32_t)			*(REG32 SENSOR_DCSSTAT);
			staticBufferDevicesStatusData.statusBLPUMP				= 		(uint32_t)			*(REG32 SENSOR_BLPUMPSTAT);
			staticBufferDevicesStatusData.statusFLPUMP				= 		(uint32_t)			*(REG32 SENSOR_FLPUMPSTAT);
			staticBufferDevicesStatusData.statusMFSI				= 		(uint32_t)			*(REG32 SENSOR_MFSISTAT);
			staticBufferDevicesStatusData.statusMFSO				= 		(uint32_t)			*(REG32 SENSOR_MFSOSTAT);
			staticBufferDevicesStatusData.statusMFSBL				= 		(uint32_t)			*(REG32 SENSOR_MFSBLSTAT);
			staticBufferDevicesStatusData.statusPOLAR				= 		(uint32_t)			*(REG32 SENSOR_POLARIZSTAT);
			staticBufferDevicesStatusData.statusRTMCB				=		(uint32_t)			*(REG32 SENSOR_RTMCBSTAT);
			newStaticBufferStatusData = 0;
		} else if (newStaticBufferActuatorData) {
			id = dataID_actuatorData;
			// Fluidic Switches
			staticBufferActuatorData.TimeStamp										= (tdUnixTimeType)  *(REG32 SENSOR_SAMPLETIME_PATI);
			staticBufferActuatorData.MultiSwitchBIData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBIData.Position						= (uint16_t)		*(REG32 SENSOR_MFSI_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBIData.msOffset_Position			= (tdMsTimeType)	5;
//			staticBufferActuatorData.MultiSwitchBIData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBIData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.MultiSwitchBOData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBOData.Position						= (uint16_t)		*(REG32 SENSOR_MFSU_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBOData.msOffset_Position			= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBOData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBOData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.MultiSwitchBUData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.MultiSwitchBUData.Position						= (uint16_t)		*(REG32 SENSOR_MFSO_STAT_SWITCH);
			staticBufferActuatorData.MultiSwitchBUData.msOffset_Position			= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBUData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.MultiSwitchBUData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			// Pumps
			staticBufferActuatorData.BLPumpData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Direction						= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Direction			= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Speed							= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Speed				= (tdMsTimeType)	3;
			staticBufferActuatorData.BLPumpData.FlowReference					= (uint16_t) 		*(REG32 SENSOR_FLOWREF_BLOOD);
			staticBufferActuatorData.BLPumpData.msOffset_FlowReference		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.Flow							= (uint16_t)		*(REG32 SENSOR_FLOW_BLOOD);
			staticBufferActuatorData.BLPumpData.msOffset_Flow				= (tdMsTimeType)	3;
			staticBufferActuatorData.BLPumpData.Current						= (uint16_t)		0; // not implemented in Simulink Model
			staticBufferActuatorData.BLPumpData.msOffset_Current				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.BLPumpData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.BLPumpData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model

			staticBufferActuatorData.FLPumpData.SwitchONnOFF					= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.Direction						= (uint8_t)  		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Direction			= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.Speed							= (uint16_t) 		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Speed				= (tdMsTimeType)	3;
			staticBufferActuatorData.FLPumpData.FlowReference					= (uint16_t) 		*(REG32 SENSOR_FLOWREF_FLUID); 
			staticBufferActuatorData.FLPumpData.msOffset_FlowReference		= (tdMsTimeType)	0; // not implemented in Simulink Model
//Flow: should be the 'flow' value in simulink			
			staticBufferActuatorData.FLPumpData.Flow							= (uint16_t)		*(REG32 SENSOR_FLOW_FLUID);
			staticBufferActuatorData.FLPumpData.msOffset_Flow				= (tdMsTimeType)	3;
			staticBufferActuatorData.FLPumpData.Current						= (uint16_t)		0; // not implemented in Simulink Model
			staticBufferActuatorData.FLPumpData.msOffset_Current				= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.FLPumpData.Status						= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.FLPumpData.msOffset_Status				= (tdMsTimeType)	0; // not implemented in Simulink Model
			// Polarizer Circuit
			staticBufferActuatorData.PolarizationData.SwitchONnOFF			= (uint8_t)  			*(REG32 SENSOR_POLARIZ_ONOFF);
			staticBufferActuatorData.PolarizationData.Direction				= (uint8_t)  			*(REG32 SENSOR_POLARIZ_DIRECT);
			staticBufferActuatorData.PolarizationData.msOffset_Direction		= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.PolarizationData.VoltageReference		= (uint16_t)  			*(REG32 SENSOR_POLARIZ_VOLTAGEREF);
			staticBufferActuatorData.PolarizationData.msOffset_VoltageReference= (tdMsTimeType)	0; // not implemented in Simulink Model
			staticBufferActuatorData.PolarizationData.Voltage					= (uint16_t)  			*(REG32 SENSOR_POLARIZ_VOLTAGE);
			staticBufferActuatorData.PolarizationData.msOffset_Voltage		= (tdMsTimeType)	0; // not implemented in Simulink Model
//			staticBufferActuatorData.PolarizationData.Status					= (uint32_t)		0; // not implemented in Simulink Model
//			staticBufferActuatorData.PolarizationData.msOffset_Status		= (tdMsTimeType)	0; // not implemented in Simulink Model
			newStaticBufferActuatorData = 0;
		} else if (newStaticBufferStatesRTB) {
			id = dataID_currentWAKDstateIs;
			// The current state is already in
			// extern tdWakdStates           staticBufferStateRTB;
			// extern tdWakdOperationalState staticBufferOperationalStateRTB;
			#if defined SEQ0 || defined SEQ4
				taskMessage("I", "decodeMsg()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
			#endif
			newStaticBufferStatesRTB = 0;
		}
		return(id);
	}
#endif

// The following implementation is just a dummy function to interface in the virtual platform
// with the Matlab Simulink interface (OFFIS SimLink). The "real" functionality will be coded by CSEM.
// This code will be attached to a serial interface to communication board, real time board, etc.
uint8_t dumpBuffToFile(uint8_t *uartBuffer, uint16_t msgSize, char* filename);
uint8_t dumpBuffToFile(uint8_t *uartBuffer, uint16_t msgSize, char* filename)
{
	uint8_t err = 0;
	#ifdef VP_SIMULATION
		FILE *pFile = fopen(filename, "a");
		if(pFile!=NULL) {
			fprintf(pFile,"*** Time: %lu ms: Start of Msg\n", xTaskGetTickCount());

			uint16_t i;
			for (i=0; i<msgSize; i++)
			{
				fprintf(pFile,"Byte %d: 0x%x", i, uartBuffer[i]);
				switch (i){
					case 0:{
						fprintf(pFile," (Recipient: %s)\n",whoIdToString(uartBuffer[i]));
						break;
					}
					case 1:{
						fprintf(pFile," (Most significant byte of msg-size)\n");
						break;
					}
					case 2:{
						fprintf(pFile," (Least significant byte of msg-size)\n");
						break;
					}
					case 3:{
						fprintf(pFile," (dataID: %s)\n", dataIdToString(uartBuffer[i]));
						break;
					}
					case 4:{
						fprintf(pFile," (msg-count: %d)\n", uartBuffer[i]);
						break;
					}
					case 5:{
						fprintf(pFile," (Sender: %s)\n",whoIdToString(uartBuffer[i]));
						break;
					}
					default:{
						fprintf(pFile,"\n");
						break;
					}
				}
			}
			fprintf(pFile,"*** End of Msg\n");
			fclose(pFile);
		} else {
			#if defined VP_SIMULATION
				printf("*** ERROR: Could not open file: uartCBout.dump!\n");
			#endif
			err = 1;
		}
	#endif
	return(err);
}

uint8_t getData(tdWhoId whoId, void *startData)
{
	switch (whoId) {
		case whoId_RTB: {
                #if defined VP_SIMULATION
			printf("\n********** ERROR in getData function! for RTB use function decodeMsg() instead! **************\n\n");
                #endif
			break;
		}
		case whoId_CB: {
			if (pDummyMsgListCBPA != NULL)
			{	// Send an Ack or Weight data on a previously send msg.
				if (pDummyMsgListCBPA->dummyMsg.header.dataId == dataID_ack)
				{
					tdMsgOnly * pMsgOnly = NULL;
					pMsgOnly = (tdMsgOnly *) startData;
					pMsgOnly->header.dataId      = pDummyMsgListCBPA->dummyMsg.header.dataId;
					pMsgOnly->header.issuedBy    = pDummyMsgListCBPA->dummyMsg.header.issuedBy;
					pMsgOnly->header.msgCount    = pDummyMsgListCBPA->dummyMsg.header.msgCount;
					pMsgOnly->header.msgSize     = sizeof(tdMsgOnly);
					pMsgOnly->header.recipientId = pDummyMsgListCBPA->dummyMsg.header.recipientId;
				} else if (pDummyMsgListCBPA->dummyMsg.header.dataId == dataID_weightData)
				{
					tdMsgWeightData * pMsgWeightData = NULL;
					pMsgWeightData = (tdMsgWeightData *) startData;
					pMsgWeightData->header.dataId      			= pDummyMsgListCBPA->dummyMsg.header.dataId;
					pMsgWeightData->header.issuedBy   			= pDummyMsgListCBPA->dummyMsg.header.issuedBy;
					pMsgWeightData->header.msgCount    			= pDummyMsgListCBPA->dummyMsg.header.msgCount;
					pMsgWeightData->header.msgSize				= sizeof(tdMsgWeightData);
					pMsgWeightData->header.recipientId			= pDummyMsgListCBPA->dummyMsg.header.recipientId;
					pMsgWeightData->weightData.BodyFat			= pDummyMsgListCBPA->dummyMsg.weightData.BodyFat;
					pMsgWeightData->weightData.TimeStamp		= pDummyMsgListCBPA->dummyMsg.weightData.TimeStamp;
					pMsgWeightData->weightData.WSBatteryLevel	= pDummyMsgListCBPA->dummyMsg.weightData.WSBatteryLevel;
					pMsgWeightData->weightData.WSStatus			= pDummyMsgListCBPA->dummyMsg.weightData.WSStatus;
					pMsgWeightData->weightData.Weight			= pDummyMsgListCBPA->dummyMsg.weightData.Weight;
				}
				// If we had a real communication partner who would have sent this Ack, we
				// would have found it in the input UART buffer. Well, instead of reading it from
				// there we put it there, since we had to generate it ourself for testing reasons.
				if(!(htonMsg(uartBufferCBin, startData)))
				{
					uint16_t msgSize = ((uartBufferCBin[1]<<8) + uartBufferCBin[2]);
					if(dumpBuffToFile(uartBufferCBin, msgSize, "uartCBin.dump"))
					{
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() Could not dump UART buffer to file!\n");
						#endif
					}
				} else {
					#if defined VP_SIMULATION
						printf("**** ERROR: getData() call of htonMsg failed!\n");
					#endif
				}

				// Remove first element from list of Acks
				tdDummyMsgList *pDeleteThis = pDummyMsgListCBPA;
				pDummyMsgListCBPA = pDummyMsgListCBPA->pNextInList;
				vPortFree(pDeleteThis);
			} else {
				if(!(ntohMsg(startData, uartBufferCBin)))
				{
					uint16_t msgSize = ((uartBufferCBin[1]<<8) + uartBufferCBin[2]);
					if(dumpBuffToFile(uartBufferCBin, msgSize, "uartCBin.dump"))
					{
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() Could not dump UART buffer to file!\n");
						#endif
					}
				} else {
						#if defined VP_SIMULATION
							printf("**** ERROR: getData() call of ntohMsg failed!\n");
						#endif
				}
				// Remove lock from uartBufferCBin to be able to receive new messages from CB
				lockUartBufferCBin = SIGNAL_OFF;
				#ifndef VP_SIMULATION
					CB_CLR_OUT_COM_BUFFERS();
				#endif
			}
			break;
		}
		default: {
			// unknown case! Error.
			#ifdef VP_SIMULATION
				printf("\n********** ERROR in getData function! (whoId not known!) **************\n\n");
			#endif
			return(1); // ERROR
			break;
		}
	}
	return(0);
}

uint8_t putData(void *startData)
{
	uint8_t err = 0;
	// Function to be implemented by CSEM. Will copy the data to serial interface of whoId and transmit.
	// Header information beginning at position "startData" gives target recipient as well as size of message.
	// Here we are doing nothing and just pretend it did work. Maybe a nice printf for debugging:
	tdMsgOnly *pMsgOnly = (tdMsgOnly *) startData;
	
	if (pMsgOnly->header.recipientId == whoId_RTB)
	{	// This message needs to take the route via the real time board
		switch (pMsgOnly->header.dataId) {
			case dataID_configureState :{
				#if defined SEQ9
					printConfigureState("putData()", (tdMsgConfigureState *) (startData));
				#endif
				
                                #ifndef HWBUILD
                                // This part of the RealTimeBoard is simulated in the Simulink model
				// Write configuration into simulink model.
				
                                tdMsgConfigureState *msgConfigureState = (tdMsgConfigureState *) (startData);
				switch(msgConfigureState->stateToConfigure) {
					case wakdStates_AllStopped :{
						*(REG32 ACTUATOR_BP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ALLSTOPPED)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Maintenance :{
						*(REG32 ACTUATOR_BP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_NoDialysate :{
						*(REG32 ACTUATOR_BP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Dialysis :{
						*(REG32 ACTUATOR_BP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen1 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN1)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Ultrafiltration :{
						*(REG32 ACTUATOR_BP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ULTRAFILT)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen2 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN2)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					default :{
						taskMessage("E", "putData Function", "Unknown state cannot be configured!");
					}
				}
				break;
                                #else
                                
                                
                                                             
                                vRTMCB_CMDS_CONFIGHANDLER(startData);

                                /*
                                tdMsgConfigureState *msgConfigureState = (tdMsgConfigureState *) (startData);
				switch(msgConfigureState->stateToConfigure) {
					case wakdStates_AllStopped :{
						*(REG32 ACTUATOR_BP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ALLSTOPPED)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ALLSTOPPED)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Maintenance :{
						*(REG32 ACTUATOR_BP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_MAINTENANCE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_MAINTENANCE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_NoDialysate :{
						*(REG32 ACTUATOR_BP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_NODIALYSATE)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_NODIALYSATE)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Dialysis :{
						*(REG32 ACTUATOR_BP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_DIALYSIS)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_DIALYSIS)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen1 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN1)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN1)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN1)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Ultrafiltration :{
						*(REG32 ACTUATOR_BP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_ULTRAFILT)		= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_ULTRAFILT)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_ULTRAFILT)		= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					case wakdStates_Regen2 :{
						*(REG32 ACTUATOR_BP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.BLPumpCtrl.direction;
						*(REG32 ACTUATOR_BP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_FP_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.FLPumpCtrl.direction;
						*(REG32 ACTUATOR_FP_FLOWREF_REGEN2)			= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
						*(REG32 ACTUATOR_PZ_DIRECTION_REGEN2)		= msgConfigureState->actCtrl.PolarizationCtrl.direction;
						*(REG32 ACTUATOR_PZ_VOLTREF_REGEN2)			= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
						break;
					}
					default :{
						taskMessage("E", "putData Function", "Unknown state cannot be configured!");
					}
				}
				break;     */                           
                                #endif
			}
			case dataID_statusRequest:{
				// Here we now have dummy functionality that pretends to be the RTB and generates the proper answer.
				// Pretend to be the ISR and inform system that new data is in RTB UART Buffer.
				#ifndef HWBUILD
                                  newStaticBufferStatesRTB = 1;	// for decode() to know what data has just arrived.
                                  // #endif
  
                                  tdQtoken Qtoken;
                                  Qtoken.command = command_BufferFromRTBavailable;
                                  Qtoken.pData	= NULL;
                                  #if defined SEQ0
                                          taskMessage("I", "putData Function", "RTB sends answer current state: '%s'|'%s'", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
                                  #endif
                                  if ( xQueueSendToBack( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
                                  {	// Seemingly the queue is full and we already waited for a long time
                                          taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromRTBavailable'");
                                  }
                                  break;
                                #else //HWBUILD

                                // Integration test:
                                
                                #endif // HWBUILD ELSE



			}
			case dataID_changeWAKDStateFromTo:{
				// The RTB will change into the new state without any condition.
				// RTB will answer this with its current status. Just as if it had received: dataID_statusRequest
				tdMsgWakdStateFromTo *pMsgWakdStateFromTo = (tdMsgWakdStateFromTo*) (startData);
				staticBufferStateRTB = pMsgWakdStateFromTo->toWakdState;
                                if(!staticBufferStateRTB) staticBufferStateRTB = wakdStates_AllStopped;
				staticBufferOperationalStateRTB = pMsgWakdStateFromTo->toWakdOpState;
				// Communicate this state transition to the Simulink model
				
                                #ifndef HWBUILD
                                *(REG32 ACTUATOR_WAKD_STATE) = pMsgWakdStateFromTo->toWakdState;
				*(REG32 ACTUATOR_WAKD_OPERATIONAL_STATE) = pMsgWakdStateFromTo->toWakdOpState;
                                #else
                                // Activate State here
                                vRTMCB_CMDS_STATEHANDLER(staticBufferStateRTB);
                                #endif
                            
				#ifdef VP_SIMULATION
					// For debugging tracing we protocol each change in state in log file
					FILE *pFile = fopen("stateTransition.dump", "a");
					if(pFile!=NULL) {
						fprintf(pFile,"*** Time: %lu ms: Changing into state ", xTaskGetTickCount());
						fprintf(pFile,"'%s'|'%s'.\n", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
						fclose(pFile);
					}
				#endif

				#if defined SEQ0 || defined SEQ4
					taskMessage("I", "putData()", "New state is '%s'|'%s'.", stateIdToString(staticBufferStateRTB), opStateIdToString(staticBufferOperationalStateRTB));
				#endif
				
                                #ifndef HWBUILD
                                newStaticBufferStatesRTB = 1;	// for decode() to know what data has just arrived.
				#endif

                                /*tdQtoken Qtoken;
				Qtoken.command = command_BufferFromRTBavailable;
				Qtoken.pData	= NULL;
				if ( xQueueSendToBack( allHandles.allQueueHandles.qhRTBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
				{	// Seemingly the queue is full and we already waited for a long time
					taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromRTBavailable'");
				}*/
				break;
			}
		}
	} else if ((pMsgOnly->header.recipientId == whoId_CB)||(pMsgOnly->header.recipientId == whoId_SP))
	{	// This message needs to take the route via the Communication Board.
		#ifdef SLIP_ENABLED
			// Convert message from host to network into a temporary memory.
			// After conversion, the SLIP protocol is added while temp memory
			// is copied to uartBufferCBout for transmission.
			uint8_t uartBufferCBoutNoSLIPyet[SLIP_BLEN];
			if (!(htonMsg(uartBufferCBoutNoSLIPyet, startData)))
			{
				uint16_t msgSize = ((uartBufferCBoutNoSLIPyet[1]<<8) + uartBufferCBoutNoSLIPyet[2]);
				if(dumpBuffToFile(uartBufferCBoutNoSLIPyet, msgSize, "uartCBout.dump"))
				{
					#if defined VP_SIMULATION
						printf("**** ERROR: putData() Could not dump UART buffer to file!\n");
					#endif
					err = 1;
				}
				#if defined VP_UartBtToPhone_ENABLED
					// Actually sending the bytes to the OVP UART link to the real phone
					// connected via serial bluetooth protocol. This is implemented via the
					// bus peripheral model "OFFIS_UartBtToPhone" at address 0x80020000
					// For the OFFIS setup, the real smartphone opens COM5 for communication
					// once the Nephron App has started.
					msgSize = CB_ConvertToSlipTX(uartBufferCBoutNoSLIPyet, msgSize, &uartBufferCBout[0]);
//					if (uartBufferCBoutNoSLIPyet[3]==dataID_currentWAKDstateIs)
//#warning All other messages have to be enabled as well!!
//					{	// Sending only this message to phone for testing. Everything else is not sent.
						int i;
						for (i=0;i<msgSize;i++)
						{
							*(REG32 UARTOUT) = uartBufferCBout[i];
						}
//					}
				#else #if defined HWBUILD

                                      // Acutally sending to CB
                                      msgSize = CB_ConvertToSlipTX(uartBufferCBoutNoSLIPyet, msgSize, &uartBufferCBout[0]);
				      uint32_t i;
                                      
                                      for(i=0;i<msgSize;i++) {    
                                          USART_SendData(USART2, uartBufferCBout[i]);
                                          while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {}
                                      }

                                #endif
                             
			}
		#else
			if (!(htonMsg(uartBufferCBout, startData)))
			{
				// Writing the UART output to a file for trace debugging.
				uint16_t msgSize = ((uartBufferCBout[1]<<8) + uartBufferCBout[2]);
				if(dumpBuffToFile(uartBufferCBout, msgSize, "uartCBout.dump"))
				{
					#if defined VP_SIMULATION
						printf("**** ERROR: putData() Could not dump UART buffer to file!\n");
					#endif
					err = 1;
				}

				#if defined VP_UartBtToPhone_ENABLED
					// Actually sending the bytes to the OVP UART link to the real phone
					// connected via serial bluetooth protocol. This is implemented via the
					// bus peripheral model "OFFIS_UartBtToPhone" at address 0x80020000
					// For the OFFIS setup, the real smartphone opens COM5 for communication
					// once the Nephron App has started.
	//				if (uartBufferCBout[3]==dataID_currentWAKDstateIs)
	//#warning All other messages have to be enabled as well!!
	//				{	// Sending only this message to phone for testing. Everything else is not sent.
						int i;
						for (i=0;i<msgSize;i++)
						{
							*(REG32 UARTOUT) = uartBufferCBout[i];
						}
	//				}
				#endif

				// the function host to network (hton) transfered the data to network protocol.
				// this changed the size of the message (mostly smaller). The size is now defined
				// in Byte 1 and Byte 2, but the byte order (most significant) is inverted to the host.
				// we have to fix that.
				#ifndef VP_SIMULATION
					uint16_t CB_DATALEN = ((uartBufferCBout[1]<<8) + uartBufferCBout[2]);
					#warning DEBUG CODE MUST BE REMOVED
					//DISABLED SLIP SHOULD BE ENABLED LATER AGAIN!!!
					// SLIP GDU
					//CB_DATALEN = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALEN, &SLIP_TX_CB[0]);
					//CB_DATALEN = CB_SendPacket (SLIP_TX_CB, CB_DATALEN);
					// NO SLIP GDU
					CB_DATALEN = CB_SendPacket (uartBufferCBout, CB_DATALEN);
					//CB_DATALEN = CB_SendPacket (uartBufferCBout, (uartBufferCBout[1]<<8+uartBufferCBout[2]));
				#endif
			} else {
					#if defined VP_SIMULATION
				printf("**** ERROR: putData() call of htonMsg failed!\n");
							#endif
				err = 1;
			}
		#endif
			#ifdef VP_SIMULATION
	//			// Since we do not have a real recipient of the msg who would ACK the msg, we just do this
	//			// here in the put() for debugging reasons. So out dummy put() here will ACK the msg it just processed.
				#ifdef DONT_EXPECT_ACK_FROM_PHONE
					switch (pMsgOnly->header.dataId) {
						case (dataID_reset):				// fall through (do the same thing)
						case (dataID_weightRequest):		// fall through (do the same thing)
						case (dataID_currentWAKDstateIs):	// fall through (do the same thing)
						case (dataID_physiologicalData):	// fall through (do the same thing)
						case (dataID_systemInfo):			// fall through (do the same thing)
						case (dataID_weightData):{
						case (dataID_bpRequest):{
						case (dataID_bpData):{
							// Put this  at end of list.
							if (pDummyMsgListCBPA == NULL)
							{	// First and only element in list
								pDummyMsgListCBPA = (tdDummyMsgList *)pvPortMalloc(sizeof(tdDummyMsgList));
								pDummyMsgListCBPA->dummyMsg.header.dataId			= dataID_ack;
								pDummyMsgListCBPA->dummyMsg.header.issuedBy 		= pMsgOnly->header.recipientId;
								pDummyMsgListCBPA->dummyMsg.header.msgCount 		= pMsgOnly->header.msgCount;
								pDummyMsgListCBPA->dummyMsg.header.msgSize		= sizeof(tdMsgOnly);
								pDummyMsgListCBPA->dummyMsg.header.recipientId	= whoId_MB;
								pDummyMsgListCBPA->pNextInList = NULL;
							} else {
								// Search end of List
								tdDummyMsgList *pSearchIndex = pDummyMsgListCBPA;
								while (pSearchIndex->pNextInList != NULL)
								{
									pSearchIndex = pSearchIndex->pNextInList;
								}
								// Create new list entry at end
								pSearchIndex->pNextInList = (tdDummyMsgList *)pvPortMalloc(sizeof(tdDummyMsgList));
								// Step into the end element
								pSearchIndex = pSearchIndex->pNextInList;
								pSearchIndex->dummyMsg.header.dataId			= dataID_ack;
								pSearchIndex->dummyMsg.header.issuedBy 		= pMsgOnly->header.recipientId;
								pSearchIndex->dummyMsg.header.msgCount 		= pMsgOnly->header.msgCount;
								pSearchIndex->dummyMsg.header.msgSize			= sizeof(tdMsgOnly);
								pSearchIndex->dummyMsg.header.recipientId		= whoId_MB;
								pSearchIndex->pNextInList = NULL;
							}
							// Inform system that CB has an ACK for us!
							tdQtoken Qtoken;
							Qtoken.command	= command_BufferFromCBavailable;
							Qtoken.pData	= NULL;
							if ( xQueueSendToBack( allHandles.allQueueHandles.qhCBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
							{
								// Seemingly the queue is full and we already waited for a long time
								taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromCBavailable'");
							}
							break;
						}
					}
				#endif
			#endif

		#if defined GENERATE_WEIGHT_DATA_ON_WEIGHT_REQUEST
			// The following code section is for demonstration purposes only. In case we did send out
			// a weight request to the communication board we originally would wait for the weight scale
			// to answer to this. Since we do not have a real communication board and real weight scale
			// during demo we just generate the weight answer here our self.
			#warning Remove this code section below after review demo.
			if (pMsgOnly->header.dataId == dataID_weightRequest)
			{	// Weight request received. Create an artificial answer, simulating a weight scale.
				// This also includes to simulate an ACK from the communication board.
				if (pDummyMsgListCBPA == NULL)
				{	// First and only element in list
					// Create the ACK
					pDummyMsgListCBPA = (tdDummyMsgList *)pvPortMalloc(sizeof(tdDummyMsgList));
					pDummyMsgListCBPA->dummyMsg.header.dataId			= dataID_ack;
					pDummyMsgListCBPA->dummyMsg.header.issuedBy 		= pMsgOnly->header.recipientId;
					pDummyMsgListCBPA->dummyMsg.header.msgCount 		= pMsgOnly->header.msgCount;
					pDummyMsgListCBPA->dummyMsg.header.msgSize			= sizeof(tdMsgOnly);
					pDummyMsgListCBPA->dummyMsg.header.recipientId		= whoId_MB;
					pDummyMsgListCBPA->pNextInList = NULL;
				} else {
					// Search end of List
					tdDummyMsgList *pSearchIndex = pDummyMsgListCBPA;
					while (pSearchIndex->pNextInList != NULL)
					{
						pSearchIndex = pSearchIndex->pNextInList;
					}
					// Create new list entry at end
					pSearchIndex->pNextInList = (tdDummyMsgList *)pvPortMalloc(sizeof(tdDummyMsgList));
					// Step into the end element
					pSearchIndex = pSearchIndex->pNextInList;
					pSearchIndex->dummyMsg.header.dataId				= dataID_ack;
					pSearchIndex->dummyMsg.header.issuedBy 				= pMsgOnly->header.recipientId;
					pSearchIndex->dummyMsg.header.msgCount 				= pMsgOnly->header.msgCount;
					pSearchIndex->dummyMsg.header.msgSize				= sizeof(tdMsgOnly);
					pSearchIndex->dummyMsg.header.recipientId			= whoId_MB;
					pSearchIndex->pNextInList = NULL;
				}
				// Search end of List
				tdDummyMsgList *pSearchIndex = pDummyMsgListCBPA;
				while (pSearchIndex->pNextInList != NULL)
				{
					pSearchIndex = pSearchIndex->pNextInList;
				}
				// Create new list entry at end
				pSearchIndex->pNextInList = (tdDummyMsgList *)pvPortMalloc(sizeof(tdDummyMsgList));
				// Step into the end element
				pSearchIndex = pSearchIndex->pNextInList;
				pSearchIndex->dummyMsg.header.dataId					= dataID_weightData;
				pSearchIndex->dummyMsg.header.issuedBy 					= whoId_CB;
				pSearchIndex->dummyMsg.header.msgCount 					= 42; // just any from CB
				pSearchIndex->dummyMsg.header.msgSize					= sizeof(tdMsgWeightData);
				pSearchIndex->dummyMsg.header.recipientId				= whoId_MB;
				pSearchIndex->dummyMsg.weightData.BodyFat				= 16;	// 16%
				pSearchIndex->dummyMsg.weightData.TimeStamp				= 123456789;
				pSearchIndex->dummyMsg.weightData.WSBatteryLevel		= 82;	// 82%
				pSearchIndex->dummyMsg.weightData.WSStatus				= 0;
				pSearchIndex->dummyMsg.weightData.Weight				= 782;	// 78.2 kg
				pSearchIndex->pNextInList = NULL;

				// Inform system that CB has an ACK for us!
				tdQtoken Qtoken;
				Qtoken.command	= command_BufferFromCBavailable;
				Qtoken.pData	= NULL;
				if ( xQueueSendToBack( allHandles.allQueueHandles.qhCBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
				{
					// Seemingly the queue is full and we already waited for a long time
					taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromCBavailable'");
				}
				// Inform system that CB has an Weight for us!
				Qtoken.command	= command_BufferFromCBavailable;
				Qtoken.pData	= NULL;
				if ( xQueueSendToBack( allHandles.allQueueHandles.qhCBPAin, &Qtoken, REMINDER_BLOCKING) != pdPASS )
				{
					// Seemingly the queue is full and we already waited for a long time
					taskMessage("E", "putData Function", "Queue of Dispatcher did not accept 'command_BufferFromCBavailable'");
				}
			}
		#endif
	}
	return(err);
}

// Defining special bytes for SLIP protocol
#define SLIP_END            0xC0		// (octal) 0300 (decimal) 192	indicates end of packet
#define SLIP_ESC            0xDB		// (octal) 0333 (decimal) 219	indicates byte stuffing
#define SLIP_ESC_END        0xDC		// (octal) 0334	(decimal) 220	ESC ESC_END means END data byte
#define SLIP_ESC_ESC        0xDD		// (octal) 0335 (decimal) 221	ESC ESC_ESC means ESC data byte

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  CB_ConvertToSlipTX
//!
//!
//! Converts the message to be sent to SLIP Protocol
//!
//! \param[in]      pointer to the raw transmission buffer (by reference)
//! \param[in]      quantity of raw data for transmission
//! \param[in]      pointer to the SLIP transmission buffer (by reference)
//!
//! \return         quantity of data to be sent
// ---------------------------------------------------------------------------------------------------------------------

/* DISBALED FOR INTEGRATION MFS 2012'07'26
uint16_t CB_ConvertToSlipTX(uint8_t *raw_tx_str, uint16_t rawtxlen, uint8_t *slip_tx_str)
{
    uint16_t x = 0;
    uint8_t D1 = 0;
    uint16_t idx = 0;

    if (rawtxlen>SLIP_BLEN) rawtxlen = SLIP_BLEN;
    slip_tx_str[idx] = SLIP_END; idx++;
    // for each byte in the packet, send the appropriate character sequence
    do{
        D1 = raw_tx_str[x]; x++;
        switch (D1)
        {
            // if it�s the same code as an END character, we send a special two character
            // code so as not to make the receiver think we sent an END
            case SLIP_END:
                slip_tx_str[idx] = SLIP_ESC; idx++;
                slip_tx_str[idx] = SLIP_ESC_END; idx++;
                break;
            // if it�s the same code as an ESC character, we send a special two character
            // code so as not to make the receiver think we sent an ESC
            case SLIP_ESC:
                slip_tx_str[idx] = SLIP_ESC; idx++;
                slip_tx_str[idx] = SLIP_ESC_ESC; idx++;
                break;
            // otherwise, we just send the character
            default:
                slip_tx_str[idx] = D1; idx++;
                break;
        }
        if(idx>= SLIP_BLEN) break;
    }while(x<rawtxlen);
    // tell the receiver that we�re done sending the packet
    slip_tx_str[idx] = SLIP_END; idx++;
    for(x=idx;x<SLIP_BLEN;x++)slip_tx_str[x]=0;
    return idx;
}*/



// -----------------------------------------------------------------------------------
//! \brief vRTMCB_CMDS_CONFIGHANDLER
//!
//! Executes state configuration by putData
//!
//! \param[IN]  pointer to ConfigMessage
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vRTMCB_CMDS_CONFIGHANDLER(void *startData){
    
    tdMsgConfigureState *msgConfigureState = (tdMsgConfigureState *) (startData);
    
    
    switch(msgConfigureState->stateToConfigure) {
            case wakdStates_AllStopped :{
                    
                 // BlPumpCtrl
                 // *(REG32 ACTUATOR_BP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.BLPumpCtrl.direction;
                 // *(REG32 ACTUATOR_BP_FLOWREF_ALLSTOPPED)	= msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
                    
                    RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                        // DIRECTION
                    putdata_blpump_direction = SENS_DIRECT;
                    RTMCB_WHICHDIRECTION = putdata_blpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
                        // VALUE: PWM% SETTING
                        // DEMO, if BP run -> run at 575, else STOP
                    if(msgConfigureState->actCtrl.BLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW  > 100){
                      RTMCB_ACT_REFERENCE = _SPEED_CODE_0575R;
                    }
                    else{
                      RTMCB_ACT_REFERENCE = _SPEED_CODE_STOP;
                    }
                    // READY TO SEND THE COMMAND...
                    RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                    
                    while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                    RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                //FLPumpCtrl
                //*(REG32 ACTUATOR_FP_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.FLPumpCtrl.direction;
                //*(REG32 ACTUATOR_FP_FLOWREF_ALLSTOPPED)	= msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW;
                
                    RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // FL PUMP DRIVEN BY PWM %
                        // DIRECTION
                    putdata_flpump_direction = SENS_DIRECT;
                    RTMCB_WHICHDIRECTION = putdata_flpump_direction;      // SENS_REVERSE (it is the same, here it has no meaning)
                        // VALUE: PWM% SETTING
                        // DEMO, if FP run -> run at 575, else STOP
                    if(msgConfigureState->actCtrl.FLPumpCtrl.flowReference/FIXPOINTSHIFT_PUMP_FLOW > 100){
                      RTMCB_ACT_REFERENCE = _SPEED_CODE_0575R;
                    }
                    else{
                      RTMCB_ACT_REFERENCE = _SPEED_CODE_STOP;
                    }
                    // READY TO SEND THE COMMAND...
                    RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                    
                    while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                    RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;

                //Polarizer    
                //*(REG32 ACTUATOR_PZ_DIRECTION_ALLSTOPPED)	= msgConfigureState->actCtrl.PolarizationCtrl.direction;
                // *(REG32 ACTUATOR_PZ_VOLTREF_ALLSTOPPED)	= msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
                   
                   // DEVICE
                    RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                    // DIRECTION
                    RTMCB_WHICHDIRECTION = msgConfigureState->actCtrl.PolarizationCtrl.direction;
                    // VALUE: VOLTAGE IN mV          
                    
                    
                    putdata_polarizer_voltage = msgConfigureState->actCtrl.PolarizationCtrl.voltageReference/FIXPOINTSHIFT_POLARIZ_VOLT;
                    
                    if(putdata_polarizer_voltage < VOLTAGE_MIN) { putdata_polarizer_voltage = VOLTAGE_MIN; }
                    if(putdata_polarizer_voltage >= VOLTAGE_MAX) { putdata_polarizer_voltage = VOLTAGE_MAX; }
                       
                    RTMCB_ACT_REFERENCE = putdata_polarizer_voltage;
                    // READY TO SEND THE COMMAND...
                    RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                    while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                    RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                // SWITCHES
                   
                  // DEVICE
                  RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                  // DIRECTION
                  RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                  RTMCB_ACT_REFERENCE = 0;
                  // READY TO SEND THE COMMAND...
                  RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                  while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                  RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                  break;
            }
     } // end switch
     

    
    /*  
    
    switch(scheduler_index){
        
        // ------------------------------------------ //
        // DEVICE: POLARIZER VOLTAGE
        // ------------------------------------------ //
        case POLARIZER_SWITCHON:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
            // ON_OFF
            RTMCB_ONNOFF = SIGNAL_ON;                     // SWITCH DEVICE ON
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_ONOFF;
        break;
        case POLARIZER_VOLTAGE:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
            // DIRECTION
            if(polarizer_direction == SENS_DIRECT) polarizer_direction = SENS_REVERSE;
            else polarizer_direction = SENS_DIRECT;
            RTMCB_WHICHDIRECTION = polarizer_direction;
            // VALUE: VOLTAGE IN mV          
            if(polarizer_downToUp==1){
                polarizer_voltage+= VOLTAGE_FACTOR;           // Up to VOLTAGE_MAX, steps: VOLTAGE_FACTOR
                if(polarizer_voltage>=VOLTAGE_MAX) polarizer_downToUp = 2;                
            }
            else{
                polarizer_voltage-= VOLTAGE_FACTOR;           // Up to VOLTAGE_MAX, steps: VOLTAGE_FACTOR
                if(polarizer_voltage<=VOLTAGE_MIN) polarizer_downToUp = 1;                
            }
            RTMCB_ACT_REFERENCE = polarizer_voltage;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES
        // ------------------------------------------ //
        case VALVES3_POSITION:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            switch(valves3_sequence){
                case 1:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                break;
                case 2:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                break;
                case 3:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
                break;
                case 4:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W2;
                break;
                case 5:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                break;
                case 6:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                break;
                case 7:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
                break;
                case 8:
                    RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W2;
                break;
            }
            valves3_sequence++;
            if(valves3_sequence==9)valves3_sequence=1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ NORMAL POSITION
        // ------------------------------------------ //
        case VALVE_NORM:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ REG 1 POSITION
        // ------------------------------------------ //
        case VALVE_REG:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
          
        break;

        // ------------------------------------------ //
        // GETTING PARTICULAR BLOCKS OF DATA
        // ACTUATOR DATA SHOULD BE ANALYZED 
        // AFTER HAVE MODIFIED SOME SETTINGS
        // ------------------------------------------ //
        case GETDATA_BY_GROUP:
            // CHOOSE A GROUP OF DATA AMONG THE AVAILABLE
            RTMCB_WHICHGROUP = RS422_ACT_DATA;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETGROUPDATA;
        break;
        // ------------------------------------------ //
        // GET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case GETRTMCB_RTC:
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETCLOCK;
        break;
        // ------------------------------------------ //
        // SET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case SETRTMCB_RTC:
            // PRESET RTMCB_RTC VALUES
            RTMCB_YEAR = 2012;
            RTMCB_MONTH = 6;
            RTMCB_DAY = 9;
            RTMCB_HOURS = 9;
            RTMCB_MINUTES = 29;
            RTMCB_SECONDS = 19;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETCLOCK;
        break;
        // ------------------------------------------ //
        // OPERATING MODES
        // 20120609: NOT YET AVAILABLE AS AUTOMATIC IN RTMCB
        // ------------------------------------------ //
        case OPMODES_CONFIGURE:
            // SETTING THE OPERATING MODE
            RTMCB_OPERATING_MODE = opmodes_counter;
            opmodes_counter++; 
            if(opmodes_counter>MICROFLUIDIC_OM_MAX) opmodes_counter = MICROFLUIDIC_OM_MIN;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
        break;
    }*/

}


// -----------------------------------------------------------------------------------
//! \brief vRTMCB_CMDS_STATEHANDLER
//!
//! Executes state CHANGE
//!
//! \param[IN]  enum changetostate
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vRTMCB_CMDS_STATEHANDLER(tdWakdStates stateId){
    
        
    switch(stateId) {
            case wakdStates_AllStopped :
            default: {
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_STOP;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_STOP;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = VOLTAGE_MIN;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
            case wakdStates_NoDialysate :{
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_STOP;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = VOLTAGE_MIN;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
            case wakdStates_Dialysis :{
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = 1000;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
            case wakdStates_Regen1 :{
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = 1000;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
            case wakdStates_Regen2 :{
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = 1000;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W2 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W2;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
            case wakdStates_Ultrafiltration :{
                    
                 // BlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_BLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                 
                 // FlPumpCtrl
                 RTMCB_WHICHDEVICE = RS422CTRL_FLPPSC;         // BL PUMP DRIVEN BY PWM %
                 RTMCB_ACT_REFERENCE = _SPEED_CODE_0320R;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // DEVICE
                 RTMCB_WHICHDEVICE = RS422CTRL_POLAR;          // POLARIZER
                 RTMCB_ACT_REFERENCE = 1000;
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;                 
                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                
                 // SWITCHES
                 RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
                 RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
                 RTMCB_ACT_REFERENCE = 0;
                 // READY TO SEND THE COMMAND...
                 RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;

                 while(RTMCB_TASK_NEW_MB_CMD != SIGNAL_OFF) {}
                 RTMCB_TASK_NEW_MB_CMD = SIGNAL_ON;
                    
                 break;
            }
     } // end switch
     

    
    /*    
    switch(scheduler_index){        
        
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ NORMAL POSITION
        // ------------------------------------------ //
        case VALVE_NORM:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W1 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
        break;
        // ------------------------------------------ //
        // DEVICE: 3-VALVES _ REG 1 POSITION
        // ------------------------------------------ //
        case VALVE_REG:
            // DEVICE
            RTMCB_WHICHDEVICE = RS422CTRL_3VALVES;          // 3-VALVES POSITION
            // DIRECTION
            RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_W1 | VALVE_3X_MFSO_W2 | VALVE_3X_MFSI_W1;
            RTMCB_ACT_REFERENCE = 0;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETSETPOINT;
          
        break;

        // ------------------------------------------ //
        // GETTING PARTICULAR BLOCKS OF DATA
        // ACTUATOR DATA SHOULD BE ANALYZED 
        // AFTER HAVE MODIFIED SOME SETTINGS
        // ------------------------------------------ //
        case GETDATA_BY_GROUP:
            // CHOOSE A GROUP OF DATA AMONG THE AVAILABLE
            RTMCB_WHICHGROUP = RS422_ACT_DATA;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETGROUPDATA;
        break;
        // ------------------------------------------ //
        // GET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case GETRTMCB_RTC:
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_GETCLOCK;
        break;
        // ------------------------------------------ //
        // SET RTMCB RTC CURRENT VALUE
        // ------------------------------------------ //
        case SETRTMCB_RTC:
            // PRESET RTMCB_RTC VALUES
            RTMCB_YEAR = 2012;
            RTMCB_MONTH = 6;
            RTMCB_DAY = 9;
            RTMCB_HOURS = 9;
            RTMCB_MINUTES = 29;
            RTMCB_SECONDS = 19;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_SETCLOCK;
        break;
        // ------------------------------------------ //
        // OPERATING MODES
        // 20120609: NOT YET AVAILABLE AS AUTOMATIC IN RTMCB
        // ------------------------------------------ //
        case OPMODES_CONFIGURE:
            // SETTING THE OPERATING MODE
            RTMCB_OPERATING_MODE = opmodes_counter;
            opmodes_counter++; 
            if(opmodes_counter>MICROFLUIDIC_OM_MAX) opmodes_counter = MICROFLUIDIC_OM_MIN;
            // READY TO SEND THE COMMAND...
            RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_MICROFLUIDICS_CONFIG;
        break;
    }*/
}