note left of MB
	At startup FC is receiving configuration
	data from DS. After FC is configured it
	proceeds configuring the RTB.
	During normal operation reconfigurations are
	a) if user (GUI) changes values of conf
	b) during dialysis FC computed new actuator
end note
note over MB
	tdMsgConfigureState *varMsg;
	varMsg->header.recipientId = whoId_RTB;
	varMsg->header.msgSize  = sizeof(tdMsgConfigureState);
	varMsg->header.dataId   = dataID_configureState;
	varMsg->header.msgCount = 42;
	varMsg->header.issuedBy = whoId_MB;
	varMsg->stateToConfigure = wakdStates_Dialysis;
	varMsg->actCtrl.TimeStamp_ActCtrl = 0;
	varMsg->actCtrl.BLPumpCtrl.direction = 0;
	varMsg->actCtrl.BLPumpCtrl.FlowReference = 33;
	varMsg->actCtrl.FLPumpCtrl.direction = 0;
	varMsg->actCtrl.FLPumpCtrl.FlowReference = 44;
	varMsg->actCtrl.PolarizationCtrl.direction = 0;
	varMsg->actCtrl.PolarizationCtrl.voltageReference = 55;
end note
MB -> RTB: putData(&varMsg);
note over MB
	free(varMsg);
end note
