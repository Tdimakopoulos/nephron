/**
  ******************************************************************************
  * @file RTC/src/eeprom.c 
  * @author  MCD Application Team
  * @version  V2.0.0
  * @date  04/27/2009
  * @brief  This file provides the EEPROM emulation firmware functions
  *        used for Time Stamping in Clock/Calendar Application
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */ 


/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private variables ---------------------------------------------------------*/
uint16_t Value;

extern uint8_t TamperNumber;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Erases EEPROM Page
  * @param  None
  * @retval : Status of the last operation (Flash write or erase) done during
  *   EEPROM formating
  */
FLASH_Status EE_Format(void)
{
  FLASH_Status FlashStatus = FLASH_COMPLETE;
  
  TamperNumber = 1;
  BKP_WriteBackupRegister(BKP_DR5,TamperNumber);

  /* Erase Page */
  FlashStatus = FLASH_ErasePage(PAGE0_BASE_ADDRESS);
  
  /* If erase operation was failed, a Flash error code is returned */
  if (FlashStatus != FLASH_COMPLETE)
  {
    return FlashStatus;
  }
  
  return FlashStatus;
}



/**
  * @brief  Writes variable data in EEPROM.
  * @param Data: 16 bit data to be written
  * @retval : - Success or error status:
  * @param FLASH_COMPLETE: on success,
  * @param PAGE_FULL: if valid page is full
  * @param Flash error code: on write Flash error
  */
uint16_t EE_WriteVariable(uint16_t Data)
{
  uint32_t LoggingTimer;
  FLASH_Status FlashStatus = FLASH_COMPLETE; 
  /* Get the valid Page start Address */
  uint32_t Address;
  uint32_t PageEndAddress;

  Address = (uint32_t)(PAGE0_BASE_ADDRESS);

  /* Get the valid Page end Address */
  PageEndAddress = (uint32_t)(PAGE0_END_ADDRESS);

  /* Check each active page address starting from begining */
  while (Address < PageEndAddress)
  {
    /* Verify if Address and Address+2 contents are 0xFFFFFFFF */
    if ((*(__IO uint32_t*)Address) == 0xFFFFFFFF)
    {
      /* Set variable data */
      FlashStatus = FLASH_ProgramHalfWord(Address, Data);
      /* If program operation was failed, a Flash error code is returned */
      if (FlashStatus != FLASH_COMPLETE)
      {
        return FlashStatus;
      }
      /* Return program operation status */
      return FlashStatus;
    }
    else
    {
      /* Next address location */
      Address = Address + 2;
    }
  }
  
  // "MEMORY FULL ERASING LOG"
  EE_Format();
  LoggingTimer=RTC_GetCounter();
  
  while((RTC_GetCounter() - LoggingTimer) < 2)
  {
  }
  
  return PAGE_FULL;
}



/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
