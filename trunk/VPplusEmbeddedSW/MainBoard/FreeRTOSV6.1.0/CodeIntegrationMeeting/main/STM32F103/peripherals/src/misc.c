// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   misc.c
//! \brief  miscellaneous functions
//!
//! This file provides all the miscellaneous firmware functions.
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
#define AIRCR_VECTKEY_MASK    ((uint32_t)0x05FA0000)

// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  NVIC_PriorityGroup
//!
//! Configures the priority grouping: pre-emption priority and subpriority
//!
//! \param[in]      NVIC_PriorityGroup : specifies the priority grouping bits length. 
//!                 This parameter can be one of the following values:
//!                 NVIC_PriorityGroup_0: 0 bits for pre-emption priority
//!                                       4 bits for subpriority
//!                 NVIC_PriorityGroup_1: 1 bits for pre-emption priority
//!                                       3 bits for subpriority
//!                 NVIC_PriorityGroup_2: 2 bits for pre-emption priority
//!                                       2 bits for subpriority
//!                 NVIC_PriorityGroup_3: 3 bits for pre-emption priority
//!                                       1 bits for subpriority
//!                 NVIC_PriorityGroup_4: 4 bits for pre-emption priority
//!                                       0 bits for subpriority
//! \return         none
// -----------------------------------------------------------------------------------
void NVIC_PriorityGroupConfig(uint32_t NVIC_PriorityGroup)
{
  // Check the parameters 
  assert_param(IS_NVIC_PRIORITY_GROUP(NVIC_PriorityGroup));
  
  // Set the PRIGROUP[10:8] bits according to NVIC_PriorityGroup value 
  SCB->AIRCR = AIRCR_VECTKEY_MASK | NVIC_PriorityGroup;
}
// -----------------------------------------------------------------------------------
//! \brief  NVIC_Init
//!
//! Initializes the NVIC peripheral according to the specified parameters in the NVIC_InitStruct
//!
//! \param[in,out]  NVIC_InitStruct: 
//!                   pointer to a NVIC_InitTypeDef structure
//!                   that contains the configuration information for the
//!                   specified NVIC peripheral
//!
//! \return         none
// -----------------------------------------------------------------------------------
void NVIC_Init(NVIC_InitTypeDef* NVIC_InitStruct)
{
  uint32_t tmppriority = 0x00, tmppre = 0x00, tmpsub = 0x0F;
  
  // Check the parameters 
  assert_param(IS_FUNCTIONAL_STATE(NVIC_InitStruct->NVIC_IRQChannelCmd));
  assert_param(IS_NVIC_PREEMPTION_PRIORITY(NVIC_InitStruct->NVIC_IRQChannelPreemptionPriority));  
  assert_param(IS_NVIC_SUB_PRIORITY(NVIC_InitStruct->NVIC_IRQChannelSubPriority));
    
  if (NVIC_InitStruct->NVIC_IRQChannelCmd != DISABLE)
  {
    // Compute the Corresponding IRQ Priority --------------------------------
    tmppriority = (0x700 - ((SCB->AIRCR) & (uint32_t)0x700))>> 0x08;
    tmppre = (0x4 - tmppriority);
    tmpsub = tmpsub >> tmppriority;

    tmppriority = (uint32_t)NVIC_InitStruct->NVIC_IRQChannelPreemptionPriority << tmppre;
    tmppriority |=  NVIC_InitStruct->NVIC_IRQChannelSubPriority & tmpsub;
    tmppriority = tmppriority << 0x04;
        
    NVIC->IP[NVIC_InitStruct->NVIC_IRQChannel] = tmppriority;
    
    // Enable the Selected IRQ Channels --------------------------------------
    NVIC->ISER[NVIC_InitStruct->NVIC_IRQChannel >> 0x05] =
      (uint32_t)0x01 << (NVIC_InitStruct->NVIC_IRQChannel & (uint8_t)0x1F);
  }
  else
  {
    // Disable the Selected IRQ Channels -------------------------------------
    NVIC->ICER[NVIC_InitStruct->NVIC_IRQChannel >> 0x05] =
      (uint32_t)0x01 << (NVIC_InitStruct->NVIC_IRQChannel & (uint8_t)0x1F);
  }
}

// -----------------------------------------------------------------------------------
//! \brief  NVIC_SetVectorTable
//!
//! Sets the vector table location and Offset
//!
//! \param[in]      NVIC_VectTab : specifies if the vector table is in RAM or FLASH memory
//!                 This parameter can be one of the following values:
//!                 NVIC_VectTab_RAM
//!                 NVIC_VectTab_FLASH
//!
//! \param[in]      Offset: Vector Table base offset field. This value must be a multiple of 0x100.
//!
//! \return         none
// -----------------------------------------------------------------------------------
void NVIC_SetVectorTable(uint32_t NVIC_VectTab, uint32_t Offset)
{ 
  // Check the parameters 
  assert_param(IS_NVIC_VECTTAB(NVIC_VectTab));
  assert_param(IS_NVIC_OFFSET(Offset));  
   
  SCB->VTOR = NVIC_VectTab | (Offset & (uint32_t)0x1FFFFF80);
}



// -----------------------------------------------------------------------------------
//! \brief  NVIC_SystemLPConfig
//!
//! Selects the condition for the system to enter low power mode
//!
//! \param[in]      LowPowerMode: Specifies the new mode for the system to enter low power mode.
//!                 This parameter can be one of the following values:
//!                     NVIC_LP_SEVONPEND
//!                     NVIC_LP_SLEEPDEEP
//!                     NVIC_LP_SLEEPONEXIT
//!
//! \param[in]      NewState:new state of LP condition.
//!                 This parameter can be: ENABLE or DISABLE.
//!
//! \return         none
// -----------------------------------------------------------------------------------
void NVIC_SystemLPConfig(uint8_t LowPowerMode, FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_NVIC_LP(LowPowerMode));
  assert_param(IS_FUNCTIONAL_STATE(NewState));  
  
  if (NewState != DISABLE)
  {
    SCB->SCR |= LowPowerMode;
  }
  else
  {
    SCB->SCR &= (uint32_t)(~(uint32_t)LowPowerMode);
  }
}

// ---------------------------------------------------------------------------------------------------------------------
//! \brief  Clears the pending bit for an interrupt.
//!
//! This function Clears the pending bit for an interrupt.
//!
//! \param  IRQn_Type       IRQn specified the IRQ vector number
//!
//! \return void
// ---------------------------------------------------------------------------------------------------------------------
void NVIC_ClearIRQPendingIRQ(IRQn_Type IRQn)
{
  NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));
}


// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------

