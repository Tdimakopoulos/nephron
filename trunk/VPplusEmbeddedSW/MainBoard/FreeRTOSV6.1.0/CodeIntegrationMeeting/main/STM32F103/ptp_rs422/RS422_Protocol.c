// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   RS422_Protocol.c
//! \brief  RS422 Implementation to communicate with the RTMCB
//!
//! Collection of data communication routines
//!
//! \author  Dudnik G.S.
//! \date    11.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
 // ----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  RS422_FILL_PACKET_VALUE08
//!
//! Analyze Adds Interval Packet 1 Value 8-bit
//!
//! \param   uint32_t
//! \return  uint32_t
// -----------------------------------------------------------------------------------
void RS422_FILL_PACKET_VALUE08(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t packet_flags, uint16_t packet_size, uint8_t channel_no, uint32_t timeStamp, uint16_t interval, uint8_t value8){
        
    // PACKET FRAME ------------------------------
    // [3:4] PACKET FLAGS
    BufferXYZ[TXI[0]] = (packet_flags >> 8);          // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_flags & 0x00FF);
    TXI[0]++;
    
    // [5:6] PACKET SIZE $$$ (TBM) $$$
    packet_size = (7);                                // 4: timestamp, 2: interval, 1: value
    BufferXYZ[TXI[0]] = (packet_size >> 8);           // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET CHANNEL NO
    BufferXYZ[TXI[0]] = channel_no;
    TXI[0]++;
    // PAYLOAD: TIMESTAMP (4), INTERVAL (2), DATA (1)
    // Timestamp [8..11]
    SET32(&BufferXYZ[0], TXI[0], timeStamp); 
    TXI[0]+=4;
    // Interval [12:13]
    SET16(&BufferXYZ[0], TXI[0], interval);
    TXI[0]+=2;
    // Data [14:15] 
    SET08(&BufferXYZ[0], TXI[0], value8);
    TXI[0]+=1;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_FILL_PACKET_VALUE16
//!
//! Analyze Adds Interval Packet 1 Value 16-bit
//!
//! \param   uint32_t
//! \return  uint32_t
// -----------------------------------------------------------------------------------
void RS422_FILL_PACKET_VALUE16(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t packet_flags, uint16_t packet_size, uint8_t channel_no, uint32_t timeStamp, uint16_t interval, uint16_t value16){
        
    // PACKET FRAME ------------------------------
    // [3:4] PACKET FLAGS
    BufferXYZ[TXI[0]] = (packet_flags >> 8);          // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_flags & 0x00FF);
    TXI[0]++;
    
    // [5:6] PACKET SIZE $$$ (TBM) $$$
    packet_size = (8);                                // 4: timestamp, 2: interval, 2: value
    BufferXYZ[TXI[0]] = (packet_size >> 8);           // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET CHANNEL NO
    BufferXYZ[TXI[0]] = channel_no;
    TXI[0]++;
    // PAYLOAD: TIMESTAMP (4), INTERVAL (2), DATA (2)
    // Timestamp [8..11]
    SET32(&BufferXYZ[0], TXI[0], timeStamp); 
    TXI[0]+=4;
    // Interval [12:13]
    SET16(&BufferXYZ[0], TXI[0], interval);
    TXI[0]+=2;
    // Data [14:15] 
    SET16(&BufferXYZ[0], TXI[0], value16);
    TXI[0]+=2;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_FILL_PACKET_VALUE32
//!
//! Analyze Adds Interval Packet 1 Value 32-bit
//!
//! \param   uint32_t
//! \return  uint32_t
// -----------------------------------------------------------------------------------
void RS422_FILL_PACKET_VALUE32(uint8_t* BufferXYZ, uint16_t* TXI, uint16_t packet_flags, uint16_t packet_size, uint8_t channel_no, uint32_t timeStamp, uint16_t interval, uint32_t value32){
        
    // PACKET FRAME ------------------------------
    // [3:4] PACKET FLAGS
    BufferXYZ[TXI[0]] = (packet_flags >> 8);          // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_flags & 0x00FF);
    TXI[0]++;
    
    // [5:6] PACKET SIZE $$$ (TBM) $$$
    packet_size = (10);                               // 4: timestamp, 2: interval, 4: value
    BufferXYZ[TXI[0]] = (packet_size >> 8);           // $$$ compose $$$
    TXI[0]++;
    BufferXYZ[TXI[0]] = (packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET CHANNEL NO
    BufferXYZ[TXI[0]] = channel_no;
    TXI[0]++;
    // PAYLOAD: TIMESTAMP (4), INTERVAL (2), DATA (4)
    // Timestamp [8..11]
    SET32(&BufferXYZ[0], TXI[0], timeStamp); 
    TXI[0]+=4;
    // Interval [12:13]
    SET16(&BufferXYZ[0], TXI[0], interval);
    TXI[0]+=2;
    // Data [14..17] 
    SET32(&BufferXYZ[0], TXI[0], value32);
    TXI[0]+=4;
}
// -----------------------------------------------------------------------------------
// ANSWERS
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_ANS_NP_SYS_ACKRTMCBREADY
//!
//! Sends an answer telling that MB knows that RTMCB is ready
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_ANSWER_NP_SYS_ACKRTMCBREADY (uint8_t* BufferXYZ, uint8_t comm_seqno){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;

    uint16_t TXI[2];

    TXI[0] = 0;
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    q_packet_size = 3; // seqno + type|class + opcode 
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_SYS | COMMUNICATION_OPCODEFLAGS_RESPONSE);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_SYS_SENDDEVICEREADY;
    TXI[0]++;    
    query_rep_len = TXI[0];
    return query_rep_len;
}

// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_ANSWER_NP_DATA_ACKINTERVAL
//!
//! Sends an answer telling that MB has received an Interval message
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_ANSWER_NP_DATA_ACKINTERVAL (uint8_t* BufferXYZ, uint8_t comm_seqno){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;

    uint16_t TXI[2];

    TXI[0] = 0;
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    q_packet_size = 3; // seqno + type|class + opcode 
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_DATA | COMMUNICATION_OPCODEFLAGS_RESPONSE);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_DATA_SENDSIGNALS;
    TXI[0]++;    
    query_rep_len = TXI[0];
    return query_rep_len;
}

// -----------------------------------------------------------------------------------
// COMMANDS
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_SetSimulationRTMode
//!
//! Sends a CMD telling if RTMCB should send real or simulated data
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_SetSimulationRTMode (uint8_t* BufferXYZ, uint8_t comm_seqno){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;

    uint16_t TXI[2];

    TXI[0] = 0;
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    q_packet_size = 6; // seqno + type|class + opcode + param + size + data(1)
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETRESET_SIMULATIONMODE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_SIMULNRT;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1; // param data size
    TXI[0]++;    
    BufferXYZ[TXI[0]] = RTMCB_SimulationNoRT; 
    TXI[0]++;
    query_rep_len = TXI[0];
    return query_rep_len;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_StartStopStreaming
//!
//! Sends a CMD telling if RTMCB should send real or simulated data
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_StartStopStreaming (uint8_t* BufferXYZ, uint8_t comm_seqno){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;

    uint16_t TXI[2];

    TXI[0] = 0;
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    q_packet_size = 6; // seqno + type|class + opcode + param + size + data(1)
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_STARTSTOP_STREAMING;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_STARTNSTOP;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1; // param data size
    TXI[0]++;    
    BufferXYZ[TXI[0]] = RTMCB_StreamingYesNo; 
    TXI[0]++;
    query_rep_len = TXI[0];
    return query_rep_len;
}

// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_GET_GROUP_DATA
//!
//! Sends a CMD asking the RTMCB to send ONE GROUP OF DATA (WHICHGROUP)
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! param3: WHICHGROUP
//!
//! RS422_FULLSTATUS                1
//! RS422_PS_DATA                   2
//! RS422_ECP_DATA                  3
//! RS422_ACT_DATA                  4
//! RS422_ALARMS_DATA               5
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_GET_GROUP_DATA (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHGROUP){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;

    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    q_packet_size = 6; // seqno + type|class + opcode + param + size + data(�)
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_DATA | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_DATA_READSIGNALGROUP;
    TXI[0]++;    
    // PARAMETER: WHICH GROUP
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_DATA_WHICHGROUP;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;          // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = WHICHGROUP; // WHICH GROUP
    TXI[0]++;    
    // ----------------------------------------   
    query_rep_len = TXI[0];
    return query_rep_len;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_SetSetPoints
//!
//! Sends a CMD to the RTMCB modifying a SETPOINT of an ACTUATOR
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//!
//! param3: WHICHDEVICE
//!
//! RS422CTRL_MFSI                  12
//! RS422CTRL_MFSO                  13
//! RS422CTRL_MFSBL                 14
//! RS422CTRL_BLPUMP                15
//! RS422CTRL_FLPUMP                16
//! RS422CTRL_POLAR                 17
//! RS422CTRL_3VALVES               18
//! RS422CTRL_BLPPSC                21  // BLPUMP SPEED CODE
//! RS422CTRL_FLPPSC                22  // FLPUMP SPEED CODE
//!
//! param4: WHICHDIRECTION
//!
//! 4A. PUMPS
//!
//! SENS_DIRECT                     1     // *LEFT-WAY1
//! SENS_REVERSE                    2     // *RIGHT-WAY2
//! SENS_TOGGLE                     3     // TESTING PURPOSES
//!
//! 4B. 3-VALVES
//!
//! RTMCB_WHICHDIRECTION = VALVE_3X_MFSU_Wx | VALVE_3X_MFSO_Wx | VALVE_3X_MFSI_Wx
//! VALVE_3X_MFSI_W1                0x01
//! VALVE_3X_MFSI_W2                0x02
//! VALVE_3X_MFSO_W1                0x04
//! VALVE_3X_MFSO_W2                0x08
//! VALVE_3X_MFSU_W1                0x10
//! VALVE_3X_MFSU_W2                0x20
//!
//! param5: ACT_REFERENCE (PUMPS, POLARIZER, VALVES)
//!
//! 5A. PUMP SPEED CONTROL BY CODE
//!
//! _SPEED_CODE_1000L               1
//! _SPEED_CODE_0915L               2
//! _SPEED_CODE_0830L               3
//! _SPEED_CODE_0745L               4
//! _SPEED_CODE_0660L               5
//! _SPEED_CODE_0575L               6
//! _SPEED_CODE_0490L               7
//! _SPEED_CODE_0405L               8
//! _SPEED_CODE_0320L               9
//! _SPEED_CODE_0235L               10
//! _SPEED_CODE_0150L               11
//! _SPEED_CODE_0065L               12
//! _SPEED_CODE_STOP                13
//! _SPEED_CODE_0065R               14
//! _SPEED_CODE_0150R               15
//! _SPEED_CODE_0235R               16
//! _SPEED_CODE_0320R               17
//! _SPEED_CODE_0405R               18
//! _SPEED_CODE_0490R               19
//! _SPEED_CODE_0575R               20
//! _SPEED_CODE_0660R               21
//! _SPEED_CODE_0745R               22
//! _SPEED_CODE_0830R               23
//! _SPEED_CODE_0915R               24 
//! _SPEED_CODE_1000R               25
//!
//! 5B. POLARIZER SETPOINT
//!
//! VOLTAGE_MIN                     0
//! VOLTAGE_MAX                     4500  // mV 0.001 x V
//! VOLTAGE_FACTOR                  10
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_SetSetPoints (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHDEVICE, uint8_t WHICHDIRECTION, uint16_t ACT_REFERENCE){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    // seqno + type|class + opcode + 
    // param + size + data(1) + 
    // param + size + data(1) + 
    // param + size + data(2) 
    q_packet_size = 13; 
    
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETSETPOINTS;
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 1: WHICH DEVICE,1,DEVICENO 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_WHICH_DEVICE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = WHICHDEVICE;          // DEVICE NO
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 2: WHICH DIRECTION,1,DIRECTION 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_DIRECTION;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = WHICHDIRECTION;       // DIRECTION NO
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 3: VALUE: VOLTAGE, SPEEDREFERENCE,2,ACT_REFERENCE 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_REFERENCE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 2;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = (ACT_REFERENCE>>8)&0xFF;  // VALUE OF REFERENCE FOR ACTUATOR H
    TXI[0]++;    
    BufferXYZ[TXI[0]] = (ACT_REFERENCE)&0xFF;     // VALUE OF REFERENCE FOR ACTUATOR L
    TXI[0]++;    
    // ----------------------------------------
    query_rep_len = TXI[0];
    return query_rep_len;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_Switch_ONnOFF
//!
//! Sends a CMD to the RTMCB to SWITCH ON/OFF a device
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//!
//! param3: WHICHDEVICE
//!
//! RS422CTRL_MFSI                  12
//! RS422CTRL_MFSO                  13
//! RS422CTRL_MFSBL                 14
//! RS422CTRL_BLPUMP                15
//! RS422CTRL_FLPUMP                16
//! RS422CTRL_POLAR                 17
//!
//! param4: ONNOFF
//! SIGNAL_ON                       1
//! SIGNAL_OFF                      0
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_Switch_ONnOFF (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t WHICHDEVICE, uint8_t ONNOFF){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    // seqno + type|class + opcode + 
    // param + size + data(1) + 
    // param + size + data(1) 
    q_packet_size = 9; 
    
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SWITCHONOFF_DEVICE;
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 1: WHICH DEVICE,1,DEVICENO 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_WHICH_DEVICE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = WHICHDEVICE;          // DEVICE NO
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 2: DEVICE ON/OFF,1,ONnOFF 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_ONNOFF;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = ONNOFF;               // DIRECTION NO
    TXI[0]++;    
    // ----------------------------------------
    query_rep_len = TXI[0];
    return query_rep_len;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_SetMicrofluidicCircuit
//!
//! Sends a CMD to RTMCB changing the OPERATING MODE
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! param3: OPERATING MODE
//! 
//! MICROFLUIDIC_STOP               1
//! MICROFLUIDIC_DIALYSIS           2
//! MICROFLUIDIC_REGENERATION_1     3
//! MICROFLUIDIC_ULTRAFILTRATION    4
//! MICROFLUIDIC_REGENERATION_2     5
//! MICROFLUIDIC_MAINTENANCE        6
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_SetMicrofluidicCircuit (uint8_t* BufferXYZ, uint8_t comm_seqno, uint8_t OPERATING_MODE){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    // seqno + type|class + opcode + 
    // param + size + data(1)
    q_packet_size = 6; 
    
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETMICROFLUIDIC_WORKINGMODE;
    TXI[0]++;    
    // ----------------------------------------   
    // PARAMETER 1: OPERATING MODE,1, MODE 
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_NEPHRONPLUS_CTRL_MICROFLUIDIC_MODE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                    // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = OPERATING_MODE;       // OPERATING MODE
    TXI[0]++;    
    // ----------------------------------------   
    query_rep_len = TXI[0];
    return query_rep_len;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_GetClock
//!
//! Asks for current values of RTMCB Clock
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_GetClock (uint8_t* BufferXYZ, uint8_t comm_seqno){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
    // seqno + type|class + opcode
    q_packet_size = 3; 
    
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_SYSTEM | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_SYSTEM_GETCLOCK;
    TXI[0]++;    
    // ----------------------------------------   
    query_rep_len = TXI[0];
    return query_rep_len;
}

// -----------------------------------------------------------------------------------
//! \brief  RS422_MAKE_NP_TX_PACKET_SetClock
//!
//! Sets the RTMCB CLock
//!
//! param1: uint8_t* BufferXYZ (transmission buffer)
//! 
//! params3...8: DATE & TIME
//! 
//! YEAR MONTH DAY HOURS MINUTES SECONDS
//!
//! YYYY MM DD hh mm ss
//!
//! \return uint16_t qty of raw data to be sent
// -----------------------------------------------------------------------------------
uint16_t RS422_MAKE_NP_TX_PACKET_SetClock (uint8_t* BufferXYZ, uint8_t comm_seqno, uint16_t YEAR, uint8_t MONTH, uint8_t DAY, uint8_t HOURS, uint8_t MINUTES, uint8_t SECONDS){

    uint16_t query_rep_len = 0;
    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint16_t TXI[2];
    // ----------------------------------------
    TXI[0] = 0;
    // FRAME ----------------------------------
    // [0:1] FRAME NO
    BufferXYZ[TXI[0]] = (q_frame_number >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (q_frame_number & 0x00FF);
    TXI[0]++;
    // [2] FRAME FLAGS     
    BufferXYZ[TXI[0]] = q_frame_flags;                  // $$$ compose $$$
    TXI[0]++;
    // PACKET FRAME ---------------------------
    // [3:4] PACKET[1] FLAGS
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_flags & 0x00FF);
    TXI[0]++;
    // [5:6] PACKET[1] SIZE $$$ (TBM) $$$

    // seqno + type|class + opcode
    // param + size + data(2)
    // (param + size + data(1)) x 5 
    q_packet_size = 22; 
    
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size >> 8);
    TXI[0]++;
    BufferXYZ[TXI[0]] = (uint8_t)(q_packet_size & 0x00FF);
    TXI[0]++;
    // [7] PACKET[1] CHANNEL NO
    BufferXYZ[TXI[0]] = CHANNEL_CMDS;
    TXI[0]++;
    // sequence number
    BufferXYZ[TXI[0]] = comm_seqno;           // $$$ compose $$$
    TXI[0]++;
    // type | class
    BufferXYZ[TXI[0]] = (COMMUNICATION_CLASS_SYSTEM | COMMUNICATION_OPCODEFLAGS_COMMAND);
    TXI[0]++;
    BufferXYZ[TXI[0]] = COMMUNICATION_OPCODE_SYSTEM_SETCLOCK;
    TXI[0]++;    
     // ----------------------------------------   
    // YEAR
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_YEAR;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 2;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = (YEAR>>8)&0xFF;          // YEAR_H
    TXI[0]++;    
    BufferXYZ[TXI[0]] = (YEAR)&0xFF;             // YEAR_L
    TXI[0]++;    
    // ----------------------------------------   
    // MONTH
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MONTH;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = MONTH;                    // MONTH
    TXI[0]++;    
    // ----------------------------------------   
    // DAY
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_DAY;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = DAY;                      // DAY
    TXI[0]++;    
    // ----------------------------------------   
    // HOURS
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_HOUR;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = HOURS;                    // HOURS
    TXI[0]++;    
    // ----------------------------------------   
    // MINUTES
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MINUTE;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = MINUTES;                  // MINUTES
    TXI[0]++;    
    // ----------------------------------------   
    // SECONDS
    BufferXYZ[TXI[0]] = COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_SECOND;
    TXI[0]++;    
    BufferXYZ[TXI[0]] = 1;                        // PARAMETER SIZE
    TXI[0]++;    
    BufferXYZ[TXI[0]] = SECONDS;                  // SECONDS
    TXI[0]++;    
    // ----------------------------------------   
    query_rep_len = TXI[0];
    return query_rep_len;
}

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// COMMANDS SCHEDULER
// -----------------------------------------------------------------------------------
//! \brief  RS422_RTMCB_SEND_COMMAND
//!
//! Sends a CMD according to scheduler
//!
//! param1: index_to_scheduler
//! 
//! \return nothing
// -----------------------------------------------------------------------------------
uint16_t RS422_RTMCB_SEND_COMMAND (uint8_t index_to_scheduler){
    uint16_t query_rep_len = 0;
    switch (rs422_rtmcb_commands_scheduler[index_to_scheduler]){
        case RS422_SIMULATIONVSRT:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_SetSimulationRTMode(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
        break;
        case RS422_STSTSTREAMING:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_StartStopStreaming(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
        break;
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
        case RS422_ONOFF:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_Switch_ONnOFF(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno, RTMCB_WHICHDEVICE, RTMCB_ONNOFF);
        break;
        case RS422_SETSETPOINT:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_SetSetPoints(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno, RTMCB_WHICHDEVICE, RTMCB_WHICHDIRECTION, RTMCB_ACT_REFERENCE);
        break;
        case RS422_MICROFLUIDICS_CONFIG:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_SetMicrofluidicCircuit(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno, RTMCB_OPERATING_MODE);
        break;
        case RS422_GETGROUPDATA:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_GET_GROUP_DATA(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno, RTMCB_WHICHGROUP);
        break;
        case RS422_GETCLOCK:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_GetClock(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno);
        break;
        case RS422_SETCLOCK:
            query_rep_len = RS422_MAKE_NP_TX_PACKET_SetClock(&RAW_Buffer_RTMCB[0], rs422_rtmcb_sequenceno, RTMCB_YEAR, RTMCB_MONTH, RTMCB_DAY, RTMCB_HOURS, RTMCB_MINUTES, RTMCB_SECONDS);
        break;
#endif
        case RS422_NOGROUP:
        break;
    }
}

// -----------------------------------------------------------------------------------
// ANALYSIS
// -----------------------------------------------------------------------------------
//! \brief  AnalyzeCommandNephronPlusSys
//!
//! CLASS NEPHRONPLUS_SYS
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//! \param[IN]      class_rs422
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeCommandNephronPlusSys(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn)
{
    uint8_t opcode_rs422 = RX[RXI];
    uint8_t parameterid_rs422 = 0;
    uint8_t paramsize_rs422 = 0;
    uint32_t paramvalue_rs422 = 0;
    
    RXI++;
    switch (opcode_rs422)
    {
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_SYS_SENDDEVICEREADY):
            //COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_DEVICESTATCONFIG
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue_rs422 = GET32(RX, RXI);
            RXI += 4;
            // HERE WE KNOW THAT RTMCB IS READY
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_DEVICESTATCONFIG)
            {
                if ((paramsize_rs422 == 4) && (paramvalue_rs422==RTMCB_MAGICNO)) RTMCB_STATUS = DEVICE_READY;
            }
            rs422_rtmcb_whichgroup = RS422_ACK_RTMCB_READY;
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_SYS_GETCLOCK):
            // $$$$ has to send the clock....
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}
// -----------------------------------------------------------------------------------
//! \brief  AnalyzeResponseNephronPlusCtrl
//!
//! CLASS NEPHRONPLUS_CTRL
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeResponseNephronPlusCtrl(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn)
{
    uint8_t opcode_rs422 = RX[RXI];
    RXI++;
    switch (opcode_rs422){
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_RESET_DEVICE):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SENDSLEEP_DEVICE):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETSETPOINTS):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SWITCHONOFF_DEVICE):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETRESET_SIMULATIONMODE):
            rs422_rtmcb_whichgroup = RS422_SIMULATIONVSRT;
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_STARTSTOP_STREAMING):
            rs422_rtmcb_whichgroup = RS422_STSTSTREAMING;
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SETMICROFLUIDIC_WORKINGMODE):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_CTRL_SWITCHONOFF_DEVICES):
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}

// -----------------------------------------------------------------------------------
//! \brief  AnalyzeResponseNephronPlusSys
//!
//! CLASS NEPHRONPLUS_SYS
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeResponseNephronPlusSys(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn)
{
    uint8_t parameterid_rs422 = 0;
    uint8_t paramsize_rs422  = 0;
    uint32_t paramvalue_rs422  = 0;
    uint8_t r_ok = 0;

    uint32_t response[4];

    uint8_t opcode_rs422 = RX[RXI];
    RXI++;
    switch (opcode_rs422)
    {
        case (COMMUNICATION_OPCODE_NEPHRONPLUS_SYS_GETDEVICEREADY):
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue_rs422 = GET32(RX, RXI);
            RXI += 4;
            // HERE WE KNOW THAT RTMCB IS READY
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_DEVICESTATCONFIG)
            {
                if ((paramsize_rs422 == 4) && (paramvalue_rs422 == RTMCB_MAGICNO)) RTMCB_STATUS = DEVICE_READY;
            }

            okyn[0] = true;
            break;
        case(COMMUNICATION_OPCODE_NEPHRONPLUS_SYS_READSTATUS):
            r_ok = 0;
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue_rs422 = GET32(RX, RXI);
            RXI += 4;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_STATUS_RTMCB_STTSTAMP)
            {
                if (paramsize_rs422 == 4)
                {
                    response[0] = paramvalue_rs422;
                    r_ok++;
                }
            }
            // RTMCBINFO.ONnOFFDevices
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue_rs422 = GET32(RX, RXI);
            RXI += 4;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_STATUS_RTMCB_ONOFFDEVS)
            {
                if (paramsize_rs422 == 4)
                {
                    response[1] = paramvalue_rs422;
                    r_ok++;
                }
            }
            // RTMCBINFO.Status
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue_rs422 = GET32(RX, RXI);
            RXI += 4;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_NEPHRONPLUS_SYS_STATUS_RTMCB_STATUS)
            {
                if (paramsize_rs422 == 4)
                {
                    response[2] = paramvalue_rs422;
                    r_ok++;
                }
            }
            // ASSIGN VALUES
            if (r_ok == 3)
            {
                rtmcbTimeStamp = response[0];
                rtmcbONnOFFDevices_get = response[1];
                rtmcbStatus = response[2];
                okyn[0] = true;
            }
            else
            {
                okyn[0] = false;
            }
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}

// -----------------------------------------------------------------------------------
//! \brief  AnalyzeResponseCommonSystem
//!
//! CLASS COMMON SYSTEM
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeResponseCommonSystem(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn)
{
    uint8_t opcode_rs422 = RX[RXI];
    // READ RTC DATA
    uint16_t r_Year = 0;
    uint8_t r_Month = 0;
    uint8_t r_Day = 0;
    uint8_t r_Hours = 0;
    uint8_t r_Minutes = 0;
    uint8_t r_Seconds = 0;
    uint8_t r_ok = 0;
    uint8_t parameterid_rs422 = 0;
    uint8_t paramsize_rs422 = 0;
    uint16_t paramvalue16_rs422 = 0;
    uint8_t paramvalue08_rs422 = 0;
    RXI++;
    switch (opcode_rs422)
    {
        case (COMMUNICATION_OPCODE_SYSTEM_GETCLOCK):
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_YEAR
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue16_rs422 = GET16(RX, RXI);
            RXI += 2;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_YEAR)
            {
                if (paramsize_rs422 == 2) r_Year = paramvalue16_rs422;
                r_ok++;
            }
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MONTH
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue08_rs422 = RX[RXI];
            RXI ++;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MONTH)
            {
                if (paramsize_rs422 == 1) r_Month = paramvalue08_rs422;
                r_ok++;
            }
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_DAY
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue08_rs422 = RX[RXI];
            RXI++;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_DAY)
            {
                if (paramsize_rs422 == 1) r_Day = paramvalue08_rs422;
                r_ok++;
            }
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_HOURS
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue08_rs422 = RX[RXI];
            RXI++;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_HOUR)
            {
                if (paramsize_rs422 == 1) r_Hours = paramvalue08_rs422;
                r_ok++;
            }
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MINUTE
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue08_rs422 = RX[RXI];
            RXI++;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_MINUTE)
            {
                if (paramsize_rs422 == 1) r_Minutes = paramvalue08_rs422;
                r_ok++;
            }
            //COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_SECOND
            parameterid_rs422 = RX[RXI];
            RXI++;
            paramsize_rs422 = RX[RXI];
            RXI++;
            paramvalue08_rs422 = RX[RXI];
            RXI++;
            if (parameterid_rs422 == COMMUNICATION_PARAMETERID_SYSTEM_CLOCK_SECOND)
            {
                if (paramsize_rs422 == 1) r_Seconds = paramvalue08_rs422;
                r_ok++;
            }
            if (r_ok == 6)
            {
                // 2011-7-9 15:11:7
                getYear = r_Year;
                getMonth = r_Month;
                getDay = r_Day;
                getHours = r_Hours;
                getMinutes = r_Minutes;
                getSeconds = r_Seconds;
                okyn[0] = true;
            }
            else
            {
                okyn[0] = false;
            }
            break;
        case (COMMUNICATION_OPCODE_SYSTEM_SETCLOCK):
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}

// -----------------------------------------------------------------------------------
//! \brief  AnalyzeCommand
//!
//! ...
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//! \param[IN]      class_rs422
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeCommand(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn, uint8_t class_rs422)
{
    switch (class_rs422)
    {
        case (COMMUNICATION_CLASS_ACQUISITION):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_BOOTMANAGER):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_COMMUNICATION):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_MEMORY):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_DATA):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_SYS):
            RXI = AnalyzeCommandNephronPlusSys(RX, RXI, RXlen, okyn);
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_POWER):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_SYSTEM):
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}
// -----------------------------------------------------------------------------------
//! \brief  AnalyzeResponse
//!
//! ...
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//! \param[IN]      class_rs422
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeResponse(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn, uint8_t class_rs422)
{
    switch (class_rs422)
    {
        case (COMMUNICATION_CLASS_ACQUISITION):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_BOOTMANAGER):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_COMMUNICATION):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_MEMORY):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_CTRL):
            RXI = AnalyzeResponseNephronPlusCtrl(RX, RXI, RXlen, okyn);
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_DATA):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_NEPHRONPLUS_SYS):
            RXI = AnalyzeResponseNephronPlusSys(RX, RXI, RXlen, okyn);
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_POWER):
            okyn[0] = true;
            break;
        case (COMMUNICATION_CLASS_SYSTEM):
            RXI = AnalyzeResponseCommonSystem(RX, RXI, RXlen, okyn);
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    return RXI;
}
        

// -----------------------------------------------------------------------------------
//! \brief  AnalyzeStructuredPacket
//!
//! Decodes an Interval Packet and reads the values
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN/OUT]  okyn
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeStructuredPacket(uint8_t* RX, uint16_t RXI, uint16_t RXlen, boolean_t* okyn)
{
    uint8_t seqno_rs422 = RX[RXI]; 
    RXI++;
    uint8_t opcode_rs422 = (uint8_t)(RX[RXI] & COMMUNICATION_OPCODEFLAGS_MASK);
    uint8_t class_rs422 = (uint8_t)(RX[RXI] & COMMUNICATION_CLASS_MASK);
    RXI++;
    switch (opcode_rs422)
    {
        case (COMMUNICATION_OPCODEFLAGS_COMMAND):
            RXI = AnalyzeCommand(RX, RXI, RXlen, okyn, class_rs422);
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODEFLAGS_RESPONSE):
            RXI = AnalyzeResponse(RX, RXI, RXlen, okyn, class_rs422);
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODEFLAGS_STATUS):
            okyn[0] = true;
            break;
        case (COMMUNICATION_OPCODEFLAGS_ERROR):
            okyn[0] = true;
            break;
        default:
            okyn[0] = false;
            break;
    }
    if(okyn[0] == true) {
        RTMCB_STATUS = COMMAND;
        //rs422_rtmcb_whichgroup = RS422_ACK_RECV_CMD;
    }
    else {
        RTMCB_STATUS = NONE;
        rs422_rtmcb_whichgroup = RS422_NOGROUP;
    }
    return RXI;
}
// -----------------------------------------------------------------------------------
//! \brief  AnalyzeIntervalPacket
//!
//! Decodes an Interval Packet and reads the values
//!
//! \param[IN]      RX
//! \param[IN/OUT]  RXI
//! \param[IN]      RXlen
//! \param[IN]      flags
//! \param[IN]      size
//! \param[IN]      CHANNEL
//! \param[IN/OUT]  okyn
//!
//! \return         RXI
// -----------------------------------------------------------------------------------
uint16_t AnalyzeIntervalPacket(uint8_t* RX, uint16_t RXI, uint16_t RXlen, uint16_t flags, uint16_t size, uint8_t CHANNEL, boolean_t* okyn)
{
    uint32_t dummy_timestamp = 0;
    uint16_t dummy_channelsScanInterval = 0;
    uint16_t dummy_value = 0;
    uint32_t dummy_dwvalue = 0;
    okyn[0] = false;
    switch (CHANNEL)
    {
        case CHANNEL_SODIUM_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataI.Sodium = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_SODIUM_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;

            break;
        case CHANNEL_POTASSIUM_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataI.Potassium = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_POTASSIUM_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_PHOSPHATE_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            dummy_value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PHOSPHATE_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_PH_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataI.pH = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PH_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_UREA_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataI.Urea = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_UREA_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_ECP_TEMP_FCI:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataI.Temperature = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_ECP_TEMP_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            ecp_message_received_counter++;
            break;
        case CHANNEL_ECP1_STATUS:

            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusECPI = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_ECP1_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case CHANNEL_SODIUM_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataO.Sodium = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_SODIUM_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_POTASSIUM_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataO.Potassium = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_POTASSIUM_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_PHOSPHATE_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            dummy_value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PHOSPHATE_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_PH_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataO.pH = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PH_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_UREA_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataO.Urea = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_UREA_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_ECP_TEMP_FCO:
            staticBufferPhysiologicalData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysiologicalData.ECPDataO.Temperature = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_ECP_TEMP_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physiologicalData;
            break;
        case CHANNEL_ECP2_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusECPO = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_ECP2_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case CHANNEL_PRESSURE_BCI:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.PressureBCI.Pressure = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PRESSURE_BCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_PRESSURE_BCO:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.PressureBCO.Pressure= GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PRESSURE_BCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_PRESSURE_FCI:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.PressureFCI.Pressure = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PRESSURE_FCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_PRESSURE_FCO:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.PressureFCO.Pressure = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PRESSURE_FCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_TEMPERATURE_BCI:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.TemperatureInOut.TemperatureInlet_Value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_TEMPERATURE_BCI;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_TEMPERATURE_BCO:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.TemperatureInOut.TemperatureOutlet_Value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_TEMPERATURE_BCO;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_CONDUCTIVITY_FCR:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.CSENS1Data.Cond_FCR = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_CONDUCTIVITY_FCR;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_CONDUCTIVITY_FCQ:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.CSENS1Data.Cond_FCQ = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_CONDUCTIVITY_FCQ;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_CONDUCTIVITY_T:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferPhysicalsensorData.CSENS1Data.Cond_PT1000 = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_CONDUCTIVITY_T;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            ps_message_received_counter++;
            break;
        case  CHANNEL_LEAKAGE_BC:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            //BLDData.Status[1] = GET16(RX, RXI);
            dummy_value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_LEAKAGE_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_LEAKAGE_FC:
            staticBufferPhysicalsensorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            //FLDData.Status[1] = GET16(RX, RXI);
            dummy_value = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_LEAKAGE_FC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_FLOW_CALC_BC:
            //BFlowData.TimeStamp[1] = GET32(RX, RXI);
            dummy_timestamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval= GET16(RX, RXI);
            RXI += 2;
            BFlowData.Value[1] = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_FLOW_CALC_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_FLOW_CALC_FC:
            //FFlowData.TimeStamp[1] = GET32(RX, RXI);
            dummy_timestamp =  GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            FFlowData.Value[1] = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_FLOW_CALC_FC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_physicalData;
            break;
        case  CHANNEL_PUMP_SPEED_BC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.BLPumpData.Speed = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PUMP_SPEED_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_PUMP_CURRENT_BC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.BLPumpData.Current= GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PUMP_CURRENT_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_PUMP_SPEED_FC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.FLPumpData.Speed = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PUMP_SPEED_FC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_PUMP_CURRENT_FC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.FLPumpData.Current = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_PUMP_CURRENT_FC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_MFSI_POSITION:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.MultiSwitchBIData.Position = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_MFSI_POSITION;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_MFSO_POSITION:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
           staticBufferActuatorData.MultiSwitchBOData.Position = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_MFSO_POSITION;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_MFSBL_POSITION:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.MultiSwitchBUData.Position = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_01 |= WRITTEN_MFSBL_POSITION;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_BPSI_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusBPSI = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_BPSI_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_BPSO_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusBPSO = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_BPSO_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_FPSI_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusFPSI = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_FPSI_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_FPSO_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusFPSO = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_FPSO_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_BTS_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusBTS = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_BTS_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_BLD_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            //BLDData.Status[1] = GET32(RX, RXI);
            dummy_dwvalue = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_BLD_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_FLD_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            //FLDData.Status[1] = GET32(RX, RXI);
            dummy_dwvalue = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_FLD_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_BLPUMP_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusBLPUMP = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_BLPUMP_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_FLPUMP_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusFLPUMP = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_FLPUMP_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_MFSI_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusMFSI = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_MFSI_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_MFSO_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusMFSO = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_MFSO_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_MFSBL_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusMFSBL = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_MFSBL_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case  CHANNEL_DCS_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusDCS = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_DCS_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case CHANNEL_POLAR_VOLTAGE:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.PolarizationData.Voltage = GET16(RX, RXI);
            RXI += 2;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_POLAR_VOLTAGE;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            act_message_received_counter++;
            break;
        case CHANNEL_POLAR_DIRECTION:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.PolarizationData.Direction = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_POLAR_DIRECTION;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case CHANNEL_POLAR_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusPOLAR = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_POLAR_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            break;
        case CHANNEL_ALARMS_STATUS:
            staticBufferAlarms.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferAlarms.Status = GET32(RX, RXI);
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_ALARMS_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_alarmRTB;
            alarms_message_received_counter++;
            break;
        case CHANNEL_RTMCB_STATUS:
            staticBufferDevicesStatus.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferDevicesStatus.statusRTMCB = GET32(RX, RXI);
            RXI += 4;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_RTMCB_STATUS;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_statusRTB;
            status_message_received_counter++;
            break;
        case  CHANNEL_PUMP_SPEEDCODE_BC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.BLPumpData.SpeedReferenceCode = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_PUMP_SPEEDCODE_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
        case  CHANNEL_PUMP_SPEEDCODE_FC:
            staticBufferActuatorData.TimeStamp = GET32(RX, RXI);
            RXI += 4;
            dummy_channelsScanInterval = GET16(RX, RXI);
            RXI += 2;
            staticBufferActuatorData.BLPumpData.SpeedReferenceCode = GET08(RX, RXI);
            RXI += 1;
            // WR OK FLAG
            RTMCB_channelsFlag_02 |= WRITTEN_PUMP_SPEEDCODE_BC;
            // CHANNEL READ OK
            okyn[0] = true;
            RTMCB_message = dataID_actuatorData;
            break;
    }
    if(okyn[0] == true) {
        RTMCB_STATUS = INTERVAL;
        rs422_rtmcb_whichgroup = RS422_ACK_RECV_DATA;
    }
    else { 
        RTMCB_STATUS = NONE;
        rs422_rtmcb_whichgroup = RS422_NOGROUP;
    }
    return RXI;
}
// -----------------------------------------------------------------------------------
//! \brief  RS422_ANALYZE_RESPONSE
//!
//! Analyze the reply received
//!
//! \param   uint32_t
//! \return  uint32_t
// -----------------------------------------------------------------------------------
uint16_t RS422_ANALYZE_RESPONSE(uint16_t ENC_PACKETS_LEN, uint8_t* BufferXYZ, uint8_t* RxBuffer, boolean_t* oks){

    uint16_t q_frame_number = 0;
    uint8_t q_frame_flags = 0;
    uint16_t q_packet_flags = 0;
    uint16_t q_packet_size = 0;
    uint8_t q_packet_channel_no = 0;
         
    uint8_t comm_opcode_flag = 0;
    uint8_t comm_class = 0;
    uint8_t comm_opcode = 0;
    uint16_t I0 = 0;

    uint16_t PACK_I0 = 3;
    uint16_t RXI[2];
    uint16_t TXI[2];

    RXI[0] = 0; RXI[1] = 0;
    TXI[0] = 0; TXI[1] = 0;
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    q_frame_number = ((RxBuffer[0]<<8)+ RxBuffer[1]);
    // [2] FRAME FLAGS     
    q_frame_flags = RxBuffer[2];
    
    // FRAME ---------------------------------------
    // [0:1] FRAME NO
    BufferXYZ[0] = (q_frame_number >> 8);
    BufferXYZ[1] = (q_frame_number & 0x00FF);
    // [2] FRAME FLAGS     
    BufferXYZ[2] = q_frame_flags;                  // $$$ compose $$$

    TXI[0] = 3;
    RXI[0] = 3;

    do{
        // PACKET 1 FRAME ------------------------------
        // [3:4] PACKET[1] FLAGS
        q_packet_flags = ((RxBuffer[RXI[0]]<<8) + RxBuffer[RXI[0]+1]);
        RXI[0]+=2;
        // [5:6] PACKET[1] SIZE $$$ (TBM) $$$
        q_packet_size = ((RxBuffer[RXI[0]]<<8) + RxBuffer[RXI[0]+1]);
        RXI[0]+=2;

        // if received size > than real string break;
        if((RXI[0]+q_packet_size+1)>ENC_PACKETS_LEN) break;
        
        // [7] PACKET[1] CHANNEL NO
        q_packet_channel_no = RxBuffer[RXI[0]];
        RXI[0]++;
        if(q_packet_channel_no == CHANNEL_CMDS){
            // ---------------------------------------------
            // STRUCTURED PACKET (COMMANDS-REPONSES)
            // ---------------------------------------------
            RXI[0] = AnalyzeStructuredPacket(RxBuffer, RXI[0], ENC_PACKETS_LEN, oks);
            if (RXI[0] >= ENC_PACKETS_LEN) break;    // return oks[0];
        }
        else{
            // ---------------------------------------------
            // INTERVAL PACKET (SAMPLES)
            // ---------------------------------------------
            RXI[0] = AnalyzeIntervalPacket(RxBuffer, RXI[0], ENC_PACKETS_LEN, q_packet_flags, q_packet_size, q_packet_channel_no, oks);
            if (RXI[0] >= ENC_PACKETS_LEN) break;    // return oks[0];
        }
    } while (RXI[0] < ENC_PACKETS_LEN);
    return 0;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
