// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   sdcard_spi1.h
//! \brief  spi1 for sdcard 
//!
//! spi1 initialization, send & receive data routines
//!
//! \author  Dudnik G.S.
//! \date    10/07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef SDCARD_SPI1_H_
#define SDCARD_SPI1_H_
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "..\\main\\global.h"
// -----------------------------------------------------------------------------------
// Exported constants & macros
// -----------------------------------------------------------------------------------
//#define SPI_MODE_HW // nCS by hardware (HW MODE DOES NOT WORK)
#define SPI_MODE_SW   // nCS by Software
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void SD_SPIConfig(void);
extern void SD_SendData(uint8_t mode, uint8_t _data);
extern uint8_t SD_ReceiveData(uint8_t mode);
extern uint8_t SD_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN);
extern uint8_t SD_SendReceive(uint8_t _dataIN);
// -----------------------------------------------------------------------------------
#endif /* SDCARD_SPI1_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
