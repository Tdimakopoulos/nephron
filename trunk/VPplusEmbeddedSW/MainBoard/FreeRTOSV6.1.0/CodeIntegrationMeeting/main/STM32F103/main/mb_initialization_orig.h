// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   mb_initialization.c
//! \brief  hardware initialization
//!
//! Routines for individual initialization devices and variables
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Define to prevent recursive inclusion
// -----------------------------------------------------------------------------------
#ifndef __MB_INITIALIZATION_H
#define __MB_INITIALIZATION_H
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported Data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported Constants
// -----------------------------------------------------------------------------------
// for PLL_DIV9, SYSTICK=1ms, HSE:25MHZ
// #define SYSTICK_FREQUENCY  150750
#ifdef STM32F_MB
// for PLL_DIV5, SYSTICK=1ms, HSE:14.7456MHZ
#define SYSTICK_FREQUENCY     74240//81920 // 8112            // for HSE=14.7456MHZ, PLLDIV=5 thigh = 1ms
#else
// for PLL_DIV3, SYSTICK=1ms, HSE:25MHZ
#define SYSTICK_FREQUENCY     37685           // OK for HSE=25MHZ, PLLDIV=3
#endif
#define SYSTEM_EXT_XTAL       14745600
// -----------------------------------------------------------------------------------
// NVIC Preemptive-priorities and Sub-priorities
// -----------------------------------------------------------------------------------
// For the time being RTMCB will use one different Priority Level per interrupt
// Sub-Priorities in this case make no sense, so they are all 0
// ----------------------------------------------------
// NVIC Preemptive-priorities
// ----------------------------------------------------
// MB
#define PreemptivePriority_SYSTICK                  0
#define PreemptivePriority_PUSHBUTTON_nINT_EXTI1    1
#define PreemptivePriority_DEBOUNCED_nMULTI_EXTI7   2
#define PreemptivePriority_RTMCB_UART4              3
#define PreemptivePriority_TIM1                     4
#define PreemptivePriority_TIM2                     5
#define PreemptivePriority_PM1uC_IRQ_EXTI15         6
#define PreemptivePriority_PM2uC_nIRQ_EXTI3         7
#define PreemptivePriority_RTMCBuC_nIRQ_EXTI4       8
#define PreemptivePriority_CB_USART2                9 
#define PreemptivePriority_PM1_USART3               10
#define PreemptivePriority_PM2_UART5                11
#define PreemptivePriority_MAINT_USB_USART1         12
#define PreemptivePriority_ACC_IRQ_EXTI9            13
#define PreemptivePriority_SD_DETECT_EXT4           14
// ----------------------------------------------------
// NVIC Sub-priorities
// ----------------------------------------------------
// MB
#define SubPriority_SYSTICK                         0
#define SubPriority_PUSHBUTTON_nINT_EXTI1           0
#define SubPriority_DEBOUNCED_nMULTI_EXTI7          0
#define SubPriority_RTMCB_UART4                     0
#define SubPriority_TIM1                            0
#define SubPriority_TIM2                            0
#define SubPriority_PM1uC_IRQ_EXTI15                0
#define SubPriority_PM2uC_nIRQ_EXTI3                0
#define SubPriority_RTMCBuC_nIRQ_EXTI4              0
#define SubPriority_CB_USART2                       0   
#define SubPriority_PM1_USART3                      0
#define SubPriority_PM2_UART5                       0
#define SubPriority_MAINT_USB_USART1                0
#define SubPriority_ACC_IRQ_EXTI9                   0
#define SubPriority_SD_DETECT_EXT4                  0
// -----------------------------------------------------------------------------------
// IO Definitions
// -----------------------------------------------------------------------------------
#define PORT_PM1    1
#define PORT_PM2    2
#define PORT_RTMCB  3

// ----------------------------------------------------------------
// CB: WCMuC_RTS (INPUT PULL_UP)
// Init State: ?
// ----------------------------------------------------------------
#define WCMuC_RTS_GPIO_PORT                 GPIOA
#define WCMuC_RTS_GPIO_CLK                  RCC_APB2Periph_GPIOA  
#define WCMuC_RTS_GPIO_PIN                  GPIO_Pin_0
#define WCMuC_RTS_GPIO_SPEED                GPIO_Speed_50MHz
#define WCMuC_RTS_GPIO_MODE                 GPIO_Mode_IN_FLOATING 
// ----------------------------------------------------------------
// CB: WCMuC_CTS (INPUT PULL_UP)
// [RTC: TIM2], TEST: EXTI1
// ----------------------------------------------------------------
#define WCMuC_CTS_GPIO_PORT                 GPIOA
#define WCMuC_CTS_GPIO_CLK                  RCC_APB2Periph_GPIOA  
#define WCMuC_CTS_GPIO_PIN                  GPIO_Pin_1
#define WCMuC_CTS_GPIO_SPEED                GPIO_Speed_50MHz
#define WCMuC_CTS_GPIO_MODE                 GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// CB: WCMuC_RX [USART2_TX]
// ----------------------------------------------------------------
#define WCMuC_RX_GPIO_PORT                  GPIOA
#define WCMuC_RX_GPIO_CLK                   RCC_APB2Periph_GPIOA  
#define WCMuC_RX_GPIO_PIN                   GPIO_Pin_2
#define WCMuC_RX_GPIO_SPEED                 GPIO_Speed_50MHz
#define WCMuC_RX_GPIO_MODE                  GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// CB: WCMuC_TX [USART2_RX]
// ----------------------------------------------------------------
#define WCMuC_TX_GPIO_PORT                  GPIOA
#define WCMuC_TX_GPIO_CLK                   RCC_APB2Periph_GPIOA  
#define WCMuC_TX_GPIO_PIN                   GPIO_Pin_3
#define WCMuC_TX_GPIO_SPEED                 GPIO_Speed_50MHz
#define WCMuC_TX_GPIO_MODE                  GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// ----------------------------------------------------------------
#define SD_SPI                              SPI1
#define SD_SPI_CLK                          RCC_APB2Periph_SPI1
// ----------------------------------------------------------------
// SDCARD: SD_SPI_nCS (OUTPUT PUSH-PULL)
// Init State: HIGH (SDCARD SPI disabled)
// ----------------------------------------------------------------
#define SD_SPI_nCS_GPIO_PORT                GPIOA
#define SD_SPI_nCS_GPIO_CLK                 RCC_APB2Periph_GPIOA  
#define SD_SPI_nCS_GPIO_PIN                 GPIO_Pin_4
#define SD_SPI_nCS_GPIO_SPEED               GPIO_Speed_50MHz
#ifdef SDCARD_CTRL_HW
#define SD_SPI_nCS_GPIO_MODE                GPIO_Mode_AF_PP
#else
#define SD_SPI_nCS_GPIO_MODE                GPIO_Mode_Out_PP
#endif
// ----------------------------------------------------------------
// SDCARD: SD_SPI_CLK (ALTERNATE PUSH-PULL)
// ----------------------------------------------------------------
#define SD_SPI_CLK_GPIO_PORT                GPIOA
#define SD_SPI_CLK_GPIO_CLK                 RCC_APB2Periph_GPIOA  
#define SD_SPI_CLK_GPIO_PIN                 GPIO_Pin_5
#define SD_SPI_CLK_GPIO_SPEED               GPIO_Speed_50MHz
#define SD_SPI_CLK_GPIO_MODE                GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// SDCARD: SD_SPI_MISO (INPUT FLOATING)
// ----------------------------------------------------------------
#define SD_SPI_MISO_GPIO_PORT               GPIOA
#define SD_SPI_MISO_GPIO_CLK                RCC_APB2Periph_GPIOA  
#define SD_SPI_MISO_GPIO_PIN                GPIO_Pin_6
#define SD_SPI_MISO_GPIO_SPEED              GPIO_Speed_50MHz
#define SD_SPI_MISO_GPIO_MODE               GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// SDCARD: SD_SPI_MOSI (ALTERNATE PUSH-PULL)
// ----------------------------------------------------------------
#define SD_SPI_MOSI_GPIO_PORT               GPIOA
#define SD_SPI_MOSI_GPIO_CLK                RCC_APB2Periph_GPIOA  
#define SD_SPI_MOSI_GPIO_PIN                GPIO_Pin_7
#define SD_SPI_MOSI_GPIO_SPEED              GPIO_Speed_50MHz
#define SD_SPI_MOSI_GPIO_MODE               GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// DEBUGGING LED (OUTPUT PUSH-PULL)
// Init State: LOW (LED ON)
// ----------------------------------------------------------------
#define DEBUG_LED_GPIO_PORT                 GPIOA
#define DEBUG_LED_GPIO_CLK                  RCC_APB2Periph_GPIOA  
#define DEBUG_LED_GPIO_PIN                  GPIO_Pin_8
#define DEBUG_LED_GPIO_SPEED                GPIO_Speed_50MHz
#define DEBUG_LED_GPIO_MODE                 GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// MAINTENANCEIF: PC_RX (STM-TX)	
// [USART1_TX] (ALTERNATE PUSH-PULL)
// ----------------------------------------------------------------
#define PC_RX_GPIO_PORT                     GPIOA
#define PC_RX_GPIO_CLK                      RCC_APB2Periph_GPIOA  
#define PC_RX_GPIO_PIN                      GPIO_Pin_9
#define PC_RX_GPIO_SPEED                    GPIO_Speed_50MHz
#define PC_RX_GPIO_MODE                     GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// MAINTENANCEIF: PC_TX (STM-RX)	
// [USART1_RX] (INPUT FLOATING)
// ----------------------------------------------------------------
#define PC_TX_GPIO_PORT                     GPIOA
#define PC_TX_GPIO_CLK                      RCC_APB2Periph_GPIOA  
#define PC_TX_GPIO_PIN                      GPIO_Pin_10
#define PC_TX_GPIO_SPEED                    GPIO_Speed_50MHz
#define PC_TX_GPIO_MODE                     GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// MAINTENANCEIF: FTDI_nRESET (OUT PUSH-PULL)
// Init State: HIGH (NO RESET)
// ----------------------------------------------------------------
#define FTDI_NRESET_GPIO_PORT               GPIOA
#define FTDI_NRESET_GPIO_CLK                RCC_APB2Periph_GPIOA  
#define FTDI_NRESET_GPIO_PIN                GPIO_Pin_11
#define FTDI_NRESET_GPIO_SPEED              GPIO_Speed_50MHz
#define FTDI_NRESET_GPIO_MODE               GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// MAINTENANCEIF: PC_USBVBUS (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC_USBVBUS_GPIO_PORT                GPIOA
#define PC_USBVBUS_GPIO_CLK                 RCC_APB2Periph_GPIOA  
#define PC_USBVBUS_GPIO_PIN                 GPIO_Pin_12
#define PC_USBVBUS_GPIO_SPEED               GPIO_Speed_50MHz
#define PC_USBVBUS_GPIO_MODE                GPIO_Mode_IPD
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_CTRL_RX (OUT PUSH-PULL)
// Init State: HIGH (RX DISABLED)
// ----------------------------------------------------------------
#define RTMCBuC_CTRL_RX_GPIO_PORT              GPIOB
#define RTMCBuC_CTRL_RX_GPIO_CLK               RCC_APB2Periph_GPIOB
#define RTMCBuC_CTRL_RX_GPIO_PIN               GPIO_Pin_0
#define RTMCBuC_CTRL_RX_GPIO_SPEED             GPIO_Speed_50MHz
#define RTMCBuC_CTRL_RX_GPIO_MODE              GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_CTRL_TX (OUT PUSH-PULL)
// Init State: LOW (TX DISABLED)
// ----------------------------------------------------------------
#define RTMCBuC_CTRL_TX_GPIO_PORT              GPIOB
#define RTMCBuC_CTRL_TX_GPIO_CLK               RCC_APB2Periph_GPIOB
#define RTMCBuC_CTRL_TX_GPIO_PIN               GPIO_Pin_1
#define RTMCBuC_CTRL_TX_GPIO_SPEED             GPIO_Speed_50MHz
#define RTMCBuC_CTRL_TX_GPIO_MODE              GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PB2 [BOOT1] (OUT PUSH-PULL)
// Init State: HIGH (arbitrary)
// ----------------------------------------------------------------
#define PB2_GPIO_PORT                       GPIOB
#define PB2_GPIO_CLK                        RCC_APB2Periph_GPIOB
#define PB2_GPIO_PIN                        GPIO_Pin_2
#define PB2_GPIO_SPEED                      GPIO_Speed_50MHz
#define PB2_GPIO_MODE                       GPIO_Mode_IPD 
// ----------------------------------------------------------------
// PB3: JTAG-JTDO
// ----------------------------------------------------------------
// PB3: JTAG-nJTRST
// ----------------------------------------------------------------
// PB5 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PB5_GPIO_PORT                       GPIOB
#define PB5_GPIO_CLK                        RCC_APB2Periph_GPIOB
#define PB5_GPIO_PIN                        GPIO_Pin_5
#define PB5_GPIO_SPEED                      GPIO_Speed_50MHz
#define PB5_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// SLEEPWKUP: PUSHBUTTON_nINT (INPUT PULL_DOWN)
// EXT INTERRUPT
// ----------------------------------------------------------------
#define PUSHBUTTON_nINT_GPIO_PORT           GPIOB
#define PUSHBUTTON_nINT_GPIO_CLK            RCC_APB2Periph_GPIOB
#define PUSHBUTTON_nINT_GPIO_PIN            GPIO_Pin_6
#define PUSHBUTTON_nINT_GPIO_SPEED          GPIO_Speed_50MHz
#define PUSHBUTTON_nINT_GPIO_MODE           GPIO_Mode_IPU
#define PUSHBUTTON_nINT_GPIO_IRQ            EXTI9_5_IRQn            // <-- EXTI6
// ----------------------------------------------------------------
// SLEEPWKUP: DEBOUNCED_nMULTI (INPUT PULL_UP)
// EXT INTERRUPT
// ----------------------------------------------------------------
#define DEBOUNCED_nMULTI_GPIO_PORT          GPIOB
#define DEBOUNCED_nMULTI_GPIO_CLK           RCC_APB2Periph_GPIOB  
#define DEBOUNCED_nMULTI_GPIO_PIN           GPIO_Pin_7
#define DEBOUNCED_nMULTI_GPIO_SPEED         GPIO_Speed_50MHz
#define DEBOUNCED_nMULTI_GPIO_MODE          GPIO_Mode_IPU
#define DEBOUNCED_nMULTI_GPIO_IRQ           EXTI9_5_IRQn              // <-- EXTI7
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_nRST (OUT PUSH-PULL)
// Init State: HIGH (NOT RESET)
// ----------------------------------------------------------------
#define RTMCBuC_nRST_GPIO_PORT              GPIOB
#define RTMCBuC_nRST_GPIO_CLK               RCC_APB2Periph_GPIOB
#define RTMCBuC_nRST_GPIO_PIN               GPIO_Pin_8
#define RTMCBuC_nRST_GPIO_SPEED             GPIO_Speed_50MHz
#define RTMCBuC_nRST_GPIO_MODE              GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_SYNC (OUT PUSH-PULL)
// Init State: lOW ()
// ----------------------------------------------------------------
#define RTMCBuC_SYNC_GPIO_PORT              GPIOB
#define RTMCBuC_SYNC_GPIO_CLK               RCC_APB2Periph_GPIOB
#define RTMCBuC_SYNC_GPIO_PIN               GPIO_Pin_9
#define RTMCBuC_SYNC_GPIO_SPEED             GPIO_Speed_50MHz
#define RTMCBuC_SYNC_GPIO_MODE              GPIO_Mode_IPD
// ----------------------------------------------------------------
// PM1: PM1uC_RX       
// [USART3_TX]        (ALTERNATE PUSH-PULL)
// ----------------------------------------------------------------
#define PM1uC_RX_GPIO_PORT                  GPIOB
#define PM1uC_RX_GPIO_CLK                   RCC_APB2Periph_GPIOB 
#define PM1uC_RX_GPIO_PIN                   GPIO_Pin_10
#define PM1uC_RX_GPIO_SPEED                 GPIO_Speed_50MHz
#define PM1uC_RX_GPIO_MODE                  GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// PM1: PM1uC_TX       
// [USART3_RX]        (INPUT FLOATING)
// ----------------------------------------------------------------
#define PM1uC_TX_GPIO_PORT                  GPIOB
#define PM1uC_TX_GPIO_CLK                   RCC_APB2Periph_GPIOB
#define PM1uC_TX_GPIO_PIN                   GPIO_Pin_11
#define PM1uC_TX_GPIO_SPEED                 GPIO_Speed_50MHz
#define PM1uC_TX_GPIO_MODE                  GPIO_Mode_IN_FLOATING

// ----------------------------------------------------------------
#define UIF_ACC_SPI                         SPI2
#define UIF_ACC_SPI_CLK                     RCC_APB1Periph_SPI2
// ----------------------------------------------------------------
// UIF: MSPARM_STE (OUT PUSH-PULL)
// Init State: HIGH (MSP SPI NOT SELECTED)
// ----------------------------------------------------------------
#define MSPARM_STE_GPIO_PORT                GPIOB
#define MSPARM_STE_GPIO_CLK                 RCC_APB2Periph_GPIOB
#define MSPARM_STE_GPIO_PIN                 GPIO_Pin_12
#define MSPARM_STE_GPIO_SPEED               GPIO_Speed_50MHz
#define MSPARM_STE_GPIO_MODE                GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// ACCELEROMETER: ACC_SPI_SCLK / UIF: MSPARM_CLK
// ----------------------------------------------------------------
#define SPI2_CLK_GPIO_PORT                  GPIOB
#define SPI2_CLK_GPIO_CLK                   RCC_APB2Periph_GPIOB
#define SPI2_CLK_GPIO_PIN                   GPIO_Pin_13
#define SPI2_CLK_GPIO_SPEED                 GPIO_Speed_50MHz
#define SPI2_CLK_GPIO_MODE                  GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// ACCELEROMETER: ACC_SPI_MISO / UIF: MSPARM_SOMI
// ----------------------------------------------------------------
#define SPI2_MISO_GPIO_PORT                 GPIOB
#define SPI2_MISO_GPIO_CLK                  RCC_APB2Periph_GPIOB
#define SPI2_MISO_GPIO_PIN                  GPIO_Pin_14
#define SPI2_MISO_GPIO_SPEED                GPIO_Speed_50MHz
#define SPI2_MISO_GPIO_MODE                 GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// ACCELEROMETER: ACC_SPI_MOSI / UIF: MSPARM_SIMO
// ----------------------------------------------------------------
#define SPI2_MOSI_GPIO_PORT                 GPIOB
#define SPI2_MOSI_GPIO_CLK                  RCC_APB2Periph_GPIOB
#define SPI2_MOSI_GPIO_PIN                  GPIO_Pin_15
#define SPI2_MOSI_GPIO_SPEED                GPIO_Speed_50MHz
#define SPI2_MOSI_GPIO_MODE                 GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// AB&C: RBAT_CHARGE (ANALOG ADC12_IN10) [PC0]
// ----------------------------------------------------------------
#define RBAT_CHARGE_GPIO_PORT               GPIOC
#define RBAT_CHARGE_GPIO_CLK                RCC_APB2Periph_GPIOC
#define RBAT_CHARGE_GPIO_PIN                GPIO_Pin_0
#define RBAT_CHARGE_GPIO_SPEED              GPIO_Speed_50MHz
#define RBAT_CHARGE_GPIO_MODE               GPIO_Mode_AIN
// ----------------------------------------------------------------
// PM: BPACK_VOLTAGE (ANALOG ADC12_IN11) [PC1]
// ----------------------------------------------------------------
#define BPACK_VOLTAGE_GPIO_PORT            GPIOC
#define BPACK_VOLTAGE_GPIO_CLK             RCC_APB2Periph_GPIOC
#define BPACK_VOLTAGE_GPIO_PIN             GPIO_Pin_1
#define BPACK_VOLTAGE_GPIO_SPEED           GPIO_Speed_50MHz
#define BPACK_VOLTAGE_GPIO_MODE            GPIO_Mode_AIN
// ----------------------------------------------------------------
// PM2: PM2uC_PRESENT (INPUT PULL_DOWN) [PC2]
// ----------------------------------------------------------------
#define PM2uC_PRESENT_GPIO_PORT             GPIOC
#define PM2uC_PRESENT_GPIO_CLK              RCC_APB2Periph_GPIOC
#define PM2uC_PRESENT_GPIO_PIN              GPIO_Pin_2
#define PM2uC_PRESENT_GPIO_SPEED            GPIO_Speed_50MHz
#define PM2uC_PRESENT_GPIO_MODE             GPIO_Mode_IPD
// ----------------------------------------------------------------
// PM2: PM2uC_nIRQ (INPUT PULL_UP) [PC3]
// ----------------------------------------------------------------
#define PM2uC_nIRQ_GPIO_PORT                GPIOC
#define PM2uC_nIRQ_GPIO_CLK                 RCC_APB2Periph_GPIOC
#define PM2uC_nIRQ_GPIO_PIN                 GPIO_Pin_3
#define PM2uC_nIRQ_GPIO_SPEED               GPIO_Speed_50MHz
#define PM2uC_nIRQ_GPIO_MODE                GPIO_Mode_IPU
#define PM2uC_nIRQ_GPIO_IRQ                 EXTI3_IRQn              // <-- EXTI
// ----------------------------------------------------------------
// SDCARD: SD_DETECT (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define SD_DETECT_GPIO_PORT                 GPIOC
#define SD_DETECT_GPIO_CLK                  RCC_APB2Periph_GPIOC
#define SD_DETECT_GPIO_PIN                  GPIO_Pin_4
#define SD_DETECT_GPIO_SPEED                GPIO_Speed_50MHz
#define SD_DETECT_GPIO_MODE                 GPIO_Mode_IPD
#define SD_DETECT_GPIO_IRQ                  EXTI4_IRQn             // <-- EXTI
// ----------------------------------------------------------------
// SDCARD: SD_PWR_EN (OUT PUSH_PULL)
// Init State: LOW (OFF)
// ----------------------------------------------------------------
#define SD_PWR_EN_GPIO_PORT                 GPIOC
#define SD_PWR_EN_GPIO_CLK                  RCC_APB2Periph_GPIOC
#define SD_PWR_EN_GPIO_PIN                  GPIO_Pin_5
#define SD_PWR_EN_GPIO_SPEED                GPIO_Speed_50MHz
#define SD_PWR_EN_GPIO_MODE                 GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PC6 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC6_GPIO_PORT                       GPIOC
#define PC6_GPIO_CLK                        RCC_APB2Periph_GPIOC
#define PC6_GPIO_PIN                        GPIO_Pin_6
#define PC6_GPIO_SPEED                      GPIO_Speed_50MHz
#define PC6_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PC7 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC7_GPIO_PORT                       GPIOC
#define PC7_GPIO_CLK                        RCC_APB2Periph_GPIOC
#define PC7_GPIO_PIN                        GPIO_Pin_7
#define PC7_GPIO_SPEED                      GPIO_Speed_50MHz
#define PC7_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PC8 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC8_GPIO_PORT                       GPIOC
#define PC8_GPIO_CLK                        RCC_APB2Periph_GPIOC
#define PC8_GPIO_PIN                        GPIO_Pin_8
#define PC8_GPIO_SPEED                      GPIO_Speed_50MHz
#define PC8_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PC9 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC9_GPIO_PORT                       GPIOC
#define PC9_GPIO_CLK                        RCC_APB2Periph_GPIOC
#define PC9_GPIO_PIN                        GPIO_Pin_9
#define PC9_GPIO_SPEED                      GPIO_Speed_50MHz
#define PC9_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_RX       
// [USART4_TX] (ALTERNATE PUSH_PULL)
// ----------------------------------------------------------------
#define RTMCBuC_RX_GPIO_PORT                GPIOC
#define RTMCBuC_RX_GPIO_CLK                 RCC_APB2Periph_GPIOC
#define RTMCBuC_RX_GPIO_PIN                 GPIO_Pin_10
#define RTMCBuC_RX_GPIO_SPEED               GPIO_Speed_50MHz
#define RTMCBuC_RX_GPIO_MODE                GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_TX       
// [USART4_RX] (INPUT FLOATING)
// ----------------------------------------------------------------
#define RTMCBuC_TX_GPIO_PORT                GPIOC
#define RTMCBuC_TX_GPIO_CLK                 RCC_APB2Periph_GPIOC
#define RTMCBuC_TX_GPIO_PIN                 GPIO_Pin_11
#define RTMCBuC_TX_GPIO_SPEED               GPIO_Speed_50MHz
#define RTMCBuC_TX_GPIO_MODE                GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// PC13 [RTC: Tamper Pin] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PC13_GPIO_PORT                      GPIOC
#define PC13_GPIO_CLK                       RCC_APB2Periph_GPIOC
#define PC13_GPIO_PIN                       GPIO_Pin_13
#define PC13_GPIO_SPEED                     GPIO_Speed_50MHz
#define PC13_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PM2: PM2uC_RX       
// [USART5_TX] (ALTERNATE PUSH-PULL)
// ----------------------------------------------------------------
#define PM2uC_RX_GPIO_PORT                  GPIOC
#define PM2uC_RX_GPIO_CLK                   RCC_APB2Periph_GPIOC
#define PM2uC_RX_GPIO_PIN                   GPIO_Pin_12
#define PM2uC_RX_GPIO_SPEED                 GPIO_Speed_50MHz
#define PM2uC_RX_GPIO_MODE                  GPIO_Mode_AF_PP
// ----------------------------------------------------------------
// ----------------------------------------------------------------
// CB: WCMuC_CTRL_TX (OUT PUSH-PULL)
// Init State: LOW (TX DISABLED)
// ----------------------------------------------------------------
#define WCMuC_CTRL_TX_GPIO_PORT             GPIOD
#define WCMuC_CTRL_TX_GPIO_CLK              RCC_APB2Periph_GPIOD
#define WCMuC_CTRL_TX_GPIO_PIN              GPIO_Pin_0
#define WCMuC_CTRL_TX_GPIO_SPEED            GPIO_Speed_50MHz
#define WCMuC_CTRL_TX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// CB: WCMuC_CTRL_RX (OUT PUSH-PULL)
// Init State: HIGH (RX DISABLED)
// ----------------------------------------------------------------
#define WCMuC_CTRL_RX_GPIO_PORT             GPIOD
#define WCMuC_CTRL_RX_GPIO_CLK              RCC_APB2Periph_GPIOD
#define WCMuC_CTRL_RX_GPIO_PIN              GPIO_Pin_1
#define WCMuC_CTRL_RX_GPIO_SPEED            GPIO_Speed_50MHz
#define WCMuC_CTRL_RX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM2: PM2uC_TX
// [USART5] (INPUT FLOATING)
// ----------------------------------------------------------------
#define PM2uC_TX_GPIO_PORT                  GPIOD
#define PM2uC_TX_GPIO_CLK                   RCC_APB2Periph_GPIOD
#define PM2uC_TX_GPIO_PIN                   GPIO_Pin_2
#define PM2uC_TX_GPIO_SPEED                 GPIO_Speed_50MHz
#define PM2uC_TX_GPIO_MODE                  GPIO_Mode_IN_FLOATING
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_PRESENT (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define RTMCBuC_PRESENT_GPIO_PORT           GPIOD
#define RTMCBuC_PRESENT_GPIO_CLK            RCC_APB2Periph_GPIOD
#define RTMCBuC_PRESENT_GPIO_PIN            GPIO_Pin_3
#define RTMCBuC_PRESENT_GPIO_SPEED          GPIO_Speed_50MHz
#define RTMCBuC_PRESENT_GPIO_MODE           GPIO_Mode_IPD
// ----------------------------------------------------------------
// RTMCBIF: RTMCBuC_nIRQ (INPUT PULL_UP)
// ----------------------------------------------------------------
#define RTMCBuC_nIRQ_GPIO_PORT              GPIOD
#define RTMCBuC_nIRQ_GPIO_CLK               RCC_APB2Periph_GPIOD
#define RTMCBuC_nIRQ_GPIO_PIN               GPIO_Pin_4
#define RTMCBuC_nIRQ_GPIO_SPEED             GPIO_Speed_50MHz
#define RTMCBuC_nIRQ_GPIO_MODE              GPIO_Mode_IPU
#define RTMCBuC_nIRQ_GPIO_IRQ               EXTI4_IRQn             // <-- EXTI
// ----------------------------------------------------------------
// PD5 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD5_GPIO_PORT                       GPIOD
#define PD5_GPIO_CLK                        RCC_APB2Periph_GPIOD
#define PD5_GPIO_PIN                        GPIO_Pin_5
#define PD5_GPIO_SPEED                      GPIO_Speed_50MHz
#define PD5_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD6 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD6_GPIO_PORT                       GPIOD
#define PD6_GPIO_CLK                        RCC_APB2Periph_GPIOD
#define PD6_GPIO_PIN                        GPIO_Pin_6
#define PD6_GPIO_SPEED                      GPIO_Speed_50MHz
#define PD6_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD7 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD7_GPIO_PORT                       GPIOD
#define PD7_GPIO_CLK                        RCC_APB2Periph_GPIOD
#define PD7_GPIO_PIN                        GPIO_Pin_7
#define PD7_GPIO_SPEED                      GPIO_Speed_50MHz
#define PD7_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// ACCELEROMETER: ACC_SPI_nCS (OUT PUSH-PULL)
// Init State: HIGH (NOT SELECTED)
// ----------------------------------------------------------------
#define ACC_SPI_nCS_GPIO_PORT               GPIOD
#define ACC_SPI_nCS_GPIO_CLK                RCC_APB2Periph_GPIOD
#define ACC_SPI_nCS_GPIO_PIN                GPIO_Pin_8
#define ACC_SPI_nCS_GPIO_SPEED              GPIO_Speed_50MHz
#define ACC_SPI_nCS_GPIO_MODE               GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// ACCELEROMETER: ACC_IRQ (INPUT PULL_UP)
// ----------------------------------------------------------------
#define ACC_IRQ_GPIO_PORT                   GPIOD
#define ACC_IRQ_GPIO_CLK                    RCC_APB2Periph_GPIOD
#define ACC_IRQ_GPIO_PIN                    GPIO_Pin_9
#define ACC_IRQ_GPIO_SPEED                  GPIO_Speed_50MHz
#define ACC_IRQ_GPIO_MODE                   GPIO_Mode_IPU
#define ACC_IRQ_GPIO_IRQ                    EXTI9_5_IRQn            // <-- EXTI9
// ----------------------------------------------------------------
// PD10 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD10_GPIO_PORT                      GPIOD
#define PD10_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD10_GPIO_PIN                       GPIO_Pin_10
#define PD10_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD10_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD11 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD11_GPIO_PORT                      GPIOD
#define PD11_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD11_GPIO_PIN                       GPIO_Pin_11
#define PD11_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD11_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD12 [NOT USED] (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PD12_GPIO_PORT                      GPIOD
#define PD12_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD12_GPIO_PIN                       GPIO_Pin_12
#define PD12_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD12_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD13 [NOT USED] (INPUT PULL-DOWN)
// ----------------------------------------------------------------
#define PD13_GPIO_PORT                      GPIOD
#define PD13_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD13_GPIO_PIN                       GPIO_Pin_13
#define PD13_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD13_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD14 [NOT USED] (INPUT PULL-DOWN)
// ----------------------------------------------------------------
#define PD14_GPIO_PORT                      GPIOD
#define PD14_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD14_GPIO_PIN                       GPIO_Pin_14
#define PD14_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD14_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// PD15 [NOT USED] (INPUT PULL-DOWN)
// ----------------------------------------------------------------
#define PD15_GPIO_PORT                      GPIOD
#define PD15_GPIO_CLK                       RCC_APB2Periph_GPIOD
#define PD15_GPIO_PIN                       GPIO_Pin_15
#define PD15_GPIO_SPEED                     GPIO_Speed_50MHz
#define PD15_GPIO_MODE                      GPIO_Mode_IPD
// ----------------------------------------------------------------
// MAINTENANCEIF: PC_OUTEN (OUT PUSH-PULL)
// Init State: LOW (OUT DISABLED)
// ----------------------------------------------------------------
#define PC_OUTEN_GPIO_PORT                  GPIOE
#define PC_OUTEN_GPIO_CLK                   RCC_APB2Periph_GPIOE
#define PC_OUTEN_GPIO_PIN                   GPIO_Pin_0
#define PC_OUTEN_GPIO_SPEED                 GPIO_Speed_50MHz
#define PC_OUTEN_GPIO_MODE                  GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// USB_PWR_EN (OUT PUSH-PULL)
// Init State: LOW (OFF)
// ----------------------------------------------------------------
#define USB_PWR_EN_GPIO_PORT                GPIOE
#define USB_PWR_EN_GPIO_CLK                 RCC_APB2Periph_GPIOE
#define USB_PWR_EN_GPIO_PIN                 GPIO_Pin_1
#define USB_PWR_EN_GPIO_SPEED               GPIO_Speed_50MHz
#define USB_PWR_EN_GPIO_MODE                GPIO_Mode_Out_PP 
// ----------------------------------------------------------------
// PE2 [NOT USED] (INPUT PULL-DOWN)
// ----------------------------------------------------------------
#define PE2_GPIO_PORT                       GPIOE
#define PE2_GPIO_CLK                        RCC_APB2Periph_GPIOE
#define PE2_GPIO_PIN                        GPIO_Pin_2
#define PE2_GPIO_SPEED                      GPIO_Speed_50MHz
#define PE2_GPIO_MODE                       GPIO_Mode_IPD
// ----------------------------------------------------------------
// PM2: PM2uC_CTRL_RX (OUT PUSH-PULL)
// Init State: HIGH (RX DISABLED)
// ----------------------------------------------------------------
#define PM2uC_CTRL_RX_GPIO_PORT             GPIOE
#define PM2uC_CTRL_RX_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PM2uC_CTRL_RX_GPIO_PIN              GPIO_Pin_3
#define PM2uC_CTRL_RX_GPIO_SPEED            GPIO_Speed_50MHz
#define PM2uC_CTRL_RX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM2: PM2uC_CTRL_TX (OUT PUSH-PULL)
// Init State: LOW (TX DISABLED)
// ----------------------------------------------------------------
#define PM2uC_CTRL_TX_GPIO_PORT             GPIOE
#define PM2uC_CTRL_TX_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PM2uC_CTRL_TX_GPIO_PIN              GPIO_Pin_4
#define PM2uC_CTRL_TX_GPIO_SPEED            GPIO_Speed_50MHz
#define PM2uC_CTRL_TX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM2: MB2uC_PRESENT (OUT PUSH-PULL)
// Init State: HIGH (MB PRESENT)
// ----------------------------------------------------------------
#define MB2uC_PRESENT_GPIO_PORT             GPIOE
#define MB2uC_PRESENT_GPIO_CLK              RCC_APB2Periph_GPIOE
#define MB2uC_PRESENT_GPIO_PIN              GPIO_Pin_5
#define MB2uC_PRESENT_GPIO_SPEED            GPIO_Speed_50MHz
#define MB2uC_PRESENT_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM2: PM2uC_SYNC (OUT PUSH-PULL)
// Init State: LOW ()
// ----------------------------------------------------------------
#define PM2uC_SYNC_GPIO_PORT                GPIOE
#define PM2uC_SYNC_GPIO_CLK                 RCC_APB2Periph_GPIOE
#define PM2uC_SYNC_GPIO_PIN                 GPIO_Pin_6
#define PM2uC_SYNC_GPIO_SPEED               GPIO_Speed_50MHz
#define PM2uC_SYNC_GPIO_MODE                GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// BPACK: POWER_GOOD_1 (INPUT PULL_UP)
// ----------------------------------------------------------------
#define POWER_GOOD_1_GPIO_PORT              GPIOE
#define POWER_GOOD_1_GPIO_CLK               RCC_APB2Periph_GPIOE
#define POWER_GOOD_1_GPIO_PIN               GPIO_Pin_7
#define POWER_GOOD_1_GPIO_SPEED             GPIO_Speed_50MHz
#define POWER_GOOD_1_GPIO_MODE              GPIO_Mode_IPU
// ----------------------------------------------------------------
// BPACK: POWER_GOOD_2 (INPUT PULL_UP)
// ----------------------------------------------------------------
#define POWER_GOOD_2_GPIO_PORT              GPIOE
#define POWER_GOOD_2_GPIO_CLK               RCC_APB2Periph_GPIOE
#define POWER_GOOD_2_GPIO_PIN               GPIO_Pin_8
#define POWER_GOOD_2_GPIO_SPEED             GPIO_Speed_50MHz
#define POWER_GOOD_2_GPIO_MODE              GPIO_Mode_IPU
// ----------------------------------------------------------------
// AB&C: WCM_PWR_EN (OUT PUSH-PULL)
// Init State: LOW (OFF)
// ----------------------------------------------------------------
#define WCM_PWR_EN_GPIO_PORT                GPIOE
#define WCM_PWR_EN_GPIO_CLK                 RCC_APB2Periph_GPIOE
#define WCM_PWR_EN_GPIO_PIN                 GPIO_Pin_9
#define WCM_PWR_EN_GPIO_SPEED               GPIO_Speed_50MHz
#define WCM_PWR_EN_GPIO_MODE                GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM1: PM1uC_CTRL_RX (OUT PUSH-PULL)
// Init State: HIGH (RX DISABLED)
// ----------------------------------------------------------------
#define PM1uC_CTRL_RX_GPIO_PORT             GPIOE
#define PM1uC_CTRL_RX_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PM1uC_CTRL_RX_GPIO_PIN              GPIO_Pin_10
#define PM1uC_CTRL_RX_GPIO_SPEED            GPIO_Speed_50MHz
#define PM1uC_CTRL_RX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM1: PM1uC_CTRL_TX (OUT PUSH-PULL)
// Init State: LOW (TX DISABLED)
// ----------------------------------------------------------------
#define PM1uC_CTRL_TX_GPIO_PORT             GPIOE
#define PM1uC_CTRL_TX_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PM1uC_CTRL_TX_GPIO_PIN              GPIO_Pin_11
#define PM1uC_CTRL_TX_GPIO_SPEED            GPIO_Speed_50MHz
#define PM1uC_CTRL_TX_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM1: MB1uC_PRESENT (OUT PUSH-PULL)
// Init State: HIGH (MB PRESENT)
// ----------------------------------------------------------------
#define MB1uC_PRESENT_GPIO_PORT             GPIOE
#define MB1uC_PRESENT_GPIO_CLK              RCC_APB2Periph_GPIOE
#define MB1uC_PRESENT_GPIO_PIN              GPIO_Pin_12
#define MB1uC_PRESENT_GPIO_SPEED            GPIO_Speed_50MHz
#define MB1uC_PRESENT_GPIO_MODE             GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM1: PM1uC_SYNC (OUT PUSH-PULL)
// Init State: LOW ()
// ----------------------------------------------------------------
#define PM1uC_SYNC_GPIO_PORT                GPIOE
#define PM1uC_SYNC_GPIO_CLK                 RCC_APB2Periph_GPIOE
#define PM1uC_SYNC_GPIO_PIN                 GPIO_Pin_13
#define PM1uC_SYNC_GPIO_SPEED               GPIO_Speed_50MHz
#define PM1uC_SYNC_GPIO_MODE                GPIO_Mode_Out_PP
// ----------------------------------------------------------------
// PM1: PM1uC_PRESENT (INPUT PULL_DOWN)
// ----------------------------------------------------------------
#define PM1uC_PRESENT_GPIO_PORT             GPIOE
#define PM1uC_PRESENT_GPIO_CLK              RCC_APB2Periph_GPIOE
#define PM1uC_PRESENT_GPIO_PIN              GPIO_Pin_14
#define PM1uC_PRESENT_GPIO_SPEED            GPIO_Speed_50MHz
#define PM1uC_PRESENT_GPIO_MODE             GPIO_Mode_IPD
// ----------------------------------------------------------------
// PM1: PM1uC_nIRQ (INPUT PULL_UP)
// ----------------------------------------------------------------
#define PM1uC_nIRQ_GPIO_PORT                GPIOE
#define PM1uC_nIRQ_GPIO_CLK                 RCC_APB2Periph_GPIOE
#define PM1uC_nIRQ_GPIO_PIN                 GPIO_Pin_15
#define PM1uC_nIRQ_GPIO_SPEED               GPIO_Speed_50MHz
#define PM1uC_nIRQ_GPIO_MODE                GPIO_Mode_IPU
#define PM1uC_nIRQ_GPIO_IRQ                 EXTI15_10_IRQn          // <-- EXTI15
// -----------------------------------------------------------------------------------
// Exported Macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported Functions
// -----------------------------------------------------------------------------------
extern void MB_HW_INIT(void);
extern void MB_RCC_Configuration(void);
extern void MB_ReturnFromStopMode(void);
extern void MB_GPIO_Config(void);
extern void MB_EXTI_Config(void);
extern void DEVICE_SysTickConfig(void);
extern void ENABLE_USART1_RX_TX(void);
extern void ENABLE_USART2_RX_TX(void);
extern void ENABLE_USART3_RX_TX(void);
extern void ENABLE_UART4_RX_TX(void);
extern void ENABLE_UART5_RX_TX(void);
extern void CONFIGURE_SYSTICK(void);
// -----------------------------------------------------------------------------------
#endif	
// -----------------------------------------------------------------------------------
// __RTMCB_INITIALIZATION_H
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
