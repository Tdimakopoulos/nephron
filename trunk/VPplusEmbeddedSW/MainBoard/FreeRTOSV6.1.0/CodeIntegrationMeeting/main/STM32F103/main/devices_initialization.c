// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//!
//! \file   devices_initialization.c
//! \brief  inherited from the STM3210C-EVAL board initialization
//!
//! Routines header for initialization of some devices and variables
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Imported Variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Local Variables
// -----------------------------------------------------------------------------------
USART_TypeDef* COM_USART[COMn] = {RTMCB_COM1,RTMCB_COM2,RTMCB_COM3,RTMCB_COM4,RTMCB_COM5}; 
GPIO_TypeDef* COM_TX_PORT[COMn] = {RTMCB_COM1_TX_GPIO_PORT,RTMCB_COM2_TX_GPIO_PORT,RTMCB_COM3_TX_GPIO_PORT,RTMCB_COM4_TX_GPIO_PORT,RTMCB_COM5_TX_GPIO_PORT};
GPIO_TypeDef* COM_RX_PORT[COMn] = {RTMCB_COM1_RX_GPIO_PORT,RTMCB_COM2_RX_GPIO_PORT,RTMCB_COM3_RX_GPIO_PORT,RTMCB_COM4_RX_GPIO_PORT,RTMCB_COM5_RX_GPIO_PORT};
const uint32_t COM_CLK[COMn] = {RTMCB_COM1_CLK,RTMCB_COM2_CLK,RTMCB_COM3_CLK,RTMCB_COM4_CLK,RTMCB_COM5_CLK};
const uint32_t COM_TX_PORT_CLK[COMn] = {RTMCB_COM1_TX_GPIO_CLK,RTMCB_COM2_TX_GPIO_CLK,RTMCB_COM3_TX_GPIO_CLK,RTMCB_COM4_TX_GPIO_CLK,RTMCB_COM5_TX_GPIO_CLK};
const uint32_t COM_RX_PORT_CLK[COMn] = {RTMCB_COM1_RX_GPIO_CLK,RTMCB_COM2_RX_GPIO_CLK,RTMCB_COM3_RX_GPIO_CLK,RTMCB_COM4_RX_GPIO_CLK,RTMCB_COM5_RX_GPIO_CLK};
const uint16_t COM_TX_PIN[COMn] = {RTMCB_COM1_TX_PIN,RTMCB_COM2_TX_PIN,RTMCB_COM3_TX_PIN,RTMCB_COM4_TX_PIN,RTMCB_COM5_TX_PIN};
const uint16_t COM_RX_PIN[COMn] = {RTMCB_COM1_RX_PIN,RTMCB_COM2_RX_PIN,RTMCB_COM3_RX_PIN,RTMCB_COM4_RX_PIN,RTMCB_COM5_RX_PIN};
// -----------------------------------------------------------------------------------
//! \brief  STM32F_MCOInit
//!
//! RTMCB STM32F_MCOInit, configure GPIO_MCO pin
//!
//! \param[in]      mone
//!
//! \return         none
// -----------------------------------------------------------------------------------
void STM32F_MCOInit()
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  // Enable the GPIO_MCO Clock 
  RCC_APB2PeriphClockCmd(MCO_GPIO_CLK, ENABLE);

  // Configure the GPIO_MCO pin 
  GPIO_InitStructure.GPIO_Pin = MCO_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(MCO_GPIO_PORT, &GPIO_InitStructure);  
}
// -----------------------------------------------------------------------------------
//! \brief  STM32F_GPIOInit 
//!
//! STM32F_GPIOInit: Initializes a GPIO
//!
//! \param[in]       _port 
//! \param[in]       _clk
//! \param[in]       _pin
//! \param[in]       _speed
//! \param[in]       _mode
//!
//! \return         none
// -----------------------------------------------------------------------------------
void STM32F_GPIOInit(GPIO_TypeDef* _port, uint32_t _clk, uint16_t _pin, GPIOSpeed_TypeDef _speed, GPIOMode_TypeDef _mode)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  
  // Enable the GPIO_LED Clock 
  RCC_APB2PeriphClockCmd(_clk, ENABLE);

  // Configure the GPIO_LED pin 
  GPIO_InitStructure.GPIO_Pin = _pin;
  GPIO_InitStructure.GPIO_Mode = _mode; 
  GPIO_InitStructure.GPIO_Speed = _speed;

  GPIO_Init(_port, &GPIO_InitStructure);
}
// -----------------------------------------------------------------------------------
//! \brief  STM32F_GPIOOn
//!
//! STM32F_GPIOOn, sets GPIO 
//!
//! \param[in]      _port
//! \param[in]      _pin
//!
//! \return         none
// -----------------------------------------------------------------------------------
void STM32F_GPIOOn(GPIO_TypeDef* _port, uint16_t _pin)
{
  _port->BSRR = _pin; 
}
// -----------------------------------------------------------------------------------
//! \brief  STM32F_GPIOOff
//!
//! STM32F_GPIOOff, resets GPIO 
//!
//! \param[in]      _port
//! \param[in]      _pin
//!
//! \return         none
// -----------------------------------------------------------------------------------
void STM32F_GPIOOff(GPIO_TypeDef* _port, uint16_t _pin)
{
  _port->BRR = _pin;
}
// -----------------------------------------------------------------------------------
//! \brief  STM32F_GPIOToggle
//!
//! STM32F_GPIOToggle, toggles GPIO 
//!
//! \param[in]      _port
//! \param[in]      _pin
//!
//! \return         none
// -----------------------------------------------------------------------------------
void STM32F_GPIOToggle(GPIO_TypeDef* _port, uint16_t _pin)
{
  _port->ODR ^= _pin;

}

// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USARTs_EnableINT
//!
//! RTMCB_USARTs_EnableINT, Enable Interrupt
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USARTs_EnableINT(void){
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
    USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
    USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USART1_Init
//!
//! RTMCB_USART1_Init, initialize serial port
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USART1_Init(void)
{
 USART_InitTypeDef USART_InitStructure;

  // USART resources configuration (Clock, GPIO pins and USART registers) ----
  // USART configured as follow:
  //      - BaudRate = 115200 baud  
  //      - Word Length = 8 Bits
  //      - One Stop Bit
  //      - No parity
  //      - Hardware flow control disabled (RTS and CTS signals)
  //      - Receive and transmit enabled
  
  //USART_InitStructure.USART_BaudRate = ((uint32_t) (115200)); // 115K2 Baud for PC
  // 62500 (8.68uS)
  USART_InitStructure.USART_BaudRate = ((uint32_t) (62500));    // 115K2 Baud for PC

  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  STM32F_COMInit(COM1, &USART_InitStructure);  
  USARTx_IRQ_Config(RTMCB_COM1_IRQn, PreemptivePriority_MAINT_USB_USART1, SubPriority_MAINT_USB_USART1);
  USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
  //USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USART2_Init
//!
//! RTMCB_USART2_Init, initialize serial port
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USART2_Init(void)
{
 USART_InitTypeDef USART_InitStructure;

  // USART resources configuration (Clock, GPIO pins and USART registers) ----
  // USART configured as follow:
  //      - BaudRate = 115200 baud  
  //      - Word Length = 8 Bits
  //      - One Stop Bit
  //      - No parity
  //      - Hardware flow control disabled (RTS and CTS signals)
  //      - Receive and transmit enabled
  
  //USART_InitStructure.USART_BaudRate = ((uint32_t) (115200)); // 115K2 Baud for PC
  // 62500 (8.68uS)
  USART_InitStructure.USART_BaudRate = ((uint32_t) (62500));    // 115K2 Baud for PC

  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  #ifdef USART2_HWFLOWCTRL
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_RTS_CTS;
#else
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
#endif
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  STM32F_COMInit(COM2, &USART_InitStructure);  
  USARTx_IRQ_Config(RTMCB_COM2_IRQn, PreemptivePriority_CB_USART2, SubPriority_CB_USART2);
  USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
  //USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USART3_Init
//!
//! RTMCB_USART3_Init, initialize serial port
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USART3_Init(void)
{
 USART_InitTypeDef USART_InitStructure;

  // USART resources configuration (Clock, GPIO pins and USART registers) ----
  // USART configured as follow:
  //      - BaudRate = 115200 baud  
  //      - Word Length = 8 Bits
  //      - One Stop Bit
  //      - No parity
  //      - Hardware flow control disabled (RTS and CTS signals)
  //      - Receive and transmit enabled
  
  //USART_InitStructure.USART_BaudRate = ((uint32_t) (115200)); // 115K2 Baud for PC
  // 62500 (8.68uS)
  USART_InitStructure.USART_BaudRate = ((uint32_t) (62500));    // 115K2 Baud for PC
  
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  STM32F_COMInit(COM3, &USART_InitStructure);  
  USARTx_IRQ_Config(RTMCB_COM3_IRQn, PreemptivePriority_PM1_USART3, SubPriority_PM1_USART3);
  USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
  //USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USART4_Init
//!
//! RTMCB_USART4_Init, initialize serial port
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USART4_Init(void)
{
 USART_InitTypeDef USART_InitStructure;

  // USART resources configuration (Clock, GPIO pins and USART registers) ----
  // USART configured as follow:
  //      - BaudRate = 115200 baud  
  //      - Word Length = 8 Bits
  //      - One Stop Bit
  //      - No parity
  //      - Hardware flow control disabled (RTS and CTS signals)
  //      - Receive and transmit enabled
  
  //USART_InitStructure.USART_BaudRate = ((uint32_t) (115200)); // 115K2 Baud for PC
  // 62500 (8.68uS)
  USART_InitStructure.USART_BaudRate = ((uint32_t) (62500));    // 115K2 Baud for PC

  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  STM32F_COMInit((uint8_t)COM4, &USART_InitStructure);  

#ifdef STM32F_UART4_DMA
  USART_DMACmd(UART4, USART_DMAReq_Rx, ENABLE);
#else 
  USARTx_IRQ_Config(RTMCB_COM4_IRQn, PreemptivePriority_RTMCB_UART4, SubPriority_RTMCB_UART4);
  USART_ITConfig(UART4, USART_IT_RXNE, DISABLE);
  //USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
#endif
}
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_USART5_Init
//!
//! RTMCB_USART5_Init, initialize serial port
//! 
//! Inputs : 
//!          none
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void RTMCB_USART5_Init(void)
{
 USART_InitTypeDef USART_InitStructure;

  // USART resources configuration (Clock, GPIO pins and USART registers) ----
  // USART configured as follow:
  //      - BaudRate = 115200 baud  
  //      - Word Length = 8 Bits
  //      - One Stop Bit
  //      - No parity
  //      - Hardware flow control disabled (RTS and CTS signals)
  //      - Receive and transmit enabled
  
  //USART_InitStructure.USART_BaudRate = ((uint32_t) (115200)); // 115K2 Baud for PC
  // 62500 (8.68uS)
  USART_InitStructure.USART_BaudRate = ((uint32_t) (62500));    // 115K2 Baud for PC

  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  STM32F_COMInit((uint8_t)COM5, &USART_InitStructure);  
  USARTx_IRQ_Config(RTMCB_COM5_IRQn, PreemptivePriority_PM2_UART5, SubPriority_PM2_UART5);
  USART_ITConfig(UART5, USART_IT_RXNE, DISABLE);
  //USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  USART_RX_INT_ENABLE
//!
//! USART_RX_INT_ENABLE, enables RX INT, CLEANS ERRORS
//! 
//! Inputs : 
//!          USARTx_
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void USART_RX_INT_ENABLE (USART_TypeDef* USARTx) {
    USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
    if(USART_GetITStatus(USARTx, USART_IT_RXNE)==SET) {
        USART_ClearITPendingBit(USARTx, USART_IT_RXNE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_PE) == SET) USART_ClearFlag(USARTx, USART_FLAG_PE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_FE) == SET) USART_ClearFlag(USARTx, USART_FLAG_FE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_NE) == SET) USART_ClearFlag(USARTx, USART_FLAG_NE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_ORE) == SET) USART_ClearFlag(USARTx, USART_FLAG_ORE);
            USART_ReceiveData(USARTx);
    }
}
// -----------------------------------------------------------------------------------
//! \brief  USART_RX_INT_DISABLE
//!
//! USART_RX_INT_DISABLE, disables RX INT, CLEANS ERRORS
//! 
//! Inputs : 
//!          USARTx_
//! 
//! Output : none
// -----------------------------------------------------------------------------------
void USART_RX_INT_DISABLE (USART_TypeDef* USARTx) {
    if(USART_GetITStatus(USARTx, USART_IT_RXNE)==SET) {
        USART_ClearITPendingBit(USARTx, USART_IT_RXNE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_PE) == SET) USART_ClearFlag(USARTx, USART_FLAG_PE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_FE) == SET) USART_ClearFlag(USARTx, USART_FLAG_FE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_NE) == SET) USART_ClearFlag(USARTx, USART_FLAG_NE);
            if (USART_GetFlagStatus(USARTx, USART_FLAG_ORE) == SET) USART_ClearFlag(USARTx, USART_FLAG_ORE);
            USART_ReceiveData(USARTx);
    }
    USART_ITConfig(USARTx, USART_IT_RXNE, DISABLE);
}
// -----------------------------------------------------------------------------------
//! \brief  USART_READ_CHECKERRORS
//!
//! USART_READ_CHECKERRORS
//! 
//! Inputs : 
//!          USARTx_
//! 
//! Output : none
// -----------------------------------------------------------------------------------
uint16_t USART_READ_CHECKERRORS (USART_TypeDef* USARTx) {
    uint16_t usart_rxdata = 0xFFFF;
    if(USART_GetITStatus(USARTx, USART_IT_RXNE)==SET) {
        USART_ClearITPendingBit(USARTx, USART_IT_RXNE);
        if (USART_GetFlagStatus(USARTx, USART_FLAG_PE) == SET) USART_ClearFlag(USARTx, USART_FLAG_PE);
        if (USART_GetFlagStatus(USARTx, USART_FLAG_FE) == SET) USART_ClearFlag(USARTx, USART_FLAG_FE);
        if (USART_GetFlagStatus(USARTx, USART_FLAG_NE) == SET) USART_ClearFlag(USARTx, USART_FLAG_NE);
        if (USART_GetFlagStatus(USARTx, USART_FLAG_ORE) == SET) USART_ClearFlag(USARTx, USART_FLAG_ORE);
        usart_rxdata = USART_ReceiveData(USARTx);
    }
    return usart_rxdata;
}
// -----------------------------------------------------------------------------------
// RTMCB USARTx_IRQ_Config, configures the USART IRQ
// 
// Inputs : 
//          USARTx_IRQn, IRQ number
// 
// Output : none
// -----------------------------------------------------------------------------------
void USARTx_IRQ_Config(uint8_t USARTx_IRQn, uint8_t USARTx_PreemptivePriority, uint8_t USARTx_SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USARTx_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = USARTx_PreemptivePriority; // 0x05;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = USARTx_SubPriority;               // 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

// -----------------------------------------------------------------------------------
// RTMCB USARTx_IRQ_Config, Configures COM port.
// 
// Inputs : 
//
//        COM: Specifies the COM port to be configured.
//        This parameter can be one of following parameters:    
//        COM1, COM2, COM3, COM4, COM5  
// 
//        USART_InitStruct: pointer to a USART_InitTypeDef structure that
//        contains the configuration information for the specified USART peripheral.
// 
// Output : none
// -----------------------------------------------------------------------------------
void STM32F_COMInit(COM_TypeDef COM, USART_InitTypeDef* USART_InitStruct)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable GPIO clock 
  RCC_APB2PeriphClockCmd(COM_TX_PORT_CLK[COM] | COM_RX_PORT_CLK[COM] | RCC_APB2Periph_AFIO, ENABLE);
  
  // Enable USART clock
  switch (COM){
    case COM1:
      RCC_APB2PeriphClockCmd(COM_CLK[COM], ENABLE);
    break;
    case COM2:
    case COM3:
    case COM4:
    case COM5:
      RCC_APB1PeriphClockCmd(COM_CLK[COM], ENABLE);
    break;
  }

  // ONLY FOR STM32-EVAL
#ifdef STM32F_EVALBOARD
  if(COM == COM2)
  {
    // Enable the USART2 Pins Software Remapping 
    GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
    RCC_APB1PeriphClockCmd(RTMCB_COM2_CLK, ENABLE);
  }
#endif

  // Configure USART Tx as alternate function push-pull 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Pin = COM_TX_PIN[COM];
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(COM_TX_PORT[COM], &GPIO_InitStructure);

  // Configure USART Rx as input floating 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Pin = COM_RX_PIN[COM];
  GPIO_Init(COM_RX_PORT[COM], &GPIO_InitStructure);

  // USART configuration 
  USART_Init(COM_USART[COM], USART_InitStruct);
    
  // Enable USART 
  USART_Cmd(COM_USART[COM], ENABLE);
}
// -----------------------------------------------------------------------------------
// RTMCB EXTIx_IRQ_Config, configures the EXTI IRQ
// 
// Inputs : 
//          EXTIx_IRQn, IRQ number
// 
// Output : none
// -----------------------------------------------------------------------------------
void EXTIx_IRQ_Config(uint8_t EXTIx_IRQn, uint8_t EXTIx_PreemptivePriority, uint8_t EXTIx_SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    NVIC_InitStructure.NVIC_IRQChannel = EXTIx_IRQn; 
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EXTIx_PreemptivePriority; 
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = EXTIx_SubPriority;               
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
// -----------------------------------------------------------------------------------
// RTMCB NVIC_SD_Detect, configures the EXTI4 as Interrupt trigger edge (+) & edge(-)
// 
// Inputs : 
//          EXTIx_IRQn, IRQ number
// 
// Output : none
// -----------------------------------------------------------------------------------
void NVIC_SD_Detect(void)
{  
    EXTI_InitTypeDef EXTI_InitStructure;
    EXTI_ClearITPendingBit( EXTI_Line4 );
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Line = EXTI_Line4 ;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}
// -----------------------------------------------------------------------------------
// RTMCB IOE_GPIO_Config, configures GPIO pins used by I2C1
// 
// Inputs : none
// 
// Output : none
// -----------------------------------------------------------------------------------
void IOE_GPIO_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  
  // Enable IOE_I2C and IOE_I2C_PORT & Alternate Function clocks 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_IOE_I2C, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB_IOE_I2C_PORT | RCC_APB2Periph_AFIO, ENABLE);

  // Reset IOE_I2C IP 
  RCC_APB1PeriphResetCmd(RCC_APB1Periph_IOE_I2C, ENABLE);
  
  // Release reset signal of IOE_I2C IP 
  RCC_APB1PeriphResetCmd(RCC_APB1Periph_IOE_I2C, DISABLE);
  
  // IOE_I2C SCL and SDA pins configuration 
  GPIO_InitStructure.GPIO_Pin = IOE_SCL_PIN | IOE_SDA_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
  GPIO_Init(IOE_I2C_PORT, &GPIO_InitStructure); 
}
// -----------------------------------------------------------------------------------
// RTMCB RTC_NVIC_Configuration, Configures RTC Interrupts
// 
// Inputs : none
// 
// Output : none
// -----------------------------------------------------------------------------------
void RTC_NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  
  // Configure one bit for preemption priority 
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  // Enable the RTC Interrupt 
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  // Enable the EXTI Line17 Interrupt 
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  // Enable the RTC Alarm Interrupt 
  NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  EXTI_ClearITPendingBit(EXTI_Line16 );
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Line = EXTI_Line16;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  NVIC_InitStructure.NVIC_IRQChannel = PVD_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

#if 0
  // Enable the TIM2 global Interrupt 
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
#endif
}
// -----------------------------------------------------------------------------------
// RTMCB Tamper_NVIC_Configuration, Configures Tamper Interrupt
// 
// Inputs : none
// 
// Output : none
// -----------------------------------------------------------------------------------
void Tamper_NVIC_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = TAMPER_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
// -----------------------------------------------------------------------------------
//! \brief  NVIC_IO_EXTI 
//!
//!  configures the EXTIx as INT trigger edge (+),(-) or both
//!
//! \param[IN]      EXTI_LineX, X: 1...19
//! \param[IN]      EXTI_Trigger:
//!                     EXTI_Trigger_Rising
//!                     EXTI_Trigger_Falling  
//!                     EXTI_Trigger_Rising_Falling
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void NVIC_IO_EXTI(uint32_t EXTI_Line, uint8_t EXTI_Trigger)
{  
    EXTI_InitTypeDef EXTI_InitStructure;
    EXTI_ClearITPendingBit( EXTI_Line );                  // EXTI_Line4 
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Line = EXTI_Line;             // EXTI_Line4 ;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger;       // EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}
// -----------------------------------------------------------------------------------
//! \brief  Update_IO_STATUS
//!
//! Updates the global variable that indicates the current status of the line
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
uint8_t Update_IO_STATUS (GPIO_TypeDef* _port, uint16_t _pin){
    return GPIO_ReadInputDataBit(_port, _pin);
}
// -----------------------------------------------------------------------------------
//! \brief  Generate_RTMCB_Interrupt
//!
//! Generate Interrupt for the RTMCB
//!
//! \param      manual time between negative edge (active int) and back to normal
//!
//! \return     none
// -----------------------------------------------------------------------------------
void Generate_RTMCB_Interrupt(uint32_t OFFON_delayTime){

    STM32F_GPIOOff (RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_PIN);  // EDGE (-) INTERRUPT
    DelayBySoft (OFFON_delayTime); 
    STM32F_GPIOOn (RTMCBuC_SYNC_GPIO_PORT, RTMCBuC_SYNC_GPIO_PIN);   // EDGE (+) NORMAL
}
// -----------------------------------------------------------------------------------
// DEVICE_IRQ_Config, configures the USART IRQ
// 
// Inputs : 
//          DEVICEx_IRQn, IRQ number
// 
// Output : none
// -----------------------------------------------------------------------------------
void DEVICE_IRQ_Config(uint8_t DEVICEx_IRQn, uint8_t DEVICEx_PreemptivePriority, uint8_t DEVICEx_SubPriority)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = DEVICEx_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = DEVICEx_PreemptivePriority; // 0x05;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = DEVICEx_SubPriority;               // 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
// -----------------------------------------------------------------------------------
//! \brief  TIM1_Config
//!
//! Timer 1 Configured as Counter that produces interrupt on upload 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void TIM1_Config(void){

  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  // Compute the prescaler value 
  // ???? I don't know why... if I comment this sentence it does not work!!!!
  uint16_t PrescalerValue = (uint16_t) (SYSTEM_EXT_XTAL / 1000) - 1;
  // ????
  /* Time base configuration */
  //RS422_TIM1_PERIOD = 1600= 2.5mS (<31.01.2012)
  TIM_TimeBaseStructure.TIM_Period = 3200; // 1600; 640: 10mS?
  TIM_TimeBaseStructure.TIM_Prescaler = 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

  // Prescaler configuration 
  // TIM_PrescalerConfig(TIM1, PrescalerValue, TIM_PSCReloadMode_Immediate);

  // TIM IT enable 
  TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);

  // TIM1 enable counter 
  TIM_Cmd(TIM1, ENABLE);

  // This is different for XL devices (STM32F105 is TIM1_UP_IRQn)
  DEVICE_IRQ_Config(TIM1_UP_TIM10_IRQn, PreemptivePriority_TIM1, SubPriority_TIM1);

}
// -----------------------------------------------------------------------------------
//! \brief  TIM2_Config
//!
//! Timer 2 Configured as Counter that produces interrupt on upload 
//!
//! \param      none
//! 
//! \return     none
// -----------------------------------------------------------------------------------
void TIM2_Config(void){

  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  // Compute the prescaler value 
  uint16_t PrescalerValue = (uint16_t) (SYSTEM_EXT_XTAL / 100000) - 1;

  /* Time base configuration */
  // Period 50000, Prescaler 148  = 100ms
  // Period 500 ? 1ms
  TIM_TimeBaseStructure.TIM_Period = 1000; // 1000
  // 144: 98.4mS
  TIM_TimeBaseStructure.TIM_Prescaler = 148; // IT'S THE SAME 128:88.8ms,144:98, 192:132ms, 160:110
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

  // Prescaler configuration 
  //TIM_PrescalerConfig(TIM2, PrescalerValue, TIM_PSCReloadMode_Immediate);

  // TIM IT enable 
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

  // TIM1 enable counter 
  TIM_Cmd(TIM2, ENABLE);

  DEVICE_IRQ_Config(TIM2_IRQn, PreemptivePriority_TIM2, SubPriority_TIM2);

}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
