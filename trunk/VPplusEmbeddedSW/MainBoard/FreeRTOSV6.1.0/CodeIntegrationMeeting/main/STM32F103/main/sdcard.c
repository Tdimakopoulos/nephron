// ----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   sdcard.c
//! \brief  Implements SDCARD Macro Functions
//!
//! ....
//!
//! \author  Dudnik G-S
//! \date    26.03.2010
//! \version 1.0
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Update_SDCARD_Presence
//!
//! Updates the global variable that indicates if the SDCARD is present or not
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void Update_SDCARD_Presence(){
    uint8_t sdcard_switch = 0;
    sdcard_switch = GPIO_ReadInputDataBit(SD_DETECT_GPIO_PORT, SD_DETECT_GPIO_PIN);
    if(sdcard_switch == SDCARD_INSERTED) SDCARD_PRESENT = SDCARD_INSERTED;
    else SDCARD_PRESENT = SDCARD_NO_THERE;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_libInit
//!
//! ....
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SD_libInit (){
  //eint8 
  sdlib_initialised = efs_init( &sdcard_efs, 0 ); 
}
// -----------------------------------------------------------------------------------
//! \brief  SD_CopyFiles_Block
//!
//! ....
//!
//! \param   readFileName, name of file to be used 
//! \param   writeFileName, name of file to be used 
//! \return  void
// -----------------------------------------------------------------------------------
int8_t SD_CopyFiles_Block (char* readFileName, char* writeFileName, uint8_t zumount ) {
    int8_t status = 0;
    uint16_t databytes = 0;

    if ( file_fopen( &sdcard_filerdx, &sdcard_efs.myFs , readFileName , 'r' ) != 0 ) return -1;
    if ( file_fopen( &sdcard_filewrx, &sdcard_efs.myFs , writeFileName , 'w' ) != 0 ) {
        file_fclose( &sdcard_filerdx );
        return -2;
    }

    while ( databytes = file_read (& sdcard_filerdx , STMEM_BUFFER_SIZE , STMEM_BUFFER ) ) {
        file_write (&sdcard_filewrx , databytes , STMEM_BUFFER ) ;
    }

    file_fclose( &sdcard_filerdx );
    file_fclose( &sdcard_filewrx );
    if(zumount==1) fs_umount( &sdcard_efs.myFs ) ;
    
    return status;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_OpenReadClose_Block
//!
//! ....
//!
//! \param   readFileName, name of file to be used 
//! \param   dataread, data to be read
//! \param   dataQty should be multiple of 512
//! \return  void
// -----------------------------------------------------------------------------------
void SD_OpenReadClose_Block (char* readFileName, uint8_t *dataread, uint16_t dataQty) {
    if ( file_fopen( &sdcard_filerdx, &sdcard_efs.myFs , readFileName , 'r' ) == 0 ) {
        file_read( &sdcard_filerdx, dataQty, dataread );
        file_fclose( &sdcard_filerdx );
    }
}
// -----------------------------------------------------------------------------------
//! \brief  SD_OpenWriteClose_Block
//!
//! ....
//!
//! \param   dataQty should be multiple of 512
//! \return  void
// -----------------------------------------------------------------------------------
void SD_OpenWriteClose_Block (char *writeFilename, uint8_t *datawrite, uint16_t dataQty){
    if ( file_fopen( &sdcard_filewrx, &sdcard_efs.myFs , writeFilename , 'w' ) == 0 ) { 
        file_write( &sdcard_filewrx, dataQty , datawrite );
        file_fclose( &sdcard_filewrx );
    }
}
// -----------------------------------------------------------------------------------
//! \brief  SD_OpenAppendClose_Block
//!
//! ....
//!
//! \param   dataQty should be multiple of 512
//! \return  void
// -----------------------------------------------------------------------------------
void SD_OpenAppendClose_Block (char *writeFilename, uint8_t *datawrite, uint16_t dataQty){
    if ( file_fopen( &sdcard_filewrx, &sdcard_efs.myFs , writeFilename , 'a' ) == 0 ) {  
        file_write( &sdcard_filewrx, dataQty , datawrite );
        file_fclose( &sdcard_filewrx );
    }
}
// -----------------------------------------------------------------------------------
//! \brief  SD_OpenFile
//!
//! ....
//!
//! \param   
//! \return  void
// -----------------------------------------------------------------------------------
int8_t SD_OpenFile (char *RWFilename, EmbeddedFile sdcard_fileRW, uint8_t openMode){
    int8_t sdopenst = file_fopen( &sdcard_fileRW, &sdcard_efs.myFs , RWFilename , openMode );
    return sdopenst;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_WriteFile
//!
//! ....
//!
//! \param   
//! \return  void
// -----------------------------------------------------------------------------------
uint16_t SD_WriteFile (EmbeddedFile sdcard_fileRW, uint8_t *datawrite, uint16_t dataQty){
    uint16_t bytesWritten = file_write( &sdcard_fileRW, dataQty , datawrite );
    return bytesWritten;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_ReadFile
//!
//! ....
//!
//! \param   
//! \return  void
// -----------------------------------------------------------------------------------
uint16_t SD_ReadFile (EmbeddedFile sdcard_fileRW, uint8_t *dataread, uint16_t dataQty){
      uint16_t bytesRead = file_read( &sdcard_fileRW, dataQty, dataread );
      return bytesRead;
}
// -----------------------------------------------------------------------------------
//! \brief  SD_CloseFile
//!
//! ....
//!
//! \param   
//! \return  void
// -----------------------------------------------------------------------------------
void SD_CloseFile (EmbeddedFile sdcard_fileRW){
    file_fclose( &sdcard_fileRW );
}
// -----------------------------------------------------------------------------------
//! \brief  ComposeFileName
//!
//! ....
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void ComposeFileName ( uint8_t* LogFileName,  uint32_t fnum ){
  uint8_t ix=0;
  char numaux[6];
  LogFileName[0] = 'C';
  for(ix=1;ix<NAME_LEN;ix++)LogFileName[ix]=0;                             
  for(ix=0;ix<6;ix++)numaux[ix]=0;                             
  // murcielago
  ix = decword_to_charstr(fnum, numaux, 6, 10000);
  if(ix!=0) {
      for(ix=1;ix<6;ix++)LogFileName[ix]=numaux[ix-1];                            
  }
  else{
      for(ix=1;ix<6;ix++)LogFileName[ix]='X';
  }
  LogFileName[6]='.';
  LogFileName[7]='T';
  LogFileName[8]='X';
  LogFileName[9]='T';
  LogFileName[10]='\0';
}

// -----------------------------------------------------------------------------------
//! \brief  SDCARD_WRITE_TEST (1 file 4096 byte)
//!
//! Test writing SDCARD
//! (efs_init & fs_umount are executer before and after respectively
//!
//! \param   current filescounter
//!
//! \return  updated filescounter 
// -----------------------------------------------------------------------------------
uint32_t SDCARD_WRITE_TEST(uint32_t filescounter){
  
  int16_t i=0;
  uint16_t k=0, w = 0, z=0;
  uint8_t FileNameX[12];
  uint8_t textx[16];
  uint8_t number[10];
  uint16_t sd_data_counter = 0;
  uint8_t chval = 'A';

  for (w=0;w<12;w++) FileNameX[w] = '\0';
  
      ComposeFileName ( &FileNameX[0], filescounter );
      filescounter++;
      //Delay(100);
      if ( file_fopen( &sdcard_filewrx, &sdcard_efs.myFs , FileNameX , 'a' ) == 0 ) {
      file_is_open = 1;
      sd_data_counter = 0;
      for(w=0; w<STMEM_BUFFER_SIZE;w++) STMEM_BUFFER[w] = '\0';
      for(z=0; z<NBLOCK_PER_FILE;z++){    // start for NBLOCK_PER_FILE
          // writes timestamp on file
          for(w=0;w<16;w++)textx[w]=0;
          textx[0] = '-'; textx[1] = '-';textx[2] = '>'; textx[3] = 'T';
          textx[4] = 'S'; textx[5] = 'T';textx[6] = 'A'; textx[7] = 'M';
          textx[8] = 'P'; textx[9] = ':';
          for(w=0;w<10;w++){ //1
            STMEM_BUFFER[sd_data_counter] = textx[w];
            sd_data_counter++;
          } //1
          // writes a first line
          for(w=0;w<10;w++)number[w]=0;                             
          i=UINT32_TO_CH_ASCII(globalTimestamp, &number[0], 8);                            
          for(w=0;w<8;w++){ //2
            if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
            else  STMEM_BUFFER[sd_data_counter] = 'X';
            sd_data_counter++;
          } //2
          STMEM_BUFFER[sd_data_counter] = '-';
          sd_data_counter++;       
          
          for(w=sd_data_counter;w<(STMEM_BUFFER_SIZE-2);w++){
              STMEM_BUFFER[sd_data_counter] = chval;
              sd_data_counter++; 
              chval++; if (chval>'Z') chval='A';
          }
          STMEM_BUFFER[sd_data_counter] = '\r'; sd_data_counter++;
          STMEM_BUFFER[sd_data_counter] = '\n'; sd_data_counter++;
            
          file_write( &sdcard_filewrx, STMEM_BUFFER_SIZE , STMEM_BUFFER );
          sd_data_counter = 0;
      } // end for NBLOCK_PER_FILE
    
    
      if(file_is_open ==1) {
        file_fclose( &sdcard_filewrx );
        file_is_open = 0;
      }
      //fs_umount( &sdcard_efs.myFs ) ;
    } // end file open
  return filescounter;
}
// -----------------------------------------------------------------------------------
//! \brief  SDCARD_WRITE_TEST_ACCELEROMETER
//!
//! Test writing SDCARD
//! (efs_init & fs_umount are executer before and after respectively
//!
//! \param   current filescounter
//!
//! \return  updated filescounter 
// -----------------------------------------------------------------------------------
uint32_t SDCARD_WRITE_TEST_ACCELEROMETER(uint32_t filescounter){
  
  int16_t i=0;
  uint16_t k=0, w = 0, z=0;
  uint8_t FileNameX[12];
  uint8_t textx[16];
  uint8_t number[10];
  uint16_t sd_data_counter = 0;
  uint8_t chval = 'A';
  uint8_t ACC_IDX = 0;

  for (w=0;w<12;w++) FileNameX[w] = '\0';
  
      ComposeFileName ( &FileNameX[0], filescounter );
      filescounter++;
      //Delay(100);
      if ( file_fopen( &sdcard_filewrx, &sdcard_efs.myFs , FileNameX , 'a' ) == 0 ) {
      file_is_open = 1;
      for(z=0; z<NBLOCK_PER_FILE;z++){    // start for NBLOCK_PER_FILE
          sd_data_counter = 0;
          for(w=0; w<STMEM_BUFFER_SIZE;w++) STMEM_BUFFER[w] = '\0';
          // IF UPDATING ACCELEROMETER VALUES WAIL
          if(ACCELEROMETER_UPDATING==1)while(ACCELEROMETER_UPDATING==1){};
          // WAIT UNTIL FULL DATA
          while(ACCELEROMETER_BFULL == SIGNAL_OFF){};
          for(ACC_IDX = 0; ACC_IDX<ACCELEROMETER_SIZE; ACC_IDX++){
              // writes timestamp 
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(globalTimestamp, &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = ',';
              sd_data_counter++;       
              // accelerometer timestamp
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(acc_TimeStamp[ACC_IDX][0], &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = ',';
              sd_data_counter++;       
              // accelerometer status
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(acc_status[ACC_IDX][0], &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = ',';
              sd_data_counter++;       
              // accelerometer out x
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(acc_out_x[ACC_IDX][0], &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = ',';
              sd_data_counter++;       
              // accelerometer out y
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(acc_out_y[ACC_IDX][0], &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = ',';
              sd_data_counter++; 
              // accelerometer out z
              for(w=0;w<10;w++)number[w]=0;                             
              i=UINT32_TO_CH_ASCII(acc_out_z[ACC_IDX][0], &number[0], 8);                            
              for(w=0;w<8;w++){ //2
                if(i!=-1) STMEM_BUFFER[sd_data_counter] = number[w];
                else  STMEM_BUFFER[sd_data_counter] = 'X';
                sd_data_counter++;
              } //2
              STMEM_BUFFER[sd_data_counter] = '\r'; sd_data_counter++;
              STMEM_BUFFER[sd_data_counter] = '\n'; sd_data_counter++;
          }
          // complete the buffer with <>0       
          for(w=sd_data_counter;w<STMEM_BUFFER_SIZE-2;w++){
              STMEM_BUFFER[sd_data_counter] = ' '; sd_data_counter++; 
          }         
          STMEM_BUFFER[sd_data_counter] = '\r'; sd_data_counter++;
          STMEM_BUFFER[sd_data_counter] = '\n'; sd_data_counter++;

          file_write( &sdcard_filewrx, STMEM_BUFFER_SIZE , STMEM_BUFFER );
          sd_data_counter = 0;
          ACCELEROMETER_BFULL = SIGNAL_OFF;
      } // end for NBLOCK_PER_FILE    
    
      if(file_is_open ==1) {
        file_fclose( &sdcard_filewrx );
        file_is_open = 0;
      }
    } // end file open
  return filescounter;
}
// -----------------------------------------------------------------------------------
//! \brief  SDCARD_READ_TEST (1 file 4096 byte)
//!
//! Test reading SDCARD
//! (efs_init & fs_umount are executer before and after respectively
//!
//! \param   current filescounter (file to be read)
//!
//! \return  sd_bytes_read (no of bytes read) 
// -----------------------------------------------------------------------------------
uint32_t SDCARD_READ_TEST(uint32_t filescounter){
  
  uint16_t w = 0;
  uint8_t FileNameX[12];
  uint16_t sd_data_counter = 0;
  uint16_t sd_read_counter = 0;
  euint32 sd_bytes_read = 0;

  for (w=0;w<12;w++) FileNameX[w] = '\0';
  
  ComposeFileName ( &FileNameX[0], filescounter );
  for(w=0; w<STMEM_BUFFER_SIZE;w++) STMEM_BUFFER[w] = '\0';
  if ( file_fopen( &sdcard_filerdx, &sdcard_efs.myFs , FileNameX , 'r' ) == 0 ) {
      file_is_open = 1;
      sd_bytes_read = file_read( &sdcard_filerdx, STMEM_BUFFER_SIZE , STMEM_BUFFER );
      if(file_is_open ==1) {
        file_fclose( &sdcard_filerdx );
        file_is_open = 0;
      }
  } // end file open
  sdbytesread = sd_bytes_read;
  return sd_bytes_read;
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
