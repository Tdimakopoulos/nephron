// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   data.h
//! \brief  Data Initialization and Cleanup
//!
//! Collection of data initialization routines
//!
//! \author  Dudnik G.S.
//! \date    20.07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
#ifndef DATA_H_
#define DATA_H_

#ifdef LINUX
	#include "interfaces.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\CodeFromOFFIS\\includes_nephron\\interfaces.h"
#endif

// -----------------------------------------------------------------------------------
// SIZE OF BUFFERS
// RTMCB COM BUFFERS
#define TXRX_MAXVALUE                           128
#define TXRX_RS422_RTMCB_MAXVALUE               384 // 488 // 512 // (512+128) // 1024
#define RX_DMA_RS422_RTMCB_TOTAL                384 // 488 // 256
#define TXRX_RS422_USB_MAXVALUE                 384 // >= TXRX_RS422_RTMCB_MAXVALUE

// CB COM BUFFERS
#define SLIP_BLEN                               1024 // [TBD]
// SDCARD TEST
#define STMEM_BUFFER_SIZE                       2048 
// SPI-UIF
#define SPI2_RXTX_LEN                           10
// -----------------------------------------------------------------------------------
// WAKD GLOBAL INFO
// -----------------------------------------------------------------------------------
#define WAKD_READY_TO_WORK_SET          0x00000001
#define WAKD_READY_TO_WORK_RST          0xFFFFFFFE
// -----------------------------------------------------------------------------------
// To say RMCB IS READY
// -----------------------------------------------------------------------------------
#define RTMCB_MAGICNO                           0x89ABCDEF
#define RTMCB_AGAIN_NOTREADY_TIME               20 // 5'' (50ms x 100times) (it was 20 = 1'')
// MB <--> RTMCB
#define DEVICE_NOTREADY                         0x00
#define DEVICE_READY                            0x01
#define INTERVAL                                0x02
#define COMMAND                                 0x03
#define NONE                                    0x00
// SCHEDULER CONSTANTS
#define SCHEDULER_NONE                          0x00
#define SCHEDULER_STARTUP                       0x01
// -----------------------------------------------------------------------------------
// HARDWARE FAULT (GDU.29.03.2012)
// -----------------------------------------------------------------------------------
typedef struct
{
  __IO uint32_t HFSR_REG0;
} HFSR_TypeDef;

#define HFSR_BASE                       ((uint32_t)0xE000ED2C) /*!< Hardware Fault Base Address */
#define HFSR                            ((HFSR_TypeDef *) HFSR_BASE)

#define  HFSR_CLEAN_ALL                 ((uint32_t)0xC0000002)            /*!< B31, B30, B01 (OR) */
// This bit is set if there is a fault related to debug.
// This is only possible when halting debug is not enabled. For monitor enabled debug, it only
// happens for BKPT when the current priority is higher than the monitor. When both halting and
// monitor debug are disabled, it only happens for debug events that are not ignored (minimally,
// BKPT). The Debug Fault Status Register is updated.
#define  HFSR_SET_DEBUGEVT              ((uint32_t)0x80000000)            /*!< B31 OR */
#define  HFSR_RST_DEBUGEVT              ((uint32_t)0x7FFFFFFF)            /*!< B31 AND */
// Hard Fault activated because a Configurable Fault was received and cannot activate because
// of priority or because the Configurable Fault is disabled.
// The Hard Fault handler then has to read the other fault status registers to determine cause.
#define  HFSR_SET_FORCED                ((uint32_t)0x40000000)            /*!< B30 OR */
#define  HFSR_RST_FORCED                ((uint32_t)0xBFFFFFFF)            /*!< B30 AND */
// This bit is set if there is a fault because of vector table read on exception processing (Bus Fault). 
// This case is always a Hard Fault. The return PC points to the pre-empted instruction.
#define  HFSR_SET_VECTTBL               ((uint32_t)0x00000002)            /*!< B01 OR */
#define  HFSR_RST_VECTTBL               ((uint32_t)0xFFFFFFFD)            /*!< B01 AND */
// -----------------------------------------------------------------------------------
// CONFIGURABLE HW FAULTS
// -----------------------------------------------------------------------------------
// MEMORY MANAGEMENT FAULT (GDU.29.03.2012)
// -----------------------------------------------------------------------------------
typedef struct
{
  __IO uint8_t MMFSR_REG0;
} MMFSR_TypeDef;

#define MMFSR_BASE                      ((uint32_t)0xE000ED28) /*!< Memory Management Fault Base Address */
#define MMFSR                           ((MMFSR_TypeDef *) MMFSR_BASE)

#define MMFSR_CLEAN_ALL                 ((uint8_t)0x9B)                   /*!< B7, B4, B3, b1, b0 (OR) */

// 1 = valid fault address in MMAR. 
// If a MemManage fault occurs that is escalated to a Hard Fault because of priority, the Hard Fault handler must clear this bit. 
// This prevents problems on return to a stacked active MemManage handler whose MMAR value has been overwritten.
#define  MMFSR_SET_MMARVALID            ((uint8_t)0x80)                   /*!< B7 OR */
#define  MMFSR_RST_MMARVALID            ((uint8_t)0x7F)                   /*!< B7 AND */
// Stacking from exception has caused one or more access violations. 
// The SP is still adjusted and the values in the context area on the stack might be incorrect.
#define  MMFSR_SET_MSTKERR              ((uint8_t)0x10)                   /*!< B4 OR */
#define  MMFSR_RST_MSTKERR              ((uint8_t)0xEF)                   /*!< B4 AND */
// Unstack from exception return has caused one or more access violations. 
// This is chained to the handler, so that the original return stack is still present. 
// SP is not adjusted from failing return and new save is not performed.
#define  MMFSR_SET_MUNSTKERR            ((uint8_t)0x08)                   /*!< B3 OR */
#define  MMFSR_RST_MUNSTKERR            ((uint8_t)0xF7)                   /*!< B3 AND */
// Data access violation flag. 
// Attempting to load or store at a location that does not permit the operation sets the DACCVIOL flag. 
// The return PC points to the faulting instruction. This error loads MMAR with the address of the attempted access.
#define  MMFSR_SET_DACCVIOL            ((uint8_t)0x02)                   /*!< B1 OR */
#define  MMFSR_RST_DACCVIOL            ((uint8_t)0xFD)                   /*!< B1 AND */
// Instruction access violation flag. 
// Attempting to fetch an instruction from a location that does not permit execution sets the IACCVIOL flag. 
// This occurs on any access to an XN region, even when the MPU is disabled or not present. 
// The return PC points to the faulting instruction. The MMAR is not written.
#define  MMFSR_SET_IACCVIOL            ((uint8_t)0x01)                   /*!< B0 OR */
#define  MMFSR_RST_IACCVIOL            ((uint8_t)0xFE)                   /*!< B0 AND */
// -----------------------------------------------------------------------------------
// BUS FAULT (GDU.29.03.2012)
// -----------------------------------------------------------------------------------
typedef struct
{
  __IO uint8_t BFSR_REG0;
} BFSR_TypeDef;

#define BFSR_BASE                     ((uint32_t)0xE000ED29) /*!< Bus Fault Base Address */
#define BFSR                          ((BFSR_TypeDef *) BFSR_BASE)

#define BFSR_CLEAN_ALL                ((uint8_t)0x9F)                   /*!< B7, B4, B3, B2, b1, b0 (OR) */

// This bit is set if the Bus Fault Address Register (BFAR) contains a valid address. 
// This is true after a bus fault where the address is known. 
// Other faults can clear this bit, such as a Mem Manage fault occurring later.
#define  BFSR_SET_BFARVALID           ((uint8_t)0x80)                   /*!< B7 OR */
#define  BFSR_RST_BFARVALID           ((uint8_t)0x7F)                   /*!< B7 AND */
// Stacking from exception has caused one or more bus faults. 
// The SP is still adjusted and the values in the context area on the stack might be incorrect.
#define  BFSR_SET_STKERR              ((uint8_t)0x10)                   /*!< B4 OR */
#define  BFSR_RST_STKERR              ((uint8_t)0xEF)                   /*!< B4 AND */
// Unstack from exception return has caused one or more bus faults. 
// This is chained to the handler, so that the original return stack is still present. 
// SP is not adjusted from failing return and new save is not performed.
#define  BFSR_SET_UNSTKERR            ((uint8_t)0x08)                   /*!< B3 OR */
#define  BFSR_RST_UNSTKERR            ((uint8_t)0xF7)                   /*!< B3 AND */
// Imprecise data bus error
#define  BFSR_SET_IMPRECISERR         ((uint8_t)0x04)                   /*!< B2 OR */
#define  BFSR_RST_IMPRECISERR         ((uint8_t)0xFB)                   /*!< B2 AND */
// Precise data bus error return
#define  BFSR_SET_PRECISERR           ((uint8_t)0x02)                   /*!< B1 OR */
#define  BFSR_RST_PRECISERR           ((uint8_t)0xFD)                   /*!< B1 AND */
// Instruction bus error flag
#define  BFSR_SET_IBUSERR             ((uint8_t)0x01)                   /*!< B0 OR */
#define  BFSR_RST_IBUSSERR            ((uint8_t)0xFE)                   /*!< B0 AND */
// -----------------------------------------------------------------------------------
// USAGE FAULT (GDU.29.03.2012)
// -----------------------------------------------------------------------------------
typedef struct
{
  __IO uint16_t UFSR_REG0;
} UFSR_TypeDef;

#define UFSR_BASE                     ((uint32_t)0xE000ED2B) /*!< Usage Fault Base Address */
#define UFSR                          ((UFSR_TypeDef *) UFSR_BASE)

#define UFSR_CLEAN_ALL                 ((uint16_t)0x030F)               /*!< B9, B8, B3, B2, b1, b0 (OR) */
// 
#define  UFSR_SET_DIVBYZERO           ((uint16_t)0x0200)               /*!< B9 OR */
#define  UFSR_RST_DIVBYZERO           ((uint16_t)0xFDFF)               /*!< B9 AND */
//
#define  UFSR_SET_UNALIGNED           ((uint16_t)0x0100)               /*!< B8 OR */
#define  UFSR_RST_UNALIGNED           ((uint16_t)0xFEFF)               /*!< B8 AND */

#define  UFSR_SET_NOCP                ((uint16_t)0x0008)               /*!< B3 OR */
#define  UFSR_RST_NOCP                ((uint16_t)0xFFF7)               /*!< B3 AND */

#define  UFSR_SET_INVPC               ((uint16_t)0x0004)               /*!< B2 OR */
#define  UFSR_RST_INVPC               ((uint16_t)0xFFFB)               /*!< B2 AND */

#define  UFSR_SET_INVSTATE            ((uint16_t)0x0002)               /*!< B1 OR */
#define  UFSR_RST_INVSTATE            ((uint16_t)0xFFFD)               /*!< B1 AND */

#define  UFSR_SET_UNDEFINSTR          ((uint16_t)0x0001)               /*!< B0 OR */
#define  UFSR_RST_UNDEFINSTR          ((uint16_t)0xFFFE)               /*!< B0 AND */

// -----------------------------------------------------------------------------------
// System handler control and state register (SCB_SHCSR) (GDU.29.03.2012)
// -----------------------------------------------------------------------------------
typedef struct
{
  __IO uint32_t SCB_SHCSR0;
} SCB_SHCSR_TypeDef;

#define SCB_SHCSR_BASE                ((uint32_t)0xE000ED24) /*!<  System handler control and state register Base Address */
#define SCB_SHCSR                     ((SCB_SHCSR_TypeDef *) SCB_SHCSR_BASE)

// BUS FAULT HANDLER ENABLE
#define  SCB_SHCSR_SET_BUSFAULTENA    ((uint32_t)0x00020000)            /*!< B17 OR */
#define  SCB_SHCSR_RST_BUSFAULTENA    ((uint32_t)0xFFFDFFFF)            /*!< B17 AND */
// BUS FAULT ACTIVE
#define  SCB_SHCSR_SET_BUSFAULTACT    ((uint32_t)0x00000002)            /*!< B01 OR */
#define  SCB_SHCSR_RST_BUSFAULTACT    ((uint32_t)0xFFFFFFFD)            /*!< B01 AND */
// BUS EXCEPTIONS: BUSFAULTPENDED(B14), BUSFAULTACT(B01)
#define  SCB_SHCSR_RST_BUS_EXCEPTIONS ((uint32_t)0xFFFFBFFD)            /*!< B01 & B14 AND */
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// RS485
// -----------------------------------------------------------------------------------
#define RS485SCHEDULETOUT   10
#define RS485TIMEOUT        5
// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
// RTMCB SENDS DATA?
// -----------------------------------------------------------------------------------
#define STREAMING_ON                    1
#define STREAMING_OFF                   0
// -----------------------------------------------------------------------------------
// RTMCB WORKING MODE
// -----------------------------------------------------------------------------------
#define SIMULATION_MODE                 1
#define REALTIME_MODE                   0
// -----------------------------------------------------------------------------------
// MICROFLUIDIC CIRCUIT MODES
// -----------------------------------------------------------------------------------
#define MICROFLUIDIC_STOP               1
#define MICROFLUIDIC_DIALYSIS           2
#define MICROFLUIDIC_REGENERATION_1     3
#define MICROFLUIDIC_ULTRAFILTRATION    4
#define MICROFLUIDIC_REGENERATION_2     5
#define MICROFLUIDIC_MAINTENANCE        6

#define MICROFLUIDIC_CODE_MIN           1
#define MICROFLUIDIC_CODE_MAX           6
// -----------------------------------------------------------------------------------
// WAKD ATTITUDE 
// -----------------------------------------------------------------------------------
#define WAKD_ATTITUDE_OK                1
#define WAKD_ATTITUDE_KO                2
#define WAKD_ATTITUDE_NOINFO            3
// -----------------------------------------------------------------------------------
// SENSORS STATUS
// -----------------------------------------------------------------------------------
#define SENSOR_OK                       1
#define SENSOR_KO                       2
#define SENSOR_NOINFO                   3
// -----------------------------------------------------------------------------------
// DEVICES STATUS
// -----------------------------------------------------------------------------------
#define DEVICE_OK                       1
#define DEVICE_KO                       2
#define DEVICE_NOINFO                   3
// -----------------------------------------------------------------------------------
// ACTIVE COMMUNICATION LINKS [ON FIRST TX/RX] OFF BY TIMEOUT
// -----------------------------------------------------------------------------------
#define CLINKS_NONE                     1
#define CLINKS_BLUETOOTH                2
#define CLINKS_USB                      3
#define CLINKS_BOTH                     4
// -----------------------------------------------------------------------------------
// DEVICES
// -----------------------------------------------------------------------------------
// UARTS
#define COM_1                           1
#define COM_2                           2
#define COM_3                           3  
#define COM_4                           4
#define COM_5                           5

// ECPS
#define ECP_NO                          0
#define ECP1                            1
#define ECP2                            2

// DIRECTION-POLARITY
#define SENS_DIRECT                     1     // *LEFT-WAY1
#define SENS_REVERSE                    2     // *RIGHT-WAY2
#define SENS_TOGGLE                     3     // TESTING PURPOSES

// ON-OFF
#define SIGNAL_ON                       1
#define SIGNAL_OFF                      0

// PUMP SETPOINT
#define SPEED_MIN                       0
#define SPEED_MAX                       3000  // rpm
#define SPEED_FACTOR                    10


// PUMP SETPOINT as % of PWM(MAX)
#define _PWM_CODE_RLEFT_MIN             1
#define _PWM_CODE_RLEFT_MAX             8
#define _PWM_CODE_STOPPED_MIN           9
#define _PWM_CODE_STOPPED               13
#define _PWM_CODE_STOPPED_MAX           17
#define _PWM_CODE_RRIGHT_MIN            18
#define _PWM_CODE_RRIGHT_MAX            25

// Pump Speed Control by Code    
#define _SPEED_CODE_1000L               1
#define _SPEED_CODE_0915L               2
#define _SPEED_CODE_0830L               3
#define _SPEED_CODE_0745L               4
#define _SPEED_CODE_0660L               5
#define _SPEED_CODE_0575L               6
#define _SPEED_CODE_0490L               7
#define _SPEED_CODE_0405L               8
#define _SPEED_CODE_0320L               9
#define _SPEED_CODE_0235L               10
#define _SPEED_CODE_0150L               11
#define _SPEED_CODE_0065L               12
#define _SPEED_CODE_STOP                13
#define _SPEED_CODE_0065R               14
#define _SPEED_CODE_0150R               15
#define _SPEED_CODE_0235R               16
#define _SPEED_CODE_0320R               17
#define _SPEED_CODE_0405R               18
#define _SPEED_CODE_0490R               19
#define _SPEED_CODE_0575R               20
#define _SPEED_CODE_0660R               21
#define _SPEED_CODE_0745R               22
#define _SPEED_CODE_0830R               23
#define _SPEED_CODE_0915R               24 
#define _SPEED_CODE_1000R               25

// PUMP CURRENT
#define CURRENT_MIN                     0
#define CURRENT_MAX                     500   // mA
#define CURRENT_FACTOR                  10
// PUMP FLOW
#define FLOW_MIN                        0     // 0.01 x ml/min
#define FLOW_MAX                        20000 // 0.01 x ml/min
#define FLOW_FACTOR                     100

// POLARIZER SETPOINT
#define VOLTAGE_MIN                     0
#define VOLTAGE_MAX                     4500  // mV 0.001 x V
#define VOLTAGE_FACTOR                  10

// POLARIZER DCDC
#define DCDC_STATUS_MASK                0x0000FFFF
#define DCDC_POWER_GOOD_LIMIT           0x07FC      // TYPICAL: 0x0FF8 (LIMIT=MID-RANGE)
#define DCDC_POWER_GOOD_OK              0x00010000  
#define DCDC_POWER_GOOD_KO              0xFFFEFFFF

// ECP RANGES
// The device shall maintain blood sodium levels within normal range (135 - 146 mmol/L)
#define SODIUM_MIN                      120     // mmol/l
#define SODIUM_MAX                      160     // mmol/l
#define SODIUM_FACTOR                   5
// Blood levels of potassium shall be maintained within normal range (3.5 - 5.0 mmol/L)
#define POTASSIUM_MIN                   300     // 0.01 x mmol/L
#define POTASSIUM_MAX                   550     // 0.01 x mmol/L
#define POTASSIUM_FACTOR                50
// The device shall keep blood phosphate levels as low as possible, but not lower than 0.8 mmol/L
#define PHOSPHATE_MIN                   80      // 0.01 x mmol/L
#define PHOSPHATE_MAX                   200     // 0.01 x mmol/L (WHO KNOWS?)
#define PHOSPHATE_FACTOR                10
// The device shall keep blood acidity within range (pH 7.30-7.50)
#define PH_MIN                          700     // 0.01 x
#define PH_MAX                          800     // 0.01 x
#define PH_FACTOR                       10
// BUN = Blood Urea Nitrogen
// Patients on dialysis have higher BUN levels, usually 40-60 mg/dL. 
#define UREA_MIN                        20      // mg/dl
#define UREA_MAX                        80      // mg/dl
#define UREA_FACTOR                     5
// ECP TEMPERATURE RANGE
#define ECPTEMP_MIN                     100     // 0.1 x C
#define ECPTEMP_MAX                     500     // 0.1 x C
#define ECPTEMP_FACTOR                  10

// CONDUCTIVIY
#define COND_G_MIN                      0     // 0.0001 x
#define COND_G_MAX                      100   // 0.0001 x
#define COND_B_MIN                      0     // 0.0001 x
#define COND_B_MAX                      100   // 0.0001 x
#define COND_FACTOR                     1

// TEMPERATURE
#define TEMPERATURE_MIN                 300  // 0.1 x
#define TEMPERATURE_MAX                 450  // 0.1 x
#define TEMPERATURE_FACTOR              10

// PRESSURE
#define PRESSURE_MIN                    0 // -600  // mmHG
#define PRESSURE_MAX                    800   // mmHG
#define PRESSURE_FACTOR                 10

// RS422 CTRL LINES
#define RTMCBuC_nIRQ_NO                 6       // from 6 to avoid overlapping rs422-if numbering
#define RTMCBuC_PRESENT_NO              7
#define SAFETY_BABD_SELFTEST_NO         8
#define SAFETY_DABD_SELFTEST_NO         9

#define RTMCB_INTERRUPTED               1
#define RTMCB_NOINTERRUPT               0
// -----------------------------------------------------------------------------------
// SDCARD
// -----------------------------------------------------------------------------------
// BEFORE JAP CHANGE
// #define SDCARD_INSERTED              1
// #define SDCARD_NO_THERE              0
// AFTER JAP CHANGE DUE TO HW PROBLEM (20.12.2011)
#define SDCARD_INSERTED                 0
#define SDCARD_NO_THERE                 1



//#define STMEM_BUFFER_SIZE               2048 // 4096    // 512   // It should be 2048 
                                                // Formatted for SD and Streaming buffer Size
#define NAME_LEN                        11

#define SD_WR_SECONDS                   100     // 60 
#define SD_MAXLINES_FILE                100

#define NBLOCK_PER_FILE                 1

#define FILESTOWRITE                    250// 50  // TESTING
// -----------------------------------------------------------------------------------
// RS422 DEVICE CODIFICATION (CTRL)
// -----------------------------------------------------------------------------------
#define RS422CTRL_BTS                   1
#define RS422CTRL_BPSI                  2
#define RS422CTRL_BPSO                  3
#define RS422CTRL_FPSI                  4
#define RS422CTRL_FPSO                  5
#define RS422CTRL_DCS                   6
#define RS422CTRL_BLD                   7
#define RS422CTRL_FLD                   8
#define RS422CTRL_BABD                  9

#define RS422CTRL_ECP1                  10
#define RS422CTRL_ECP2                  11
#define RS422CTRL_MFSI                  12
#define RS422CTRL_MFSO                  13
#define RS422CTRL_MFSBL                 14
#define RS422CTRL_BLPUMP                15
#define RS422CTRL_FLPUMP                16
#define RS422CTRL_POLAR                 17
#define RS422CTRL_3VALVES               18
#define RS422CTRL_VITCS                 19  // VITAMIN C SENSOR
#define RS422CTRL_VITCI                 20  // VITAMIN C INJECTOR
#define RS422CTRL_BLPPSC                21  // BLPUMP SPEED CODE
#define RS422CTRL_FLPPSC                22  // FLPUMP SPEED CODE

// -----------------------------------------------------------------------------------
// DATA ACQUISITION
// -----------------------------------------------------------------------------------
// bit  SCANCYCLE_FLAGS
// b0:   [reserved]
// b1:   MFSI
// b2:   MFSO       
// b3:   MFSBL      
// b4:   BLPUMP                 
// b5:   FLPUMP          
// b6:   BPSI         
// b7:   BPSO    
// b8:   FPSI          
// b9:   FPSO           
// b10:  BTS                    
// b11:  DCS          
// b12:  BLD       
// b13:  FLD   
// b14:  ECP1   
// b15:  ECP2          
// ..
// ..
// b31: SCAN COMPLETED

// SET AFTER SUCCESSFUL WRITING
#define OK_MFSI       0x00000002
#define OK_MFSO       0x00000004
#define OK_MFSU       0x00000008
#define OK_BLPUMP     0x00000010  
#define OK_FLPUMP     0x00000020  
#define OK_BPSI       0x00000040
#define OK_BPSO       0x00000080
#define OK_FPSI       0x00000100
#define OK_FPSO       0x00000200
#define OK_BTS        0x00000400
#define OK_DCS        0x00000800
#define OK_BLD        0x00001000
#define OK_FLD        0x00002000
#define OK_ECP1       0x00004000
#define OK_ECP2       0x00008000
#define OKII_BLPUMP   0x00010000  
#define OKII_FLPUMP   0x00020000  

// RESET AFTER SUCCESSFUL READING
#define RD_MFSI       0xFFFFFFFD
#define RD_MFSO       0xFFFFFFFB
#define RD_MFSU       0xFFFFFFF7
#define RD_BLPUMP     0xFFFFFFEF
#define RD_FLPUMP     0xFFFFFFDF
#define RD_BPSI       0xFFFFFFBF
#define RD_BPSO       0xFFFFFF7F
#define RD_FPSI       0xFFFFFEFF
#define RD_FPSO       0xFFFFFDFF
#define RD_BTS        0xFFFFFBFF
#define RD_DCS        0xFFFFF7FF
#define RD_BLD        0xFFFFEFFF
#define RD_FLD        0xFFFFDFFF
#define RD_ECP1       0xFFFFBFFF
#define RD_ECP2       0xFFFF7FFF
#define RDII_BLPUMP   0xFFFEFFFF
#define RDII_FLPUMP   0xFFFDFFFF

// ALARMS STATUS
// b0 = BABD
// b1 = BLD
// b2 = DABD
// b3 = FLD
#define BABD_ALARM_ON   0x00000001
#define BLD_ALARM_ON    0x00000002
#define DABD_ALARM_ON   0x00000004
#define FLD_ALARM_ON    0x00000008
#define BABD_ALARM_OFF  0xFFFFFFFE
#define BLD_ALARM_OFF   0xFFFFFFFD
#define DABD_ALARM_OFF  0xFFFFFFFB
#define FLD_ALARM_OFF   0xFFFFFFF7

#define ALARMS_CLEAR    0xFFFFFFF0

#define BABD_CHANGED    1
#define BLD_CHANGED     2
#define DABD_CHANGED    3
#define FLD_CHANGED     4
// -----------------------------------------------------------------------------------
// CONTROL: SCHEDULER
// -----------------------------------------------------------------------------------
// SCHEDULE TO EXECUTE
#define WR_BLPUMP_ONOFF     0x0001
#define WR_FLPUMP_ONOFF     0x0002
#define WR_BLPUMP_SETPOINT  0x0004
#define WR_FLPUMP_SETPOINT  0x0008

#define WR_MFSI_POSITION    0x0010
#define WR_MFSO_POSITION    0x0020
#define WR_MFSU_POSITION    0x0040

#define WR_MFSI_ONOFF       0x0100
#define WR_MFSO_ONOFF       0x0200
#define WR_MFSU_ONOFF       0x0400

#define WR_POLAR_ONOFF      0x1000
#define WR_POLAR_SETPOINT   0x2000

// CLEAR AFTER EXECUTED
#define RD_BLPUMP_ONOFF     0xFFFE
#define RD_FLPUMP_ONOFF     0xFFFD 
#define RD_BLPUMP_SETPOINT  0xFFFB
#define RD_FLPUMP_SETPOINT  0xFFF7
 
#define RD_MFSI_POSITION    0xFFEF
#define RD_MFSO_POSITION    0xFFDF
#define RD_MFSU_POSITION    0xFFBF

#define RD_MFSI_ONOFF       0xFEFF
#define RD_MFSO_ONOFF       0xFDFF
#define RD_MFSU_ONOFF       0xFBFF

#define RD_POLAR_ONOFF      0xEFFF
#define RD_POLAR_SETPOINT   0xDFFF

// -----------------------------------
// DIRECTION-POLARITY [SETTINGS]
// -----------------------------------
#define VALVESX3_ON         0x01000000
#define VALVESX3_OFF        0xFEFFFFFF
#define VALVE_MFSI_WAY1     0x00000001
#define VALVE_MFSI_WAY2     0x00000002
#define VALVE_MFSO_WAY1     0x00000100
#define VALVE_MFSO_WAY2     0x00000200
#define VALVE_MFSU_WAY1     0x00010000
#define VALVE_MFSU_WAY2     0x00020000
#define VALVES_MFSX_ALL     0xFF000000
#define VALVE_3X_MFSI_W1    0x01
#define VALVE_3X_MFSI_W2    0x02
#define VALVE_3X_MFSO_W1    0x04
#define VALVE_3X_MFSO_W2    0x08
#define VALVE_3X_MFSU_W1    0x10
#define VALVE_3X_MFSU_W2    0x20

// -----------------------------------------------------------------------------------
// AUXILIAR BATTERY (see stm32f107-lqfp100-RTMCuC_FWRES_v0r2 up)
// -----------------------------------------------------------------------------------
// Vn[V]  Nn    Meaning         Nmin    Nmax
// 1.11   1401	PRECHARGING	>1375	<1450
// 1.88   2378	FAST CHARGING	>2300	<2500
// 1.36   1723	CHARGE DONE	>1675	<1825
// 2.75   3481	CHARGE SUSPEND	>3350	<3650
// 3.24   4095	VRTCuC/NO VBAT	>3800
// -----------------------------------------------------------------------------------
#define   VBATRT_PRE_CHARGING     0x10000000
#define   VBATRT_FAST_CHARGING    0x20000000
#define   VBATRT_CHARGE_DONE      0x40000000
#define   VBATRT_CHARGE_SUSPEND   0x80000000
#define   VBATRT_NO_SUPPLY        0xF0000000
#define   VBATRT_CLEAR_STATUS     0x0000FFFF

#define   VBATRT_PRECHARGE_NMIN   1375
#define   VBATRT_PRECHARGE_NMAX   1450
#define   VBATRT_FSTCHARGE_NMIN   2300
#define   VBATRT_FSTCHARGE_NMAX   2500
#define   VBATRT_CHRGEDONE_NMIN   1675
#define   VBATRT_CHRGEDONE_NMAX   1825
#define   VBATRT_CHRGESUSP_NMIN   3350
#define   VBATRT_CHRGESUSP_NMAX   3650
#define   VBATRT_NO_SUPPLY_NMIN   3800

// -----------------------------------------------------------------------------------
// MB UIF Protocol Command                  CODES	Rate(1/f) [s]
// -----------------------------------------------------------------------------------
// UIF_ACC_SPI
#define UIF_TXRX_LEN                        10
#define UIF_ACC_BYTES                       7
#define UIF_COM_TRIALS                      100

// STATE DIAGRAM
// COMMON-A
#define UIF_STATE_START                     0
#define UIF_STATE_TXCMD                     1
// INFO
#define UIF_STATE_CHKTX                     2
// COMMON-B
#define UIF_STATE_ACKNK                     3
#define UIF_STATE_DUMMY                     4
// CMDS
#define UIF_STATE_RXD2                      15
#define UIF_STATE_RXD1                      16
#define UIF_STATE_RXD0                      17
#define UIF_STATE_RXACKNK                   18
#define UIF_STATE_TXACKNK                   19

#define MB_UIF_CMD_VBAT1_INFO	            0x20	// 60
#define MB_UIF_CMD_VBAT2_INFO	            0x21	// 60
#define MB_UIF_CMD_WAKD_STATUS	            0x23	// 60
#define MB_UIF_CMD_WAKD_ATTITUDE	    0x24	// 5
#define MB_UIF_CMD_WAKD_COMMLINK	    0x25	// 60
#define MB_UIF_CMD_WAKD_OPMODE	            0x26	// 60
#define MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS    0x27	// 60
#define MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS    0x28	// 60
#define MB_UIF_CMD_WAKD_BLPUMP_INFO	    0x2A	// 60
#define MB_UIF_CMD_WAKD_FLPUMP_INFO	    0x2B	// 60
#define MB_UIF_CMD_WAKD_BLTEMPERATURE	    0x2C	// 1
#define MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE  0x2D	// 1
#define MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE  0x2E	// 1
#define MB_UIF_CMD_WAKD_FLCONDUCTIVITY	    0x2F	// 1

#define MB_UIF_CMD_WAKD_HFD_INFO	    0x30	// 60
#define MB_UIF_CMD_WAKD_SU_INFO	            0x31	// 60
#define MB_UIF_CMD_WAKD_POLAR_INFO	    0x32	// 60
#define MB_UIF_CMD_WAKD_ECPS_INFO	    0x33	// 60
#define MB_UIF_CMD_WAKD_PS_INFO             0x34	// 1
#define MB_UIF_CMD_WAKD_ACT_INFO            0x35        // 1
#define MB_UIF_CMD_WAKD_ACK_COMMANDS        0x36        // 1

#define MB_UIF_CMD_PDATA_INITIALS	    0x40	// (once, atb)
#define MB_UIF_CMD_PDATA_PATIENTCODE	    0x41	// (once, atb)





#define MB_UIF_CMD_PDATA_GENDERAGE	    0x42	// (once, atb)
#define MB_UIF_CMD_PDATA_WEIGHT	            0x43	// once, when measured



#define MB_UIF_CMD_PDATA_SEWALL	            0x47	// when measured, 5
#define MB_UIF_CMD_PDATA_BLPRESSURE	    0x48	// once, when measured
#define MB_UIF_CMD_PDATA_ECP1_A             0x49
#define MB_UIF_CMD_PDATA_ECP1_B             0x4A
#define MB_UIF_CMD_PDATA_ECP2_A             0x4B
#define MB_UIF_CMD_PDATA_ECP2_B             0x4C

#define MB_UIF_CMD_ALARMS	            0x50	// 1
#define MB_UIF_CMD_ERRORS	            0x51	// 1
#define MB_UIF_CMD_TEST_LINK	            0x52	// (once, atb)

#define MB_UIF_CMD_WAKD_SHUTDOWN	    0x60	// 1
#define MB_UIF_CMD_WAKD_START_OPERATION	    0x61	// 1
#define MB_UIF_CMD_WAKD_MODE_OPERATION	    0x62	// 1
#define MB_UIF_CMD_SCALE_GET_WEIGHT	    0x63	// 1
#define MB_UIF_CMD_SEW_START_STREAMING	    0x64	// 1
#define MB_UIF_CMD_SEW_STOP_STREAMING	    0x65	// 1
#define MB_UIF_CMD_NIBP_START_MEASUREMENT   0x66	// 1		

#define MB_UIF_CMD_ACKNOWLEDGE	            0x77	
#define MB_UIF_CMD_NOT_ACKNOWLEDGE	    0x99	
#define MB_UIF_CMD_FORBIDDEN	            0xFF	
#define MB_UIF_CMD_END                      0x88
#define MB_UIF_CMD_TEST1	            0xAB	
#define MB_UIF_CMD_TEST2	            0xCD	
#define MB_UIF_CMD_CLR	                    0x00	

#define MB_UIF_QTY_CMD_SET                  6    
#define MB_UIF_QTY_CMD_GET                  2   

// QUANTITY OF BYTES TO BE RECEIVED
#define MB_UIF_QTY_VBAT1_INFO	            8     //  1
#define MB_UIF_QTY_VBAT2_INFO	            8     //  2
#define MB_UIF_QTY_VBATS_INFO	            12    //  3 
#define MB_UIF_QTY_WAKD_STATUS	            8     //  4
#define MB_UIF_QTY_WAKD_ATTITUDE	    6     //  5
#define MB_UIF_QTY_WAKD_COMMLINK	    6     //  6
#define MB_UIF_QTY_WAKD_OPMODE	            6     //  7
#define MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS    8     //  8
#define MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS    8     //  9
#define MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS    12    // 10
#define MB_UIF_QTY_WAKD_BLPUMP_INFO	    8     // 11
#define MB_UIF_QTY_WAKD_FLPUMP_INFO	    8     // 12
#define MB_UIF_QTY_WAKD_BLTEMPERATURE	    10    // 13
#define MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE  8     // 14
#define MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE  8     // 15
#define MB_UIF_QTY_WAKD_FLCONDUCTIVITY	    10    // 16
#define MB_UIF_QTY_WAKD_HFD_INFO	    6     // 17
#define MB_UIF_QTY_WAKD_SU_INFO	            6     // 18
#define MB_UIF_QTY_WAKD_POLAR_INFO	    8     // 19
#define MB_UIF_QTY_WAKD_ECP1_INFO	    6     // 20
#define MB_UIF_QTY_WAKD_ECP2_INFO	    6     // 21
#define MB_UIF_QTY_WAKD_ECPS_INFO	    8     // 22
#define MB_UIF_QTY_PDATA_FIRSTNAME	    18    // 23  
#define MB_UIF_QTY_PDATA_LASTNAME	    18    // 24 
#define MB_UIF_QTY_PDATA_GENDERAGE	    6     // 25
#define MB_UIF_QTY_PDATA_WEIGHT	            6     // 26
#define MB_UIF_QTY_PDATA_HEARTRATE	    5     // 27
#define MB_UIF_QTY_PDATA_BREATHRATE	    5     // 28
#define MB_UIF_QTY_PDATA_ACTIVITY	    5     // 29
#define MB_UIF_QTY_PDATA_SEWALL	            7     // 30
#define MB_UIF_QTY_PDATA_BLPRESSURE	    8     // 31 
#define MB_UIF_QTY_PDATA_ECP1	            12    // 32
#define MB_UIF_QTY_PDATA_ECP2	            12    // 33
#define MB_UIF_QTY_ALARMS	            8     // 34
#define MB_UIF_QTY_ERRORS	            8     // 35
//#define MB_UIF_QTY_TEST_LINK	            6     // 36
#define MB_UIF_QTY_WAKD_SHUTDOWN	    4     //
#define MB_UIF_QTY_WAKD_START_OPERATION	    4     //
#define MB_UIF_QTY_WAKD_MODE_OPERATION	    4     //
#define MB_UIF_QTY_SCALE_GET_WEIGHT	    4     //
#define MB_UIF_QTY_SEW_START_STREAMING	    4     //
#define MB_UIF_QTY_SEW_STOP_STREAMING	    4     //
#define MB_UIF_QTY_NIBP_START_MEASUREMENT   4     //
#define MB_UIF_QTY_ANSWER_0                 0     // 0 == ACK/NACK
#define MB_UIF_QTY_ANSWER_1                 5
#define MB_UIF_QTY_ANSWER_2                 3

#define CMDS 1
#define OPMS 2
#define MSGS 3

#define MB_UIF_SCHNO_VBAT1_INFO                   0
#define MB_UIF_SCHNO_VBAT2_INFO                  20
#define MB_UIF_SCHNO_WAKD_STATUS                 40
#define MB_UIF_SCHNO_WAKD_ATTITUDE               60
#define MB_UIF_SCHNO_WAKD_COMMLINK              100
#define MB_UIF_SCHNO_WAKD_OPMODE                120
#define MB_UIF_SCHNO_WAKD_BLCIRCUIT_STATUS      140
#define MB_UIF_SCHNO_WAKD_FLCIRCUIT_STATUS      160
#define MB_UIF_SCHNO_WAKD_BLPUMP_INFO           200
#define MB_UIF_SCHNO_WAKD_FLPUMP_INFO           220
#define MB_UIF_SCHNO_WAKD_BLTEMPERATURE         240
#define MB_UIF_SCHNO_WAKD_BLCIRCUIT_PRESSURE    260
#define MB_UIF_SCHNO_WAKD_FLCIRCUIT_PRESSURE    280
#define MB_UIF_SCHNO_WAKD_FLCONDUCTIVITY        300
#define MB_UIF_SCHNO_WAKD_HFD_INFO              320
#define MB_UIF_SCHNO_WAKD_SU_INFO               340
#define MB_UIF_SCHNO_WAKD_POLAR_INFO            360
#define MB_UIF_SCHNO_WAKD_ECP1_INFO             380
#define MB_UIF_SCHNO_WAKD_ECP2_INFO             400
#define MB_UIF_SCHNO_ALARMS                     440
#define MB_UIF_SCHNO_ERRORS                     460
#define MB_UIF_SCHNO_TEST_LINK                  480
#define MB_UIF_SCHNO_PDATA_FIRSTNAME            500
#define MB_UIF_SCHNO_PDATA_LASTNAME             520
#define MB_UIF_SCHNO_PDATA_GENDERAGE            540
#define MB_UIF_SCHNO_PDATA_WEIGHT               560
#define MB_UIF_SCHNO_PDATA_HEARTRATE            580
#define MB_UIF_SCHNO_PDATA_BREATHRATE           600
#define MB_UIF_SCHNO_PDATA_ACTIVITY             620
#define MB_UIF_SCHNO_PDATA_SEWALL               640
#define MB_UIF_SCHNO_PDATA_BLPRESSURE           660
#define MB_UIF_SCHNO_PDATA_ECP1_A               680
#define MB_UIF_SCHNO_PDATA_ECP1_B               700
#define MB_UIF_SCHNO_PDATA_ECP2_A               720
#define MB_UIF_SCHNO_PDATA_ECP2_B               740
#define MB_UIF_SCHNO_WAKD_SHUTDOWN              760
#define MB_UIF_SCHNO_WAKD_START_OPERATION       780
#define MB_UIF_SCHNO_WAKD_MODE_OPERATION        800
#define MB_UIF_SCHNO_SCALE_GET_WEIGHT           820
#define MB_UIF_SCHNO_SEW_START_STREAMING        840
#define MB_UIF_SCHNO_SEW_STOP_STREAMING         860
#define MB_UIF_SCHNO_NIBP_START_MEASUREMENT     880

#define MB_UIF_SCHNO_WAKD_GET_COMMANDS          800

#define MB_ACC_SCHNO_READ_XYZ_2                 840

// COMMANDS ASSIGMENT  (in a 16-bit word for MB) -------------------------------------
#define MB_COMMAND_SHUTDOWN                     0x0001
#define MB_COMMAND_START_OPERATION              0x0002
#define MB_COMMAND_CHANGE_OPMODE                0x0004
#define MB_COMMAND_GET_WEIGHT                   0x0008
#define MB_COMMAND_START_SEWSTREAM              0x0010
#define MB_COMMAND_STOP_SEWSTREAM               0x0020
#define MB_COMMAND_START_NIBP                   0x0040

// Patient Activity
#define ACTIVITY_STEADY                         0
#define ACTIVITY_LAYING                         1
#define ACTIVITY_WALKING                        2
#define ACTIVITY_RUNNING                        3
#define ACTIVITY_UNKNOWN                        4
// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
// ----------------------------------------
// MB STATUS (INCL. BATTERY)
// ----------------------------------------
struct MBINFO{
	uint32_t Status[2];
	uint32_t ONnOFFDevices[2];
	uint32_t TimeStamp[2];
};
struct MBINFO MBInformation;
// ----------------------------------------
// RTMCB STATUS (INCL. BATTERY)
// ----------------------------------------
struct RTMCBINFO{
    uint32_t Status[2];
    uint32_t ONnOFFDevices[2];
    uint32_t TimeStamp[2];
};
struct RTMCBINFO RTMCBInformation;
// ----------------------------------------
// ALARMS
// ----------------------------------------
struct Alarms{
    uint32_t Status[2];
    uint32_t TimeStamp[2];
};
struct Alarms AlarmsData;
// -----------------------------------------------------------------------------------
// PHYSICAL SENSORS [PS]
// -----------------------------------------------------------------------------------
// PRESSURE SENSOR
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct PressureSensorSettings{
    uint32_t Configuration[2][2];
    uint32_t Calibration[2][2];
    uint32_t SamplingPeriod[2];
} ;
// FPSI
struct PressureSensorSettings PressureCfgFCInlet;
// FPSO
struct PressureSensorSettings PressureCfgFCOutlet;
// BPSI
struct PressureSensorSettings PressureCfgBCInlet;
// BPSO
struct PressureSensorSettings PressureCfgBCOutlet;
// ----------------------------------------
// DATA
// ----------------------------------------
struct PressureSensorData{
    int16_t Pressure_Value[2];
    uint32_t Pressure_TimeStamp[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// FPSI
struct PressureSensorData PressureFCInlet;
// FPSO
struct PressureSensorData PressureFCOutlet;
// BPSI
struct PressureSensorData PressureBCInlet;
// BPSO
struct PressureSensorData PressureBCOutlet;

// -----------------------------------------------------------------------------------
// TEMPERATURE SENSOR
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct TemperatureSensorSettings{
    uint32_t Configuration[2][2];
    uint32_t Calibration[2][2];
    uint32_t SamplingPeriod[2];
    uint8_t SampleMeanNb[2];
} ;
// BTS
struct TemperatureSensorSettings TemperatureCfgFCInOut;
// ----------------------------------------
// DATA
// ----------------------------------------
struct TemperatureSensorData{
    uint16_t TemperatureInlet_Value[2];
    uint16_t TemperatureOutlet_Value[2];
    uint32_t Temperature_TimeStamp[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// BTS
struct TemperatureSensorData TemperatureInOut;
// -----------------------------------------------------------------------------------
// CONDUCTIVITY SENSOR
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct ConductivitySensorSettings{
    uint32_t Settings[2][2];
    uint32_t SamplingPeriod[2];
    uint32_t ISinusFrequency[2];
} ;
// DCS
struct ConductivitySensorSettings CSENS1Settings;
// ----------------------------------------
// DATA
// ----------------------------------------
struct ConductivitySensorData{
    uint16_t Cond_FCR[2];
    uint16_t Cond_FCQ[2];
    uint16_t Cond_PT1000[2];
    uint32_t Cond_TimeStamp[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// DCS
struct ConductivitySensorData CSENS1Data;
// -----------------------------------------------------------------------------------
// lEAKAGE DETECTOR
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct LeakageDetectorSettings{
    uint32_t Settings[2][2];
    uint32_t SamplingPeriod[2];
} ;
// BLD
struct LeakageDetectorSettings BLDSettings;
// FLD
struct LeakageDetectorSettings FLDSettings;
// ----------------------------------------
// DATA
// ----------------------------------------
struct LeakageDetectorData{
    uint32_t TimeStamp[2];
    uint16_t Value[2];
    uint32_t Status[2];
} ;
// BLD
struct LeakageDetectorData BLDData;
// FLD
struct LeakageDetectorData FLDData;
// -----------------------------------------------------------------------------------
// ACTUATORS [ACT]
// -----------------------------------------------------------------------------------
// MULTISWITCH
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct MultiSwitchSettings{
    uint32_t Settings[2][2];
} ;
// MFSBI
struct MultiSwitchSettings MultiSwitchISettings;
// MFSBO
struct MultiSwitchSettings MultiSwitchOSettings;
// MFSBL
struct MultiSwitchSettings MultiSwitchBLSettings;
// ----------------------------------------
// DATA
// ----------------------------------------
struct MultiSwitchData{
    uint8_t SwitchONnOFF[2];
    uint8_t Position[2];
    uint32_t TimeStamp_Position[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// MFSBI
struct MultiSwitchData MultiSwitchIData;
// MFSBO
struct MultiSwitchData MultiSwitchOData;
// MFSBL
struct MultiSwitchData MultiSwitchBLData;
// -----------------------------------------------------------------------------------
// PUMPS
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct PUMPSettings{
    uint32_t Settings[2][2];
} ;
// BLPUMP
struct PUMPSettings BLPumpSettings;
// FLPUMP
struct PUMPSettings FLPumpSettings;
// ----------------------------------------
// DATA
// ----------------------------------------
struct PUMPData{
    uint8_t SwitchONnOFF[2];
    uint8_t Direction[2];
    uint32_t TimeStamp_Direction[2];
    uint16_t SpeedReference[2];
    uint32_t TimeStamp_SpeedReference[2];
    uint16_t Speed[2];
    uint32_t TimeStamp_Speed[2];
    uint16_t Flow[2];
    uint32_t TimeStamp_Flow[2];
    uint16_t Current[2];
    uint32_t TimeStamp_Current[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// BLPUMP
struct PUMPData BLPumpData;
// FLPUMP
struct PUMPData FLPumpData;
// ----------------------------------------
// FLOW DATA (DERIVATED FROM PUMP OR OTHER
// ----------------------------------------
struct FlowData{
    uint16_t Value[2];
    uint32_t TimeStamp[2];
} ;
// BFLOW
struct FlowData BFlowData;
// FFLOW
struct FlowData FFlowData;
// -----------------------------------------------------------------------------------
// POLARIZER
// -----------------------------------------------------------------------------------
// ----------------------------------------
// DATA
// ----------------------------------------
struct POLARDataX{
    uint8_t SwitchONnOFF[2];
    uint8_t Direction[2];
    uint32_t TimeStamp_Direction[2];
    uint16_t VoltageReference[2];
    uint32_t TimeStamp_VoltageReference[2];
    uint16_t Voltage[2];
    uint32_t TimeStamp_Voltage[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// POLAR
struct POLARDataX POLARData;

// Status:
// b15...b0: POWER GOOD ADC VALUE
// b16 = 1 power good ok; b16 = 0 power good bad.
// -----------------------------------------------------------------------------------
// ELECTROCHEMICAL PLATFORMS [ECP]
// -----------------------------------------------------------------------------------
// SETTINGS
// ----------------------------------------
struct ECPSettings{
    uint32_t Settings[2];
    uint8_t TypeCalib[2];
    uint8_t ChannelNo[2];
    uint32_t AutoCalibration[5][2];
    uint32_t Calibration_Sodium[2];
    uint32_t Calibration_Potassium[2];
    uint32_t Calibration_Phosphate[2];
    uint32_t Calibration_pH[2];
    uint32_t Calibration_Urea[2];
    uint32_t Calibration_ECPTemperature[2];
} ;

// ECP1
struct ECPSettings ECP1Settings;
// ECP2
struct ECPSettings ECP2Settings;
// ----------------------------------------
// DATA
// ----------------------------------------
struct ECPData{
    uint8_t SwitchONnOFF[2];
    uint8_t TypeData[2];
    uint8_t Channel[2];
    uint8_t QtyofDataToRead[2];
    uint32_t Values_TimeStamp[2];
    uint16_t Values_Sodium[2];
    uint16_t Values_Potassium[2];
    uint16_t Values_Phosphate[2];
    uint16_t Values_pH[2];
    uint16_t Values_Urea[2];
    uint16_t Values_ECPTemperature[2];
    uint32_t TimeStamp_VAL_Sodium[2];
    uint32_t TimeStamp_VAL_Potassium[2];
    uint32_t TimeStamp_VAL_Phosphate[2];
    uint32_t TimeStamp_VAL_pH[2];
    uint32_t TimeStamp_VAL_Urea[2];
    uint32_t TimeStamp_VAL_ECPTemperature[2];
    uint32_t Status[2];
    uint32_t TimeStamp_Status[2];
} ;
// ECP1
struct ECPData ECP1Data;
// ECP2
struct ECPData ECP2Data;
// ----------------------------------------
// CHANNELS SCAN INTERVAL
// ----------------------------------------
#if 0
struct ChannelsScanInterval{
    uint16_t Sodium_FCI[2];       // 60000
    uint16_t Potassium_FCI[2];    // 60000
    uint16_t Phosphate_FCI[2];    // 60000
    uint16_t pH_FCI[2];           // 60000
    uint16_t Urea_FCI[2];         // 60000
    uint16_t ECPTemp_FCI[2];      // 60000
    uint16_t ECP1_Status[2];      // 60000
    uint16_t Sodium_FCO[2];       // 60000
    uint16_t Potassium_FCO[2];    // 60000
    uint16_t Phosphate_FCO[2];    // 60000
    uint16_t pH_FCO[2];           // 60000
    uint16_t Urea_FCO[2];         // 60000
    uint16_t ECPTemp_FCO[2];      // 60000
    uint16_t ECP2_Status[2];      // 60000
    uint16_t Pressure_BCI[2];     // 1000
    uint16_t Pressure_BCO[2];     // 1000
    uint16_t Pressure_FCI[2];     // 1000
    uint16_t Pressure_FCO[2];     // 1000
    uint16_t Temperature_BCI[2];  // 1000
    uint16_t Temperature_BCO[2];  // 1000
    uint16_t Conductivity_FCR[2]; // 1000
    uint16_t Conductivity_FCQ[2]; // 1000
    uint16_t Conductivity_T[2];   // 1000
    uint16_t Leakage_BC[2];       // 1000
    uint16_t Leakage_FC[2];       // 1000
    uint16_t Flow_Calc_BC[2];     // 1000
    uint16_t Flow_Calc_FC[2];     // 1000
    uint16_t PumpSpeed_BC[2];     // 1000
    uint16_t PumpCurrent_BC[2];   // 1000
    uint16_t PumpSpeed_FC[2];     // 1000
    uint16_t PumpCurrent_FC[2];   // 1000
    uint16_t Position_MFSI[2];    // 1000
    uint16_t Position_MFSO[2];    // 1000
    uint16_t Position_MFSBL[2];   // 1000
    uint16_t BPSI_Status[2];      // 1000
    uint16_t BPSO_Status[2];      // 1000
    uint16_t FPSI_Status[2];      // 1000
    uint16_t FPSO_Status[2];      // 1000
    uint16_t BTS_Status[2];       // 1000
    uint16_t BLD_Status[2];       // 1000
    uint16_t FLD_Status[2];       // 1000
    uint16_t BLPump_Status[2];    // 1000
    uint16_t FLPump_Status[2];    // 1000
    uint16_t MFSI_Status[2];      // 1000
    uint16_t MFSO_Status[2];      // 1000
    uint16_t MFSBL_Status[2];     // 1000
    uint16_t DCS_Status[2];       // 1000
    uint16_t Voltage_POLAR[2];    // 1000
    uint16_t Direction_POLAR[2];  // 1000
    uint16_t POLAR_Status[2];     // 1000
    uint16_t ALARMS_Status[2];    // 50
    uint16_t RTMCB_Status[2];     // 10000
};
struct ChannelsScanInterval ChannelsScanIntervals;
#endif
// ----------------------------------------
// DATE TIME
// ----------------------------------------
// ----------------------------------------
// DATE-TIME Structure definition 
// ----------------------------------------
// Time Structure definition 
// ----------------------------------------
struct Date_g
{
  uint8_t Month;
  uint8_t Day;
  uint16_t Year;
};

struct Time_g
{
  uint8_t Seconds;
  uint8_t Minutes;
  uint8_t Hours;
};


struct DateTime{
  struct Date_g Date;
  struct Time_g Time;
};

struct DateTime current_DateTime;

// ----------------------------------------
// NETEDEVS
// ----------------------------------------
#define DEVICESQTY                      15
#define PS_DEVSQTY                      8
#define ACT_DEVSQTY                     5

#define DEVICES_BPSI                    0
#define DEVICES_BPSO                    1
#define DEVICES_FPSI                    2
#define DEVICES_FPSO                    3
#define DEVICES_BTS                     4
#define DEVICES_DCS                     5
#define DEVICES_BLD                     6
#define DEVICES_FLD                     7
#define DEVICES_ECP1                    8
#define DEVICES_ECP2                    9
#define DEVICES_BLPUMP                  10
#define DEVICES_FLPUMP                  11
#define DEVICES_MFSBI                   12 
#define DEVICES_MFSBO                   13
#define DEVICES_MFSBL                   14

struct DEVICEINFO{
    uint32_t Status[2];
    uint32_t TimestampTimer[2];
    struct DateTime DateTimeX[2];
};

struct NETDEVSINFORMATION {
    struct DEVICEINFO InfoX[DEVICESQTY][2];
    uint32_t InfoUpdate[4][2];
};
struct NETDEVSINFORMATION NetDevsInfo;

struct NETDEVSINFORMATION NetDevsInfo;
// ----------------------------------------
// RTMCB SYSTEM INITIALIZED / PC ACK
// ----------------------------------------
#define LED_TOGGLE_PERIOD   1000
#define RS422_RTMCB_TX_CNT  16     
#define USART_TX_PERIOD     500
#define UIF_ACC_TX_PERIOD   1000    // of full scheduler
#define UIF_ACC_TX_TIME     20
#define UIF_ACC_DATA_SIMUL  500

// ----------------------------------------
// RTMCB TASKS
// ----------------------------------------

// After UART buffer "in" is complete, ISR informs Task CBPA which
// calls the function decodeMsg() (by CSEM). decodeMsg() interprets
// the "in" buffer and copies the values to the following structures.
// From there CBPA will read and process them.
#ifdef HBUILD
  extern tdDevicesStatus          staticBufferDevicesStatus;
#else
  extern tdDevicesStatus          staticBufferDevicesStatusData;
#endif
extern tdAlarms			staticBufferAlarms;
extern tdPhysiologicalData 	staticBufferPhysiologicalData;
extern tdPhysicalsensorData	staticBufferPhysicalsensorData;
extern tdActuatorData		staticBufferActuatorData;
extern tdWakdStates		staticBufferStateRTB;
extern tdWakdOperationalState	staticBufferOperationalStateRTB;

#ifndef VP_SIMULATION
	// flag for RTMCB TASK
	extern tdDataId RTMCB_message;

	extern uint8_t MB_SYSTEM_READY;
	extern uint8_t RTMCB_SimulationNoRT;
	extern uint8_t RTMCB_StreamingYesNo;
	extern uint32_t RTMCB_channelsFlag_01;
	extern uint32_t RTMCB_channelsFlag_02;
	extern uint8_t LINE_RTMCBuC_nIRQ;
	extern uint8_t STATUS_RTMCBuC_nIRQ;
        // IF 1 RESET PROCEDURE WILL EXECUTE IN RTOS SYSTICK
        extern uint8_t RTMCB_RESET_CMD;                    
        
        extern __IO uint32_t TimingDelay;
        extern uint32_t globalTimestamp;
        extern uint16_t led_toggle_counter;
        
        extern uint16_t uif_acc_data_counter;
        extern uint16_t uif_acc_cmd_counter;
        // ADC
        extern uint16_t ADCDMACounter;
        extern uint16_t ADC_FeedBack_ReadValue[4];
        extern uint16_t ADC_FeedBack_EchoValue[4];

	// ----------------------------------------
	// SD-CARD
	// ----------------------------------------
	extern uint8_t SDCARD_PRESENT;
        extern uint8_t SDCARD_HISTORIC;
	extern EmbeddedFileSystem sdcard_efs;
	extern EmbeddedFile sdcard_filerdx, sdcard_filerd1, sdcard_filerd2;
	extern EmbeddedFile sdcard_filewrx, sdcard_filewr1, sdcard_filewr2;
	extern DirList sdcard_dirlist;
	extern int8_t sdlib_initialised;
	extern uint8_t STMEM_BUFFER[STMEM_BUFFER_SIZE];
	extern eint32  sectorCount; // added gdu
	extern uint32_t sdfilecounter;
	extern uint8_t file_is_open;
	extern esint8 res;
	extern esint32 sdbytesread;
	extern uint8_t LOCK_CRITICAL_INTS;
	extern uint16_t a,abc;  // MFS a in SVN, abc in DEMO042
	// ----------------------------------------
	// SERIAL PORT TEST
	// ----------------------------------------
	extern uint8_t zrx_out_str[TXRX_MAXVALUE];
	extern uint8_t ztx_out_str[TXRX_MAXVALUE];
	extern uint16_t OUT_STR_TX_LEN;
	extern uint16_t OUT_STR_RX_LEN;
	extern uint16_t out_reclen;
	extern uint8_t OUT_COMM_FLAG;

        // ----------------------------------------
	// RS422-RTMCB
	// ----------------------------------------
	extern uint8_t RTMCB_STATUS;
	extern uint8_t QUERY_INFO;
	extern uint8_t RTMCB_DMA_RX_NEW;

	extern uint16_t out_rtmcb_reclen_0;
	extern uint16_t out_rtmcb_reclen_1;
	extern uint8_t INP_COMM_RTMCB_FLAG;
	extern uint8_t OUT_COMM_RTMCB_FLAG;
	extern uint8_t zrx_out_rtmcb_str_1[TXRX_RS422_RTMCB_MAXVALUE];
	extern uint8_t zrx_out_rtmcb_str_0[TXRX_RS422_RTMCB_MAXVALUE];
	// maximum necessary in fact 256 instead of 488
	extern uint16_t UART4_RX_DMA_RTMCB[RX_DMA_RS422_RTMCB_TOTAL];
	extern uint8_t zrx_out_rtmcb_str_x[RX_DMA_RS422_RTMCB_TOTAL];
	extern uint16_t  rs422_tx_rtmcb_counter;
	extern uint8_t BufferXYZ_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
	extern uint8_t RAW_Buffer_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
	extern uint8_t COB_Buffer_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
	extern uint8_t OK_RTMCB[2];
	extern uint16_t rtmcb_dma_counter_new;
	extern uint16_t rtmcb_dma_counter_old;
	extern uint16_t rtmcb_dma_pointer_auto;
	extern uint16_t rtmcb_dma_pointer;
	//extern uint16_t rtmcb_dma_received;
	extern uint8_t  rtmcb_cmd_streaming;



	extern uint32_t rtmcbTimeStamp;
	extern uint32_t rtmcbONnOFFDevices_get;
	extern uint32_t rtmcbONnOFFDevices_set;
	extern uint32_t rtmcbStatus;
	extern uint16_t getYear, setYear;
	extern uint8_t getMonth, setMonth;
	extern uint8_t getDay, setDay;
	extern uint8_t getHours, setHours;
	extern uint8_t getMinutes, setMinutes;
	extern uint8_t getSeconds, setSeconds;

	// RS422 SCHEDULER, APPLICATION, ETC
	extern uint8_t rs422_stateindex_senddata;
	extern uint8_t rs422_rtmcb_whichgroup;
	extern uint8_t rs422_rtmcb_sequenceno;
	extern uint8_t rs422_rtmcb_commands_scheduler[8];
	extern uint16_t rs422_timecounter;
	// -----------------------------------------------------------------------------------
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
        // -----------------------------------------------------------------------------------
        extern uint8_t RTMCB_TASK_NEW_MB_CMD;               // MB TASK GENERATES NEW COMMAND
        extern uint8_t RTMCB_TASK_NEW_MESSAGE;              // RTMCB ANSWERS OR SENDS DATA
        extern uint8_t RTMCB_MBTASK_PUTDATA_WHICHGROUP;     // WHICH COMMAND MB --> RTMCB?
        extern uint8_t RTMCB_MBTASK_GETDATA_WHICHGROUP;     // WHICH COMMAND RTMCB --> MB?
        extern uint8_t RTMCB_MBTASK_GETDATA_WHICHSUBGROUP;  // WHICH COMMAND RTMCB --> MB?
        extern uint8_t rs422_rtmcb_commands_scheduler_bkp;  // backup of current
        extern uint8_t rtmcb_task_sent_counter;             // MB TASK RTMCB CMD SENT
        extern uint8_t rs422_rtmcb_received_counter;        // FULL ANSWER RECEIVED
        
        extern uint8_t RTMCB_WHICHGROUP;
        extern uint8_t RTMCB_WHICHDEVICE;
        extern uint8_t RTMCB_ONNOFF;
        extern uint8_t RTMCB_WHICHDIRECTION;
        extern uint16_t RTMCB_ACT_REFERENCE;
        extern uint8_t RTMCB_OPERATING_MODE;
        extern uint16_t RTMCB_YEAR;
        extern uint8_t RTMCB_MONTH;
        extern uint8_t RTMCB_DAY;
        extern uint8_t RTMCB_HOURS;
        extern uint8_t RTMCB_MINUTES;
        extern uint8_t RTMCB_SECONDS;
        // -----------------------------------------------------------------------------------
#endif                    // 20120608: MB_TASK_CMD_RTMCB
        // -----------------------------------------------------------------------------------

        // ----------------------------------------
	// RS422-CB
	// ----------------------------------------
	extern uint16_t out_cb_reclen_0;
	extern uint8_t OUT_COM_CB_FLAG;
	extern uint8_t SLIP_RX_CB[SLIP_BLEN];
#endif

extern uint8_t lockUartBufferCBin;
extern uint8_t uartBufferCBin[SLIP_BLEN];
extern uint8_t uartBufferCBout[SLIP_BLEN];

#ifndef VP_SIMULATION
	extern uint8_t RAW_RX_CB_1[SLIP_BLEN];
	extern uint8_t SLIP_TX_CB[SLIP_BLEN];
	// ----------------------------------------
	// SPI-UIF
	// ----------------------------------------
	extern uint8_t mb_uif_acc_exchange[SPI2_RXTX_LEN];

	extern uint16_t uif_acc_scheduler_counter;
	extern uint16_t uif_acc_scheduter_timer;

	extern uint8_t uif_data_inpx[UIF_TXRX_LEN];
	extern uint8_t uif_acc_din;
	extern uint8_t uif_acc_dinprev;
	extern uint8_t uif_acc_dout;

	extern uint16_t UIF_ACC_CMD_NO;
	extern uint8_t UIF_ACC_CMD_ID;
	extern uint8_t UIF_ACC_STATEDIAG;
	extern uint8_t UIF_ACC_INDEX;
	extern uint16_t UIF_ACC_TEST_OK;
	extern uint16_t UIF_ACC_TEST_TIMEOUT;
	extern uint16_t UIF_ACC_ACK_NO;
	extern uint16_t UIF_ACC_NACK_NO;

	extern uint16_t uif_acc_data_ok;
	extern uint16_t uif_acc_data_ko;
	extern uint8_t uif_acc_trial_counter;
	// PATIENT INFO ----------------------------------------------------------------------
	extern uint8_t P_DATA_FIRSTNAME[14];
	extern uint8_t P_DATA_LASTNAME[14];
	extern uint8_t P_DATA_GENDER[2];
	extern uint8_t P_DATA_AGE;
	extern uint16_t P_DATA_WEIGHT;
	extern uint8_t P_DATA_HEARTRATE;
	extern uint8_t P_DATA_BREATHINGRATE;
	extern uint8_t P_DATA_ACTIVITYCODE;
	extern uint16_t P_DATA_SISTOLICBP;
	extern uint16_t P_DATA_DIASTOLICBP;
	extern uint16_t P_DATA_NA_ECP1;
	extern uint16_t P_DATA_K_ECP1;
	extern uint16_t P_DATA_PH_ECP1;
	extern uint16_t P_DATA_UREA_ECP1;
	extern uint16_t P_DATA_NA_ECP2;
	extern uint16_t P_DATA_K_ECP2;
	extern uint16_t P_DATA_PH_ECP2;
	extern uint16_t P_DATA_UREA_ECP2;

	// WAKD INFO -------------------------------------------------------------------------
	extern uint16_t VoltageBatteryPM1;
	extern uint16_t VoltageBatteryPM2;
	extern int16_t WAKDAttitude;
	extern uint16_t blood_circuit_pressure;
	extern uint16_t dialysate_circuit_pressure;

	// Battery Packs Variables -----------------------------------------------------------
	// Battery Pack 1
	extern uint16_t MB_BPACK_1_Voltage;
	extern uint16_t MB_BPACK_1_RemainingCapacity;
	// Battery Pack 2
	extern uint16_t MB_BPACK_2_Voltage;
	extern uint16_t MB_BPACK_2_RemainingCapacity;
	// WAKD Status
	extern uint32_t MB_WAKD_Status;
	// WAKD Attitude
	extern uint16_t MB_WAKD_Attitude;
	// CB Status
	extern uint16_t MB_CB_Status;
	// WAKD Operating Mode
	extern uint16_t MB_WAKD_OperatingMode;
	// WAKD Microfluidics
	extern uint32_t MB_WAKD_BLCircuit_Status;
	extern uint32_t MB_WAKD_FLCircuit_Status;
	// WAKD Pumps
        extern uint16_t MB_WAKD_BLPump_Speed;
        extern uint16_t MB_WAKD_BLPump_DirStatus;
        extern uint8_t MB_WAKD_BLPump_SpeedCode;
        extern uint8_t blpump_up;
        extern uint16_t MB_WAKD_FLPump_Speed;
        extern uint16_t MB_WAKD_FLPump_DirStatus;
        extern uint8_t MB_WAKD_FLPump_SpeedCode;
        extern uint8_t flpump_up;
        // WAKD Temperature Sensor
	extern uint16_t MB_WAKD_BTS_InletTemperature;
	extern uint16_t MB_WAKD_BTS_OutletTemperature;
	extern uint16_t MB_WAKD_BTS_Status;
	// WAKD Pressure Sensors
	extern uint16_t MB_WAKD_BPS_Pressure;
	extern uint16_t MB_WAKD_BPS_Status;
	extern uint16_t MB_WAKD_FPS_Pressure;
	extern uint16_t MB_WAKD_FPS_Status;
	// WAKD Conductivity Sensor
	extern uint16_t MB_WAKD_DCS_Conductance;
	extern uint16_t MB_WAKD_DCS_Susceptance;
	extern uint16_t MB_WAKD_DCS_Status;
	// WAKD High Flux Dialyser
	extern uint16_t MB_WAKD_HFD_Status;
	// WAKD Sorbent Unit
	extern uint16_t MB_WAKD_SU_Status;
	// WAKD Polarizer
        extern uint16_t MB_WAKD_POLAR_Voltage;
        extern uint16_t MB_WAKD_POLAR_Status;
        extern uint8_t polar_up;
        extern uint8_t polar_sign;
        // WAKD ECPs
        extern uint16_t MB_WAKD_ECP1_Status;
        extern uint16_t MB_WAKD_ECP2_Status;
        // WAKD PS INFO
        extern uint32_t MB_WAKD_PS_Status;
        // WAKD ACT INFO
        extern uint32_t MB_WAKD_ACT_Status;
        // WAKD Alarms, Errors, Link Test
	extern uint32_t MB_WAKD_ALARMS_Status;
	extern uint32_t MB_WAKD_ERRORS_Status;
	// UIF Information
	extern uint32_t UIF_CMDS_Register;
	extern uint32_t UIF_OPMS_Register;
	extern uint32_t UIF_MSGS_Register;



	extern uint8_t MB_UIF_MSG_VBAT1_INFO[MB_UIF_QTY_VBAT1_INFO];
	extern uint8_t MB_UIF_MSG_VBAT2_INFO[MB_UIF_QTY_VBAT2_INFO];
	extern uint8_t MB_UIF_MSG_VBATS_INFO[MB_UIF_QTY_VBATS_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_STATUS[MB_UIF_QTY_WAKD_STATUS];
	extern uint8_t MB_UIF_MSG_WAKD_ATTITUDE[MB_UIF_QTY_WAKD_ATTITUDE];
	extern uint8_t MB_UIF_MSG_WAKD_COMMLINK[MB_UIF_QTY_WAKD_COMMLINK];
	extern uint8_t MB_UIF_MSG_WAKD_OPMODE[MB_UIF_QTY_WAKD_OPMODE];
	extern uint8_t MB_UIF_MSG_WAKD_BLCIRCUIT_STATUS[MB_UIF_QTY_WAKD_BLCIRCUIT_STATUS];
	extern uint8_t MB_UIF_MSG_WAKD_FLCIRCUIT_STATUS[MB_UIF_QTY_WAKD_FLCIRCUIT_STATUS];
	extern uint8_t MB_UIF_MSG_WAKD_MFCIRCUIT_STATUS[MB_UIF_QTY_WAKD_MFCIRCUIT_STATUS];
	extern uint8_t MB_UIF_MSG_WAKD_BLPUMP_INFO[MB_UIF_QTY_WAKD_BLPUMP_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_FLPUMP_INFO[MB_UIF_QTY_WAKD_FLPUMP_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_BLCIRCUIT_PRESSURE[MB_UIF_QTY_WAKD_BLCIRCUIT_PRESSURE];
	extern uint8_t MB_UIF_MSG_WAKD_FLCIRCUIT_PRESSURE[MB_UIF_QTY_WAKD_FLCIRCUIT_PRESSURE];
	extern uint8_t MB_UIF_MSG_WAKD_FLCONDUCTIVITY[MB_UIF_QTY_WAKD_FLCONDUCTIVITY];
	extern uint8_t MB_UIF_MSG_WAKD_HFD_INFO[MB_UIF_QTY_WAKD_HFD_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_SU_INFO[MB_UIF_QTY_WAKD_SU_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_POLAR_INFO[MB_UIF_QTY_WAKD_POLAR_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_ECP1_INFO[MB_UIF_QTY_WAKD_ECP1_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_ECP2_INFO[MB_UIF_QTY_WAKD_ECP2_INFO];
	extern uint8_t MB_UIF_MSG_WAKD_ECPS_INFO[MB_UIF_QTY_WAKD_ECPS_INFO];
	extern uint8_t MB_UIF_MSG_PDATA_FIRSTNAME[MB_UIF_QTY_PDATA_FIRSTNAME];
	extern uint8_t MB_UIF_MSG_PDATA_LASTNAME[MB_UIF_QTY_PDATA_LASTNAME];
	extern uint8_t MB_UIF_MSG_PDATA_GENDERAGE[MB_UIF_QTY_PDATA_GENDERAGE];
	extern uint8_t MB_UIF_MSG_PDATA_WEIGHT[MB_UIF_QTY_PDATA_WEIGHT];
	extern uint8_t MB_UIF_MSG_PDATA_HEARTRATE[MB_UIF_QTY_PDATA_HEARTRATE];
	extern uint8_t MB_UIF_MSG_PDATA_BREATHRATE[MB_UIF_QTY_PDATA_BREATHRATE];
	extern uint8_t MB_UIF_MSG_PDATA_ACTIVITY[MB_UIF_QTY_PDATA_ACTIVITY];
	extern uint8_t MB_UIF_MSG_PDATA_SEWALL[MB_UIF_QTY_PDATA_SEWALL];
	extern uint8_t MB_UIF_MSG_PDATA_BLPRESSURE[MB_UIF_QTY_PDATA_BLPRESSURE];
	extern uint8_t MB_UIF_MSG_PDATA_ECP1[MB_UIF_QTY_PDATA_ECP1];
	extern uint8_t MB_UIF_MSG_PDATA_ECP2[MB_UIF_QTY_PDATA_ECP2];
	extern uint8_t MB_UIF_MSG_ALARMS[MB_UIF_QTY_ALARMS];
	extern uint8_t MB_UIF_MSG_ERRORS[MB_UIF_QTY_ERRORS];
	extern uint8_t MB_UIF_MSG_WAKD_START_OPERATION[MB_UIF_QTY_WAKD_START_OPERATION];
	extern uint8_t MB_UIF_MSG_WAKD_MODE_OPERATION[MB_UIF_QTY_WAKD_MODE_OPERATION];
	extern uint8_t MB_UIF_MSG_SCALE_GET_WEIGHT[MB_UIF_QTY_SCALE_GET_WEIGHT];
	extern uint8_t MB_UIF_MSG_SEW_START_STREAMING[MB_UIF_QTY_SEW_START_STREAMING];
	extern uint8_t MB_UIF_MSG_SEW_STOP_STREAMING[MB_UIF_QTY_SEW_STOP_STREAMING];
	extern uint8_t MB_UIF_MSG_NIBP_START_MEASUREMENT[MB_UIF_QTY_NIBP_START_MEASUREMENT];

	// -----------------------------------------------------------------------------------
	extern uint8_t MB_UIF_MSG_WAKD_SHUTDOWN[MB_UIF_QTY_CMD_GET+4];
	extern uint8_t MB_UIF_MSG_WAKD_BLTEMPERATURE[MB_UIF_QTY_CMD_SET+MB_UIF_QTY_ANSWER_0];
	extern uint8_t MB_UIF_MSG_TEST_LINK[MB_UIF_QTY_CMD_SET+MB_UIF_QTY_ANSWER_0];

        // -----------------------------------------------------------------------------------
        // Generic Exchange Buffer for Group 1
        extern uint8_t MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[MB_UIF_QTY_CMD_SET];
        // -----------------------------------------------------------------------------------
        // ACCELEROMETER
        // -----------------------------------------------------------------------------------
#define ACCELEROMETER_SIZE 36
        // DON'T ERASE
        extern uint8_t accelerometer_ok;
        extern uint16_t acc_whoami;
        extern uint32_t acc_TimeStamp[ACCELEROMETER_SIZE][2];
        extern uint16_t acc_status[ACCELEROMETER_SIZE][2];
        extern int16_t acc_out_x[ACCELEROMETER_SIZE][2];
        extern int16_t acc_out_y[ACCELEROMETER_SIZE][2];
        extern int16_t acc_out_z[ACCELEROMETER_SIZE][2];
        uint8_t ACCELEROMETER_INDEX;
        uint8_t ACCELEROMETER_BFULL;
        extern uint8_t ACCELEROMETER_UPDATING; 
        // -----------------------------------------------------------------------------------
        // RTMCB-MB TESTING
        // -----------------------------------------------------------------------------------
        extern uint32_t status_message_received_counter;
        extern uint32_t ps_message_received_counter;
        extern uint32_t ecp_message_received_counter;
        extern uint32_t act_message_received_counter;
        extern uint32_t alarms_message_received_counter;
        // -----------------------------------------------------------------------------------
        // Exported functions
        // -----------------------------------------------------------------------------------
        // special functions:
        // ------------------
        // HFSR READ
        extern uint32_t HardFault_Read(HFSR_TypeDef*);
        // MMSR READ
        extern uint8_t MemoryManagementFault_Read(MMFSR_TypeDef*);
        // BFSR READ
        extern uint8_t BusFault_Read(BFSR_TypeDef*);
        // UFSR READ
        extern uint16_t UsageFault_Read(UFSR_TypeDef*);
        // OTHER EXCEPTION HANDLERS ENABLE
        // BUS FAULT HANDLER ENABLE
        extern void BusFault_Handler_Enable (uint8_t);
        // BUS FAULTS EXCEPTION CLEAR
        extern void BusFault_Exception_Clear(void);
        // ------------------
        // DATA FUNCTIONS
        // ------------------
	extern void MB_DATA_INIT(void);
	extern void MB_SYSTEM_INIT(void);
	extern void MB_RTMCB_SCHEDULER_FILL(uint8_t fillmode);
	#if 0
	extern void EXECUTE_SENSORS_ACTUATORS_UPDATE(void);
	#endif
	// -----------------------------------------------------------------------------------
	#endif /* DATA_H_ */
	// -----------------------------------------------------------------------------------
	// (C) COPYRIGHT 2011 CSEM SA
	// -----------------------------------------------------------------------------------
	// END OF FILE
	// -----------------------------------------------------------------------------------
#endif
