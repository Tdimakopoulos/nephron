// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   clock_calendar.c
//! \brief  rtc routines
//!
//! Clock Calendar basic routines
//!
//! \author  MCD Application Team
//! \date    11.06.2011
//! \version 1.0 First RTMCB Version (GDU) 
// -----------------------------------------------------------------------------------
#ifndef __CLOCK_CALENDAR_H
#define __CLOCK_CALENDAR_H

// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
#define BATTERY_REMOVED 98
#define BATTERY_RESTORED 99
#define SECONDS_IN_DAY 86399
#define CONFIGURATION_DONE 0xAAAA
#define CONFIGURATION_RESET 0x0000
#define OCTOBER_FLAG_SET 0x4000
#define MARCH_FLAG_SET 0x8000
#define DEFAULT_DAY 22
#define DEFAULT_MONTH 10
#define DEFAULT_YEAR 2009
#define DEFAULT_HOURS 22
#define DEFAULT_MINUTES 33
#define DEFAULT_SECONDS 44
#define LEAP 1
#define NOT_LEAP 0


// ----------------------------------------
// BACKUP REGISTERS DEFINITION (RTMCB v0r0)
// ----------------------------------------
// BKP_DR1: CONFIGURATION_DONE
// BKP_DR2: DATE{Month}
// BKP_DR3: DATE{Day}
// BKP_DR4: DATE{Year}
// BKP_DR5: TamperNumber
// BKP_DR6: ClockSource (?)
// BKP_DR7: SummerTimeCorrect
// BKP_DR8: ALARMDATE{Month}
// BKP_DR9: ALARMDATE{Day}
// BKP_DR10: ALARMDATE{Year}
// BKP_DR11: TIME{Seconds}
// BKP_DR12: TIME{Minutes}
// BKP_DR13: TIME{Hours}
// BKP_DR14: ALARMTIME{Seconds}
// BKP_DR15: ALARMTIME{Minutes}
// BKP_DR16: ALARMTIME{Hours}
// BKP_DR17: TIMECOUNTER_MSB
// BKP_DR18: TIMECOUNTER_LSB
// BKP_DR19: ALARMTIMECOUNTER_MSB
// BKP_DR20: ALARMTIMECOUNTER_LSB
// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
extern __IO uint8_t TotalMenuPointer;
extern uint8_t BatteryRemoved;
extern uint16_t SummerTimeCorrect;
extern uint8_t TamperNumber;
extern uint8_t TamperEvent;
extern uint8_t AlarmStatus;
extern uint8_t AlarmDate;
extern float f32_Frequency;
extern uint32_t TimerFrequency;

// -----------------------------------------------------------------------------------
// Exported type definitions
// -----------------------------------------------------------------------------------
// Time Structure definition 
// ----------------------------------------
struct Time_s
{
  uint8_t SecLow;
  uint8_t SecHigh;
  uint8_t MinLow;
  uint8_t MinHigh;
  uint8_t HourLow;
  uint8_t HourHigh;
};
extern struct Time_s s_TimeStructVar;

// ----------------------------------------
// Alarm Structure definition 
// ----------------------------------------
struct AlarmTime_s
{
  uint8_t SecLow;
  uint8_t SecHigh;
  uint8_t MinLow;
  uint8_t MinHigh;
  uint8_t HourLow;
  uint8_t HourHigh;
};
extern struct AlarmTime_s s_AlarmStructVar;

// ----------------------------------------
// Date Structure definition 
// ----------------------------------------
struct Date_s
{
  uint8_t Month;
  uint8_t Day;
  uint16_t Year;
};
extern struct Date_s s_DateStructVar;

// ----------------------------------------
// Alarm Date Structure definition 
// ----------------------------------------
struct AlarmDate_s
{
  uint8_t Month;
  uint8_t Day;
  uint16_t Year;
};
extern struct AlarmDate_s s_AlarmDateStructVar;
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void RTC_Configuration(void);
extern void SetTime(uint8_t,uint8_t,uint8_t);
extern void SetAlarmTime(uint8_t,uint8_t,uint8_t);
extern void SetDate(uint8_t,uint8_t,uint16_t);
extern void SetAlarmDate(uint8_t,uint8_t,uint16_t);
extern void DateUpdate(void);
extern uint16_t WeekDay(uint16_t,uint8_t,uint8_t);
extern uint8_t CheckLeap(uint16_t);
extern uint32_t CalculateTimeG(void);
extern void DelayLowPower(__IO uint32_t nCount);
extern void CheckForDaysElapsed(void);
extern void SummerTimeCorrection(void);
extern void AutoClockCalibration(void);
extern void RTC_Application(void);
extern void ReadDate(void);
extern void ReadTime(void);
// -----------------------------------------------------------------------------------
#endif /* __CLOCK_CALENDAR_H */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
