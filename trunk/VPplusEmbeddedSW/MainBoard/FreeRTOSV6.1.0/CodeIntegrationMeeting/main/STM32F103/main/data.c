// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   data.c
//! \brief  Data Initialization and Cleanup
//!
//! Collection of data initialization routines
//!
//! \author  Dudnik G.S.
//! \date    20.07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"


// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// APPLICATION DATA
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// TIMING, RTC, ETC
// -----------------------------------------------------------------------------------
// gdu:SysTick
uint32_t globalTimestamp = 0;
uint16_t led_toggle_counter = 0;
uint16_t uif_acc_data_counter = 0;
uint16_t uif_acc_cmd_counter = 0;

// DMA
uint16_t ADCDMACounter = 0;

// RTC
__IO uint32_t TimingDelay = 0;
__IO uint8_t TotalMenuPointer = 1;
uint8_t AlarmStatus = 0;
uint8_t AlarmDate = 0;
uint8_t TamperEvent = 0;
uint8_t TamperNumber = 0;
uint8_t BatteryRemoved = 0;
uint16_t SummerTimeCorrect = 0;
float f32_Frequency = 0;
uint32_t TimerFrequency = 0;
// -----------------------------------------------------------------------------------
// MB SYSTEM INITIALIZED / PC ACK
// -----------------------------------------------------------------------------------
uint8_t MB_SYSTEM_READY = DEVICE_NOTREADY;
uint8_t RTMCB_SimulationNoRT = SIMULATION_MODE;
uint8_t RTMCB_StreamingYesNo = STREAMING_OFF;
// flags to tell if new data from RTMCB
// #ifndef INTER_INTEGRATION_201103
uint32_t RTMCB_channelsFlag_01 = 0x00000000;
uint32_t RTMCB_channelsFlag_02 = 0x00000000;
uint8_t LINE_RTMCBuC_nIRQ = SIGNAL_OFF;
// IF 1 RESET PROCEDURE WILL EXECUTE IN RTOS SYSTICK
uint8_t RTMCB_RESET_CMD = 1;                    
// -----------------------------------------------------------------------------------
// OFFIS STRUCTURES
// -----------------------------------------------------------------------------------
#include "..\\CodeFromOFFIS\\includes_nephron\\interfaces.h"
// After UART buffer "in" is complete, ISR informs Task CBPA which
// calls the function decodeMsg() (by CSEM). decodeMsg() interprets
// the "in" buffer and copies the values to the following structures.
// From there CBPA will read and process them.
tdDevicesStatus         staticBufferDevicesStatus;
tdAlarms                staticBufferAlarms;
tdPhysiologicalData 	staticBufferPhysiologicalData;
tdPhysicalsensorData	staticBufferPhysicalsensorData;
tdActuatorData		staticBufferActuatorData;
tdWakdStates		staticBufferStateRTB		= wakdStates_UndefinedInitializing;
tdWakdOperationalState	staticBufferOperationalStateRTB	= wakdStatesOS_UndefinedInitializing;

// Flag for the task
tdDataId RTMCB_message = dataID_undefined;
// -----------------------------------------------------------------------------------
// ADC
// -----------------------------------------------------------------------------------
uint16_t ADC_FeedBack_ReadValue[4];
uint16_t ADC_FeedBack_EchoValue[4];
// -----------------------------------------------------------------------------------
// SD CARD
// -----------------------------------------------------------------------------------
uint8_t SDCARD_PRESENT = SDCARD_NO_THERE;
uint8_t SDCARD_HISTORIC = SIGNAL_OFF;  // IF NOW PRESENT AND NOT BEFORE RE-INIT
EmbeddedFileSystem sdcard_efs;
EmbeddedFile sdcard_filerdx; 
EmbeddedFile sdcard_filewrx; 
int8_t sdlib_initialised; 
uint8_t STMEM_BUFFER[STMEM_BUFFER_SIZE];
uint32_t sdfilecounter = 0;
uint8_t file_is_open = 0;
esint8 res = 0;
esint32 sdbytesread = 0;
uint8_t LOCK_CRITICAL_INTS = 0;
uint16_t abc=0;
// -----------------------------------------------------------------------------------
// SERIAL PORT TEST (USART3/UART5)
// ***TB-MODIFIED 
// (CANNOT USE AT THE SAME TIME)
// -----------------------------------------------------------------------------------
uint8_t zrx_out_str[TXRX_MAXVALUE];
uint8_t ztx_out_str[TXRX_MAXVALUE];
uint16_t OUT_STR_TX_LEN = 0;
uint16_t OUT_STR_RX_LEN = 0;
uint16_t out_reclen = 0;
uint8_t OUT_COMM_FLAG = 0x80;
// -----------------------------------------------------------------------------------
// RS422-RTMCB (RTMCB-USART/DMA)
// -----------------------------------------------------------------------------------
uint8_t RTMCB_STATUS = DEVICE_NOTREADY;
uint8_t QUERY_INFO = 0;
uint8_t RTMCB_DMA_RX_NEW = SIGNAL_OFF;

uint16_t out_rtmcb_reclen_0 = 0;
uint16_t out_rtmcb_reclen_1 = 0;
uint8_t INP_COMM_RTMCB_FLAG = 0;
uint8_t OUT_COMM_RTMCB_FLAG = 0;
uint8_t zrx_out_rtmcb_str_1[TXRX_RS422_RTMCB_MAXVALUE];
uint8_t zrx_out_rtmcb_str_0[TXRX_RS422_RTMCB_MAXVALUE];
// maximum necessary in fact 256 instead of 488
uint16_t UART4_RX_DMA_RTMCB[RX_DMA_RS422_RTMCB_TOTAL];
uint8_t zrx_out_rtmcb_str_x[RX_DMA_RS422_RTMCB_TOTAL];
uint16_t  rs422_tx_rtmcb_counter = 0;
uint8_t BufferXYZ_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
uint8_t RAW_Buffer_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
uint8_t COB_Buffer_RTMCB[TXRX_RS422_RTMCB_MAXVALUE];
uint8_t OK_RTMCB[2];
uint16_t rtmcb_dma_counter_new = 0;
uint16_t rtmcb_dma_counter_old = 0;
uint16_t rtmcb_dma_pointer_auto = 0;
uint16_t rtmcb_dma_pointer = 0;
uint8_t  rtmcb_cmd_streaming = SIGNAL_OFF;

uint32_t rtmcbTimeStamp = 0;
uint32_t rtmcbONnOFFDevices_get = 0;
uint32_t rtmcbONnOFFDevices_set = 0; 
uint32_t rtmcbStatus = 0;
uint16_t getYear = 0, setYear = 0;
uint8_t getMonth = 0, setMonth = 0;
uint8_t getDay = 0, setDay = 0;
uint8_t getHours = 0, setHours = 0;
uint8_t getMinutes = 0, setMinutes = 0;
uint8_t getSeconds = 0, setSeconds = 0;

// ----------------------------------------
// RTMCB RS422 SCHEDULER, APPLICATION, ETC
// ----------------------------------------
uint8_t rs422_rtmcb_commands_scheduler[8];
uint8_t rs422_stateindex_senddata = 0;
uint8_t rs422_rtmcb_whichgroup = RS422_NOGROUP;
uint8_t rs422_rtmcb_sequenceno = 0;
uint16_t rs422_timecounter = 0;

// -----------------------------------------------------------------------------------
// RTMCB TASK LLD OUTPUT DATA
// -----------------------------------------------------------------------------------
#ifdef MB_TASK_CMD_RTMCB  // 20120608: MB CAN SEND ORDERS TO RTMCB & RECEIVE ACKS
// -----------------------------------------------------------------------------------
uint8_t RTMCB_TASK_NEW_MB_CMD = SIGNAL_OFF;                     // MB TASK GENERATES NEW COMMAND
uint8_t RTMCB_TASK_NEW_MESSAGE = SIGNAL_OFF;                    // RTMCB ANSWERS OR SENDS DATA
uint8_t RTMCB_MBTASK_PUTDATA_WHICHGROUP = RS422_NOGROUP;        // WHICH COMMAND MB --> RTMCB?
uint8_t RTMCB_MBTASK_GETDATA_WHICHGROUP = RS422_NOGROUP;        // WHICH COMMAND RTMCB --> MB?
uint8_t RTMCB_MBTASK_GETDATA_WHICHSUBGROUP = dataID_undefined;  // WHICH COMMAND RTMCB --> MB?
uint8_t rs422_rtmcb_commands_scheduler_bkp = RS422_NOGROUP;     // backup of current
uint8_t rtmcb_task_sent_counter = 0;                            // MB TASK RTMCB CMD SENT
uint8_t rs422_rtmcb_received_counter = 0;                       // FULL ANSWER RECEIVED

uint8_t RTMCB_WHICHGROUP = 0;
uint8_t RTMCB_WHICHDEVICE = 0;
uint8_t RTMCB_ONNOFF = 0;
uint8_t RTMCB_WHICHDIRECTION = 0; // SENS_DIRECT;
uint16_t RTMCB_ACT_REFERENCE = 0; // _SPEED_CODE_1000L; // MAXIMUM SPEED LEFT, 0;
uint8_t RTMCB_OPERATING_MODE = 0;
uint16_t RTMCB_YEAR = 0;
uint8_t RTMCB_MONTH = 0;
uint8_t RTMCB_DAY = 0;
uint8_t RTMCB_HOURS = 0;
uint8_t RTMCB_MINUTES = 0;
uint8_t RTMCB_SECONDS = 0;
// -----------------------------------------------------------------------------------
#endif                    // 20120608: MB_TASK_CMD_RTMCB
// -----------------------------------------------------------------------------------
// RS422-CB
// -----------------------------------------------------------------------------------
xSemaphoreHandle xCBMBSemaphore;    

uint16_t out_cb_reclen_0 = 0;
uint8_t OUT_COM_CB_FLAG = 0;
uint8_t lockUartBufferCBin = SIGNAL_OFF;

uint8_t SLIP_RX_CB[SLIP_BLEN];
uint8_t uartBufferCBin[SLIP_BLEN];
uint8_t uartBufferCBout[SLIP_BLEN];
uint32_t dummy_WhatDataToPullCB;

	
uint8_t SLIP_TX_CB[SLIP_BLEN];
uint16_t crc16_cb_ccrx = 0;
uint16_t crc16_cb_recv = 0;
uint16_t crc16_cb_cctx = 0;
// -----------------------------------------------------------------------------------
// USB-USART1
// -----------------------------------------------------------------------------------
uint16_t out_usb_reclen_0 = 0;
uint8_t INP_COMM_USB_FLAG = 0;
uint8_t zrx_out_usb_str_0[TXRX_RS422_USB_MAXVALUE];
// -----------------------------------------------------------------------------------
// SPI-UIF
// -----------------------------------------------------------------------------------
uint8_t mb_uif_acc_exchange[SPI2_RXTX_LEN];

uint16_t uif_acc_scheduler_counter=0;
uint16_t uif_acc_scheduter_timer = 0;

uint8_t uif_data_inpx[UIF_TXRX_LEN];
uint8_t uif_acc_din = 0;
uint8_t uif_acc_dinprev = 0;
uint8_t uif_acc_dout = 0;

uint16_t UIF_ACC_CMD_NO = MB_UIF_SCHNO_TEST_LINK;
uint8_t UIF_ACC_CMD_ID = 0;
uint8_t UIF_ACC_STATEDIAG = UIF_STATE_START;
uint8_t UIF_ACC_INDEX = 0;
uint16_t UIF_ACC_TEST_OK = 0;
uint16_t UIF_ACC_COMMAND_OK = 0;
uint16_t UIF_ACC_TEST_TIMEOUT = 0;
uint16_t UIF_ACC_ACK_NO = 0;
uint16_t UIF_ACC_NACK_NO = 0;

// User Generates a Command
uint16_t UIF_USER_COMMAND = 0;
// WAKD Feedbacks Command once accepted...
uint16_t MB_USER_COMMAND = 0;

uint16_t uif_acc_data_ok = 0;
uint16_t uif_acc_data_ko = 0;
uint8_t uif_acc_trial_counter = 0;
// PATIENT INFO ----------------------------------------------------------------------
uint8_t P_DATA_INITIALS[4] = {'M','S','G','\0'};
uint8_t P_DATA_PATIENTCODE[4] = {'1','2','3','\0'};
uint8_t P_DATA_GENDER[2] = {'F','\0'};
uint8_t P_DATA_AGE = 50;
uint16_t P_DATA_WEIGHT = 63;
uint8_t P_DATA_HEARTRATE = 75;
uint8_t P_DATA_BREATHINGRATE = 20;
uint8_t P_DATA_ACTIVITYCODE = ACTIVITY_RUNNING;
uint16_t P_DATA_SISTOLICBP = 12;
uint16_t P_DATA_DIASTOLICBP = 8;
uint16_t P_DATA_NA_ECP1 = 123;
uint16_t P_DATA_K_ECP1 = 22;
uint16_t P_DATA_PH_ECP1 = 7;
uint16_t P_DATA_UREA_ECP1 = 9;
uint16_t P_DATA_NA_ECP2 = 111;
uint16_t P_DATA_K_ECP2 = 55;
uint16_t P_DATA_PH_ECP2 = 10;
uint16_t P_DATA_UREA_ECP2 = 88;
// WAKD INFO -------------------------------------------------------------------------
uint16_t VoltageBatteryPM1 = 12800;         // in mV
uint16_t VoltageBatteryPM2 = 0;             // in mV
int16_t WAKDAttitude = 90;                  // in degrees
uint8_t blood_outgoing_temperature = 38;    // C
uint8_t blood_incoming_temperature = 37;    // C
uint16_t blood_circuit_pressure = 333;       // mbar
uint16_t dialysate_circuit_pressure = 495;   // mbar
// Battery Packs Variables -----------------------------------------------------------
// Battery Pack 1
uint8_t MB_BPACK_1_Percent = 0;
uint16_t MB_BPACK_1_Voltage = 0;
uint16_t MB_BPACK_1_RemainingCapacity = 0;
// Battery Pack 2
uint8_t MB_BPACK_2_Percent = 0;
uint16_t MB_BPACK_2_Voltage = 0;
uint16_t MB_BPACK_2_RemainingCapacity = 0;
// WAKD Status 
uint32_t MB_WAKD_Status = 0;
// WAKD Attitude
uint16_t MB_WAKD_Attitude = 0;
uint16_t MB_WAKD_ACC_X[32];
uint16_t MB_WAKD_ACC_Y[32];
uint16_t MB_WAKD_ACC_Z[32];

// CB Status
uint16_t MB_CLINKS_Status = CLINKS_NONE;
// WAKD Operating Mode
uint16_t MB_WAKD_OperatingMode = 0;
// WAKD Microfluidics
uint32_t MB_WAKD_BLCircuit_Status = SENSOR_KO;
uint32_t MB_WAKD_FLCircuit_Status = SENSOR_NOINFO;
// WAKD Pumps
uint16_t MB_WAKD_BLPump_Speed = 0;
uint16_t MB_WAKD_BLPump_DirStatus = 0;
uint8_t MB_WAKD_BLPump_SpeedCode = _PWM_CODE_RRIGHT_MAX;
uint8_t blpump_up = 0;

uint16_t MB_WAKD_FLPump_Speed = 0;
uint16_t MB_WAKD_FLPump_DirStatus = 0;
uint8_t MB_WAKD_FLPump_SpeedCode = _PWM_CODE_RLEFT_MIN;
uint8_t flpump_up = 1;

// WAKD Temperature Sensor
uint16_t MB_WAKD_BTS_InletTemperature = 0;
uint16_t MB_WAKD_BTS_OutletTemperature = 0;
// WAKD Pressure Sensors
uint16_t MB_WAKD_BPS_Pressure = 0;
uint16_t MB_WAKD_FPS_Pressure = 0;
// WAKD Conductivity Sensor
uint16_t MB_WAKD_DCS_Conductance = 0;
uint16_t MB_WAKD_DCS_Susceptance = 1000;
// WAKD High Flux Dialyser
uint8_t MB_WAKD_HFD_Status = DEVICE_OK;
uint8_t MB_WAKD_HFD_PercentOK = 0;
// WAKD Sorbent Unit
uint16_t MB_WAKD_SU_Status = 0;
// WAKD Polarizer
uint16_t MB_WAKD_POLAR_Voltage = 0;
uint16_t MB_WAKD_POLAR_Status = 0;
uint8_t polar_up = 0;
uint8_t polar_sign = 0;
// WAKD ECPs
uint16_t MB_WAKD_ECP1_Status = 0;
uint16_t MB_WAKD_ECP2_Status = 0;
// WAKD PS INFO
uint32_t MB_WAKD_PS_Status = 0;
uint8_t MB_WAKD_BTS_Status = SENSOR_OK;
uint8_t MB_WAKD_BPS_Status = SENSOR_KO;
uint8_t MB_WAKD_FPS_Status = SENSOR_NOINFO;
uint8_t MB_WAKD_DCS_Status = SENSOR_OK;
uint8_t MB_WAKD_VCS_Status = SENSOR_KO;

// WAKD ACT INFO
uint32_t MB_WAKD_ACT_Status = 0;
// WAKD Alarms, Errors, Link Test
uint32_t MB_WAKD_ALARMS_Status = 0x00000000;
uint32_t MB_WAKD_ERRORS_Status = 0xFFFFFFFF;
// UIF Information
uint16_t UIF_CMDS_Register = 1234;
uint32_t UIF_OPMS_Register = 55555555;
uint32_t UIF_MSGS_Register = 87654321;

// -----------------------------------------------------------------------------------
uint8_t MB_UIF_MSG_CMD_EXCHANGE_BUFFER[MB_UIF_QTY_CMD_GET+4] = {MB_UIF_CMD_WAKD_SHUTDOWN,MB_UIF_CMD_END,MB_UIF_CMD_CLR,MB_UIF_CMD_CLR,MB_UIF_CMD_CLR,MB_UIF_CMD_CLR};
uint8_t MB_UIF_MSG_TEST_LINK[MB_UIF_QTY_CMD_SET] = {MB_UIF_CMD_TEST_LINK,0x12,0x34,0x56,0x78,MB_UIF_CMD_END};
// -----------------------------------------------------------------------------------
// Generic Exchange Buffer for Group 1
uint8_t MB_UIF_MSG_GROUP_1_EXCHANGE_BUFFER[MB_UIF_QTY_CMD_SET] = {0x00, 0x00, 0x00, 0x00, 0x00, MB_UIF_CMD_END};
// -----------------------------------------------------------------------------------
// ACCELEROMETER
// -----------------------------------------------------------------------------------
// DON'T ERASE
uint8_t accelerometer_ok = SIGNAL_OFF;    // 0: NOT OK, 1: OK
uint16_t acc_whoami = 0;
uint32_t acc_TimeStamp[ACCELEROMETER_SIZE][2];
uint16_t acc_status[ACCELEROMETER_SIZE][2];
int16_t acc_out_x[ACCELEROMETER_SIZE][2];
int16_t acc_out_y[ACCELEROMETER_SIZE][2];
int16_t acc_out_z[ACCELEROMETER_SIZE][2];
uint8_t ACCELEROMETER_INDEX = 0;
uint8_t ACCELEROMETER_BFULL = SIGNAL_OFF;
uint8_t ACCELEROMETER_UPDATING = 0;       // 1: updating, do not copy, wait...
// -----------------------------------------------------------------------------------
// RTMCB-MB TESTING
// -----------------------------------------------------------------------------------
uint32_t status_message_received_counter=0;
uint32_t ps_message_received_counter=0;
uint32_t ecp_message_received_counter=0;
uint32_t act_message_received_counter=0;
uint32_t alarms_message_received_counter=0;
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  HardFault_Read
//!
//! Tries to read Hardware Fault Status register
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
uint32_t HardFault_Read(HFSR_TypeDef* scb_hfsr)
{
    uint32_t hfsr_rtmcb = 0;
    uint32_t hfsr = scb_hfsr->HFSR_REG0 & HFSR_SET_DEBUGEVT;
    if(hfsr!=0) hfsr_rtmcb = 0x00000001;
    hfsr = scb_hfsr->HFSR_REG0 & HFSR_SET_FORCED;
    if(hfsr!=0) hfsr_rtmcb = 0x00000002;
    hfsr = scb_hfsr->HFSR_REG0 & HFSR_SET_VECTTBL;
    if(hfsr!=0) hfsr_rtmcb = 0x00000004;
    //scb_hfsr->HFSR_REG0|= HFSR_CLEAN_ALL;
    HFSR->HFSR_REG0|= HFSR_CLEAN_ALL;
    return hfsr_rtmcb;
}
// -----------------------------------------------------------------------------------
//! \brief  MemoryManagementFault_Read
//!
//! Tries to read Memory Management Fault Status register
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
uint8_t MemoryManagementFault_Read(MMFSR_TypeDef* scb_mmfsr)
{
    uint8_t mmfsr = scb_mmfsr->MMFSR_REG0;
    //scb_mmfsr->MMFSR_REG0 |= MMFSR_CLEAN_ALL;
    MMFSR->MMFSR_REG0 |= MMFSR_CLEAN_ALL;
    return mmfsr;
}

// -----------------------------------------------------------------------------------
//! \brief  BusFault_Read
//!
//! Tries to read Bus Fault Status register
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
uint8_t BusFault_Read(BFSR_TypeDef* scb_bfsr)
{
    uint8_t bfsr = scb_bfsr->BFSR_REG0;
    BFSR->BFSR_REG0 |= BFSR_CLEAN_ALL;
    return bfsr;
}


// -----------------------------------------------------------------------------------
//! \brief  UsageFault_Read
//!
//! Tries to read Usage Fault Status register
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
uint16_t UsageFault_Read(UFSR_TypeDef* scb_ufsr)
{
    uint16_t ufsr = scb_ufsr->UFSR_REG0;
    //scb_ufsr->UFSR_REG0 |= UFSR_CLEAN_ALL;
    UFSR->UFSR_REG0 |= UFSR_CLEAN_ALL;
    return ufsr;
}
// -----------------------------------------------------------------------------------
//! \brief  BusFault_Handler_Enable (uint8_t enableNdisable)
//!
//! Tries to read Usage Fault Status register
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void BusFault_Handler_Enable (uint8_t enableNdisable)
{
    if(enableNdisable==1){
        SCB_SHCSR->SCB_SHCSR0 |= SCB_SHCSR_SET_BUSFAULTENA;
    }
    else{
        SCB_SHCSR->SCB_SHCSR0 &= SCB_SHCSR_RST_BUSFAULTENA;
    }
}

// -----------------------------------------------------------------------------------
//! \brief  BusFault_Exception_Clear
//!
//! Tries to clear the Bus Exception flags
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void BusFault_Exception_Clear(void)
{
    SCB_SHCSR->SCB_SHCSR0 &= SCB_SHCSR_RST_BUS_EXCEPTIONS;
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  MB_DATA_INIT
//!
//! gathers all initialization data functions for startup
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void MB_DATA_INIT(void)
{
    // WORKING MODE
    MB_SYSTEM_INIT();
    // RTMCB COM INDEXES/FLAGS
    CLR_RTMCB_COMM_BUFFERS();
    // CB COM INDEXES/FLAGS
    CB_CLR_OUT_COM_BUFFERS();       
    // SCHEDULER
    MB_RTMCB_SCHEDULER_FILL(SCHEDULER_NONE);

}
// -----------------------------------------------------------------------------------
//! \brief  MB_SYSTEM_INIT
//!         
//! Working Mode Variables
//!
//! \param[in,out]  none
//! \return         nothing
// -----------------------------------------------------------------------------------
void MB_SYSTEM_INIT(void){

    uint8_t z = 0;
    // ----------------------------------------
    // MB SYSTEM INITIALIZED / PC ACK
    // ----------------------------------------
    MB_SYSTEM_READY = DEVICE_NOTREADY;
    // ----------------------------------------
    // RTMCB
    // ----------------------------------------
    RTMCB_SimulationNoRT = REALTIME_MODE;
    RTMCB_StreamingYesNo = STREAMING_OFF;
    // flags to tell if new data
    RTMCB_channelsFlag_01 = 0x00000000;
    RTMCB_channelsFlag_02 = 0x00000000;
    
    // RS422 SCHEDULER, APPLICATION, ETC
    rs422_rtmcb_whichgroup = RS422_NOGROUP;
    rs422_rtmcb_sequenceno = 0;
    
    rtmcb_dma_counter_new = 0;
    rtmcb_dma_counter_old = 0;
    rtmcb_dma_pointer = 0;
    //rtmcb_dma_received = 0;
    for(z=0;z<SPI2_RXTX_LEN;z++) {
        mb_uif_acc_exchange[z] = MB_UIF_CMD_TEST1;
        
    }
    uif_acc_data_ok = 0;
    uif_acc_data_ko = 0;
    uif_acc_trial_counter = 0;
}

// -----------------------------------------------------------------------------------
//! \brief  MB_RTMCB_SCHEDULER_FILL
//!         
//! Fills the command scheduler
//!
//! \param[in,out]  none
//! \return         nothing
// -----------------------------------------------------------------------------------
void MB_RTMCB_SCHEDULER_FILL(uint8_t fillmode){
    uint8_t z = 0;
    rs422_stateindex_senddata = 0;
    for(z=0;z<8;z++) rs422_rtmcb_commands_scheduler[z]=0;
    switch(fillmode){
        case SCHEDULER_STARTUP:
        RTMCB_SimulationNoRT = REALTIME_MODE;
        RTMCB_StreamingYesNo = STREAMING_ON;
        rs422_rtmcb_commands_scheduler[0] = RS422_SIMULATIONVSRT;
        rs422_rtmcb_commands_scheduler[1] = RS422_STSTSTREAMING;
        break;
        case SCHEDULER_NONE:
        break;
    }
}
// -----------------------------------------------------------------------------------
//! \brief  EXECUTE_SENSORS_ACTUATORS_UPDATE
//!         
//! Transfers [1] --> [0] when it is new.... 
//!
//! \param[in,out]  none
//! \return         none
// -----------------------------------------------------------------------------------
// DATA[1]: ACQUIRED (RECEIVED FROM SENSOR)
// DATA[0]: INFORMED (SEND TO MB)
// -----------------------------------------------------------------------------------
#ifndef INTER_INTEGRATION_201103
void EXECUTE_SENSORS_ACTUATORS_UPDATE(){
    uint32_t channelsFlags = 0;

    // ----------------------------------------
    // RTMCB_channelsFlag_01
    // ----------------------------------------
    // SODIUM_FCI      
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_SODIUM_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_SODIUM_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_Sodium[0] = ECP1Data.TimeStamp_VAL_Sodium[1];
        ChannelsScanIntervals.Sodium_FCI[0] = ChannelsScanIntervals.Sodium_FCI[1];
        ECP1Data.Values_Sodium[0] = ECP1Data.Values_Sodium[1];
    }
    // ----------------------------------------
    // POTASSIUM_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_POTASSIUM_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_POTASSIUM_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_Potassium[0] = ECP1Data.TimeStamp_VAL_Potassium[1];
        ChannelsScanIntervals.Potassium_FCI[0] = ChannelsScanIntervals.Potassium_FCI[1];
        ECP1Data.Values_Potassium[0] = ECP1Data.Values_Potassium[1];
    }
    // ----------------------------------------
    // PHOSPHATE_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PHOSPHATE_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PHOSPHATE_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_Phosphate[0] = ECP1Data.TimeStamp_VAL_Phosphate[1];
        ChannelsScanIntervals.Phosphate_FCI[0] = ChannelsScanIntervals.Phosphate_FCI[1];
        ECP1Data.Values_Phosphate[0] = ECP1Data.Values_Phosphate[1];
    }
    // ----------------------------------------
    // PH_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PH_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PH_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_pH[0] = ECP1Data.TimeStamp_VAL_pH[1];
        ChannelsScanIntervals.pH_FCI[0] = ChannelsScanIntervals.pH_FCI[1];
        ECP1Data.Values_pH[0] = ECP1Data.Values_pH[1];
    }
    // ----------------------------------------
    // UREA_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_UREA_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_UREA_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_Urea[0] = ECP1Data.TimeStamp_VAL_Urea[1];
        ChannelsScanIntervals.Urea_FCI[0] = ChannelsScanIntervals.Urea_FCI[1];
        ECP1Data.Values_Urea[0] = ECP1Data.Values_Urea[1];
    }
    // ----------------------------------------
    // ECP_TEMP_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_ECP_TEMP_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_ECP_TEMP_FCI;
        // [1] --> [0]
        ECP1Data.TimeStamp_VAL_ECPTemperature[0] = ECP1Data.TimeStamp_VAL_ECPTemperature[1];
        ChannelsScanIntervals.ECPTemp_FCI[0] = ChannelsScanIntervals.ECPTemp_FCI[1];
        ECP1Data.Values_ECPTemperature[0] = ECP1Data.Values_ECPTemperature[1];
    }
    // ----------------------------------------
    // SODIUM_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_SODIUM_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_SODIUM_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_Sodium[0] = ECP2Data.TimeStamp_VAL_Sodium[1];
        ChannelsScanIntervals.Sodium_FCO[0] = ChannelsScanIntervals.Sodium_FCO[1];
        ECP2Data.Values_Sodium[0] = ECP2Data.Values_Sodium[1];
    }
    // ----------------------------------------
    // POTASSIUM_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_POTASSIUM_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_POTASSIUM_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_Potassium[0] = ECP2Data.TimeStamp_VAL_Potassium[1];
        ChannelsScanIntervals.Potassium_FCO[0] = ChannelsScanIntervals.Potassium_FCO[1];
        ECP2Data.Values_Potassium[0] = ECP2Data.Values_Potassium[1];
    }
    // ----------------------------------------
    // PHOSPHATE_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PHOSPHATE_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PHOSPHATE_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_Phosphate[0] = ECP2Data.TimeStamp_VAL_Phosphate[1];
        ChannelsScanIntervals.Phosphate_FCO[0] = ChannelsScanIntervals.Phosphate_FCO[1];
        ECP2Data.Values_Phosphate[0] = ECP2Data.Values_Phosphate[1];
    }
    // ----------------------------------------
    // PH_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PH_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PH_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_pH[0] = ECP2Data.TimeStamp_VAL_pH[1];
        ChannelsScanIntervals.pH_FCO[0] = ChannelsScanIntervals.pH_FCO[1];
        ECP2Data.Values_pH[0] = ECP2Data.Values_pH[1];
    }
    // ----------------------------------------
    // UREA_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_UREA_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_UREA_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_Urea[0] = ECP2Data.TimeStamp_VAL_Urea[1];
        ChannelsScanIntervals.Urea_FCO[0] = ChannelsScanIntervals.Urea_FCO[1];
        ECP2Data.Values_Urea[0] = ECP2Data.Values_Urea[1];
    }
    // ----------------------------------------
    // ECP_TEMP_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_ECP_TEMP_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_ECP_TEMP_FCO;
        // [1] --> [0]
        ECP2Data.TimeStamp_VAL_ECPTemperature[0] = ECP2Data.TimeStamp_VAL_ECPTemperature[1];
        ChannelsScanIntervals.ECPTemp_FCO[0] = ChannelsScanIntervals.ECPTemp_FCO[1];
        ECP2Data.Values_ECPTemperature[0] = ECP2Data.Values_ECPTemperature[1];
    }
    // ----------------------------------------
    // PRESSURE_BCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PRESSURE_BCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PRESSURE_BCI;
        // [1] --> [0]
        PressureBCInlet.Pressure_TimeStamp[0] = PressureBCInlet.Pressure_TimeStamp[1];
        ChannelsScanIntervals.Pressure_BCI[0] = ChannelsScanIntervals.Pressure_BCI[1];
        PressureBCInlet.Pressure_Value[0] = PressureBCInlet.Pressure_Value[1];
    }
    // ----------------------------------------
    // PRESSURE_BCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PRESSURE_BCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PRESSURE_BCO;
        // [1] --> [0]
        PressureBCOutlet.Pressure_TimeStamp[0] = PressureBCOutlet.Pressure_TimeStamp[1];
        ChannelsScanIntervals.Pressure_BCO[0] = ChannelsScanIntervals.Pressure_BCO[1];
        PressureBCOutlet.Pressure_Value[0] = PressureBCOutlet.Pressure_Value[1];
    }
    // ----------------------------------------
    // PRESSURE_FCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PRESSURE_FCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PRESSURE_FCI;
        // [1] --> [0]
        PressureFCInlet.Pressure_TimeStamp[0] = PressureFCInlet.Pressure_TimeStamp[1];
        ChannelsScanIntervals.Pressure_FCI[0] = ChannelsScanIntervals.Pressure_FCI[1];
        PressureFCInlet.Pressure_Value[0] = PressureFCInlet.Pressure_Value[1];
    }
    // ----------------------------------------
    // PRESSURE_FCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PRESSURE_FCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PRESSURE_FCO;
        // [1] --> [0]
        PressureFCOutlet.Pressure_TimeStamp[0] = PressureFCOutlet.Pressure_TimeStamp[1];
        ChannelsScanIntervals.Pressure_FCO[0] = ChannelsScanIntervals.Pressure_FCO[1];
        PressureFCOutlet.Pressure_Value[0] = PressureFCOutlet.Pressure_Value[1];
    }
    // ----------------------------------------
    // TEMPERATURE_BCI
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_TEMPERATURE_BCI;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_TEMPERATURE_BCI;
        // [1] --> [0]
        TemperatureInOut.Temperature_TimeStamp[0] = TemperatureInOut.Temperature_TimeStamp[1];
        ChannelsScanIntervals.Temperature_BCI[0] = ChannelsScanIntervals.Temperature_BCI[1];
        TemperatureInOut.TemperatureInlet_Value[0] = TemperatureInOut.TemperatureInlet_Value[1];
    }
    // ----------------------------------------
    // TEMPERATURE_BCO
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_TEMPERATURE_BCO;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_TEMPERATURE_BCO;
        // [1] --> [0]
        TemperatureInOut.Temperature_TimeStamp[0] = TemperatureInOut.Temperature_TimeStamp[1];
        ChannelsScanIntervals.Temperature_BCO[0] = ChannelsScanIntervals.Temperature_BCO[1];
        TemperatureInOut.TemperatureOutlet_Value[0] = TemperatureInOut.TemperatureOutlet_Value[1];
    }
    // ----------------------------------------
    // CONDUCTIVITY_FCR
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_CONDUCTIVITY_FCR;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_CONDUCTIVITY_FCR;
        // [1] --> [0]
        CSENS1Data.Cond_TimeStamp[0] = CSENS1Data.Cond_TimeStamp[1];
        ChannelsScanIntervals.Conductivity_FCR[0] = ChannelsScanIntervals.Conductivity_FCR[1];
        CSENS1Data.Cond_FCR[0] = CSENS1Data.Cond_FCR[1];
    }
    // ----------------------------------------
    // CONDUCTIVITY_FCQ
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_CONDUCTIVITY_FCQ;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_CONDUCTIVITY_FCQ;
        // [1] --> [0]
        CSENS1Data.Cond_TimeStamp[0] = CSENS1Data.Cond_TimeStamp[1];
        ChannelsScanIntervals.Conductivity_FCQ[0] = ChannelsScanIntervals.Conductivity_FCQ[1];
        CSENS1Data.Cond_FCQ[0] = CSENS1Data.Cond_FCQ[1];
    }
    // ----------------------------------------
    // CONDUCTIVITY_T
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_CONDUCTIVITY_T;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_CONDUCTIVITY_T;
        // [1] --> [0]
        CSENS1Data.Cond_TimeStamp[0] = CSENS1Data.Cond_TimeStamp[1];
        ChannelsScanIntervals.Conductivity_T[0] = ChannelsScanIntervals.Conductivity_T[1];
        CSENS1Data.Cond_PT1000[0] = CSENS1Data.Cond_PT1000[1];
    }
    // ----------------------------------------
    // LEAKAGE_BC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_LEAKAGE_BC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_LEAKAGE_BC;
        // [1] --> [0]
        BLDData.TimeStamp[0] = BLDData.TimeStamp[1];
        ChannelsScanIntervals.Leakage_BC[0] = ChannelsScanIntervals.Leakage_BC[1];
        BLDData.Status[0] = BLDData.Status[1];
    }
    // ----------------------------------------
    // LEAKAGE_FC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_LEAKAGE_FC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_LEAKAGE_FC;
        // [1] --> [0]
        FLDData.TimeStamp[0] = FLDData.TimeStamp[1];
        ChannelsScanIntervals.Leakage_FC[0] = ChannelsScanIntervals.Leakage_FC[1];
        FLDData.Status[0] = FLDData.Status[1];
    }
    // ----------------------------------------
    // FLOW_CALC_BC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_FLOW_CALC_BC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_FLOW_CALC_BC;
        // [1] --> [0]
        BFlowData.TimeStamp[0] = BFlowData.TimeStamp[1];
        ChannelsScanIntervals.Flow_Calc_BC[0] = ChannelsScanIntervals.Flow_Calc_BC[1];
        BFlowData.Value[0] = BFlowData.Value[1];
    }
    // ----------------------------------------
    // FLOW_CALC_FC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_FLOW_CALC_FC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_FLOW_CALC_FC;
        // [1] --> [0]
        FFlowData.TimeStamp[0] = FFlowData.TimeStamp[1];
        ChannelsScanIntervals.Flow_Calc_FC[0] = ChannelsScanIntervals.Flow_Calc_FC[1];
        FFlowData.Value[0] = FFlowData.Value[1];
    }
    // ----------------------------------------
    // PUMP_SPEED_BC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PUMP_SPEED_BC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PUMP_SPEED_BC;
        // [1] --> [0]
        BLPumpData.TimeStamp_Speed[0] = BLPumpData.TimeStamp_Speed[1];
        ChannelsScanIntervals.PumpSpeed_BC[0] = ChannelsScanIntervals.PumpSpeed_BC[1];
        BLPumpData.Speed[0] = BLPumpData.Speed[1];
    }
    // ----------------------------------------
    // PUMP_CURRENT_BC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PUMP_CURRENT_BC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PUMP_CURRENT_BC;
        // [1] --> [0]
        BLPumpData.TimeStamp_Current[0] = BLPumpData.TimeStamp_Current[1];
        ChannelsScanIntervals.PumpCurrent_BC[0] = ChannelsScanIntervals.PumpCurrent_BC[1];
        BLPumpData.Current[0] = BLPumpData.Current[1];
    }
    // ----------------------------------------
    // PUMP_SPEED_FC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PUMP_SPEED_FC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PUMP_SPEED_FC;
        // [1] --> [0]
        FLPumpData.TimeStamp_Speed[0] = FLPumpData.TimeStamp_Speed[1];
        ChannelsScanIntervals.PumpSpeed_FC[0] = ChannelsScanIntervals.PumpSpeed_FC[1];
        FLPumpData.Speed[0] = FLPumpData.Speed[1];
    }
    // ----------------------------------------
    // PUMP_CURRENT_FC
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_PUMP_CURRENT_FC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_PUMP_CURRENT_FC;
        // [1] --> [0]
        FLPumpData.TimeStamp_Current[0] = FLPumpData.TimeStamp_Current[1];
        ChannelsScanIntervals.PumpCurrent_FC[0] = ChannelsScanIntervals.PumpCurrent_FC[1];
        FLPumpData.Current[0] = FLPumpData.Current[1];
    }
    // ----------------------------------------
    // MFSI_POSITION
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_MFSI_POSITION;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_MFSI_POSITION;
        // [1] --> [0]
        MultiSwitchIData.TimeStamp_Position[0] = MultiSwitchIData.TimeStamp_Position[1];
        ChannelsScanIntervals.Position_MFSI[0] = ChannelsScanIntervals.Position_MFSI[1];
        MultiSwitchIData.Position[0] = MultiSwitchIData.Position[1];
    }
    // ----------------------------------------
    // MFSO_POSITION
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_MFSO_POSITION;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_MFSO_POSITION;
        // [1] --> [0]
        MultiSwitchOData.TimeStamp_Position[0] = MultiSwitchOData.TimeStamp_Position[1];
        ChannelsScanIntervals.Position_MFSO[0] = ChannelsScanIntervals.Position_MFSO[1];
        MultiSwitchOData.Position[0] = MultiSwitchOData.Position[1];
    }
    // ----------------------------------------
    // MFSBL_POSITION
    channelsFlags = RTMCB_channelsFlag_01 & WRITTEN_MFSBL_POSITION;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_01 &= READOK_MFSBL_POSITION;
        // [1] --> [0]
        MultiSwitchBLData.TimeStamp_Position[0] = MultiSwitchBLData.TimeStamp_Position[1];
        ChannelsScanIntervals.Position_MFSBL[0] = ChannelsScanIntervals.Position_MFSBL[1];
        MultiSwitchBLData.Position[0] = MultiSwitchBLData.Position[1];
    }
    // ----------------------------------------
    // RTMCB_channelsFlag_02
    // ----------------------------------------
    // POLAR_VOLTAGE
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_POLAR_VOLTAGE;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_POLAR_VOLTAGE;
        // [1] --> [0]
        POLARData.TimeStamp_Voltage[0] = POLARData.TimeStamp_Voltage[1];
        ChannelsScanIntervals.Voltage_POLAR[0] = ChannelsScanIntervals.Voltage_POLAR[1];
        POLARData.Voltage[0] = POLARData.Voltage[1];
    }
    // ----------------------------------------
    // POLAR_DIRECTION
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_POLAR_DIRECTION;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_POLAR_DIRECTION;
        // [1] --> [0]
        POLARData.TimeStamp_Direction[0] = POLARData.TimeStamp_Direction[1];
        ChannelsScanIntervals.Direction_POLAR[0] = ChannelsScanIntervals.Direction_POLAR[1];
        POLARData.Direction[0] = POLARData.Direction[1];
    }
    // ----------------------------------------
    // ECP1_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_ECP1_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_ECP1_STATUS;
        // [1] --> [0]
        ECP1Data.TimeStamp_Status[0] = ECP1Data.TimeStamp_Status[1];
        ChannelsScanIntervals.ECP1_Status[0] = ChannelsScanIntervals.ECP1_Status[1];
        ECP1Data.Status[0] = ECP1Data.Status[1];
    }
    // ----------------------------------------
    // ECP2_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_ECP2_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_ECP2_STATUS;
        // [1] --> [0]
        ECP2Data.TimeStamp_Status[0] = ECP2Data.TimeStamp_Status[1];
        ChannelsScanIntervals.ECP2_Status[0] = ChannelsScanIntervals.ECP2_Status[1];
        ECP2Data.Status[0] = ECP2Data.Status[1];
    }
    // ----------------------------------------
    // BPSI_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_BPSI_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_BPSI_STATUS;
        // [1] --> [0]
        PressureBCInlet.TimeStamp_Status[0] = PressureBCInlet.TimeStamp_Status[1];
        ChannelsScanIntervals.BPSI_Status[0] = ChannelsScanIntervals.BPSI_Status[1];
        PressureBCInlet.Status[0] = PressureBCInlet.Status[1];
    }
    // ----------------------------------------
    // BPSO_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_BPSO_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_BPSO_STATUS;
        // [1] --> [0]
        PressureBCOutlet.TimeStamp_Status[0] = PressureBCOutlet.TimeStamp_Status[1];
        ChannelsScanIntervals.BPSO_Status[0] = ChannelsScanIntervals.BPSO_Status[1];
        PressureBCOutlet.Status[0] = PressureBCOutlet.Status[1];
    }
    // ----------------------------------------
    // FPSI_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_FPSI_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_FPSI_STATUS;
        // [1] --> [0]
        PressureFCInlet.TimeStamp_Status[0] = PressureFCInlet.TimeStamp_Status[1];
        ChannelsScanIntervals.FPSI_Status[0] = ChannelsScanIntervals.FPSI_Status[1];
        PressureFCInlet.Status[0] = PressureFCInlet.Status[1];
    }
    // ----------------------------------------
    // FPSO_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_FPSO_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_FPSO_STATUS;
        // [1] --> [0]
        PressureFCOutlet.TimeStamp_Status[0] = PressureFCOutlet.TimeStamp_Status[1];
        ChannelsScanIntervals.FPSO_Status[0] = ChannelsScanIntervals.FPSO_Status[1];
        PressureFCOutlet.Status[0] = PressureFCOutlet.Status[1];
    }
    // ----------------------------------------
    // BTS_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_BTS_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_BTS_STATUS;
        // [1] --> [0]
        TemperatureInOut.TimeStamp_Status[0] = TemperatureInOut.TimeStamp_Status[1];
        ChannelsScanIntervals.BTS_Status[0] = ChannelsScanIntervals.BTS_Status[1];
        TemperatureInOut.Status[0] = TemperatureInOut.Status[1];
    }
    // ----------------------------------------
    // BLD_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_BLD_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_BLD_STATUS;
        // [1] --> [0]
        BLDData.TimeStamp[0] = BLDData.TimeStamp[1];
        ChannelsScanIntervals.BLD_Status[0] = ChannelsScanIntervals.BLD_Status[1];
        BLDData.Status[0] = BLDData.Status[1];
    }
    // ----------------------------------------
    // FLD_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_FLD_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_FLD_STATUS;
        // [1] --> [0]
        FLDData.TimeStamp[0] = FLDData.TimeStamp[1];
        ChannelsScanIntervals.FLD_Status[0] = ChannelsScanIntervals.FLD_Status[1];
        FLDData.Status[0] = FLDData.Status[1];
    }
    // ----------------------------------------
    // BLPUMP_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_BLPUMP_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_BLPUMP_STATUS;
        // [1] --> [0]
        BLPumpData.TimeStamp_Status[0] = BLPumpData.TimeStamp_Status[1];
        ChannelsScanIntervals.BLPump_Status[0] = ChannelsScanIntervals.BLPump_Status[1];
        BLPumpData.Status[0] = BLPumpData.Status[1];
    }
    // ----------------------------------------
    // FLPUMP_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_FLPUMP_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_FLPUMP_STATUS;
        // [1] --> [0]
        FLPumpData.TimeStamp_Status[0] = FLPumpData.TimeStamp_Status[1];
        ChannelsScanIntervals.FLPump_Status[0] = ChannelsScanIntervals.FLPump_Status[1];
        FLPumpData.Status[0] = FLPumpData.Status[1];
    }
    // ----------------------------------------
    // MFSI_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_MFSI_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_MFSI_STATUS;
        // [1] --> [0]
        MultiSwitchIData.TimeStamp_Status[0] = MultiSwitchIData.TimeStamp_Status[1];
        ChannelsScanIntervals.MFSI_Status[0] = ChannelsScanIntervals.MFSI_Status[1];
        MultiSwitchIData.Status[0] = MultiSwitchIData.Status[1];
    }
    // ----------------------------------------
    // MFSO_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_MFSO_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_MFSO_STATUS;
        // [1] --> [0]
        MultiSwitchOData.TimeStamp_Status[0] = MultiSwitchOData.TimeStamp_Status[1];
        ChannelsScanIntervals.MFSO_Status[0] = ChannelsScanIntervals.MFSO_Status[1];
        MultiSwitchOData.Status[0] = MultiSwitchOData.Status[1];
    }
    // ----------------------------------------
    // MFSBL_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_MFSBL_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_MFSBL_STATUS;
        // [1] --> [0]
        MultiSwitchBLData.TimeStamp_Status[0] = MultiSwitchBLData.TimeStamp_Status[1];
        ChannelsScanIntervals.MFSBL_Status[0] = ChannelsScanIntervals.MFSBL_Status[1];
        MultiSwitchBLData.Status[0] = MultiSwitchBLData.Status[1];
    }
    // ----------------------------------------
    // DCS_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_DCS_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_DCS_STATUS;
        // [1] --> [0]
        CSENS1Data.TimeStamp_Status[0] = CSENS1Data.TimeStamp_Status[1];
        ChannelsScanIntervals.DCS_Status[0] = ChannelsScanIntervals.DCS_Status[1];
        CSENS1Data.Status[0] = CSENS1Data.Status[1];
    }
    // ----------------------------------------
    // POLAR_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_POLAR_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_POLAR_STATUS;
        // [1] --> [0]
        POLARData.TimeStamp_Status[0] = POLARData.TimeStamp_Status[1];
        ChannelsScanIntervals.POLAR_Status[0] = ChannelsScanIntervals.POLAR_Status[1];
        POLARData.Status[0] = POLARData.Status[1];
    }
    // ----------------------------------------
    // ALARMS_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_ALARMS_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_ALARMS_STATUS;
        // [1] --> [0]
        AlarmsData.TimeStamp[0] = AlarmsData.TimeStamp[1];
        ChannelsScanIntervals.ALARMS_Status[0] = ChannelsScanIntervals.ALARMS_Status[1];
        AlarmsData.Status[0] = AlarmsData.Status[1];
    }
    // ----------------------------------------
    // RTMCB_STATUS
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_RTMCB_STATUS;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_RTMCB_STATUS;
        // [1] --> [0]
        RTMCBInformation.TimeStamp[0] = RTMCBInformation.TimeStamp[1];
        ChannelsScanIntervals.RTMCB_Status[0] = ChannelsScanIntervals.RTMCB_Status[1];
        RTMCBInformation.Status[0] = RTMCBInformation.Status[1];
    }
    // ----------------------------------------
    // BLPUMP SPEEDCODE
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_PUMP_SPEEDCODE_BC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_PUMP_SPEEDCODE_BC;
        // [1] --> [0]
        BLPumpData.TimeStamp_Speed[0] = BLPumpData.TimeStamp_Speed[1];        
        BLPumpData.SpeedReferenceCode[0] = BLPumpData.SpeedReferenceCode[1];
    }
     // ----------------------------------------
    // BLPUMP SPEEDCODE
    channelsFlags = RTMCB_channelsFlag_02 & WRITTEN_PUMP_SPEEDCODE_FC;
    if(channelsFlags!=0){
        RTMCB_channelsFlag_02 &= READOK_PUMP_SPEEDCODE_FC;
        // [1] --> [0]
        FLPumpData.TimeStamp_Speed[0] = FLPumpData.TimeStamp_Speed[1];        
        FLPumpData.SpeedReferenceCode[0] = FLPumpData.SpeedReferenceCode[1];
    }
    // ----------------------------------------
    channelsFlags = 0;
}
#endif
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
