Creating function blkStruct = slblocks
  % Specify that the product should appear in the library browser
  % and be cached in its repository
  Browser.Library = 'OffisLibrary';
  Browser.Name    = 'OFFIS Library';
  blkStruct.Browser = Browser;
