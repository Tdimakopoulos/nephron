/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef WATCHDOG_H
#define WARCHDOG_H

#include "peripheral/impTypes.h"
#include "peripheral/bhm.h"
#include "peripheral/ppm.h"

/////////////////////////// Register data declaration //////////////////////////

// Here I define a data Structure that will contain all the vlaues of my registers.

typedef struct OFFIS_wd_RegisterS { 
  union { 
    Uns32 value;
  } count;
  union { 
    Uns32 value;
  } ctrl;
} OFFIS_wd_RegisterT, *OFFIS_wd_RegisterPT;

///////////////////////////////// Port handles /////////////////////////////////

typedef struct handlesS {
  void          *OFFIS_wd_sPort;
  ppmNetHandle  reset;
} handles_type, *handles_pointerType;

#endif
