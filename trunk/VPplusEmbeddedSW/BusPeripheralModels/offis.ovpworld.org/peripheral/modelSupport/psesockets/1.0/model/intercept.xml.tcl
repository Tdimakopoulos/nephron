#
# Copyright (c) 2005-2011 Imperas Software Ltd. All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS
# OF IMPERAS LTD. USE, DISCLOSURE, OR REPRODUCTION IS PROHIBITED
# EXCEPT AS MAY BE PROVIDED FOR IN A WRITTEN AGREEMENT WITH IMPERAS LTD.
#
# NAME : psesockets

imodelnewsemihostlibrary  \
                -name psesockets  \
                -vendor ovpworld.org  \
                -library modelSupport  \
                -version 1.0  \
                -imagefile model  \
                -attributetable modelAttrs

iadddocumentation  \
                -name License  \
                -text "Open Source Apache 2.0"

iadddocumentation  \
                -name Description  \
                -text "
PSE (peripheral) model access to host sockets and log files."

  imodeladdformal  \
                -name portFile  \
                -type string
  iadddocumentation  \
                -name Description  \
                -text "
If specified writes allocated portnumber to this file"


  imodeladdsupportedprocessor  \
                -name PSE   \
                -processorname pse

