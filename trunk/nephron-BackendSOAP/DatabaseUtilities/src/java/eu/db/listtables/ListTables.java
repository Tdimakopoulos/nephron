/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.db.listtables;

import eu.db.conn.dbManager;
import eu.db.functions.DeleteTable;
import eu.db.functions.ListAndSelect;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class ListTables {

    public static void main(String[] args) throws SQLException {
        dbManager pManager = new dbManager();
        pManager.Setup("lists.exodussa.com", "1527", "sun-appserv-samples", "APP", "APP");
        try {
            pManager.GetConnection();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        ListAndSelect pls = new ListAndSelect();
        pls.setpManager(pManager);
        System.out.println(pls.GetTableList("APP"));

        DeleteTable pdel = new DeleteTable();
        pdel.setpManager(pManager);
        System.out.println(pdel.DeleteTable("NEPHRONSETTINGS"));

    }
}
