/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.db.functions;

import eu.db.conn.dbManager;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author tdim
 */
public class ListAndSelect {
    
    private dbManager pManager;
            
    public String GetTableList(String Schema) throws SQLException
    {
        String szReturn="";
        DatabaseMetaData metaData;
        metaData = pManager.GetConnection().getMetaData();
        ResultSet catalogs = metaData.getCatalogs();
        ResultSet tables = metaData.getTables(null, null, null, null);
        while (tables.next()) {

            if (tables.getString(2).equalsIgnoreCase(Schema)) {
                System.out.println("Table : "+tables.getString(3));
                szReturn=szReturn+tables.getString(3)+"<br/>";
            }
        }
        return szReturn;
    }

    /**
     * @return the pManager
     */
    public dbManager getpManager() {
        return pManager;
    }

    /**
     * @param pManager the pManager to set
     */
    public void setpManager(dbManager pManager) {
        this.pManager = pManager;
    }
}
