/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.db.functions;

import eu.db.conn.dbManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author tdim
 */
public class DeleteTable {

    private dbManager pManager;

    public String DeleteTable(String TableName) {
        try {
            Statement stmt;
            stmt = getpManager().GetStatement();
            stmt.executeUpdate("DROP Table "+ TableName);
            return "Table deleted successfully...";
        } catch (SQLException s) {
            return "Table not deleted" + " " + s.getMessage();

        }
    }

    /**
     * @return the pManager
     */
    public dbManager getpManager() {
        return pManager;
    }

    /**
     * @param pManager the pManager to set
     */
    public void setpManager(dbManager pManager) {
        this.pManager = pManager;
    }
}
