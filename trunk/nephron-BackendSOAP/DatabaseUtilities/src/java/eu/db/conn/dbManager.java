/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.db.conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author tdim
 */
public class dbManager {

    private String hostName = null;
    private String portNo = null;
    private String DBName = null;
    private String username = null;
    private String password = null;
    Connection pConnection = null;

    public void Setup(
            String hostName,
            String portNo, String db, String user, String pass) {

        this.hostName = hostName;
        this.portNo = portNo;
        DBName = db;
        username = user;
        password = pass;
    }

    public Connection GetConnection() throws SQLException {
        if (pConnection == null) {
            pConnection = DriverManager.getConnection("jdbc:derby://" + hostName + ":" + portNo + "/" + DBName, username, password);
        }
        return pConnection;
    }
    public Statement GetStatement() throws SQLException
    {
        return pConnection.createStatement();
    }
}
