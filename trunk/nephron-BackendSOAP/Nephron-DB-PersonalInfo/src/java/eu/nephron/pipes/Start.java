/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.pipes;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
 
public class Start
{
 
    public static void main(String... args)
    {
        new Start().go();
    }
 
    public void go()
    {
        try
        {
            long past = System.currentTimeMillis();
            int BUFFER = 2048;
 
            PipedInputStream convertPipe = new PipedInputStream(BUFFER);
            PipedOutputStream dataPipe = new PipedOutputStream(convertPipe);
 
            PipedInputStream convertedWordPipe = new PipedInputStream(BUFFER);
            PipedOutputStream outputPipe = new PipedOutputStream(convertedWordPipe);
 
            DataSource dataSource = new DataSource(dataPipe);
            WordWatcher pipe = new WordWatcher(convertPipe,outputPipe);
            DataConsumer dataConsumer = new DataConsumer(convertedWordPipe);
 
            Thread producerThread = new Thread(dataSource);
            Thread pipeThread = new Thread(pipe);
            Thread consumerThread = new Thread(dataConsumer);
 
            producerThread.start();
            pipeThread.start();
            consumerThread.start();
 
            consumerThread.join();
 
            long now = System.currentTimeMillis();
            System.out.println("Time taken: " + (now-past) + " ms");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
