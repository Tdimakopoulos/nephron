/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.entity.ContactInfo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class ContactInfoFacade extends AbstractFacade<ContactInfo> {
    @PersistenceContext(unitName = "nephron-personal")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContactInfoFacade() {
        super(ContactInfo.class);
    }
    
}
