/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.address.Address;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@Stateless
public class AddressFacade extends AbstractFacade<Address> {
    @PersistenceContext(unitName = "nephron-personal")
    private EntityManager em;

    @PersistenceUnit(unitName = "nephron-personal")
    private EntityManagerFactory emf;
    //declare a transaction manager
    //transactions are used when we do add or edit
    @Resource
    private UserTransaction utx;
    
    @Override
    protected EntityManager getEntityManager() {
        EntityManager em = null;
        
        em = emf.createEntityManager();
        return em;
    }

    
    public UserTransaction getTransactionManager() {
        return utx;
    }
    public AddressFacade() {
        super(Address.class);
    }
    
    
}
