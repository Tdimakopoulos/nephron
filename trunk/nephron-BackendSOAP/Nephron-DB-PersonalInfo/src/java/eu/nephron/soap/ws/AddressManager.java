/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.AddressFacade;
import eu.nephron.model.address.Address;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "AddressManager")
public class AddressManager {

    @EJB
    private AddressFacade addressFacade;
    
    //@PersistenceContext 
    //private EntityManager em;

    @PersistenceUnit(unitName = "nephron-personal")
    private EntityManagerFactory emf;
    //declare a transaction manager
    //transactions are used when we do add or edit
    @Resource
    private UserTransaction utx;
    
    public String MakeNewAddress(String szCountryCode, String szCity, String szStreet, String szStreetNo, String szPostCode, String AdministrativeArea,String szFloor,String szName) {
         EntityManager em2 = null;
        em2 = emf.createEntityManager();
        if(!em2.isOpen())
            {
            return "Entity manager is closed";
        }
        if (em2==null)
        {
            return "Entity manager is null";
        }
        Address address = new Address();
        address.setCode(szCountryCode);
        address.setCity(szCity);
        address.setStreet(szStreet);
        address.setStreetNo(szStreetNo);
        address.setPostCode(szPostCode);
        address.setAdministrativeArea(AdministrativeArea);
        address.setFloor(szFloor);
        address.setName(szName);
        try {
            utx.begin();
            em2.persist(address);
            utx.commit();
            return "No Errors";
        } catch (Exception e) {

            return "Errors on Persist";
        }
    }
    
    @WebMethod(operationName = "CreateAddressInfo")
    public String CreateAddressInfo(String szCountryCode, String szCity, String szStreet, String szStreetNo, String szPostCode, String AdministrativeArea,String szFloor,String szName) {
        Address address = new Address();
        address.setCode(szCountryCode);
        address.setCity(szCity);
        address.setStreet(szStreet);
        address.setStreetNo(szStreetNo);
        address.setPostCode(szPostCode);
        address.setAdministrativeArea(AdministrativeArea);
        address.setFloor(szFloor);
        address.setName(szName);
        addressFacade.create(address);
        return address.getId().toString();
    }
    
     public List<Address> FindAddressAll()
    {
        return addressFacade.findAll();
    }
    
    public List<Address> FindAddressByID(Long iID)
    {
           EntityManager em2 = null;
        em2 = emf.createEntityManager();
        return em2.createNamedQuery("Address.findbyID").setParameter("id",iID).getResultList();
    }
    
    public void EditAddress(Address Entity)
    {
        addressFacade.edit(Entity);
    }
    public void RemoveAddress(Address Entity)
    {
        addressFacade.remove(Entity);
    }
}
