/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.ContactInfoFacade;
import eu.nephron.model.address.Address;
import eu.nephron.model.entity.ContactInfo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ContactInfoManager")
public class ContactInfoManager {

    @EJB
    private ContactInfoFacade contactInfoFacade;

    @PersistenceContext 
    private EntityManager em;
    
    @WebMethod(operationName = "CreateContactInfo")
    public String CreateContactInfo(String szPhone,String szMobile,String szEmail,String szCPhone,String szCMobile,String szCEmail) {
        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setPhone(szPhone);
        contactInfo.setMobile(szMobile);
        contactInfo.setEmail(szEmail);
        contactInfo.setCompanyPhone(szCPhone);
        contactInfo.setCompanyMobile(szCMobile);
        contactInfo.setCompanyEmail(szCEmail);
        contactInfoFacade.create(contactInfo);
        return contactInfo.getId().toString();
    }
    
    public List<ContactInfo> FindContactInfoAll()
    {
        return contactInfoFacade.findAll();
    }
    
    public List<ContactInfo> FindContactInfoByID(Long iID)
    {
        return em.createNamedQuery("ContactInfo.findbyID").setParameter("id",iID).getResultList();
    }
    public void EditContactInfo(ContactInfo Entity)
    {
        contactInfoFacade.edit(Entity);
    }
    public void RemoveContactInfo(ContactInfo Entity)
    {
        contactInfoFacade.remove(Entity);
    }
}
