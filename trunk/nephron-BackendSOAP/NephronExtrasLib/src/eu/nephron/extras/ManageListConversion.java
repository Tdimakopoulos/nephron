/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.extras;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tdim
 */
public class ManageListConversion {

    public String ListToStringScheduleActAndTask(ArrayList<ScheduleActAndTask> pValues) {
        String dRet = "";
        for (int i = 0; i < pValues.size(); i++) {
            dRet = dRet + pValues.get(i).getId() + "<NE>";
            dRet = dRet + pValues.get(i).getImei() + "<NE>";
            dRet = dRet + pValues.get(i).getSdesc() + "<NE>";
            dRet = dRet + pValues.get(i).getText1() + "<NE>";
            dRet = dRet + pValues.get(i).getText2() + "<NE>";
            dRet = dRet + pValues.get(i).getDateap() + "<NE>";
            dRet = dRet + pValues.get(i).getDoctorid() + "<NE>";
            dRet = dRet + pValues.get(i).getDurationhours() + "<NE>";
            dRet = dRet + pValues.get(i).getI1() + "<NE>";
            dRet = dRet + pValues.get(i).getI2() + "<NE>";
            dRet = dRet + pValues.get(i).getItype() + "<NE>";
            dRet = dRet + pValues.get(i).getLl1() + "<NE>";
            dRet = dRet + pValues.get(i).getLl2() + "<NE>";
            dRet = dRet + pValues.get(i).getPatientid() + "<NE>";
            dRet = dRet + "<br>";
        }
        return dRet;
    }

    public ArrayList<ScheduleActAndTask> StringToListScheduleActAndTask(String pValues) {
        ArrayList<ScheduleActAndTask> pList = new ArrayList<ScheduleActAndTask>();

        String[] pStr = pValues.split("<br>");

        for (int i = 0; i < pStr.length; i++) {
            String[] pResutls = pStr[i].split("<NE>");
            System.out.println(" Position :" + i + "Value" + pStr[i]);
            ScheduleActAndTask pItem = new ScheduleActAndTask();

            pItem.setId(Long.parseLong(pResutls[0]));
            pItem.setImei(pResutls[1]);
            pItem.setSdesc(pResutls[2]);
            pItem.setText1(pResutls[3]);
            pItem.setText2(pResutls[4]);
            pItem.setDateap(Long.parseLong(pResutls[5]));
            pItem.setDoctorid(Long.parseLong(pResutls[6]));
            pItem.setDurationhours(Long.parseLong(pResutls[7]));
            pItem.setI1(Integer.parseInt(pResutls[8]));
            pItem.setI2(Integer.parseInt(pResutls[9]));
            pItem.setItype(Integer.parseInt(pResutls[10]));
            pItem.setLl1(Long.parseLong(pResutls[11]));
            pItem.setLl2(Long.parseLong(pResutls[12]));
            pItem.setPatientid(Long.parseLong(pResutls[13]));
            pList.add(pItem);
        }

        return pList;
    }

    public static void main(String[] args) {
        ManageListConversion pp = new ManageListConversion();
        ArrayList<ScheduleActAndTask> pList = new ArrayList<ScheduleActAndTask>();
        ScheduleActAndTask pItem1 = new ScheduleActAndTask();
        pItem1.setDateap(Long.MIN_VALUE);
        pItem1.setDoctorid(Long.MIN_VALUE);
        pItem1.setDurationhours(Long.MIN_VALUE);
        pItem1.setI1(0);
        pItem1.setI2(0);
        pItem1.setId(Long.MIN_VALUE);
        pItem1.setImei("PHONE1");
        pItem1.setItype(1);
        pItem1.setLl1(Long.MIN_VALUE);
        pItem1.setLl2(Long.MIN_VALUE);
        pItem1.setPatientid(Long.MIN_VALUE);
        pItem1.setSdesc("APP 1");
        pItem1.setText1("ttt---11");
        pItem1.setText2("APP1 --11");
        pList.add(pItem1);


        ScheduleActAndTask pItem2 = new ScheduleActAndTask();
        pItem2.setDateap(Long.MIN_VALUE);
        pItem2.setDoctorid(Long.MIN_VALUE);
        pItem2.setDurationhours(Long.MIN_VALUE);
        pItem2.setI1(0);
        pItem2.setI2(0);
        pItem2.setId(Long.MIN_VALUE);
        pItem2.setImei("PHONE2");
        pItem2.setItype(1);
        pItem2.setLl1(Long.MIN_VALUE);
        pItem2.setLl2(Long.MIN_VALUE);
        pItem2.setPatientid(Long.MIN_VALUE);
        pItem2.setSdesc("APP 2");
        pItem2.setText1("ttt---22");
        pItem2.setText2("APP1 --22");
        pList.add(pItem2);

        System.out.println(pp.ListToStringScheduleActAndTask(pList));

        ArrayList<ScheduleActAndTask> pList2 = pp.StringToListScheduleActAndTask(pp.ListToStringScheduleActAndTask(pList));
        System.out.println("Size:"+pList2.size());
        System.out.println("Imei1:"+pList2.get(0).getImei());
        System.out.println("Imei2:"+pList2.get(1).getImei());
    }
}
