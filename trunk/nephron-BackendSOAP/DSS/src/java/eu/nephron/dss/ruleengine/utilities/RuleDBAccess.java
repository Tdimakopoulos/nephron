/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.ruleengine.utilities;

/**
 *
 * @author tdim
 */
public class RuleDBAccess {

    public static java.util.List<eu.nephron.dsshelper.ws.WakdAlerts> selectAlertsByIMEI(java.lang.String imei) {
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service service = new eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service();
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper port = service.getSmartPhoneDSSHelperPort();
        return port.selectAlertsByIMEI(imei);
    }

    public static java.util.List<eu.nephron.dsshelper.ws.WakdMeasurments> selectMeasurmentsByIMEI(java.lang.String imei) {
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service service = new eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service();
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper port = service.getSmartPhoneDSSHelperPort();
        return port.selectMeasurmentsByIMEI(imei);
    }

    public static java.util.List<eu.nephron.dsshelper.ws.QuestionaryTable> selectQuastionaryByIMEI(java.lang.String imei) {
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service service = new eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service();
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper port = service.getSmartPhoneDSSHelperPort();
        return port.selectQuastionaryByIMEI(imei);
    }

    public static java.util.List<eu.nephron.dsshelper.ws.WakdStatus> selectWakdStatusByIMEI(java.lang.String imei) {
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service service = new eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service();
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper port = service.getSmartPhoneDSSHelperPort();
        return port.selectWakdStatusByIMEI(imei);
    }

    public static java.util.List<eu.nephron.dsshelper.ws.WeightMeasurments> selectWeightMeasurmentsByIMEI(java.lang.String imei) {
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service service = new eu.nephron.dsshelper.ws.SmartPhoneDSSHelper_Service();
        eu.nephron.dsshelper.ws.SmartPhoneDSSHelper port = service.getSmartPhoneDSSHelperPort();
        return port.selectWeightMeasurmentsByIMEI(imei);
    }
    
    
}
