package eu.nephron.dss.rules.profile;

import eu.nephron.dss.eventhandler.bean.DSSEventHandlerBean.RuleEngineType;

public class ProfileData {

	private String szProfileName;
	public String getSzProfileName() {
		return szProfileName;
	}
	public void setSzProfileName(String szProfileName) {
		this.szProfileName = szProfileName;
	}
	public RuleEngineType getSzProfileType() {
		return szProfileType;
	}
	public void setSzProfileType(RuleEngineType szProfileType) {
		this.szProfileType = szProfileType;
	}
	private RuleEngineType szProfileType;
	
}
