/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.ws;

import eu.nephron.dss.db.DSSRuleResults;
import eu.nephron.dss.ruleengine.controller.bean.DSSControllerBean;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DSSWSManager")
public class DSSWSManager {
   

    @WebMethod(operationName = "DSS-InitializeRepo")
    @Oneway
    public void dssInitializeRepo()
    {
      DSSControllerBean pController= new DSSControllerBean();
      pController.DeleteRepository();
      pController.ReinitializeRepository();
    }
    
    @WebMethod(operationName = "DSS-LocalRepo")
    @Oneway
    public void dssLocalRepo()
    {
      DSSControllerBean pController= new DSSControllerBean();
      
      pController.SetToLocal();
    }
    
    @WebMethod(operationName = "DSS-RemoteRepo")
    @Oneway
    public void dssRemoteRepo()
    {
      DSSControllerBean pController= new DSSControllerBean();
      
      pController.SetToLive();
    }
    
    
    
}
