package eu.nephron.dss.ruleengine.controller.bean;

import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
//import eu.nephron.dss.eventhandler.bean.WAKDMeasurmentHandlerBean;
import eu.nephron.dss.ruleengine.core.RuleEngineGuvnorAssets;
import eu.nephron.dss.ruleengine.repository.RepositoryController;
import eu.nephron.dss.rules.profile.ProfileManager;




public class DSSControllerBean  {
	
	public void ReinitializeRepository()
	{
                ProfileManager pManager=new ProfileManager();
		RuleEngineGuvnorAssets dAssets= new RuleEngineGuvnorAssets();
		String[] rules=dAssets.PopulateLocalRepositoryForCategory(pManager.GetProfileNameForSensors().getSzProfileName());
                String[] rules1=dAssets.PopulateLocalRepositoryForCategory(pManager.GetProfileNameForStatistics().getSzProfileName());
		String[] rules2=dAssets.PopulateLocalRepositoryForPackage(pManager.GetPackageNameForNephron());
	}
	
	public void SetToLocal()
	{
		RepositoryController pController = new RepositoryController();
		pController.SetRepositoryToLocal();
	}
	
	public void SetToLive()
	{
		RepositoryController pController = new RepositoryController();
		pController.SetRepositoryToGuvnor();
	}
	
	public void DeleteRepository()
	{
		RepositoryController pController = new RepositoryController();
		pController.DeleteRepository();
	}

}
