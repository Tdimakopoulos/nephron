package eu.nephron.dss.rules.profile;

import eu.nephron.dss.eventhandler.bean.DSSEventHandlerBean.RuleEngineType;

public class ProfileManager {

	public ProfileData GetProfileNameForSensors() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Nephron.Sensors");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);

		return pReturn;
	}
        
        public ProfileData GetProfileNameForStatistics() {
		ProfileData pReturn = new ProfileData();

		pReturn.setSzProfileName("Nephron.Statistics");
		pReturn.setSzProfileType(RuleEngineType.DRL_RULES);

		return pReturn;
	}
	
        //Nephron.Questionnaires
        //Nephron.Weight
        //Nephron.SleepCycle
        //Nephron.Medication
                
	public String GetPackageNameForNephron() {
		
		return "Nephron";
	}
}
