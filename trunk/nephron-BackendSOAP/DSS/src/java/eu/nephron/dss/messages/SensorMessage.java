/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.messages;

/**
 *
 * @author tdim
 */
public class SensorMessage {

    private String IMEI;
    private Long patiendid;
    private Long doctorid;
    private Long nurseid;
    private Long mobileid;
    private Long wakdid;
    private String szvalue;
    private Long lvalue;
    private float fvalue;
    private Double dvalue;
    private int itype;
    private String sztype;
    private String wakdstatus;
    private int wakdstatus1;
    private int wakdstatus2;
    private int wakdstatus3;
    private int wakdstatus4;
    private int wakdstatus5;
    private int wakdstatus6;
    private int wakdstatus7;
    private int wakdstatus8;

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the patiendid
     */
    public Long getPatiendid() {
        return patiendid;
    }

    /**
     * @param patiendid the patiendid to set
     */
    public void setPatiendid(Long patiendid) {
        this.patiendid = patiendid;
    }

    /**
     * @return the doctorid
     */
    public Long getDoctorid() {
        return doctorid;
    }

    /**
     * @param doctorid the doctorid to set
     */
    public void setDoctorid(Long doctorid) {
        this.doctorid = doctorid;
    }

    /**
     * @return the nurseid
     */
    public Long getNurseid() {
        return nurseid;
    }

    /**
     * @param nurseid the nurseid to set
     */
    public void setNurseid(Long nurseid) {
        this.nurseid = nurseid;
    }

    /**
     * @return the mobileid
     */
    public Long getMobileid() {
        return mobileid;
    }

    /**
     * @param mobileid the mobileid to set
     */
    public void setMobileid(Long mobileid) {
        this.mobileid = mobileid;
    }

    /**
     * @return the wakdid
     */
    public Long getWakdid() {
        return wakdid;
    }

    /**
     * @param wakdid the wakdid to set
     */
    public void setWakdid(Long wakdid) {
        this.wakdid = wakdid;
    }

    /**
     * @return the szvalue
     */
    public String getSzvalue() {
        return szvalue;
    }

    /**
     * @param szvalue the szvalue to set
     */
    public void setSzvalue(String szvalue) {
        this.szvalue = szvalue;
    }

    /**
     * @return the lvalue
     */
    public Long getLvalue() {
        return lvalue;
    }

    /**
     * @param lvalue the lvalue to set
     */
    public void setLvalue(Long lvalue) {
        this.lvalue = lvalue;
    }

    /**
     * @return the fvalue
     */
    public float getFvalue() {
        return fvalue;
    }

    /**
     * @param fvalue the fvalue to set
     */
    public void setFvalue(float fvalue) {
        this.fvalue = fvalue;
    }

    /**
     * @return the dvalue
     */
    public Double getDvalue() {
        return dvalue;
    }

    /**
     * @param dvalue the dvalue to set
     */
    public void setDvalue(Double dvalue) {
        this.dvalue = dvalue;
    }

    /**
     * @return the itype
     */
    public int getItype() {
        return itype;
    }

    /**
     * @param itype the itype to set
     */
    public void setItype(int itype) {
        this.itype = itype;
    }

    /**
     * @return the sztype
     */
    public String getSztype() {
        return sztype;
    }

    /**
     * @param sztype the sztype to set
     */
    public void setSztype(String sztype) {
        this.sztype = sztype;
    }

    /**
     * @return the wakdstatus
     */
    public String getWakdstatus() {
        return wakdstatus;
    }

    /**
     * @param wakdstatus the wakdstatus to set
     */
    public void setWakdstatus(String wakdstatus) {
        this.wakdstatus = wakdstatus;
    }

    /**
     * @return the wakdstatus1
     */
    public int getWakdstatus1() {
        return wakdstatus1;
    }

    /**
     * @param wakdstatus1 the wakdstatus1 to set
     */
    public void setWakdstatus1(int wakdstatus1) {
        this.wakdstatus1 = wakdstatus1;
    }

    /**
     * @return the wakdstatus2
     */
    public int getWakdstatus2() {
        return wakdstatus2;
    }

    /**
     * @param wakdstatus2 the wakdstatus2 to set
     */
    public void setWakdstatus2(int wakdstatus2) {
        this.wakdstatus2 = wakdstatus2;
    }

    /**
     * @return the wakdstatus3
     */
    public int getWakdstatus3() {
        return wakdstatus3;
    }

    /**
     * @param wakdstatus3 the wakdstatus3 to set
     */
    public void setWakdstatus3(int wakdstatus3) {
        this.wakdstatus3 = wakdstatus3;
    }

    /**
     * @return the wakdstatus4
     */
    public int getWakdstatus4() {
        return wakdstatus4;
    }

    /**
     * @param wakdstatus4 the wakdstatus4 to set
     */
    public void setWakdstatus4(int wakdstatus4) {
        this.wakdstatus4 = wakdstatus4;
    }

    /**
     * @return the wakdstatus5
     */
    public int getWakdstatus5() {
        return wakdstatus5;
    }

    /**
     * @param wakdstatus5 the wakdstatus5 to set
     */
    public void setWakdstatus5(int wakdstatus5) {
        this.wakdstatus5 = wakdstatus5;
    }

    /**
     * @return the wakdstatus6
     */
    public int getWakdstatus6() {
        return wakdstatus6;
    }

    /**
     * @param wakdstatus6 the wakdstatus6 to set
     */
    public void setWakdstatus6(int wakdstatus6) {
        this.wakdstatus6 = wakdstatus6;
    }

    /**
     * @return the wakdstatus7
     */
    public int getWakdstatus7() {
        return wakdstatus7;
    }

    /**
     * @param wakdstatus7 the wakdstatus7 to set
     */
    public void setWakdstatus7(int wakdstatus7) {
        this.wakdstatus7 = wakdstatus7;
    }

    /**
     * @return the wakdstatus8
     */
    public int getWakdstatus8() {
        return wakdstatus8;
    }

    /**
     * @param wakdstatus8 the wakdstatus8 to set
     */
    public void setWakdstatus8(int wakdstatus8) {
        this.wakdstatus8 = wakdstatus8;
    }
    
    
}
