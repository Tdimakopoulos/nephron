/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.messages;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tdim
 */
public class SensorMessages {

    private List<SensorMessage> pMessages = new ArrayList<SensorMessage>();

    /**
     * @return the pMessages
     */
    public List<SensorMessage> getpMessages() {
        return pMessages;
    }

    /**
     * @param pMessages the pMessages to set
     */
    public void setpMessages(List<SensorMessage> pMessages) {
        this.pMessages = pMessages;
    }
    
}
