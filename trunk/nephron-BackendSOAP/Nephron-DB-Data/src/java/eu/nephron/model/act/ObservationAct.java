package eu.nephron.model.act;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="OBSERVATIONACT")
public class ObservationAct{
	
	private static final long serialVersionUID = 1692777844218154602L;
	
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="OBSERVATIONACT_ID")
	private Long id;
	
	@Column(name="OBSERVATIONACT_STATUS", nullable=false)
	private int actStatus;
        
	@Column(name="OBSERVATIONMOOD_CODE", nullable=false)
	private int moodCode;
        
        @Column(name="OBSERVATIONTYPE_CODE", nullable=false)
	private int typeCode;
        
	@Column(name="OBSERVATIONTEXT", length=255)
	private String text;
	
	@Column(name="OBSERVATIONACTIVITY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date activityDate;
	
        @Column(name="PATIENTID")
	private Long pID;
        
	@Column(name="LVALUE")
	private Long value;
        
        @Column(name="IVALUE")
	private int ivalue;
        
        @Column(name="FVALUE")
	private Float fvalue;
        
        @Column(name="SVALUE")
	private String svalue;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the actStatus
     */
    public int getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(int actStatus) {
        this.actStatus = actStatus;
    }

    /**
     * @return the moodCode
     */
    public int getMoodCode() {
        return moodCode;
    }

    /**
     * @param moodCode the moodCode to set
     */
    public void setMoodCode(int moodCode) {
        this.moodCode = moodCode;
    }

    /**
     * @return the typeCode
     */
    public int getTypeCode() {
        return typeCode;
    }

    /**
     * @param typeCode the typeCode to set
     */
    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the activityDate
     */
    public Date getActivityDate() {
        return activityDate;
    }

    /**
     * @param activityDate the activityDate to set
     */
    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    /**
     * @return the pID
     */
    public Long getpID() {
        return pID;
    }

    /**
     * @param pID the pID to set
     */
    public void setpID(Long pID) {
        this.pID = pID;
    }

    /**
     * @return the value
     */
    public Long getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Long value) {
        this.value = value;
    }

    /**
     * @return the ivalue
     */
    public int getIvalue() {
        return ivalue;
    }

    /**
     * @param ivalue the ivalue to set
     */
    public void setIvalue(int ivalue) {
        this.ivalue = ivalue;
    }

    /**
     * @return the fvalue
     */
    public Float getFvalue() {
        return fvalue;
    }

    /**
     * @param fvalue the fvalue to set
     */
    public void setFvalue(Float fvalue) {
        this.fvalue = fvalue;
    }

    /**
     * @return the svalue
     */
    public String getSvalue() {
        return svalue;
    }

    /**
     * @param svalue the svalue to set
     */
    public void setSvalue(String svalue) {
        this.svalue = svalue;
    }

	
	
}
