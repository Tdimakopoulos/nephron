package eu.nephron.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue("1")
@NamedQueries({
	@NamedQuery(name="Device.findWakdBySerialNumber",
		query="select d from Device d where d.role.code='WAKD' and d.serialNumber=:serialNumber"),
	@NamedQuery(name="Device.findSmartPhoneBySerialNumber",
		query="select d from Device d where d.role.code='SP' and d.serialNumber=:serialNumber")
//	@NamedQuery(name="Device.findWakdByCommandID", 
//		query="select d from CommandAct ca, Participation p, Device d where ca.id=:commandID and ca.actStatus=1 and ca.moodCode=4 and p.act.id=ca.id and p.medicalEntity.id=d.id and d.role.code='WAKD'"),
//	@NamedQuery(name="Device.findAssociatedDevice", 
//		query="select d2 from AssociationAct a, Participation p1, Participation p2, Device d1, Device d2 where p1.act.id=a.id and p2.act.id=a.id and p1.medicalEntity.id=d1.id and p2.medicalEntity.id=d2.id and d1.id<>d2.id and d1.id=:deviceID")
})
public class Device extends MedicalEntity {

	private static final long serialVersionUID = 744885745424571530L;

	@Column(name="SERIAL_NUMBER", unique=true)
	private String serialNumber;
	
	@Column(name="EXPIRATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationDate;
	
	@Column(name="MANUFACTURED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date manufacturedDate;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getManufacturedDate() {
		return manufacturedDate;
	}

	public void setManufacturedDate(Date manufacturedDate) {
		this.manufacturedDate = manufacturedDate;
	}
	
}
