package eu.nephron.model.act;

import java.util.Date;
import java.util.Set;
import javax.persistence.Column;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="SCHEDULEACT")
public class ScheduledAct {

	private static final long serialVersionUID = -5241114269822648098L;
	
        @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHEDULEACT_ID")
	private Long id;
	
	@Column(name="SCHEDULEACT_STATUS")
	//protected ActStatusEnum actStatus;
	private int actStatus;
        
	@Column(name="SCHEDULEMOOD_CODE")
	//protected MoodCodeEnum moodCode;
	private int moodCode;
        
	@Column(name="SCHEDULETEXT", length=255)
	private String text;
	
	@Column(name="SCHEDULEACTIVITY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date activityDate;
	
	@Column(name="SCHEDULEEFFECTIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date effectiveDate;
	
	@Column(name="SCHEDULEPATIENTS")
	private Long patient;
        
	

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the actStatus
     */
    public int getActStatus() {
        return actStatus;
    }

    /**
     * @param actStatus the actStatus to set
     */
    public void setActStatus(int actStatus) {
        this.actStatus = actStatus;
    }

    /**
     * @return the moodCode
     */
    public int getMoodCode() {
        return moodCode;
    }

    /**
     * @param moodCode the moodCode to set
     */
    public void setMoodCode(int moodCode) {
        this.moodCode = moodCode;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the activityDate
     */
    public Date getActivityDate() {
        return activityDate;
    }

    /**
     * @param activityDate the activityDate to set
     */
    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the patient
     */
    public Long getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(Long patient) {
        this.patient = patient;
    }
        
        
	
}
