package eu.nephron.model.alert;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import eu.nephron.model.entity.Device;

@Entity
@Table(name="ALERT")
@NamedQueries({
	@NamedQuery(name="Alert.findAllAlerts", query="select a from Alert a")
})
public class Alert {

	private static final long serialVersionUID = -3597699315082686412L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ALERT_ID")
	private Long id;
	
	@Column(name="CODE")
	private String code;
	
	@Column(name="SEVERITY")
	private Integer severity;
	
	@Column(name="ALERT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date alertDate;
	
	@ManyToOne
	@JoinColumn(name="DEVICE_ID")
	private Device device;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getSeverity() {
		return severity;
	}

	public void setSeverity(Integer severity) {
		this.severity = severity;
	}

	public Date getAlertDate() {
		return alertDate;
	}

	public void setAlertDate(Date alertDate) {
		this.alertDate = alertDate;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}
		
}
