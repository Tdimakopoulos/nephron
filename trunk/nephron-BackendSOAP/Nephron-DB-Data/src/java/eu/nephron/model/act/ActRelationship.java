package eu.nephron.model.act;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table(name="ACT_RELATIONSHIP")
//@NamedQueries({
//	@NamedQuery(name="ActRelationship.findCommandBySmartphoneID", 
//	query="select ar from " +
//			"ActRelationship ar, " +
//			"CommandAct ca1, " +
//			"CommandAct ca2, " +
//			"Participation p1, " +
//			"Participation p2, " +
//			"Device d " +
//		"where d.id=:deviceID " +
//		  "and d.role.code='SP' " +
//		  "and ar.inboundAct.id=ca1.id " +
//		  "and ar.outboundAct.id=ca2.id " +
//		  "and ca1.actStatus=1 " +
//		  "and ca2.actStatus=1 " +
//		  "and ca1.moodCode=3 " +
//		  "and ca2.moodCode=4 " +
//		  "and p1.act.id=ca1.id " +
//		  "and p2.act.id=ca2.id " +
//		  "and p1.medicalEntity.id=p2.medicalEntity.id")
//})
public class ActRelationship {

	private static final long serialVersionUID = 8822661625931934260L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACT_RELATIONSHIP_ID")
	private Long id;
	
	@Column(name="NOTES")
	private String notes;
	
	@ManyToOne
	@JoinColumn(name="INBOUND_ACT_ID", nullable=false)
	private Act inboundAct;	
	
	@ManyToOne
	@JoinColumn(name="OUTBOUND_ACT_ID", nullable=false)
	private Act outboundAct;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Act getInboundAct() {
		return inboundAct;
	}

	public void setInboundAct(Act inboundAct) {
		this.inboundAct = inboundAct;
	}

	public Act getOutboundAct() {
		return outboundAct;
	}

	public void setOutboundAct(Act outboundAct) {
		this.outboundAct = outboundAct;
	}

}
