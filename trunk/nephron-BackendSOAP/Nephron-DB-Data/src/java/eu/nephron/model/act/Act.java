package eu.nephron.model.act;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;





@Entity
@Table(name="ACT")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ACT_TYPE", discriminatorType = DiscriminatorType.INTEGER)
public  class Act  {

	private static final long serialVersionUID = 4517033537175407944L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACT_ID")
	protected Long id;
	
	@Column(name="ACT_STATUS", nullable=false)
	//protected ActStatusEnum actStatus;
	protected int actStatus;
        
	@Column(name="MOOD_CODE", nullable=false)
	//protected MoodCodeEnum moodCode;
	protected int moodCode;
        
	@Column(name="TEXT", length=255)
	protected String text;
	
	@Column(name="ACTIVITY_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date activityDate;
	
	@Column(name="EFFECTIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date effectiveDate;
	
//	@OneToMany(mappedBy="act")
//	protected Set<Participation> participations;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getActStatus() {
		return actStatus;
	}

	public void setActStatus(int actStatus) {
		this.actStatus = actStatus;
	}

	public int getMoodCode() {
		return moodCode;
	}

	public void setMoodCode(int moodCode) {
		this.moodCode = moodCode;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

//	public Set<Participation> getParticipations() {
//		return participations;
//	}
//
//	public void setParticipations(Set<Participation> participations) {
//		this.participations = participations;
//	}

}
