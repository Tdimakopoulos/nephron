package eu.nephron.model.act;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("5")
public class AssociationAct extends Act {

	private static final long serialVersionUID = -363241224782647667L;

}
