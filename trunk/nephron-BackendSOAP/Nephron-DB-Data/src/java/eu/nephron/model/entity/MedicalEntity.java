package eu.nephron.model.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import eu.nephron.model.act.Participation;
import eu.nephron.model.role.Role;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name="MEDICAL_ENTITY")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "MEDICAL_ENTITY_TYPE", discriminatorType = DiscriminatorType.INTEGER)
@NamedQueries({
	@NamedQuery(name="MedicalEntity.findAll", query="select a from MedicalEntity a"),
        @NamedQuery(name="MedicalEntity.findAllbyID", query="select a from MedicalEntity a where a.id=:id")
})
public  class MedicalEntity {

	private static final long serialVersionUID = -713984218446098753L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MEDICAL_ENTITY_ID")
	protected Long id;
	
	@Column(name="CODE")
	protected String code;
	
	@Column(name="NAME", nullable=false)
	protected String name;
	
	@Column(name="QUANTITY")
	protected Integer quantity;
	
	@Column(name="EFFECTIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date effectiveDate;
	
	@ManyToOne
	@JoinColumn(name="ROLE_ID", nullable=false)
	protected Role role;
	
        
//	@OneToMany(mappedBy="medicalEntity")
//	protected Set<Participation> participations;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

//	public Set<Participation> getParticipations() {
//                
//		return participations;
//	}
//
//	public void setParticipations(Set<Participation> participations) {
//		this.participations = participations;
//	}
	
}
