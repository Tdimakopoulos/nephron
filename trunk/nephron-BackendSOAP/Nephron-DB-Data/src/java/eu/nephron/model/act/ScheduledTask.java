package eu.nephron.model.act;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="SCHEDULED_TASK")
public class ScheduledTask {

	private static final long serialVersionUID = 3662992255773507629L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SCHEDULED_TASK_ID")
	private Long id;
	
	@Column(name="OUTCOME")
	private Integer outcome;
	
	@Column(name="SCHEDULEACT_ID")
	private Long actid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getOutcome() {
		return outcome;
	}

	public void setOutcome(Integer outcome) {
		this.outcome = outcome;
	}

    /**
     * @return the actid
     */
    public Long getActid() {
        return actid;
    }

    /**
     * @param actid the actid to set
     */
    public void setActid(Long actid) {
        this.actid = actid;
    }

	
	
}
