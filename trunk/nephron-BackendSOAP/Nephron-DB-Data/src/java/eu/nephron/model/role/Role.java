package eu.nephron.model.role;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import eu.nephron.model.entity.MedicalEntity;

@Entity
@Table(name = "ROLE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ROLE_TYPE", discriminatorType = DiscriminatorType.INTEGER)
@NamedQueries({
	@NamedQuery(name="Role.findAllRoles", query="select r from Role r"),
	@NamedQuery(name="Role.findByCode", query="select r from Role r where r.code=:code"),
	@NamedQuery(name="Role.findByName", query="select r from Role r where r.name=:name")
})
public class Role {

	private static final long serialVersionUID = 7374119357989429920L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ROLE_ID")
	protected Long id;

	@Column(name = "CODE", unique=true, nullable=false)
	protected String code;

	@Column(name = "NAME", unique=true, nullable=false)
	protected String name;

	@Column(name = "STATUS")
	protected String status;

	@Column(name = "QUANTITY")
	protected Integer quantity;

	@Column(name = "EFFECTIVE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date effectiveDate;

	@Column(name = "ADDRESS_ID")
	protected Long address;
	
//	@OneToMany(mappedBy="role")
//	private Set<MedicalEntity> players;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Long getAddress() {
		return address;
	}

	public void setAddress(Long address) {
		this.address = address;
	}

//	public Set<MedicalEntity> getPlayers() {
//		return players;
//	}
//
//	public void setPlayers(Set<MedicalEntity> players) {
//		this.players = players;
//	}

}
