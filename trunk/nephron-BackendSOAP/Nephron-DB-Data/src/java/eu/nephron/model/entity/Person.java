package eu.nephron.model.entity;

import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@DiscriminatorValue("0")
@NamedQueries({
	@NamedQuery(name="Person.findbyID", query="select p.personalInfo,p.contactInfo,p.address,p.id from Person p where p.id=:id")
//	@NamedQuery(name="Person.findPatientByLastName", query="select p from Person p where p.role.code='PTNT' and p.personalInfo.lastName=:lastName")
})
public class Person extends MedicalEntity {

	private static final long serialVersionUID = 4566288674204325343L;
	
        
	@Column(name = "PERSONALINFO_ID")
	private Long personalInfo;
	
	@Column(name = "CONTACTINFO_ID")
	private Long contactInfo;
	
	@Column(name = "ADDRESS_ID")
	private Long address;

        @Column(name="GENDER")
	protected int gender;
	
	@Column(name="BIRTH_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date birthDate;

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
        
	public Long getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(Long personalInfo) {
		this.personalInfo = personalInfo;
	}

	public Long getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(Long contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Long getAddress() {
		return address;
	}

	public void setAddress(Long address) {
		this.address = address;
	}
	
}
