package eu.nephron.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import org.hibernate.annotations.Parameter;
//import org.hibernate.annotations.Type;


public  class LivingSubject extends MedicalEntity {

	private static final long serialVersionUID = -4570973140060018575L;

	@Column(name="GENDER")
	protected int gender;
	
	@Column(name="BIRTH_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date birthDate;

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
}
