/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.RoleFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "Role")
public class Role {
    @EJB
    private RoleFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createRole")
    @Oneway
    public void createRole(@WebParam(name = "entity") eu.nephron.model.role.Role entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editRole")
    @Oneway
    public void editRole(@WebParam(name = "entity") eu.nephron.model.role.Role entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeRole")
    @Oneway
    public void removeRole(@WebParam(name = "entity") eu.nephron.model.role.Role entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findRole")
    public eu.nephron.model.role.Role findRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllRole")
    public List<eu.nephron.model.role.Role> findAllRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeRole")
    public List<eu.nephron.model.role.Role> findRangeRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countRole")
    public int countRole() {
        return ejbRef.count();
    }
    
}
