/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.PatientFacade;
import eu.nephron.model.role.Patient;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "PatientRole")
public class PatientRole {
    @EJB
    private PatientFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createPatientRole")
    @Oneway
    public void createPatientRole(@WebParam(name = "entity") Patient entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editPatientRole")
    @Oneway
    public void editPatientRole(@WebParam(name = "entity") Patient entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removePatientRole")
    @Oneway
    public void removePatientRole(@WebParam(name = "entity") Patient entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findPatientRole")
    public Patient findPatientRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllPatientRole")
    public List<Patient> findAllPatientRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangePatientRole")
    public List<Patient> findRangePatientRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countPatientRole")
    public int countPatientRole() {
        return ejbRef.count();
    }
    
}
