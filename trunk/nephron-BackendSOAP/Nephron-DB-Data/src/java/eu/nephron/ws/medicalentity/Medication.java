/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 */
package eu.nephron.ws.medicalentity;

import eu.nephron.beans.Children.MedicationFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "Medication")
public class Medication {
    @EJB
    private MedicationFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createMedication")
    @Oneway
    public void createMedication(@WebParam(name = "entity") eu.nephron.model.entity.Medication entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editMedication")
    @Oneway
    public void editMedication(@WebParam(name = "entity") eu.nephron.model.entity.Medication entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeMedication")
    @Oneway
    public void removeMedication(@WebParam(name = "entity") eu.nephron.model.entity.Medication entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findMedication")
    public eu.nephron.model.entity.Medication findMedication(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllMedication")
    public List<eu.nephron.model.entity.Medication> findAllMedication() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeMedication")
    public List<eu.nephron.model.entity.Medication> findRangeMedication(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countMedication")
    public int countMedication() {
        return ejbRef.count();
    }
    
}
