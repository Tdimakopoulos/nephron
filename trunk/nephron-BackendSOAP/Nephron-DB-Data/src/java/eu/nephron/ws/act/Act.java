/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.act;

import eu.nephron.beans.ActFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "Act")
public class Act {
    @EJB
    private ActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createAct")
    @Oneway
    public void createAct(@WebParam(name = "entity") eu.nephron.model.act.Act entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editAct")
    @Oneway
    public void editAct(@WebParam(name = "entity") eu.nephron.model.act.Act entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeAct")
    @Oneway
    public void removeAct(@WebParam(name = "entity") eu.nephron.model.act.Act entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findAct")
    public eu.nephron.model.act.Act findAct(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllAct")
    public List<eu.nephron.model.act.Act> findAllAct() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeAct")
    public List<eu.nephron.model.act.Act> findRangeAct(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countAct")
    public int countAct() {
        return ejbRef.count();
    }
    
}
