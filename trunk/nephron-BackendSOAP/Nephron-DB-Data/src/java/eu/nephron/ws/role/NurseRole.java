/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.NurseFacade;
import eu.nephron.model.role.Nurse;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "NurseRole")
public class NurseRole {
    @EJB
    private NurseFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createNurseRole")
    @Oneway
    public void createNurseRole(@WebParam(name = "entity") Nurse entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editNurseRole")
    @Oneway
    public void editNurseRole(@WebParam(name = "entity") Nurse entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeNurseRole")
    @Oneway
    public void removeNurseRole(@WebParam(name = "entity") Nurse entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findNurseRole")
    public Nurse findNurseRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllNurseRole")
    public List<Nurse> findAllNurseRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeNurseRole")
    public List<Nurse> findRangeNurseRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countNurseRole")
    public int countNurseRole() {
        return ejbRef.count();
    }
    
}
