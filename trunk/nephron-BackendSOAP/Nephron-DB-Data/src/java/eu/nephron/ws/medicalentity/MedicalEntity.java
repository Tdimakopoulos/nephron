/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.medicalentity;

import eu.nephron.beans.MedicalEntityFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "MedicalEntity")
public class MedicalEntity {
    @EJB
    private MedicalEntityFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createMedicalEntity")
    @Oneway
    public void createMedicalEntity(@WebParam(name = "entity") eu.nephron.model.entity.MedicalEntity entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editMedicalEntity")
    @Oneway
    public void editMedicalEntity(@WebParam(name = "entity") eu.nephron.model.entity.MedicalEntity entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeMedicalEntity")
    @Oneway
    public void removeMedicalEntity(@WebParam(name = "entity") eu.nephron.model.entity.MedicalEntity entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findMedicalEntity")
    public eu.nephron.model.entity.MedicalEntity findMedicalEntity(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllMedicalEntity")
    public List<eu.nephron.model.entity.MedicalEntity> findAllMedicalEntity() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeMedicalEntity")
    public List<eu.nephron.model.entity.MedicalEntity> findRangeMedicalEntity(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countMedicalEntity")
    public int countMedicalEntity() {
        return ejbRef.count();
    }
    
}
