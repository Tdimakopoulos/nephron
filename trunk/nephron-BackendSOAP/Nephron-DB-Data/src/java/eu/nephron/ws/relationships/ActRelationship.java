/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.relationships;

import eu.nephron.beans.ActRelationshipFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ActRelationship")
public class ActRelationship {
    @EJB
    private ActRelationshipFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createActRelationship")
    @Oneway
    public void createActRelationship(@WebParam(name = "entity") eu.nephron.model.act.ActRelationship entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editActRelationship")
    @Oneway
    public void editActRelationship(@WebParam(name = "entity") eu.nephron.model.act.ActRelationship entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeActRelationship")
    @Oneway
    public void removeActRelationship(@WebParam(name = "entity") eu.nephron.model.act.ActRelationship entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findActRelationship")
    public eu.nephron.model.act.ActRelationship findActRelationship(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllActRelationship")
    public List<eu.nephron.model.act.ActRelationship> findAllActRelationship() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeActRelationship")
    public List<eu.nephron.model.act.ActRelationship> findRangeActRelationship(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countActRelationship")
    public int countActRelationship() {
        return ejbRef.count();
    }
    
}
