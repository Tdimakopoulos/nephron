/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.SmartPhoneFacade;
import eu.nephron.model.role.SmartPhone;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "SmartPhoneRole")
public class SmartPhoneRole {
    @EJB
    private SmartPhoneFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createSmartPhoneRole")
    @Oneway
    public void createSmartPhoneRole(@WebParam(name = "entity") SmartPhone entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editSmartPhoneRole")
    @Oneway
    public void editSmartPhoneRole(@WebParam(name = "entity") SmartPhone entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeSmartPhoneRole")
    @Oneway
    public void removeSmartPhoneRole(@WebParam(name = "entity") SmartPhone entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findSmartPhoneRole")
    public SmartPhone findSmartPhoneRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllSmartPhoneRole")
    public List<SmartPhone> findAllSmartPhoneRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeSmartPhoneRole")
    public List<SmartPhone> findRangeSmartPhoneRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countSmartPhoneRole")
    public int countSmartPhoneRole() {
        return ejbRef.count();
    }
    
}
