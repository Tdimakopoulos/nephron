/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.participation;

import eu.nephron.beans.ParticipationFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "Participation")
public class Participation {
    @EJB
    private ParticipationFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createParticipation")
    @Oneway
    public void createParticipation(@WebParam(name = "entity") eu.nephron.model.act.Participation entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editParticipation")
    @Oneway
    public void editParticipation(@WebParam(name = "entity") eu.nephron.model.act.Participation entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeParticipation")
    @Oneway
    public void removeParticipation(@WebParam(name = "entity") eu.nephron.model.act.Participation entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findParticipation")
    public eu.nephron.model.act.Participation findParticipation(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllParticipation")
    public List<eu.nephron.model.act.Participation> findAllParticipation() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeParticipation")
    public List<eu.nephron.model.act.Participation> findRangeParticipation(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countParticipation")
    public int countParticipation() {
        return ejbRef.count();
    }
    
}
