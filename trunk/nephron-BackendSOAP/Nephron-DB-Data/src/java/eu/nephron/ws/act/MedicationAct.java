/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.act;

import eu.nephron.beans.MedicationActFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "MedicationAct")
public class MedicationAct {
    @EJB
    private MedicationActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createMedicationAct")
    @Oneway
    public void createMedicationAct(@WebParam(name = "entity") eu.nephron.model.act.MedicationAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editMedicationAct")
    @Oneway
    public void editMedicationAct(@WebParam(name = "entity") eu.nephron.model.act.MedicationAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeMedicationAct")
    @Oneway
    public void removeMedicationAct(@WebParam(name = "entity") eu.nephron.model.act.MedicationAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findMedicationAct")
    public eu.nephron.model.act.MedicationAct findMedicationAct(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllMedicationAct")
    public List<eu.nephron.model.act.MedicationAct> findAllMedicationAct() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeMedicationAct")
    public List<eu.nephron.model.act.MedicationAct> findRangeMedicationAct(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countMedicationAct")
    public int countMedicationAct() {
        return ejbRef.count();
    }
    
}
