/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.act;

import eu.nephron.beans.ObservationActFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ObservationAct")
public class ObservationAct {
    @EJB
    private ObservationActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createObservationAct")
    @Oneway
    public void createObservationAct(@WebParam(name = "entity") eu.nephron.model.act.ObservationAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editObservationAct")
    @Oneway
    public void editObservationAct(@WebParam(name = "entity") eu.nephron.model.act.ObservationAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeObservationAct")
    @Oneway
    public void removeObservationAct(@WebParam(name = "entity") eu.nephron.model.act.ObservationAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findObservationAct")
    public eu.nephron.model.act.ObservationAct findObservationAct(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllObservationAct")
    public List<eu.nephron.model.act.ObservationAct> findAllObservationAct() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeObservationAct")
    public List<eu.nephron.model.act.ObservationAct> findRangeObservationAct(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countObservationAct")
    public int countObservationAct() {
        return ejbRef.count();
    }
    
}
