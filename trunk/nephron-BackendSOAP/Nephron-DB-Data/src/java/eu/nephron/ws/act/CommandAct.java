/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.act;

import eu.nephron.beans.CommandActFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "CommandAct")
public class CommandAct {
    @EJB
    private CommandActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createCommandAct")
    @Oneway
    public void createCommandAct(@WebParam(name = "entity") eu.nephron.model.act.CommandAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editCommandAct")
    @Oneway
    public void editCommandAct(@WebParam(name = "entity") eu.nephron.model.act.CommandAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeCommandAct")
    @Oneway
    public void removeCommandAct(@WebParam(name = "entity") eu.nephron.model.act.CommandAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findCommandAct")
    public eu.nephron.model.act.CommandAct findCommandAct(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllCommandAct")
    public List<eu.nephron.model.act.CommandAct> findAllCommandAct() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeCommandAct")
    public List<eu.nephron.model.act.CommandAct> findRangeCommandAct(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countCommandAct")
    public int countCommandAct() {
        return ejbRef.count();
    }
    
}
