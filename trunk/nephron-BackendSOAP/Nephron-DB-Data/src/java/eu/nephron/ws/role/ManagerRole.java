/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.ManagerFacade;
import eu.nephron.model.role.Manager;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ManagerRole")
public class ManagerRole {
    @EJB
    private ManagerFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createManagerRole")
    @Oneway
    public void createManagerRole(@WebParam(name = "entity") Manager entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editManagerRole")
    @Oneway
    public void editManagerRole(@WebParam(name = "entity") Manager entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeManagerRole")
    @Oneway
    public void removeManagerRole(@WebParam(name = "entity") Manager entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findManagerRole")
    public Manager findManagerRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllManagerRole")
    public List<Manager> findAllManagerRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeManagerRole")
    public List<Manager> findRangeManagerRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countManagerRole")
    public int countManagerRole() {
        return ejbRef.count();
    }
    
}
