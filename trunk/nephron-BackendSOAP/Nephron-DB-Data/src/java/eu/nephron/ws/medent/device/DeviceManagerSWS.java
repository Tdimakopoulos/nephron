/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.medent.device;

import eu.nephron.beans.DeviceFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DeviceManagerSWS")
public class DeviceManagerSWS {

    @EJB
    private DeviceFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createDevice")
    @Oneway
    public void createDevice(@WebParam(name = "entity") eu.nephron.model.entity.Device entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editDevice")
    @Oneway
    public void editDevice(@WebParam(name = "entity") eu.nephron.model.entity.Device entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeDevice")
    @Oneway
    public void removeDevice(@WebParam(name = "entity") eu.nephron.model.entity.Device entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findDevice")
    public eu.nephron.model.entity.Device findDevice(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllDevice")
    public List<eu.nephron.model.entity.Device> findAllDevice() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeDevice")
    public List<eu.nephron.model.entity.Device> findRangeDevice(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countDevice")
    public int countDevice() {
        return ejbRef.count();
    }
}
