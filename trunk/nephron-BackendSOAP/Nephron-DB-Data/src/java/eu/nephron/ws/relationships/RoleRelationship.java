/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.relationships;

import eu.nephron.beans.RoleRelationshipFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "RoleRelationship")
public class RoleRelationship {
    @EJB
    private RoleRelationshipFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createRoleRelationship")
    @Oneway
    public void createRoleRelationship(@WebParam(name = "entity") eu.nephron.model.role.RoleRelationship entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editRoleRelationship")
    @Oneway
    public void editRoleRelationship(@WebParam(name = "entity") eu.nephron.model.role.RoleRelationship entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeRoleRelationship")
    @Oneway
    public void removeRoleRelationship(@WebParam(name = "entity") eu.nephron.model.role.RoleRelationship entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findRoleRelationship")
    public eu.nephron.model.role.RoleRelationship findRoleRelationship(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllRoleRelationship")
    public List<eu.nephron.model.role.RoleRelationship> findAllRoleRelationship() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeRoleRelationship")
    public List<eu.nephron.model.role.RoleRelationship> findRangeRoleRelationship(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countRoleRelationship")
    public int countRoleRelationship() {
        return ejbRef.count();
    }
    
}
