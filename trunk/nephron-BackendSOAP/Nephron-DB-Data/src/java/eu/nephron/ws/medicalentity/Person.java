/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.medicalentity;

import eu.nephron.beans.Children.PersonFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "Person")
public class Person {
    @EJB
    private PersonFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createPerson")
    @Oneway
    public void createPerson(@WebParam(name = "entity") eu.nephron.model.entity.Person entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editPerson")
    @Oneway
    public void editPerson(@WebParam(name = "entity") eu.nephron.model.entity.Person entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removePerson")
    @Oneway
    public void removePerson(@WebParam(name = "entity") eu.nephron.model.entity.Person entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findPerson")
    public eu.nephron.model.entity.Person findPerson(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllPerson")
    public List<eu.nephron.model.entity.Person> findAllPerson() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangePerson")
    public List<eu.nephron.model.entity.Person> findRangePerson(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countPerson")
    public int countPerson() {
        return ejbRef.count();
    }
    
}
