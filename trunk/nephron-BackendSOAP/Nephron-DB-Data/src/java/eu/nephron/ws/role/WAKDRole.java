/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.WAKDFacade;
import eu.nephron.model.role.WAKD;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "WAKDRole")
public class WAKDRole {
    @EJB
    private WAKDFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createWAKDRole")
    @Oneway
    public void createWAKDRole(@WebParam(name = "entity") WAKD entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editWAKDRole")
    @Oneway
    public void editWAKDRole(@WebParam(name = "entity") WAKD entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeWAKDRole")
    @Oneway
    public void removeWAKDRole(@WebParam(name = "entity") WAKD entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findWAKDRole")
    public WAKD findWAKDRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllWAKDRole")
    public List<WAKD> findAllWAKDRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeWAKDRole")
    public List<WAKD> findRangeWAKDRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countWAKDRole")
    public int countWAKDRole() {
        return ejbRef.count();
    }
    
}
