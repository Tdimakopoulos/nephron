/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.role;

import eu.nephron.beans.Children.DoctorFacade;
import eu.nephron.model.role.Doctor;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DoctorRole")
public class DoctorRole {
    @EJB
    private DoctorFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createDoctorRole")
    @Oneway
    public void createDoctorRole(@WebParam(name = "entity") Doctor entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editDoctorRole")
    @Oneway
    public void editDoctorRole(@WebParam(name = "entity") Doctor entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeDoctorRole")
    @Oneway
    public void removeDoctorRole(@WebParam(name = "entity") Doctor entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findDoctorRole")
    public Doctor findDoctorRole(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllDoctorRole")
    public List<Doctor> findAllDoctorRole() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeDoctorRole")
    public List<Doctor> findRangeDoctorRole(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countDoctorRole")
    public int countDoctorRole() {
        return ejbRef.count();
    }
    
}
