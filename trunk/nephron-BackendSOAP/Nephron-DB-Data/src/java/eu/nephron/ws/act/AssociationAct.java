/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ws.act;

import eu.nephron.beans.AssociationActFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "AssociationAct")
public class AssociationAct {
    @EJB
    private AssociationActFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createAssociationAct")
    @Oneway
    public void createAssociationAct(@WebParam(name = "entity") eu.nephron.model.act.AssociationAct entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editAssociationAct")
    @Oneway
    public void editAssociationAct(@WebParam(name = "entity") eu.nephron.model.act.AssociationAct entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeAssociationAct")
    @Oneway
    public void removeAssociationAct(@WebParam(name = "entity") eu.nephron.model.act.AssociationAct entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findAssociationAct")
    public eu.nephron.model.act.AssociationAct findAssociationAct(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllAssociationAct")
    public List<eu.nephron.model.act.AssociationAct> findAllAssociationAct() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeAssociationAct")
    public List<eu.nephron.model.act.AssociationAct> findRangeAssociationAct(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countAssociationAct")
    public int countAssociationAct() {
        return ejbRef.count();
    }
    
}
