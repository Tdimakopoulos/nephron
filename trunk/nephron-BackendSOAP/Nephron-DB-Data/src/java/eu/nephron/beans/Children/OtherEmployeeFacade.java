/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans.Children;

import eu.nephron.model.role.OtherEmployee;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class OtherEmployeeFacade extends AbstractFacade<OtherEmployee> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OtherEmployeeFacade() {
        super(OtherEmployee.class);
    }
    
}
