/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.act.QuestionnaireAct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class QuestionnaireActFacade extends AbstractFacade<QuestionnaireAct> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuestionnaireActFacade() {
        super(QuestionnaireAct.class);
    }
    
}
