/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.entity.MedicalEntity;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class MedicalEntityFacade extends AbstractFacade<MedicalEntity> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MedicalEntityFacade() {
        super(MedicalEntity.class);
    }
    
}
