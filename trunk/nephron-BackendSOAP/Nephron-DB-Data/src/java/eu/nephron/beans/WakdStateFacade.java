/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.WakdState;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class WakdStateFacade extends AbstractFacade<WakdState> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WakdStateFacade() {
        super(WakdState.class);
    }
    
    public List<WakdState> FindParents(Device device)
    {
        List<WakdState> wakdp = em.createQuery("select s from WakdState s where s.device.serialNumber=:serialNumber and s.date=(select max(s.date) from WakdState s where s.device.id=:deviceID)").setParameter("serialNumber", device.getSerialNumber()).getResultList();
        return wakdp;
    }
    
}
