/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.beans;

import eu.nephron.model.alert.Alert;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class AlertFacade extends AbstractFacade<Alert> {
    @PersistenceContext(unitName = "nephrondatadb")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlertFacade() {
        super(Alert.class);
    }
    
}
