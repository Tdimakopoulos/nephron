/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.pipes;

import java.io.InputStream;
import java.io.OutputStream;
 
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
 
public class WordWatcher implements Runnable
{
    private InputStream inputStream;
    private OutputStream outputStream;
 
    public WordWatcher(InputStream inputStream, OutputStream outputStream)
    {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }
 
    @Override
    public void run()
    {
        try
        {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(inputStream);
            while (reader.hasNext())
            {
                int next = reader.next();
                switch (next)
                {
                case XMLStreamConstants.CHARACTERS:
                {
                    String text = reader.getText();
                    if(text.contains("WSINPUTPIPEDB"))
                    {
                        text = "WSINPUTPIPEDB"+text;
                    }
                    outputStream.write(text.getBytes());
                    break;
                }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                outputStream.flush();
                outputStream.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
