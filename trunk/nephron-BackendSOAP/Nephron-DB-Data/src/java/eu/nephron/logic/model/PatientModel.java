/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.logic.model;

import java.util.List;

/**
 *
 * @author tdim
 */
public class PatientModel {
    private Long PatientID;
    private List<MedicalDevicesModel> pMedicalDevices;

    private String name;
    private String surname;
    private String middlename;
    private String dateofbirth;
    
    /**
     * @return the PatientID
     */
    public Long getPatientID() {
        return PatientID;
    }

    /**
     * @param PatientID the PatientID to set
     */
    public void setPatientID(Long PatientID) {
        this.PatientID = PatientID;
    }

    /**
     * @return the pMedicalDevices
     */
    public List<MedicalDevicesModel> getpMedicalDevices() {
        return pMedicalDevices;
    }

    /**
     * @param pMedicalDevices the pMedicalDevices to set
     */
    public void setpMedicalDevices(List<MedicalDevicesModel> pMedicalDevices) {
        this.pMedicalDevices = pMedicalDevices;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the dateofbirth
     */
    public String getDateofbirth() {
        return dateofbirth;
    }

    /**
     * @param dateofbirth the dateofbirth to set
     */
    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }
    
}
