/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.logic.model;

/**
 *
 * @author tdim
 */
public class DoctorModel {
 
    private Long DoctorID;
    private String name;
    private String surname;
    /**
     * @return the DoctorID
     */
    public Long getDoctorID() {
        return DoctorID;
    }

    /**
     * @param DoctorID the DoctorID to set
     */
    public void setDoctorID(Long DoctorID) {
        this.DoctorID = DoctorID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
}
