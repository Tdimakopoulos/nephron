/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.logic;

import eu.nephron.beans.ActFacade;
import eu.nephron.beans.AssociationActFacade;
import eu.nephron.beans.Children.MedicalPartActRelationshipFacadeLocal;
import eu.nephron.beans.Children.PersonFacade;
import eu.nephron.beans.Children.SmartPhoneFacade;
import eu.nephron.beans.Children.WAKDFacade;
import eu.nephron.beans.DeviceFacade;
import eu.nephron.beans.MedicalEntityFacade;
import eu.nephron.beans.ParticipationFacade;
import eu.nephron.beans.RoleFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.logic.model.CompleteTreeModel;
import eu.nephron.logic.model.DoctorModel;
import eu.nephron.logic.model.MedicalDevicesModel;
import eu.nephron.logic.model.PatientModel;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.MedicalEntity;
import eu.nephron.model.relationships.MedicalPartActRelationship;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "NephronLogicWS")
public class NephronLogicWS {
    

    @EJB
    private RoleFacade pRole;
    @EJB
    private SmartPhoneFacade pSmartPhone;
    @EJB
    private WAKDFacade pwakd;
    @EJB
    private DeviceFacade pdevice;
    @EJB
    private PersonFacade pPerson;
    @EJB
    private CrudService crudService;
    @EJB
    private AssociationActFacade pAssociationAct;
    @EJB
    private DeviceFacade pDevice;
    @EJB
    private ParticipationFacade ejbRefparticipation;
    @EJB
    private MedicalEntityFacade ejbRef;
    @EJB
    private ActFacade actejbRef;
    @EJB
    private MedicalPartActRelationshipFacadeLocal mpactfac;
    
    private MedicalEntity FindMedicalEntity(Long ID)
    {
        return ejbRef.find(ID);
    }
    
    private Device FindDevice(Long ID)
    {
        return pdevice.find(ID);
    }
    
    private <T> boolean CheckForInstance(final T[] array, final T v) {
        for (final T e : array) {
            if (e == v || v != null && v.equals(e)) {
                return true;
            }
        }

        return false;
    }
    
    @WebMethod(operationName = "findConnectionsBetweenMedicalEntities")
    public Object[] findConnectionsBetweenMedicalEntities(Long DoctorID, String publickey) {
        
        List<Long> doctorrecord = new ArrayList();
        boolean bfind=false;
        //List<Participation> preturn = ejbRefparticipation.findAll();
        
        List<MedicalPartActRelationship> pmrelalist = crudService.findWithNamedQuery("MedicalPartActRelationship.findAll");
        
        List<Long> Actid = new ArrayList();
        
        Long iActID=0L;
        for (int i=0;i<pmrelalist.size();i++)
        {
            if (pmrelalist.get(i).getMedicanEntityID()==DoctorID)
            {
                iActID=pmrelalist.get(i).getActID();
                Actid.add(iActID);
            }
        }
        
        for (int i=0;i<pmrelalist.size();i++)
        {
            if(CheckForInstance(Actid.toArray(),pmrelalist.get(i).getActID())){
                if(pmrelalist.get(i).getMedicanEntityID()!=DoctorID)
                {
                 MedicalEntity ppatient = ejbRef.find(pmrelalist.get(i).getMedicanEntityID());//new Person();
                 doctorrecord.add(ppatient.getId());
                 bfind=true;
                }
            }
        }
        if(bfind==true)
        {
            return doctorrecord.toArray();
        }else
        {
            doctorrecord.add(new Long(-1));
            return doctorrecord.toArray();
        }
    }
    
    @WebMethod(operationName = "GetCompleteStructure")
    public CompleteTreeModel MakeTree(Long DoctorID, String publickey) {
    
        CompleteTreeModel pReturn= new CompleteTreeModel();
        
        //load all connection in memory (1 ws call)
        List<MedicalPartActRelationship> pmrelalist = crudService.findWithNamedQuery("MedicalPartActRelationship.findAll");
        
        //find patients for doctor
        Object[] Doctors=findConnections(DoctorID,publickey,pmrelalist);
        
        DoctorModel pDoctor= new DoctorModel();

        pDoctor.setDoctorID(DoctorID);
        pReturn.setpDoctor(pDoctor);
        
        List<PatientModel> pPatients= new ArrayList();
        
        for (int i=0;i<Doctors.length;i++)
        {
            PatientModel pEntry= new PatientModel();
            pEntry.setPatientID((Long)Doctors[i]);
            Object[] MedicalDevices=findConnections((Long)Doctors[i],publickey,pmrelalist); 
            List<MedicalDevicesModel> pMedicalDevices= new ArrayList();
            for (int imd=0;imd<MedicalDevices.length;imd++)
            {
                if((Long)MedicalDevices[imd]!=(Long)Doctors[i])
                {
                    if((Long)MedicalDevices[imd]!=DoctorID)
                    {
                        // (imd x ws call)
                        Device pdevicetemp=FindDevice((Long)MedicalDevices[imd]);
                        MedicalDevicesModel pentry=new MedicalDevicesModel();
                        pentry.setIMEI(pdevicetemp.getSerialNumber());
                        pentry.setType(pdevicetemp.getCode());
                        pMedicalDevices.add(pentry);
                    }
                }
            }
            pEntry.setpMedicalDevices(pMedicalDevices);
            pPatients.add(pEntry);
        }
        
        pReturn.setpPatients(pPatients);
        return pReturn;
    }
    
    
    private Object[] findConnections(Long MedicalEntityID, String publickey,List<MedicalPartActRelationship> pmrelalist) {
        
        List<Long> MedicalConnectedRecords = new ArrayList();
        boolean bfind=false;
        List<Long> Actid = new ArrayList();
        Long iActID=0L;
        
        for (int i=0;i<pmrelalist.size();i++)
        {
            if (pmrelalist.get(i).getMedicanEntityID()==MedicalEntityID)
            {
                iActID=pmrelalist.get(i).getActID();
                Actid.add(iActID);
            }
        }
        
        for (int i=0;i<pmrelalist.size();i++)
        {
            if(CheckForInstance(Actid.toArray(),pmrelalist.get(i).getActID())){
                if(pmrelalist.get(i).getMedicanEntityID()!=MedicalEntityID)
                {
                 MedicalEntity ppatient = ejbRef.find(pmrelalist.get(i).getMedicanEntityID());
                 MedicalConnectedRecords.add(ppatient.getId());
                 bfind=true;
                }
            }
        }
        
        if(bfind==true)
        {
            return MedicalConnectedRecords.toArray();
        }else
        {
            MedicalConnectedRecords.add(new Long(-1));
            return MedicalConnectedRecords.toArray();
        }
    }

    
}
