/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.logic.model;

import java.util.List;

/**
 *
 * @author tdim
 */
public class CompleteTreeModel {
    private DoctorModel pDoctor;
    private List<PatientModel> pPatients;

    /**
     * @return the pDoctor
     */
    public DoctorModel getpDoctor() {
        return pDoctor;
    }

    /**
     * @param pDoctor the pDoctor to set
     */
    public void setpDoctor(DoctorModel pDoctor) {
        this.pDoctor = pDoctor;
    }

    /**
     * @return the pPatients
     */
    public List<PatientModel> getpPatients() {
        return pPatients;
    }

    /**
     * @param pPatients the pPatients to set
     */
    public void setpPatients(List<PatientModel> pPatients) {
        this.pPatients = pPatients;
    }
    
}
