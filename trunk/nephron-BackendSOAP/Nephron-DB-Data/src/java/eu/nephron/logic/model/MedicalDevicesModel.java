/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.logic.model;

/**
 *
 * @author tdim
 */
public class MedicalDevicesModel {
   private String IMEI;
   private String type;

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
   
}
