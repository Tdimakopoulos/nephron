/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.ActFacade;
import eu.nephron.beans.AlertFacade;
import eu.nephron.beans.WakdStateFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.model.alert.Alert;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.WakdState;
import eu.nephron.securitycheck.SecurityCheckPKI;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;
import eu.nephron.wakd.api.incoming.AlertMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "HardwareControl")
public class HardwareControl {

    @EJB
    private CrudService crudservice;
    
    @EJB
    private AlertFacade AlertRef;

    @EJB
    private WakdStateFacade pWakdState;
     
    @PersistenceUnit
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "CreateAlert")
    public String createAlert(String serial, String hexStr, String publickey) throws DecoderException {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        SecurityCheckPKI pki = new SecurityCheckPKI();
        if (!pki.CheckUser(publickey)) {
            return "User is not Authorized";
        }
        AlertMsg msg = new AlertMsg();
        msg.decode(Hex.decodeHex(hexStr.toCharArray()));

        Alert alert = new Alert();
        alert.setCode(msg.getAlert().toString());

        List<Device> wakd = em.createQuery("select d from Device d where d.role.code='WAKD' and d.serialNumber=:serialNumber").setParameter("serialNumber", serial).getResultList();

        alert.setDevice(wakd.get(0));
        alert.setAlertDate(new Date());
        alert.setSeverity(1);
        AlertRef.create(alert);
        return null;
    }
    
    @WebMethod(operationName = "UpdateStatus")
    public String updateState(String serial, String hexStr, String publickey) throws DecoderException {
        
      
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        SecurityCheckPKI pki = new SecurityCheckPKI();
        if (!pki.CheckUser(publickey)) {
            return "User is not Authorized";
        }
        List<Device> wakd = em.createQuery("select d from Device d where d.role.code='WAKD' and d.serialNumber=:serialNumber").setParameter("serialNumber", serial).getResultList();
        CurrentStateMsg msgs = new CurrentStateMsg();
        msgs.decode(Hex.decodeHex(hexStr.toCharArray()));
        WakdState pstate= new WakdState();
        pstate.setDate(new Date());
        pstate.setDevice(wakd.get(0));
        pstate.setOpState(new Integer(msgs.getCurrentOpState().getIdentifier()));
        pstate.setState(new Integer(msgs.getCurrentState().getIdentifier()));
        List<WakdState> wakdp = em.createQuery("select s from WakdState s where s.device.serialNumber=:serialNumber and s.date=(select max(s.date) from WakdState s where s.device.id=:deviceID)").setParameter("serialNumber", serial).getResultList();
        pstate.setParent(wakdp.get(0));
        pWakdState.create(pstate);
        return null;
    }
    
    @WebMethod(operationName = "CreateState")
    public void createState(Long deviceID, WakdStateEnum wakdState, WakdOpStateEnum wakdOpState, String publickey) {
		Map<String, Object> deviceParams = new HashMap<String, Object>();
		deviceParams.put("deviceID", deviceID);
		Device device = (Device) crudservice.findSingleWithNamedQuery("Device.findAssociatedDevice", deviceParams);
		WakdState currentState = new WakdState();
		currentState.setDate(new Date());
		currentState.setDevice(device);
		currentState.setState(new Integer(wakdState.getIdentifier()));
		currentState.setOpState(new Integer(wakdOpState.getIdentifier()));
		Map<String, Object> stateParams = new HashMap<String, Object>();
		stateParams.put("deviceID", device.getId());
		WakdState parentState = (WakdState) crudservice.findSingleWithNamedQuery("WakdState.findWakdStateByMaxDate", stateParams);
		currentState.setParent(parentState);
		crudservice.create(currentState);
	}
    
    @WebMethod(operationName = "FindCurrentState")
    public WakdState findCurrentState(Long deviceID, String publickey) {
		
		Map<String, Object> deviceParams = new HashMap<String, Object>();
		deviceParams.put("deviceID", deviceID);
		Device device = (Device) crudservice.findSingleWithNamedQuery("Device.findAssociatedDevice", deviceParams);
		Map<String, Object> stateParams = new HashMap<String, Object>();
		stateParams.put("deviceID", device.getId());
		return (WakdState) crudservice.findSingleWithNamedQuery("WakdState.findWakdStateByMaxDate", stateParams);
	}
    
}
