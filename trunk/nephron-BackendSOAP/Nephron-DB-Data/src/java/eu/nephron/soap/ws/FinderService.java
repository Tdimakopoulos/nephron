/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws;

import eu.nephron.beans.Children.MedicationFacade;
import eu.nephron.beans.Children.PersonFacade;
import eu.nephron.beans.DeviceFacade;
import eu.nephron.beans.MedicalEntityFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.MedicalEntity;
import eu.nephron.model.entity.Person;
import eu.nephron.securitycheck.SecurityCheckPKI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "FinderService")
public class FinderService {

    @EJB
    private CrudService crudService;
    
    @EJB
    private MedicalEntityFacade ejbMedicalEntitytRef;
    
    @EJB
    private PersonFacade ejbRef;
    
    @EJB
    private DeviceFacade devejbRef;
    
    @EJB
    private MedicationFacade medejbRef;
    
    @WebMethod(operationName = "findPersonByID")
    public eu.nephron.model.entity.Person FinderSRVfindPersonByID(@WebParam(name = "id") Long id, String publickey) {
        return ejbRef.find((Object)id);
    }
    
    @WebMethod(operationName = "findDeviceByID")
    public eu.nephron.model.entity.Device FinderSRVfindDeviceByID(@WebParam(name = "id") Long id, String publickey) {
        return devejbRef.find((Object)id);
    }
    
    @WebMethod(operationName = "findMedicationByID")
    public eu.nephron.model.entity.Medication FinderSRVfindMedicationByID(@WebParam(name = "id") Long id, String publickey) {
        return medejbRef.find((Object)id);
    }
}
