/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.soap.ws.commandmanager;


import eu.nephron.beans.ActFacade;
import eu.nephron.beans.Children.MedicalPartActRelationshipFacade;
import eu.nephron.beans.Children.MedicalPartActRelationshipFacadeLocal;
import eu.nephron.beans.Children.PersonFacade;
import eu.nephron.beans.MedicalEntityFacade;
import eu.nephron.beans.ParticipationFacade;
import eu.nephron.crudservice.CrudService;
import eu.nephron.model.act.Act;
import eu.nephron.model.act.ActStatusEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.act.AssociationAct;
import eu.nephron.model.act.MoodCodeEnum;
import eu.nephron.model.act.Participation;
import eu.nephron.model.entity.MedicalEntity;
import eu.nephron.model.entity.Person;
import eu.nephron.model.relationships.MedicalPartActRelationship;
import eu.nephron.model.role.Patient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ActServiceManager")
public class ActServiceManager {

    @EJB
    private CrudService crudService;
    @EJB
    private ParticipationFacade ejbRefparticipation;
    @EJB
    private MedicalEntityFacade ejbRef;
    
    @EJB
    private ActFacade actejbRef;
    
    @EJB
    private MedicalPartActRelationshipFacadeLocal mpactfac;
            
    private void AddRelationship(Long actId, Long medicalEntityID, Long perparticipationId) {
        MedicalPartActRelationship pEntity = new MedicalPartActRelationship();
        pEntity.setActID(actId);
        pEntity.setMedicanEntityID(medicalEntityID);
        pEntity.setParticipationID(perparticipationId);
        crudService.create(pEntity);
    }

    @WebMethod(operationName = "CreateAssociationAct")
    public void createAssociationAct(List<Long> medicalEntityIDs, String publickey) {
        AssociationAct act = new AssociationAct();

        act.setActStatus(2);//ActStatusEnum.SUCCESSFUL
        act.setMoodCode(4);//MoodCodeEnum.EVENT
        crudService.create(act);
        for (Long medicalEntityID : medicalEntityIDs) {
            MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
            Participation participation = new Participation();

            Participation perparticipation = (Participation) crudService.create(participation);
            AddRelationship(act.getId(), entity.getId(), perparticipation.getId());
        }
    }

    @WebMethod(operationName = "UpdateAssociationAct")
    public void updateAssociationAct(Long actID, List<Long> medicalEntityIDs, String publickey) {
        AssociationAct act = (AssociationAct) crudService.find(AssociationAct.class, actID);
        for (Long medicalEntityID : medicalEntityIDs) {
            MedicalEntity entity = (MedicalEntity) crudService.find(MedicalEntity.class, medicalEntityID);
            Participation participation = new Participation();

            Participation perparticipation = (Participation) crudService.create(participation);
            AddRelationship(act.getId(), entity.getId(), perparticipation.getId());
        }
    }

    private <T> boolean CheckForInstance(final T[] array, final T v) {
        for (final T e : array) {
            if (e == v || v != null && v.equals(e)) {
                return true;
            }
        }

        return false;
    }

    @WebMethod(operationName = "deleteConnections")
    public String deleteConnections(Long pID, String publickey) {
        List<Long> doctorrecord = new ArrayList();
        boolean bfind=false;
        //List<Participation> preturn = ejbRefparticipation.findAll();
        
        List<MedicalPartActRelationship> pmrelalist = crudService.findWithNamedQuery("MedicalPartActRelationship.findAll");
        
        List<Long> Actid = new ArrayList();
        List<Long> pard = new ArrayList();
        List<Long> mpad = new ArrayList();
        
        Long iActID=0L;
        Long iPaid=0L;
        for (int i=0;i<pmrelalist.size();i++)
        {
            if (pmrelalist.get(i).getMedicanEntityID()==pID)
            {
                iActID=pmrelalist.get(i).getActID();
                iPaid=pmrelalist.get(i).getParticipationID();
                Actid.add(iActID);
                pard.add(iPaid);
                mpad.add(pmrelalist.get(i).getId());
            }
        }
        
        for(int i=0;i<Actid.size();i++)
            actejbRef.remove(actejbRef.find(Actid.get(i)));
        
        for(int i=0;i<pard.size();i++)
            ejbRefparticipation.remove(ejbRefparticipation.find(pard.get(i)));
        
        for(int i=0;i<mpad.size();i++)
            mpactfac.remove(mpactfac.find(mpad.get(i)));
        
        return "OK";
    }
    
    @WebMethod(operationName = "findConnections")
    public Object[] findConnections(Long DoctorID, String publickey) {
        
        List<Long> doctorrecord = new ArrayList();
        boolean bfind=false;
        //List<Participation> preturn = ejbRefparticipation.findAll();
        
        List<MedicalPartActRelationship> pmrelalist = crudService.findWithNamedQuery("MedicalPartActRelationship.findAll");
        
        List<Long> Actid = new ArrayList();
        
        Long iActID=0L;
        for (int i=0;i<pmrelalist.size();i++)
        {
            if (pmrelalist.get(i).getMedicanEntityID()==DoctorID)
            {
                iActID=pmrelalist.get(i).getActID();
                Actid.add(iActID);
            }
        }
        
        for (int i=0;i<pmrelalist.size();i++)
        {
            if(CheckForInstance(Actid.toArray(),pmrelalist.get(i).getActID())){
                if(pmrelalist.get(i).getMedicanEntityID()!=DoctorID)
                {
                 MedicalEntity ppatient = ejbRef.find(pmrelalist.get(i).getMedicanEntityID());//new Person();
                 doctorrecord.add(ppatient.getId());
                 bfind=true;
                }
            }
        }
        if(bfind==true)
        {
            return doctorrecord.toArray();
        }else
        {
            doctorrecord.add(new Long(-1));
            return doctorrecord.toArray();
        }
    }

}
