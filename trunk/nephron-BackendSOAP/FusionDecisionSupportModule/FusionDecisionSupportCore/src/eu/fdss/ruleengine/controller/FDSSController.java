/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.ruleengine.controller;

import eu.fdss.ruleengine.repository.RepositoryController;

/**
 *
 * @author tdim
 */
public class FDSSController {

    public void SetToLocal() {
        RepositoryController pController = new RepositoryController();
        pController.SetRepositoryToLocal();
    }

    public void SetToLive() {
        RepositoryController pController = new RepositoryController();
        pController.SetRepositoryToGuvnor();
    }

    public void DeleteRepository() {
        RepositoryController pController = new RepositoryController();
        pController.DeleteRepository();
    }
}
