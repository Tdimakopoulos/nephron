/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.ruleengine.utilities;

/**
 *
 * @author tom
 */
public class RuleResultsDroolsHelper {

    public void StoreResults(
            String rulename,
            String ruleresults,
            String ruleresults1,
            String ruleresults2,
            String ruleresults3) {
        int iresult1 = 0;
        int iresult2 = 0;
        int iresult3 = 0;
        boolean bresult1 = false;
        boolean bresult2 = false;
        boolean bresult3 = false;
        eu.nephron.dss.server.ws.DssServerRuleResults dSSServerRuleResults = new eu.nephron.dss.server.ws.DssServerRuleResults();
        dSSServerRuleResults.setBresult1(bresult3);
        dSSServerRuleResults.setBresult2(bresult3);
        dSSServerRuleResults.setBresult3(bresult3);
        dSSServerRuleResults.setIresult1(iresult3);
        dSSServerRuleResults.setIresult2(iresult3);
        dSSServerRuleResults.setIresult3(iresult3);
        dSSServerRuleResults.setRulename(rulename);
        dSSServerRuleResults.setRuleresults(rulename);
        dSSServerRuleResults.setRuleresults1(rulename);
        dSSServerRuleResults.setRuleresults2(rulename);
        dSSServerRuleResults.setRuleresults3(rulename);
        dssServerRuleResultscreate(dSSServerRuleResults);
    }

    public void StoreResultsFull(
            String rulename,
            String ruleresults,
            String ruleresults1,
            String ruleresults2,
            String ruleresults3,
            int iresult1,
            int iresult2,
            int iresult3,
            boolean bresult1,
            boolean bresult2,
            boolean bresult3) {
        eu.nephron.dss.server.ws.DssServerRuleResults dSSServerRuleResults = new eu.nephron.dss.server.ws.DssServerRuleResults();
        dSSServerRuleResults.setBresult1(bresult3);
        dSSServerRuleResults.setBresult2(bresult3);
        dSSServerRuleResults.setBresult3(bresult3);
        dSSServerRuleResults.setIresult1(iresult3);
        dSSServerRuleResults.setIresult2(iresult3);
        dSSServerRuleResults.setIresult3(iresult3);
        dSSServerRuleResults.setRulename(rulename);
        dSSServerRuleResults.setRuleresults(rulename);
        dSSServerRuleResults.setRuleresults1(rulename);
        dSSServerRuleResults.setRuleresults2(rulename);
        dSSServerRuleResults.setRuleresults3(rulename);
        dssServerRuleResultscreate(dSSServerRuleResults);
    }

    private static void dssServerRuleResultscreate(eu.nephron.dss.server.ws.DssServerRuleResults dSSServerRuleResults) {
        eu.nephron.dss.server.ws.DSSServerRuleResultsWS_Service service = new eu.nephron.dss.server.ws.DSSServerRuleResultsWS_Service();
        eu.nephron.dss.server.ws.DSSServerRuleResultsWS port = service.getDSSServerRuleResultsWSPort();
        port.dssServerRuleResultscreate(dSSServerRuleResults);
    }
}
