/*
 * 
 */
package eu.fdss.rule.utils.statistics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Ratio.
 */
public class Ratio extends StatisticData {

	

	/**
	 * Calculate rate.
	 *
	 * @param pList the list
	 * @return the double
	 */
	private Double CalculateRate(List<Double> pList) {
		Double dreturn = null;
		
		List<Double> pChangesInTime = new ArrayList<Double>();
		
		//Calculate values changes between two measurements
		for (int i=0;i<pList.size()-1;i++)
		{
			pChangesInTime.add(pList.get(i)-pList.get(i+1));
		}
		
		//Calculate the Rate
		for (int j=0;j<pChangesInTime.size();j++)
		{
			dreturn=dreturn+pChangesInTime.get(j);
		}
		dreturn=dreturn/pChangesInTime.size();
		return dreturn;
	}
}
