/*
 * 
 */
package eu.fdss.rule.utils.statistics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;



// TODO: Auto-generated Javadoc
/**
 * The Class StatisticData.
 */
public class StatisticData {

	/** The p return. */
	private List<Double> pReturn = new ArrayList<Double>();

    /**
     * @return the pReturn
     */
    public List<Double> getpReturn() {
        return pReturn;
    }

    /**
     * @param pReturn the pReturn to set
     */
    public void setpReturn(List<Double> pReturn) {
        this.pReturn = pReturn;
    }

	
}
