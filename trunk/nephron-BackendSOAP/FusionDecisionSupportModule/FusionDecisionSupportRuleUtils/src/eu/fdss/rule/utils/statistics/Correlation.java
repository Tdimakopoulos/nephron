/*
 * 
 */
package eu.fdss.rule.utils.statistics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Correlation.
 */
public class Correlation extends StatisticData {

	
	
	/**
	 * Calculate auto correlation.
	 *
	 * @param pList the list
	 * @return the list
	 */
	private List<Double> CalculateAutoCorrelation(List<Double> pList)
	{
		List<Double> pReturn = new ArrayList<Double>();
		Double sum= new Double(0);
		
		for (int i=0;i<pList.size();i++)
		{
			sum=new Double(0);
			for(int j=0;j<pList.size()-1;j++)
			{
				sum=sum+(pList.get(j)*pList.get(j+1));
			}
			pReturn.add(sum);
		}
		return pReturn;
	}
}
