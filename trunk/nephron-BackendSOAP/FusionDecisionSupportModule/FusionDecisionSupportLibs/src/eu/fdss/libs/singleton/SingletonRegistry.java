/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.singleton;


import java.util.HashMap;

public class SingletonRegistry {
   protected SingletonRegistry() {
      // Exists only to thwart instantiation.
   }
   public static Singleton getInstance(String Classname) {
      return (Singleton)SingletonRegistryCore.getInstance(Classname);
   }
    private static Class getClass(String classname) 
                                         throws ClassNotFoundException {
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      if(classLoader == null)
         classLoader = Singleton.class.getClassLoader();
      return (classLoader.loadClass(classname));
   }
}
