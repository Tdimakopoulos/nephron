/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.executioncontroller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;


/** Exception that signals that a topological sort failed 
*/
public final class TopologicalSortException extends Exception {
    /** all vertexes */
    private Collection vertexes;

    /** map with edges */
    private Map edges;

    /** result if called twice */
    private Set[] result;

    /** counter to number the vertexes */
    private int counter;

    /** vertexes sorted by increasing value of y */
    private Stack<Vertex> dualGraph = new Stack<Vertex>();

    TopologicalSortException(Collection vertexes, Map edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    /** Because the full sort was not possible, this methods
     * returns the best possible substitute for it that is available.
     *
     */
    public final List partialSort() {
        Set[] all = topologicalSets();

        ArrayList<Object> res = new ArrayList<Object>(vertexes.size());

        for (int i = 0; i < all.length; i++) {
            for (Object e : all[i]) {
                res.add(e);
            }
        }

        return res;
    }

    
    public final Set[] unsortableSets() {
        Set[] all = topologicalSets();

        ArrayList<Set> unsort = new ArrayList<Set>();

        for (int i = 0; i < all.length; i++) {
            if ((all[i].size() > 1) || !(all[i] instanceof HashSet)) {
                unsort.add(all[i]);
            }
        }

        return unsort.toArray(new Set[0]);
    }

    @Override
    public String getMessage() {
        StringWriter w = new StringWriter();
        PrintWriter pw = new PrintWriter(w);
        printDebug(pw);
        pw.close();
        return w.toString();
    }
    
    @Override
    public String toString() {
        String s = getClass().getName();
        return s;
    }

    private void printDebug(java.io.PrintWriter w) {
        w.print("TopologicalSortException - Collection: "); // NOI18N
        w.print(vertexes);
        w.print(" with edges "); // NOI18N
        w.print(edges);
        w.println(" cannot be sorted"); // NOI18N

        Set[] bad = unsortableSets();

        for (int i = 0; i < bad.length; i++) {
            w.print(" Conflict #"); // NOI18N
            w.print(i);
            w.print(": "); // NOI18N
            w.println(bad[i]);
        }
    }
    
    
    public final void printStackTrace(java.io.PrintWriter w) {
        printDebug(w);
        super.printStackTrace(w);
    }

    
    public final void printStackTrace(java.io.PrintStream s) {
        java.io.PrintWriter w = new java.io.PrintWriter(s);
        this.printStackTrace(w);
        w.flush();
    }

    
    public final Set[] topologicalSets() {
        if (result != null) {
            return result;
        }

        HashMap<Object,Vertex> vertexInfo = new HashMap<Object,Vertex>();

        // computes value X and Y for each vertex
        counter = 0;

        Iterator it = vertexes.iterator();

        while (it.hasNext()) {
            constructDualGraph(counter, it.next(), vertexInfo);
        }

        // now connect vertexes that cannot be sorted into own
        // sets
        // map from the original objects to 
        Map<Object,Set> objectsToSets = new HashMap<Object,Set>();

        ArrayList<Set> sets = new ArrayList<Set>();

        while (!dualGraph.isEmpty()) {
            Vertex v = dualGraph.pop();

            if (!v.visited) {
                Set<Object> set = new HashSet<Object>();
                visitDualGraph(v, set);

                if ((set.size() == 1) && v.edgesFrom.contains(v)) {
                    // mark if there is a self reference and the
                    // set is only one element big, it means that there
                    // is a self cycle
                    //
                    // do not use HashSet but Collections.singleton
                    // to recognize such cycles
                    set = Collections.singleton(v.object);
                }

                sets.add(set);

                // fill the objectsToSets mapping
                it = set.iterator();

                while (it.hasNext()) {
                    objectsToSets.put(it.next(), set);
                }
            }
        }

        // now topologically sort the sets
    
        HashMap<Set,Collection<Set>> edgesBetweenSets = new HashMap<Set,Collection<Set>>();
        it = edges.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Collection leadsTo = (Collection) entry.getValue();

            if ((leadsTo == null) || leadsTo.isEmpty()) {
                continue;
            }

            Set from = objectsToSets.get(entry.getKey());

            Collection<Set> setsTo = edgesBetweenSets.get(from);

            if (setsTo == null) {
                setsTo = new ArrayList<Set>();
                edgesBetweenSets.put(from, setsTo);
            }

            Iterator convert = leadsTo.iterator();

            while (convert.hasNext()) {
                Set to = objectsToSets.get(convert.next());

                if (from != to) {
                    // avoid self cycles
                    setsTo.add(to);
                }
            }
        }

        // 2. do the sort
        try {
            List<Set> listResult = Utilities.topologicalSort(sets, edgesBetweenSets);
            result = listResult.toArray(new Set[0]);
        } catch (TopologicalSortException ex) {
            throw new IllegalStateException("Cannot happen"); // NOI18N
        }

        return result;
    }

    /** Traverses the tree
     */
    private Vertex constructDualGraph(int counter, Object vertex, HashMap<Object,Vertex> vertexInfo) {
        Vertex info = vertexInfo.get(vertex);

        if (info == null) {
            info = new Vertex(vertex, counter++);
            vertexInfo.put(vertex, info);
        } else {
            // already (being) processed
            return info;
        }

        // process children
        Collection c = (Collection) edges.get(vertex);

        if (c != null) {
            Iterator it = c.iterator();

            while (it.hasNext()) {
                Vertex next = constructDualGraph(counter, it.next(), vertexInfo);
                next.edgesFrom.add(info);
            }
        }

        // leaving the vertex
        info.y = counter++;

        dualGraph.push(info);

        return info;
    }

    /** Visit dual graph. Decreasing value of Y gives the order.
     */
    private void visitDualGraph(Vertex vertex, Collection<Object> visited) {
        if (vertex.visited) {
            return;
        }

        visited.add(vertex.object);
        vertex.visited = true;

        Iterator it = vertex.edges();

        while (it.hasNext()) {
            Vertex v = (Vertex) it.next();
            visitDualGraph(v, visited);
        }
    }

    private static final class Vertex implements Comparable<Vertex> {
        /** the found object */
        public Object object;

        /** list of vertexes that point to this one */
        public List<Vertex> edgesFrom = new ArrayList<Vertex>();

        /** the counter state when we entered the vertex */
        public final int x;

        /** the counter when we exited the vertex */
        public int y;

        /** already sorted, true if the edges has been sorted */
        public boolean sorted;

        /** true if visited in dual graph */
        public boolean visited;

        public Vertex(Object obj, int x) {
            this.x = x;
            this.object = obj;
        }

        /** Iterator over edges
   
         */
        public Iterator edges() {
            if (!sorted) {
                Collections.sort(edgesFrom);
                sorted = true;
            }

            return edgesFrom.iterator();
        }

        /** Comparing based on value of Y.
         */
        public int compareTo(Vertex o) {
            return o.y - y;
        }
    }
}
