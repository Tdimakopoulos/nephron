/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.singleton;

/**
 *
 * @author tdim
 */
public class FailSafe {

    public boolean CheckUnique(Object s1, Object s2) {

        if (s1 == s2) {
            return true;
        } else {
            return false;
        }
    }

    public Object GetClass(Object T) {
        return T.getClass();
    }
    
    public String GetCurrentMethod()
    {
        String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        
        return methodName;
    }
    
    public String GetCurrentMethodInPosition(int i)
    {
        String methodName = Thread.currentThread().getStackTrace()[i].getMethodName();
        
        return methodName;
    }
    
    public int GetStactSize()
    {
        return Thread.currentThread().getStackTrace().length;
    }
    
    public Object GetStackClass()
    {
        return Thread.currentThread().getStackTrace()[1].getClass();
    }
    
    public Object GetStackClassInPosition(int i)
    {
        return Thread.currentThread().getStackTrace()[i].getClass();
    }
    
    
    /**
     * Reallocates an array with a new size, and copies the contents of the old
     * array to the new array.
     *
     * oldArray the old array, to be reallocated.
     * newSize the new array size.
     * A new array with the same contents.
     */
    private static Object resizeArray(Object oldArray, int newSize) {
        int oldSize = java.lang.reflect.Array.getLength(oldArray);
        Class elementType = oldArray.getClass().getComponentType();
        Object newArray = java.lang.reflect.Array.newInstance(
                elementType, newSize);
        int preserveLength = Math.min(oldSize, newSize);
        if (preserveLength > 0) {
            System.arraycopy(oldArray, 0, newArray, 0, preserveLength);
        }
        return newArray;
    }
}
