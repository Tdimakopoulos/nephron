/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.singleton;

public class ClassicSingleton {
   private static ClassicSingleton instance = null;
   
   //Constructor Not to be accessed from outisde
   protected ClassicSingleton() {
      // Exists only to defeat instantiation.
   }
   
   //Use this to create new Instance of the Basic Singleton Class
   public static ClassicSingleton getInstance() {
      if(instance == null) {
         instance = new ClassicSingleton();
      }
      return instance;
   }
   
}
