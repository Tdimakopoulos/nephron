/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.singleton;

public class SingletonThreadSafe {
  private static Singleton singleton = null;
  private static boolean firstThread = true;
  
  protected SingletonThreadSafe() {
    // Exists only to defeat instantiation.
  }
  
  public synchronized static Singleton getInstance() {
     if(singleton == null) {
        simulateRandomActivity();
        singleton = new Singleton();
     }
     System.out.println("created singleton: " + singleton);
     return singleton;
  }
  
  private static void simulateRandomActivity() {
     try {
        if(firstThread) {
           firstThread = false;
           System.out.println("sleeping...");
           // This nap should give the second thread enough time
           // to get by the first thread.
             Thread.currentThread().sleep(50);
       }
     }
     catch(InterruptedException ex) {
        System.out.println("Sleep interrupted");
     }
  }
  
}
