/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.singleton;

import java.util.HashMap;


public class SingletonRegistryCore {
   public static SingletonRegistry REGISTRY = new SingletonRegistry();
   private static HashMap map = new HashMap();

   protected SingletonRegistryCore() {
      // Exists to defeat instantiation
   }
   public static synchronized Object getInstance(String classname) {
      Object singleton = map.get(classname);
      if(singleton != null) {
         return singleton;
      }
      try {
         singleton = Class.forName(classname).newInstance();
         System.out.println("Info : created singleton: " + singleton);
      }
      catch(ClassNotFoundException cnf) {
         System.out.println("FATAL : Couldn't find class " + classname);    
      }
      catch(InstantiationException ie) {
         System.out.println("FATAL : Couldn't instantiate an object of type " + 
                       classname);    
      }
      catch(IllegalAccessException ia) {
         System.out.println("FATAL : Couldn't access class " + classname);    
      }
      map.put(classname, singleton);
      return singleton;
   }
}
