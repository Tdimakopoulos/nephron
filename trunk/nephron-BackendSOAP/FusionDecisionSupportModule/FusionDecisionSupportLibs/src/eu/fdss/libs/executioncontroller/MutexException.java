/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.executioncontroller;

public class MutexException extends Exception {
    static final long serialVersionUID = 2806363561939985219L;

    /** encapsulate exception*/
    private Exception ex;

    /** Create an encapsulated exception.
    * @param ex the exception
    */
    public MutexException(Exception ex) {
        super(ex.toString());
        this.ex = ex;
    }

    /** Get the encapsulated exception.
    * @return the exception
    */
    public Exception getException() {
        return ex;
    }

    public Throwable getCause() {
        return ex;
    }

}
