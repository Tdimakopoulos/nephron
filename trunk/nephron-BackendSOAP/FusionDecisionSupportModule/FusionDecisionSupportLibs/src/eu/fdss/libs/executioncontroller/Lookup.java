/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.fdss.libs.executioncontroller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * A general registry permitting clients to find instances of services
 */
public abstract class Lookup {
    /** A dummy lookup that never returns any results.
     */
    public static final Lookup EMPTY = new Empty();

    /** default instance */
    private static Lookup defaultLookup;

    /** Empty constructor for use by subclasses. */
    public Lookup() {
    }

    /** Static method to obtain the global lookup in the whole system.
     */
    public static synchronized Lookup getDefault() {
        if (defaultLookup != null) {
            return defaultLookup;
        }

        // You can specify a Lookup impl using a system property if you like.
        String className = System.getProperty("eu.fdss.service.providers" // NOI18N
            );

        if ("-".equals(className)) { // NOI18N

            // Suppress even MetaInfServicesLookup.
            return EMPTY;
        }

        ClassLoader l = Thread.currentThread().getContextClassLoader();

        try {
            if (className != null) {
                defaultLookup = (Lookup) Class.forName(className, true, l).newInstance();

                return defaultLookup;
            }
        } catch (Exception e) {
            // do not use ErrorManager because we are in the startup code
            // and ErrorManager might not be ready
            e.printStackTrace();
        }

        

    
        return defaultLookup;
    }
    
    

    /** Look up an object matching a given interface.
     
     */
    public abstract <T> T lookup(Class<T> clazz);

    /** The general lookup method. Callers can get list of all instances and classes
     */
    public abstract <T> Result<T> lookup(Template<T> template);

    /** Look up the first item matching a given template.
     
     */
    public <T> Item<T> lookupItem(Template<T> template) {
        Result<T> res = lookup(template);
        Iterator<? extends Item<T>> it = res.allItems().iterator();
        return it.hasNext() ? it.next() : null;
    }

    /**
     * Find a result corresponding to a given class.
     */
    public <T> Lookup.Result<T> lookupResult(Class<T> clazz) {
        return lookup(new Lookup.Template<T>(clazz));
    }

    /**
     * Find all instances corresponding to a given class.
     
     */
    public <T> Collection<? extends T> lookupAll(Class<T> clazz) {
        return lookupResult(clazz).allInstances();
    }

    /**
     * Objects implementing interface Lookup.Provider are capable of
     * and willing to provide a lookup (usually bound to the object).
          */
    public interface Provider {
        /**
         * Returns lookup associated with the object.
              */
        Lookup getLookup();
    }

    
    
    public static final class Template<T> extends Object {
        /** cached hash code */
        private int hashCode;

        /** type of the service */
        private Class<T> type;

        /** identity to search for */
        private String id;

        /** instance to search for */
        private T instance;

        /** General template to find all possible instances.
    
         */
        @Deprecated
        public Template() {
            this(null);
        }

        /** Create a simple template matching by class.
    
         */
        public Template(Class<T> type) {
            this(type, null, null);
        }

        /** Constructor to create new template.
         */
        public Template(Class<T> type, String id, T instance) {
            this.type = extractType(type);
            this.id = id;
            this.instance = instance;
        }

        @SuppressWarnings("unchecked")
        private Class<T> extractType(Class<T> type) {
            return (type == null) ? (Class<T>)Object.class : type;
        }

        /** Get the class (or superclass or interface) to search for.
         */
        public Class<T> getType() {
            return type;
        }

        /** Get the persistent identifier being searched for, if any.
         */
        public String getId() {
            return id;
        }

        /** Get the specific instance being searched for, if any.
         */
        public T getInstance() {
            return instance;
        }

        /* Computes hashcode for this template. The hashcode is cached.
        */
        @Override
        public int hashCode() {
            if (hashCode != 0) {
                return hashCode;
            }

            hashCode = ((type == null) ? 1 : type.hashCode()) + ((id == null) ? 2 : id.hashCode()) +
                ((instance == null) ? 3 : 0);

            return hashCode;
        }

        /* Checks whether two templates represent the same query.
         */
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Template)) {
                return false;
            }

            Template t = (Template) obj;

            if (hashCode() != t.hashCode()) {
                // this is an optimalization - the hashCodes should have been
                // precomputed
                return false;
            }

            if (type != t.type) {
                return false;
            }

            if (id == null) {
                if (t.id != null) {
                    return false;
                }
            } else {
                if (!id.equals(t.id)) {
                    return false;
                }
            }

            if (instance == null) {
                return (t.instance == null);
            } else {
                return instance.equals(t.instance);
            }
        }

        /* for debugging */
        @Override
        public String toString() {
            return "Lookup.Template[type=" + type + ",id=" + id + ",instance=" + instance + "]"; // NOI18N
        }
    }

    /** Result of a lookup request.
     */
    public static abstract class Result<T> extends Object {
        /** Registers a listener that is invoked when there is a possible
         */
        public abstract Collection<? extends T> allInstances();

        /** Get all classes represented in the result.
         
         */
        public Set<Class<? extends T>> allClasses() {
            return Collections.emptySet();
        }

        /** Get all registered items.
         
         */
        public Collection<? extends Item<T>> allItems() {
            return Collections.emptyList();
        }
    }

    /** A single item in a lookup result.
     
     */
    public static abstract class Item<T> extends Object {
        /** Get the instance itself.
     
         */
        public abstract T getInstance();

        /** Get the implementing class of the instance.
     
         */
        public abstract Class<? extends T> getType();

     

        /** Get a persistent indentifier for the item.
     
         */
        public abstract String getId();

        /** Get a human presentable name for the item.
         */
        public abstract String getDisplayName();

        /* show ID for debugging */
        @Override
        public String toString() {
            return getId();
        }
    }

    //
    // Implementation of the default lookup
    //
    private static final class Empty extends Lookup {
        private static final Result NO_RESULT = new Result() {
     
                public Collection allInstances() {
                    return Collections.EMPTY_SET;
                }
            };

        Empty() {
        }

        public <T> T lookup(Class<T> clazz) {
            return null;
        }

        @SuppressWarnings("unchecked")
        public <T> Result<T> lookup(Template<T> template) {
            return NO_RESULT;
        }
    }
}
