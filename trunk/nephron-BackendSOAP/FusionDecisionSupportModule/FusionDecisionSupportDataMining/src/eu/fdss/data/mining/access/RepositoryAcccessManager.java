/*
 * Rapidminer project needs the rapidminer jar files locate in rapidminer.home/lib
 */
package eu.fdss.data.mining.access;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;

import com.rapidminer.RapidMiner;
import com.rapidminer.RapidMiner.ExecutionMode;
import com.rapidminer.repository.Repository;
import com.rapidminer.repository.RepositoryLocation;
import com.rapidminer.repository.RepositoryManager;
import com.rapidminer.repository.remote.RemoteRepository;
import com.rapidminer.tools.FileSystemService;
import com.rapidminer.tools.LogService;

/**
 *
 * @author tdim
 */
public class RepositoryAcccessManager {

    /**
     * Singleton instance
     */
    private volatile RepositoryAcccessManager INSTANCE = null;
    /**
     * File name for the properties
     */
    public String PROPERTY_RUN_FILE = "nephron.properties";
    /**
     * Property name for the URL to the test repository
     */
    public String PROPERTY_RUN_REPOSITORY_URL = "rapidminer.nephron.repository.url";
    /**
     * Property name for the location to the test repository
     */
    public String PROPERTY_RUN_REPOSITORY_LOCATION = "rapidminer.nephron.repository.location";
    /**
     * Property name for the user name for the test repository
     */
    public String PROPERTY_RUN_REPOSITORY_USER = "rapidminer.nephron.repository.user";
    /**
     * Property name for the password for the test repository
     */
    public String PROPERTY_RUN_REPOSITORY_PASSWORD = "rapidminer.nephron.repository.password";
    /**
     * A regular expression. It is matched against the process location
     * (including the process name) relative to
     * {@value #PROPERTY_RUN_REPOSITORY_LOCATION}. If it matches, the process
     * is NOT tested. Please note that the entire string must be matched. That
     * means if you wanted to exclude any processes which contain the substring
     * "ignore" the expression must be something like ".*ignore.*" .
     */
    private final String PROPERTY_RUN_REPOSITORY_EXCLUDE = "rapidminer.nephron.repository.exclude";
    /**
     * Displayed repostiory alias.
     */
    public String REPOSITORY_ALIAS = "FusionDecisionSupport";
    private boolean initialized = false;
    private boolean repositoryPresent = false;
    private Repository repository;
    private RepositoryLocation repositoryLocation;
    private String processExclusionPattern = null;

    /**
     * Does not allow external instantiation
     */
    private RepositoryAcccessManager() {
    }

    /**
     * Returns the singleton instance of the context
     *
     
     */
    public RepositoryAcccessManager get() {
        if (INSTANCE == null) {
            synchronized (RepositoryAcccessManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RepositoryAcccessManager();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Initializes RapidMiner and tries to fetch the information for the test
     * repository.
     */
    public void initRapidMiner() {

        if (!isInitialized()) {
            File testConfigFile = FileSystemService.getUserConfigFile(PROPERTY_RUN_FILE);

            Properties properties = new Properties();
            if (testConfigFile.exists()) {
                FileInputStream in;
                try {
                    in = new FileInputStream(testConfigFile);
                    properties.load(in);
                    in.close();
                } catch (Exception e) {
                    throw new RuntimeException("Failed to read " + testConfigFile, e);
                }
            } else {
                properties = System.getProperties();
            }

            String repositoryUrl = properties.getProperty(PROPERTY_RUN_REPOSITORY_URL);
            String repositoryLocation = properties.getProperty(PROPERTY_RUN_REPOSITORY_LOCATION);
            String repositoryUser = properties.getProperty(PROPERTY_RUN_REPOSITORY_USER);
            String repositoryPassword = properties.getProperty(PROPERTY_RUN_REPOSITORY_PASSWORD);

            this.processExclusionPattern = properties.getProperty(PROPERTY_RUN_REPOSITORY_EXCLUDE);


            RapidMiner.setExecutionMode(ExecutionMode.TEST);
            RapidMiner.init();

            try {
                if (repositoryUrl != null && repositoryLocation != null && repositoryUser != null && repositoryPassword != null) {
                    setRepository(new RemoteRepository(new URL(repositoryUrl), REPOSITORY_ALIAS, repositoryUser, repositoryPassword.toCharArray(), true));
                    setRepositoryLocation(new RepositoryLocation(repositoryLocation));
                    RepositoryManager.getInstance(null).addRepository(getRepository());
                    setRepositoryPresent(true);
                } else {

                    LogService.getRoot().log(Level.WARNING,
                            "com.rapidminer.test.TestContext.define_system_property",
                            new Object[]{PROPERTY_RUN_REPOSITORY_URL,
                                PROPERTY_RUN_REPOSITORY_LOCATION,
                                PROPERTY_RUN_REPOSITORY_USER,
                                PROPERTY_RUN_REPOSITORY_PASSWORD,
                                PROPERTY_RUN_FILE});
                }
            } catch (Exception e) {
                setRepositoryPresent(false);
                throw new RuntimeException("Failed to intialize test repository", e);
            }

            setInitialized(true);
        }

    }

    /**
     * @param initialized the initialized to set
     */
    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    /**
     * @return the initialized
     */
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @param repository the repository to set
     */
    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    /**
     * @return the repository
     */
    public Repository getRepository() {
        return repository;
    }

    /**
     * @param repositoryLocation the repositoryLocation to set
     */
    public void setRepositoryLocation(RepositoryLocation repositoryLocation) {
        this.repositoryLocation = repositoryLocation;
    }

    /**
     * @return the repositoryLocation
     */
    public RepositoryLocation getRepositoryLocation() {
        return repositoryLocation;
    }

    /**
     * @param repositoryPresent the repositoryPresent to set
     */
    public void setRepositoryPresent(boolean repositoryPresent) {
        this.repositoryPresent = repositoryPresent;
    }

    /**
     * @return the repositoryPresent
     */
    public boolean isRepositoryPresent() {
        return repositoryPresent;
    }

    public String getProcessExclusionPattern() {
        return processExclusionPattern;
    }
}
