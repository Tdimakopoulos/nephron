/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.file.ws;

import eu.nephron.bean.FileRepositoryFacadeLocal;
import eu.nephron.encdec.encdec;
import eu.nephron.entity.FileRepository;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 * 
 */
@WebService(serviceName = "NephronECGManagerWS")
public class NephronECGManagerWS {

    @EJB
    private FileRepositoryFacadeLocal ejbRef;
    
    
    @WebMethod(operationName = "ECGTransfer")
    public String ECGTransfer(@WebParam(name = "ECG") byte[] ecg,@WebParam(name = "Length") int length,@WebParam(name = "IMEI") String imei,@WebParam(name = "Date") Long Dated) {
        
        //File manager
        NephronECGFileManager pManager = new NephronECGFileManager();
        //Zipper and Unzipper
        encdec pZipper = new encdec();
        
        //unzip
        String txt=pZipper.decompressString(ecg, length);
        String Filename=imei+Dated+".ecg";
        
        //save file
        String ret=pManager.StoreFile(txt, Filename);
        
        //update database
        FileRepository fileRepository = new FileRepository();
        fileRepository.setDdate(Dated);
        fileRepository.setFilename(Filename);
        fileRepository.setImei(imei);
        ejbRef.create(fileRepository);
        return ret;
    }
}
