/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bean;

import eu.nephron.entity.FileRepository;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface FileRepositoryFacadeLocal {

    void create(FileRepository fileRepository);

    void edit(FileRepository fileRepository);

    void remove(FileRepository fileRepository);

    FileRepository find(Object id);

    List<FileRepository> findAll();

    List<FileRepository> findRange(int[] range);

    int count();
    
}
