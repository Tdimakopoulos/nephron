/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bean;

import eu.nephron.entity.FileRepository;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class FileRepositoryFacade extends AbstractFacade<FileRepository> implements FileRepositoryFacadeLocal {
    @PersistenceContext(unitName = "NephronFileAttachmentsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FileRepositoryFacade() {
        super(FileRepository.class);
    }
    
}
