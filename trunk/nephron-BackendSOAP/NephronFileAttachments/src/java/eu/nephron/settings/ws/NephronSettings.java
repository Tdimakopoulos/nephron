/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.settings.ws;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "NephronSettings")
public class NephronSettings {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "GetECGPath")
    public String GetECGPath() {
        return "/home/exodus/nephron/wsfile/";
    }
    
    
}
