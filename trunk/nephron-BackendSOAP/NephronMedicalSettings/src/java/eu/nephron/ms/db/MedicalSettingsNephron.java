/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ms.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tom
 */
@Entity
public class MedicalSettingsNephron implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long dDoctorID;
    private Long dPatientID;
    private boolean dGeneric;
    private Long dUpperLimit;
    private Long dLowLimit;
    private Long dAvgLimit;
    private Long dOtherLimit1;
    private Long dOtherLimit2;
    private Long dOtherLimit3;
    private Long dOtherLimit4;
    private int ilimittype;
    private String sText1;
    private String sText2;
    private String sText3;
    private String sText4;
    private int ivalue1;
    private int ivalue2;
    private int ivalue3;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicalSettingsNephron)) {
            return false;
        }
        MedicalSettingsNephron other = (MedicalSettingsNephron) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.ms.db.MedicalSettingsNephron[ id=" + id + " ]";
    }

    /**
     * @return the dDoctorID
     */
    public Long getdDoctorID() {
        return dDoctorID;
    }

    /**
     * @param dDoctorID the dDoctorID to set
     */
    public void setdDoctorID(Long dDoctorID) {
        this.dDoctorID = dDoctorID;
    }

    /**
     * @return the dPatientID
     */
    public Long getdPatientID() {
        return dPatientID;
    }

    /**
     * @param dPatientID the dPatientID to set
     */
    public void setdPatientID(Long dPatientID) {
        this.dPatientID = dPatientID;
    }

    /**
     * @return the dGeneric
     */
    public boolean isdGeneric() {
        return dGeneric;
    }

    /**
     * @param dGeneric the dGeneric to set
     */
    public void setdGeneric(boolean dGeneric) {
        this.dGeneric = dGeneric;
    }

    /**
     * @return the dUpperLimit
     */
    public Long getdUpperLimit() {
        return dUpperLimit;
    }

    /**
     * @param dUpperLimit the dUpperLimit to set
     */
    public void setdUpperLimit(Long dUpperLimit) {
        this.dUpperLimit = dUpperLimit;
    }

    /**
     * @return the dLowLimit
     */
    public Long getdLowLimit() {
        return dLowLimit;
    }

    /**
     * @param dLowLimit the dLowLimit to set
     */
    public void setdLowLimit(Long dLowLimit) {
        this.dLowLimit = dLowLimit;
    }

    /**
     * @return the dAvgLimit
     */
    public Long getdAvgLimit() {
        return dAvgLimit;
    }

    /**
     * @param dAvgLimit the dAvgLimit to set
     */
    public void setdAvgLimit(Long dAvgLimit) {
        this.dAvgLimit = dAvgLimit;
    }

    /**
     * @return the dOtherLimit1
     */
    public Long getdOtherLimit1() {
        return dOtherLimit1;
    }

    /**
     * @param dOtherLimit1 the dOtherLimit1 to set
     */
    public void setdOtherLimit1(Long dOtherLimit1) {
        this.dOtherLimit1 = dOtherLimit1;
    }

    /**
     * @return the dOtherLimit2
     */
    public Long getdOtherLimit2() {
        return dOtherLimit2;
    }

    /**
     * @param dOtherLimit2 the dOtherLimit2 to set
     */
    public void setdOtherLimit2(Long dOtherLimit2) {
        this.dOtherLimit2 = dOtherLimit2;
    }

    /**
     * @return the dOtherLimit3
     */
    public Long getdOtherLimit3() {
        return dOtherLimit3;
    }

    /**
     * @param dOtherLimit3 the dOtherLimit3 to set
     */
    public void setdOtherLimit3(Long dOtherLimit3) {
        this.dOtherLimit3 = dOtherLimit3;
    }

    /**
     * @return the dOtherLimit4
     */
    public Long getdOtherLimit4() {
        return dOtherLimit4;
    }

    /**
     * @param dOtherLimit4 the dOtherLimit4 to set
     */
    public void setdOtherLimit4(Long dOtherLimit4) {
        this.dOtherLimit4 = dOtherLimit4;
    }

    /**
     * @return the ilimittype
     */
    public int getIlimittype() {
        return ilimittype;
    }

    /**
     * @param ilimittype the ilimittype to set
     */
    public void setIlimittype(int ilimittype) {
        this.ilimittype = ilimittype;
    }

    /**
     * @return the sText1
     */
    public String getsText1() {
        return sText1;
    }

    /**
     * @param sText1 the sText1 to set
     */
    public void setsText1(String sText1) {
        this.sText1 = sText1;
    }

    /**
     * @return the sText2
     */
    public String getsText2() {
        return sText2;
    }

    /**
     * @param sText2 the sText2 to set
     */
    public void setsText2(String sText2) {
        this.sText2 = sText2;
    }

    /**
     * @return the sText3
     */
    public String getsText3() {
        return sText3;
    }

    /**
     * @param sText3 the sText3 to set
     */
    public void setsText3(String sText3) {
        this.sText3 = sText3;
    }

    /**
     * @return the sText4
     */
    public String getsText4() {
        return sText4;
    }

    /**
     * @param sText4 the sText4 to set
     */
    public void setsText4(String sText4) {
        this.sText4 = sText4;
    }

    /**
     * @return the ivalue1
     */
    public int getIvalue1() {
        return ivalue1;
    }

    /**
     * @param ivalue1 the ivalue1 to set
     */
    public void setIvalue1(int ivalue1) {
        this.ivalue1 = ivalue1;
    }

    /**
     * @return the ivalue2
     */
    public int getIvalue2() {
        return ivalue2;
    }

    /**
     * @param ivalue2 the ivalue2 to set
     */
    public void setIvalue2(int ivalue2) {
        this.ivalue2 = ivalue2;
    }

    /**
     * @return the ivalue3
     */
    public int getIvalue3() {
        return ivalue3;
    }

    /**
     * @param ivalue3 the ivalue3 to set
     */
    public void setIvalue3(int ivalue3) {
        this.ivalue3 = ivalue3;
    }
    
}
