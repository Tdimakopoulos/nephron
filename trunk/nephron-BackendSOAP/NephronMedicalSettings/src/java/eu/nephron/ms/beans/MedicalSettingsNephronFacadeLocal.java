/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ms.beans;

import eu.nephron.ms.db.MedicalSettingsNephron;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tom
 */
@Local
public interface MedicalSettingsNephronFacadeLocal {

    void create(MedicalSettingsNephron medicalSettingsNephron);

    void edit(MedicalSettingsNephron medicalSettingsNephron);

    void remove(MedicalSettingsNephron medicalSettingsNephron);

    MedicalSettingsNephron find(Object id);

    List<MedicalSettingsNephron> findAll();

    List<MedicalSettingsNephron> findRange(int[] range);

    int count();
    
}
