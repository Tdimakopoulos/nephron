/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ms.beans;

import eu.nephron.ms.db.MedicalSettingsNephron;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tom
 */
@Stateless
public class MedicalSettingsNephronFacade extends AbstractFacade<MedicalSettingsNephron> implements MedicalSettingsNephronFacadeLocal {
    @PersistenceContext(unitName = "NephronMedicalSettingsPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MedicalSettingsNephronFacade() {
        super(MedicalSettingsNephron.class);
    }
    
}
