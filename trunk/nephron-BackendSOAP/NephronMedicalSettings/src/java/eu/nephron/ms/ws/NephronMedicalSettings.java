/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.ms.ws;

import eu.nephron.ms.beans.MedicalSettingsNephronFacadeLocal;
import eu.nephron.ms.db.MedicalSettingsNephron;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tom
 */
@WebService(serviceName = "NephronMedicalSettings")
@Stateless()
public class NephronMedicalSettings {
    @EJB
    private MedicalSettingsNephronFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "NephronMedicalSettingscreate")
    @Oneway
    public void NephronMedicalSettingscreate(@WebParam(name = "medicalSettingsNephron") MedicalSettingsNephron medicalSettingsNephron) {
        ejbRef.create(medicalSettingsNephron);
    }

    @WebMethod(operationName = "NephronMedicalSettingsedit")
    @Oneway
    public void NephronMedicalSettingsedit(@WebParam(name = "medicalSettingsNephron") MedicalSettingsNephron medicalSettingsNephron) {
        ejbRef.edit(medicalSettingsNephron);
    }

    @WebMethod(operationName = "NephronMedicalSettingsremove")
    @Oneway
    public void NephronMedicalSettingsremove(@WebParam(name = "medicalSettingsNephron") MedicalSettingsNephron medicalSettingsNephron) {
        ejbRef.remove(medicalSettingsNephron);
    }

    @WebMethod(operationName = "NephronMedicalSettingsfind")
    public MedicalSettingsNephron NephronMedicalSettingsfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "NephronMedicalSettingsfindAll")
    public List<MedicalSettingsNephron> NephronMedicalSettingsfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "NephronMedicalSettingsfindRange")
    public List<MedicalSettingsNephron> NephronMedicalSettingsfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "NephronMedicalSettingscount")
    public int NephronMedicalSettingscount() {
        return ejbRef.count();
    }
    
}
