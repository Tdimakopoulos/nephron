/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.server.beans;

import eu.nephron.dss.db.DSSServerRuleResults;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tom
 */
@Local
public interface DSSServerRuleResultsFacadeLocal {

    void create(DSSServerRuleResults dSSServerRuleResults);

    void edit(DSSServerRuleResults dSSServerRuleResults);

    void remove(DSSServerRuleResults dSSServerRuleResults);

    DSSServerRuleResults find(Object id);

    List<DSSServerRuleResults> findAll();

    List<DSSServerRuleResults> findRange(int[] range);

    int count();
    
}
