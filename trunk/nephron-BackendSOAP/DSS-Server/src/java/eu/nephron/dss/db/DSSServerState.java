/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tom
 */
@Entity
public class DSSServerState implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String status_dss_server;
    private String state1;
    private String state2;
    private String state3;
    private int statei1;
    private int statei2;
    private int statei3;
    private int statei4;
    private boolean stateb1;
    private boolean stateb2;
    private boolean stateb3;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DSSServerState)) {
            return false;
        }
        DSSServerState other = (DSSServerState) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.dss.db.DSSServerState[ id=" + id + " ]";
    }

    /**
     * @return the status_dss_server
     */
    public String getStatus_dss_server() {
        return status_dss_server;
    }

    /**
     * @param status_dss_server the status_dss_server to set
     */
    public void setStatus_dss_server(String status_dss_server) {
        this.status_dss_server = status_dss_server;
    }

    /**
     * @return the state1
     */
    public String getState1() {
        return state1;
    }

    /**
     * @param state1 the state1 to set
     */
    public void setState1(String state1) {
        this.state1 = state1;
    }

    /**
     * @return the state2
     */
    public String getState2() {
        return state2;
    }

    /**
     * @param state2 the state2 to set
     */
    public void setState2(String state2) {
        this.state2 = state2;
    }

    /**
     * @return the state3
     */
    public String getState3() {
        return state3;
    }

    /**
     * @param state3 the state3 to set
     */
    public void setState3(String state3) {
        this.state3 = state3;
    }

    /**
     * @return the statei1
     */
    public int getStatei1() {
        return statei1;
    }

    /**
     * @param statei1 the statei1 to set
     */
    public void setStatei1(int statei1) {
        this.statei1 = statei1;
    }

    /**
     * @return the statei2
     */
    public int getStatei2() {
        return statei2;
    }

    /**
     * @param statei2 the statei2 to set
     */
    public void setStatei2(int statei2) {
        this.statei2 = statei2;
    }

    /**
     * @return the statei3
     */
    public int getStatei3() {
        return statei3;
    }

    /**
     * @param statei3 the statei3 to set
     */
    public void setStatei3(int statei3) {
        this.statei3 = statei3;
    }

    /**
     * @return the statei4
     */
    public int getStatei4() {
        return statei4;
    }

    /**
     * @param statei4 the statei4 to set
     */
    public void setStatei4(int statei4) {
        this.statei4 = statei4;
    }

    /**
     * @return the stateb1
     */
    public boolean isStateb1() {
        return stateb1;
    }

    /**
     * @param stateb1 the stateb1 to set
     */
    public void setStateb1(boolean stateb1) {
        this.stateb1 = stateb1;
    }

    /**
     * @return the stateb2
     */
    public boolean isStateb2() {
        return stateb2;
    }

    /**
     * @param stateb2 the stateb2 to set
     */
    public void setStateb2(boolean stateb2) {
        this.stateb2 = stateb2;
    }

    /**
     * @return the stateb3
     */
    public boolean isStateb3() {
        return stateb3;
    }

    /**
     * @param stateb3 the stateb3 to set
     */
    public void setStateb3(boolean stateb3) {
        this.stateb3 = stateb3;
    }
    
}
