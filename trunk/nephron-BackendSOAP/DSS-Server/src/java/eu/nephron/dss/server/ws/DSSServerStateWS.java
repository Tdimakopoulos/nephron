/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.server.ws;

import eu.nephron.dss.db.DSSServerState;
import eu.nephron.dss.server.beans.DSSServerStateFacadeLocal;
import eu.nephron.info.dssinfo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tom
 */
@WebService(serviceName = "DSSServerStateWS")
@Stateless()
public class DSSServerStateWS {
    @EJB
    private DSSServerStateFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "DSSServerStateStartServer")
    @Oneway
    public void DSSServerStateStartServer() {
        dssinfo pserver = new dssinfo();
        pserver.PrintStartupInfo();
    }
    
    @WebMethod(operationName = "DSSServerStatecreate")
    @Oneway
    public void DSSServerStatecreate(@WebParam(name = "dSSServerState") DSSServerState dSSServerState) {
        ejbRef.create(dSSServerState);
    }

    @WebMethod(operationName = "DSSServerStateedit")
    @Oneway
    public void DSSServerStateedit(@WebParam(name = "dSSServerState") DSSServerState dSSServerState) {
        ejbRef.edit(dSSServerState);
    }

    @WebMethod(operationName = "DSSServerStateremove")
    @Oneway
    public void DSSServerStateremove(@WebParam(name = "dSSServerState") DSSServerState dSSServerState) {
        ejbRef.remove(dSSServerState);
    }

    @WebMethod(operationName = "DSSServerStatefind")
    public DSSServerState DSSServerStatefind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "DSSServerStatefindAll")
    public List<DSSServerState> DSSServerStatefindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "DSSServerStatefindRange")
    public List<DSSServerState> DSSServerStatefindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "DSSServerStatecount")
    public int DSSServerStatecount() {
        return ejbRef.count();
    }
    
}
