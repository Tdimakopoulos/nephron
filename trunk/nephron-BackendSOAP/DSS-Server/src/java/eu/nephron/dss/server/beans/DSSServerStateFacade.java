/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.server.beans;

import eu.nephron.dss.db.DSSServerState;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tom
 */
@Stateless
public class DSSServerStateFacade extends AbstractFacade<DSSServerState> implements DSSServerStateFacadeLocal {
    @PersistenceContext(unitName = "DSS-ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DSSServerStateFacade() {
        super(DSSServerState.class);
    }
    
}
