/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.server.ws;

import eu.nephron.dss.db.DSSServerRuleResults;
import eu.nephron.dss.server.beans.DSSServerRuleResultsFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tom
 */
@WebService(serviceName = "DSSServerRuleResultsWS")
@Stateless()
public class DSSServerRuleResultsWS {
    @EJB
    private DSSServerRuleResultsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "DSSServerRuleResultscreate")
    @Oneway
    public void DSSServerRuleResultscreate(@WebParam(name = "dSSServerRuleResults") DSSServerRuleResults dSSServerRuleResults) {
        ejbRef.create(dSSServerRuleResults);
    }

    @WebMethod(operationName = "DSSServerRuleResultsedit")
    @Oneway
    public void DSSServerRuleResultsedit(@WebParam(name = "dSSServerRuleResults") DSSServerRuleResults dSSServerRuleResults) {
        ejbRef.edit(dSSServerRuleResults);
    }

    @WebMethod(operationName = "DSSServerRuleResultsremove")
    @Oneway
    public void DSSServerRuleResultsremove(@WebParam(name = "dSSServerRuleResults") DSSServerRuleResults dSSServerRuleResults) {
        ejbRef.remove(dSSServerRuleResults);
    }

    @WebMethod(operationName = "DSSServerRuleResultsfind")
    public DSSServerRuleResults DSSServerRuleResultsfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "DSSServerRuleResultsfindAll")
    public List<DSSServerRuleResults> DSSServerRuleResultsfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "DSSServerRuleResultsfindRange")
    public List<DSSServerRuleResults> DSSServerRuleResultsfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "DSSServerRuleResultscount")
    public int DSSServerRuleResultscount() {
        return ejbRef.count();
    }
    
}
