/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dss.server.beans;

import eu.nephron.dss.db.DSSServerState;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tom
 */
@Local
public interface DSSServerStateFacadeLocal {

    void create(DSSServerState dSSServerState);

    void edit(DSSServerState dSSServerState);

    void remove(DSSServerState dSSServerState);

    DSSServerState find(Object id);

    List<DSSServerState> findAll();

    List<DSSServerState> findRange(int[] range);

    int count();
    
}
