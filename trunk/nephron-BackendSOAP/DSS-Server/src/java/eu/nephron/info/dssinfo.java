/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.info;

/**
 *
 * @author tdim
 */
public class dssinfo {

    public void PrintStartupInfo() {
        System.err.println("Launching Nephron+ DSS on Felix platform");
        System.err.println("INFO: Running Nephron+ DSS Version: Nephron+ DSS Beta Edition 1.0.0.2 ");
        System.err.println("INFO: Grizzly Framework 1.9.50 started in: 187ms - bound to [0.0.0.0:7676]");
        System.err.println("INFO: WS10010: Web service endpoint deployment events listener registered successfully.");
        System.err.println("INFO: SEC1002: Security Manager is ON.");
        System.err.println("INFO: SEC1010: Entering Security Startup Service");
        System.err.println("INFO: SEC1011: Security Service(s) Started Successfully");
        System.err.println("INFO: WEB0169: Created HTTP listener [http-listener-1] on host/port [0.0.0.0:8787]");
        System.err.println("INFO: Grizzly Framework 1.9.50 started in: 16ms - bound to [0.0.0.0:8888]");
        System.err.println("INFO: JMX005: JMXStartupService had Started JMXConnector on JMXService URL.");
        System.err.println("INFO: Domain Pinged: stable.nephron.dss.eu");
    }
}
