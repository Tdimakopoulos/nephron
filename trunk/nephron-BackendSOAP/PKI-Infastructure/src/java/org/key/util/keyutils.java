/**
 * Part of The Security Library
 * 
 * Key Utils Functions
 * 
 * Functions
 * ---------------
 * todo
 * 
 * @author tdi
 */
package org.key.util;

import java.io.File;

import java.io.FileOutputStream;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.*;
import org.security.utils.Util;

public class keyutils {

    public String formatKey(Key key) {
        StringBuffer sb = new StringBuffer();
        String algo = key.getAlgorithm();
        String fmt = key.getFormat();
        byte[] encoded = key.getEncoded();
        sb.append("Key[algorithm=" + algo + ", format=" + fmt +
                ", bytes=" + encoded.length + "]\n");
        sb.append("Key Material (in hex):: ");
        sb.append(Util.byteArray2Hex(key.getEncoded()));
        return sb.toString();
    }

    public String getKey(Key key) {
        StringBuffer sb = new StringBuffer();
        sb.append(Util.byteArray2Hex(key.getEncoded()));
        return sb.toString();
    }

    public void writeKeyStoreToFile(
            final KeyStore keyStore,
            final File file,
            final char[] masterPassword)
            throws KeyStoreException, IOException,
            NoSuchAlgorithmException, CertificateException {

        final FileOutputStream out = new FileOutputStream(file);
        try {
            keyStore.store(out, masterPassword);
        } finally {
            out.close();
        }
    }
}
