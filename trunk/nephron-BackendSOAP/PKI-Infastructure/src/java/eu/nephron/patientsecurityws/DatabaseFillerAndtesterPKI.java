/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientsecurityws;

import eu.nephron.entity.patientconnections;
import eu.nephron.role.bean.patientconnectionsFacadeLocal;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "DatabaseFillerAndtesterPKI")
public class DatabaseFillerAndtesterPKI {

    @EJB
    private patientconnectionsFacadeLocal ejbRef;
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "CreatePatientConnectionsFiller")
    public String CreatePatientConnectionsFiller() {
        patientconnections pppatientconnections = new patientconnections();
        pppatientconnections.setIdpatient( new Long(3));
        pppatientconnections.setIdaddress(new Long(1));
        pppatientconnections.setIdcontact(new Long(1));
        pppatientconnections.setIdpersonnal(new Long(1));
        ejbRef.create(pppatientconnections);
        return "Created" ;
    }
    
    @WebMethod(operationName = "CreatePatientConnectionsFillerwithids")
    public String CreatePatientConnectionsFillerwithids(Long id1,Long id2,Long id3,Long id4) {
        patientconnections pppatientconnections = new patientconnections();
        pppatientconnections.setIdpatient( id1);
        pppatientconnections.setIdaddress(id2);
        pppatientconnections.setIdcontact(id3);
        pppatientconnections.setIdpersonnal(id4);
        ejbRef.create(pppatientconnections);
        return "Created" ;
    }
}
