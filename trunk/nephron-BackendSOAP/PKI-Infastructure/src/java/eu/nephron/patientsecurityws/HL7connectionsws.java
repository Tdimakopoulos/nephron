/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.patientsecurityws;

import eu.nephron.entity.HL7connections;
import eu.nephron.role.bean.HL7connectionsFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "HL7connectionsws")
public class HL7connectionsws {
    @EJB
    private HL7connectionsFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createHL7connections")
    @Oneway
    public void createHL7connections(@WebParam(name = "entity") HL7connections entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editHL7connections")
    @Oneway
    public void editHL7connections(@WebParam(name = "entity") HL7connections entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeHL7connections")
    @Oneway
    public void removeHL7connections(@WebParam(name = "entity") HL7connections entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findHL7connections")
    public HL7connections findHL7connections(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllHL7connections")
    public List<HL7connections> findAllHL7connections() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeHL7connections")
    public List<HL7connections> findRangeHL7connections(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countHL7connections")
    public int countHL7connections() {
        return ejbRef.count();
    }
    
}
