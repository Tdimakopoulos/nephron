/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.role.ws;

import eu.nephron.entity.RoleDB;
import eu.nephron.role.bean.RoleDBFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "RoleService")
public class RoleService {
    @EJB
    private RoleDBFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "roleDB") RoleDB roleDB) {
        ejbRef.create(roleDB);
    }
    
    @WebMethod(operationName = "createValues")
    @Oneway
    public void createValues(String role,String fnc) {
        RoleDB roleDB=new RoleDB();
        roleDB.setAccessfunction(fnc);
        roleDB.setRole(role);
        ejbRef.create(roleDB);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "roleDB") RoleDB roleDB) {
        ejbRef.edit(roleDB);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "roleDB") RoleDB roleDB) {
        ejbRef.remove(roleDB);
    }

    @WebMethod(operationName = "find")
    public RoleDB find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<RoleDB> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<RoleDB> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
