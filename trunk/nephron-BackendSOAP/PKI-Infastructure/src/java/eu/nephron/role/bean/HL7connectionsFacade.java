/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.role.bean;

import eu.nephron.entity.HL7connections;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7connectionsFacade extends AbstractFacade<HL7connections> {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7connectionsFacade() {
        super(HL7connections.class);
    }
    
}
