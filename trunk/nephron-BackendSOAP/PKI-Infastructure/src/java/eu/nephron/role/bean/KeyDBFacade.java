/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.role.bean;

import eu.nephron.entity.KeyDB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class KeyDBFacade extends AbstractFacade<KeyDB> {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KeyDBFacade() {
        super(KeyDB.class);
    }
    
}
