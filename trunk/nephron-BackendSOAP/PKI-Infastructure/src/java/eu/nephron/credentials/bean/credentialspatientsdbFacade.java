/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.bean;

import eu.nephron.credentials.db.credentialspatientsdb;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class credentialspatientsdbFacade extends AbstractFacade<credentialspatientsdb> implements credentialspatientsdbFacadeLocal {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public credentialspatientsdbFacade() {
        super(credentialspatientsdb.class);
    }
    
}
