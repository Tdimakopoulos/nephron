/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.patient.ws;

import eu.nephron.credentials.bean.credentialspatientsdbFacadeLocal;
import eu.nephron.credentials.db.credentialspatientsdb;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "CredentialsPatientWS")
public class CredentialsPatientWS {
    @EJB
    private credentialspatientsdbFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "credentialspatientsdb") credentialspatientsdb credentialspatientsdb) {
        ejbRef.create(credentialspatientsdb);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "credentialspatientsdb") credentialspatientsdb credentialspatientsdb) {
        ejbRef.edit(credentialspatientsdb);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "credentialspatientsdb") credentialspatientsdb credentialspatientsdb) {
        ejbRef.remove(credentialspatientsdb);
    }

    @WebMethod(operationName = "find")
    public credentialspatientsdb find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<credentialspatientsdb> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<credentialspatientsdb> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
