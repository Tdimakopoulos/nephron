/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class credentialspatientsdb implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String password;
    private Long patientid;
    private String field1;
    private String field2;
    private String field3;
    private Long id1;
    private Long id2;
    private Long id3;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof credentialspatientsdb)) {
            return false;
        }
        credentialspatientsdb other = (credentialspatientsdb) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.credentials.db.credentialsdb[ id=" + id + " ]";
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    

    /**
     * @return the field1
     */
    public String getField1() {
        return field1;
    }

    /**
     * @param field1 the field1 to set
     */
    public void setField1(String field1) {
        this.field1 = field1;
    }

    /**
     * @return the field2
     */
    public String getField2() {
        return field2;
    }

    /**
     * @param field2 the field2 to set
     */
    public void setField2(String field2) {
        this.field2 = field2;
    }

    /**
     * @return the field3
     */
    public String getField3() {
        return field3;
    }

    /**
     * @param field3 the field3 to set
     */
    public void setField3(String field3) {
        this.field3 = field3;
    }

    /**
     * @return the id1
     */
    public Long getId1() {
        return id1;
    }

    /**
     * @param id1 the id1 to set
     */
    public void setId1(Long id1) {
        this.id1 = id1;
    }

    /**
     * @return the id2
     */
    public Long getId2() {
        return id2;
    }

    /**
     * @param id2 the id2 to set
     */
    public void setId2(Long id2) {
        this.id2 = id2;
    }

    /**
     * @return the id3
     */
    public Long getId3() {
        return id3;
    }

    /**
     * @param id3 the id3 to set
     */
    public void setId3(Long id3) {
        this.id3 = id3;
    }

    /**
     * @return the patientid
     */
    public Long getPatientid() {
        return patientid;
    }

    /**
     * @param patientid the patientid to set
     */
    public void setPatientid(Long patientid) {
        this.patientid = patientid;
    }
    
}
