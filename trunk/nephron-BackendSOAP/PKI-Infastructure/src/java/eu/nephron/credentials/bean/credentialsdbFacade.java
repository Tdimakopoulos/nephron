/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.bean;

import eu.nephron.credentials.db.credentialsdb;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class credentialsdbFacade extends AbstractFacade<credentialsdb> implements credentialsdbFacadeLocal {
    @PersistenceContext(unitName = "web-jpaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public credentialsdbFacade() {
        super(credentialsdb.class);
    }
    
}
