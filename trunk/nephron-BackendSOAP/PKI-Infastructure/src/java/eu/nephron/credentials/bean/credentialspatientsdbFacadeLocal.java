/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.bean;

import eu.nephron.credentials.db.credentialspatientsdb;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface credentialspatientsdbFacadeLocal {

    void create(credentialspatientsdb credentialspatientsdb);

    void edit(credentialspatientsdb credentialspatientsdb);

    void remove(credentialspatientsdb credentialspatientsdb);

    credentialspatientsdb find(Object id);

    List<credentialspatientsdb> findAll();

    List<credentialspatientsdb> findRange(int[] range);

    int count();
    
}
