/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.ws;

import eu.nephron.credentials.bean.credentialsdbFacadeLocal;
import eu.nephron.credentials.db.credentialsdb;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "CredentialsWS")
public class CredentialsWS {
    @EJB
    private credentialsdbFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "credentialsdb") credentialsdb credentialsdb) {
        ejbRef.create(credentialsdb);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "credentialsdb") credentialsdb credentialsdb) {
        ejbRef.edit(credentialsdb);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "credentialsdb") credentialsdb credentialsdb) {
        ejbRef.remove(credentialsdb);
    }

    @WebMethod(operationName = "find")
    public credentialsdb find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<credentialsdb> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<credentialsdb> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
