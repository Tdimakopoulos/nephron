/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.credentials.bean;

import eu.nephron.credentials.db.credentialsdb;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface credentialsdbFacadeLocal {

    void create(credentialsdb credentialsdb);

    void edit(credentialsdb credentialsdb);

    void remove(credentialsdb credentialsdb);

    credentialsdb find(Object id);

    List<credentialsdb> findAll();

    List<credentialsdb> findRange(int[] range);

    int count();
    
}
