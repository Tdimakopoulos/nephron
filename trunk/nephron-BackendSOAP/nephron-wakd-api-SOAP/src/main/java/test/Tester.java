/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import eu.nephron.textconv.AlarmsMsg;
import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.incoming.AlertMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import eu.nephron.wakd.measurments.MeasurmentsIncomingAPI;
import eu.nephron.wakd.measurments.WeightIncomingAPI;
import java.sql.SQLException;

/**
 *
 * @author tdim
 */
public class Tester {
    
    public static void main(String[] args) {
        byte[] pmsg = ByteUtils.convertStringToByteArray("01001317080402E98C005200000000075BCD15");
        WeightIncomingAPI pWIAPI = new WeightIncomingAPI();
        pWIAPI.SetMSG(pmsg, true);
        pWIAPI.decode();
        if(pWIAPI.IsWeight())
        {

System.out.println("BodyFat : "+pWIAPI.getBodyfat_s());
System.out.println("Weight : "+pWIAPI.getWeight_s());
System.out.println("Battery : "+pWIAPI.getWsbattery_s());
System.out.println("Time : "+pWIAPI.getTime_s());

        }
   } 
    
    public static void main2(String[] args)
    {
        
byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C0901000000B4058D00000000002E00000000004C00000000002200000000017500000000058F00000000003000000000004D00000000002400000000016B00000000");
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C0A01000000F0058E00000000002F00000000004D00000000002300000000017600000000059000000000003100000000004E00000000002500000000016C00000000");
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C0D010000012C058F00000000003000000000004E00000000002400000000017700000000059100000000003200000000004F00000000002600000000016D00000000");
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C0E0100000168059000000000003100000000004F00000000002500000000017800000000059200000000003300000000005000000000002700000000016E00000000");
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C0F01000001A4059100000000003200000000005000000000002600000000017900000000059300000000003400000000005100000000002800000000016F00000000");	
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C1401000001E0059200000000003300000000005100000000002700000000017A00000000059400000000003500000000005200000000002900000000017000000000");
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C15010000021C059300000000003400000000005200000000002800000000017B00000000059500000000003600000000005300000000002A00000000017100000000");	
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C180100000258059400000000003500000000005300000000002900000000017C00000000059600000000003700000000005400000000002B00000000017200000000");	
//
//byte[] pmsg=ByteUtils.convertStringToByteArray("0400460C190100000294059500000000003600000000005400000000002A00000000017D00000000059700000000003800000000005500000000002C00000000017300000000");	
        
        MeasurmentsIncomingAPI pp= new MeasurmentsIncomingAPI();
        pp.SetMSG(pmsg,true);
        
        System.out.println(pp.IsPHYSIOLOGICAL());
        pp.decode();
        System.out.println((double) Integer.parseInt(pp.getInlet_s(),16));
        System.out.println(pp.getInlet_s1());
        System.out.println(pp.getInlet_s2());
        System.out.println(pp.getInlet_s3());
        System.out.println(pp.getInlet_s4());
        System.out.println(pp.getOutlet_s());
        System.out.println(pp.getOutlet_s1());
        System.out.println(pp.getOutlet_s2());
        System.out.println(pp.getOutlet_s3());
        System.out.println(pp.getOutlet_s4());
        
        byte[] pmsg2=ByteUtils.convertStringToByteArray("0400080403010101");
        CurrentStateMsg pMsg= new CurrentStateMsg();
        pMsg.decode(pmsg2);
        System.out.println(pMsg.getCurrentOpState());
        System.out.println(pMsg.getCurrentState());
        
        byte[] pmsg3=ByteUtils.convertStringToByteArray("040008080801000B");
        AlertMsg pmm= new AlertMsg();
        pmm.decode(pmsg3);
        System.out.println(pmm.getAlert());
        AlarmsMsg pp2;
        pp2 = new AlarmsMsg(pmm.GetAlertValue());
        System.out.println(pp2.getmsgAlert());
        System.out.println(pp2.getmsgDoctor());
        System.out.println(pp2.getmsgPatient());
    }
}
