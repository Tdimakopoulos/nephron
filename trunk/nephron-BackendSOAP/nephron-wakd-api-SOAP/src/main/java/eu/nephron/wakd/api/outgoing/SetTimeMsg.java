package eu.nephron.wakd.api.outgoing;

import java.util.Arrays;

import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;


public class SetTimeMsg extends OutgoingWakdMsg {

	private static final long serialVersionUID = 804233754811247934L;
	
	private long unixTime;
	
	public SetTimeMsg(int id, long unixTime) {
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.SET_TIME);
		this.header.setSize(133);
		this.unixTime = unixTime;		
	}

	public long getUnixTime() {
		return unixTime;
	}

	public void setUnixTime(long unixTime) {
		this.unixTime = unixTime;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+4);
		System.arraycopy(ByteUtils.longToByteArray(this.unixTime, 4), 0, bytes, 6, 4);
		return bytes;
	}

}
