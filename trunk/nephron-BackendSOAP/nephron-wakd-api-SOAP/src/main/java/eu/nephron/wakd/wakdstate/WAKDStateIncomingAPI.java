/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.wakdstate;

/**
 *
 * @author tdim
 */
public class WAKDStateIncomingAPI {

    byte[] Incomming = null;
    boolean gbdebug;

    public void SetMSG(byte[] msg, boolean bdebug) {
        Incomming = msg;
        gbdebug = bdebug;
    }

    public boolean IsWAKDStatus() {
        boolean bret = false;
        if (gbdebug) {
            System.out.println("Length (8) : " + Incomming.length);
            System.out.println("Type (4): " + unsignedByteToInt(Incomming[3]));
        }
        if (Incomming.length == 8) {
            if (unsignedByteToInt(Incomming[3]) == 4) {
                bret = true;

            }
        }
        //WakControl.wakdState(2, unsignedByteToInt(Incomming[6]))
        return bret;
    }

    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }
}
