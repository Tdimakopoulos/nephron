package eu.nephron.utils;

import org.apache.commons.lang.StringUtils;

public abstract class ByteUtils {

    public static byte intToByte(int value) {
        return (byte) (value & 0xFF);
    }

    public static byte longToByte(long value) {
        return (byte) (value & 0xFF);
    }

    public static byte[] intToByteArray(int value, int size) {

        switch (size) {
            case 2:
                return new byte[]{(byte) ((value & 0xFF00) >> 8), (byte) (value & 0xFF)};
            case 4:
                return new byte[]{(byte) ((value & 0xFF000000) >> 24), (byte) ((value & 0xFF0000) >> 16), (byte) ((value & 0xFF00) >> 8), (byte) (value & 0xFF)};
            default:
                throw new IllegalArgumentException("Size must be 2 or 4");
        }
    }

    public static byte[] longToByteArray(long value, int size) {
        switch (size) {
            case 2:
                return new byte[]{(byte) ((value & 0xFF00) >> 8), (byte) (value & 0xFF)};
            case 4:
                return new byte[]{(byte) ((value & 0xFF000000) >> 24), (byte) ((value & 0xFF0000) >> 16), (byte) ((value & 0xFF00) >> 8), (byte) (value & 0xFF)};
            default:
                throw new IllegalArgumentException("Size must be 2 or 4");
        }
    }

    public static final int byteArrayToInt(byte[] b, int pos, int size) {
        switch (size) {
            case 2:
                return ((b[pos++] & 0xFF) << 8) + (b[pos] & 0xFF);
            case 4:
                return (b[pos++] << 24) + ((b[pos++] & 0xFF) << 16) + ((b[pos++] & 0xFF) << 8) + (b[pos] & 0xFF);
            default:
                throw new IllegalArgumentException("Size must be 2 or 4");
        }
    }

    public static String toBinaryString(byte n) {
        StringBuilder sb = new StringBuilder("00000000");
        for (int bit = 0; bit < 8; bit++) {
            if (((n >> bit) & 1) > 0) {
                sb.setCharAt(7 - bit, '1');
            }
        }
        return sb.toString();
    }

    public static String toBinaryString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String binaryString = ByteUtils.toBinaryString(bytes[i]);
            sb.append(StringUtils.leftPad(binaryString, 8, "0"));
        }
        return sb.toString();
    }

    public static byte[] convertStringToByteArray(String s) {

        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static String convertByteArrayToString(byte[] raw) {
        final String HEXES = "0123456789ABCDEF";
        if (raw == null) {
            return null;
        }
        final StringBuilder hex = new StringBuilder(2 * raw.length);
        for (final byte b : raw) {
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F)));
        }
        return hex.toString();
    }
}
