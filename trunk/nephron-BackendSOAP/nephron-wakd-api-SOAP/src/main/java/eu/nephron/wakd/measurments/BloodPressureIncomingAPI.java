/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.measurments;

import eu.nephron.utils.ByteUtils;
import java.math.BigInteger;

/**
 *
 * @author tdim
 */
public class BloodPressureIncomingAPI {

    private byte[] Incomming = null;
    private boolean gbdebug;
    private BigInteger systolic_bi;
    private String systolic_s;
    private BigInteger diastolic_bi;
    private String diastolic_s;
    private BigInteger heartrate_bi;
    private String heartrate_s;
    private BigInteger bptime_bi;
    private String bptime_s;

    public void SetMSG(byte[] msg, boolean bdebug) {
        setIncomming(msg);
        setGbdebug(bdebug);
    }

    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

    public boolean IsBloodPressure() {
        boolean bret = false;
        if (isGbdebug()) {
            System.out.println("Length : " + getIncomming().length);
            System.out.println("Type : " + unsignedByteToInt(getIncomming()[3]));
        }

        if (unsignedByteToInt(getIncomming()[3]) == 25) {
            bret = true;

        }

        return bret;
    }

    public void decode() {
        byte[] incomingMessage = getIncomming();

        byte[] weigth = new byte[]{incomingMessage[6]};
        systolic_bi = new BigInteger(weigth);
        systolic_s = systolic_bi.toString(8);

        byte[] boydfat = new byte[]{incomingMessage[7]};
        diastolic_bi = new BigInteger(boydfat);
        diastolic_s = diastolic_bi.toString(8);

        byte[] battery = new byte[]{incomingMessage[8]};
        heartrate_bi = new BigInteger(battery);
        heartrate_s = heartrate_bi.toString(8);

        byte[] time = new byte[]{incomingMessage[9], incomingMessage[10], incomingMessage[11], incomingMessage[12]};
        bptime_bi = new BigInteger(time);
        bptime_s = bptime_bi.toString(32);



    }

    /**
     * @return the Incomming
     */
    public byte[] getIncomming() {
        return Incomming;
    }

    /**
     * @param Incomming the Incomming to set
     */
    public void setIncomming(byte[] Incomming) {
        this.Incomming = Incomming;
    }

    /**
     * @return the gbdebug
     */
    public boolean isGbdebug() {
        return gbdebug;
    }

    /**
     * @param gbdebug the gbdebug to set
     */
    public void setGbdebug(boolean gbdebug) {
        this.gbdebug = gbdebug;
    }

    /**
     * @return the systolic_bi
     */
    public BigInteger getSystolic_bi() {
        return systolic_bi;
    }

    /**
     * @param systolic_bi the systolic_bi to set
     */
    public void setSystolic_bi(BigInteger systolic_bi) {
        this.systolic_bi = systolic_bi;
    }

    /**
     * @return the systolic_s
     */
    public String getSystolic_s() {
        return systolic_s;
    }

    /**
     * @param systolic_s the systolic_s to set
     */
    public void setSystolic_s(String systolic_s) {
        this.systolic_s = systolic_s;
    }

    /**
     * @return the diastolic_bi
     */
    public BigInteger getDiastolic_bi() {
        return diastolic_bi;
    }

    /**
     * @param diastolic_bi the diastolic_bi to set
     */
    public void setDiastolic_bi(BigInteger diastolic_bi) {
        this.diastolic_bi = diastolic_bi;
    }

    /**
     * @return the diastolic_s
     */
    public String getDiastolic_s() {
        return diastolic_s;
    }

    /**
     * @param diastolic_s the diastolic_s to set
     */
    public void setDiastolic_s(String diastolic_s) {
        this.diastolic_s = diastolic_s;
    }

    /**
     * @return the heartrate_bi
     */
    public BigInteger getHeartrate_bi() {
        return heartrate_bi;
    }

    /**
     * @param heartrate_bi the heartrate_bi to set
     */
    public void setHeartrate_bi(BigInteger heartrate_bi) {
        this.heartrate_bi = heartrate_bi;
    }

    /**
     * @return the heartrate_s
     */
    public String getHeartrate_s() {
        return heartrate_s;
    }

    /**
     * @param heartrate_s the heartrate_s to set
     */
    public void setHeartrate_s(String heartrate_s) {
        this.heartrate_s = heartrate_s;
    }

    /**
     * @return the bptime_bi
     */
    public BigInteger getBptime_bi() {
        return bptime_bi;
    }

    /**
     * @param bptime_bi the bptime_bi to set
     */
    public void setBptime_bi(BigInteger bptime_bi) {
        this.bptime_bi = bptime_bi;
    }

    /**
     * @return the bptime_s
     */
    public String getBptime_s() {
        return bptime_s;
    }

    /**
     * @param bptime_s the bptime_s to set
     */
    public void setBptime_s(String bptime_s) {
        this.bptime_s = bptime_s;
    }
}
