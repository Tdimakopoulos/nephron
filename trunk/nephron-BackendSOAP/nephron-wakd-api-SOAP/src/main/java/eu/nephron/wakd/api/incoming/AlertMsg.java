package eu.nephron.wakd.api.incoming;

import java.util.Arrays;

import eu.nephron.wakd.api.Header;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.enums.WakdAlertEnum;
import eu.nephron.wakd.api.enums.WakdCommandEnum;


public class AlertMsg extends IncomingWakdMsg {

	private static final long serialVersionUID = -8522286386866209503L;
	byte alertvalue;
	public AlertMsg() {
		super();
	}
	
	public AlertMsg(int id, WakdAlertEnum alert) {
		super();
		this.header.setId(id);
		this.header.setCommand(WakdCommandEnum.SYSTEM_INFO);
		this.header.setSize(8);
		this.alert = alert;
	}

	private WakdAlertEnum alert;
	
	public WakdAlertEnum getAlert() {
		return alert;
	}

	public void setAlert(WakdAlertEnum alert) {
		this.alert = alert;
	}

	public byte[] encode() {
		byte[] bytes = Arrays.copyOf(header.encode(), Header.HEADER_SIZE+2);
		bytes[6] = (byte)0;
		bytes[7] = alert.getValue();
		return bytes;
	}

	@Override
	public void decode(byte[] bytes) {
		this.header.decode(bytes);
		this.alert = WakdAlertEnum.getWakdAlertEnum(bytes[7]);
                alertvalue=bytes[7];
	}
        public byte GetAlertValue()
        {
            return alertvalue;
        }

}
