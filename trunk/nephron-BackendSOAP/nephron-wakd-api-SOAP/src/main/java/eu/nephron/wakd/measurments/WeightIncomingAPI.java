/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.measurments;

import eu.nephron.utils.ByteUtils;
import java.math.BigInteger;

/**
 *
 * @author tdim
 */
public class WeightIncomingAPI {

    private byte[] Incomming = null;
    private boolean gbdebug;
    
    private String weight_s;
    
    private String bodyfat_s;
    
    private String wsbattery_s;
    
    private String time_s;

    public void SetMSG(byte[] msg, boolean bdebug) {
        setIncomming(msg);
        setGbdebug(bdebug);
    }

    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

    public boolean IsWeight() {
        boolean bret = false;
        if (isGbdebug()) {
            System.out.println("Length : " + getIncomming().length);
            System.out.println("Type : " + unsignedByteToInt(getIncomming()[3]));
        }

        if (unsignedByteToInt(getIncomming()[3]) == 23) {
            bret = true;

        }

        return bret;
    }

    public void decode() {
        byte[] incomingMessage = getIncomming();

        byte[] weigth = new byte[]{incomingMessage[6], incomingMessage[7]};
        int iweight = ByteUtils.byteArrayToInt(getIncomming(), 6, 2);///10;
        long lweight = Long.parseLong(String.valueOf(iweight));
        double Dweight = lweight / 10.00;

        System.out.println("-->" + lweight + " " + Dweight);
        weight_s = String.valueOf(Dweight);
        


        byte[] boydfat = new byte[]{incomingMessage[8]};
        int ifat = unsignedByteToInt(getIncomming()[8]);///10;
        long lfat = Long.parseLong(String.valueOf(ifat));
        double Dfat = lfat / 10.00;
        bodyfat_s = String.valueOf(Dfat);
        


        byte[] battery = new byte[]{incomingMessage[9], incomingMessage[10]};
        int ibat = ByteUtils.byteArrayToInt(getIncomming(), 9, 2);
        wsbattery_s = String.valueOf(ibat);
        

        byte[] time = new byte[]{incomingMessage[15], incomingMessage[16], incomingMessage[17], incomingMessage[18]};
        int itime = ByteUtils.byteArrayToInt(getIncomming(), 15, 4);

        time_s = String.valueOf(itime);
        
    }

    /**
     * @return the Incomming
     */
    public byte[] getIncomming() {
        return Incomming;
    }

    /**
     * @param Incomming the Incomming to set
     */
    public void setIncomming(byte[] Incomming) {
        this.Incomming = Incomming;
    }

    /**
     * @return the gbdebug
     */
    public boolean isGbdebug() {
        return gbdebug;
    }

    /**
     * @param gbdebug the gbdebug to set
     */
    public void setGbdebug(boolean gbdebug) {
        this.gbdebug = gbdebug;
    }

    /**
     * @return the weight_s
     */
    public String getWeight_s() {
        return weight_s;
    }

    /**
     * @param weight_s the weight_s to set
     */
    public void setWeight_s(String weight_s) {
        this.weight_s = weight_s;
    }

    /**
     * @return the bodyfat_s
     */
    public String getBodyfat_s() {
        return bodyfat_s;
    }

    /**
     * @param bodyfat_s the bodyfat_s to set
     */
    public void setBodyfat_s(String bodyfat_s) {
        this.bodyfat_s = bodyfat_s;
    }

    /**
     * @return the wsbattery_s
     */
    public String getWsbattery_s() {
        return wsbattery_s;
    }

    /**
     * @param wsbattery_s the wsbattery_s to set
     */
    public void setWsbattery_s(String wsbattery_s) {
        this.wsbattery_s = wsbattery_s;
    }

    /**
     * @return the time_s
     */
    public String getTime_s() {
        return time_s;
    }

    /**
     * @param time_s the time_s to set
     */
    public void setTime_s(String time_s) {
        this.time_s = time_s;
    }
}
