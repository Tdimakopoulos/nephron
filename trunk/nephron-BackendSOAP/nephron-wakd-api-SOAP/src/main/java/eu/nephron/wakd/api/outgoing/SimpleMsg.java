package eu.nephron.wakd.api.outgoing;

import eu.nephron.wakd.api.OutgoingWakdMsg;
import eu.nephron.wakd.api.enums.WakdCommandEnum;


public class SimpleMsg extends OutgoingWakdMsg {

	private static final long serialVersionUID = 8598583075931094670L;

	private SimpleMsg(int id, WakdCommandEnum command) {
		this.header.setId(id);
		this.header.setCommand(command);
		this.header.setSize(6);
	}

	@Override
	public byte[] encode() {
		return header.encode();
	}

}
