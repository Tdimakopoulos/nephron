/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.measurments;

import eu.nephron.wakd.math.nephronmaths;
import java.math.BigInteger;

/**
 *
 * @author tdim
 */
public class MeasurmentsPhysicalIncomingAPI {

    private byte[] Incomming = null;
    private boolean gbdebug;
    private String PressureFCI;
    private String PressureFCO;
    private String PressureBCI;
    private String PressureBCO;
    private String TempIN;
    private String TempOut;
    private String CondFCR;
    private String CondPT;
    private BigInteger b_PressureFCI;
    private BigInteger b_PressureFCO;
    private BigInteger b_PressureBCI;
    private BigInteger b_PressureBCO;
    private BigInteger b_TempIN;
    private BigInteger b_TempOut;
    private BigInteger b_CondFCR;
    private BigInteger b_CondPT;

    public void SetMSG(byte[] msg, boolean bdebug) {
        setIncomming(msg);
        setGbdebug(bdebug);
    }

    public boolean IsPHYSICAL() {
        boolean bret = false;
        if (isGbdebug()) {
            System.out.println("Length : " + getIncomming().length);
            System.out.println("Type : " + unsignedByteToInt(getIncomming()[3]));
        }

        if (unsignedByteToInt(getIncomming()[3]) == 17) {
            bret = true;

        }

        return bret;
    }

    public void decode() {
        byte[] incomingMessage = getIncomming();
        PressureFCI = nephronmaths.GetValue(incomingMessage, 10, 11, 1.00, 16);
        PressureFCO = nephronmaths.GetValue(incomingMessage, 16, 17, 1.00, 16);
        PressureBCI = nephronmaths.GetValue(incomingMessage, 22, 23, 1.00, 16);
        PressureBCO = nephronmaths.GetValue(incomingMessage, 28, 29, 1.00, 16);
        TempIN = nephronmaths.GetValue(incomingMessage, 34, 35, 1.00, 16);
        TempOut = nephronmaths.GetValue(incomingMessage, 36, 37, 1.00, 16);
        CondFCR = nephronmaths.GetValue(incomingMessage, 42, 43, 1.00, 16);
        CondPT = nephronmaths.GetValue(incomingMessage, 46, 47, 1.00, 16);

    }

    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

    /**
     * @return the Incomming
     */
    public byte[] getIncomming() {
        return Incomming;
    }

    /**
     * @param Incomming the Incomming to set
     */
    public void setIncomming(byte[] Incomming) {
        this.Incomming = Incomming;
    }

    /**
     * @return the gbdebug
     */
    public boolean isGbdebug() {
        return gbdebug;
    }

    /**
     * @param gbdebug the gbdebug to set
     */
    public void setGbdebug(boolean gbdebug) {
        this.gbdebug = gbdebug;
    }

    /**
     * @return the PressureFCI
     */
    public String getPressureFCI() {
        return PressureFCI;
    }

    /**
     * @param PressureFCI the PressureFCI to set
     */
    public void setPressureFCI(String PressureFCI) {
        this.PressureFCI = PressureFCI;
    }

    /**
     * @return the PressureFCO
     */
    public String getPressureFCO() {
        return PressureFCO;
    }

    /**
     * @param PressureFCO the PressureFCO to set
     */
    public void setPressureFCO(String PressureFCO) {
        this.PressureFCO = PressureFCO;
    }

    /**
     * @return the PressureBCI
     */
    public String getPressureBCI() {
        return PressureBCI;
    }

    /**
     * @param PressureBCI the PressureBCI to set
     */
    public void setPressureBCI(String PressureBCI) {
        this.PressureBCI = PressureBCI;
    }

    /**
     * @return the PressureBCO
     */
    public String getPressureBCO() {
        return PressureBCO;
    }

    /**
     * @param PressureBCO the PressureBCO to set
     */
    public void setPressureBCO(String PressureBCO) {
        this.PressureBCO = PressureBCO;
    }

    /**
     * @return the TempIN
     */
    public String getTempIN() {
        return TempIN;
    }

    /**
     * @param TempIN the TempIN to set
     */
    public void setTempIN(String TempIN) {
        this.TempIN = TempIN;
    }

    /**
     * @return the TempOut
     */
    public String getTempOut() {
        return TempOut;
    }

    /**
     * @param TempOut the TempOut to set
     */
    public void setTempOut(String TempOut) {
        this.TempOut = TempOut;
    }

    /**
     * @return the CondFCR
     */
    public String getCondFCR() {
        return CondFCR;
    }

    /**
     * @param CondFCR the CondFCR to set
     */
    public void setCondFCR(String CondFCR) {
        this.CondFCR = CondFCR;
    }

    /**
     * @return the CondPT
     */
    public String getCondPT() {
        return CondPT;
    }

    /**
     * @param CondPT the CondPT to set
     */
    public void setCondPT(String CondPT) {
        this.CondPT = CondPT;
    }

    /**
     * @return the b_PressureFCI
     */
    public BigInteger getB_PressureFCI() {
        return b_PressureFCI;
    }

    /**
     * @param b_PressureFCI the b_PressureFCI to set
     */
    public void setB_PressureFCI(BigInteger b_PressureFCI) {
        this.b_PressureFCI = b_PressureFCI;
    }

    /**
     * @return the b_PressureFCO
     */
    public BigInteger getB_PressureFCO() {
        return b_PressureFCO;
    }

    /**
     * @param b_PressureFCO the b_PressureFCO to set
     */
    public void setB_PressureFCO(BigInteger b_PressureFCO) {
        this.b_PressureFCO = b_PressureFCO;
    }

    /**
     * @return the b_PressureBCI
     */
    public BigInteger getB_PressureBCI() {
        return b_PressureBCI;
    }

    /**
     * @param b_PressureBCI the b_PressureBCI to set
     */
    public void setB_PressureBCI(BigInteger b_PressureBCI) {
        this.b_PressureBCI = b_PressureBCI;
    }

    /**
     * @return the b_PressureBCO
     */
    public BigInteger getB_PressureBCO() {
        return b_PressureBCO;
    }

    /**
     * @param b_PressureBCO the b_PressureBCO to set
     */
    public void setB_PressureBCO(BigInteger b_PressureBCO) {
        this.b_PressureBCO = b_PressureBCO;
    }

    /**
     * @return the b_TempIN
     */
    public BigInteger getB_TempIN() {
        return b_TempIN;
    }

    /**
     * @param b_TempIN the b_TempIN to set
     */
    public void setB_TempIN(BigInteger b_TempIN) {
        this.b_TempIN = b_TempIN;
    }

    /**
     * @return the b_TempOut
     */
    public BigInteger getB_TempOut() {
        return b_TempOut;
    }

    /**
     * @param b_TempOut the b_TempOut to set
     */
    public void setB_TempOut(BigInteger b_TempOut) {
        this.b_TempOut = b_TempOut;
    }

    /**
     * @return the b_CondFCR
     */
    public BigInteger getB_CondFCR() {
        return b_CondFCR;
    }

    /**
     * @param b_CondFCR the b_CondFCR to set
     */
    public void setB_CondFCR(BigInteger b_CondFCR) {
        this.b_CondFCR = b_CondFCR;
    }

    /**
     * @return the b_CondPT
     */
    public BigInteger getB_CondPT() {
        return b_CondPT;
    }

    /**
     * @param b_CondPT the b_CondPT to set
     */
    public void setB_CondPT(BigInteger b_CondPT) {
        this.b_CondPT = b_CondPT;
    }
}
