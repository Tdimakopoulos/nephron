package eu.nephron.wakd.api;

import eu.nephron.wakd.api.enums.WhoEnum;


public abstract class IncomingWakdMsg extends WakdMsg {

	private static final long serialVersionUID = -915511887610766156L;
	
	protected IncomingWakdMsg() {
		this.header = new Header();
		this.header.setSender(WhoEnum.MB);
		this.header.setRecipient(WhoEnum.SP);
	}
	
	protected abstract void decode(byte[] bytes);
}
