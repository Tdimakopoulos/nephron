/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.math;

import java.math.BigInteger;

/**
 *
 * @author tdim
 */
public abstract class nephronmaths {
    
    public static String BigIntToString(BigInteger value,int iconv) {
        return value.toString(iconv);
    }
    
    public static Double StringToDouble(String value)
    {
        return Double.parseDouble(value);
    }
    
    public static Long StringToLong(String value)
    {
        return Long.parseLong(value);
    }
    
    public static String DoubleToString(double value)
    {
        return String.valueOf(value);
    }
    
    public static String Divide(long value,double devide)
    {
        //    long lfat=Long.parseLong(String.valueOf(ifat));
        double Dret=value/devide;
        return DoubleToString(Dret);
    }
    
    public static String Divide(String value,double devide)
    {
        long ltemp=Long.parseLong(value);
        double Dret=ltemp/devide;
        return DoubleToString(Dret);
    }
    
    public static String Divide(BigInteger value,double devide,int iconv)
    {
        long ltemp=Long.parseLong(BigIntToString(value,iconv));
        double Dret=ltemp/devide;
        return DoubleToString(Dret);
    }
    public static String GetValue(byte[] value,int from,int to,double devide,int conv)
    {
        byte[] value_array = new byte[]{value[from], value[to]};
        BigInteger b_value = new BigInteger(value_array);
        return nephronmaths.Divide(b_value, devide, conv);
    }
//    byte[] inlet_Sodium = new byte[]{incomingMessage[10], incomingMessage[11]};
//        inlet_sod = new BigInteger(inlet_Sodium);
//        inlet_s = nephronmaths.Divide(inlet_sod, 10, 16);
}
