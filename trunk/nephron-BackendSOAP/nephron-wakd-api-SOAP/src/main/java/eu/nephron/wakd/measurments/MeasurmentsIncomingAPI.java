/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.measurments;

import eu.nephron.wakd.math.nephronmaths;
import java.math.BigInteger;

/**
 *
 * @author tdim
 */
public class MeasurmentsIncomingAPI {

    byte[] Incomming = null;
    private BigInteger inlet_sod;
    private String inlet_s;
    private BigInteger outlet_sod;
    private String outlet_s;
    private BigInteger inlet_pot;
    private String inlet_s1;
    private BigInteger outlet_pot;
    private String outlet_s1;
    private BigInteger inlet_ph;
    private String inlet_s2;
    private BigInteger outlet_ph;
    private String outlet_s2;
    private BigInteger inlet_ur;
    private String inlet_s3;
    private BigInteger outlet_ur;
    private String outlet_s3;
    private BigInteger inlet_temp;
    private String inlet_s4;
    private BigInteger outlet_temp;
    private String outlet_s4;
    boolean gbdebug;

    public double GetInletTemp()
    {
        return (double) Integer.parseInt(inlet_s4,16);
    }
            
    public double GetOutletTemp()
    {
        return (double) Integer.parseInt(outlet_s4,16);
    }      
    
    public double GetInletUria()
    {
        return (double) Integer.parseInt(inlet_s3,16);
    }
            
    public double GetOutletUria()
    {
        return (double) Integer.parseInt(outlet_s3,16);
    }      
    
    public double GetInletPH()
    {
        return (double) Integer.parseInt(inlet_s2,16);
    }
            
    public double GetOutletPH()
    {
        return (double) Integer.parseInt(outlet_s2,16);
    }      
    
    public double GetInletSodium()
    {
        return (double) Integer.parseInt(inlet_s,16);
    }
            
    public double GetOutletSodium()
    {
        return (double) Integer.parseInt(outlet_s,16);
    }      
    
    public double GetInletPotasium()
    {
        return (double) Integer.parseInt(inlet_s1,16);
    }
            
    public double GetOutletPotasium()
    {
        return (double) Integer.parseInt(outlet_s1,16);
    }      
    
    public void SetMSG(byte[] msg, boolean bdebug) {
        Incomming = msg;
        gbdebug = bdebug;
    }

    public boolean IsPHYSIOLOGICAL() {
        boolean bret = false;
        if (gbdebug) {
            System.out.println("Length : " + Incomming.length);
            System.out.println("Type : " + unsignedByteToInt(Incomming[3]));
        }
        if (Incomming.length == 70) {
            if (unsignedByteToInt(Incomming[3]) == 12) {
                bret = true;

            }
        }
        return bret;
    }

    
    public void decode() {
        byte[] incomingMessage = Incomming;

        byte[] inlet_Sodium = new byte[]{incomingMessage[10], incomingMessage[11]};
        inlet_sod = new BigInteger(inlet_Sodium);
        inlet_s = nephronmaths.Divide(inlet_sod, 10.00, 16);
        
        byte[] outlet_Sodium = new byte[]{incomingMessage[40], incomingMessage[41]};
        outlet_sod = new BigInteger(outlet_Sodium);
        outlet_s = nephronmaths.Divide(outlet_sod, 10.00, 16);

        byte[] inlet_Potassium = new byte[]{incomingMessage[16], incomingMessage[17]};
        inlet_pot = new BigInteger(inlet_Potassium);
        inlet_s1 = nephronmaths.Divide(inlet_pot, 10.00, 16);

        byte[] outlet_Potassium = new byte[]{incomingMessage[46], incomingMessage[47]};
        outlet_pot = new BigInteger(outlet_Potassium);
        outlet_s1 = nephronmaths.Divide(outlet_pot, 10.00, 16);

        byte[] inlet_pH = new byte[]{incomingMessage[22], incomingMessage[23]};
        inlet_ph = new BigInteger(inlet_pH);
        inlet_s2 = nephronmaths.Divide(inlet_ph, 10.00, 16);

        byte[] outlet_pH = new byte[]{incomingMessage[52], incomingMessage[53]};
        outlet_ph = new BigInteger(outlet_pH);
        outlet_s2 = nephronmaths.Divide(outlet_ph, 10.00, 16);

        byte[] inlet_Urea = new byte[]{incomingMessage[28], incomingMessage[29]};
        inlet_ur = new BigInteger(inlet_Urea);
        inlet_s3 = nephronmaths.Divide(inlet_ur, 10.00, 16);

        byte[] outlet_Urea = new byte[]{incomingMessage[58], incomingMessage[59]};
        outlet_ur = new BigInteger(outlet_Urea);
        outlet_s3 = nephronmaths.Divide(outlet_ur, 10.00, 16);

        byte[] inlet_Temperature = new byte[]{incomingMessage[34], incomingMessage[35]};
        inlet_temp = new BigInteger(
                inlet_Temperature);
        inlet_s4 = nephronmaths.Divide(inlet_temp, 10.00, 16);

        byte[] outlet_Temperature = new byte[]{incomingMessage[66], incomingMessage[65]};
        outlet_temp = new BigInteger(outlet_Temperature);
        outlet_s4 = nephronmaths.Divide(outlet_temp, 10.00, 16);
    }

    public int unsignedByteToInt(byte b) {
        return (int) b & 0xFF;
    }

    /**
     * @return the inlet_sod
     */
    public BigInteger getInlet_sod() {
        return inlet_sod;
    }

    /**
     * @param inlet_sod the inlet_sod to set
     */
    public void setInlet_sod(BigInteger inlet_sod) {
        this.inlet_sod = inlet_sod;
    }

    /**
     * @return the inlet_s
     */
    public String getInlet_s() {
        return inlet_s;
    }

    /**
     * @param inlet_s the inlet_s to set
     */
    public void setInlet_s(String inlet_s) {
        this.inlet_s = inlet_s;
    }

    /**
     * @return the outlet_sod
     */
    public BigInteger getOutlet_sod() {
        return outlet_sod;
    }

    /**
     * @param outlet_sod the outlet_sod to set
     */
    public void setOutlet_sod(BigInteger outlet_sod) {
        this.outlet_sod = outlet_sod;
    }

    /**
     * @return the outlet_s
     */
    public String getOutlet_s() {
        return outlet_s;
    }

    /**
     * @param outlet_s the outlet_s to set
     */
    public void setOutlet_s(String outlet_s) {
        this.outlet_s = outlet_s;
    }

    /**
     * @return the inlet_pot
     */
    public BigInteger getInlet_pot() {
        return inlet_pot;
    }

    /**
     * @param inlet_pot the inlet_pot to set
     */
    public void setInlet_pot(BigInteger inlet_pot) {
        this.inlet_pot = inlet_pot;
    }

    /**
     * @return the inlet_s1
     */
    public String getInlet_s1() {
        return inlet_s1;
    }

    /**
     * @param inlet_s1 the inlet_s1 to set
     */
    public void setInlet_s1(String inlet_s1) {
        this.inlet_s1 = inlet_s1;
    }

    /**
     * @return the outlet_pot
     */
    public BigInteger getOutlet_pot() {
        return outlet_pot;
    }

    /**
     * @param outlet_pot the outlet_pot to set
     */
    public void setOutlet_pot(BigInteger outlet_pot) {
        this.outlet_pot = outlet_pot;
    }

    /**
     * @return the outlet_s1
     */
    public String getOutlet_s1() {
        return outlet_s1;
    }

    /**
     * @param outlet_s1 the outlet_s1 to set
     */
    public void setOutlet_s1(String outlet_s1) {
        this.outlet_s1 = outlet_s1;
    }

    /**
     * @return the inlet_ph
     */
    public BigInteger getInlet_ph() {
        return inlet_ph;
    }

    /**
     * @param inlet_ph the inlet_ph to set
     */
    public void setInlet_ph(BigInteger inlet_ph) {
        this.inlet_ph = inlet_ph;
    }

    /**
     * @return the inlet_s2
     */
    public String getInlet_s2() {
        return inlet_s2;
    }

    /**
     * @param inlet_s2 the inlet_s2 to set
     */
    public void setInlet_s2(String inlet_s2) {
        this.inlet_s2 = inlet_s2;
    }

    /**
     * @return the outlet_ph
     */
    public BigInteger getOutlet_ph() {
        return outlet_ph;
    }

    /**
     * @param outlet_ph the outlet_ph to set
     */
    public void setOutlet_ph(BigInteger outlet_ph) {
        this.outlet_ph = outlet_ph;
    }

    /**
     * @return the outlet_s2
     */
    public String getOutlet_s2() {
        return outlet_s2;
    }

    /**
     * @param outlet_s2 the outlet_s2 to set
     */
    public void setOutlet_s2(String outlet_s2) {
        this.outlet_s2 = outlet_s2;
    }

    /**
     * @return the inlet_ur
     */
    public BigInteger getInlet_ur() {
        return inlet_ur;
    }

    /**
     * @param inlet_ur the inlet_ur to set
     */
    public void setInlet_ur(BigInteger inlet_ur) {
        this.inlet_ur = inlet_ur;
    }

    /**
     * @return the inlet_s3
     */
    public String getInlet_s3() {
        return inlet_s3;
    }

    /**
     * @param inlet_s3 the inlet_s3 to set
     */
    public void setInlet_s3(String inlet_s3) {
        this.inlet_s3 = inlet_s3;
    }

    /**
     * @return the outlet_ur
     */
    public BigInteger getOutlet_ur() {
        return outlet_ur;
    }

    /**
     * @param outlet_ur the outlet_ur to set
     */
    public void setOutlet_ur(BigInteger outlet_ur) {
        this.outlet_ur = outlet_ur;
    }

    /**
     * @return the outlet_s3
     */
    public String getOutlet_s3() {
        return outlet_s3;
    }

    /**
     * @param outlet_s3 the outlet_s3 to set
     */
    public void setOutlet_s3(String outlet_s3) {
        this.outlet_s3 = outlet_s3;
    }

    /**
     * @return the inlet_temp
     */
    public BigInteger getInlet_temp() {
        return inlet_temp;
    }

    /**
     * @param inlet_temp the inlet_temp to set
     */
    public void setInlet_temp(BigInteger inlet_temp) {
        this.inlet_temp = inlet_temp;
    }

    /**
     * @return the inlet_s4
     */
    public String getInlet_s4() {
        return inlet_s4;
    }

    /**
     * @param inlet_s4 the inlet_s4 to set
     */
    public void setInlet_s4(String inlet_s4) {
        this.inlet_s4 = inlet_s4;
    }

    /**
     * @return the outlet_temp
     */
    public BigInteger getOutlet_temp() {
        return outlet_temp;
    }

    /**
     * @param outlet_temp the outlet_temp to set
     */
    public void setOutlet_temp(BigInteger outlet_temp) {
        this.outlet_temp = outlet_temp;
    }

    /**
     * @return the outlet_s4
     */
    public String getOutlet_s4() {
        return outlet_s4;
    }

    /**
     * @param outlet_s4 the outlet_s4 to set
     */
    public void setOutlet_s4(String outlet_s4) {
        this.outlet_s4 = outlet_s4;
    }
}
