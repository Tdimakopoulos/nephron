/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.textconv;

/**
 *
 * @author tdim
 */
public class AlarmsMsg {
  private static final long serialVersionUID = -6035789785730909913L;
	
	String msgPatient,msgDoctor,alert,date;
	int idDatabase,status;

	public AlarmsMsg(byte value){
		
		switch (value){
		case (byte)1: // decTreeMsg_detectedBPsysAbsL
            alert="decTreeMsg_detectedBPsysAbsL";
            msgPatient ="Blood pressure systolic low, please contact physician";
            msgDoctor  ="BP-sys lower than normal range";
            break;

        case (byte)2: // decTreeMsg_detectedBPsysAbsH
            alert="decTreeMsg_detectedBPsysAbsH";
            msgPatient ="Blood pressure systolic high, please contact physician";
            msgDoctor  ="BP-sys higher than normal range";
            break;
        case (byte)3: // decTreeMsg_detectedBPsysAAbsH
            alert="decTreeMsg_detectedBPsysAAbsH";
            msgPatient ="Blood pressure systolic high, please contact physician";
            msgDoctor  ="ALARM: BP-sys high";
            break;
        case (byte)4: // decTreeMsg_detectedBPdiaAbsL
            alert="decTreeMsg_detectedBPdiaAbsL";
            msgPatient ="Blood pressure diastolic low, please contact physician";
            msgDoctor  ="BP-dia lower than normal range";
            break;
        case (byte)5: // decTreeMsg_detectedBPdiaAbsH
            alert="decTreeMsg_detectedBPdiaAbsH";
            msgPatient ="Blood pressure diastolic high, please contact physician";
            msgDoctor  ="BP-dia higher than normal range";
            break;
        case (byte)6: // decTreeMsg_detectedBPdiaAAbsH
            alert="decTreeMsg_detectedBPdiaAAbsH";
            msgPatient ="Blood pressure diastolic high, please contact physician";
            msgDoctor  ="ALARM: BP-dia high";
            break;
        case (byte)7: // dectreeMsg_detectedWghtAbsL
            alert="dectreeMsg_detectedWghtAbsL";
            msgPatient ="Measured weight low, please contact physician";
            msgDoctor  ="Measured weight lower than threshold";
            break;
        case (byte)8: // dectreeMsg_detectedWghtAbsH
            alert="dectreeMsg_detectedWghtAbsH";
            msgPatient ="Measured weight high, please contact physician";
            msgDoctor  ="Measured weight higher than threshold";
            break;
        case (byte)9: // dectreeMsg_detectedWghtTrdT
            alert="dectreeMsg_detectedWghtTrdT";
            msgPatient ="Weight trend exceeds threshold, please contact physician";
            msgDoctor  ="Weight trend exceeds threshold";
            break;
        case (byte)10: // dectreeMsg_detectedpumpBaccDevi
            alert="dectreeMsg_detectedpumpBaccDevi";
            msgPatient ="Detected blood pump deviation";//23 debug
            msgDoctor  ="Detected blood pump deviation";
            break;
        case (byte)11: // dectreeMsg_detectedpumpFaccDevi
            alert="dectreeMsg_detectedpumpFaccDevi";
            msgPatient ="Detected dialysate pump deviation";//23 //debug
            msgDoctor  ="Detected dialysate pump deviation";
            break;
        case (byte)12: // dectreeMsg_detectedBPorFPhigh
            alert="dectreeMsg_detectedBPorFPhigh";
            msgPatient ="Pumps Stopped - Contact physician";
            msgDoctor  ="ALARM: pressure (high) excess, all pumps stopped";
            break;
        case (byte)13: // dectreeMsg_detectedBPorFPlow
            alert="dectreeMsg_detectedBPorFPlow";
            msgPatient ="Pumps Stopped - Contact physician";
            msgDoctor  ="ALARM: pressure (low) excess, all pumps stopped";
            break;
        case (byte)14: // dectreeMsg_detectedBTSoTH
            alert="dectreeMsg_detectedBTSoTH";
            msgPatient ="Pumps Stopped - Contact physician";
            msgDoctor  ="Temperature (high) excess at BTSo, all pumps stopped";
            break;
        case (byte)15: // dectreeMsg_detectedBTSoTL
            alert="dectreeMsg_detectedBTSoTL";
            msgPatient ="Pumps Stopped - Contact physician";
            msgDoctor  ="Temperature (low) excess at BTSo, all pumps stopped";
            break;
        case (byte)16: // dectreeMsg_detectedBTSiTH
            alert="dectreeMsg_detectedBTSiTH";
            msgPatient ="Temperature (high) excess at BTSi, all pumps stopped, please contact physician";
            msgDoctor  ="Temperature (high) excess at BTSi, all pumps stopped";
            break;
        case (byte)17: // dectreeMsg_detectedBTSiTL
            alert="dectreeMsg_detectedBTSiTL";
            msgPatient ="Temperature (low) excess at BTSi, all pumps stopped, please contact physician";
            msgDoctor  ="Temperature (low) excess at BTSi, all pumps stopped";
            break;
        case (byte)18: // dectreeMsg_detectedSorDys
            alert="dectreeMsg_detectedSorDys";
            msgPatient ="Please replace the sorbent cartridge"; 
            msgDoctor  ="A sorbent disfunction was detected, patient was instructed to replace the cartridge";
            break;
        case (byte)19: // dectreeMsg_detectedPhAAbsL
            alert="dectreeMsg_detectedPhAAbsL";
            msgPatient ="pH low, please contact physician";
            msgDoctor  ="pH low";
            break;
        case (byte)20: // dectreeMsg_detectedPhAAbsH
            alert="dectreeMsg_detectedPhAAbsH";
            msgPatient ="pH high, please contact physician";
            msgDoctor  ="pH high";
            break;
        case (byte)21: // dectreeMsg_detectedPhAbsLorPhAbsH
            alert="dectreeMsg_detectedPhAbsLorPhAbsH";
            msgPatient ="pH out of normal range, please contact physician";
            msgDoctor  ="pH out of normal range";
            break;
        case (byte)22: // dectreeMsg_detectedPhTrdLPsT
            alert="dectreeMsg_detectedPhTrdLPsT";
            msgPatient ="pH decrease out of normal range, please contact physician";
            msgDoctor  ="pH decrease out of normal range";
            break;
        case (byte)23: // dectreeMsg_detectedPhTrdHPsT
            alert="dectreeMsg_detectedPhTrdHPsT";
            msgPatient ="pH increase out of normal range, please contact physician";
            msgDoctor  ="pH increase out of normal range";
            break;
        case (byte)24: // dectreeMsg_detectedNaAbsLorNaAbsH
            alert="dectreeMsg_detectedNaAbsLorNaAbsH";
            msgPatient ="Calcium out of normal range, please contact physician";
            msgDoctor  ="Calcium out of normal range";
            break;
        case (byte)25: // dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT
            alert="dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT";
            msgPatient ="Fast change in Calcium was detected in trend analysis, please contact the physician"; 
            msgDoctor  ="Fast change in Calcium was detected in trend analysis";
            break;
        case (byte)26: // dectreeMsg_detectedKAbsLorKAbsHandSorDys
            alert="dectreeMsg_detectedKAbsLorKAbsHandSorDys";
            msgPatient ="Please replace sorbent cartridge";
            msgDoctor  ="Patient was asked to replace sorbent cartridge";
            break;
        case (byte)27: // dectreeMsg_detectedKAbsLorKAbsHnoSorDys
            alert="dectreeMsg_detectedKAbsLorKAbsHnoSorDys";
            msgPatient ="K+ out of normal range, please contact physician";
            msgDoctor  ="K+ out of normal range";
            break;
        case (byte)28: // dectreeMsg_detectedKAAL
            alert="dectreeMsg_detectedKAAL";
            msgPatient ="Potassium lower than normal range, please contact physician";
            msgDoctor  ="Potassium lower than normal range";
            break;
        case (byte)29: // dectreeMsg_detectedKAAHandKincrease
            alert="dectreeMsg_detectedKAAHandKincrease";
            msgPatient ="Replace sorbent cartridge, please contact physician";
            msgDoctor  ="Patient was asked to replace sorbent cartridge";
            break;
        case (byte)30: // dectreeMsg_detectedKAAHnoKincrease
            alert="dectreeMsg_detectedKAAHnoKincrease";
            msgPatient ="Potassium higher than normal range, please contact physician";
            msgDoctor  ="Potassium higher than normal range";
            break;
        case (byte)31: // dectreeMsg_detectedKTrdLPsT
            alert="dectreeMsg_detectedKTrdLPsT";
            msgPatient ="Potassium trend deviates from threshold, please contact physician";
            msgDoctor  ="Potassium trend deviates from threshold";
            break;
        case (byte)32: //
            //dectreeMsg_detectedKTrdHPsT
            alert="dectreeMsg_detectedKTrdHPsT";
            msgPatient ="A fast increase in K+ trend was detected, please contact physician";
            msgDoctor  ="A increasing fast change in K+ trend was detected  in trend analysis.";
            break;
        case (byte)33: // dectreeMsg_detectedKTrdofnriandSorDys
            alert="dectreeMsg_detectedKTrdofnriandSorDys";
            msgPatient ="Potassium removal low, please replace sorbent cartridge.";
            msgDoctor  ="Patient was asked to replace sorbent cartridge due to low potassium removal";
            break; 
        case (byte)34: // dectreeMsg_detectedPhTrdofnriandSorDys
            alert="dectreeMsg_detectedPhTrdofnriandSorDys";
            msgPatient ="pH dialysate out of range, please contact physician";
            msgDoctor  ="pH dialysate out of range";
            break; 
        case (byte)35: // dectreeMsg_detectedUreaAAbsHandSorDys
            alert="dectreeMsg_detectedUreaAAbsHandSorDys";
            msgPatient ="Urea removal low, please contact physician";
            msgDoctor  ="Urea removal low";
            break; 
        case (byte)36: // dectreeMsg_detectedUreaAAbsHnoSorDys
            alert="dectreeMsg_detectedUreaAAbsHnoSorDys";
            msgPatient ="Urea out of normal range (high), please contact physician";
            msgDoctor  ="Urea out of normal range (high)";
            break;
        case (byte)37: // dectreeMsg_detectedUreaTrdHPsT
            alert="dectreeMsg_detectedUreaTrdHPsT";
            msgPatient ="Urea increase out of normal range (high), please contact physician";
            msgDoctor  ="Urea increase out of normal range (high)";
            break;
        case (byte)38: // flowControlMsg_excretionBagNotConnected
            alert="flowControlMsg_excretionBagNotConnected";
            msgPatient ="Please connect regeneration bag";
            msgDoctor  ="";
            break;
        case (byte)39: // rtbAlarmMsg_detectedBABD
            alert="rtbAlarmMsg_detectedBABD";
            msgPatient ="Detected air bubble in bloodline, all pumps stopped, please replace bloodline";
            msgDoctor  ="Detected air bubble in bloodline, all pumps stopped";
            break;
        case (byte)40: // rtbAlarmMsg_detectedBLD
            alert="rtbAlarmMsg_detectedBLD"; //bloodline leakage
            msgPatient ="Detected leakage in bloodline, all pumps stopped, please replace bloodline";
            msgDoctor  ="Detected leakage in bloodline, all pumps stopped";
            break;
        case (byte)41: // rtbAlarmMsg_detectedDABD
            alert="rtbAlarmMsg_detectedDABD"; //dialysate air bubble detected
            msgPatient ="Detected air bubble in dialysate line, dialysate circuit stopped";
            msgDoctor  ="Detected air bubble in dialysate line, dialysate circuit stopped";
            break;
        case (byte)42: // rtbAlarmMsg_detectedFLD
            alert="rtbAlarmMsg_detectedFLD"; //fluid leakage
            msgPatient ="Detected fluid leakage in dialysate circuit, dialysate circuit stopped ";
            msgDoctor  ="Detected fluid leakage in dialysate circuit, dialysate circuit stopped ";
            break;

			 
			 
			 
		}
	}
	
	
	
	
	
	public String getmsgPatient() {
		return msgPatient;    
	}
	
	public String getmsgDoctor() {
		return msgDoctor;    
	}
	public String getmsgAlert() {
		return alert;    
	}
	public int getIdDatabase(){
		return idDatabase;
	}
	public String getDate(){
		return date;
	}
	public int getStatus(){
		return status;
	}
	
	public void setIdDatabase(int idDatabase) {
		this.idDatabase = idDatabase;
	}   
}
