package eu.nephron.wakd.api.sensor;

import eu.nephron.utils.ByteUtils;


public class EcgData extends SensorData {

	private static final long serialVersionUID = -4243010173433348686L;

	private int heartRate;
	
	private int respirationlRate;

	public int getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}

	public int getRespirationlRate() {
		return respirationlRate;
	}

	public void setRespirationlRate(int respirationlRate) {
		this.respirationlRate = respirationlRate;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = new byte[2];
		bytes[0] = ByteUtils.intToByte(heartRate);
		bytes[1] = ByteUtils.intToByte(respirationlRate);
		return bytes;
	}

}
