/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.ws;

import eu.nephron.crudservice.CrudService;
import eu.nephron.secure.DB.WAKDAlerts;
import eu.nephron.secure.beans.WAKDAlertsFacadeLocal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author Tom
 */
@WebService(serviceName = "WAKDAlertsManager")
public class WAKDAlertsManager {
    @EJB
    private WAKDAlertsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

   
     
    @PersistenceUnit
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "ReturnAllAlertsForLast7Days")
    public List<WAKDAlerts> ReturnAllAlertsForLast7Days() {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager  em = emf.createEntityManager();

        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date today7 = cal.getTime();
        Long ddate = today7.getTime();
        
        List<WAKDAlerts> wakd = em.createQuery("select a from WAKDAlerts a where a.ddate>:DateFrom order by a.ddate desc").setParameter("DateFrom", ddate).getResultList();
        return wakd;
    }
    
    @WebMethod(operationName = "findWAKDAlertsManagerWithIMEI")
    public List<WAKDAlerts> findWAKDAlertsManagerWithIMEI(@WebParam(name = "IMEI") String IMEI) {
        //Map<String, Object> params = new HashMap<String, Object>();
	//	params.put("FindIMEI", IMEI);
        //return crudservice.findWithQuery("WAKDAlerts.findByIMEI", params);
        
        
         assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        
        List<WAKDAlerts> wakd = em.createQuery("select a from WAKDAlerts a where a.IMEI=:IMEI order by a.ddate desc").setParameter("IMEI", IMEI).getResultList();
        return wakd;
    }
     
    @WebMethod(operationName = "findWAKDAlertsManager")
    public WAKDAlerts findWAKDAlertsManager(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllWAKDAlertsManager")
    public List<WAKDAlerts> findAllWAKDAlertsManager() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeWAKDAlertsManager")
    public List<WAKDAlerts> findRangeWAKDAlertsManager(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countWAKDAlertsManager")
    public int countWAKDAlertsManager() {
        return ejbRef.count();
    }
    
}
