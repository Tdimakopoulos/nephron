/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.DB;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author tdim
 */
@Entity
@NamedQueries({
	@NamedQuery(name="ScheduleActAndTask.findByIMEI", query="select a from ScheduleActAndTask a where a.imei=:FindIMEI")
})
public class ScheduleActAndTask implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String imei;
    private Long patientid;
    private Long doctorid;
    private Long dateap;
    private Long durationhours;
    private String sdesc;
    private String text1;
    private String text2;
    private int i1;
    private int i2;
    private int itype;
    private Long ll1;
    private Long ll2;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScheduleActAndTask)) {
            return false;
        }
        ScheduleActAndTask other = (ScheduleActAndTask) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.ScheduleActAndTask[ id=" + id + " ]";
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the patientid
     */
    public Long getPatientid() {
        return patientid;
    }

    /**
     * @param patientid the patientid to set
     */
    public void setPatientid(Long patientid) {
        this.patientid = patientid;
    }

    /**
     * @return the doctorid
     */
    public Long getDoctorid() {
        return doctorid;
    }

    /**
     * @param doctorid the doctorid to set
     */
    public void setDoctorid(Long doctorid) {
        this.doctorid = doctorid;
    }

    /**
     * @return the dateap
     */
    public Long getDateap() {
        return dateap;
    }

    /**
     * @param dateap the dateap to set
     */
    public void setDateap(Long dateap) {
        this.dateap = dateap;
    }

    /**
     * @return the durationhours
     */
    public Long getDurationhours() {
        return durationhours;
    }

    /**
     * @param durationhours the durationhours to set
     */
    public void setDurationhours(Long durationhours) {
        this.durationhours = durationhours;
    }

    /**
     * @return the sdesc
     */
    public String getSdesc() {
        return sdesc;
    }

    /**
     * @param sdesc the sdesc to set
     */
    public void setSdesc(String sdesc) {
        this.sdesc = sdesc;
    }

    /**
     * @return the text1
     */
    public String getText1() {
        return text1;
    }

    /**
     * @param text1 the text1 to set
     */
    public void setText1(String text1) {
        this.text1 = text1;
    }

    /**
     * @return the text2
     */
    public String getText2() {
        return text2;
    }

    /**
     * @param text2 the text2 to set
     */
    public void setText2(String text2) {
        this.text2 = text2;
    }

    /**
     * @return the i1
     */
    public int getI1() {
        return i1;
    }

    /**
     * @param i1 the i1 to set
     */
    public void setI1(int i1) {
        this.i1 = i1;
    }

    /**
     * @return the i2
     */
    public int getI2() {
        return i2;
    }

    /**
     * @param i2 the i2 to set
     */
    public void setI2(int i2) {
        this.i2 = i2;
    }

    /**
     * @return the itype
     */
    public int getItype() {
        return itype;
    }

    /**
     * @param itype the itype to set
     */
    public void setItype(int itype) {
        this.itype = itype;
    }

    /**
     * @return the ll1
     */
    public Long getLl1() {
        return ll1;
    }

    /**
     * @param ll1 the ll1 to set
     */
    public void setLl1(Long ll1) {
        this.ll1 = ll1;
    }

    /**
     * @return the ll2
     */
    public Long getLl2() {
        return ll2;
    }

    /**
     * @param ll2 the ll2 to set
     */
    public void setLl2(Long ll2) {
        this.ll2 = ll2;
    }
    
}
