/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.beans;

import eu.nephron.secure.DB.WAKDStatus;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Tom
 */
@Local
public interface WAKDStatusFacadeLocal {

    void create(WAKDStatus wAKDStatus);

    void edit(WAKDStatus wAKDStatus);

    void remove(WAKDStatus wAKDStatus);

    WAKDStatus find(Object id);

    List<WAKDStatus> findAll();

    List<WAKDStatus> findRange(int[] range);

    int count();
    
}
