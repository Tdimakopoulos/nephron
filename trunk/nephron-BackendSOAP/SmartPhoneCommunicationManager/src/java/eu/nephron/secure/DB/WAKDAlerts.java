/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.DB;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Tom
 */
@Entity
@NamedQueries({
	@NamedQuery(name="WAKDAlerts.findByIMEI", query="select a from WAKDAlerts a where a.IMEI=:FindIMEI")
})
public class WAKDAlerts implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String msgAlert;
    private String msgDoctor;
    private String msgPatient;
    
    
    private String IMEI;
    private Long ddate;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WAKDAlerts)) {
            return false;
        }
        WAKDAlerts other = (WAKDAlerts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.WAKDAlerts[ id=" + id + " ]";
    }

    /**
     * @return the msgAlert
     */
    public String getMsgAlert() {
        return msgAlert;
    }

    /**
     * @param msgAlert the msgAlert to set
     */
    public void setMsgAlert(String msgAlert) {
        this.msgAlert = msgAlert;
    }

    /**
     * @return the msgDoctor
     */
    public String getMsgDoctor() {
        return msgDoctor;
    }

    /**
     * @param msgDoctor the msgDoctor to set
     */
    public void setMsgDoctor(String msgDoctor) {
        this.msgDoctor = msgDoctor;
    }

    /**
     * @return the msgPatient
     */
    public String getMsgPatient() {
        return msgPatient;
    }

    /**
     * @param msgPatient the msgPatient to set
     */
    public void setMsgPatient(String msgPatient) {
        this.msgPatient = msgPatient;
    }

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the ddate
     */
    public Long getDdate() {
        return ddate;
    }

    /**
     * @param ddate the ddate to set
     */
    public void setDdate(Long ddate) {
        this.ddate = ddate;
    }
    
}
