/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.beans;

import eu.nephron.secure.DB.ScheduleActAndTask;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface ScheduleActAndTaskFacadeLocal {

    void create(ScheduleActAndTask scheduleActAndTask);

    void edit(ScheduleActAndTask scheduleActAndTask);

    void remove(ScheduleActAndTask scheduleActAndTask);

    ScheduleActAndTask find(Object id);

    List<ScheduleActAndTask> findAll();

    List<ScheduleActAndTask> findRange(int[] range);

    int count();
    
}
