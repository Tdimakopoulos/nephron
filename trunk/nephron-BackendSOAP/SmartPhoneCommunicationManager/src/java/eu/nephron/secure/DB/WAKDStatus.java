/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.DB;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Tom
 */
@Entity
@NamedQueries({
	@NamedQuery(name="WAKDStatus.findByIMEI", query="select a from WAKDStatus a where a.IMEI=:FindIMEI")
})
public class WAKDStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String IMEI;
    private String OpState;
    private String dState;
    private Long ddate;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WAKDStatus)) {
            return false;
        }
        WAKDStatus other = (WAKDStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.WAKDStatus[ id=" + id + " ]";
    }

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the OpState
     */
    public String getOpState() {
        return OpState;
    }

    /**
     * @param OpState the OpState to set
     */
    public void setOpState(String OpState) {
        this.OpState = OpState;
    }

    /**
     * @return the dState
     */
    public String getdState() {
        return dState;
    }

    /**
     * @param dState the dState to set
     */
    public void setdState(String dState) {
        this.dState = dState;
    }

    /**
     * @return the ddate
     */
    public Long getDdate() {
        return ddate;
    }

    /**
     * @param ddate the ddate to set
     */
    public void setDdate(Long ddate) {
        this.ddate = ddate;
    }
    
}
