/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.ws;

import eu.nephron.secure.DB.WAKDAlerts;
import eu.nephron.secure.DB.WAKDMeasurments;
import eu.nephron.secure.beans.WAKDMeasurmentsFacadeLocal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author Tom
 */
@WebService(serviceName = "WAKDMeasurmentsManager")
public class WAKDMeasurmentsManager {
    @EJB
    private WAKDMeasurmentsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

 @PersistenceUnit
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEI")
    public List<WAKDMeasurments> findWAKDMeasurmentsManagerWithIMEI(@WebParam(name = "IMEI") String IMEI) {
        
        
        
         assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        
        List<WAKDMeasurments> wakd = em.createQuery("select a from WAKDMeasurments a where a.IMEI=:IMEI order by a.ddate desc").setParameter("IMEI", IMEI).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEIAndType")
    public List<WAKDMeasurments> findWAKDMeasurmentsManagerWithIMEIAndType(@WebParam(name = "IMEI") String IMEI,Long TYPE) {
        
        
        
         assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        
        List<WAKDMeasurments> wakd = em.createQuery("select a from WAKDMeasurments a where a.IMEI=:IMEI AND a.type=:TYPE order by a.ddate desc").setParameter("IMEI", IMEI).setParameter("TYPE", TYPE).getResultList();
        return wakd;
    }
    
    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom")
    public List<WAKDMeasurments> findWAKDMeasurmentsManagerWithIMEIAndTypeAmdDateFrom(@WebParam(name = "IMEI") String IMEI,Long TYPE,Long DateFrom) {
        
        
        
         assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        
        List<WAKDMeasurments> wakd = em.createQuery("select a from WAKDMeasurments a where a.IMEI=:IMEI AND a.type=:TYPE AND a.ddate>:DateFrom order by a.ddate desc").setParameter("IMEI", IMEI).setParameter("TYPE", TYPE).setParameter("DateFrom", DateFrom).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "ReturnAllMeasurmentsForLastDay")
    public List<WAKDMeasurments> ReturnAllMeasurmentsForLastDay() {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = emf.createEntityManager();


        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date today7 = cal.getTime();
        Long DateFrom = today7.getTime();
        
        List<WAKDMeasurments> wakd = em.createQuery("select a from WAKDMeasurments a where a.ddate>:DateFrom order by a.ddate desc").setParameter("DateFrom", DateFrom).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "ReturnAllMeasurmentsForLastDayAndType")
    public List<WAKDMeasurments> ReturnAllMeasurmentsForLastDayAndType(Long TYPE) {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em  = emf.createEntityManager();


        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date today7 = cal.getTime();
        Long DateFrom = today7.getTime();
        
        List<WAKDMeasurments> wakd = em.createQuery("select a from WAKDMeasurments a where a.type=:TYPE AND a.ddate>:DateFrom order by a.ddate desc").setParameter("TYPE", TYPE).setParameter("DateFrom", DateFrom).getResultList();
        return wakd;
    }
    
    @WebMethod(operationName = "findWAKDMeasurmentsManager")
    public WAKDMeasurments findWAKDMeasurmentsManager(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllWAKDMeasurmentsManager")
    public List<WAKDMeasurments> findAllWAKDMeasurmentsManager() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeWAKDMeasurmentsManager")
    public List<WAKDMeasurments> findRangeWAKDMeasurmentsManager(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
