/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.beans;

import eu.nephron.secure.DB.WAKDMeasurments;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Tom
 */
@Stateless
public class WAKDMeasurmentsFacade extends AbstractFacade<WAKDMeasurments> implements WAKDMeasurmentsFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WAKDMeasurmentsFacade() {
        super(WAKDMeasurments.class);
    }
    
}
