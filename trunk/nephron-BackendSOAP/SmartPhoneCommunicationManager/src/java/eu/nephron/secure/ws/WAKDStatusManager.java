/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.ws;

import eu.nephron.secure.DB.WAKDMeasurments;
import eu.nephron.secure.DB.WAKDStatus;
import eu.nephron.secure.beans.WAKDStatusFacadeLocal;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author Tom
 */
@WebService(serviceName = "WAKDStatusManager")
public class WAKDStatusManager {
    @EJB
    private WAKDStatusFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")


@PersistenceUnit
    private EntityManagerFactory emf;

    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "findWAKDStatusManagerWithIMEI")
    public List<WAKDStatus> findWAKDStatusManagerWithIMEI(@WebParam(name = "IMEI") String IMEI) {
        
        
        
         assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();

        
        List<WAKDStatus> wakd = em.createQuery("select a from WAKDStatus a where a.IMEI=:IMEI").setParameter("IMEI", IMEI).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "findWAKDStatusManager")
    public WAKDStatus findWAKDStatusManager(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllWAKDStatusManager")
    public List<WAKDStatus> findAllWAKDStatusManager() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeWAKDStatusManager")
    public List<WAKDStatus> findRangeWAKDStatusManager(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countWAKDStatusManager")
    public int countWAKDStatusManager() {
        return ejbRef.count();
    }
    
}
