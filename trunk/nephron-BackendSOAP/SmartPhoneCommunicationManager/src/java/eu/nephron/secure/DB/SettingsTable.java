/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.DB;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author tdim
 */
@Entity
@NamedQueries({
	@NamedQuery(name="SettingsTable.findByIMEI", query="select a from SettingsTable a where a.imei=:FindIMEI")
})
public class SettingsTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String optionname;
    private String optionvalue;
    private String imei;
    
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SettingsTable)) {
            return false;
        }
        SettingsTable other = (SettingsTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.SettingsTable[ id=" + id + " ]";
    }

    /**
     * @return the optionname
     */
    public String getOptionname() {
        return optionname;
    }

    /**
     * @param optionname the optionname to set
     */
    public void setOptionname(String optionname) {
        this.optionname = optionname;
    }

    /**
     * @return the optionvalue
     */
    public String getOptionvalue() {
        return optionvalue;
    }

    /**
     * @param optionvalue the optionvalue to set
     */
    public void setOptionvalue(String optionvalue) {
        this.optionvalue = optionvalue;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }
    
}
