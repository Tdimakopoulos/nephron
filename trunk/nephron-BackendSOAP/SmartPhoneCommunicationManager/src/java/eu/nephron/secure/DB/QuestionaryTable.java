/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.DB;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author tdim
 */
@Entity
@NamedQueries({
	@NamedQuery(name="QuestionaryTable.findByIMEI", query="select a from QuestionaryTable a where a.imei=:FindIMEI")
})
public class QuestionaryTable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String questionid;
    private String imei;
    private String dated;
    private String reply;
    private boolean breply;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QuestionaryTable)) {
            return false;
        }
        QuestionaryTable other = (QuestionaryTable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.secure.DB.QuestionaryTable[ id=" + id + " ]";
    }

    /**
     * @return the questionid
     */
    public String getQuestionid() {
        return questionid;
    }

    /**
     * @param questionid the questionid to set
     */
    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the dated
     */
    public String getDated() {
        return dated;
    }

    /**
     * @param dated the dated to set
     */
    public void setDated(String dated) {
        this.dated = dated;
    }

    /**
     * @return the reply
     */
    public String getReply() {
        return reply;
    }

    /**
     * @param reply the reply to set
     */
    public void setReply(String reply) {
        this.reply = reply;
    }

    /**
     * @return the breply
     */
    public boolean isBreply() {
        return breply;
    }

    /**
     * @param breply the breply to set
     */
    public void setBreply(boolean breply) {
        this.breply = breply;
    }
    
}
