/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.beans;

import eu.nephron.secure.DB.WAKDMeasurments;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Tom
 */
@Local
public interface WAKDMeasurmentsFacadeLocal {

    void create(WAKDMeasurments wAKDMeasurments);

    void edit(WAKDMeasurments wAKDMeasurments);

    void remove(WAKDMeasurments wAKDMeasurments);

    WAKDMeasurments find(Object id);

    List<WAKDMeasurments> findAll();

    List<WAKDMeasurments> findRange(int[] range);

    int count();
    
}
