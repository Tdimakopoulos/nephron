/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.secure.beans;

import eu.nephron.secure.DB.WAKDAlerts;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Tom
 */
@Local
public interface WAKDAlertsFacadeLocal {

    void create(WAKDAlerts wAKDAlerts);

    void edit(WAKDAlerts wAKDAlerts);

    void remove(WAKDAlerts wAKDAlerts);

    WAKDAlerts find(Object id);

    List<WAKDAlerts> findAll();

    List<WAKDAlerts> findRange(int[] range);

    int count();
    
}
