/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.weight;

import eu.nephron.secure.DB.WeightMeasurments;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface WeightMeasurmentsFacadeLocal {

    void create(WeightMeasurments weightMeasurments);

    void edit(WeightMeasurments weightMeasurments);

    void remove(WeightMeasurments weightMeasurments);

    WeightMeasurments find(Object id);

    List<WeightMeasurments> findAll();

    List<WeightMeasurments> findRange(int[] range);

    int count();
    
}
