/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.weight;

import eu.nephron.secure.DB.WeightMeasurments;
import eu.nephron.secure.beans.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class WeightMeasurmentsFacade extends AbstractFacade<WeightMeasurments> implements WeightMeasurmentsFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WeightMeasurmentsFacade() {
        super(WeightMeasurments.class);
    }
    
}
