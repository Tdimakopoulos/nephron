/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.weight;

import eu.nephron.WeightDataValues.bean.weightdatavaluesFacadeLocal;
import eu.nephron.WeightDataValues.db.weightdatavalues;
import eu.nephron.secure.DB.WeightMeasurments;
import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.measurments.WeightIncomingAPI;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "WeightManager")
public class WeightManager {
    @EJB
    private WeightMeasurmentsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")
    
    @EJB
    private weightdatavaluesFacadeLocal ejbRefwdv;

    
    @WebMethod(operationName = "WeightManagerNewValueReceived")
    public String WeightManagerNewValueReceived(
             @WebParam(name = "imei")String imei,
     @WebParam(name = "date")String dated,
     @WebParam(name = "reply")String reply) {
        WeightMeasurments pvarweightMeasurments= new WeightMeasurments();
        pvarweightMeasurments.setDated(dated);
        pvarweightMeasurments.setImei(imei);
        pvarweightMeasurments.setReply(reply);
        ejbRef.create(pvarweightMeasurments);
        
        byte[] pmsg = ByteUtils.convertStringToByteArray(reply);
        WeightIncomingAPI pWIAPI = new WeightIncomingAPI();
        pWIAPI.SetMSG(pmsg, true);
        pWIAPI.decode();
        if(pWIAPI.IsWeight())
        {
            weightdatavalues entity= new weightdatavalues();
            entity.setBodyfat(pWIAPI.getBodyfat_s());
            entity.setImei(imei);
            entity.setTbd("To be Define");
            entity.setWeight(pWIAPI.getWeight_s());
            entity.setWsbattery(pWIAPI.getWsbattery_s());
            //entity.setWtime(pWIAPI.getTime_s());
            entity.setWtime(new Date().getTime());
            ejbRefwdv.create(entity);
        }
        return "ok";
    }
    
    @WebMethod(operationName = "WeightManagercreate")
    @Oneway
    public void WeightManagercreate(@WebParam(name = "weightMeasurments") WeightMeasurments weightMeasurments) {
        ejbRef.create(weightMeasurments);
    }

    @WebMethod(operationName = "WeightManageredit")
    @Oneway
    public void WeightManageredit(@WebParam(name = "weightMeasurments") WeightMeasurments weightMeasurments) {
        ejbRef.edit(weightMeasurments);
    }

    @WebMethod(operationName = "WeightManagerremove")
    @Oneway
    public void WeightManagerremove(@WebParam(name = "weightMeasurments") WeightMeasurments weightMeasurments) {
        ejbRef.remove(weightMeasurments);
    }

    @WebMethod(operationName = "WeightManagerfind")
    public WeightMeasurments WeightManagerfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "WeightManagerfindAll")
    public List<WeightMeasurments> WeightManagerfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "WeightManagerfindRange")
    public List<WeightMeasurments> WeightManagerfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "WeightManagercount")
    public int WeightManagercount() {
        return ejbRef.count();
    }
    
}
