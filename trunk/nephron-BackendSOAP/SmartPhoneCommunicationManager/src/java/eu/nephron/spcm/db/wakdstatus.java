/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class wakdstatus implements Serializable {
    private static final long serialVersionUID = 1234242523234432455L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

     private String szIMEI;
    private String szWAKDStatus;
    private Long ddatereceive;
    private String sztextfield1;
    private String sztextfield2;
    private String sztextfield3;
    private String sztextfield4;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof wakdstatus)) {
            return false;
        }
        wakdstatus other = (wakdstatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.spcm.db.wakdstatus[ id=" + id + " ]";
    }

    /**
     * @return the szIMEI
     */
    public String getSzIMEI() {
        return szIMEI;
    }

    /**
     * @param szIMEI the szIMEI to set
     */
    public void setSzIMEI(String szIMEI) {
        this.szIMEI = szIMEI;
    }

    /**
     * @return the szWAKDStatus
     */
    public String getSzWAKDStatus() {
        return szWAKDStatus;
    }

    /**
     * @param szWAKDStatus the szWAKDStatus to set
     */
    public void setSzWAKDStatus(String szWAKDStatus) {
        this.szWAKDStatus = szWAKDStatus;
    }

    /**
     * @return the ddatereceive
     */
    public Long getDdatereceive() {
        return ddatereceive;
    }

    /**
     * @param ddatereceive the ddatereceive to set
     */
    public void setDdatereceive(Long ddatereceive) {
        this.ddatereceive = ddatereceive;
    }

    /**
     * @return the sztextfield1
     */
    public String getSztextfield1() {
        return sztextfield1;
    }

    /**
     * @param sztextfield1 the sztextfield1 to set
     */
    public void setSztextfield1(String sztextfield1) {
        this.sztextfield1 = sztextfield1;
    }

    /**
     * @return the sztextfield2
     */
    public String getSztextfield2() {
        return sztextfield2;
    }

    /**
     * @param sztextfield2 the sztextfield2 to set
     */
    public void setSztextfield2(String sztextfield2) {
        this.sztextfield2 = sztextfield2;
    }

    /**
     * @return the sztextfield3
     */
    public String getSztextfield3() {
        return sztextfield3;
    }

    /**
     * @param sztextfield3 the sztextfield3 to set
     */
    public void setSztextfield3(String sztextfield3) {
        this.sztextfield3 = sztextfield3;
    }

    /**
     * @return the sztextfield4
     */
    public String getSztextfield4() {
        return sztextfield4;
    }

    /**
     * @param sztextfield4 the sztextfield4 to set
     */
    public void setSztextfield4(String sztextfield4) {
        this.sztextfield4 = sztextfield4;
    }

  

    
    
}
