/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.webservices;

import eu.nephron.secure.DB.WAKDAlerts;
import eu.nephron.secure.DB.WAKDMeasurments;
import eu.nephron.secure.DB.WAKDStatus;
import eu.nephron.secure.beans.WAKDAlertsFacadeLocal;
import eu.nephron.secure.beans.WAKDMeasurmentsFacadeLocal;
import eu.nephron.secure.beans.WAKDStatusFacadeLocal;
import eu.nephron.spcm.db.btop;
import eu.nephron.spcm.db.ptob;
import eu.nephron.spcm.db.wakdstatus;
import eu.nephron.spcm.facade.btopFacadeLocal;
import eu.nephron.spcm.facade.ptobFacadeLocal;
import eu.nephron.spcm.facade.wakdstatusFacadeLocal;
import eu.nephron.textconv.AlarmsMsg;
import eu.nephron.utils.ByteUtils;
//import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.incoming.AlertMsg;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;
import eu.nephron.wakd.measurments.MeasurmentsIncomingAPI;
import eu.nephron.wakd.measurments.MeasurmentsPhysicalIncomingAPI;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "CommunicationManager")
public class CommunicationManager {

    @EJB
    private btopFacadeLocal ejbbtop;
    @EJB
    private ptobFacadeLocal ejbptob;
    @EJB
    private wakdstatusFacadeLocal ejbwakds;
    @EJB
    private WAKDStatusFacadeLocal ejbRef1;
    @EJB
    private WAKDMeasurmentsFacadeLocal ejbRef2;
    @EJB
    private WAKDAlertsFacadeLocal ejbRef3;

    public void StoreWAKD(@WebParam(name = "imei") String IMEI, @WebParam(name = "wakdstatus") String WAKDStatus, @WebParam(name = "date") Long Dater, @WebParam(name = "param1") String txt1,
            @WebParam(name = "param2") String txt2, @WebParam(name = "param3") String txt3, @WebParam(name = "param4") String txt4) {
        wakdstatus pEntity = new wakdstatus();
        List<wakdstatus> pList = ejbwakds.findAll();

        //if we have entry for IMEI update
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getSzIMEI().equalsIgnoreCase(IMEI)) {
                pEntity = pList.get(i);
                pEntity.setDdatereceive(Dater);
                pEntity.setSzWAKDStatus(WAKDStatus);
                ejbwakds.edit(pEntity);
                return;
            }
        }

        //No Entry find then add
        pEntity.setDdatereceive(Dater);
        pEntity.setSzIMEI(IMEI);
        pEntity.setSzWAKDStatus(WAKDStatus);
        pEntity.setSztextfield1(txt1);
        pEntity.setSztextfield2(txt2);
        pEntity.setSztextfield3(txt3);
        pEntity.setSztextfield4(txt4);
        ejbwakds.create(pEntity);

        byte[] pmsg2 = ByteUtils.convertStringToByteArray("0400080403010101");
        CurrentStateMsg pMsg = new CurrentStateMsg();
        pMsg.decode(pmsg2);
        WAKDStatus wAKDStatus = new WAKDStatus();
        wAKDStatus.setIMEI(IMEI);
        wAKDStatus.setOpState(pMsg.getCurrentOpState().toString());
        wAKDStatus.setdState(pMsg.getCurrentState().toString());
        wAKDStatus.setDdate((new Date()).getTime());
        ejbRef1.create(wAKDStatus);
    }

    public void StoreCommand(@WebParam(name = "imei") String IMEI, @WebParam(name = "command") String Command, @WebParam(name = "date") Long Dater, @WebParam(name = "param1") String txt1,
            @WebParam(name = "param2") String txt2, @WebParam(name = "param3") String txt3, @WebParam(name = "param4") String txt4) {
        ptob pEntity = new ptob();
        pEntity.setDatereceive(Dater);
        pEntity.setIMEI(IMEI);
        pEntity.setCommand(Command);
        pEntity.setTextfield1(txt1);
        pEntity.setTextfield2(txt2);
        pEntity.setTextfield3(txt3);
        pEntity.setTextfield4(txt4);
        ejbptob.create(pEntity);

        byte[] pmsg = ByteUtils.convertStringToByteArray(Command);
        MeasurmentsIncomingAPI pp = new MeasurmentsIncomingAPI();
        MeasurmentsPhysicalIncomingAPI pp_2 = new MeasurmentsPhysicalIncomingAPI();
        pp.SetMSG(pmsg, true);
        pp_2.SetMSG(pmsg, true);
        if (pp_2.IsPHYSICAL()) {
            WAKDMeasurments wAKDMeasurments = new WAKDMeasurments();
            wAKDMeasurments.setDate((new Date()).getTime());
            wAKDMeasurments.setIMEI(IMEI);
            wAKDMeasurments.setType(new Long(11));
            wAKDMeasurments.setValue(Double.parseDouble(pp_2.getCondFCR()));
            ejbRef2.create(wAKDMeasurments);

            WAKDMeasurments wAKDMeasurments22 = new WAKDMeasurments();
            wAKDMeasurments22.setDate((new Date()).getTime());
            wAKDMeasurments22.setIMEI(IMEI);
            wAKDMeasurments22.setType(new Long(12));
            wAKDMeasurments22.setValue(Double.parseDouble(pp_2.getCondPT()));
            ejbRef2.create(wAKDMeasurments22);

            WAKDMeasurments wAKDMeasurments33 = new WAKDMeasurments();
            wAKDMeasurments33.setDate((new Date()).getTime());
            wAKDMeasurments33.setIMEI(IMEI);
            wAKDMeasurments33.setType(new Long(13));
            wAKDMeasurments33.setValue(Double.parseDouble(pp_2.getPressureBCI()));
            ejbRef2.create(wAKDMeasurments33);

            WAKDMeasurments wAKDMeasurments44 = new WAKDMeasurments();
            wAKDMeasurments44.setDate((new Date()).getTime());
            wAKDMeasurments44.setIMEI(IMEI);
            wAKDMeasurments44.setType(new Long(14));
            wAKDMeasurments44.setValue(Double.parseDouble(pp_2.getPressureBCO()));
            ejbRef2.create(wAKDMeasurments44);

            WAKDMeasurments wAKDMeasurments55 = new WAKDMeasurments();
            wAKDMeasurments55.setDate((new Date()).getTime());
            wAKDMeasurments55.setIMEI(IMEI);
            wAKDMeasurments55.setType(new Long(15));
            wAKDMeasurments55.setValue(Double.parseDouble(pp_2.getPressureFCI()));
            ejbRef2.create(wAKDMeasurments55);

            WAKDMeasurments wAKDMeasurments66 = new WAKDMeasurments();
            wAKDMeasurments66.setDate((new Date()).getTime());
            wAKDMeasurments66.setIMEI(IMEI);
            wAKDMeasurments66.setType(new Long(16));
            wAKDMeasurments66.setValue(Double.parseDouble(pp_2.getPressureFCO()));
            ejbRef2.create(wAKDMeasurments66);

            WAKDMeasurments wAKDMeasurments77 = new WAKDMeasurments();
            wAKDMeasurments77.setDate((new Date()).getTime());
            wAKDMeasurments77.setIMEI(IMEI);
            wAKDMeasurments77.setType(new Long(17));
            wAKDMeasurments77.setValue(Double.parseDouble(pp_2.getTempIN()));
            ejbRef2.create(wAKDMeasurments77);

            WAKDMeasurments wAKDMeasurments88 = new WAKDMeasurments();
            wAKDMeasurments88.setDate((new Date()).getTime());
            wAKDMeasurments88.setIMEI(IMEI);
            wAKDMeasurments88.setType(new Long(18));
            wAKDMeasurments88.setValue(Double.parseDouble(pp_2.getTempOut()));
            ejbRef2.create(wAKDMeasurments88);
        } //we have measurments save them
        else if (pp.IsPHYSIOLOGICAL()) {

            pp.decode();

            WAKDMeasurments wAKDMeasurments = new WAKDMeasurments();
            wAKDMeasurments.setDate((new Date()).getTime());
            wAKDMeasurments.setIMEI(IMEI);
            wAKDMeasurments.setType(new Long(1));
            wAKDMeasurments.setValue(pp.GetInletPH());
            ejbRef2.create(wAKDMeasurments);

            WAKDMeasurments wAKDMeasurments1 = new WAKDMeasurments();
            wAKDMeasurments1.setDate((new Date()).getTime());
            wAKDMeasurments1.setIMEI(IMEI);
            wAKDMeasurments1.setType(new Long(2));
            wAKDMeasurments1.setValue(pp.GetInletPotasium());
            ejbRef2.create(wAKDMeasurments1);

            WAKDMeasurments wAKDMeasurments3 = new WAKDMeasurments();
            wAKDMeasurments3.setDate((new Date()).getTime());
            wAKDMeasurments3.setIMEI(IMEI);
            wAKDMeasurments3.setType(new Long(3));
            wAKDMeasurments3.setValue(pp.GetInletSodium());
            ejbRef2.create(wAKDMeasurments3);

            WAKDMeasurments wAKDMeasurments4 = new WAKDMeasurments();
            wAKDMeasurments4.setDate((new Date()).getTime());
            wAKDMeasurments4.setIMEI(IMEI);
            wAKDMeasurments4.setType(new Long(4));
            wAKDMeasurments4.setValue(pp.GetInletTemp());
            ejbRef2.create(wAKDMeasurments4);

            WAKDMeasurments wAKDMeasurments5 = new WAKDMeasurments();
            wAKDMeasurments5.setDate((new Date()).getTime());
            wAKDMeasurments5.setIMEI(IMEI);
            wAKDMeasurments5.setType(new Long(5));
            wAKDMeasurments5.setValue(pp.GetInletUria());
            ejbRef2.create(wAKDMeasurments5);

            WAKDMeasurments wAKDMeasurments6 = new WAKDMeasurments();
            wAKDMeasurments6.setDate((new Date()).getTime());
            wAKDMeasurments6.setIMEI(IMEI);
            wAKDMeasurments6.setType(new Long(6));
            wAKDMeasurments6.setValue(pp.GetOutletPH());
            ejbRef2.create(wAKDMeasurments6);

            WAKDMeasurments wAKDMeasurments7 = new WAKDMeasurments();
            wAKDMeasurments7.setDate((new Date()).getTime());
            wAKDMeasurments7.setIMEI(IMEI);
            wAKDMeasurments7.setType(new Long(7));
            wAKDMeasurments7.setValue(pp.GetOutletPotasium());
            ejbRef2.create(wAKDMeasurments7);

            WAKDMeasurments wAKDMeasurments8 = new WAKDMeasurments();
            wAKDMeasurments8.setDate((new Date()).getTime());
            wAKDMeasurments8.setIMEI(IMEI);
            wAKDMeasurments8.setType(new Long(8));
            wAKDMeasurments8.setValue(pp.GetOutletSodium());
            ejbRef2.create(wAKDMeasurments8);

            WAKDMeasurments wAKDMeasurments9 = new WAKDMeasurments();
            wAKDMeasurments9.setDate((new Date()).getTime());
            wAKDMeasurments9.setIMEI(IMEI);
            wAKDMeasurments9.setType(new Long(9));
            wAKDMeasurments9.setValue(pp.GetOutletTemp());
            ejbRef2.create(wAKDMeasurments9);

            WAKDMeasurments wAKDMeasurments10 = new WAKDMeasurments();
            wAKDMeasurments10.setDate((new Date()).getTime());
            wAKDMeasurments10.setIMEI(IMEI);
            wAKDMeasurments10.setType(new Long(10));
            wAKDMeasurments10.setValue(pp.GetOutletUria());
            ejbRef2.create(wAKDMeasurments10);

        } //we have alert save it
        else {
            AlertMsg pmm = new AlertMsg();
            pmm.decode(pmsg);
            //System.out.println(pmm.getAlert());
            AlarmsMsg pp2;
            pp2 = new AlarmsMsg(pmm.GetAlertValue());
            //System.out.println(pp2.getmsgAlert());
            //System.out.println(pp2.getmsgDoctor());
            //System.out.println(pp2.getmsgPatient());
            WAKDAlerts wAKDAlerts = new WAKDAlerts();
            wAKDAlerts.setDdate((new Date()).getTime());
            wAKDAlerts.setIMEI(IMEI);
            wAKDAlerts.setMsgAlert(pmm.getAlert().toString());
            wAKDAlerts.setMsgDoctor(pp2.getmsgDoctor().toString());
            wAKDAlerts.setMsgPatient(pp2.getmsgPatient().toString());
            ejbRef3.create(wAKDAlerts);
        }



    }

    public String RetrieveCommand(@WebParam(name = "imei") String IMEI) {
        String Command = null;
        List<btop> pList = ejbbtop.findAll();
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getIMEI().equalsIgnoreCase(IMEI)) {
                if (!pList.get(i).isACK()) {
                    Command = pList.get(i).getId() + ":" + pList.get(i).getCommand();
                    return Command;
                }
            }
        }
        return Command;
    }

    public String MarkACK(@WebParam(name = "commandid") Long CommandID) {
        List<btop> pList = ejbbtop.findAll();
        System.out.println("Look for : " + CommandID);
        for (int i = 0; i < pList.size(); i++) {
            System.out.println("ID:" + pList.get(i).getId());
            if (pList.get(i).getId().toString().equalsIgnoreCase(CommandID.toString())) {
                System.out.println("Found do update");
                btop pElement = pList.get(i);
                pElement.setACK(true);
                ejbbtop.edit(pElement);
                return "Command Updated";
            }
        }
        return "Command ID does not exist";
    }
}
