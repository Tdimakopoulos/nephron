/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class ptob implements Serializable {
    private static final long serialVersionUID = 134678432468L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String IMEI;
    private String Command;
    private Long datereceive;
    private String textfield1;
    private String textfield2;
    private String textfield3;
    private String textfield4;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ptob)) {
            return false;
        }
        ptob other = (ptob) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.spcm.db.ptob[ id=" + id + " ]";
    }

    /**
     * @return the IMEI
     */
    public String getIMEI() {
        return IMEI;
    }

    /**
     * @param IMEI the IMEI to set
     */
    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    /**
     * @return the Command
     */
    public String getCommand() {
        return Command;
    }

    /**
     * @param Command the Command to set
     */
    public void setCommand(String Command) {
        this.Command = Command;
    }

    /**
     * @return the datereceive
     */
    public Long getDatereceive() {
        return datereceive;
    }

    /**
     * @param datereceive the datereceive to set
     */
    public void setDatereceive(Long datereceive) {
        this.datereceive = datereceive;
    }

    /**
     * @return the textfield1
     */
    public String getTextfield1() {
        return textfield1;
    }

    /**
     * @param textfield1 the textfield1 to set
     */
    public void setTextfield1(String textfield1) {
        this.textfield1 = textfield1;
    }

    /**
     * @return the textfield2
     */
    public String getTextfield2() {
        return textfield2;
    }

    /**
     * @param textfield2 the textfield2 to set
     */
    public void setTextfield2(String textfield2) {
        this.textfield2 = textfield2;
    }

    /**
     * @return the textfield3
     */
    public String getTextfield3() {
        return textfield3;
    }

    /**
     * @param textfield3 the textfield3 to set
     */
    public void setTextfield3(String textfield3) {
        this.textfield3 = textfield3;
    }

    /**
     * @return the textfield4
     */
    public String getTextfield4() {
        return textfield4;
    }

    /**
     * @param textfield4 the textfield4 to set
     */
    public void setTextfield4(String textfield4) {
        this.textfield4 = textfield4;
    }
    
}
