/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.facade;

import eu.nephron.spcm.db.btop;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface btopFacadeLocal {

    void create(btop btop);

    void edit(btop btop);

    void remove(btop btop);

    btop find(Object id);

    List<btop> findAll();

    List<btop> findRange(int[] range);

    int count();
    
}
