/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.facade;

import eu.nephron.spcm.db.ptob;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface ptobFacadeLocal {

    void create(ptob ptob);

    void edit(ptob ptob);

    void remove(ptob ptob);

    ptob find(Object id);

    List<ptob> findAll();

    List<ptob> findRange(int[] range);

    int count();
    
}
