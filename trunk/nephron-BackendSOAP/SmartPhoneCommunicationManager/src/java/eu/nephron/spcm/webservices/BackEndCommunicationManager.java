/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.spcm.webservices;

import eu.nephron.spcm.db.btop;
import eu.nephron.spcm.db.ptob;
import eu.nephron.spcm.db.wakdstatus;
import eu.nephron.spcm.facade.btopFacadeLocal;
import eu.nephron.spcm.facade.ptobFacadeLocal;
import eu.nephron.spcm.facade.wakdstatusFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "BackEndCommunicationManager")
public class BackEndCommunicationManager {

//    /**
//     * This is a sample web service operation
//     */
//    @WebMethod(operationName = "hello")
//    public String hello(@WebParam(name = "name") String txt) {
//        return "Hello " + txt + " !";
//    }
    @EJB
    private btopFacadeLocal ejbbtop;
    @EJB
    private ptobFacadeLocal ejbptob;
    @EJB
    private wakdstatusFacadeLocal ejbwakds;
    
    public void StorePhoneCommand(@WebParam(name = "imei")String IMEI,@WebParam(name = "command") String Command, @WebParam(name = "date")Long Dater, @WebParam(name = "param1")String txt1, @WebParam(name = "param2")String txt2, @WebParam(name = "param3")String txt3, @WebParam(name = "param4")String txt4) {
        btop pEntity = new btop();
        pEntity.setDatereceive(Dater);
        pEntity.setIMEI(IMEI);
        pEntity.setCommand(Command);
        pEntity.setTextfield1(txt1);
        pEntity.setTextfield2(txt2);
        pEntity.setTextfield3(txt3);
        pEntity.setTextfield4(txt4);
        pEntity.setACK(false);
        pEntity.setDateack(new Long(0));
        ejbbtop.create(pEntity);
    }
    
    public String RetrieveWAKDStatus(@WebParam(name = "imei")String IMEI) {
        String Command = "-1";
        List<wakdstatus> pList = ejbwakds.findAll();
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getSzIMEI().equalsIgnoreCase(IMEI)) {
                
                    Command = pList.get(i).getSzWAKDStatus();
                    return Command;
                
            }
        }
        return Command;
    }
    
    public List<ptob> RetrievePhoneCommands(@WebParam(name = "imei")String IMEI)
    {
        List<ptob> preturn = ejbptob.findAll();
        for(int i=0;i<preturn.size();i++)
        {
            if (preturn.get(i).getIMEI().equalsIgnoreCase(IMEI))
            {
                // we keep this record we are intrested on this command from the phone with IMEI
            }else
            {
                // other phone delete this entry.
                preturn.remove(i);
            }
        }
        return preturn;
    }
}
