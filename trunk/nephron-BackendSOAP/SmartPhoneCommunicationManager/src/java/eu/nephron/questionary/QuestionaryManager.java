/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.questionary;

import eu.nephron.secure.DB.QuestionaryTable;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "QuestionaryManager")
public class QuestionaryManager {

    @EJB
    private QuestionaryTableFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "QuestionaryManagerSaveQuestionReply")
    public String QuestionaryManagerSaveQuestionReply(
            @WebParam(name = "questionid") String questionid,
            @WebParam(name = "imei") String imei,
            @WebParam(name = "questionarydate") String dated,
            @WebParam(name = "questionreply") String reply) {
        QuestionaryTable questionaryTable = new QuestionaryTable();
        if (reply.equalsIgnoreCase("true")) {
            questionaryTable.setBreply(true);
        }
        if (reply.equalsIgnoreCase("false")) {
            questionaryTable.setBreply(false);
        }
        questionaryTable.setDated(dated);
        questionaryTable.setImei(imei);
        questionaryTable.setQuestionid(questionid);
        questionaryTable.setReply(reply);
        ejbRef.create(questionaryTable);
        return "ok";
    }

    @WebMethod(operationName = "QuestionaryManagercreate")
    @Oneway
    public void QuestionaryManagercreate(@WebParam(name = "questionaryTable") QuestionaryTable questionaryTable) {
        ejbRef.create(questionaryTable);
    }

    @WebMethod(operationName = "QuestionaryManageredit")
    @Oneway
    public void QuestionaryManageredit(@WebParam(name = "questionaryTable") QuestionaryTable questionaryTable) {
        ejbRef.edit(questionaryTable);
    }

    @WebMethod(operationName = "QuestionaryManagerremove")
    @Oneway
    public void QuestionaryManagerremove(@WebParam(name = "questionaryTable") QuestionaryTable questionaryTable) {
        ejbRef.remove(questionaryTable);
    }

    @WebMethod(operationName = "QuestionaryManagerfind")
    public QuestionaryTable QuestionaryManagerfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "QuestionaryManagerfindAll")
    public List<QuestionaryTable> QuestionaryManagerfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "QuestionaryManagerfindRange")
    public List<QuestionaryTable> QuestionaryManagerfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "QuestionaryManagercount")
    public int QuestionaryManagercount() {
        return ejbRef.count();
    }
}
