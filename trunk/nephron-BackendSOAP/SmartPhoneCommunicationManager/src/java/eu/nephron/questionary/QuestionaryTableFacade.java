/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.questionary;

import eu.nephron.secure.DB.QuestionaryTable;
import eu.nephron.secure.beans.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class QuestionaryTableFacade extends AbstractFacade<QuestionaryTable> implements QuestionaryTableFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuestionaryTableFacade() {
        super(QuestionaryTable.class);
    }
    
}
