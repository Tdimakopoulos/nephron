/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.questionary;

import eu.nephron.secure.DB.QuestionaryTable;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface QuestionaryTableFacadeLocal {

    void create(QuestionaryTable questionaryTable);

    void edit(QuestionaryTable questionaryTable);

    void remove(QuestionaryTable questionaryTable);

    QuestionaryTable find(Object id);

    List<QuestionaryTable> findAll();

    List<QuestionaryTable> findRange(int[] range);

    int count();
    
}
