/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.schedule.webservices;

import eu.nephron.secure.DB.ScheduleActAndTask;
import eu.nephron.secure.beans.ScheduleActAndTaskFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ScheduleActAndTaskManager")
public class ScheduleActAndTaskManager {
    @EJB
    private ScheduleActAndTaskFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createScheduleActAndTaskManager")
    @Oneway
    public void createScheduleActAndTaskManager(@WebParam(name = "scheduleActAndTask") ScheduleActAndTask scheduleActAndTask) {
        ejbRef.create(scheduleActAndTask);
    }

    @WebMethod(operationName = "editScheduleActAndTaskManager")
    @Oneway
    public void editScheduleActAndTaskManager(@WebParam(name = "scheduleActAndTask") ScheduleActAndTask scheduleActAndTask) {
        ejbRef.edit(scheduleActAndTask);
    }

    @WebMethod(operationName = "removeScheduleActAndTaskManager")
    @Oneway
    public void removeScheduleActAndTaskManager(@WebParam(name = "scheduleActAndTask") ScheduleActAndTask scheduleActAndTask) {
        ejbRef.remove(scheduleActAndTask);
    }

    @WebMethod(operationName = "findScheduleActAndTaskManager")
    public ScheduleActAndTask findScheduleActAndTaskManager(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllScheduleActAndTaskManager")
    public List<ScheduleActAndTask> findAllScheduleActAndTaskManager() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeScheduleActAndTaskManager")
    public List<ScheduleActAndTask> findRangeScheduleActAndTaskManager(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countScheduleActAndTaskManager")
    public int countScheduleActAndTaskManager() {
        return ejbRef.count();
    }
    
}
