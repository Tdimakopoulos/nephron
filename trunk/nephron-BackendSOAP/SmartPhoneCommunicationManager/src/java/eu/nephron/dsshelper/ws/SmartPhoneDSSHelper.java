/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.dsshelper.ws;

import eu.nephron.secure.DB.QuestionaryTable;
import eu.nephron.secure.DB.WAKDAlerts;
import eu.nephron.secure.DB.WAKDMeasurments;
import eu.nephron.secure.DB.WAKDStatus;
import eu.nephron.secure.DB.WeightMeasurments;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "SmartPhoneDSSHelper")
public class SmartPhoneDSSHelper {

    @PersistenceUnit
    private EntityManagerFactory emf;
    @Resource
    private UserTransaction utx;

    /**
     * This is a sample web service operation
     */
//    @WebMethod(operationName = "hello")
//    public String hello(@WebParam(name = "name") String txt) {
//        return "Hello " + txt + " !";
//    }

    @WebMethod(operationName = "SelectWeightMeasurmentsByIMEI")
    public List<WeightMeasurments> SelectWeightMeasurmentsByIMEI(@WebParam(name = "IMEI") String IMEI) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("WeightMeasurments.findByIMEI");

        Map<String, Object> deviceParams = new HashMap<String, Object>();
        deviceParams.put("FindIMEI", IMEI);
        Set<Map.Entry<String, Object>> rawParameters = deviceParams.entrySet();
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
    
    @WebMethod(operationName = "SelectWakdStatusByIMEI")
    public List<WAKDStatus> SelectWakdStatusByIMEI(@WebParam(name = "IMEI")String IMEI) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("WAKDStatus.findByIMEI");

        Map<String, Object> deviceParams = new HashMap<String, Object>();
        deviceParams.put("FindIMEI", IMEI);
        Set<Map.Entry<String, Object>> rawParameters = deviceParams.entrySet();
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
    
    @WebMethod(operationName = "SelectMeasurmentsByIMEI")
    public List<WAKDMeasurments> SelectMeasurmentsByIMEI(@WebParam(name = "IMEI")String IMEI) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("WAKDMeasurments.findByIMEI");

        Map<String, Object> deviceParams = new HashMap<String, Object>();
        deviceParams.put("FindIMEI", IMEI);
        Set<Map.Entry<String, Object>> rawParameters = deviceParams.entrySet();
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
    
    @WebMethod(operationName = "SelectAlertsByIMEI")
    public List<WAKDAlerts> SelectAlertsByIMEI(@WebParam(name = "IMEI")String IMEI) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("WAKDAlerts.findByIMEI");

        Map<String, Object> deviceParams = new HashMap<String, Object>();
        deviceParams.put("FindIMEI", IMEI);
        Set<Map.Entry<String, Object>> rawParameters = deviceParams.entrySet();
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
    
    @WebMethod(operationName = "SelectQuastionaryByIMEI")
    public List<QuestionaryTable> SelectQuastionaryByIMEI(@WebParam(name = "IMEI")String IMEI) {
        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();
        Query query = em.createNamedQuery("QuestionaryTable.findByIMEI");

        Map<String, Object> deviceParams = new HashMap<String, Object>();
        deviceParams.put("FindIMEI", IMEI);
        Set<Map.Entry<String, Object>> rawParameters = deviceParams.entrySet();
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }
}
