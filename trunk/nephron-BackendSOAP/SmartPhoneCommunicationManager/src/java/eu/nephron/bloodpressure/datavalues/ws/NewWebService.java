/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.datavalues.ws;

import eu.nephron.WeightDataValues.db.weightdatavalues;
import eu.nephron.bloodpressure.datavalues.bean.bloodpressurevaluesFacadeLocal;
import eu.nephron.bloodpressure.datavalues.db.bloodpressurevalues;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "bloodpressurevaluesws")
public class NewWebService {
    @EJB
    private bloodpressurevaluesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

     @PersistenceUnit
    private EntityManagerFactory emf;
    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEI")
    public List<bloodpressurevalues> findWAKDMeasurmentsManagerWithIMEI(@WebParam(name = "IMEI") String IMEI) {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();


        List<bloodpressurevalues> wakd = em.createQuery("select a from bloodpressurevalues a where a.imei=:IMEI").setParameter("IMEI", IMEI).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEIAndDateFrom")
    public List<bloodpressurevalues> findWAKDMeasurmentsManagerWithIMEIAndDateFrom(@WebParam(name = "IMEI") String IMEI, @WebParam(name = "DATEFROM") Long DateFrom) {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();


        List<bloodpressurevalues> wakd = em.createQuery("select a from bloodpressurevalues a where a.imei=:IMEI  AND a.bptime>:DateFrom").setParameter("IMEI", IMEI).setParameter("DateFrom", DateFrom).getResultList();
        return wakd;
    }
    
    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "bloodpressurevalues") bloodpressurevalues bloodpressurevalues) {
        ejbRef.create(bloodpressurevalues);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "bloodpressurevalues") bloodpressurevalues bloodpressurevalues) {
        ejbRef.edit(bloodpressurevalues);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "bloodpressurevalues") bloodpressurevalues bloodpressurevalues) {
        ejbRef.remove(bloodpressurevalues);
    }

    @WebMethod(operationName = "find")
    public bloodpressurevalues find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<bloodpressurevalues> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<bloodpressurevalues> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
