/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.bean;

import eu.nephron.bloodpressure.bloodpressure;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface bloodpressureFacadeLocal {

    void create(bloodpressure bloodpressure);

    void edit(bloodpressure bloodpressure);

    void remove(bloodpressure bloodpressure);

    bloodpressure find(Object id);

    List<bloodpressure> findAll();

    List<bloodpressure> findRange(int[] range);

    int count();
    
}
