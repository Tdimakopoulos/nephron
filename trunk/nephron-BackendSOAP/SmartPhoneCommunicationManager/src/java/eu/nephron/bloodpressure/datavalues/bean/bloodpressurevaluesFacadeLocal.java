/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.datavalues.bean;

import eu.nephron.bloodpressure.datavalues.db.bloodpressurevalues;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface bloodpressurevaluesFacadeLocal {

    void create(bloodpressurevalues bloodpressurevalues);

    void edit(bloodpressurevalues bloodpressurevalues);

    void remove(bloodpressurevalues bloodpressurevalues);

    bloodpressurevalues find(Object id);

    List<bloodpressurevalues> findAll();

    List<bloodpressurevalues> findRange(int[] range);

    int count();
    
}
