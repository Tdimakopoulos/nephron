/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.bean;

import eu.nephron.bloodpressure.bloodpressure;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class bloodpressureFacade extends AbstractFacade<bloodpressure> implements bloodpressureFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public bloodpressureFacade() {
        super(bloodpressure.class);
    }
    
}
