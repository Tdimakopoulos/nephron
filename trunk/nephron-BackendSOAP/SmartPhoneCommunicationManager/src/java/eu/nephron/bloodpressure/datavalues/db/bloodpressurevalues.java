/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.datavalues.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class bloodpressurevalues implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

     
    private String imei;
    private String systolic;
    private String diastolic;
    private String heartrate;
    private Long bptime;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof bloodpressurevalues)) {
            return false;
        }
        bloodpressurevalues other = (bloodpressurevalues) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.bloodpressure.datavalues.db.bloodpressurevalues[ id=" + id + " ]";
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the systolic
     */
    public String getSystolic() {
        return systolic;
    }

    /**
     * @param systolic the systolic to set
     */
    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    /**
     * @return the diastolic
     */
    public String getDiastolic() {
        return diastolic;
    }

    /**
     * @param diastolic the diastolic to set
     */
    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }

    /**
     * @return the heartrate
     */
    public String getHeartrate() {
        return heartrate;
    }

    /**
     * @param heartrate the heartrate to set
     */
    public void setHeartrate(String heartrate) {
        this.heartrate = heartrate;
    }

    /**
     * @return the bptime
     */
    public Long getBptime() {
        return bptime;
    }

    /**
     * @param bptime the bptime to set
     */
    public void setBptime(Long bptime) {
        this.bptime = bptime;
    }
    
}
