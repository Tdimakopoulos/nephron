/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.ws;

import eu.nephron.WeightDataValues.db.weightdatavalues;
import eu.nephron.bloodpressure.bean.bloodpressureFacadeLocal;
import eu.nephron.bloodpressure.bloodpressure;
import eu.nephron.bloodpressure.datavalues.bean.bloodpressurevaluesFacadeLocal;
import eu.nephron.bloodpressure.datavalues.db.bloodpressurevalues;
import eu.nephron.secure.DB.WeightMeasurments;
import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.measurments.BloodPressureIncomingAPI;
import eu.nephron.wakd.measurments.WeightIncomingAPI;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "BloodPressureWS")
public class BloodPressureWS {
    @EJB
    private bloodpressureFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @EJB
    private bloodpressurevaluesFacadeLocal bpejbRef;
    
    @WebMethod(operationName = "BloodPressureNewValueReceived")
    public String WeightManagerNewValueReceived(
             @WebParam(name = "imei")String imei,
     @WebParam(name = "date")String dated,
     @WebParam(name = "reply")String reply) {
        bloodpressure pvarweightMeasurments= new bloodpressure();
        pvarweightMeasurments.setDated(dated);
        pvarweightMeasurments.setImei(imei);
        pvarweightMeasurments.setReply(reply);
        ejbRef.create(pvarweightMeasurments);
        
        byte[] pmsg = ByteUtils.convertStringToByteArray(reply);
        BloodPressureIncomingAPI pBPAPI= new BloodPressureIncomingAPI(); 
//        WeightIncomingAPI pWIAPI = new WeightIncomingAPI();
        pBPAPI.SetMSG(pmsg, true);
        pBPAPI.decode();
        if(pBPAPI.IsBloodPressure())
        {
        bloodpressurevalues entity = new bloodpressurevalues();
        entity.setBptime(new Date().getTime());
        entity.setDiastolic(pBPAPI.getDiastolic_s());
        entity.setHeartrate(pBPAPI.getHeartrate_s());
        entity.setImei(imei);
        entity.setSystolic(pBPAPI.getSystolic_s());
        bpejbRef.create(entity);
        }

        return "ok";
    }
    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "bloodpressure") bloodpressure bloodpressure) {
        ejbRef.create(bloodpressure);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "bloodpressure") bloodpressure bloodpressure) {
        ejbRef.edit(bloodpressure);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "bloodpressure") bloodpressure bloodpressure) {
        ejbRef.remove(bloodpressure);
    }

    @WebMethod(operationName = "find")
    public bloodpressure find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<bloodpressure> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<bloodpressure> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
