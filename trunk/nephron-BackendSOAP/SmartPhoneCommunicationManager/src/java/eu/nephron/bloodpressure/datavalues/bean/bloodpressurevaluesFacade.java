/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.bloodpressure.datavalues.bean;

import eu.nephron.bloodpressure.datavalues.db.bloodpressurevalues;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class bloodpressurevaluesFacade extends AbstractFacade<bloodpressurevalues> implements bloodpressurevaluesFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public bloodpressurevaluesFacade() {
        super(bloodpressurevalues.class);
    }
    
}
