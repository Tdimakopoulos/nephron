/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.settings;

import eu.nephron.secure.DB.SettingsTable;
import eu.nephron.secure.beans.AbstractFacade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class SettingsTableFacade extends AbstractFacade<SettingsTable> implements SettingsTableFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SettingsTableFacade() {
        super(SettingsTable.class);
    }
    
}
