/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.settings;

import eu.nephron.secure.DB.SettingsTable;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "WAKDSettingsManager")
public class WAKDSettingsManager {
    @EJB
    private SettingsTableFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "WAKDSettingsManagercreate")
    @Oneway
    public void WAKDSettingsManagercreate(@WebParam(name = "settingsTable") SettingsTable settingsTable) {
        ejbRef.create(settingsTable);
    }

    @WebMethod(operationName = "WAKDSettingsManageredit")
    @Oneway
    public void WAKDSettingsManageredit(@WebParam(name = "settingsTable") SettingsTable settingsTable) {
        ejbRef.edit(settingsTable);
    }

    @WebMethod(operationName = "WAKDSettingsManagerremove")
    @Oneway
    public void WAKDSettingsManagerremove(@WebParam(name = "settingsTable") SettingsTable settingsTable) {
        ejbRef.remove(settingsTable);
    }

    @WebMethod(operationName = "WAKDSettingsManagerfind")
    public SettingsTable WAKDSettingsManagerfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "WAKDSettingsManagerfindAll")
    public List<SettingsTable> WAKDSettingsManagerfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "WAKDSettingsManagerfindRange")
    public List<SettingsTable> WAKDSettingsManagerfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "WAKDSettingsManagercount")
    public int WAKDSettingsManagercount() {
        return ejbRef.count();
    }
    
}
