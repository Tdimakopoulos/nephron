/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.wakd.settings;

import eu.nephron.secure.DB.SettingsTable;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface SettingsTableFacadeLocal {

    void create(SettingsTable settingsTable);

    void edit(SettingsTable settingsTable);

    void remove(SettingsTable settingsTable);

    SettingsTable find(Object id);

    List<SettingsTable> findAll();

    List<SettingsTable> findRange(int[] range);

    int count();
    
}
