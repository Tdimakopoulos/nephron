/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.WeightDataValues.ws;

import eu.nephron.WeightDataValues.bean.weightdatavaluesFacadeLocal;
import eu.nephron.WeightDataValues.db.weightdatavalues;
import eu.nephron.secure.DB.WAKDMeasurments;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "WeightDataValuesWS")
public class WeightDataValuesWS {

    @EJB
    private weightdatavaluesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")
    @PersistenceUnit
    private EntityManagerFactory emf;
    @Resource
    private UserTransaction utx;

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEI")
    public List<weightdatavalues> findWAKDMeasurmentsManagerWithIMEI(@WebParam(name = "IMEI") String IMEI) {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();


        List<weightdatavalues> wakd = em.createQuery("select a from weightdatavalues a where a.imei=:IMEI").setParameter("IMEI", IMEI).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "findWAKDMeasurmentsManagerWithIMEIAndDateFrom")
    public List<weightdatavalues> findWAKDMeasurmentsManagerWithIMEIAndDateFrom(@WebParam(name = "IMEI") String IMEI, @WebParam(name = "DATEFROM") Long DateFrom) {



        assert emf != null;  //Make sure injection went through correctly.
        EntityManager em = null;
        em = emf.createEntityManager();


        List<weightdatavalues> wakd = em.createQuery("select a from weightdatavalues a where a.imei=:IMEI  AND a.wtime>:DateFrom").setParameter("IMEI", IMEI).setParameter("DateFrom", DateFrom).getResultList();
        return wakd;
    }

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "weightdatavalues") weightdatavalues weightdatavalues) {
        ejbRef.create(weightdatavalues);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "weightdatavalues") weightdatavalues weightdatavalues) {
        ejbRef.edit(weightdatavalues);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "weightdatavalues") weightdatavalues weightdatavalues) {
        ejbRef.remove(weightdatavalues);
    }

    @WebMethod(operationName = "find")
    public weightdatavalues find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<weightdatavalues> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<weightdatavalues> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
}
