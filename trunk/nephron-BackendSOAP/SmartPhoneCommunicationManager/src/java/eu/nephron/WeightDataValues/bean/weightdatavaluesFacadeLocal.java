/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.WeightDataValues.bean;

import eu.nephron.WeightDataValues.db.weightdatavalues;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface weightdatavaluesFacadeLocal {

    void create(weightdatavalues weightdatavalues);

    void edit(weightdatavalues weightdatavalues);

    void remove(weightdatavalues weightdatavalues);

    weightdatavalues find(Object id);

    List<weightdatavalues> findAll();

    List<weightdatavalues> findRange(int[] range);

    int count();
    
}
