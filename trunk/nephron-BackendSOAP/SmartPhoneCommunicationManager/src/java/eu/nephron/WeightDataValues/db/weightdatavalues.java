/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.WeightDataValues.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class weightdatavalues implements Serializable {
    private static long serialVersionUID = 1L;

    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String weight;
    private String imei;
    private String bodyfat;
    private String wsbattery;
    private String tbd;
    private Long wtime;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof weightdatavalues)) {
            return false;
        }
        weightdatavalues other = (weightdatavalues) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.WeightDataValues.db.weightdatavalues[ id=" + id + " ]";
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the bodyfat
     */
    public String getBodyfat() {
        return bodyfat;
    }

    /**
     * @param bodyfat the bodyfat to set
     */
    public void setBodyfat(String bodyfat) {
        this.bodyfat = bodyfat;
    }

    /**
     * @return the wsbattery
     */
    public String getWsbattery() {
        return wsbattery;
    }

    /**
     * @param wsbattery the wsbattery to set
     */
    public void setWsbattery(String wsbattery) {
        this.wsbattery = wsbattery;
    }

    /**
     * @return the tbd
     */
    public String getTbd() {
        return tbd;
    }

    /**
     * @param tbd the tbd to set
     */
    public void setTbd(String tbd) {
        this.tbd = tbd;
    }

    /**
     * @return the wtime
     */
    public Long getWtime() {
        return wtime;
    }

    /**
     * @param wtime the wtime to set
     */
    public void setWtime(Long wtime) {
        this.wtime = wtime;
    }
    
}
