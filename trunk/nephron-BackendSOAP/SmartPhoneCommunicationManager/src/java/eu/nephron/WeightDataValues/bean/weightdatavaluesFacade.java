/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.WeightDataValues.bean;

import eu.nephron.WeightDataValues.db.weightdatavalues;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class weightdatavaluesFacade extends AbstractFacade<weightdatavalues> implements weightdatavaluesFacadeLocal {
    @PersistenceContext(unitName = "SmartPhoneCommunicationManagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public weightdatavaluesFacade() {
        super(weightdatavalues.class);
    }
    
}
