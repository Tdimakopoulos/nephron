/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.testcase;

import eu.nephron.Singleton.ClassicSingleton;
import eu.nephron.failsafe.FailSafe;

/**
 *
 * @author tdim
 */
public class SingletonTest {
    private ClassicSingleton sone = null, stwo = null;
    static FailSafe pff=new FailSafe();
    
    public static void main(String[] args) {
        SingletonTest pTest=new SingletonTest();
        System.out.println("stack size : "+pff.GetStactSize());
        for(int i=0;i<pff.GetStactSize();i++)
        System.out.println("Class  : "+pff.GetCurrentMethodInPosition(i).toString());
        pTest.setUp();
        }
    
     public void setUp() {
      System.out.println("getting singleton...");
      sone = ClassicSingleton.getInstance();
      System.out.println("stack size : "+pff.GetStactSize());
        for(int i=0;i<pff.GetStactSize();i++)
        System.out.println("Class  : "+pff.GetCurrentMethodInPosition(i).toString());
      System.out.println("...got singleton: " + sone);
      System.out.println("getting singleton...");
      stwo = ClassicSingleton.getInstance();
      System.out.println("stack size : "+pff.GetStactSize());
        for(int i=0;i<pff.GetStactSize();i++)
        System.out.println("Class  : "+pff.GetCurrentMethodInPosition(i).toString());
      System.out.println("...got singleton: " + stwo);
      if(sone==stwo)
          System.out.println("Singleton Same");
      else
          System.out.println("Singleton Not Same");
   }
}
