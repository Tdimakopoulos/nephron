/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.Singleton.registry;

import eu.nephron.registry.SingletonRegistry;
import java.util.HashMap;

public class Singleton {
   protected Singleton() {
      // Exists only to thwart instantiation.
   }
   public static Singleton getInstance(String Classname) {
      return (Singleton)SingletonRegistry.getInstance(Classname);
   }
    private static Class getClass(String classname) 
                                         throws ClassNotFoundException {
      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
      if(classLoader == null)
         classLoader = Singleton.class.getClassLoader();
      return (classLoader.loadClass(classname));
   }
}
