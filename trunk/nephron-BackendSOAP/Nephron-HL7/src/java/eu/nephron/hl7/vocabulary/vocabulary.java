/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.hl7.vocabulary;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class vocabulary implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String hl7Group;
    private String hl7Name;
    private String hl7Value;
    private String hl7Description;
    private String hl7Type;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof vocabulary)) {
            return false;
        }
        vocabulary other = (vocabulary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.hl7.vocabulary.vocabulary[ id=" + id + " ]";
    }

    /**
     * @return the hl7Group
     */
    public String getHl7Group() {
        return hl7Group;
    }

    /**
     * @param hl7Group the hl7Group to set
     */
    public void setHl7Group(String hl7Group) {
        this.hl7Group = hl7Group;
    }

    /**
     * @return the hl7Name
     */
    public String getHl7Name() {
        return hl7Name;
    }

    /**
     * @param hl7Name the hl7Name to set
     */
    public void setHl7Name(String hl7Name) {
        this.hl7Name = hl7Name;
    }

    /**
     * @return the hl7Value
     */
    public String getHl7Value() {
        return hl7Value;
    }

    /**
     * @param hl7Value the hl7Value to set
     */
    public void setHl7Value(String hl7Value) {
        this.hl7Value = hl7Value;
    }

    /**
     * @return the hl7Description
     */
    public String getHl7Description() {
        return hl7Description;
    }

    /**
     * @param hl7Description the hl7Description to set
     */
    public void setHl7Description(String hl7Description) {
        this.hl7Description = hl7Description;
    }

    /**
     * @return the hl7Type
     */
    public String getHl7Type() {
        return hl7Type;
    }

    /**
     * @param hl7Type the hl7Type to set
     */
    public void setHl7Type(String hl7Type) {
        this.hl7Type = hl7Type;
    }
    
}
