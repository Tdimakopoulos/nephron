/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.hl7.vocabulary.beans;

import eu.nephron.hl7.vocabulary.vocabulary;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class vocabularyFacade extends AbstractFacade<vocabulary> {
    @PersistenceContext(unitName = "Nephron-HL7PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public vocabularyFacade() {
        super(vocabulary.class);
    }
    
}
