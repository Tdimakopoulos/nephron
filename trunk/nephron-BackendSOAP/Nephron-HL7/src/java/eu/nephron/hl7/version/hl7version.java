/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.hl7.version;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 *
 * @author tdim
 */
@Entity
public class hl7version implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long hl7Version;
    private Long NephronVersion;
    private Long PersonalVersion;
    private String description;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof hl7version)) {
            return false;
        }
        hl7version other = (hl7version) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.hl7.version.hl7version[ id=" + id + " ]";
    }

    /**
     * @return the hl7Version
     */
    public Long getHl7Version() {
        return hl7Version;
    }

    /**
     * @param hl7Version the hl7Version to set
     */
    public void setHl7Version(Long hl7Version) {
        this.hl7Version = hl7Version;
    }

    /**
     * @return the NephronVersion
     */
    public Long getNephronVersion() {
        return NephronVersion;
    }

    /**
     * @param NephronVersion the NephronVersion to set
     */
    public void setNephronVersion(Long NephronVersion) {
        this.NephronVersion = NephronVersion;
    }

    /**
     * @return the PersonalVersion
     */
    public Long getPersonalVersion() {
        return PersonalVersion;
    }

    /**
     * @param PersonalVersion the PersonalVersion to set
     */
    public void setPersonalVersion(Long PersonalVersion) {
        this.PersonalVersion = PersonalVersion;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
}
