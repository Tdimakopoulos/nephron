/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.hl7.vocabulary.ws;

import eu.nephron.hl7.vocabulary.beans.vocabularyFacade;
import eu.nephron.hl7.vocabulary.vocabulary;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "HL7Vocabulary")
public class HL7Vocabulary {
    @EJB
    private vocabularyFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "createHL7Vocabulary")
    @Oneway
    public void createHL7Vocabulary(@WebParam(name = "entity") vocabulary entity) {
        ejbRef.create(entity);
    }

    @WebMethod(operationName = "editHL7Vocabulary")
    @Oneway
    public void editHL7Vocabulary(@WebParam(name = "entity") vocabulary entity) {
        ejbRef.edit(entity);
    }

    @WebMethod(operationName = "removeHL7Vocabulary")
    @Oneway
    public void removeHL7Vocabulary(@WebParam(name = "entity") vocabulary entity) {
        ejbRef.remove(entity);
    }

    @WebMethod(operationName = "findHL7Vocabulary")
    public vocabulary findHL7Vocabulary(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAllHL7Vocabulary")
    public List<vocabulary> findAllHL7Vocabulary() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRangeHL7Vocabulary")
    public List<vocabulary> findRangeHL7Vocabulary(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "countHL7Vocabulary")
    public int countHL7Vocabulary() {
        return ejbRef.count();
    }
    
}
