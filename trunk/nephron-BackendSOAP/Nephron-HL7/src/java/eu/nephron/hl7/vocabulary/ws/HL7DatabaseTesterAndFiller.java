/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.hl7.vocabulary.ws;

import eu.nephron.hl7.vocabulary.beans.vocabularyFacade;
import eu.nephron.hl7.vocabulary.vocabulary;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "HL7DatabaseTesterAndFiller")
public class HL7DatabaseTesterAndFiller {

    @EJB
    private vocabularyFacade ejbRef;
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "CreateBasicVocabulary")
    public String CreateBasicVocabulary() {
        
        CreateVoc("Doctor Role","Role","Doctor","STR","DOC");
        CreateVoc("Nurse Role","Role","Nurse","STR","NUR");
        CreateVoc("Patient Role","Role","Patient","STR","PAT");
        CreateVoc("WAKD Role","Role","WAKD","STR","WAKD");
        CreateVoc("SmartPhone Role","Role","SmartPhone","STR","SP");
        
        CreateVoc("ACT Status Text","ACTSTATUS","PENDING","ENUM","1");
        CreateVoc("ACT Status Text","ACTSTATUS","SUCCESSFUL","ENUM","2");
        CreateVoc("ACT Status Text","ACTSTATUS","FAILED","ENUM","3");
        
        CreateVoc("MoodCode Text","MOODCODE","DEFINITION","ENUM","1");
        CreateVoc("MoodCode Text","MOODCODE","INTENT","ENUM","2");
        CreateVoc("MoodCode Text","MOODCODE","REQUEST","ENUM","3");
        CreateVoc("MoodCode Text","MOODCODE","EVENT","ENUM","4");
        CreateVoc("MoodCode Text","MOODCODE","CRITERION","ENUM","5");
        CreateVoc("MoodCode Text","MOODCODE","GOAL","ENUM","6");
        
        
        CreateVoc("Education Level Text","EDULEVEL","ELEMENTARY","ENUM","1");
        CreateVoc("Education Level Text","EDULEVEL","HIGH_SCOOL","ENUM","2");
        CreateVoc("Education Level Text","EDULEVEL","COLLEGE","ENUM","3");
        CreateVoc("Education Level Text","EDULEVEL","UNIVERSITY","ENUM","4");
        CreateVoc("Education Level Text","EDULEVEL","MASTER","ENUM","5");
        CreateVoc("Education Level Text","EDULEVEL","PHD","ENUM","6");
        CreateVoc("Education Level Text","EDULEVEL","UNDEFINED","ENUM","7");
        
        
        CreateVoc("Gender Text","GENDER","MALE","ENUM","1");
        CreateVoc("Gender Text","GENDER","FEMALE","ENUM","2");
        CreateVoc("Gender Text","GENDER","HERMAPHRODITE","ENUM","3");
        CreateVoc("Education Level Text","EDULEVEL","UNDEFINED","ENUM","4");
        
        CreateVoc("Marital Status Text","MARITALSTATUS","SINGLE","ENUM","1");
        CreateVoc("Marital Status Text","MARITALSTATUS","MARRIED","ENUM","2");
        CreateVoc("Marital Status Text","MARITALSTATUS","DIVORCED","ENUM","3");
        CreateVoc("Marital Status Text","MARITALSTATUS","UNDEFINED","ENUM","4");
        
        
        CreateVoc("Sensor Type","SENSORTYPE","Inlet PH","ENUM","1");
CreateVoc("Sensor Type","SENSORTYPE","Inlet Potasium","ENUM","2");
CreateVoc("Sensor Type","SENSORTYPE","Inlet Sodium","ENUM","3");
CreateVoc("Sensor Type","SENSORTYPE","Inlet Temperature","ENUM","4");
CreateVoc("Sensor Type","SENSORTYPE","Inlet Uria","ENUM","5");

CreateVoc("Sensor Type","SENSORTYPE","Outlet PH","ENUM","6");
CreateVoc("Sensor Type","SENSORTYPE","Outlet Potasium","ENUM","7");
CreateVoc("Sensor Type","SENSORTYPE","Outlet Sodium","ENUM","8");
CreateVoc("Sensor Type","SENSORTYPE","Outlet Temperature","ENUM","9");
CreateVoc("Sensor Type","SENSORTYPE","Outlet Uria","ENUM","10");

        
        return "Created : Role + ACTStatus + MoodCode + Sensors" ;
    }
    
    
    private void CreateVoc(String szdesc,String szgroup,String szname,String sztype,String szvalue)
    {
        vocabulary entity= new vocabulary();
        entity.setHl7Description(szdesc);
        entity.setHl7Group(szgroup);
        entity.setHl7Name(szname);
        entity.setHl7Type(sztype);
        entity.setHl7Value(szvalue);
        ejbRef.create(entity);
    }
}
