package eu.nephron.restful.resource;

import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.jboss.resteasy.util.Hex;

import eu.nephron.controller.ServiceLocator;
import eu.nephron.controller.entity.MedicalEntityService;
import eu.nephron.wakd.api.incoming.AlertMsg;

@Path("/alert")
public class AlertResource {
	
	@POST
	@Path("/create")
	public void update(
			@QueryParam("deviceID") Long deviceID,
			@QueryParam("message") String hexStr,
			@QueryParam("userID") Long userID) {
		
		AlertMsg msg = new AlertMsg();
		msg.decode(Hex.decodeHex(hexStr));
		this.getMedicalEntityService().createAlert(deviceID, msg.getAlert(), userID);
		System.out.println("ALERT DETECTED: " + msg.getAlert());
	}
	
	private MedicalEntityService getMedicalEntityService() {
		try {
			return ServiceLocator.getResource("nephron/MedicalEntityServiceImpl/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
