package eu.nephron.restful.resource;

import javax.naming.NamingException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.jboss.resteasy.util.Hex;

import eu.nephron.controller.ServiceLocator;
import eu.nephron.controller.entity.MedicalEntityService;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;

@Path("/state")
public class StateResource {
	
	@POST
	@Path("/create")
	public void update(
			@QueryParam("deviceID") Long deviceID,
			@QueryParam("message") String hexStr,
			@QueryParam("userID") Long userID) {
		
		CurrentStateMsg msg = new CurrentStateMsg();
		msg.decode(Hex.decodeHex(hexStr));
		this.getMedicalEntityService().createState(deviceID, msg.getCurrentState(), msg.getCurrentOpState(), userID);
		System.out.println("CURRENT STATE: " + msg.getCurrentState() + ", " + msg.getCurrentOpState());
	}
	
	private MedicalEntityService getMedicalEntityService() {
		try {
			return ServiceLocator.getResource("nephron/MedicalEntityServiceImpl/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
