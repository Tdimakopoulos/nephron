package eu.nephron.controller.test.role;

import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;

public class RoleServiceTest extends ControllerServiceTest {
	
	@Test(groups="createDB")
	public void testCreateRoles() {
		roleService.createDoctor(this.userID);
		roleService.createPatient(this.userID);
		roleService.createWakd(this.userID);
		roleService.createSmartPhone(this.userID);
	}
		
}
