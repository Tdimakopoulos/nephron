package eu.nephron.controller.test.address;

import org.testng.annotations.Test;

import eu.nephron.controller.test.ControllerServiceTest;

public class AddressServiceTest extends ControllerServiceTest {

	@Test(groups="createDB")
	public void testCreateCountries() {
		addressService.createCountry("GR", "Greece", this.userID);
		addressService.createCountry("AL", "Albania", this.userID);
	}

}
