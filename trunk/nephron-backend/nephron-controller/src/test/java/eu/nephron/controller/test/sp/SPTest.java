package eu.nephron.controller.test.sp;

import org.apache.commons.codec.binary.Hex;
import org.testng.annotations.Test;

import eu.nephron.controller.act.CommandRemoteService;
import eu.nephron.controller.entity.MedicalEntityRemoteService;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;
import eu.nephron.model.entity.WakdState;
import eu.nephron.test.ResourceLocator;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;
import eu.nephron.wakd.api.outgoing.ChangeStateMsg;

public class SPTest {

	@Test(groups="createCommand")
	public void testCreateShutdownCommand() {
		
		MedicalEntityRemoteService medicalEntityService = ResourceLocator.lookup("nephron/MedicalEntityServiceImpl/remote");
		CommandRemoteService commandService= ResourceLocator.lookup("nephron/CommandServiceImpl/remote");
				
		Device device = medicalEntityService.findSmartPhoneBySerialNumber("SP1");
		Person person = medicalEntityService.findDoctorByLastName("Mein");
		WakdState currentState = medicalEntityService.findCurrentState(device.getId(), 1L);
		
		ChangeStateMsg msg = new ChangeStateMsg(
				1,
				WakdStateEnum.getWakdStateEnum((byte)currentState.getState().intValue()),
				WakdOpStateEnum.getWakdOpStateEnum((byte)currentState.getOpState().intValue()),
				WakdStateEnum.ALL_STOPPED,
				WakdOpStateEnum.AUTOMATIC);

		commandService.createCommand(msg, person.getId(), device.getId(), 1L);
		System.out.println(Hex.encodeHexString(msg.encode()));
	}
}
