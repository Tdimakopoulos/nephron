package eu.nephron.controller.entity.bean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.nephron.controller.ActionAuditInterceptor;
import eu.nephron.controller.entity.MedicalEntityRemoteService;
import eu.nephron.controller.entity.MedicalEntityService;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.controller.user.RoleService;
import eu.nephron.model.alert.Alert;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;
import eu.nephron.model.entity.WakdState;
import eu.nephron.wakd.api.enums.WakdAlertEnum;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;

@SuppressWarnings("unchecked")
@Stateless
public class MedicalEntityServiceImpl implements MedicalEntityService, MedicalEntityRemoteService {

	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@EJB
	private RoleService roleService;
	
	@Override
	public WakdState findCurrentState(Long deviceID, Long userID) {
		
		Map<String, Object> deviceParams = new HashMap<String, Object>();
		deviceParams.put("deviceID", deviceID);
		Device device = (Device) crudService.findSingleWithNamedQuery("Device.findAssociatedDevice", deviceParams);
		Map<String, Object> stateParams = new HashMap<String, Object>();
		stateParams.put("deviceID", device.getId());
		return (WakdState) crudService.findSingleWithNamedQuery("WakdState.findWakdStateByMaxDate", stateParams);
	}
	
	@Override
	public Person findPatientByLastName(String lastName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("lastName", lastName);
		return (Person) crudService.findSingleWithNamedQuery("Person.findPatientByLastName", params);
	}
	
	@Override
	public Person findDoctorByLastName(String lastName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("lastName", lastName);
		return (Person) crudService.findSingleWithNamedQuery("Person.findDoctorByLastName", params);
	}
	
	@Override
	public Device findWakdBySerialNumber(String serialNumber) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serialNumber", serialNumber);
		return (Device) crudService.findSingleWithNamedQuery("Device.findWakdBySerialNumber", params);
	}
	
	@Override
	public Device findSmartPhoneBySerialNumber(String serialNumber) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serialNumber", serialNumber);
		return (Device) crudService.findSingleWithNamedQuery("Device.findSmartPhoneBySerialNumber", params);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createPatient(Person patient, Long userID) {
		patient.setCode("P");
		patient.setName("Patient");
		patient.setEffectiveDate(new Date());
		patient.setRole(this.roleService.findRoleByCode("PTNT"));
		this.crudService.create(patient);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createDoctor(Person doctor, Long userID) {
		doctor.setCode("D");
		doctor.setName("Doctor");
		doctor.setEffectiveDate(new Date());
		doctor.setRole(this.roleService.findRoleByCode("DOC"));
		this.crudService.create(doctor);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createWakd(Device wakd, Long userID) {
		wakd.setCode("WAKD");
		wakd.setName("WAKD");
		wakd.setEffectiveDate(new Date());
		wakd.setRole(this.roleService.findRoleByCode("WAKD"));
		this.crudService.create(wakd);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createSmartPhone(Device smartphone, Long userID) {
		smartphone.setCode("SP");
		smartphone.setName("SP");
		smartphone.setEffectiveDate(new Date());
		smartphone.setRole(this.roleService.findRoleByCode("SP"));
		this.crudService.create(smartphone);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createAlert(Long deviceID, WakdAlertEnum wakdAlert, Long userID) {
		Map<String, Object> deviceParams = new HashMap<String, Object>();
		deviceParams.put("deviceID", deviceID);
		Device device = (Device) crudService.findSingleWithNamedQuery("Device.findAssociatedDevice", deviceParams);
		Alert alert = new Alert();
		alert.setDevice(device);
		alert.setDescription(wakdAlert.toString());
		crudService.create(alert);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createState(Long deviceID, WakdStateEnum wakdState, WakdOpStateEnum wakdOpState, Long userID) {
		Map<String, Object> deviceParams = new HashMap<String, Object>();
		deviceParams.put("deviceID", deviceID);
		Device device = (Device) crudService.findSingleWithNamedQuery("Device.findAssociatedDevice", deviceParams);
		WakdState currentState = new WakdState();
		currentState.setDate(new Date());
		currentState.setDevice(device);
		currentState.setState(new Integer(wakdState.getIdentifier()));
		currentState.setOpState(new Integer(wakdOpState.getIdentifier()));
		Map<String, Object> stateParams = new HashMap<String, Object>();
		stateParams.put("deviceID", device.getId());
		WakdState parentState = (WakdState) crudService.findSingleWithNamedQuery("WakdState.findWakdStateByMaxDate", stateParams);
		currentState.setParent(parentState);
		crudService.create(currentState);
	}

}
