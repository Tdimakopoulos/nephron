package eu.nephron.controller.address;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;
import eu.nephron.model.address.Country;

@Remote
public interface AddressRemoteService extends ControllerService {

	public void createCountry(String code, String name, Long userID);
	
	public Country findCountryByCode(String code);
	
	public Country findCountryByName(String name);
	
}
