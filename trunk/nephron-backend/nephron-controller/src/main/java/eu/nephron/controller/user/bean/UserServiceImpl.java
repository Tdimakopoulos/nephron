package eu.nephron.controller.user.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import eu.nephron.controller.persistence.CrudService;
import eu.nephron.controller.user.UserService;
import eu.nephron.controller.user.UserRemoteService;
import eu.nephron.model.user.NephronUser;

@Stateless
public class UserServiceImpl implements UserService, UserRemoteService {

	@EJB
	private CrudService<NephronUser> crudService;
	
	@Override
	public NephronUser findUserByUserID(Long userID) {
		return crudService.find(NephronUser.class, userID);
	}
	
	@Override
	public NephronUser findUserByUserName(String userName) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userName", userName);
		return (NephronUser) crudService.findSingleWithNamedQuery("NephronUser.findByUserName", params);
	}
	
	@Override
	public NephronUser findUserByUserNameAndPassword(String userName, String password) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userName", userName);
		params.put("password", password);
		return (NephronUser) crudService.findSingleWithNamedQuery("NephronUser.findByUserNameAndPassword", params);
	}

	@Override
	public void createUser(NephronUser user, Long userID) {
		crudService.create(user);
	}

}
