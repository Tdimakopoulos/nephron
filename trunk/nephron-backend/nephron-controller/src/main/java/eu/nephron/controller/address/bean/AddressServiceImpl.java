package eu.nephron.controller.address.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.nephron.controller.ActionAuditInterceptor;
import eu.nephron.controller.address.AddressService;
import eu.nephron.controller.address.AddressRemoteService;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.model.address.Country;

@SuppressWarnings("unchecked")
@Stateless
public class AddressServiceImpl implements AddressService, AddressRemoteService {

	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createCountry(String code, String name, Long userID) {
		Country country = new Country();
		country.setCode(code);
		country.setName(name);
		crudService.create(country);
	}

	@Override
	public Country findCountryByCode(String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", code);
		return (Country) crudService.findSingleWithNamedQuery("Country.findByCode", params);
	
	}
	
	@Override
	public Country findCountryByName(String name) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		return (Country) crudService.findSingleWithNamedQuery("Country.findByName", params);
	}
	
}
