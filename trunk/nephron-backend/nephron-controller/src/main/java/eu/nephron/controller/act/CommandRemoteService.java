package eu.nephron.controller.act;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;
import eu.nephron.model.act.ActRelationship;
import eu.nephron.wakd.api.IncomingWakdMsg;
import eu.nephron.wakd.api.OutgoingWakdMsg;

@Remote
public interface CommandRemoteService extends ControllerService {

	public ActRelationship readCommand(Long deviceID, Long userID);

	public void createCommand(OutgoingWakdMsg message, Long personID, Long deviceID, Long userID);
	
	public void updateCommand(IncomingWakdMsg message, Long commandID, Integer statusID, Long userID);
	
	public void updateCommandStatus(Long commandID, Integer statusID, Long userID);

}
