package eu.nephron.controller.act;

import java.util.List;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;

@Remote
public interface ActRemoteService extends ControllerService {

	public void createAssociationAct(List<Long> medicalEntityIDs, Long userID);
	
	public void updateAssociationAct(Long actID, List<Long> medicalEntityIDs, Long userID);
}
