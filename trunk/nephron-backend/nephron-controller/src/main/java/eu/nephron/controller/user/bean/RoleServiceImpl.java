package eu.nephron.controller.user.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.nephron.controller.ActionAuditInterceptor;
import eu.nephron.controller.persistence.CrudService;
import eu.nephron.controller.user.RoleRemoteService;
import eu.nephron.controller.user.RoleService;
import eu.nephron.model.role.Doctor;
import eu.nephron.model.role.Patient;
import eu.nephron.model.role.Role;
import eu.nephron.model.role.SmartPhone;
import eu.nephron.model.role.WAKD;

@SuppressWarnings("unchecked")
@Stateless
public class RoleServiceImpl implements RoleService, RoleRemoteService {

	@SuppressWarnings("rawtypes")
	@EJB
	private CrudService crudService;
	
	@Override
	public List<Role> findAllRoles() {
		return crudService.findWithNamedQuery("Role.findAllRoles");
	}
	
	@Override
	public Role findRoleByCode(String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", code);
		return (Role) crudService.findSingleWithNamedQuery("Role.findByCode", params);
	
	}
	
	@Override
	public Role findRoleByName(String name) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		return (Role) crudService.findSingleWithNamedQuery("Role.findByName", params);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createPatient(Long userID) {
		Role role = new Patient(); 
		role.setCode("PTNT");
		role.setName("Patient");
		crudService.create(role);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createDoctor(Long userID) {
		Role role = new Doctor(); 
		role.setCode("DOC");
		role.setName("Doctor");
		crudService.create(role);
	}

	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createWakd(Long userID) {
		Role role = new WAKD(); 
		role.setCode("WAKD");
		role.setName("Wearable Artificial Kidney");
		crudService.create(role);
	}
	
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public void createSmartPhone(Long userID) {
		Role role = new SmartPhone(); 
		role.setCode("SP");
		role.setName("Smart phone");
		crudService.create(role);
	}

}
