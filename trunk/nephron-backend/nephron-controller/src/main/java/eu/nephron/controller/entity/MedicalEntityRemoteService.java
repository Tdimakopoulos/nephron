package eu.nephron.controller.entity;

import javax.ejb.Remote;

import eu.nephron.controller.ControllerService;
import eu.nephron.model.entity.Device;
import eu.nephron.model.entity.Person;
import eu.nephron.model.entity.WakdState;
import eu.nephron.wakd.api.enums.WakdAlertEnum;
import eu.nephron.wakd.api.enums.WakdOpStateEnum;
import eu.nephron.wakd.api.enums.WakdStateEnum;

@Remote
public interface MedicalEntityRemoteService extends ControllerService {
	
	public WakdState findCurrentState(Long deviceID, Long userID);

	public Person findPatientByLastName(String lastName);
	
	public Person findDoctorByLastName(String lastName);
	
	public Device findSmartPhoneBySerialNumber(String serialNumber);
	
	public Device findWakdBySerialNumber(String serialNumber);
	
	public void createPatient(Person patient, Long userID);
	
	public void createDoctor(Person doctor, Long userID);
	
	public void createWakd(Device wakd, Long userID);
	
	public void createSmartPhone(Device smartphone, Long userID);
	
	public void createAlert(Long deviceID, WakdAlertEnum alert, Long userID);
	
	public void createState(Long deviceID, WakdStateEnum wakdState, WakdOpStateEnum wakdOpState, Long userID);
	
}
