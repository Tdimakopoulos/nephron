package eu.nephron.wakd.api;

import java.io.Serializable;

import eu.nephron.utils.ByteUtils;
import eu.nephron.wakd.api.enums.WakdCommandEnum;
import eu.nephron.wakd.api.enums.WhoEnum;

public class Header implements Serializable {

	private static final long serialVersionUID = -4413874247302804998L;
	
	public static final int DATA_ID = 3;
	
	public static final int HEADER_SIZE = 6;

	private int id;
	
	private int size;
	
	private WhoEnum sender;
	
	private WhoEnum recipient;
	
	private WakdCommandEnum command;
	
	public Header() {
		this.sender = WhoEnum.SP;
		this.recipient = WhoEnum.CB;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public WhoEnum getSender() {
		return sender;
	}

	public void setSender(WhoEnum sender) {
		this.sender = sender;
	}

	public WhoEnum getRecipient() {
		return recipient;
	}

	public void setRecipient(WhoEnum recipient) {
		this.recipient = recipient;
	}

	public WakdCommandEnum getCommand() {
		return command;
	}

	public void setCommand(WakdCommandEnum command) {
		this.command = command;
	}
	
	public byte[] encode() {
		byte[] bytes = new byte[6];
		bytes[0] = this.recipient.getValue();
		System.arraycopy(ByteUtils.intToByteArray(this.size, 2), 0, bytes, 1, 2);
		bytes[3] = this.command.getValue();
		bytes[4] = ByteUtils.intToByte(this.id);
		bytes[5] = this.sender.getValue();
		return bytes;
	}
	
	public void decode(byte[] bytes) {
		this.recipient = WhoEnum.getWhoEnum(bytes[0]);
		this.size = ByteUtils.byteArrayToInt(bytes, 1, 2);
		this.command = WakdCommandEnum.getWakdCommandEnum(bytes[3]);
		this.id = bytes[4]; 
		this.sender = WhoEnum.getWhoEnum(bytes[0]);
	}

}
