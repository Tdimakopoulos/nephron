package eu.nephron.wakd.api.sensor;


public class FlowPumpCtrlData extends PumpCtrlData {

	private static final long serialVersionUID = -473791258050550475L;

	public FlowPumpCtrlData(PumpDirectionEnum direction, int flowReference) {
		super(direction, flowReference);
	}
}
