package eu.nephron.wakd.api;

import eu.nephron.wakd.api.enums.WakdCommandEnum;
import eu.nephron.wakd.api.incoming.CurrentStateMsg;

public abstract class IncomingWakdMsgFactory {

	public static IncomingWakdMsg createIncomingWakdMsg(byte[] bytes) {

		IncomingWakdMsg msg = null;
		WakdCommandEnum command = WakdCommandEnum.getWakdCommandEnum(bytes[Header.DATA_ID]);
		switch (command) {
		case CURRENT_STATE:
			msg = new CurrentStateMsg();
			msg.decode(bytes);
			break;
		default:
			break;
		}
		
		return msg;
	}

}
