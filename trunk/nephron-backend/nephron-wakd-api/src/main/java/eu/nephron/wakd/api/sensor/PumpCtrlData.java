package eu.nephron.wakd.api.sensor;

import eu.nephron.utils.ByteUtils;

public abstract class PumpCtrlData extends SensorCtrlData {

	private static final long serialVersionUID = 868807817274489409L;

	private PumpDirectionEnum direction;
	
	private int flowReference;

	public PumpCtrlData(PumpDirectionEnum direction, int flowReference) {
		super();
		this.direction = direction;
		this.flowReference = flowReference;
	}

	public PumpDirectionEnum getDirection() {
		return direction;
	}

	public void setDirection(PumpDirectionEnum direction) {
		this.direction = direction;
	}

	public int getFlowReference() {
		return flowReference;
	}

	public void setFlowReference(int flowReference) {
		this.flowReference = flowReference;
	}
	
	@Override
	public byte[] encode() {
		byte[] bytes = new byte[3];
		bytes[0] = direction.getValue();
		System.arraycopy(ByteUtils.intToByteArray(this.flowReference, 2), 0, bytes, 1, 2);
		return bytes; 
	}
	
	public static enum PumpDirectionEnum {
		PLUS((byte)1),
		MINUS((byte)2);
		
		private byte value;

		private PumpDirectionEnum(byte value) {
			this.value = value;
		}

		public byte getValue() {
			return value;
		}

		public void setValue(byte value) {
			this.value = value;
		}
	}
	
}
