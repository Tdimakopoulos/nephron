package eu.nephron.wakd.api.sensor;

import eu.nephron.utils.ByteUtils;

public class BloodPressureData extends SensorData {

	private static final long serialVersionUID = 4824838792670764481L;

	private int systolic;
	
	private int diastolic;
	
	private int heartRate;

	public int getSystolic() {
		return systolic;
	}

	public void setSystolic(int systolic) {
		this.systolic = systolic;
	}

	public int getDiastolic() {
		return diastolic;
	}

	public void setDiastolic(int diastolic) {
		this.diastolic = diastolic;
	}

	public int getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}

	@Override
	public byte[] encode() {
		byte[] bytes = new byte[2];
		bytes[0] = ByteUtils.intToByte(systolic);
		bytes[1] = ByteUtils.intToByte(diastolic);
		bytes[2] = ByteUtils.intToByte(heartRate);
		return bytes;
	}
	
}
