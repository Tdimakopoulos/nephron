package eu.nephron.jaxb.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.act.ActDTO;
import eu.nephron.jaxb.model.act.ActRelationshipDTO;
import eu.nephron.jaxb.model.act.ParticipationDTO;
import eu.nephron.jaxb.model.entity.MedicalEntityDTO;

@XmlType(name="NephronEntity")
@XmlSeeAlso({
		ActDTO.class,
		ActRelationshipDTO.class,
		MedicalEntityDTO.class,
		ParticipationDTO.class
	})
public abstract class NephronEntityDTO implements Serializable {

	private static final long serialVersionUID = -3907234325917093070L;
	
}