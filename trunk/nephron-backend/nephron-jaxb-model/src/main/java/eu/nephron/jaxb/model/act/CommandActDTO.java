package eu.nephron.jaxb.model.act;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="CommandAct")
@XmlRootElement(name="commandAct")
public class CommandActDTO extends ActDTO {

	private static final long serialVersionUID = 1921575990566295809L;
	
	private String wakdMsg;

	@XmlElement
	public String getWakdMsg() {
		return wakdMsg;
	}

	public void setWakdMsg(String wakdMsg) {
		this.wakdMsg = wakdMsg;
	}
	
}
