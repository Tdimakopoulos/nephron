package eu.nephron.jaxb.model.act;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="MoodCodeEnum")
@XmlRootElement(name="moodCodeEnum")
public enum MoodCodeEnumDTO {
	UNDEFINED(0), DEFINITION(1), INTENT(2), REQUEST(3), EVENT(4), CRITERION(5), GOAL(6);

	private Integer identifier;

	private MoodCodeEnumDTO(Integer identifier) {
		this.identifier = identifier;
	}

	@XmlElement
	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}
	
}
