package eu.nephron.jaxb.model.entity;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Material")
@XmlSeeAlso({
	DeviceDTO.class
})

public abstract class MaterialDTO extends MedicalEntityDTO {

	private static final long serialVersionUID = 4841659167468881081L;

}
