package eu.nephron.jaxb.model.act;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.NephronEntityDTO;

@XmlType(name="Participation")
@XmlRootElement(name="participation")
public class ParticipationDTO extends NephronEntityDTO {

	private static final long serialVersionUID = 8632758230763405136L;

	private Long id;

	private String notes;
	
	private Date participationDate;

	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@XmlElement
	public Date getParticipationDate() {
		return participationDate;
	}

	public void setParticipationDate(Date participationDate) {
		this.participationDate = participationDate;
	}

}
