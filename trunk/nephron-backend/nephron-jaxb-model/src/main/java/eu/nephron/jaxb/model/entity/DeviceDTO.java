package eu.nephron.jaxb.model.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Device")
@XmlRootElement
public class DeviceDTO extends MaterialDTO {

	private static final long serialVersionUID = 850007589781934921L;
	
	private String serialNumber;

	@XmlElement
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
}
