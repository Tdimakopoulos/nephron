package eu.nephron.jaxb.model.act;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import eu.nephron.jaxb.model.NephronEntityDTO;

@XmlType(name="Act")
@XmlSeeAlso({
	CommandActDTO.class
})
public abstract class ActDTO extends NephronEntityDTO {

	private static final long serialVersionUID = -4562844316400682371L;
	
	protected Long id;
	
	protected ActStatusEnumDTO actStatus;
	
	protected MoodCodeEnumDTO moodCode;
	
	@XmlElement
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlElement
	public ActStatusEnumDTO getActStatus() {
		return actStatus;
	}

	public void setActStatus(ActStatusEnumDTO actStatus) {
		this.actStatus = actStatus;
	}

	@XmlElement
	public MoodCodeEnumDTO getMoodCode() {
		return moodCode;
	}

	public void setMoodCode(MoodCodeEnumDTO moodCode) {
		this.moodCode = moodCode;
	}
	
}
