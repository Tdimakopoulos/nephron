package eu.nephron.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import eu.nephron.model.address.Address;

@Entity
@DiscriminatorValue("0")
@NamedQueries({
	@NamedQuery(name="Person.findDoctorByLastName", query="select p from Person p where p.role.code='DOC' and p.personalInfo.lastName=:lastName"),
	@NamedQuery(name="Person.findPatientByLastName", query="select p from Person p where p.role.code='PTNT' and p.personalInfo.lastName=:lastName")
})
public class Person extends LivingSubject {

	private static final long serialVersionUID = 4566288674204325343L;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="PERSONAL_INFO_ID")
	private PersonalInfo personalInfo;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="CONTACT_INFO_ID")
	private ContactInfo contactInfo;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="ADDRESS_ID")
	private Address address;

	public PersonalInfo getPersonalInfo() {
		return personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

	public ContactInfo getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(ContactInfo contactInfo) {
		this.contactInfo = contactInfo;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
}
