package eu.nephron.model.entity;

import org.apache.commons.lang.math.NumberUtils;

public enum EducationalLevelEnum {
	UNDEFINED(0), ELEMENTARY(1), HIGH_SCOOL(2), COLLEGE(3), UNIVERSITY(4), MASTER(5), PHD(6);
	
	public static EducationalLevelEnum getEducationalLevelEnum(Integer educationalLevel) {
		switch (null != educationalLevel ? educationalLevel : NumberUtils.INTEGER_ZERO) {
		case 1:
			return ELEMENTARY;
		case 2:
			return HIGH_SCOOL;
		case 3:
			return COLLEGE;
		case 4:
			return UNIVERSITY;
		case 5:
			return MASTER;
		case 6:
			return PHD;
		default:
			return UNDEFINED;
		}
	}

	private Integer identifier;

	private EducationalLevelEnum(Integer identifier) {
		this.identifier = identifier;
	}

	public Integer getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Integer identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.valueOf(this.identifier);
	}
	
}
