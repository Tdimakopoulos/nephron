package eu.nephron.model.address;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import eu.nephron.model.NephronEntity;

@Entity
@Table(name="ADDRESS")
public class Address extends NephronEntity<Long> {

	private static final long serialVersionUID = -6887529216107152471L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ADDRESS_ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID", nullable=false)
	private Country country;
	
	@Column(name="ADMINISTRATIVE_AREA")
	private String administrativeArea;
	
	@Column(name="CITY", nullable=false)
	private String city;
	
	@Column(name="STREET", nullable=false)
	private String street;
	
	@Column(name="STREET_NO")
	private String streetNo;
	
	@Column(name="FLOOR")
	private String floor;
	
	@Column(name="POST_CODE", nullable=false)
	private String postCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getAdministrativeArea() {
		return administrativeArea;
	}

	public void setAdministrativeArea(String administrativeArea) {
		this.administrativeArea = administrativeArea;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

}
