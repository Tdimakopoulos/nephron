package eu.nephron.model.act;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("3")
public class QuestionnaireAct extends Act {

	private static final long serialVersionUID = -7772362479745118843L;
	
	@OneToMany(mappedBy="act")
	private Set<QuestionnaireTask> tasks;

	public Set<QuestionnaireTask> getTasks() {
		return tasks;
	}

	public void setTasks(Set<QuestionnaireTask> tasks) {
		this.tasks = tasks;
	}
	
}
