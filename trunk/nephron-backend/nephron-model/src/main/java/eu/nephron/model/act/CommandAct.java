package eu.nephron.model.act;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@DiscriminatorValue("0")
public class CommandAct extends Act {
	
	private static final long serialVersionUID = -6665269315346075795L;
	
	@Lob
	@Column(name="WAKD_MSG")
	private byte[] wakdMsg;

	public byte[] getWakdMsg() {
		return wakdMsg;
	}

	public void setWakdMsg(byte[] wakdMsg) {
		this.wakdMsg = wakdMsg;
	}
	
}
