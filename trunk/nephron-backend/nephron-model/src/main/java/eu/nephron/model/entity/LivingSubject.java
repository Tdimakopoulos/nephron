package eu.nephron.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

@MappedSuperclass
public abstract class LivingSubject extends MedicalEntity {

	private static final long serialVersionUID = -4570973140060018575L;

	@Column(name="GENDER")
	@Type(
        type = "eu.nephron.model.GenericEnumUserType",
        parameters = {
                @Parameter(name = "enumClass", value = "eu.nephron.model.entity.GenderEnum"),
                @Parameter(name = "identifierMethod", value = "getIdentifier"),
                @Parameter(name = "valueOfMethod", value="getGenderEnum")})
	protected GenderEnum gender;
	
	@Column(name="BIRTH_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date birthDate;

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
}
