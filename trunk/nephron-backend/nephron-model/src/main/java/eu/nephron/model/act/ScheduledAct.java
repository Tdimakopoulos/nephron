package eu.nephron.model.act;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("4")
public class ScheduledAct extends Act {

	private static final long serialVersionUID = -5241114269822648098L;
	
	@OneToMany(mappedBy="act")
	private Set<ScheduledTask> tasks;

	public Set<ScheduledTask> getTasks() {
		return tasks;
	}

	public void setTasks(Set<ScheduledTask> tasks) {
		this.tasks = tasks;
	}
	
}
