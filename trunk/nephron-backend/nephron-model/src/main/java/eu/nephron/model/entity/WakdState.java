package eu.nephron.model.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.nephron.model.NephronEntity;

@Entity
@Table(name="WAKD_STATE")
@NamedQueries({
	@NamedQuery(name="WakdState.findWakdStateByMaxDate",
				query="select s from WakdState s where s.device.id=:deviceID and s.date=(select max(s.date) from WakdState s where s.device.id=:deviceID)")
})
public class WakdState extends NephronEntity<Long> {

	private static final long serialVersionUID = -1690642835490297690L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="WAKD_STATE_ID")
	private Long id;
	
	@Column(name="WAKD_STATE", nullable=false)
	private Integer state;
	
	@Column(name="WAKD_OP_STATE", nullable=false)
	private Integer opState;
	
	@Column(name="STATUS_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="DEVICE_ID")
	private Device device;
	
	@ManyToOne
	@JoinColumn(name="PARENT_STATE_ID")
	private WakdState parent;
	
	@OneToMany(mappedBy="parent")
	private Set<WakdState> children;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getOpState() {
		return opState;
	}

	public void setOpState(Integer opState) {
		this.opState = opState;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public WakdState getParent() {
		return parent;
	}

	public void setParent(WakdState parent) {
		this.parent = parent;
	}

	public Set<WakdState> getChildren() {
		return children;
	}

	public void setChildren(Set<WakdState> children) {
		this.children = children;
	}

}
