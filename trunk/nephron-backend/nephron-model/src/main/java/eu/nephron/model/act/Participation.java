package eu.nephron.model.act;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.nephron.model.NephronEntity;
import eu.nephron.model.entity.MedicalEntity;

@Entity
@Table(name="PARTICIPATION")
public class Participation extends NephronEntity<Long> {

	private static final long serialVersionUID = -935425689924167275L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PARTICIPATION_ID")
	private Long id;
	
	@Column(name="NOTES")
	private String notes;
	
	@Column(name="PARTICIPATION_DATE", nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date participationDate;
	
	@ManyToOne
	@JoinColumn(name="ACT_ID", nullable=false)
	private Act act;
	
	@ManyToOne
	@JoinColumn(name="MEDICAL_ENTITY_ID", nullable=false)
	private MedicalEntity medicalEntity;
	
	public Participation() {
		this.participationDate = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getParticipationDate() {
		return participationDate;
	}

	public void setParticipationDate(Date participationDate) {
		this.participationDate = participationDate;
	}

	public Act getAct() {
		return act;
	}

	public void setAct(Act act) {
		this.act = act;
	}

	public MedicalEntity getMedicalEntity() {
		return medicalEntity;
	}

	public void setMedicalEntity(MedicalEntity medicalEntity) {
		this.medicalEntity = medicalEntity;
	}
	
}
