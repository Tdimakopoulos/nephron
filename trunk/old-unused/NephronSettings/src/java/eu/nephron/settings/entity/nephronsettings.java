/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.settings.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class nephronsettings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String szgroup;
    private String szname;
    private String value;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof nephronsettings)) {
            return false;
        }
        nephronsettings other = (nephronsettings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.settings.entity.nephronsettings[ id=" + id + " ]";
    }

    /**
     * @return the szgroup
     */
    public String getSzgroup() {
        return szgroup;
    }

    /**
     * @param szgroup the szgroup to set
     */
    public void setSzgroup(String szgroup) {
        this.szgroup = szgroup;
    }

    /**
     * @return the szname
     */
    public String getSzname() {
        return szname;
    }

    /**
     * @param szname the szname to set
     */
    public void setSzname(String szname) {
        this.szname = szname;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}
