/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.smdb;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 *
 * @author tdim
 */
@Entity
public class ManageShutdownMessages implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long DeviceID;
    private int icommandID;
    private int iACK;
    private String Message;
    private String szTextfield1;
    private String szTextfield2;
    private String szTextfield3;
    private int iField1;
    private int iField2;
    private int iField3;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    /**
     * @return the DeviceID
     */
    public Long getDeviceID() {
        return DeviceID;
    }

    /**
     * @param DeviceID the DeviceID to set
     */
    public void setDeviceID(Long DeviceID) {
        this.DeviceID = DeviceID;
    }

    /**
     * @return the icommandID
     */
    public int getIcommandID() {
        return icommandID;
    }

    /**
     * @param icommandID the icommandID to set
     */
    public void setIcommandID(int icommandID) {
        this.icommandID = icommandID;
    }

    /**
     * @return the iACK
     */
    public int getiACK() {
        return iACK;
    }

    /**
     * @param iACK the iACK to set
     */
    public void setiACK(int iACK) {
        this.iACK = iACK;
    }

    /**
     * @return the Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message the Message to set
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return the szTextfield1
     */
    public String getSzTextfield1() {
        return szTextfield1;
    }

    /**
     * @param szTextfield1 the szTextfield1 to set
     */
    public void setSzTextfield1(String szTextfield1) {
        this.szTextfield1 = szTextfield1;
    }

    /**
     * @return the szTextfield2
     */
    public String getSzTextfield2() {
        return szTextfield2;
    }

    /**
     * @param szTextfield2 the szTextfield2 to set
     */
    public void setSzTextfield2(String szTextfield2) {
        this.szTextfield2 = szTextfield2;
    }

    /**
     * @return the szTextfield3
     */
    public String getSzTextfield3() {
        return szTextfield3;
    }

    /**
     * @param szTextfield3 the szTextfield3 to set
     */
    public void setSzTextfield3(String szTextfield3) {
        this.szTextfield3 = szTextfield3;
    }

    /**
     * @return the iField1
     */
    public int getiField1() {
        return iField1;
    }

    /**
     * @param iField1 the iField1 to set
     */
    public void setiField1(int iField1) {
        this.iField1 = iField1;
    }

    /**
     * @return the iField2
     */
    public int getiField2() {
        return iField2;
    }

    /**
     * @param iField2 the iField2 to set
     */
    public void setiField2(int iField2) {
        this.iField2 = iField2;
    }

    /**
     * @return the iField3
     */
    public int getiField3() {
        return iField3;
    }

    /**
     * @param iField3 the iField3 to set
     */
    public void setiField3(int iField3) {
        this.iField3 = iField3;
    }
    
}
