/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.controller;

import eu.nephron.smdb.ManageShutdownMessages;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class ManageShutdownMessagesFacade extends AbstractFacade<ManageShutdownMessages> implements ManageShutdownMessagesFacadeLocal {
    @PersistenceContext(unitName = "phonemanagerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ManageShutdownMessagesFacade() {
        super(ManageShutdownMessages.class);
    }
    
}
