/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.controller;

import eu.nephron.smdb.ManageShutdownMessages;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface ManageShutdownMessagesFacadeLocal {

    void create(ManageShutdownMessages manageShutdownMessages);

    void edit(ManageShutdownMessages manageShutdownMessages);

    void remove(ManageShutdownMessages manageShutdownMessages);

    ManageShutdownMessages find(Object id);

    List<ManageShutdownMessages> findAll();

    List<ManageShutdownMessages> findRange(int[] range);

    int count();
    
}
