/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.phonemanager.soap;

import eu.nephron.controller.ManageShutdownMessagesFacadeLocal;
import eu.nephron.smdb.ManageShutdownMessages;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "ManagePhoneMessages")
public class ManagePhoneMessages {

    @EJB
    private ManageShutdownMessagesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "SPScreate")
    @Oneway
    public void SPScreate(@WebParam(name = "manageShutdownMessages") ManageShutdownMessages manageShutdownMessages) {
        ejbRef.create(manageShutdownMessages);
    }

    @WebMethod(operationName = "SPSedit")
    @Oneway
    public void SPSedit(@WebParam(name = "manageShutdownMessages") ManageShutdownMessages manageShutdownMessages) {
        ejbRef.edit(manageShutdownMessages);
    }

    @WebMethod(operationName = "SPSremove")
    @Oneway
    public void SPSremove(@WebParam(name = "manageShutdownMessages") ManageShutdownMessages manageShutdownMessages) {
        ejbRef.remove(manageShutdownMessages);
    }

    @WebMethod(operationName = "SPSfind")
    public ManageShutdownMessages SPSfind(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "SPSfindAll")
    public List<ManageShutdownMessages> SPSfindAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "SPSfindRange")
    public List<ManageShutdownMessages> SPSfindRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "SPScount")
    public int SPScount() {
        return ejbRef.count();
    }

    @WebMethod(operationName = "SPSCreateShutdown")
    public void SPSCreateShutdown(@WebParam(name = "imei") String imei, @WebParam(name = "reason") String reason) {
        ManageShutdownMessages pmsg = new ManageShutdownMessages();
        pmsg.setIcommandID(0);
        pmsg.setiACK(0);
        pmsg.setSzTextfield1(imei);
        pmsg.setMessage(reason);
        ejbRef.create(pmsg);
    }

    @WebMethod(operationName = "SPSUpdateShutdownACK")
    public void SPSUpdateShutdownACK(@WebParam(name = "imei") String imei) {
        List<ManageShutdownMessages> pfindall = ejbRef.findAll();
        ManageShutdownMessages pfind = new ManageShutdownMessages();
        for (int i = 0; i < pfindall.size(); i++) {
            if (pfindall.get(i).getSzTextfield1() != null) {
                if (pfindall.get(i).getSzTextfield1().equalsIgnoreCase(imei)) {
                    pfind = pfindall.get(i);
                    pfind.setiACK(1);
                    ejbRef.edit(pfind);
                }
            }
        }
    }
}
