/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.converter;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
/**
 *
 * @author tdim
 */



public class WakdMessageConverter {
        public String convertTo(byte[] source, String destination) {
		return null != source ? Hex.encodeHexString(source) : null;
	}

	public byte[] convertFrom(String source, byte[] destination) {
		try {
			return Hex.decodeHex(source.toCharArray());
		} catch (DecoderException e) {
			throw new RuntimeException(e);
		}
	}

}
