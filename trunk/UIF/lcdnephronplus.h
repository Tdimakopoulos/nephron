// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   lcdnephronplus.h
//! \brief  specific functions for NEPHRONPLUS LCD
//!
//! Creation of the definitions, variables and prototype functions exported
//!
//! \author  DUDNIK G.S.
//! \date    23.12.2011
//! \version 1.0 First version (gdu)
// -----------------------------------------------------------------------------------
#ifndef _LCDNEPHRONPLUS_H_
#define _LCDNEPHRONPLUS_H_
// -----------------------------------------------------------------------------------
// Include
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
// fonts -----------------------------------------------------------------------------
#define G_FONT_DEFAULT 	      font_standard
#define G_FONT_ARIALR 	      ArialRegular7x8	
#define MAX_ARIALR	      19
#define G_FONT_ARIAL18N	      ArialNumbers12x20	
#define G_FONT_ARIAL20	      ArialRegular20
#define G_FONT_TERMINAL6      Terminal6Patterns
// menuitems status ------------------------------------------------------------------
#define MAIN_ITEMS            3
#define P_DATA_ITEMS	      4
#define STATUS_ITEMS	      4

#define P_DATA_QTY	      24    // quantity of p_data types
#define STATUS_QTY            24    // quantity of status items
#define COMMAND_QTY           10
#define MODE_QTY              4
#define CMD_ACCEPTED_MAX      10
#define CMD_REJECTED_MAX      16
#define INSTMSG_QTY           8


// display time to sleep in seconds --------------------------------------------------
#define   DISPLAY_TIME_TO_SLEEP 15  // 10 (<19.08.2010)
// key programming value -------------------------------------------------------------
#define KEYCODE_0_RESERVED    0
#define KEYCODE_1_OK          1    
#define KEYCODE_2_TOP         2
#define KEYCODE_3_BOTTOM      3
#define KEYCODE_4_RIGHT       4
#define KEYCODE_5_LEFT        5
// -----------------------------------------------------------------------------------
// Exported type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported inline code definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void lcd_JackTest(void);
extern void lcd_DisplayOFF(void);
extern void LCD_DISPLAY_MAIN_MENU(uint8_t which_item);
extern void LCD_DISPLAY_P_DATA_SUBMENU(uint8_t page, uint8_t max_index);
extern void LCD_DISPLAY_STATUS_SUBMENU(uint8_t page,  uint8_t max_index);
extern void LCD_DISPLAY_COMMAND_SUBMENU(uint8_t index, uint8_t which_item, uint8_t max_index);
extern void LCD_DISPLAY_MODE_SUBMENU(uint8_t index, uint8_t which_item, uint8_t max_index);
extern void LCD_DISPLAY_PRESENTATION(void);
extern void LCD_DISPLAY_PRESENTATION_EV(void);
extern void LCD_DISPLAY_P_DATA_CODE_EV (short counter);
extern void LCD_DISPLAY_COMMAND_LIST (uint8_t index);
extern void LCD_DISPLAY_COMMAND_ACCEPTED (uint8_t index);
extern void LCD_DISPLAY_COMMAND_REJECTED (uint8_t index);
extern void LCD_DISPLAY_Show_KEY(uint8_t key_pressed);
extern void DisplayMessage(uint8_t mode);
extern void LCD_DISPLAY_INSTANT_MESSAGES (uint8_t index);
extern void lcd_LoadNephronPlus (void);
extern void LCD_DISPLAY_NEPHRONPLUS(void);
extern void DisplayAlarm(uint32_t alarmID);
extern void DisplayError(uint32_t errorID);
// -----------------------------------------------------------------------------------
#endif // _LCDNEPHRONPLUS_H_ //
// -----------------------------------------------------------------------------------
// END LCDNEPHRONPLUS.H
// -----------------------------------------------------------------------------------
