// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   UCB0.c
//! \brief  UCB0 module functions
//!
//! Creation of the function of initialization and communication for UCB0 in SPI MASTER.
//!
//! \author  DUDNIK G.S.
//! \date    21.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "ucb0.h"
#include "io.h"
#include "uifMbProtocol.h"
// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
#define RXSPI_UCB0_LEN  10
#define RXSPI_TRIALS    100
uint8_t rx_spi_ucb0 = 0;
uint8_t rx_flag_ucb0 = 0;
uint16_t rx_data_counter = 0;
uint8_t rx_spi_ucb0_data[RXSPI_UCB0_LEN];
uint8_t not_answer = 0;
uint8_t setget = MB_UNKNOWN; // 1: set info, 2: get cmds
uint8_t mb_uif_cmd = 0;
uint8_t rx_spi_ucb0_cmds[4];
uint8_t send_ko = 0;
uint8_t rx_state_counter = 0;
uint8_t rx_spi_trial_counter = 0;
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the UCB0 in SPI MASTER mode
//!
//! This function initializes the UCB0 hardware to operate in SPI MASTER mode.
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
void ucb0InitSPIMS(void)
{
    UCB0CTL1 |= UCSWRST;
    UCB0CTL0 |= UCMST+UCSYNC+UCMSB+UCMODE_0+UCCKPL;
                                    // b0: sync, b2,b1: 4-pin UCxSTE active low,
                                    // b3: SPI master, b4: 8-bit, b5: msb 1st
                                    // b6:clk inactive low				
                                    // b7:data changes 1st, latches 2nd
					
    // @@@@ 03-12-2009 = 4-MHZ?
    UCB0CTL1 |= UCSSEL_2;           // SMCLK
    //UCB0BR0 = 0x01;
    //UCB0BR0 = 0x02;               // /2 = 4MHZ
    //UCB0BR0 = 0x10;               // /16 = 500KHZ
    UCB0BR0 = 0x50;                 // /80 = 100KHZ
    UCB0BR1 = 0;	            // prescaler: UCB0BR0 + UCB0BR1x256	
    UCB0CTL1 &= ~UCSWRST;           // **Initialize USCI state machine**
    //UCB0IE |= UCRXIE;		    // Enable USCI_B0 RX interrupt
    
}
// -----------------------------------------------------------------------------------
//! \brief  ucb0InitSPISL
//!         Initialization of the UCB0 in SPI SLAVE mode
//!
//! This function initializes the UCB0 hardware to operate in SPI SLAVE mode.
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
//! UCCnxCTL0, n= A,B, x=0,1
//!
//! UCCKPH: 
//!   1 Data is captured on the first UCLK edge and changed on the following edge
//!   0 Data is changed on the first UCLK edge and captured on the following edge
//! UCCKPL: <--
//!   0 The inactive state is low
//!   1 The inactive state is high
//! UCMSB:
//!   0 LSB first
//!   1 MSB first
//! UCMST:
//!   0 Slave mode
//!   1 Master mode
//! UCMODEx:
//!   0 3-pin SPI
//! UCSYNC:
//!   0 Asynchronous mode
//!   1 Synchronous mode
// -----------------------------------------------------------------------------------
void ucb0InitSPISL(void)
{
    uint8_t x = 0;
    UCB0CTL1 |= UCSWRST;                      // SOFTWARE RESET

    UCB0CTL0 |= UCSYNC+UCMSB+UCMODE_2;        //+UCCKPH;        //12-02-2010: WATCH OUT!
                                              // b0: sync, b2,b1: 3-pin SPI,
                                              // b3: SPI slave, b4: 8-bit, b5: msb 1st
                                              // b6:clk inactive low				
                                              // b7:data changes 1st, latches 2nd
    
    UCB0CTL1 &= ~UCSWRST;		      // **Initialize USCI state machine**
    
    UCB0IE |= UCRXIE;	                      // Enable USCI_B0 RX interrupt
    
    for (x=0;x < 20;x++) rx_spi_ucb0_data[x] = 0xFF;
    rx_flag_ucb0 = 0;
    rx_data_counter = 0;
    
    CLR_MASK(UCB0IFG,UCTXIFG);      // Clear the transmit interrupt flag
    CLR_MASK(UCB0IFG,UCRXIFG);      // Clear the transmit interrupt flag

}
// -----------------------------------------------------------------------------------
//! \brief  USCI_B0_ISR interrupt
//!
//! Reads Incoming data and looks for EOM
//! When EOM is received --> a flag is set
//!
//! \param   nothing
//! \return  void
// -----------------------------------------------------------------------------------
#pragma vector=USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
  uint8_t z = 0;
  uint8_t x = 0;
  
  switch(__even_in_range(UCB0IV,4))
  {
    
    case 0:break;                             // Vector 0 - no interrupt
    case 2:                                   // Vector 2 - RXIFG
      // waits for end of line...
      rx_spi_ucb0 = UCB0RXBUF;
      
      not_answer = 0;
      if (rx_spi_ucb0 == MB_UIF_CMD_TEST1) { // S
          for(z=0;z<RXSPI_UCB0_LEN;z++) rx_spi_ucb0_data[z]= MB_UIF_CMD_CLR; 
          for(z=0;z<4;z++) rx_spi_ucb0_cmds[z] = MB_UIF_CMD_CLR;
          rx_flag_ucb0 = 1;
          rx_data_counter = 0;
          rx_state_counter = 0;
          rx_spi_trial_counter = 0;
      } // S
      else{ // S
        if(rx_flag_ucb0 == 1){ // T
          if ((rx_spi_ucb0 == MB_UIF_CMD_ACKNOWLEDGE)||(rx_spi_ucb0 == MB_UIF_CMD_NOT_ACKNOWLEDGE)) {
              rx_flag_ucb0 = 0;
              rx_data_counter = 0;
              not_answer = 1;
          }
          else{ // U
              rx_spi_ucb0_data[rx_data_counter]= rx_spi_ucb0;
              x = rx_data_counter;
              if(setget != MB_GETCMDS){
                  if(rx_data_counter<(RXSPI_UCB0_LEN-1)) rx_data_counter++;
              }
              if(x==0) { // V1
                mb_uif_cmd = rx_spi_ucb0;
                setget = MB_UIF_CMD_TYPE(rx_spi_ucb0);
                if(setget == MB_UNKNOWN){
                    rx_flag_ucb0 = 0;
                    rx_data_counter = 0;
                    not_answer = 1;
                }
              } // V1
              else{ // V2
                if(setget == MB_SETINFO){ // W 
                    if (rx_spi_ucb0 == MB_UIF_CMD_END) { // X                    
                        if(rx_data_counter==MB_UIF_QTY_CMD_SET) rx_spi_ucb0 = MB_UIF_CMD_SET_PROCESS(rx_spi_ucb0_data, RXSPI_UCB0_LEN);
                        else  {
                            rx_spi_ucb0 = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                            rx_flag_ucb0 = 0;
                            rx_data_counter = 0;
                        }
                    } // X
                } // W
                else { // W (setget = MB_GETCMDS)
                  if (rx_spi_ucb0 == MB_UIF_CMD_END) { // X1                    
                      rx_spi_ucb0 = MB_UIF_CMDS_GET(mb_uif_cmd, 0); // sends first D[1] (from D[1]:D[0])
                      rx_spi_ucb0_cmds[0] = rx_spi_ucb0;
                      rx_state_counter = 1;
                      send_ko = 0;
                  } // X1
                  else if(rx_state_counter == 1){  // X2-I should have received D[1]
                      rx_spi_ucb0 = MB_UIF_CMDS_GET(mb_uif_cmd, 1); // sends second D[0] (from D[1]:D[0])
                      rx_spi_ucb0_cmds[1] = rx_spi_ucb0;
                      rx_state_counter = 2;                    
                  } // X2
                  else if(rx_state_counter == 2){  // X3-I should have received D[1]
                      if(rx_spi_ucb0 != MB_UIF_CMDS_GET(mb_uif_cmd, 0)) send_ko = 1;
                      if(send_ko == 0) rx_spi_ucb0 = MB_UIF_CMD_ACKNOWLEDGE;
                      else rx_spi_ucb0 = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                      rx_state_counter = 3;                    
                  } // X3
                  else if(rx_state_counter == 3){  // X4-I should have received [D0]                    
                      if(rx_spi_ucb0 != MB_UIF_CMDS_GET(mb_uif_cmd, 1)) send_ko = 1;
                      if(send_ko == 0) {
                          rx_spi_ucb0 = MB_UIF_CMD_ACKNOWLEDGE;
                          // Clear Command Register Here
                          UIF_MB_CLR_CMD();
                      }
                      else {
                          rx_spi_ucb0 = MB_UIF_CMD_NOT_ACKNOWLEDGE;
                      }
                      rx_flag_ucb0 = 1; // Is it useful for something here (16.04.2012, revision)
                      rx_data_counter = 0;
                      rx_state_counter = 0;
                      // kind of time-out of trials, waiting for ending the full msg                      
                      rx_spi_trial_counter++;
                      if(rx_spi_trial_counter>= RXSPI_TRIALS){
                            rx_spi_trial_counter = 0;
                            rx_flag_ucb0 = 0;
                      }                 
                  } // X1-X2-X3-X4                 
                } // W
              } // V1-V2
          } // U
        } // T  
      } // S  
      if(not_answer==0){
          while (!(UCB0IFG&UCTXIFG));           // USCI_B0 TX buffer ready?
          UCB0TXBUF = rx_spi_ucb0;
      }
      break;
    case 4:break;                             // Vector 4 - TXIFG
    default: break;
  }
}
// -----------------------------------------------------------------------------------
#if 0 // ok loopback
// -----------------------------------------------------------------------------------
#pragma vector=USCI_B0_VECTOR
__interrupt void USCI_B0_ISR(void)
{
  switch(__even_in_range(UCB0IV,4))
  {
    case 0:break;                             // Vector 0 - no interrupt
    case 2:                                   // Vector 2 - RXIFG
      // waits for end of line...
      rx_spi_ucb0 = UCB0RXBUF;
      // TEST
      if (rx_spi_ucb0 == 0x35) rx_spi_ucb0 = MB_UIF_CMD_ACKNOWLEDGE;
      
      while (!(UCB0IFG&UCTXIFG));           // USCI_B0 TX buffer ready?
      UCB0TXBUF = rx_spi_ucb0;
      break;
    case 4:break;                             // Vector 4 - TXIFG
    default: break;
  }
}
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------
// UCB0ICTL /* USCI B0 Interrupt Enable Register */
// UCB0IFG  /* USCI B0 Interrupt Flags Register */
// UCRXIFG  /* USCI Receive Interrupt Flag */
// -----------------------------------------------------------------------------------
// END UCB0.C
// -----------------------------------------------------------------------------------