// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   UCA0.c
//! \brief  UCA0 module functions
//!
//! Creation of the function of initialization and communication for UCA0 in UART mode.
//!
//! \author  DUDNIK G.S.
//! \date    21.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "uca0.h"
#include "io.h"
// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
uint8_t rx_flag_uart_uca0 = 0;
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the UCA0 in UART mode
//!
//! This function initializes the UCA0 hardware to operate in UART mode.
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
void uca0InitUart(void)
{
    UCA0CTL1 |= UCSWRST;            // **Put state machine in reset**

    UCA0CTL0 = 0;                   // DEFAULT: UART MODE, ASYNC, 8N1	
    UCA0CTL1 |= UCSSEL_2;           // SMCLK=DCO

    // V2: Baud rate divider with 8MHz = 8MHz/115200 = ~69.4
    // V2: UCBRx = 69; In UCA0MCTL: UCBRSx = round(0.44*8) = round(3.52) = 4
    // V2: ACLK = LFXT1CLK/8, MCLK = SMCLK = DCO = CALxxx_8MHZ = 8MHz

    UCA0BR0 = 0x45;                 // V2: 8MHz / 115200
    UCA0BR1 = 0;                    // V2: 8MHz / 115200
    UCA0MCTL = UCBRS2;        	    // V2: Modulation UCBRSx = 4 (0100) (or 3????)
    UCA0CTL1 &= ~UCSWRST;           // V2: **Initialize USCI state machine**

    UCA0IE |= UCRXIE;	            // V2: Enable USCI_A0 RX interrupt
}
// -----------------------------------------------------------------------------------
//! \brief  sendMsgUartUCA0
//!
//! Send a MSG using UART UCA0
//! It is currently used to send a message when a button is pressed
//!
//! \param   Byte : value which will be sent by the UCA0
//! \return  void
// -----------------------------------------------------------------------------------
void sendMsgUartUCA0 (volatile uint8_t* msg, int qty){
  volatile int i=0;
  for(i=0;i<qty;i++){
    while (!(UCA0IFG&UCTXIFG));       // USCI_A0 TX buffer ready?
    UCA0TXBUF = msg[i];               // TX -> RXed character
  }
  while (!(UCA0IFG&UCTXIFG));         // USCI_A0 TX buffer ready?
  UCA0TXBUF = '\r';                   // TX -> RXed character
  while (!(UCA0IFG&UCTXIFG));         // USCI_A0 TX buffer ready?
  UCA0TXBUF = '\n';                   // TX -> RXed character
}
// -----------------------------------------------------------------------------------
//! \brief  USCI_A0_ISR interrupt
//!
//! Echo back RXed character, confirm TX buffer is ready first
//! characters reception by USCI_A0, when '\n' is received --> a flag is set
//!
//! \param   Byte : value which will be sent by the UCA0
//! \return  void
// -----------------------------------------------------------------------------------
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
{
  uint8_t rx_data_uart_uca0 = 0;
  switch(__even_in_range(UCA0IV,4))			// ok, max case 4
  {
  case 0:break;                             // Vector 0 - no interrupt
  case 2:                                   // Vector 2 - RXIFG
    // waits for end of line...
    rx_data_uart_uca0 = UCA0RXBUF;
    if(rx_data_uart_uca0 == '\n') rx_flag_uart_uca0 = 1;
    break;
  case 4:break;                             // Vector 4 - TXIFG
  default: break;
  }
}
// -----------------------------------------------------------------------------

