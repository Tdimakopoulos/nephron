// -----------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   io.h
//! \brief  I/O header
//!
//! Creation of the definitions, variables and prototype functions exported
//!
//! \author  Porchet J-A
//! \date    29.12.2009
//! \version 1.0
// -----------------------------------------------------------------------------------
#ifndef __IO_H__
#define __IO_H__
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "global.h"
#include <msp430.h>
// -----------------------------------------------------------------------------------
// Exported definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// MSP430F5435
// Serie 05: less energy, non used lines as LOGIC-OUTPUT = 0
// DIR: 1=OUT, 0=INP
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// PORT 1
// ---------------------------- //
#define PORT1_TESTSMCLK	0x40	// P1.6

// ---------------------------- //
// PORT 2
// ---------------------------- //
#define PORT2_INTFLAGS	0x1F	// P2[4..0]
#define KEY_1_OK        0x01    // P2.0 LOGIC-INP
// previous
// #define KEY_2_TOP       0x02    // P2.1 LOGIC-INP
// #define KEY_3_BOTTOM    0x04    // P2.2 LOGIC-INP
// #define KEY_4_RIGHT     0x08    // P2.3 LOGIC-INP
// #define KEY_5_LEFT      0x10    // P2.4 LOGIC-INP
// since 07.01.2012 (front panel reception)
#define KEY_2_TOP       0x04    // P2.2 LOGIC-INP
#define KEY_3_BOTTOM    0x02    // P2.1 LOGIC-INP
#define KEY_4_RIGHT     0x10    // P2.4 LOGIC-INP
#define KEY_5_LEFT      0x08    // P2.3 LOGIC-INP

#define PORT2_TESTACLK  0x40    // P2.5 LOGIC-OUT (CLOCK TEST 1) (usci_b0 spi-ms)

// ---------------------------- //
// PORT 3
// ---------------------------- //
// P3.3...P3.0 UCB0-SPI
// P3.5...P3.4 UCA0-UART
// P3.6 UCA1-SPI (CLK)
// P3.7 UCB1-I2C (SDA)
#define PORT3_SEL       0xFE    // P3.0 I/O
#define PORT3_DIR       0xFF    // ALL OUTPUTS
//#define PORT3_SEL     0xFF    // P3.0/USCI B0 SPI - STE
                                // P3.1/USCI B0 SPI - SIMO
                                // P3.2/USCI B0 SPI - SOMI
                                // P3.3/USCI B0 SPI - SCK
                                // P3.4/USCI A0 UART - TXD
                                // P3.5/USCI A0 UART - RXD
                                // P3.6/USCI A1 SPI - SCK
                                // P3.7/USCI B1 I2C - SDA
#define MSPARM_CS       0x01    // ACTIVE LOW
// ---------------------------- //
// PORT 4
// ---------------------------- //
#define WAKD_ONOFF      0x01	// P4.0 LOGIC-INP - ON/OFF BUTTON
#define UIF_PWR_EN      0x02	// P4.1	LOGIC-OUT - DC/DC 5V ENABLE
#define UIF_LCD_EN	0x04	// P4.2	LOGIC-OUT - DISPLAY SUPPLY ENABLE	
#define UIF_BACKL_EN	0x08	// P4.3 LOGIC-OUT - DISPLAY BACKLIGHT ENABLE	
#define UIF_LCD_nRST	0x10	// P4.4 LOGIC-OUT - DISPLAY RESET	
#define UIF_LCD_nCS1B	0x20	// P4.5 LOGIC-OUT - DISPLAY CHIP SELECT	
#define UIF_LCD_A0	0x40	// P4.6 LOGIC-OUT - DISPLAY CMD/DATA
#define PM_TEST_02      0x80    // P4.7 LOGIC-OUT (CLOCK TEST 2)

// ---------------------------- //
// PORT 5
// ---------------------------- //
// P5.0 VeREF(+)
// P5.1 VeREF(-)
// P5.4 UCB1-I2C (SCL)
// P5.6 UCA1-SPI (SIMO)
// ---------------------------- //
// PORT 6
// ---------------------------- //
// NOT USED
// ---------------------------- //
// PORT 7
// ---------------------------- //
// P7.7..P7.2 NOT USED
// ---------------------------- //
// PORT 7
// ---------------------------- //
// P7.2 NOT USED
#define K1_OUT_OK       0x08    // P7.3 // used k_ok
#define K2_OUT_TOP      0x10    // P7.4 // used k_top
#define K3_OUT_BOTTOM   0x20    // P7.5 // used k_bottom
#define K4_OUT_RIGHT    0x40    // P7.6 // used k_right
#define K5_OUT_LEFT     0x80    // P7.7 // used k_left

// ---------------------------- //
// PORT 8
// ---------------------------- //
#define UIF_BUZZER      0x01    // P8.0 UIF BUZZER: PWM
#define UIF_LED_R       0x02    // P8.1 LED R (TBD)
#define UIF_LED_G       0x04    // P8.2 LED G (TBD)
#define UIF_LED_B       0x08    // P8.3 LED B (TBD)
#define UIF_FLASHLSYNC  0x10    // P8.4 FLASHLIGHT SYNC (SEE PROETEX ALARM)
// P8.7...P8.5 NOT USED
// ---------------------------- //
// For the ARM communication with the SPI
// ---------------------------- //
#define IO_SET_J1           SET_MASK(P1OUT,BIT6)
#define IO_CLR_J1           CLR_MASK(P1OUT,BIT6)
#define IO_SET_J2           SET_MASK(P2OUT,BIT6)
#define IO_CLR_J2           CLR_MASK(P2OUT,BIT6)
#define IO_SET_J4           SET_MASK(P4OUT,BIT7)
#define IO_CLR_J4           CLR_MASK(P4OUT,BIT7)

#define IO_SET_WAKD         SET_MASK(P4OUT,BIT0)
#define IO_CLR_WAKD         CLR_MASK(P4OUT,BIT0)

// ---------------------------- //
// For the ARM communication with the SPI
// ---------------------------- //
#define MSPARM_STE_HIGH     SET_MASK(P3OUT,BIT0)    //!< CS line in high state
#define MSPARM_STE_LOW      CLR_MASK(P3OUT,BIT0)    //!< CS line in low state
#define MSPARM_RST_HIGH     SET_MASK(P8OUT,BIT6)    //!< 
#define MSPARM_RST_LOW      CLR_MASK(P8OUT,BIT6)    //!< 

// ---------------------------- //
// For the user interface
// ---------------------------- //
#define IO_PWR_EN           SET_MASK(P4OUT,BIT1)    //!< 
#define IO_PWR_DIS          CLR_MASK(P4OUT,BIT1)    //!< 
#define IO_LCD_POWER_EN     SET_MASK(P4OUT,BIT2)    //!< 
#define IO_LCD_POWER_DIS    CLR_MASK(P4OUT,BIT2)    //!< 
#define IO_BACKL_POWER_EN   SET_MASK(P4OUT,BIT3)    //!< 
#define IO_BACKL_POWER_DIS  CLR_MASK(P4OUT,BIT3)    //!< 
#define IO_UI_A0_HIGH       SET_MASK(P4OUT,BIT6)    //!< 
#define IO_UI_A0_LOW        CLR_MASK(P4OUT,BIT6)    //!< 
#define IO_UI_CS1B_HIGH     SET_MASK(P4OUT,BIT5)    //!< 
#define IO_UI_CS1B_LOW      CLR_MASK(P4OUT,BIT5)    //!< 
#define IO_UI_RST_HIGH      SET_MASK(P4OUT,BIT4)    //!< 
#define IO_UI_RST_LOW       CLR_MASK(P4OUT,BIT4)    //!< 

#define IO_BUZZER_HIGH      SET_MASK(P8OUT,BIT0)    //!< 
#define IO_BUZZER_LOW       CLR_MASK(P8OUT,BIT0)    //!< 

#define IO_SET_RED          SET_MASK(P8OUT,BIT1)    //!< 
#define IO_CLR_RED          CLR_MASK(P8OUT,BIT1)    //!< 

#define IO_SET_GREEN        SET_MASK(P8OUT,BIT2)    //!< 
#define IO_CLR_GREEN        CLR_MASK(P8OUT,BIT2)    //!< 

#define IO_SET_BLUE         SET_MASK(P8OUT,BIT3)    //!< 
#define IO_CLR_BLUE         CLR_MASK(P8OUT,BIT3)    //!< 

#define IO_FL_SYNC_HIGH     SET_MASK(P8OUT,BIT4)    //!< 
#define IO_FL_SYNC_LOW      CLR_MASK(P8OUT,BIT4)    //!< 

#define IO_UI_P64_HIGH      SET_MASK(P6OUT,BIT4)    //!< PIN 1
#define IO_UI_P64_LOW       CLR_MASK(P6OUT,BIT4)    //!< PIN 1

#define IO_UI_P63_HIGH      SET_MASK(P6OUT,BIT3)    //!< PIN 80
#define IO_UI_P63_LOW       CLR_MASK(P6OUT,BIT3)    //!< PIN 80

// -----------------------------------------------------------------------------------
// Exported macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported inline function
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Prototypes of exported functions
// -----------------------------------------------------------------------------------
extern void ioInitJ(void);
extern void ioInitG(void);
// -----------------------------------------------------------------------------------
#endif 
// -----------------------------------------------------------------------------------
// END __IO_H_
// -----------------------------------------------------------------------------------
