// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   Clock.c
//! \brief  MCU clock functions
//!
//! This file implements the initialization function of the MCU clock
//!
//! \author  Porchet J-A
//! \date    17.01.2010
//! \version 1.0
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "clock.h"


// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Initialize all internal clocks
//!
//! Initialize all internal clocks
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void clockInitJ(void)
{
    __bis_SR_register(SCG0);    // Disable the FLL control loop
    UCSCTL3 = SELREF_0;         // Set DCO FLL reference = XT1CLK
    UCSCTL5 = 0;
    UCSCTL6 = 0xC1CD;
    UCSCTL4 = SELA_0;           // Set ACLK = XT1CLK
    UCSCTL4 |= SELS_4;          // Set SMCLK = DCOCLKDIV
    UCSCTL4 |= SELM_4;          // Set MCLK = DCOCLKDIV
    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select DCO range 16MHz operation

    UCSCTL2 = FLLD_1 + 244;     // Set DCO Multiplier for 8MHz
                                // (N + 1) * FLLRef = Fdco
                                // (244 + 1) * 32768 = 8MHz
                                // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);    // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
    __delay_cycles(250000);

    // Loop until XT1,XT2 & DCO fault flag is cleared
    do
    {
        // Clear XT2,XT1,DCO fault flags
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        SFRIFG1 &= ~OFIFG;  // Clear fault flags
    }
    while(SFRIFG1&OFIFG);   // Test oscillator fault flag
}
// -----------------------------------------------------------------------------------
//! \brief  Initialize all internal clocks
//!
//! Initialize all internal clocks
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void clockInitG(void)
{
    __bis_SR_register(SCG0);    // Disable the FLL control loop

#if 0    
    UCSCTL3 |= SELREF_2;        // Set DCO FLL reference = REFO
    UCSCTL4 |= SELA_2;          // Set ACLK = REFO

    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select DCO range 16MHz operation
    UCSCTL2 = FLLD_1 + 249;     // Set DCO Multiplier for 8MHz
                                // (N + 1) * FLLRef = Fdco
                                // (249 + 1) * 32768 = 8MHz
                                // Set FLL Div = fDCOCLK/2
#else        
    UCSCTL3 = SELREF_0;         // Set DCO FLL reference = XT1CLK
    UCSCTL5 = 0;
    UCSCTL6 = 0xC1CD;
    UCSCTL4 = SELA_0;           // Set ACLK = XT1CLK
    UCSCTL4 |= SELS_4;          // Set SMCLK = DCOCLKDIV
    UCSCTL4 |= SELM_4;          // Set MCLK = DCOCLKDIV
    UCSCTL0 = 0x0000;           // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;        // Select DCO range 16MHz operation

    UCSCTL2 = FLLD_1 + 244;     // Set DCO Multiplier for 8MHz
                                // (N + 1) * FLLRef = Fdco
                                // (244 + 1) * 32768 = 8MHz
                                // Set FLL Div = fDCOCLK/2
#endif    
    __bic_SR_register(SCG0);    // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
    __delay_cycles(250000);

    // Loop until XT1,XT2 & DCO fault flag is cleared
    do
    {
        // Clear XT2,XT1,DCO fault flags
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
        SFRIFG1 &= ~OFIFG;  // Clear fault flags
    }
    while(SFRIFG1&OFIFG);   // Test oscillator fault flag
}