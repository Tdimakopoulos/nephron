// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   font_symbols.h
//! \brief  font generated with GLCD Font Creator
//!
//! collection of symbols (tbd if used or not)
//!
//! \author  DUDNIK G.S.
//! \date    20.12.2011
//! \version 1.0 First version (gdu)
// -----------------------------------------------------------------------------------
#ifndef SYMBOLS_H
#define SYMBOLS_H
// -----------------------------------------------------------------------------------
//GLCD FontName : BATTERY_8x8
//GLCD FontSize : 8 x 8
// -----------------------------------------------------------------------------------
#define SYMBOL_BATSM  1

#define BATSM_LINES           5
#define BATSM_COLS            8
#define BATSM_SYM_COLS        8*1
#define BATSM_CHARS_PER_COL   1
#define BATSM_START_X         110
#define BATSM_START_Y         20
#define BATSM_FONT_ROWS       8
// -----------------------------------------------------------------------------------
const char BATTERY_8x8[BATSM_LINES][BATSM_COLS] = {
        0x06, 0x0F, 0x09, 0x09, 0x09, 0x09, 0x0F, 0x00,
        0x06, 0x0F, 0x09, 0x09, 0x09, 0x0F, 0x0F, 0x00,  
        0x06, 0x0F, 0x09, 0x09, 0x0F, 0x0F, 0x0F, 0x00,  
        0x06, 0x0F, 0x09, 0x0F, 0x0F, 0x0F, 0x0F, 0x00,  
        0x06, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x00   
};
// -----------------------------------------------------------------------------------
//GLCD FontName : BATTERY_16x8
//GLCD FontSize : 16 x 8
// -----------------------------------------------------------------------------------
#define SYMBOL_BATBG  2

#define BATBG_LINES           5
#define BATBG_COLS            16
#define BATBG_SYM_COLS        16*1
#define BATBG_CHARS_PER_COL   1
#define BATBG_START_X         116
#define BATBG_START_Y         8+2
#define BATBG_FONT_ROWS       16

// -----------------------------------------------------------------------------------
const char BATTERY_16x8[BATBG_LINES][BATBG_COLS] = {
        0x1E, 0x1E, 0x3F, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x3F, 0x00, 0x00, 0x00, 0x00,  
        0x1E, 0x1E, 0x3F, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x3F, 0x3F, 0x3F, 0x00, 0x00, 0x00, 0x00,  
        0x1E, 0x1E, 0x3F, 0x21, 0x21, 0x21, 0x21, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x00, 0x00, 0x00, 0x00,  
        0x1E, 0x1E, 0x3F, 0x21, 0x21, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x00, 0x00, 0x00, 0x00,  
        0x1E, 0x1E, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x3F, 0x00, 0x00, 0x00, 0x00   
};
// -----------------------------------------------------------------------------------
//GLCD FontName : BT_8x12
//GLCD FontSize : 8 x 12
// -----------------------------------------------------------------------------------
#define SYMBOL_BTSM   4

#define BT_SM_SYM_COLS        8*2
#define BT_SM_CHARS_PER_COL   2
#define BT_SM_START_X         120
#define BT_SM_START_Y         32// 16
#define BT_SM_FONT_ROWS       8

// -----------------------------------------------------------------------------------
const unsigned short BT_8x12[] = {
        0x06, 0x03, 0x8C, 0x01, 0xD8, 0x00, 0xFF, 0x0F, 0x63, 0x0C, 0xF6, 0x06, 0x9C, 0x03, 0x08, 0x01   
};
// -----------------------------------------------------------------------------------
#define SYMBOL_OK             5

#define OK_SYM_COLS           13*2
#define OK_CHARS_PER_COL      2
#define OK_START_X            112
#define OK_START_Y            39
#define OK_FONT_ROWS          13

// -----------------------------------------------------------------------------------
#if 0
const unsigned short BUTTONS13x9[] = {
        0xFE, 0x00, 0x82, 0x00, 0xBA, 0x00, 0xC6, 0x00, 0xC6, 0x00, 0xBA, 0x00, 0x82, 0x00, 0xFE, 0x00, 0x92, 0x00, 0xAA, 0x00, 0xC6, 0x00, 0x82, 0x00, 0xFE, 0x00,  // Code for char 
};
#else
const unsigned short BUTTONS13x9[] = {
        0xFE, 0x00, 0x01, 0x01, 0x39, 0x01, 0x45, 0x01, 0x45, 0x01, 0x39, 0x01, 0x01, 0x01, 0x7D, 0x01, 0x11, 0x01, 0x29, 0x01, 0x45, 0x01, 0x01, 0x01, 0xFE, 0x00   // Code for char 
};
#endif

// -----------------------------------------------------------------------------------
//GLCD FontName : KEYS_8x8
//GLCD FontSize : 8 x 8
// -----------------------------------------------------------------------------------
#define SYMBOL_KEYS           6

#define KEYS_LINES            4
#define KEYS_COLS             8
#define KEYS_SYM_COLS         8*1
#define KEYS_CHARS_PER_COL    1
#define KEYS_FONT_ROWS        8

#define SYMBOL_KEYS_TOP       0
#define SYMBOL_KEYS_BOTTOM    1
#define SYMBOL_KEYS_LEFT      2
#define SYMBOL_KEYS_RIGHT     3

// -----------------------------------------------------------------------------------
const unsigned short KEYS_8x8[KEYS_LINES][KEYS_COLS] = {
        0x08, 0x0C, 0x7E, 0x7F, 0x7E, 0x0C, 0x08, 0x00,  // Code for key TOP
        0x10, 0x30, 0x7E, 0xFE, 0x7E, 0x30, 0x10, 0x00,  // Code for key BOTTOM
        0x08, 0x1C, 0x3E, 0x7F, 0x1C, 0x1C, 0x1C, 0x00,  // Code for key LEFT
        0x00, 0x1C, 0x1C, 0x1C, 0x7F, 0x3E, 0x1C, 0x08,  // Code for key RIGHT
        };

// -----------------------------------------------------------------------------------
//GLCD FontName : USB20A16x8
//GLCD FontSize : 16 x 8
// -----------------------------------------------------------------------------------
#define SYMBOL_USB20A         7		

#define USB20A_SYM_COLS       16*1
#define USB20A_CHARS_PER_COL  1
#define USB20A_START_X        116
#define USB20A_START_Y        8+2
#define USB20A_FONT_ROWS      16

// -----------------------------------------------------------------------------------
const unsigned short USB20A16x8[] = {
        0x38, 0x38, 0x38, 0x10, 0x18, 0x14, 0x12, 0x37, 0x57, 0x57, 0xD0, 0xD2, 0x17, 0x12, 0x38, 0x10   // Code for char 
};
// -----------------------------------------------------------------------------------
#endif  // __SYMBOLS_H_
// -----------------------------------------------------------------------------------
