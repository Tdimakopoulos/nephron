// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   lcd.c
//! \brief  lcd functions
//!
//! dogm132_5e lcd functionality
//!
//! \author  DUDNIK G.S.
//! \date    20.12.2011
//! \version 1.0 First version (gdu)
//! \version 2.0 Adaptation to the programming guidelines (JAP)
//! \version 2.1 Adaptation to Nephron+ (gdu)
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include "lcd.h"
#include "font.h"
#include "io.h"
#include "uca1.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
const uint8_t font_type = 2;  // 1: ArialRegular7x8, 2: Terminal6
// Display driver for DOGM132---------------------------------------------------------
#define DOGM_DISPLAY_ON		0xAF	// 10101111	LCD display ON/OFF 0:OFF, 1:ON
#define DOGM_DISPLAY_OFF	0xAE	// 10101110
#define DOGM_START_LINE_MASK	0x40	// 01aaaaaa,    aaaaaa = Display start address
					// Sets the display RAM start line address
#define	DOGM_PAGE_ADDR_MASK	0xB0	// 1011pppp,    pppp = Page address
					// Sets the display RAM page address
#define	DOGM_COL_ADDR_UP_BIT	0x10	// 0001mmmm,    mmmm = Most significant column address
#define	DOGM_COL_ADDR_LW_BIT	0x00	// 0000llll,    llll = Least significant column address
#define DOGM_ADC_SEG_NORMAL	0xA0	// 10100000     Sets the display RAM address SEG output NORMAL
#define DOGM_ADC_SEG_REVERSE	0xA1	// 10100001     Sets the display RAM address SEG output REVERSE
#define DOGM_DISPLAY_NORMAL	0xA6	// 10100110     Sets the LCD display NORMAL
#define DOGM_DISPLAY_REVERSE	0xA7	// 10100111     Sets the LCD display REVERSE
#define DOGM_DISPLAY_ALL_OFF	0xA4	// 10100100     Displays ALL points OFF (normal display)
#define DOGM_DISPLAY_ALL_ON	0xA5	// 10100100     Displays ALL points ON
#define DOGM_LCDBIAS_RATIO19	0xA2	// 10100010     Sets the LCD drive voltage bias ratio: 0=1/9 bias
#define DOGM_LCDBIAS_RATIO17	0xA3	// 10100011     Sets the LCD drive voltage bias ratio: 0=1/7 bias (ST7565R)
#define	DOGM_INTERNAL_RESET	0xE2	// 11100010	Internal Reset
#define	DOGM_COM_OUT_NORMAL	0xC0	// 11000xxx	Select COM output scan direction NORMAL
#define	DOGM_COM_OUT_REVERSE	0xC8	// 11001xxx	Select COM output scan direction REVERSE
#define DOGM_POWER_CTRL_SET	0x28	// 00101sss	Select internal power supply operating mode
#define DOGM_ON_BST_REG_FOLLOW	0x2F	// 00101111	0x28 | 0x07 = ALL ON
#define	DOGM_VOLT_RES_RATIO	0x20	// 00100rrr	Select Internal resistor ratio (Rb/Ra) mode
#define	DOGM_VOLT_RES_RATIO_1	0x21	// 00100rrr	Select RATIO 1
#define	DOGM_VOLT_RES_RATIO_2	0x22	// 00100rrr	Select RATIO 2
#define	DOGM_VOLT_RES_RATIO_4	0x24	// 00100rrr	Select RATIO 4
#define	DOGM_VOLT_RES_MAXRATIO	0x27	// 00100rrr	Select MAX Internal resistor ratio (Rb/Ra) mode
#define	DOGM_SET_CONTRAST	0x81	// 10000001     Set the V0 output VOLTAGE electronic VOLUME register
#define DOGM_INDICATOR_OFF	0xAC	// 10101100	Static Indicator OFF
#define DOGM_INDICATOR_ON	0xAD	// 10101101	Static Indicator ON
#define DOGM_INDICATOR_SET	0x01	// 00000001	Static Indicator FLASHING MODE (gdu: I guess 1 = FLASHING)
#define	DOGM_BOOST_RATIO_SET	0xF8	// 11111000	Select Booster ratio
#define	DOGM_BOOST_X2_X3_X4	0x00	// 00000000     Booster ratio x2, x3, x4
#define	DOGM_BOOST_X5		0x01	// 00000001     Booster ratio x5
#define	DOGM_BOOST_X6		0x03	// 00000011     Booster ratio x6
#define	DOGM_POWER_SAVE		0x00	// 00000000     Display OFF and display all points ON compound command
#define	DOGM_NOP		0xE3	// 11100011     Command for NON-OPERATION
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
static uint8_t DisplayFrame[DISPLAYFRAMESIZE];
static uint8_t LcdInverted = FALSE;
// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
static void CenterStringH(uint8_t *x, const uint8_t *str);
static void SendCommand(uint8_t Byte);
static void SendDisplaydata(uint8_t Byte);
static void GotoXY(uint8_t x, uint8_t y);
//static void DrawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h);
//static void FillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h);
static void DrawLine(uint8_t x, uint8_t y, int16_t len, uint8_t direction);
static void DrawLineH(uint8_t x,uint8_t y, int16_t len);
static void DrawLineV(uint8_t x,uint8_t y, int16_t len);
//static void SetPixel(uint8_t x, uint8_t y, uint8_t pixel_status);
//static void DrawString(uint8_t x, uint8_t y, uint8_t *s, uint8_t transparent_background);
static uint8_t DrawCharTransparentBG(uint8_t x, uint8_t y, uint8_t c);
static uint8_t DrawCharClearBG(uint8_t x, uint8_t y, uint8_t c);
static void DrawDataColumnClearBG(uint8_t x, uint8_t y, uint8_t data);
static void DrawDataColumnTransparentBG(uint8_t x, uint8_t y, uint8_t data);
static void DrawDataColumnTransparentBG_Inverted(uint8_t x, uint8_t y, uint8_t data);
static void DrawPixel(uint8_t x, uint8_t y);
static void ClearPixel(uint8_t x, uint8_t y);
static uint8_t DrawArialRTransparentBG(uint8_t x, uint8_t y, uint8_t c, uint8_t norm_inv);
static uint8_t DrawArialRClearBG(uint8_t x, uint8_t y, uint8_t c);
//static void DrawArialRString(uint8_t x, uint8_t y, const uint8_t *s, uint8_t transparent_background, uint8_t norm_inv);
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  SendCommand
//!         Sends a CMD to the LCD
//!
//! ...
//!
//! \param[in]      Byte
//! \return         nothing
// -----------------------------------------------------------------------------------
void SendCommand(uint8_t Byte)
{
    IO_UI_A0_LOW;
    uca1_Xmit1Byte(Byte);
}
// -----------------------------------------------------------------------------------
//! \brief  SendDisplaydata
//!         Sends data to the LCD
//!
//! ...
//!
//! \param[in]      Byte
//! \return         nothing
// -----------------------------------------------------------------------------------
void SendDisplaydata(uint8_t Byte)
{
    IO_UI_A0_HIGH;
    uca1_Xmit1Byte(Byte);
}
// -----------------------------------------------------------------------------------
//! \brief  GotoXY
//!         Sets the LCD cursor at x,y position
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \return         nothing
// -----------------------------------------------------------------------------------
void GotoXY(uint8_t x, uint8_t y)
{
    uint8_t Set[3];

    Set[0] = DOGM_PAGE_ADDR_MASK + y;
    Set[1] = DOGM_COL_ADDR_UP_BIT + (x >> 4);
    Set[2] = DOGM_COL_ADDR_LW_BIT + (x & 0x0F);

    SendCommand(Set[0]);
    SendCommand(Set[1]);
    SendCommand(Set[2]);
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_Init
//!         Initializes the LCD hardware
//!
//! ...
//!
//! \param[in]      mode
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_Init(uint8_t mode)
{

    // Enable the LCD backlight 
    IO_BACKL_POWER_EN;
    
    //Display start line
    SendCommand(DOGM_START_LINE_MASK);          // 0x40 Display start line 0

    //Bottom view
//    SendCommand(DOGM_ADC_SEG_NORMAL);		// 0xA0: ADC normal
//    SendCommand(DOGM_COM_OUT_REVERSE);	// 0xC8: Reverse COM63~COM0
    
    //Top view
    SendCommand(DOGM_ADC_SEG_REVERSE);		// 0xA1: ADC reverse
    SendCommand(DOGM_COM_OUT_NORMAL);           // 0xC0: Normal COM0~COM63
    
    //Normal / Inverted
    // normal reverse works...
    SendCommand(DOGM_DISPLAY_NORMAL);           // 0xA6: Display normal (letters blue, background white)
//    SendCommand(DOGM_DISPLAY_REVERSE);	// 0xA7: Display reverse (letters white, background blue)
    
    //Hardware options
    SendCommand(DOGM_LCDBIAS_RATIO19);		// 0xA2: Set bias 1/9 (Duty 1/65)
    SendCommand(DOGM_ON_BST_REG_FOLLOW);	// 0x2F: Booster, Regulator and Follower on
    SendCommand(DOGM_BOOST_RATIO_SET);          // 0xF8: Set internal Booster to 4x
    SendCommand(0x00);				// cont... x2x3x4
    
    //Contrast options
    SendCommand(DOGM_VOLT_RES_RATIO+3);         //  ok with contrast 0x20
    SendCommand(DOGM_SET_CONTRAST);		// 0x81: Contrast set
    SendCommand(0x20);				// 0x21(132), 0x16(128)		
    SendCommand(DOGM_INDICATOR_OFF);		// 0xAC: No indicator
    SendCommand(0x00);				// cont...

    if (mode == 0)  {
        lcd_DisplayClearOFF();
        SendCommand(DOGM_DISPLAY_ON);           // Display off
    }
    else {
        SendCommand(DOGM_DISPLAY_ON);           // Display on
        lcd_DisplayClear();
    }
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_ClearLocalFrameOFF
//!         Clears the LCD local image buffer, pixels 1
//!
//! ...
//!
//! \param[in]      none
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_ClearLocalFrameOFF(void)
{
    //clears the local RAM but don't send it
    uint16_t i;

    for(i=0; i<DISPLAYFRAMESIZE; i++)
        DisplayFrame[i] = 0xFF;
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DisplayClearOFF
//!         Clears the local RAM and send this cleared frame, pixels 1
//!
//! ...
//!
//! \param[in]      none
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DisplayClearOFF(){
        
        lcd_ClearLocalFrameOFF();
        lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_ClearLocalFrame
//!         Clears the LCD local image buffer, pixels 0
//!
//! ...
//!
//! \param[in]      none
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_ClearLocalFrame(void)
{
  uint16_t i;

    for(i=0; i<DISPLAYFRAMESIZE; i++)
        DisplayFrame[i] = 0x00;
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DisplayClear
//!         Clears the local RAM and send this cleared frame, pixels 0
//!
//! ...
//!
//! \param[in]      none
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DisplayClear(void)
{
    // Clears the local frame and send this cleared frame
    lcd_ClearLocalFrame();
    lcd_SendDisplayFrame();
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_SendDisplayFrame
//!         Sends the LCD local image buffer to the display
//!
//! ...
//!
//! \param[in]      none
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_SendDisplayFrame(void)
{
    uint8_t page,pg;
    uint8_t column;
    for(page=0; page<4; page++)
    {
        GotoXY(0,page);
        pg = page;
        for(column=0; column<132; column++)
        {
            if((pg+(column<<3))>DISPLAYFRAMESIZE)
                break;
            SendDisplaydata(DisplayFrame[pg + (column<<3)]);
        }
    }
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DrawString
//!         Draws a string str at position x, y
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      str
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DrawString(uint8_t x, uint8_t y, const uint8_t *str)
{
    DrawArialRString(x, y, str, 200,1);
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DrawStringH
//!         Draws a string str at position y, centered in x
//!
//! ...
//!
//! \param[in]      y
//! \param[in]      str
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DrawStringH(uint8_t y, const uint8_t *str)
{
    uint8_t x;

    CenterStringH(&x, str);
    DrawArialRString(x, y, str, 200,1);
}
// -----------------------------------------------------------------------------------
//! \brief  CenterStringH
//!         Centers a string str along x
//!
//! Used to center the string in the display
//!
//! \param[in]      x
//! \param[in]      str
//! \return         nothing
// -----------------------------------------------------------------------------------
void CenterStringH(uint8_t *x, const uint8_t *str)
{
    uint8_t NberChar;
    uint8_t TempU8;

    NberChar=0;
    while(*str++ != '\0')
        NberChar++;
    
    if(NberChar >= 22)
    {
        // Too many characters
        *x = 0;
    }
    else
    {
        TempU8 = DISP_WIDTH - (NberChar*6);
        *x = TempU8>>1;
    }
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DrawRect
//!         Draws a rectangle started at x,y of width w and height h
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      w
//! \param[in]      h
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DrawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    DrawLineH(x, y, w);
    DrawLineH(x, y+h-1, w);
    DrawLineV(x, y, h);
    DrawLineV(x+w-1, y, h);
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_DrawRoundedRect
//!         Draws a roundend rectangle started at x,y of width w and height h
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      w
//! \param[in]      h
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_DrawRoundedRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    DrawLineH(x+2, y, w-4);
    DrawLineH(x+2, y+h-1, w-4);
    DrawLineV(x, y+2, h-4);
    DrawLineV(x+w-1, y+2, h-4);
    DrawPixel(x+1, y+1);
    DrawPixel(x+w-2, y+1);
    DrawPixel(x+1, y+h-2);
    DrawPixel(x+w-2, y+h-2);
}
// -----------------------------------------------------------------------------------
//! \brief  lcd_FillRect
//!         Fills a roundend rectangle started at x,y of width w and height h
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      w
//! \param[in]      h
//! \return         nothing
// -----------------------------------------------------------------------------------
void lcd_FillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h)
{
    for(; h > 0; h--, y++)
        DrawLineH(x, y, w);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawLine
//!         Draws a line started at x,y of length len, of direction direction(1:v,0:h)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      len
//! \param[in]      direction
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawLine(uint8_t x, uint8_t y, int16_t len, uint8_t direction)
{
    // direction: 0 = horizontal,
    //            1 = vertical

    if(direction == 0)
    {
        if(len>0)
        {
            for(; len>0; len--)
            {
                SetPixel(x, y, 1);
                x++;
            }
        }
        else
        {
            for(; len<0; len++)
            {
                SetPixel(x, y, 1);
                x--;
            }
        }
    }
    else
    {
        if(len>0)
        {
            for(; len>0; len--)
            {
                SetPixel(x, y, 1);
                y++;
            }
        }
        else
        {
            for (; len<0; len++)
            {
                SetPixel(x, y, 1);
                y--;
            }
        }
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawLineH
//!         Draws an horizontal line started at x,y of length len
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      len
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawLineH(uint8_t x,uint8_t y, int16_t len)
{
    DrawLine(x,y,len,0);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawLineV
//!         Draws a vertical line started at x,y of length len
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      len
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawLineV(uint8_t x,uint8_t y, int16_t len)
{
    DrawLine(x,y,len,1);
}
// -----------------------------------------------------------------------------------
//! \brief  SetPixel
//!         sets a pixel at x,y with status 1(ON), 0(OFF)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      pixel_status
//! \return         nothing
// -----------------------------------------------------------------------------------
void SetPixel(uint8_t x, uint8_t y, uint8_t pixel_status)
{
    if((x<DISP_WIDTH) && (y<DISP_HEIGHT))
    {
        if(pixel_status != 0)
            DisplayFrame[(y>>3) + (x<<3)] |=  (1<<(y & 0x07));
        else
            DisplayFrame[(y>>3) + (x<<3)] &= ~(1<<(y & 0x07));
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawString
//!         Draws the string s at x,y with transparent or clear background
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      s
//! \param[in]      transparent_background
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawString(uint8_t x, uint8_t y, uint8_t *s, uint8_t transparent_background)
{
    if(transparent_background)
    {
        while(*s != '\0')
        {
            x += DrawCharTransparentBG(x, y, *s)+1;
            s++;
        }
    }
    else
    {
        while(*s != '\0')
        {
            x += DrawCharClearBG(x, y, *s)+1;
            s++;
        }
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawCharTransparentBG
//!         Draws a character c at x,y with the Standard font (transparent background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \return         nothing
// -----------------------------------------------------------------------------------
uint8_t DrawCharTransparentBG(uint8_t x, uint8_t y, uint8_t c)
{
    uint8_t w = 0; //width of drawn character (return value)
    uint8_t data = 0x00;
    const uint8_t *ptr = font_Standard[c-0x20];
    
    while((data != 0xAA) && (w++<7))
    {
        data = ptr[0];
        if(data != 0xAA)
            DrawDataColumnTransparentBG(x++, y, data);
        else
            w--;
        ptr++;
    }

    if(w>7)
        w=7;

    return (w);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawCharClearBG
//!         Draws a character c at x,y with the Standard font (clear background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \return         nothing
// -----------------------------------------------------------------------------------
uint8_t DrawCharClearBG(uint8_t x, uint8_t y, uint8_t c)
{
    uint8_t w = 0; //width of drawn character (return value)
    uint8_t data = 0x00;
    const uint8_t *ptr = font_Standard[c-0x20];
    
    while((data != 0xAA) && (w++ < 7))
    {
        data = ptr[0];
        if (data != 0xAA)
            DrawDataColumnClearBG(x++, y, data);
        else
            w--;
        ptr++;
    }

    if(w>7)
        w = 7;
    return (w);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawDataColumnTransparentBG
//!         Draws a data column data at x,y (clear background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      data
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawDataColumnTransparentBG(uint8_t x, uint8_t y, uint8_t data)
{
    uint8_t row;

    for(row=0; row<8; row++)
    {
        if ((data & (1<<row)) != 0)
            DrawPixel(x, y);
        y++;
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawDataColumnTransparentBG_Inverted
//!         Draws a data column data at x,y (inverted background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      data
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawDataColumnTransparentBG_Inverted(uint8_t x, uint8_t y, uint8_t data)
{
    unsigned char row;

    for(row=0; row<8; row++)
    {
        if((data & (1<<row)) != 0)
            ClearPixel(x, y);
        y++;
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawDataColumnClearBG
//!         Draws a data column data at x,y (clear background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      data
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawDataColumnClearBG(uint8_t x, uint8_t y, uint8_t data)
{
    uint8_t row;

    for(row=0; row<8; row++)
    {
        if((data & (1<<row)) != 0)
            DrawPixel(x, y);
        else
            ClearPixel(x, y);
        y++;
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawPixel
//!         Draws a pixel at x,y (mode: 1-LcdInverted)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      data
//! \return         nothing
// -----------------------------------------------------------------------------------
void DrawPixel(uint8_t x, uint8_t y)
{
    SetPixel(x, y, 1 - LcdInverted);
}
// -----------------------------------------------------------------------------------
//! \brief  ClearPixel
//!         Draws a pixel at x,y (mode: 0+LcdInverted)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      data
//! \return         nothing
// -----------------------------------------------------------------------------------
void ClearPixel(uint8_t x, uint8_t y)
{
    SetPixel(x, y, 0 + LcdInverted);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawArialRTransparentBG
//!         Draws a character c at x,y with Terminal6Patterns font (normal/inverted)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \param[in]      norm_inv
//! \return         w
// -----------------------------------------------------------------------------------
uint8_t DrawArialRTransparentBG(uint8_t x, uint8_t y, uint8_t c, uint8_t norm_inv)
{
    uint8_t w = 0; //width of drawn character (return value)
    uint8_t data = 0x00;
    const uint8_t *ptr = Terminal6Patterns[c-0x20];
  
    while((data != 0xAA) && (w++ < 7))
    {
        data = ptr[0];
        if(data != 0xAA)
        {
            if(norm_inv > 0)
                DrawDataColumnTransparentBG(x++, y, data);
            else
                DrawDataColumnTransparentBG_Inverted(x++, y, data);
        }
        else
        {
            w--;
        }

        ptr++;
    }

    if(w > 7)
        w = 7;

    return (w);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawArialRTransparentBG
//!         Draws a character c at x,y with font_Arial6 font (clear background)
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \return         w
// -----------------------------------------------------------------------------------
uint8_t DrawArialRClearBG(uint8_t x, uint8_t y, uint8_t c)
{
    uint8_t w = 0; //width of drawn character (return value)
    uint8_t data = 0x00;
    const uint8_t *ptr = font_Arial6[c-0x20];

    while (data != 0xAA && w++ < 7)
    {
        data = ptr[0];
        if (data != 0xAA)
        {
            DrawDataColumnClearBG(x++, y, data);
        }
        else
        {
            w--;
        }

        ptr++;
    }

    if(w > 7)
        w = 7;
    
    return (w);
}
// -----------------------------------------------------------------------------------
//! \brief  DrawArialRString
//!         Draws a string s at x,y transparent/clear background, normal/inverted font
//!
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \param[in]      norm_inv
//! \return         w
// -----------------------------------------------------------------------------------
void DrawArialRString(uint8_t x, uint8_t y, const uint8_t *s, uint8_t transparent_background, uint8_t norm_inv)
{
    if(transparent_background)
    {
        while(*s)
        {
            x += DrawArialRTransparentBG(x, y, *s, norm_inv)-1;
            s++;
        }
    }
    else
    {
        while (*s)
        {
            x += DrawArialRClearBG(x, y, *s)-1;
            s++;
        }
    }
}
// -----------------------------------------------------------------------------------
//! \brief  DrawArialRStringW
//!         Draws a string s at x,y transparent/clear background, normal/inverted font
//!         with more distance between characters
//! ...
//!
//! \param[in]      x
//! \param[in]      y
//! \param[in]      c
//! \param[in]      norm_inv
//! \return         w
// -----------------------------------------------------------------------------------
void DrawArialRStringW(uint8_t x, uint8_t y, const uint8_t *s, uint8_t transparent_background, uint8_t norm_inv)
{
    if(transparent_background)
    {
        while(*s)
        {
            x += DrawArialRTransparentBG(x, y, *s, norm_inv)-1+1;
            s++;
        }
    }
    else
    {
        while (*s)
        {
            x += DrawArialRClearBG(x, y, *s)-1+1;
            s++;
        }
    }
}
// -----------------------------------------------------------------------------------
