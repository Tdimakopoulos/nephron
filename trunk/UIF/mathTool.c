// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   mathtool.c
//! \brief  math and data convertion utilities
//!
//! ....
//! ....
//!
//!
//! \author  Dudnik G.
//! \date    22.10.2010
//! \version 1.0
//! \remarks
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  decimal_to_char
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t decimal_to_char(uint8_t _charn){
 	return (uint8_t)(_charn + 48);
}
// -----------------------------------------------------------------------------------
//! \brief  decword_to_charstr
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t decword_to_charstr(short numbers, uint8_t *charstr, uint8_t charqty, short multiplier){
      uint8_t ix=0;
      short decaux = 0;
      uint8_t zeroflag = 0;
      uint8_t trueqty = 0;
      for(ix=0;ix<charqty;ix++) charstr[ix]=0;
      decaux = numbers;
      for(ix=0;ix<(charqty-1);ix++){
              charstr[ix] = (uint8_t)(decaux/multiplier);
              if(charstr[ix]>0) {
                      if((decaux - (charstr[ix]*multiplier))>=0)decaux = decaux - (charstr[ix]*multiplier);
                      else decaux = 0;
                      zeroflag = 1;
              }
              if((charstr[ix]==0) && (zeroflag==0)&& (ix<(charqty-2))) 	
                      charstr[ix] = ' ';
              else 					
                    charstr[ix] = decimal_to_char(charstr[ix]);
              multiplier/=10;		
      }
      charstr[charqty-1] = ' ';
      trueqty = charqty;
      return trueqty;
}
// -----------------------------------------------------------------------------------
//! \brief  decword_to_indexstr
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t decword_to_indexstr(short numbers, uint8_t *charstr, uint8_t charqty, short multiplier){
      uint8_t ix=0;
      short decaux = 0;
      uint8_t zeroflag = 0;
      uint8_t trueqty = 0;
      //short multiplier = 10000;
      for(ix=0;ix<charqty;ix++) charstr[ix]=0;
      decaux = numbers;
      for(ix=0;ix<(charqty-1);ix++){
              charstr[ix] = (uint8_t)(decaux/multiplier);
              if(charstr[ix]>0) {
                      if((decaux - (charstr[ix]*multiplier))>=0)decaux = decaux - (charstr[ix]*multiplier);
                      else decaux = 0;
                      zeroflag = 1;
              }
              if((charstr[ix]==0) && (zeroflag==0)&& (ix<(charqty-2))) 	
                      charstr[ix] = 10; // index to ' ';
              multiplier/=10;		
      }
      charstr[charqty-1] = 10; // index to ' ';
      trueqty = charqty;
      return trueqty;
}
// -----------------------------------------------------------------------------------
//! \brief  ascii_to_char
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t ascii_to_char(uint8_t _d0){
  uint8_t _d0hexa = 0;
  if(_d0 >= 48 && _d0 <= 57){         // 0 ... 9
    _d0hexa = _d0 - 48;
  }
  else if(_d0 >= 65 && _d0 <= 70){    // A ... F
    _d0hexa = _d0 - 55;
  }
  else if(_d0>=97 && _d0 <=102){      // a ... f
    _d0hexa = _d0 - 87;
  }
   return _d0hexa;
}
// -----------------------------------------------------------------------------------
//! \brief  ascii2_to_char
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t asciix2_to_char(uint8_t _dh, uint8_t _dl){
  uint8_t _d0res = 0;
  _d0res = ascii_to_char(_dh)*16 + ascii_to_char(_dl);
  return _d0res;
}
// -----------------------------------------------------------------------------------
//! \brief  calculate_lrc
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
uint8_t calculate_lrc(uint8_t *chain, int max_index){
  uint8_t lrc_buffer;
  int i;
  lrc_buffer = 0;
  for(i=1; i< max_index; i=i+2){
    lrc_buffer += asciix2_to_char(chain[i], chain[i+1]);
  }
  lrc_buffer = 0xFF - (lrc_buffer&0xFF);
  lrc_buffer += 1;
  return lrc_buffer;
}
// -----------------------------------------------------------------------------------
//! \brief  char_to_ascii
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
// mode=1 higher nibble, mode=0 lower nibble
uint8_t char_to_ascii(uint8_t _d0, int mode){
  uint8_t _result = 0;
  uint8_t _ascii = 0;
  if(mode == 1)       _ascii = (_d0 & 0xF0)/16;
  else if (mode == 0) _ascii = (_d0 & 0x0F);
  if(_ascii <=9) _result = _ascii + 48;
  else _result = _ascii + 55;
  return _result;
}
// -----------------------------------------------------------------------------------
//! \brief  int_to_ascii
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void int_to_ascii(unsigned int data, uint8_t* dout){
  int i = 0;
  uint8_t dh = (data & 0xFF00)>>8;
  uint8_t dl = data & 0x00FF;
  for(i=0;i<5;i++) dout[i] = 0;
  dout[0] = char_to_ascii(dh, 1);
  dout[1] = char_to_ascii(dh, 0);
  dout[2] = char_to_ascii(dl, 1);
  dout[3] = char_to_ascii(dl, 0);
}
// -----------------------------------------------------------------------------------
//! \brief  byte_to_ascii
//!         ...
//!
//! Complete description of the function
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void byte_to_ascii(uint8_t data, uint8_t* dout){
  int i = 0;
  for(i=0;i<3;i++) dout[i] = 0;
  dout[0] = char_to_ascii(data, 1);
  dout[1] = char_to_ascii(data, 0);
}

// -----------------------------------------------------------------------------------
//! \brief  div_c_float
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
float div_c_float (int x, float y){
    if(y!=0)  return (x/y);
    else return 0;
}
// -----------------------------------------------------------------------------------
// END mathtool.c
// -----------------------------------------------------------------------------------
