// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifMbProtocol.c
//! \brief  Derivated from PM (ltms3) where PM is Master, UIF will be Slave
//!
//! Data Exchange routines between UIF and MB
//!
//! \author  DUDNIK G.S.
//! \date    27.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "global.h"
#include "io.h"
#include "uifMbProtocol.h"
#include "uifApplication.h"
#include "clock.h" /* for DELAY_MS() */
#include "lcdnephronplus.h" /* for DisplayAlarm() */
// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
// UIF SLAVE implementation
uint16_t SlaveRcv = 0xAA;
uint8_t pmdl_counter = 0;                     // counter of data being received
uint8_t pmdl_cmd_rx = MESSAGE_NEW;
uint8_t pmdl_command = MB_UIF_CMD_FORBIDDEN;  // current command
uint8_t pmdl_cmd_end = 0;                     // flag, 1 if full cmd received
uint8_t pmdl_command_len = 0;                 // qty of cmd to receive
uint8_t pmdl_datacounter = 0;                 // counter of data being sent
uint8_t pmdl_data_tosend = 0;                 // flag, 1 if there is data to send
uint8_t pmdl_data_len = 0;                    // qty of data to send
uint8_t pmdl_full_received = 0;
//uint8_t pmdl_rx_buffer[MB_UIF_MAXLEN];        // reception buffer
uint8_t pmdl_tx_buffer[MB_UIF_TXLEN];         // transmission buffer

uint8_t mbuif_pbuffer[MB_UIF_MAXLEN];
uint8_t index = 0;
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief MB_UIF_SetSPIVariables_TestLink
//!
//! Reads the data received and prepares the transmission buffer to send data
//! 
//! Inputs : ....
//! 
//! Outputs : ....
// -----------------------------------------------------------------------------------
void MB_UIF_SetSPIVariables_TestLink(){
    pmdl_data_tosend = 1;
    pmdl_datacounter = 0;
    pmdl_data_len = MB_UIF_QTY_ANSWER_2;
    pmdl_tx_buffer[0] = MB_UIF_CMD_ACKNOWLEDGE;
    SET16(&pmdl_tx_buffer[0], 1, MB_WAKD_UIF_LinkTest);
}
// -----------------------------------------------------------------------------------
//! \brief  GET16
//!
//! get 32-bit value from buffer
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint16_t GET16(uint8_t *rxbuffer, uint8_t rxindex){
  uint16_t val16 = rxbuffer[rxindex+0];
  uint16_t value = val16 << 8;
  value += rxbuffer[rxindex+1];
  return value;
}
// -----------------------------------------------------------------------------------
//! \brief  GET32
//!
//! get 32-bit value from buffer
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
uint32_t GET32(uint8_t *rxbuffer, uint8_t rxindex){
  uint32_t value = 0;
  uint32_t val32 = rxbuffer[rxindex+0];
  val32 = val32 << 24;
  value += val32;
  val32 = rxbuffer[rxindex+1];
  val32 = val32 << 16;
  value += val32;
  value += rxbuffer[rxindex+2] << 8;
  value += rxbuffer[rxindex+3];
  return value;
}
// -----------------------------------------------------------------------------------
//! \brief  SET32
//!
//! set a 32-bit value to buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SET32(uint8_t *txbuffer, uint32_t txindex, uint32_t data_out){
    txbuffer[txindex+0] = (uint8_t)(data_out>>24);
    txbuffer[txindex+1] = (uint8_t)(data_out>>16);
    txbuffer[txindex+2] = (uint8_t)(data_out>>8);
    txbuffer[txindex+3] = (uint8_t)(data_out);
}
// -----------------------------------------------------------------------------------
//! \brief  SET16
//!
//! set a 16-bit value to buffer
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void SET16(uint8_t *txbuffer, uint32_t txindex, uint16_t data_out){
    txbuffer[txindex+0] = (uint8_t)(data_out>>8);
    txbuffer[txindex+1] = (uint8_t)(data_out);
}
// -----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------
//! \brief  uif_ApplicationInitData
//!         ...
//!
//! ....
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void uif_ApplicationInitData(){
    
    // Alarms
    WAKD_FLASHLIGHT_ONOFF = FLASHLIGHT_ON; // FLASHLIGHT_OFF;
    WAKD_BUZZER_ONOFF = BUZZER_OFF;
    WAKD_LED_BLUE_CONTROL = LEDS_TOGGLE;
    WAKD_LED_GREEN_CONTROL = LEDS_TOGGLE;
    WAKD_LED_RED_CONTROL = LEDS_TOGGLE;
    WAKD_UIF_IO_TEST = IOTEST_ON;
  
    // ----------------------------------
    // Load Dummy Data in
    // PM_DL exchange set of variables
    // Clear Variables, etc
    // ----------------------------------
    PM_DL_ClearExchangeData();
    // ----------------------------------
    // usci_b0
    // ----------------------------------
    usci_b0_ClearData();
}

// -----------------------------------------------------------------------------------------
//! \brief  PM_DL_ClearExchangeData
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
void PM_DL_ClearExchangeData(void){

    // Battery Pack 1
    MB_BPACK_1_Percent = 0x00;
    MB_BPACK_1_RemainingCapacity = 0x0000;
    // Battery Pack 2
    MB_BPACK_2_Percent = 0x00;
    MB_BPACK_2_RemainingCapacity = 0x0000;
    // add the others...
    // ...
}


// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_VBAT1_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_VBAT1_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_BPACK_1_Percent =  rx_spi_data[2]; // GET16(rx_spi_data, 1); 
    spi_bp1_voltage_ok = TRUE;
    MB_BPACK_1_RemainingCapacity = GET16(rx_spi_data, 3); 
    spi_bp1_remainingcapacity_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_VBAT2_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_VBAT2_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_BPACK_2_Percent=  rx_spi_data[2]; // GET16(rx_spi_data, 1); 
    spi_bp2_voltage_ok = TRUE;
    MB_BPACK_2_RemainingCapacity = GET16(rx_spi_data, 3); 
    spi_bp2_remainingcapacity_ok = TRUE;
    return mb_uif_answer;
}

// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_STATUS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_STATUS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint32_t wakd_stat = 0;
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_Status =  GET32(rx_spi_data, 1); 
    
    wakd_stat = MB_WAKD_Status&WAKD_READY_TO_WORK_SET;
    if(wakd_stat!=0)  WAKD_FLAG_READY_TO_WORK = 1;
    else              WAKD_FLAG_READY_TO_WORK = 0;
     
    spi_wakd_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_ATTITUDE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_ATTITUDE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_Attitude =  GET16(rx_spi_data, 1); 
    spi_wakd_attitude_ok = TRUE;
    return mb_uif_answer;
}

// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_COMMLINK
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_COMMLINK(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_CLINKS_Status = GET16(rx_spi_data, 1); 
    spi_clinks_status_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_OPCODE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_OPCODE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_OperatingMode = GET16(rx_spi_data, 1); 
    spi_wakd_operatingmode_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_BLCIRCUIT_STATUS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_BLCIRCUIT_STATUS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_BLCircuit_Status = GET32(rx_spi_data, 1); 
    spi_wakd_blcircuit_status_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_FLCIRCUIT_STATUS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_FLCIRCUIT_STATUS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_FLCircuit_Status = GET32(rx_spi_data, 1); 
    spi_wakd_flcircuit_status_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_BLPUMP_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_BLPUMP_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
#if 0    
    MB_WAKD_BLPump_Speed = GET16(rx_spi_data, 1);      
    MB_WAKD_BLPump_DirStatus = GET16(rx_spi_data, 3);  
#else   
    MB_WAKD_BLPump_SpeedCode = rx_spi_data[2];
#endif    
    spi_wakd_blpump_info_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_FLPUMP_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_FLPUMP_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
#if 0    
    MB_WAKD_FLPump_Speed = GET16(rx_spi_data, 1);      
    MB_WAKD_FLPump_DirStatus = GET16(rx_spi_data, 3);  
#else
    MB_WAKD_FLPump_SpeedCode = rx_spi_data[2];
#endif    
    spi_wakd_flpump_info_ok = TRUE;
    return mb_uif_answer;
 }
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_BLTEMPERATURE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_BLTEMPERATURE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_BTS_InletTemperature = GET16(rx_spi_data, 1);        
    MB_WAKD_BTS_OutletTemperature = GET16(rx_spi_data, 3);  
    spi_wakd_bts_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_BLCIRCUIT_PRESSURE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_BLCIRCUIT_PRESSURE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_BPS_Pressure = GET16(rx_spi_data, 1);        
    spi_wakd_bps_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_FLCIRCUIT_PRESSURE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_FLCIRCUIT_PRESSURE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_FPS_Pressure = GET16(rx_spi_data, 1);        
    spi_wakd_fps_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_FLCONDUCTIVITY
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_FLCONDUCTIVITY(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_DCS_Conductance = GET16(rx_spi_data, 1);        
    MB_WAKD_DCS_Susceptance = GET16(rx_spi_data, 3);  
    spi_wakd_dcs_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_HFD_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_HFD_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_HFD_Status = rx_spi_data[1]; 
    MB_WAKD_HFD_PercentOK = rx_spi_data[2]; 
    spi_wakd_hfd_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_SU_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_SU_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_SU_Status = GET16(rx_spi_data, 1); 
    spi_wakd_su_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_POLAR_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_POLAR_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_POLAR_Voltage = GET16(rx_spi_data, 1);        
    MB_WAKD_POLAR_Status = GET16(rx_spi_data, 3);  
    spi_wakd_polar_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_ECP1_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_ECPS_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_ECP1_Status = GET16(rx_spi_data, 1); 
    MB_WAKD_ECP2_Status = GET16(rx_spi_data, 3); 
    spi_wakd_ecps_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_PS_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_PS_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_PS_Status = GET32(rx_spi_data, 1); 
    spi_wakd_ps_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_ACT_INFO
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_ACT_INFO(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_WAKD_ACT_Status = GET32(rx_spi_data, 1); 
    spi_wakd_act_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_CMD_EXECUTED
//!
//! ....
//! This routine is the only one that does not update data for visualization
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_CMD_EXECUTED(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_CMDS_Register = GET16(rx_spi_data, 1); 
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_ALARMS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_ALARMS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint32_t tmp = GET32(rx_spi_data, 1);
    for(uint8_t i = 0; i < 4; i++) {
      MB_WAKD_ALARMS_Status[i] = (uint8_t) (tmp & 0xFF);
      tmp >>= 8;
    }
    spi_wakd_alarms_status_ok = TRUE;
   // uint8_t alarm = MB_WAKD_ALARMS_Status & 0x000000FF;
    // Display the alarm
   // DisplayAlarm(alarm);
    // Disable all the buttons
//    P2IES = 0x00;
//    P2IFG = 0x00;
//    P2IE = 0x00;
//    // Wait 5 secs
//    DELAY_MS(5000);
//    // Enable the ok button
//    P2IFG = 0x00;
//    P2IES = KEY_1_OK;
//    P2IE = KEY_1_OK;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_WAKD_ERRORS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_WAKD_ERRORS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint32_t tmp = GET32(rx_spi_data, 1);
    for(uint8_t i = 0; i < 4; i++) {
      MB_WAKD_ERRORS_Status[i] = (uint8_t) (tmp & 0xFF);
      tmp >>= 8;
    }
    spi_wakd_errors_status_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_FIRSTNAME
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_INITIALS(uint8_t *rx_spi_data, uint8_t rx_spi_len){    
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint8_t x = 0;
    for(x=0; x< 4;x++) MB_P_DATA_INITIALS[x] = rx_spi_data[x+1];  // ok
    spi_wakd_pdata_firstname_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_LASTNAME
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_PATIENTCODE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint8_t x = 0;
    for(x=0; x< 4;x++) MB_P_DATA_PATIENTCODE[x] = rx_spi_data[x+1];   // ok
    spi_wakd_pdata_lastname_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_GENDERAGE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_GENDERAGE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_GENDER[0] = rx_spi_data[1];                           // ok
    MB_P_DATA_AGE = rx_spi_data[2];                                 // ok
    spi_wakd_pdata_genderage_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_WEIGHT
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_WEIGHT(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_WEIGHT = GET16(rx_spi_data, 1);                       // ok
    spi_wakd_pdata_weight_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
#if 0
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_HEARTRATE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_HEARTRATE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_HEARTRATE = rx_spi_data[1];                           // ok
    spi_wakd_pdata_hr_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_BREATHRATE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_BREATHRATE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_BREATHINGRATE = rx_spi_data[1];                       // ok
    spi_wakd_pdata_br_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_ACTIVITY
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_ACTIVITY(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_ACTIVITYCODE = rx_spi_data[1];                        // ok
    spi_wakd_pdata_activity_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_SEWALL
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_SEWALL(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_HEARTRATE = rx_spi_data[1];                           // ok
    spi_wakd_pdata_hr_ok = TRUE;
    MB_P_DATA_BREATHINGRATE = rx_spi_data[2];                       // ok
    spi_wakd_pdata_br_ok = TRUE;
    MB_P_DATA_ACTIVITYCODE = rx_spi_data[3];                        // ok
    spi_wakd_pdata_activity_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_BLPRESSURE
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_BLPRESSURE(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    //...
    MB_P_DATA_SISTOLICBP = GET16(rx_spi_data, 1);
    MB_P_DATA_DIASTOLICBP = GET16(rx_spi_data, 3);
    spi_wakd_pdata_bloodpressure_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_ECP1_A
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_ECP1_A(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_NA_ECP1 = GET16(rx_spi_data, 1);                      // ok
    MB_P_DATA_K_ECP1 = GET16(rx_spi_data, 3);                       // ok
    spi_wakd_ecp1_a_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_ECP1_B
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_ECP1_B(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_PH_ECP1 = GET16(rx_spi_data, 1);                      // ok 
    MB_P_DATA_UREA_ECP1 = GET16(rx_spi_data, 3);                    // ok
    spi_wakd_ecp1_b_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_ECP2_A
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_ECP2_A(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_NA_ECP2 = GET16(rx_spi_data, 1);                      // ok
    MB_P_DATA_K_ECP2 = GET16(rx_spi_data, 3);                       // ok
    spi_wakd_ecp2_a_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_PDATA_ECP2_B
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_PDATA_ECP2_B(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    MB_P_DATA_PH_ECP2 = GET16(rx_spi_data, 1);                      // ok
    MB_P_DATA_UREA_ECP2 = GET16(rx_spi_data, 3);                    // ok
    spi_wakd_ecp2_b_data_ok = TRUE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  ANALYZE_TEST_LINK
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t ANALYZE_TEST_LINK(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint32_t test_link_rx = GET32(rx_spi_data, 1);
    if(test_link_rx != TEST_LINK_OK) mb_uif_answer = MB_UIF_CMD_NOT_ACKNOWLEDGE;
    //if(test_link_rx != TEST_LINK_KO) mb_uif_answer = MB_UIF_CMD_NOT_ACKNOWLEDGE;
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
//! \brief  MB_UIF_CMD_GET_PROCESS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t MB_UIF_CMD_GET_PROCESS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint8_t mb_uif_cmd = rx_spi_data[0];
    switch(mb_uif_cmd){
    default:  // UNKNOWN COMMAND
            mb_uif_answer = MB_UIF_CMD_NOT_ACKNOWLEDGE;
        break;
    }
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
#if 0
// -----------------------------------------------------------------------------------------
//! \brief  MB_UIF_CMD_SET_PROCESS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t MB_UIF_CMD_SET_PROCESS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint8_t mb_uif_cmd = rx_spi_data[0];
    switch(mb_uif_cmd){
        case MB_UIF_CMD_TEST_LINK:
            mb_uif_answer = ANALYZE_TEST_LINK(rx_spi_data, rx_spi_len);
        break;
    case MB_UIF_CMD_WAKD_BLTEMPERATURE:
            mb_uif_answer = ANALYZE_WAKD_BLTEMPERATURE(rx_spi_data, rx_spi_len);
        break;
    default:  // UNKNOWN COMMAND
            mb_uif_answer = MB_UIF_CMD_NOT_ACKNOWLEDGE;
        break;
    }
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
#else
// -----------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------
//! \brief  MB_UIF_CMD_SET_PROCESS
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------------
uint8_t MB_UIF_CMD_SET_PROCESS(uint8_t *rx_spi_data, uint8_t rx_spi_len){
    uint8_t mb_uif_answer = MB_UIF_CMD_ACKNOWLEDGE;
    uint8_t mb_uif_cmd = rx_spi_data[0];
    switch(mb_uif_cmd){
        case MB_UIF_CMD_TEST_LINK:
            mb_uif_answer = ANALYZE_TEST_LINK(rx_spi_data, rx_spi_len);
        break;
    case MB_UIF_CMD_VBAT1_INFO:
          mb_uif_answer = ANALYZE_WAKD_VBAT1_INFO(rx_spi_data, rx_spi_len);            
          break;
    case MB_UIF_CMD_VBAT2_INFO:
          mb_uif_answer = ANALYZE_WAKD_VBAT2_INFO(rx_spi_data, rx_spi_len);            
          break;
    case MB_UIF_CMD_WAKD_STATUS:
          mb_uif_answer = ANALYZE_WAKD_STATUS(rx_spi_data, rx_spi_len);           
          break;
    case MB_UIF_CMD_WAKD_ATTITUDE:
          mb_uif_answer = ANALYZE_WAKD_ATTITUDE(rx_spi_data, rx_spi_len);           
          break;
    case MB_UIF_CMD_WAKD_COMMLINK:
          mb_uif_answer = ANALYZE_WAKD_COMMLINK(rx_spi_data, rx_spi_len);           
          break;
    case MB_UIF_CMD_WAKD_OPMODE:
          mb_uif_answer = ANALYZE_WAKD_OPCODE(rx_spi_data, rx_spi_len);                
          break;
    case MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS:
          mb_uif_answer = ANALYZE_WAKD_BLCIRCUIT_STATUS(rx_spi_data, rx_spi_len);                
          break;
    case MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS:
          mb_uif_answer = ANALYZE_WAKD_FLCIRCUIT_STATUS(rx_spi_data, rx_spi_len);                
          break;
    case MB_UIF_CMD_WAKD_BLPUMP_INFO:
          mb_uif_answer = ANALYZE_WAKD_BLPUMP_INFO(rx_spi_data, rx_spi_len);                     
          break;
    case MB_UIF_CMD_WAKD_FLPUMP_INFO:
          mb_uif_answer = ANALYZE_WAKD_FLPUMP_INFO(rx_spi_data, rx_spi_len);                     
          break;
    case MB_UIF_CMD_WAKD_BLTEMPERATURE:
        mb_uif_answer = ANALYZE_WAKD_BLTEMPERATURE(rx_spi_data, rx_spi_len);            
        break;
    case MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE:
          mb_uif_answer = ANALYZE_WAKD_BLCIRCUIT_PRESSURE(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE:
          mb_uif_answer = ANALYZE_WAKD_FLCIRCUIT_PRESSURE(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_FLCONDUCTIVITY:
          mb_uif_answer = ANALYZE_WAKD_FLCONDUCTIVITY(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_HFD_INFO:
          mb_uif_answer = ANALYZE_WAKD_HFD_INFO(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_SU_INFO:
          mb_uif_answer = ANALYZE_WAKD_SU_INFO(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_POLAR_INFO:
          mb_uif_answer = ANALYZE_WAKD_POLAR_INFO(rx_spi_data, rx_spi_len);                          
          break;
    case MB_UIF_CMD_WAKD_ECPS_INFO:
          mb_uif_answer = ANALYZE_WAKD_ECPS_INFO(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_WAKD_PS_INFO:
          mb_uif_answer = ANALYZE_WAKD_PS_INFO(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_WAKD_ACT_INFO:
          mb_uif_answer = ANALYZE_WAKD_ACT_INFO(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_WAKD_ACK_COMMANDS:
          break;
    case MB_UIF_CMD_ALARMS:
          mb_uif_answer = ANALYZE_WAKD_ALARMS(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_ERRORS:
          mb_uif_answer = ANALYZE_WAKD_ERRORS(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_INITIALS:
          mb_uif_answer = ANALYZE_PDATA_INITIALS(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_PATIENTCODE:
          mb_uif_answer = ANALYZE_PDATA_PATIENTCODE(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_GENDERAGE:
          mb_uif_answer = ANALYZE_PDATA_GENDERAGE(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_WEIGHT:
          mb_uif_answer = ANALYZE_PDATA_WEIGHT(rx_spi_data, rx_spi_len);                               
          break;
#if 0          
    case MB_UIF_CMD_PDATA_HEARTRATE:
          mb_uif_answer = ANALYZE_PDATA_HEARTRATE(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_BREATHRATE:
          mb_uif_answer = ANALYZE_PDATA_BREATHRATE(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_ACTIVITY:
          mb_uif_answer = ANALYZE_PDATA_ACTIVITY(rx_spi_data, rx_spi_len);                               
          break;
#endif          
    case MB_UIF_CMD_PDATA_SEWALL:
          mb_uif_answer = ANALYZE_PDATA_SEWALL(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_BLPRESSURE:
          mb_uif_answer = ANALYZE_PDATA_BLPRESSURE(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_ECP1_A:
          mb_uif_answer = ANALYZE_PDATA_ECP1_A(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_ECP1_B:
          mb_uif_answer = ANALYZE_PDATA_ECP1_B(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_ECP2_A:
          mb_uif_answer = ANALYZE_PDATA_ECP2_A(rx_spi_data, rx_spi_len);                               
          break;
    case MB_UIF_CMD_PDATA_ECP2_B:
          mb_uif_answer = ANALYZE_PDATA_ECP2_B(rx_spi_data, rx_spi_len);                               
          break;
    default:  // UNKNOWN COMMAND
            mb_uif_answer = MB_UIF_CMD_NOT_ACKNOWLEDGE;
        break;
    }
    return mb_uif_answer;
}
// -----------------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------------
//! \brief  MB_UIF_CMD_TYPE
//!
//! reads the CMD received
//!
//! \param   ....
//! \return  1: SET INFO, 2: GET CMDS, (0, else): UNKNOWN
// -----------------------------------------------------------------------------------------
uint8_t MB_UIF_CMD_TYPE(uint8_t mb_uif_cmd){

  //uint8_t setget = MB_UNKNOWN;
    switch(mb_uif_cmd){
        // SET INFO
        case MB_UIF_CMD_VBAT1_INFO:
        case MB_UIF_CMD_VBAT2_INFO:
        case MB_UIF_CMD_WAKD_STATUS:
        case MB_UIF_CMD_WAKD_ATTITUDE:
        case MB_UIF_CMD_WAKD_COMMLINK:
        case MB_UIF_CMD_WAKD_OPMODE:
        case MB_UIF_CMD_WAKD_BLCIRCUIT_STATUS:
        case MB_UIF_CMD_WAKD_FLCIRCUIT_STATUS:
        case MB_UIF_CMD_WAKD_BLPUMP_INFO:
        case MB_UIF_CMD_WAKD_FLPUMP_INFO:
        case MB_UIF_CMD_WAKD_BLTEMPERATURE:
        case MB_UIF_CMD_WAKD_BLCIRCUIT_PRESSURE:
        case MB_UIF_CMD_WAKD_FLCIRCUIT_PRESSURE:
        case MB_UIF_CMD_WAKD_FLCONDUCTIVITY:
            
        case MB_UIF_CMD_WAKD_HFD_INFO:
        case MB_UIF_CMD_WAKD_SU_INFO:
        case MB_UIF_CMD_WAKD_POLAR_INFO:
        case MB_UIF_CMD_WAKD_ECPS_INFO:
        case MB_UIF_CMD_WAKD_PS_INFO:
        case MB_UIF_CMD_WAKD_ACT_INFO:
            
        case MB_UIF_CMD_PDATA_INITIALS:
        case MB_UIF_CMD_PDATA_PATIENTCODE:
        case MB_UIF_CMD_PDATA_GENDERAGE:
        case MB_UIF_CMD_PDATA_WEIGHT:
//        case MB_UIF_CMD_PDATA_HEARTRATE:
//        case MB_UIF_CMD_PDATA_BREATHRATE:
//        case MB_UIF_CMD_PDATA_ACTIVITY:
        case MB_UIF_CMD_PDATA_SEWALL:
        case MB_UIF_CMD_PDATA_BLPRESSURE:
        case MB_UIF_CMD_PDATA_ECP1_A:
        case MB_UIF_CMD_PDATA_ECP1_B:
        case MB_UIF_CMD_PDATA_ECP2_A:
        case MB_UIF_CMD_PDATA_ECP2_B:
            
        case MB_UIF_CMD_ALARMS:
        case MB_UIF_CMD_ERRORS:
        case MB_UIF_CMD_TEST_LINK:
        
          return MB_SETINFO;
        
        // GET CMDS
        case MB_UIF_CMD_WAKD_SHUTDOWN:
        case MB_UIF_CMD_WAKD_START_OPERATION:
        case MB_UIF_CMD_WAKD_MODE_OPERATION:
        case MB_UIF_CMD_SCALE_GET_WEIGHT:
        case MB_UIF_CMD_SEW_START_STREAMING:
        case MB_UIF_CMD_SEW_STOP_STREAMING:
        case MB_UIF_CMD_NIBP_START_MEASUREMENT:
        
          return MB_GETCMDS;
        default:
          return MB_UNKNOWN;
    }
    //return setget;
}

// -----------------------------------------------------------------------------------------
//! \brief  MB_UIF_CMDS_GET
//!
//! returns 1 byte from command buffer or operation mode buffer
//!
//! \param   mb_uif_cmd = command
//! \param   idx = 0, D[1], idx = 1: D[0]
//! \return  D[1] or D[0]
// -----------------------------------------------------------------------------------------
uint8_t MB_UIF_CMDS_GET(uint8_t mb_uif_cmd, uint8_t idx){ // sends first D[1] (from D[1]:D[0])
  uint8_t data = 0;
    switch(mb_uif_cmd){
        case MB_UIF_CMD_WAKD_START_OPERATION:
        case MB_UIF_CMD_WAKD_MODE_OPERATION:
        case MB_UIF_CMD_WAKD_SHUTDOWN:
        case MB_UIF_CMD_SCALE_GET_WEIGHT:
        case MB_UIF_CMD_SEW_START_STREAMING:
        case MB_UIF_CMD_SEW_STOP_STREAMING:
        case MB_UIF_CMD_NIBP_START_MEASUREMENT:
            if(idx==0) data = ((UIF_CMDS_Register >> 8) & 0x00FF);
            if(idx==1) data = (UIF_CMDS_Register & 0x00FF);
            break;
        default:
            data = 0;
            break;
    }
    return data;
}

// -----------------------------------------------------------------------------------------
//! \brief  UIF_MB_CLR_CMD
//!
//! 
//!
//! \param   none
//! \return  none
// -----------------------------------------------------------------------------------------
void UIF_MB_CLR_CMD(){
    UIF_CMDS_Register = 0;
}
// -----------------------------------------------------------------------------------------
// END uifmbprotocol.c
// -----------------------------------------------------------------------------------------
