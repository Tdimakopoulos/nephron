// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   main.c
//! \brief  Start-UP routine
//!
//! Nephron+: UIF
//!
//! \author  DUDNIK G.S.
//! \date    20.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------
// Routines finished with J: Jack
// Routines finished with G: Gabriela
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "global.h"
#include <msp430.h>
#include "clock.h"
#include "io.h"
#include "timerB0.h"
#include "timerA0.h"
#include "timerA1.h"
#include "lcd.h"
#include "uca0.h"
#include "uca1.h"
#include "ucb0.h"
#include "i2c.h"
#include "tps61050.h"
#include "lcdnephronplus.h"
#include "uifmbprotocol.h"
#include "uifmenu.h"
#include "uifapplication.h"
#include "uifmenutest.h"

// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  UIF Main
//!
//! UIF Project Main function
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
__noreturn void main(void)
{
    
    // ------------------------------------------------------------
    // HW INIT 
    // ------------------------------------------------------------
    // Stop watchdog timer to prevent time out reset
    WDTCTL = WDTPW + WDTHOLD;
    
    // Initialization of all the I/O (must be the first initialization)
    //ioInitJ();
    ioInitG();

    // Initialization of the msp clocks
    //clockInitJ();
    clockInitG();
    
    // UCA0-UART [AUX COM]
    uca0InitUart();
    
    // UCB0-SPIMS [UIF <--> MB] UIF MASTER
    // ucb0InitSPIMS();
    // UCB0-SPISL [UIF <--> MB] MB MASTER
    ucb0InitSPISL();
    
    // timerA0 interrupt: buzzer toggling / switch OFF
    IO_BUZZER_LOW;
    timerA0_Init();
    // Enable the timer A0
    timerA0_Enable = 0; // buzzer alarm disabled

    timerA1_Init();
    // Enable the timer A1
    timerA1_Enable = 1; 
    
    // Initialization of the timer B0
    timerB0_Init();
    
    // Reset the LCD
    IO_UI_RST_LOW;
    DELAY_US(500);
    IO_UI_RST_HIGH;
    DELAY_US(500);
    
    // uca1 init: LCD communication IF
    //uca1InitJ();
    uca1InitG();
    UCA1_ENABLE;
    
    // LCD init 
    lcd_Init(1);

    // Enable the timer B0
    TIMERB0_EN;

    // APPLICATION INIT DATA
    uif_ApplicationInitData();    
    // ----------------------------------
    lcd_DisplayClear();
    Display_Menu_Status = Display_Initialize_MainMenu();
    LCD_DISPLAY_NEPHRONPLUS();
    // Clears the Keyboard Interrupt
    // otherwise it executes all the key int routines
    P2IFG &= ~PORT2_INTFLAGS; 	        // P2[4..0] IFG cleared
    // ----------------------------------
    // Enable the interruptions
    __enable_interrupt();
    __bis_SR_register(GIE);       // Enter LPM4, enable interrupts

    // This requires the global interrupt enabled...
    // LED DRIVER I2C
    ucb1InitI2C();
    
    // FLASHLIGHT Read & Write test
    tps61050_ReadRegister();
    tps61050_WriteRegister();
    // ----------------------------------
    // Simulation of commands 
    // received from MB
    // --> Uncomment for testing
#if 0    
    MB_UIF_CMD_TEST();
#endif    
    // ----------------------------------
    // Simulation of keys pressed
    // by settings keys int flags
    // --> Uncomment for testing
#if 0    
    UIF_MenuTest();
#endif 
    // ----------------------------------

#if defined ALARM_TEST
    /* Simulate an alarm event:
     * Instead of using the ISR and the alarm analyze function we
     * set a specific alarm here directly.
     */
    DELAY_MS(7000);
    MB_WAKD_ALARMS_Status = 8;
    spi_wakd_alarms_status_ok = TRUE;
    // Display the alarm
    DisplayAlarm(MB_WAKD_ALARMS_Status);
    // Disable all the buttons
    P2IES = 0x00;
    P2IFG = 0x00;
    P2IE = 0x00;
    // Wait 5 secs
    DELAY_MS(5000);
    // Enable the ok button
    P2IFG = 0x00;
    P2IES = KEY_1_OK;
    P2IE = KEY_1_OK;
#endif // ALARM_TEST

    while(1)
    {
    }
}
// -----------------------------------------------------------------------------------
// END MAIN.C
// -----------------------------------------------------------------------------------

