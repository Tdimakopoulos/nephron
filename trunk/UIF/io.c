// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   io.c
//! \brief  Functions for the I/O
//!
//! This file implements functions of initialization of the I/O and the utilization of
//!  this one. 
//!
//! \author  Porchet J-A
//! \date    17.01.2010
//! \version 1.0
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "io.h"


// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the digital I/O
//!
//! Initialization of the digital I/O
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void ioInitJ(void)
{
    // Initialization of the port 1
    P1OUT = 0x00;                       // All pin are put in low state 
    P1DIR = 0xFF;                       // All pin are in output mode
    P1REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P1DS = 0x00;                        // All pin in reduced drive strength mode
    P1SEL = 0x00;                       // All pin are in I/O mode

    P1IES = 0x00;                       // All pin are in low-to-high transition
    P1IFG = 0x00;                       // IFG cleared for all pins
    P1IE = 0x00;                        // Disable all interrupts
    
    // Initialization of the port 2
    P2OUT = 0x00;                       // All pin are put in low state 
    P2DIR = BIT5|BIT6|BIT7;             // Pin 5, 6 and 7 are in output mode
    P2REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P2REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P2DS = 0x00;                        // All pin in reduced drive strength mode
    P2SEL = 0x00;                       // All pin are in I/O mode

    P2IES = 0x00;                       // All pin are in low-to-high transition
    P2IFG = 0x00;                       // IFG cleared for all pins
    P2IE = 0x00;                        // Disable all interrupts
    
    // Initialization of the port 3
    P3OUT = BIT0|BIT1|BIT2|BIT3;        // Resistor pull-up
    P3OUT |= BIT4|BIT5;                 // Resistor pull-up
    P3OUT |= BIT7;                      // Resistor pull-up
    P3DIR = 0x00;                       // All pin are in input mode
    P3REN = BIT0|BIT1|BIT2|BIT3;        // Pullup/pulldown resistor enabled
    P3REN |= BIT4|BIT5;                 // Pullup/pulldown resistor enabled
    P3REN |= BIT7;                      // Pullup/pulldown resistor enabled
    P3DS = 0x00;                        // All pin in reduced drive strength mode
    P3SEL = BIT6|BIT7;                  // Pin 6 : CLK, pin 7 : SDA 
    
    // Initialization of the port 4
    P4OUT = BIT1|BIT2|BIT4|BIT5;        // Pin 1, 2, 4 and 5 are in high state 
    P4OUT |= BIT0;                      // Resistor pull-up
    P4DIR = 0xFE;                       // All pin (except pin 0) are in output mode
    P4REN = BIT0;                       // Pullup/pulldown resistor enabled
    P4DS = 0x00;                        // All pin in reduced drive strength mode
    P4SEL = 0x00;                       // All pin are in I/O mode
    
    // Initialization of the port 5
    P5OUT = BIT4;                       // Pin 4 : Resistor pull-up
    P5DIR = BIT2|BIT3|BIT5|BIT7;        // Pin 2, 3, 5 and 7 are in output mode
    P5REN = BIT4;                       // Pin 4 : Pullup/pulldown resistor enabled
    P5DS = 0x00;                        // All pin in reduced drive strength mode
    P5SEL = BIT4|BIT6;                  // Pin 4: SCL, pin 6 : SIMO
    
    // Initialization of the port 6
    P6OUT = 0x00;                       // All pin are put in low state
    P6DIR = 0xFF;                       // All pin are in output mode
    P6REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P6DS = 0x00;                        // All pin in reduced drive strength mode
    P6SEL = 0x00;                       // All pin are in I/O mode
    
    // Initialization of the port 7
    P7OUT = 0x00;                       // All pin are put in low state
    P7DIR = 0x00;                       // All pin are in input mode
    P7REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P7DS = 0x00;                        // All pin in reduced drive strength mode
    P7SEL = BIT0|BIT1;                  // Pin 0: XIN and pin 1 : XOUT
    
    // Initialization of the port 8
    P8OUT = 0x00;                       // All pin are put in low state
    P8DIR = 0xFF;                       // All pin are in output mode
    P8REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P8DS = 0x00;                        // All pin in reduced drive strength mode
    P8SEL = 0x00;                       // All pin are in I/O mode
}
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the digital I/O
//!
//! Initialization of the digital I/O
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void ioInitG(void)
{
    // Initialization of the port 1
    P1OUT = 0x00;                       // All pin are put in low state 
    P1DIR = 0xFF;                       // All pin are in output mode
    P1REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P1DS = 0x00;                        // All pin in reduced drive strength mode
    P1SEL = BIT6;                       // All pin are in I/O mode except P1.6 (SMCLK)

    P1IES = 0x00;                       // All pin are in low-to-high transition
    P1IFG = 0x00;                       // IFG cleared for all pins
    P1IE = 0x00;                        // Disable all interrupts
    
    // Initialization of the port 2
    P2OUT = BIT0|BIT1|BIT2|BIT3|BIT4;   // Pin 0, 1, 2, 3 and 4 => resistor pull-up
    P2DIR = BIT5|BIT6|BIT7;             // Pin 5, 6 and 7 are in output mode
    P2REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P2REN = BIT0|BIT1|BIT2|BIT3|BIT4;   // Pin 0, 1, 2, 3 and 4 : Pullup/pulldown resistor enabled
    P2DS = 0x00;                        // All pin in reduced drive strength mode
    P2SEL = BIT6;                       // All pin are in I/O mode except P2.6 (ACLK)

    P2IES = 0x00;                       // All pin are in low-to-high transition
    P2IFG = 0x00;                       // IFG cleared for all pins
    P2IE = 0x00;                        // Disable all interrupts
    
    P2IES |= PORT2_INTFLAGS; 	        // P2[4..0] Hi/lo edge
    P2IFG &= ~PORT2_INTFLAGS; 	        // P2[4..0] IFG cleared
    P2IE |= PORT2_INTFLAGS;   		// P2[4..0] interrupt enabled
    
    // Initialization of the port 3
    P3OUT = BIT0|BIT1|BIT2|BIT3;        // Resistor pull-up
    P3OUT |= BIT4|BIT5;                 // Resistor pull-up
    P3OUT |= BIT6;                      // Resistor pull-up
    P3OUT |= BIT7;                      // Resistor pull-up
    P3DIR = 0x00;                       // All pin are in input mode
    P3REN = BIT0|BIT1|BIT2|BIT3;        // Pullup/pulldown resistor enabled
    P3REN |= BIT4|BIT5;                 // Pullup/pulldown resistor enabled
    P3REN |= BIT6;                      // Pullup/pulldown resistor enabled
    P3REN |= BIT7;                      // Pullup/pulldown resistor enabled
    P3DS = 0x00;                        // All pin in reduced drive strength mode
    P3SEL = BIT0|BIT1|BIT2|BIT3;        // P3.0/USCI B0 SPI - STE
                                        // P3.1/USCI B0 SPI - SIMO
                                        // P3.2/USCI B0 SPI - SOMI
                                        // P3.3/USCI B0 SPI - SCK
    P3SEL |= BIT4|BIT5;                 // P3.4/USCI A0 UART - TXD
                                        // P3.5/USCI A0 UART - RXD
    P3SEL |= BIT6|BIT7;                 // Pin 6 : CLK, pin 7 : SDA 
    
    // Initialization of the port 4
    P4OUT = BIT1|BIT2|BIT4|BIT5;        // Pin 1, 2, 4 and 5 are in high state 
    P4OUT |= BIT0;                      // Resistor pull-up
    P4DIR = 0xFE;                       // All pin (except pin 0) are in output mode
    P4REN = BIT0;                       // Pullup/pulldown resistor enabled
    P4DS = 0x00;                        // All pin in reduced drive strength mode
    P4SEL = BIT7;                       // All pin are in I/O mode except P4.7 TB0CLK OR SMCLK
    
    // Initialization of the port 5
    P5OUT = BIT4;                       // Pin 4 : Resistor pull-up
    P5DIR = BIT2|BIT3|BIT5|BIT7;        // Pin 2, 3, 5 and 7 are in output mode
    P5REN = BIT4;                       // Pin 4 : Pullup/pulldown resistor enabled
    P5DS = 0x00;                        // All pin in reduced drive strength mode
    P5SEL = BIT4|BIT6;                  // Pin 4: SCL, pin 6 : SIMO
    
    // Initialization of the port 6
    P6OUT = 0x00;                       // All pin are put in low state
    P6DIR = 0xFF;                       // All pin are in output mode
    P6REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P6DS = 0x00;                        // All pin in reduced drive strength mode
    P6SEL = 0x00;                       // All pin are in I/O mode
    
    // Initialization of the port 7
    P7OUT = 0x00;                       // All pin are put in low state
    P7DIR = 0x00;                       // All pin are in input mode
    P7REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P7DS = 0x00;                        // All pin in reduced drive strength mode
    P7SEL = BIT0|BIT1;                  // Pin 0: XIN and pin 1 : XOUT
    
    // Initialization of the port 8
    P8OUT = 0x00;                       // All pin are put in low state
    P8DIR = 0xFF;                       // All pin are in output mode
    P8REN = 0x00;                       // All pullup/pulldown resistor are disabled
    P8DS = 0x00;                        // All pin in reduced drive strength mode
    P8SEL = 0x00;                       // All pin are in I/O mode
}
