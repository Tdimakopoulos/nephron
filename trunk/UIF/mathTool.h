// -----------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------
//
//! \file   mathTool.h
//! \brief  math and data convertion utilities
//!
//! ....
//! ....
//!
//!
//! \author  Dudnik G.
//! \date    22.10.2010
//! \version 1.0
//! \remarks
// -----------------------------------------------------------------------------
#ifndef MATHTOOL_H
#define MATHTOOL_H

// ---------------------------------------------------------------------------------------------------------------------
// Exported functions
// ---------------------------------------------------------------------------------------------------------------------
extern uint8_t decimal_to_char(uint8_t _charn);
extern uint8_t decword_to_charstr(short numbers, uint8_t *charstr, uint8_t charqty, short multiplier);
extern uint8_t decword_to_indexstr(short numbers, uint8_t *charstr, uint8_t charqty, short multiplier);
extern uint8_t ascii_to_char(uint8_t _d0);
extern uint8_t asciix2_to_char(uint8_t _dh, uint8_t _dl);
extern uint8_t calculate_lrc(uint8_t *chain, int max_index);
extern uint8_t char_to_ascii(uint8_t _d0, int mode);
extern void int_to_ascii(unsigned int data, uint8_t* dout);
extern void byte_to_ascii(uint8_t data, uint8_t* dout);
extern float div_c_float (int x, float y);
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// END MATHTOOL.H
// -----------------------------------------------------------------------------------

