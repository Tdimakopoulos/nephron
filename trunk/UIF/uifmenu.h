// -----------------------------------------------------------------------------------
// Copyright (C) 2009          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifmenu.h
//! \brief  Nephron+: UIF Processor
//!
//! Menu: for keyboard & LCD-DOG Display
//!
//! \author  Gabriela Dudnik
//! \date    17.12.2009
//! \version 1.0 (ltms3)
//! \version 2.0 (nephron+)
// -----------------------------------------------------------------------------------
#ifndef UIFMENU_H
#define UIFMENU_H

// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
#define FULL_MENU                       1	        // 1=COMPLETE MENU, 0=DEMO ONLY

#define DISPLAY_STATUS_PRESENTATION	1		// Presentation
#define DISPLAY_STATUS_MAIN_MENU	2		// Main Menu
#define DISPLAY_STATUS_P_DATA_SUBM	3		// P_DATA SubMenu
#define DISPLAY_STATUS_STATUS_SUBM	4		// Status SubMenu
#define	DISPLAY_STATUS_CONFIG_SUBM	5		// Configuration SubMenu
#define DISPLAY_STATUS_P_DATA_INFO	6		// Shows P_DATA Meaning
#define	DISPLAY_STATUS_COMMAND_LIST	8		// Command List
#define	DISPLAY_STATUS_COMMAND_DONE	9		// Command Confirm
#define	DISPLAY_STATUS_MODE_LIST	10		// Mode List
#define	DISPLAY_STATUS_COMMAND_REJECTED	11		// Command rejected

#define DISPLAY_MAX_LINES         	3

// -----------------------------------------------------------------------------------
// keyboard codes
// -----------------------------------------------------------------------------------
#define KEYCODE_0_RESERVED              0	
#define KEYCODE_1_OK                    1
#define KEYCODE_2_TOP                   2
#define KEYCODE_3_BOTTOM                3
#define KEYCODE_4_RIGHT                 4
#define KEYCODE_5_LEFT                  5

// display time to sleep in seconds
#define   DISPLAY_TIME_TO_SLEEP 15  // 10 (<19.08.2010)
// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
// UIF CONTROL VARIABLES -------------------------------------------------------------
extern uint8_t   Display_Menu_Status;
// user if main menu
extern uint8_t   main_menu_item_index;
extern uint8_t   main_menu_item_list_index;
// user if p_data submenu
extern uint8_t   p_data_submenu_page_index;
// user if command submenu
extern uint8_t   command_submenu_item_index;
extern uint8_t   command_submenu_item_list_index;
// user if mode submenu
extern uint8_t   mode_submenu_item_index;
extern uint8_t   mode_submenu_item_list_index;
// user if status menu
extern uint8_t   status_submenu_page_index;
// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
extern char Display_Initialize_MainMenu();

void display_menu(char menustatus);

// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// END UIFMENU.H
// -----------------------------------------------------------------------------------
