// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   TimerA0.c
//! \brief  Functions for the timer A0
//!
//! This file implements functions of initialization and interruption for the timer A0
//!
//! \author  DUDNIK G.S.
//! \date    24.12.2011
//! \version 1.0
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include section
// -----------------------------------------------------------------------------------
#include "timerA0.h"
#include "io.h"
#include "uifApplication.h"
#include "lcdnephronplus.h"
#include "uifmenu.h"
// -----------------------------------------------------------------------------------
// Private definitions 
// -----------------------------------------------------------------------------------
#define ALARM_CHECK_TIMER 10000
#define ALARM_LOCK_DURATION 1000
#define ALARM_MAX_DISPLAY_DURATION 3000
#define ALARM_MB_CHECK_PERIOD 5000
#define ERROR_CHECK_TIMER 15000
#define ERROR_LOCK_DURATION 1000
#define ERROR_MAX_DISPLAY_DURATION 3000
#define ERROR_MB_CHECK_PERIOD 5000
// -----------------------------------------------------------------------------------
// Private macros
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private structures
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private type
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private constants
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
uint8_t timerA0_Enable = 0;
uint8_t timerA0_Toggle = 0;
uint16_t timerA0_Counter = 0;
uint8_t timerA0_flashlight = 0;
uint32_t alarm_timer = 0;
uint32_t display_alarm_timer = 0;
uint32_t alarm_check_timer = 0;
uint32_t error_timer = 0;
uint32_t display_error_timer = 0;
uint32_t error_check_timer = 0;

// -----------------------------------------------------------------------------------
// Private function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definition
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Initialization of the timer A0
//!
//! Initialization of the timer A0.
//!
//! \param   void
//! \return  void
// -----------------------------------------------------------------------------------
void timerA0_Init(void)
{
    TA1CCR0 = 1000;                         // 1000 = 1ms, 250 and 2500 (sounds bad)
                                            // 500..1500 ok
    TA1CTL = TASSEL_2 +ID_3 + MC_1 + TACLR; // SMCLK, upmode, clear TAR
    TA1CCTL0 = CCIE;                        // TACCR0 interrupt enabled       
}
// -----------------------------------------------------------------------------------
//! \brief  Interrupt function of the timer A0 (TIMER1_A3)
//!
//! Timer1 A0 interrupt service routine: toggles BUZZER ON-OFF
//!
//! \param  void
//! \return void
// -----------------------------------------------------------------------------------
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR (void)
{
  
  /* Copy mb buffer to uif buffer if there is new alarms data */
  if(alarm_check_timer < ALARM_MB_CHECK_PERIOD) {
    alarm_check_timer++;
  } else {
    if(spi_wakd_alarms_status_ok == TRUE){
      
      for(uint8_t i = 0; i < 4; i++) {
        UIF_WAKD_ALARMS_Status[i] = MB_WAKD_ALARMS_Status[i];
        if(alarms_ack[UIF_WAKD_ALARMS_Status[i]] == FALSE) {
          alarms_received[UIF_WAKD_ALARMS_Status[i]] = TRUE;
        }
      }
      
      /* Evict received and acknowledged alarm if not present in the new
       * alarm message */
      for(uint8_t i = 0; i < 39; i++) {
        if(alarms_ack[i] == TRUE &&
           alarms_received[i] == TRUE &&
             i != UIF_WAKD_ALARMS_Status[0] &&
               i != UIF_WAKD_ALARMS_Status[1] &&
                 i != UIF_WAKD_ALARMS_Status[2] &&
                   i != UIF_WAKD_ALARMS_Status[3]) {
                     alarms_ack[i] = FALSE;
                     alarms_received[i] = FALSE;
                   }
      }
      
      spi_wakd_alarms_status_ok = FALSE;
    }
  }
  
  /* Copy mb buffer to uif buffer if there is new errors data */
  if(error_check_timer < ERROR_MB_CHECK_PERIOD) {
    error_check_timer++;
  } else {
    if(spi_wakd_errors_status_ok == TRUE){
      
      for(uint8_t i = 0; i < 4; i++) {
        UIF_WAKD_ERRORS_Status[i] = MB_WAKD_ERRORS_Status[i];
        if(errors_ack[UIF_WAKD_ERRORS_Status[i]] == FALSE) {
          errors_received[UIF_WAKD_ERRORS_Status[i]] = TRUE;
        }
      }
      
      /* Evict received and acknowledged alarm if not present in the new
       * alarm message */
      for(uint8_t i = 0; i < 39; i++) {
        if(errors_ack[i] == TRUE &&
           errors_received[i] == TRUE &&
             i != UIF_WAKD_ERRORS_Status[0] &&
               i != UIF_WAKD_ERRORS_Status[1] &&
                 i != UIF_WAKD_ERRORS_Status[2] &&
                   i != UIF_WAKD_ERRORS_Status[3]) {
                     errors_ack[i] = FALSE;
                     errors_received[i] = FALSE;
                   }
      }
      
      spi_wakd_errors_status_ok = FALSE;
    }
  }
  
  if(WAKD_BUZZER_ONOFF == BUZZER_ON){
      if(timerA0_Toggle==0){        
          timerA0_Toggle = 1;
          IO_BUZZER_HIGH;
      }
      else{
          timerA0_Toggle = 0;
          IO_BUZZER_LOW;
      }
  }
  if(WAKD_UIF_IO_TEST == IOTEST_ON){
      if(timerA0_Toggle==0){        
          timerA0_Toggle = 1;
          IO_UI_P64_HIGH;
      }
      else{
          timerA0_Toggle = 0;
          IO_UI_P64_LOW;
      }
  }  
  
  if(alarm_timer < ALARM_CHECK_TIMER) {
    alarm_timer++;
    alarm_raised = 0;
  } else {
    alarm_raised++;
    if(alarms_received[alarm_raised] == TRUE && 
       alarms_ack[alarm_raised] == FALSE) {
         switch(display_alarm_timer) {
         case 0:
           DisplayAlarm(alarm_raised);
           P2IFG = 0x0;
           P2IE = 0x0;
           display_alarm_timer++;
           alarm_raised--;
           break;
         case ALARM_LOCK_DURATION:
           P2IFG = 0x0;
           P2IE = KEY_1_OK;           
           display_alarm_timer++;
           alarm_raised--;
           break;
         case ALARM_MAX_DISPLAY_DURATION:
           P2IFG = 0x0;
           P2IE = PORT2_INTFLAGS;
           display_menu(Display_Menu_Status);
           display_alarm_timer = 0;
           break;
         default:
           display_alarm_timer++;
           alarm_raised--;
           break;
         }
       }
    if(alarm_raised == 38) {
      alarm_timer = 0;
    }
  }
  
  if(error_timer < ERROR_CHECK_TIMER) {
    error_timer++;
    error_raised = 0;
  } else {
    error_raised++;
    if(errors_received[error_raised] == TRUE && 
       errors_ack[error_raised] == FALSE) {
         switch(display_error_timer) {
         case 0:
           DisplayError(error_raised);
           P2IFG = 0x0;
           P2IE = 0x0;
           display_error_timer++;
           error_raised--;
           break;
         case ERROR_LOCK_DURATION:
           P2IFG = 0x0;
           P2IE = KEY_1_OK;           
           display_error_timer++;
           error_raised--;
           break;
         case ERROR_MAX_DISPLAY_DURATION:
           P2IFG = 0x0;
           P2IE = PORT2_INTFLAGS;
           display_menu(Display_Menu_Status);
           display_error_timer = 0;
           break;
         default:
           display_error_timer++;
           error_raised--;
           break;
         }
       }
    if(error_raised == 38) {
      error_timer = 0;
    }
  }
   
//  if(alarm_timer < ALARM_CHECK_TIMER) {
//    alarm_timer += 1;
//  } else {
//    if(alarm_raised == 0) {
//      alarm_raised++;
//      if(alarm_raised > 39) {
//        alarm_raised = 0;
//      } else if(alarms_received[alarm_raised] == TRUE &&
//          alarms_ack[alarm_raised] == FALSE) {
//            DisplayAlarm(alarm_raised);
//          }
//    }
//    }
            
            
                // Disable all the buttons
//    P2IFG = 0x00;
//    P2IE = 0x00;
//    DELAY_MS(2000);
    // Enable only the ok button
//    P2IFG = 0x00;
//    P2IE = KEY_1_OK;
//    DELAY_MS(3000);
//    display_menu(Display_Menu_Status);
//                    P2IFG = 0x00;
//                    P2IE = PORT2_INTFLAGS;

  
  //if (UIF_WAKD_ALARMS_Status != "noprob") {
    // initialize timer for alarm
    // if timer = 0
    //   save lcd in buffer and state
    //   show message
    //   cont counting
    //   disable keys
    // else if timer = 5000
    //   return status
    //   restore previous buffer and state
    //   enable lim. func.
    // else
    //   if ok pressed
    //     restore previous buffer and state
    //     enable full func.
    //   cont counting
  //}

  if(WAKD_FLASHLIGHT_ONOFF == FLASHLIGHT_ON){  
      timerA0_Counter++;
      if(timerA0_Counter == 1000){
          timerA0_Counter = 0;
          IO_FL_SYNC_HIGH;
          timerA0_flashlight = 1;
      }
      
      if(timerA0_flashlight==2) {
          IO_FL_SYNC_LOW;
          timerA0_flashlight = 0;
      }
      if(timerA0_flashlight == 1) timerA0_flashlight++;
  } 
  
}
// -----------------------------------------------------------------------------------
