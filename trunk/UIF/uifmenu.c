// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifmenu.c
//! \brief  Nephron+: UIF Processor
//!
//! Menu: for keyboard & LCD-DOG Display
//!
//! \author  Gabriela Dudnik
//! \date    17.12.2009
//! \version 1.0 (ltms3)
//! \version 2.0 (nephron+)
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include <msp430.h>
#include "io.h"
#include "uca0.h"
#include "lcd.h"
#include "lcdnephronplus.h"
#include "uifmenu.h"
#include "uifApplication.h"
#include "mathtool.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// UIF CONTROL VARIABLES -------------------------------------------------------------
uint8_t   Display_Menu_Status = 0;
// user if main menu
uint8_t   main_menu_item_index = 0;
uint8_t   main_menu_item_list_index = 0;
// user if p_data submenu
uint8_t   p_data_submenu_page_index = 0;
// user if command submenu
uint8_t   command_submenu_item_index = 0;
uint8_t   command_submenu_item_list_index = 0;
// user if mode submenu
uint8_t mode_submenu_item_index = 0;
uint8_t mode_submenu_item_list_index = 0;
// user if status menu
uint8_t   status_submenu_page_index = 0;
// previous mode
uint16_t previous_mode = 0x0001; // starting from all stopped

// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Presentation
//!         ...
//!
//! Actions if pressing keys on Presentation
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Presentation (char keypressed, char substatus){
	char newstatus = substatus;
	switch(keypressed){
            case KEYCODE_1_OK:
                newstatus = DISPLAY_STATUS_MAIN_MENU;
                main_menu_item_index = 1;
                LCD_DISPLAY_MAIN_MENU(main_menu_item_index);	
            break;
	}
	return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Menu_Selected
//!         ...
//!
//! Draws the First Window of the correspondent SubItem in Main Menu
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Menu_Selected(char which_submenu_index){
        char newstatus;
        switch(which_submenu_index){
            case 1:
                newstatus = DISPLAY_STATUS_P_DATA_SUBM;
                p_data_submenu_page_index=0;//++;
                LCD_DISPLAY_P_DATA_SUBMENU(p_data_submenu_page_index, P_DATA_QTY);       // 0 index in event list, 1 index in display
                break;
            case 2:
                newstatus = DISPLAY_STATUS_STATUS_SUBM;
                status_submenu_page_index=0;
                LCD_DISPLAY_STATUS_SUBMENU(status_submenu_page_index, STATUS_QTY);
                break;
            case 3:
                newstatus = DISPLAY_STATUS_CONFIG_SUBM;
                command_submenu_item_index++;
                LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index, COMMAND_QTY);
                
                break;
        }
        return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Main
//!         ...
//!
//! Actions if pressing keys on Main
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Main (char keypressed, char substatus){
	char newstatus = substatus;
        switch(keypressed){
            case KEYCODE_3_BOTTOM:
              if(main_menu_item_index < DISPLAY_MAX_LINES){
                main_menu_item_index++; // MAIN_ITEMS
                LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
              }
            break;
            case KEYCODE_2_TOP:
              if(main_menu_item_index > 1){
                main_menu_item_index--;
                LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
              }
            break;
            case KEYCODE_1_OK:
                newstatus = Display_Analyze_Menu_Selected(main_menu_item_index);
            break;
            // @@@@ TEST 25.03.2010
            case KEYCODE_5_LEFT:
              newstatus =  DISPLAY_STATUS_PRESENTATION;
              LCD_DISPLAY_NEPHRONPLUS();
            break;
        }
	
	return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_P_DATA
//!         ...
//!
//! Actions if pressing keys on P_DATA (Patient Data)
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_P_DATA (char keypressed, char substatus){
	char newstatus = substatus;
	switch(keypressed){
          case KEYCODE_3_BOTTOM:
          	if(p_data_submenu_page_index<P_DATA_QTY){
                p_data_submenu_page_index+=P_DATA_ITEMS;
                LCD_DISPLAY_P_DATA_SUBMENU(p_data_submenu_page_index, P_DATA_QTY);
                }
            break;
          case KEYCODE_2_TOP:
          	if(p_data_submenu_page_index >= P_DATA_ITEMS){
                p_data_submenu_page_index-=P_DATA_ITEMS;
                LCD_DISPLAY_P_DATA_SUBMENU(p_data_submenu_page_index, P_DATA_QTY);
              }
            break;
          case KEYCODE_5_LEFT:
            newstatus = DISPLAY_STATUS_MAIN_MENU;
            main_menu_item_index=1; // MAIN_ITEMS
            LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
            break;
        }
	return newstatus;
}

// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Status
//!         ...
//!
//! Actions if pressing keys on Status
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Status (char keypressed, char substatus){
	char newstatus = substatus;
	switch(keypressed){
          case KEYCODE_3_BOTTOM:
          	if(status_submenu_page_index<STATUS_QTY){
                status_submenu_page_index+=STATUS_ITEMS;
                LCD_DISPLAY_STATUS_SUBMENU(status_submenu_page_index, STATUS_QTY);
                }
            break;
          case KEYCODE_2_TOP:
          	if(status_submenu_page_index >= STATUS_ITEMS){
                status_submenu_page_index-=STATUS_ITEMS;
                LCD_DISPLAY_STATUS_SUBMENU(status_submenu_page_index, STATUS_QTY);
              }
            break;
          case KEYCODE_5_LEFT:
            newstatus = DISPLAY_STATUS_MAIN_MENU;
            main_menu_item_index=2; // MAIN_ITEMS
            LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
            break;
        }
	return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Config
//!         ...
//!
//! Actions if pressing keys on Config
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Config (char keypressed, char substatus){
    char newstatus = substatus;
    switch(keypressed){
        case KEYCODE_3_BOTTOM:
          // @@@@ DISPLAY_MAX_LINES-1: ONLY BECAUSE THERE ARE 2 OPTIONS (<3)
        if(command_submenu_item_index < DISPLAY_MAX_LINES){
            command_submenu_item_index++;
            LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index, COMMAND_QTY);
        }
        else{
            if((command_submenu_item_list_index+2)<(COMMAND_QTY-1)){
                command_submenu_item_list_index++;
                LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
            }
        }
            break;
        case KEYCODE_2_TOP:
            if(command_submenu_item_index > 1){
                command_submenu_item_index--;
                LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
            }
            else{
              if(command_submenu_item_list_index > 0){
                command_submenu_item_list_index--;
                LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
              }
            }
            break;
    case KEYCODE_1_OK:
      if(command_submenu_item_index == 3) {
        newstatus = DISPLAY_STATUS_MODE_LIST;
        command_submenu_item_list_index = 0;
        command_submenu_item_index = 1;
        LCD_DISPLAY_MODE_SUBMENU(command_submenu_item_list_index, command_submenu_item_index, MODE_QTY);            
      } else {
        newstatus = DISPLAY_STATUS_COMMAND_LIST;
        LCD_DISPLAY_COMMAND_LIST(command_submenu_item_list_index+command_submenu_item_index-1);
      }
      break;
    case KEYCODE_5_LEFT:
      newstatus = DISPLAY_STATUS_MAIN_MENU;
      main_menu_item_index=3; // MAIN_ITEMS
      LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
      break;
    }
    return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Command_List
//!         ...
//!
//! Actions if pressing keys on Command List
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Command_List(char keypressed, char substatus){
    char newstatus = substatus;
    char which_command = 0;
    switch (keypressed){
        case KEYCODE_1_OK:
            // Update NEPHRONPLUS COMMAND flags
            which_command = command_submenu_item_list_index+command_submenu_item_index-1;
            MB_SET_COMMAND_FLAG(which_command);
            newstatus = DISPLAY_STATUS_COMMAND_DONE;
            LCD_DISPLAY_COMMAND_ACCEPTED(command_submenu_item_list_index+command_submenu_item_index-1);
            break;
        case KEYCODE_5_LEFT:
            newstatus = DISPLAY_STATUS_CONFIG_SUBM;
            LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
            break;
    }
    return newstatus;
}

char Display_Analyze_Mode_List(char keypressed, char substatus) {
  char newstatus = substatus;
  uint16_t which_command = 0;
  switch (keypressed){
  case KEYCODE_3_BOTTOM:
    if(mode_submenu_item_index < DISPLAY_MAX_LINES){
      mode_submenu_item_index++;
      LCD_DISPLAY_MODE_SUBMENU(mode_submenu_item_list_index, mode_submenu_item_index, MODE_QTY);
    }
    else{
      if((mode_submenu_item_list_index+2)<(MODE_QTY-1)){
        mode_submenu_item_list_index++;
        LCD_DISPLAY_MODE_SUBMENU(mode_submenu_item_list_index, mode_submenu_item_index,MODE_QTY);
      }
    }
    break;
  case KEYCODE_2_TOP:
    if(mode_submenu_item_index > 1){
      mode_submenu_item_index--;
      LCD_DISPLAY_MODE_SUBMENU(mode_submenu_item_list_index, mode_submenu_item_index,MODE_QTY);
    }
    else{
      if(mode_submenu_item_list_index > 0){
        mode_submenu_item_list_index--;
        LCD_DISPLAY_MODE_SUBMENU(mode_submenu_item_list_index, mode_submenu_item_index,MODE_QTY);
      }
    }
    break;
  case KEYCODE_1_OK:
    which_command = 7 + mode_submenu_item_list_index * 3 + mode_submenu_item_index;
    switch(previous_mode) {
      case UIF_COMMAND_SHUTDOWN: {
        if (which_command == UIF_COMMAND_START_OPERATION) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;
      case UIF_COMMAND_START_OPERATION:
        if (which_command == UIF_COMMAND_GOTO_DIALYSIS 
            || which_command == UIF_COMMAND_GOTO_REGEN1 
            || which_command == UIF_COMMAND_GOTO_ULTRA 
            || which_command == UIF_COMMAND_GOTO_REGEN2) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;
      case UIF_COMMAND_GOTO_DIALYSIS:
        if (which_command == UIF_COMMAND_GOTO_REGEN1 || which_command == UIF_COMMAND_START_OPERATION) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;
      case UIF_COMMAND_GOTO_REGEN1:
        if (which_command == UIF_COMMAND_GOTO_ULTRA || which_command == UIF_COMMAND_START_OPERATION) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;
        case UIF_COMMAND_GOTO_ULTRA:
        if (which_command == UIF_COMMAND_GOTO_REGEN2 || which_command == UIF_COMMAND_START_OPERATION) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;
        case UIF_COMMAND_GOTO_REGEN2:
        if (which_command == UIF_COMMAND_GOTO_DIALYSIS || which_command == UIF_COMMAND_START_OPERATION) {
          newstatus = DISPLAY_STATUS_COMMAND_DONE;
          previous_mode = which_command;
          LCD_DISPLAY_COMMAND_ACCEPTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        } else {
          LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        }
        break;        
      default:
        LCD_DISPLAY_COMMAND_REJECTED(mode_submenu_item_list_index+mode_submenu_item_index-1);
        break;
    }
  }
    break;
  case KEYCODE_5_LEFT:
    mode_submenu_item_list_index = 0;
    mode_submenu_item_index = 1;
    newstatus = DISPLAY_STATUS_CONFIG_SUBM;
    LCD_DISPLAY_COMMAND_SUBMENU(mode_submenu_item_list_index, mode_submenu_item_index,COMMAND_QTY);
    break;
  }
  return newstatus;
}

// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Command_Done
//!         ...
//!
//! ...
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Command_Done(char keypressed, char substatus){
    char newstatus = substatus;
    switch (keypressed){
        case KEYCODE_5_LEFT:
            newstatus = DISPLAY_STATUS_CONFIG_SUBM;
            LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
            break;
    }
    return newstatus;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Initialize_MainMenu
//!         ...
//!
//! Presentation Screen
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Initialize_MainMenu(){
    LCD_DISPLAY_NEPHRONPLUS();
    return DISPLAY_STATUS_PRESENTATION;
}
// -----------------------------------------------------------------------------------
//! \brief  Display_Analyze_Menu_Main
//!         ...
//!
//! Main Analyzing routine
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
char Display_Analyze_Menu_Main(char keypressed, char menustatus) {
    char newstatus = menustatus;
    switch (newstatus){
        case (DISPLAY_STATUS_PRESENTATION):	// Presentation
            newstatus = Display_Analyze_Presentation(keypressed, newstatus);
            break;
        case (DISPLAY_STATUS_MAIN_MENU):	// Main Menu
            p_data_submenu_page_index = 0;
            command_submenu_item_index = 0;
            command_submenu_item_list_index  = 0;
            mode_submenu_item_index = 0;
            status_submenu_page_index = 0;
            newstatus = Display_Analyze_Main(keypressed, newstatus);
            break;
        case (DISPLAY_STATUS_P_DATA_SUBM):	// P_DATA SubMenu
            newstatus = Display_Analyze_P_DATA(keypressed, newstatus);
            break;
        case (DISPLAY_STATUS_STATUS_SUBM):	// Status SubMenu
            newstatus = Display_Analyze_Status(keypressed, newstatus);
            break;
        case (DISPLAY_STATUS_CONFIG_SUBM):	// Configuration SubMenu
            newstatus = Display_Analyze_Config(keypressed, newstatus);
            break;
        case (DISPLAY_STATUS_COMMAND_LIST):     // Command list
            newstatus = Display_Analyze_Command_List(keypressed, newstatus);
            break;
    case DISPLAY_STATUS_MODE_LIST: // Mode list
      newstatus = Display_Analyze_Mode_List(keypressed, newstatus);
      break;
        case (DISPLAY_STATUS_COMMAND_DONE):
            newstatus = Display_Analyze_Command_Done(keypressed, newstatus);
            break;
        default:
            break;
    }
    return newstatus;
}

void display_menu(char menustatus) {
    switch(menustatus) {
        case (DISPLAY_STATUS_PRESENTATION):	// Presentation
            LCD_DISPLAY_NEPHRONPLUS();
            break;
        case (DISPLAY_STATUS_MAIN_MENU):	// Main Menu
            LCD_DISPLAY_MAIN_MENU(main_menu_item_index);
            break;
        case (DISPLAY_STATUS_P_DATA_SUBM):	// P_DATA SubMenu
            LCD_DISPLAY_P_DATA_SUBMENU(p_data_submenu_page_index, P_DATA_QTY);
            break;
        case (DISPLAY_STATUS_STATUS_SUBM):	// Status SubMenu
            LCD_DISPLAY_STATUS_SUBMENU(status_submenu_page_index, STATUS_QTY);
            break;
        case (DISPLAY_STATUS_CONFIG_SUBM):	// Configuration SubMenu
            LCD_DISPLAY_COMMAND_SUBMENU(command_submenu_item_list_index, command_submenu_item_index,COMMAND_QTY);
            break;
        case (DISPLAY_STATUS_COMMAND_LIST):     // Command list
            LCD_DISPLAY_COMMAND_LIST(command_submenu_item_list_index+command_submenu_item_index-1);
            break;
        case (DISPLAY_STATUS_COMMAND_DONE):
            LCD_DISPLAY_COMMAND_ACCEPTED(command_submenu_item_list_index+command_submenu_item_index-1);
            break;
        case (DISPLAY_STATUS_COMMAND_REJECTED):
            LCD_DISPLAY_COMMAND_REJECTED(command_submenu_item_list_index+command_submenu_item_index-1);
            break;
        default:
            break;
    }

}

// -----------------------------------------------------------------------------------
//! \brief  Port_2 Interrupt
//!         ...
//!
//! Port 2 interrupt service routine: keyboard input interrupts
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
#if 0  
    uint8_t message[16];
#endif
    
    uint8_t  p2_flags = P2IFG;				// ALL THE FLAGS
    
   
        if((p2_flags & KEY_4_RIGHT)!=0){
                P2IFG &= ~KEY_4_RIGHT;  		// P2.1 IFG cleared
                if(FULL_MENU==1) Display_Menu_Status = Display_Analyze_Menu_Main(KEYCODE_4_RIGHT, Display_Menu_Status);
                P7OUT ^= K4_OUT_RIGHT;
#if 0                
                message[0] = 'R'; message[1] = 'I';message[2] = 'G';message[3] = 'H';message[4] = 'T';message[5] = ' ';message[6] = 'K';message[7] = 'E';
                message[8] = 'Y'; message[9] = '\0'; message[10] = '\0'; message[11] = '\0'; message[12] = '\0'; message[13] = '\0'; message[14] = '\0'; message[15] = '\0';
                sendMsgUartUCA0 (message, 9);
#endif
#if 0                
                LCD_DISPLAY_Show_KEY(KEYCODE_4_RIGHT);
#endif                
        }
    
        if((p2_flags & KEY_3_BOTTOM)!=0){
                P2IFG &= ~KEY_3_BOTTOM;  		// P2.2 IFG cleared
                if(FULL_MENU==1) Display_Menu_Status = Display_Analyze_Menu_Main(KEYCODE_3_BOTTOM, Display_Menu_Status);
                P7OUT ^= K3_OUT_BOTTOM;
#if 0
                message[0] = 'B'; message[1] = 'O';message[2] = 'T';message[3] = 'T';message[4] = 'O';message[5] = 'M';message[6] = ' ';message[7] = 'K';
                message[8] = 'E'; message[9] = 'Y'; message[10] = '\0'; message[11] = '\0'; message[12] = '\0'; message[13] = '\0'; message[14] = '\0'; message[15] = '\0';
                sendMsgUartUCA0 (message, 10);
#endif
#if 0
                LCD_DISPLAY_Show_KEY(KEYCODE_3_BOTTOM);
#endif                
        }
    
        if((p2_flags & KEY_5_LEFT)!=0){
                P2IFG &= ~KEY_5_LEFT;  			// P2.3 IFG cleared
                if(FULL_MENU==1) Display_Menu_Status = Display_Analyze_Menu_Main(KEYCODE_5_LEFT, Display_Menu_Status);
                P7OUT ^= K5_OUT_LEFT;
#if 0
                message[0] = 'L'; message[1] = 'E';message[2] = 'F';message[3] = 'T';message[4] = ' ';message[5] = 'K';message[6] = 'E';message[7] = 'Y';
                message[8] = '\0'; message[9] = '\0'; message[10] = '\0'; message[11] = '\0'; message[12] = '\0'; message[13] = '\0'; message[14] = '\0'; message[15] = '\0';
                sendMsgUartUCA0 (message, 8);
#endif
#if 0                
                LCD_DISPLAY_Show_KEY(KEYCODE_5_LEFT);
#endif                
        }   
        if((p2_flags & KEY_2_TOP)!=0){
                P2IFG &= ~KEY_2_TOP;  			// P2.4 IFG cleared
                if(FULL_MENU==1) Display_Menu_Status = Display_Analyze_Menu_Main(KEYCODE_2_TOP, Display_Menu_Status);
                P7OUT ^= K2_OUT_TOP;
#if 0                
                message[0] = 'T'; message[1] = 'O';message[2] = 'P';message[3] = ' ';message[4] = 'K';message[5] = 'E';message[6] = 'Y';message[7] = '\0';
                message[8] = '\0'; message[9] = '\0'; message[10] = '\0'; message[11] = '\0'; message[12] = '\0'; message[13] = '\0'; message[14] = '\0'; message[15] = '\0';
                sendMsgUartUCA0 (message, 7);
#endif
#if 0                
                LCD_DISPLAY_Show_KEY(KEYCODE_2_TOP);
#endif                
        }
        if((p2_flags & KEY_1_OK)!=0){
                P2IFG &= ~KEY_1_OK;  			// P2.5 IFG cleared
                if(alarm_raised != 0) {
                  alarms_ack[alarm_raised+1] = TRUE;
                  P2IE = PORT2_INTFLAGS;
                  display_menu(Display_Menu_Status);
                } else if (error_raised != 0) {
                  errors_ack[error_raised+1] = TRUE;
                  P2IE = PORT2_INTFLAGS;
                  display_menu(Display_Menu_Status);
                } else {
                    if(FULL_MENU==1) Display_Menu_Status = Display_Analyze_Menu_Main(KEYCODE_1_OK, Display_Menu_Status);
                }
                P7OUT ^=K1_OUT_OK;
#if 0
                message[0] = 'O'; message[1] = 'K';message[2] = ' ';message[3] = 'K';message[4] = 'E';message[5] = 'Y';message[6] = '\0';message[7] = '\0';
                message[8] = '\0'; message[9] = '\0'; message[10] = '\0'; message[11] = '\0'; message[12] = '\0'; message[13] = '\0'; message[14] = '\0'; message[15] = '\0';
                sendMsgUartUCA0 (message, 6);		
#endif
#if 0                
                LCD_DISPLAY_Show_KEY(KEYCODE_1_OK);
#endif                
    }
}
// -----------------------------------------------------------------------------------
// END UIFMENU.C
// -----------------------------------------------------------------------------------
