// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   lcd.h
//! \brief  ...
//!
//! Creation of the definitions, variables and prototype functions exported
//!
//! \author  DUDNIK G.S.
//! \date    27.01.2010
//! \version 1.0 First version (GDU)
// -----------------------------------------------------------------------------------
#ifndef _LCD_H_
#define _LCD_H_
// -----------------------------------------------------------------------------------
// Include
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
// Display properties (for graphics functions) ---------------------------------------
#define DISPLAYFRAMESIZE        1056    // Size of the LCD frame
#define DISP_WIDTH              132L
#define DISP_HEIGHT             32L
// -----------------------------------------------------------------------------------
// Exported type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported inline code definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void lcd_Init(uint8_t);

extern void lcd_ClearLocalFrame(void);
extern void lcd_ClearLocalFrameOFF(void);
extern void lcd_SendDisplayFrame(void);
extern void lcd_DisplayClear(void);
extern void lcd_DisplayClearOFF(void);

extern void lcd_DrawString(uint8_t x, uint8_t y, const uint8_t *str);
extern void lcd_DrawStringH(uint8_t y, const uint8_t *str);

extern void lcd_DrawRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h);
extern void lcd_FillRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h);

extern void lcd_DrawRoundedRect(uint8_t x, uint8_t y, uint8_t w, uint8_t h);

extern void SetPixel(uint8_t x, uint8_t y, uint8_t pixel_status);
extern void DrawArialRString(uint8_t x, uint8_t y, const uint8_t *s, uint8_t transparent_background, uint8_t norm_inv);
extern void DrawArialRStringW(uint8_t x, uint8_t y, const uint8_t *s, uint8_t transparent_background, uint8_t norm_inv);
// -----------------------------------------------------------------------------------
#endif 
// -----------------------------------------------------------------------------------
// END __LCD_H_
// -----------------------------------------------------------------------------------
