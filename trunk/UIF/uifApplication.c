// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uifApplication.c
//! \brief  Nephron+: UIF Application
//!
//! Application Routines
//!
//! \author  Gabriela Dudnik
//! \date    26.12.2011
//! \version 1.0 (gdu)
//! \version 2.0 (nephron+)
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Include 
// -----------------------------------------------------------------------------------
#include "global.h"
#include "io.h"
#include "clock.h"
#include "uifmenu.h"
#include "uifApplication.h"
#include "mathtool.h"
#include "lcd.h"
// -----------------------------------------------------------------------------------
// Constant definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Type definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
// PM DL Protocol: -------------------------------------------------------------------
// ALARMS CONTROL --------------------------------------------------------------------
uint8_t WAKD_FLASHLIGHT_ONOFF = FLASHLIGHT_OFF;
uint8_t WAKD_BUZZER_ONOFF = BUZZER_OFF;
uint8_t WAKD_LED_BLUE_CONTROL = LEDS_OFF;
uint8_t WAKD_LED_GREEN_CONTROL = LEDS_ON;
uint8_t WAKD_LED_RED_CONTROL = LEDS_TOGGLE;
uint8_t WAKD_UIF_IO_TEST = IOTEST_OFF;

// UIF PATIENT INFO ----------------------------------------------------------------------
uint8_t UIF_P_DATA_INITIALS[4] = {' ',' ',' ','\0'};
uint8_t UIF_P_DATA_PATIENTCODE[4] = {' ',' ',' ','\0'};
uint8_t UIF_P_DATA_GENDER[2] = {' ','\0'};
uint8_t UIF_P_DATA_AGE;
uint16_t UIF_P_DATA_WEIGHT = 63;
uint8_t UIF_P_DATA_HEARTRATE = 75;
uint8_t UIF_P_DATA_BREATHINGRATE = 20;
uint8_t UIF_P_DATA_ACTIVITYCODE = ACTIVITY_RUNNING;
uint16_t UIF_P_DATA_SISTOLICBP = 12;
uint16_t UIF_P_DATA_DIASTOLICBP = 8;
uint16_t UIF_P_DATA_NA_ECP1 = 123;
uint16_t UIF_P_DATA_K_ECP1 = 22;
uint16_t UIF_P_DATA_PH_ECP1 = 7;
uint16_t UIF_P_DATA_UREA_ECP1 = 9;
uint16_t UIF_P_DATA_NA_ECP2 = 111;
uint16_t UIF_P_DATA_K_ECP2 = 55;
uint16_t UIF_P_DATA_PH_ECP2 = 10;
uint16_t UIF_P_DATA_UREA_ECP2 = 88;

// WAKD INFO -------------------------------------------------------------------------
int16_t WAKDAttitude = 90;                  // in degrees
uint8_t blood_outgoing_temperature = 38;    // C
uint8_t blood_incoming_temperature = 37;    // C

// [UIF] DATA ------------------------------------------------------------------------
// [UIF] WAKD Battery Packs 
uint8_t UIF_BPACK_1_Percent = 0;
uint8_t UIF_BPACK_2_Percent = 0;
// [UIF] WAKD Status
uint32_t UIF_WAKD_Status = 0;
uint8_t WAKD_FLAG_READY_TO_WORK = 99;
// [UIF] MB Communication Links Status
uint16_t UIF_MB_CLINKS_Status = 0;
// ---------------------------------------- // 
// [UIF] WAKD Attitude 
uint8_t UIF_WAKD_Attitude = 0;
// [UIF] WAKD Operating Mode
uint8_t UIF_WAKD_OperatingMode = 0;
// [UIF] WAKD Microfluidics
uint32_t UIF_WAKD_BLCircuit_Status = 0;
uint32_t UIF_WAKD_FLCircuit_Status = 0;
// [UIF] WAKD Temperature Sensor
uint16_t UIF_BTS_InletTemperature = 38;     // C
uint16_t UIF_BTS_OutletTemperature = 37;    // C
// WAKD Conductivity Sensor
uint16_t UIF_DCS_Conductance = 0;
uint16_t UIF_DCS_Susceptance = 0;
uint32_t UIF_DCS_Status = 0;
// [UIF] WAKD Pressure Sensors
uint16_t UIF_BPS_Pressure = 333;            // mbar
uint16_t UIF_FPS_Pressure = 495;            // mbar
// [UIF] WAKD Pumps Info
uint8_t UIF_BLPump_SpeedCode = 0;
uint8_t UIF_FLPump_SpeedCode = 0;
// [UIF] WAKD High Flux Dialyser
uint8_t UIF_WAKD_HFD_Status = 0;
uint8_t UIF_WAKD_HFD_PercentOK = 0;
// [UIF] WAKD Sorbent Unit
uint16_t UIF_WAKD_SU_Status = 0;
// [UIF] WAKD Polarizer
uint16_t UIF_WAKD_POLAR_Voltage = 0;
uint16_t UIF_WAKD_POLAR_Status = 0;
// [UIF] WAKD ECP1, ECP2
uint16_t UIF_WAKD_ECP1_Status = 0;
uint16_t UIF_WAKD_ECP2_Status = 0;
// [UIF] WAKD PS
uint32_t UIF_WAKD_PS_Status = 0;
// [UIF] WAKD ACT
uint32_t UIF_WAKD_ACT_Status = 0;
// [UIF] WAKD ALARMS/ERRORS
uint8_t UIF_WAKD_ALARMS_Status[4] = {0, 0, 0, 0};
boolean_t alarms_received[39];
boolean_t alarms_ack[39];
uint8_t alarm_raised = 0;
uint8_t error_raised = 0;
boolean_t errors_received[44];
boolean_t errors_ack[45];
uint8_t UIF_WAKD_ERRORS_Status[4] = {0, 0, 0, 0};


// MB PATIENT INFO ----------------------------------------------------------------------
uint8_t MB_P_DATA_INITIALS[4] = {' ',' ',' ','\0'};
uint8_t MB_P_DATA_PATIENTCODE[4] = {' ',' ',' ','\0'};
uint8_t MB_P_DATA_GENDER[2] = {'F','\0'};
uint8_t MB_P_DATA_AGE;
uint16_t MB_P_DATA_WEIGHT;
uint8_t MB_P_DATA_HEARTRATE = 75;
uint8_t MB_P_DATA_BREATHINGRATE = 20;
uint8_t MB_P_DATA_ACTIVITYCODE = ACTIVITY_RUNNING;
uint16_t MB_P_DATA_SISTOLICBP = 12;
uint16_t MB_P_DATA_DIASTOLICBP = 8;
uint16_t MB_P_DATA_NA_ECP1 = 123;
uint16_t MB_P_DATA_K_ECP1 = 22;
uint16_t MB_P_DATA_PH_ECP1 = 7;
uint16_t MB_P_DATA_UREA_ECP1 = 9;
uint16_t MB_P_DATA_NA_ECP2 = 111;
uint16_t MB_P_DATA_K_ECP2 = 55;
uint16_t MB_P_DATA_PH_ECP2 = 10;
uint16_t MB_P_DATA_UREA_ECP2 = 88;

// Battery Packs Variables -----------------------------------------------------------
// Battery Pack 1
uint8_t MB_BPACK_1_Percent = 0;
uint16_t MB_BPACK_1_RemainingCapacity = 0;
// Battery Pack 2
uint8_t MB_BPACK_2_Percent = 0;
uint16_t MB_BPACK_2_RemainingCapacity = 0;
// WAKD Status 
uint32_t MB_WAKD_Status = 0;
// WAKD Attitude
uint16_t MB_WAKD_Attitude = 0;
// MB Communication Links Status
uint16_t MB_CLINKS_Status = 0;
// WAKD Operating Mode
uint16_t MB_WAKD_OperatingMode = 0;
// WAKD Microfluidics
uint32_t MB_WAKD_BLCircuit_Status = 0;
uint32_t MB_WAKD_FLCircuit_Status = 0;
// WAKD Pumps
uint16_t MB_WAKD_BLPump_Speed = 0;
uint16_t MB_WAKD_BLPump_DirStatus = 0;
uint8_t MB_WAKD_BLPump_SpeedCode = 0;
uint16_t MB_WAKD_FLPump_Speed = 0;
uint16_t MB_WAKD_FLPump_DirStatus = 0;
uint8_t MB_WAKD_FLPump_SpeedCode = 0;
// WAKD Temperature Sensor
uint16_t MB_WAKD_BTS_InletTemperature = 0;
uint16_t MB_WAKD_BTS_OutletTemperature = 0;
// WAKD Pressure Sensors
uint16_t MB_WAKD_BPS_Pressure = 0;
uint16_t MB_WAKD_FPS_Pressure = 0;
// WAKD Conductivity Sensor
uint16_t MB_WAKD_DCS_Conductance = 0;
uint16_t MB_WAKD_DCS_Susceptance = 0;
// WAKD High Flux Dialyser
uint8_t MB_WAKD_HFD_Status = 0;
uint8_t MB_WAKD_HFD_PercentOK = 0;
// WAKD Sorbent Unit
uint16_t MB_WAKD_SU_Status = 0;
// WAKD Polarizer
uint16_t MB_WAKD_POLAR_Voltage = 0;
uint16_t MB_WAKD_POLAR_Status = 0;
// WAKD DEVICES
uint16_t MB_WAKD_ECP1_Status = 0;
uint16_t MB_WAKD_ECP2_Status = 0;
uint32_t MB_WAKD_PS_Status = 0;
uint32_t MB_WAKD_ACT_Status = 0;

// WAKD FEEDBACK FROM UIF COMMANDS
uint16_t MB_CMDS_Register = 0x0000;

// WAKD Alarms, Errors, Link Test
uint8_t MB_WAKD_ALARMS_Status[4] = {0, 0, 0, 0};
uint8_t MB_WAKD_ERRORS_Status[4] = {0, 0, 0, 0};
uint16_t MB_WAKD_UIF_LinkTest = 0;


// UIF Information
//uint16_t UIF_CMDS_Register = 0x1234;
uint16_t UIF_CMDS_Register = 0x0000;
//uint16_t UIF_OPMS_Register = 0x5555;
uint16_t UIF_MSGS_Register = 0x4321;


// data received flags
boolean_t spi_bp1_voltage_ok = FALSE;
boolean_t spi_bp2_voltage_ok = FALSE;
boolean_t spi_bp1_remainingcapacity_ok = FALSE;
boolean_t spi_bp2_remainingcapacity_ok = FALSE;
boolean_t spi_wakd_status_ok = FALSE;
boolean_t spi_wakd_attitude_ok = FALSE;
boolean_t spi_clinks_status_ok = FALSE;
boolean_t spi_wakd_operatingmode_ok = FALSE;
boolean_t spi_wakd_blcircuit_status_ok = FALSE;
boolean_t spi_wakd_flcircuit_status_ok = FALSE;
boolean_t spi_wakd_blpump_info_ok = FALSE;
boolean_t spi_wakd_flpump_info_ok = FALSE;
boolean_t spi_wakd_bts_data_ok = FALSE;
boolean_t spi_wakd_bps_data_ok = FALSE;
boolean_t spi_wakd_fps_data_ok = FALSE;
boolean_t spi_wakd_dcs_data_ok = FALSE;
boolean_t spi_wakd_hfd_status_ok = FALSE;
boolean_t spi_wakd_su_status_ok = FALSE;
boolean_t spi_wakd_polar_data_ok = FALSE;
boolean_t spi_wakd_ecps_status_ok = FALSE;
boolean_t spi_wakd_ps_status_ok = FALSE;
boolean_t spi_wakd_act_status_ok = FALSE;
boolean_t spi_wakd_pdata_firstname_ok = FALSE;
boolean_t spi_wakd_pdata_lastname_ok = FALSE;
boolean_t spi_wakd_pdata_genderage_ok = FALSE;
boolean_t spi_wakd_pdata_weight_ok = FALSE;
boolean_t spi_wakd_pdata_hr_ok = FALSE;
boolean_t spi_wakd_pdata_br_ok = FALSE;
boolean_t spi_wakd_pdata_activity_ok = FALSE;
boolean_t spi_wakd_pdata_bloodpressure_ok = FALSE;
boolean_t spi_nibp_data_ok = FALSE;
boolean_t spi_wakd_ecp1_a_data_ok = FALSE;
boolean_t spi_wakd_ecp1_b_data_ok = FALSE;
boolean_t spi_wakd_ecp2_a_data_ok = FALSE;
boolean_t spi_wakd_ecp2_b_data_ok = FALSE;
boolean_t spi_wakd_alarms_status_ok = FALSE;
boolean_t spi_wakd_errors_status_ok = FALSE;
#if 0
boolean_t spi_wakd_cmd_shutdown_ok = FALSE;
boolean_t spi_wakd_cmd_start_op_ok = FALSE;
boolean_t spi_wakd_cmd_mode_op_ok = FALSE;
boolean_t spi_wakd_cmd_getweight_ok = FALSE;
boolean_t spi_wakd_cmd_start_sew_ok = FALSE;
boolean_t spi_wakd_cmd_stop_sew_ok = FALSE;
boolean_t spi_wakd_cmd_start_nibp_ok = FALSE;
#endif

// Detect that the file has been closed before switching off -------------------------
//uint8_t file_is_closed = 0;
//uint8_t switch_off_timeout = 0;

// USCI B0 USCI B1 TESTS -------------------------------------------------------------
uint8_t *UIFTxData;                           // Pointer to TX data
uint8_t MST_Data;
uint8_t start_rx_ok = 0;
uint8_t stop_rx_ok = 0;
uint8_t usci_b0_counter = 0;
uint8_t usci_b0_received = 0;
uint8_t usci_b0_data_rx[UB0_LEN];
uint8_t usci_b0_data_tx[UB0_LEN];

// Status & Error Variables ----------------------------------------------------------

// SOURCE: ARM (to be revised) -------------------------------------------------------

// all the information in one variable -----------------------------------------------
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Local function prototypes
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Inline code definitions 
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  usci_b0_ClearData
//!
//! ....
//!
//! \param   ....
//! \return  ....
// -----------------------------------------------------------------------------------
void usci_b0_ClearData(){
    for(usci_b0_counter=0;usci_b0_counter<UB0_LEN;usci_b0_counter++) {
      usci_b0_data_tx[usci_b0_counter] =  0x61+usci_b0_counter; 
      usci_b0_data_rx[usci_b0_counter] =  0x00;
    }    
    usci_b0_counter = 0;
}

// -----------------------------------------------------------------------------------
//! \brief  Calculate_WAKDAttitude_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdAttitude
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_WAKDAttitude_Code (uint8_t uif_wakdAttitude){
  switch (uif_wakdAttitude){
      case WAKD_ATTITUDE_OK:
      return 0;
      case WAKD_ATTITUDE_KO:
      return 1;
      case WAKD_ATTITUDE_NOINFO:
      default:
      return 2;      
  }  
}
// -----------------------------------------------------------------------------------
//! \brief  Calculate_WAKDOperatingMode_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdOperatingMode
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_WAKDOperatingMode_Code (uint8_t uif_wakdOperatingMode){
    switch (uif_wakdOperatingMode){
      case MICROFLUIDIC_DIALYSIS:
      return 1;
      case MICROFLUIDIC_REGENERATION_1:
      return 2;      
      case MICROFLUIDIC_ULTRAFILTRATION:
      return 3;      
      case MICROFLUIDIC_REGENERATION_2:
      return 4;      
      case MICROFLUIDIC_MAINTENANCE:
      return 5;      
      case MICROFLUIDIC_STOP:
      default:
      return 0;
  } 
}  

// -----------------------------------------------------------------------------------
//! \brief  Calculate_MBCLINKS_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdOperatingMode
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_MBCLINKS_Code (uint8_t uif_mbclinks){
    switch (uif_mbclinks){
      case COMMLINKS_BT:
      return 1;      
      case COMMLINKS_USB:
      return 2;      
      case COMMLINKS_BOTH:
      return 3;
      case COMMLINKS_NONE:
      default:
      return 0;
  } 
}  
// -----------------------------------------------------------------------------------
//! \brief  Calculate_WAKDCSensorStatus_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_dcsstatus
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_WAKDCSensorStatus_Code (uint8_t uif_dcsstatus){
    switch (uif_dcsstatus){
      case SENSOR_OK:
      return 0;     
      case SENSOR_KO:
      return 1;     
      case SENSOR_NOINFO:
      default:
      return 2;
  }  
}

// -----------------------------------------------------------------------------------
//! \brief  Calculate_MFCircuit_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_mfcircstatus
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_MFCircuit_Code (uint8_t uif_mfcircstatus){
    switch (uif_mfcircstatus){
      case DEVICE_OK:
      return 0;     
      case DEVICE_KO:
      return 1;     
      case DEVICE_NOINFO:
      default:
      return 2;
  }  
}
// -----------------------------------------------------------------------------------
//! \brief  Calculate_HFDstatus_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdAttitude
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_HFDstatus_Code (uint8_t uif_hfdstatus){
  switch (uif_hfdstatus){
      case DEVICE_OK:
      return 0;
      case DEVICE_KO:
      return 1;
      case DEVICE_NOINFO:
        default:
      return 2;      
  }  
}
// -----------------------------------------------------------------------------------
//! \brief  Calculate_SUstatus_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdAttitude
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_SUstatus_Code (uint8_t uif_sustatus){
  switch (uif_sustatus){
      case DEVICE_OK:
      return 0;
      case DEVICE_KO:
      return 1;
      case DEVICE_NOINFO:
        default:
      return 2;      
  }  
}


// -----------------------------------------------------------------------------------
//! \brief  Calculate_POLARstatus_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdAttitude
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_POLARstatus_Code (uint8_t uif_polarstatus){
  switch (uif_polarstatus){
      case DEVICE_OK:
      return 0;
      case DEVICE_KO:
      return 1;
      case DEVICE_NOINFO:
        default:
      return 2;      
  }  
}
// -----------------------------------------------------------------------------------
//! \brief  Calculate_ECPSstatus_Code
//!         ...
//!
//! Returns an index to the pre-fixed table depending on the received value
//!
//! \param[in]      Arg_1 : uif_wakdAttitude
//! \return         index to the table
// -----------------------------------------------------------------------------------
uint8_t Calculate_ECPSstatus_Code (uint8_t uif_ecpstatus){
  switch (uif_ecpstatus){
      case DEVICE_OK:
      return 0;
      case DEVICE_KO:
      return 1;
      case DEVICE_NOINFO:
        default:
      return 2;      
  }  
}
// -----------------------------------------------------------------------------------
//! \brief  buzzer_Test
//!         ...
//!
//! Toggles BUZZER 1000 times, period 500uS
//!
//! \param[in]      Arg_1 : 1st parameter
//! \param[out]     Arg_2 : 2nd parameter
//! \param[in,out]  Arg_3 : 3rd parameter
//! \return         Description of the return value
// -----------------------------------------------------------------------------------
void buzzer_Test(){
    // Test buzzer
    uint16_t i=0;
    for(i=0; i<1000; i++)
    {
        IO_BUZZER_HIGH;
        DELAY_US(500);
        IO_BUZZER_LOW;
        DELAY_US(500);
    }
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  LCD_UPDATE_VARIABLES
//!         ...
//!
//! Updates received variables if necessary
//!
//! \return         none
// -----------------------------------------------------------------------------------
void   LCD_UPDATE_VARIABLES(){
    uint8_t x = 0;
    if(spi_bp1_voltage_ok == TRUE){
        UIF_BPACK_1_Percent = MB_BPACK_1_Percent;
        spi_bp1_voltage_ok = FALSE;
    }
    if(spi_bp2_voltage_ok == TRUE){
        UIF_BPACK_2_Percent = MB_BPACK_2_Percent;
        spi_bp2_voltage_ok = FALSE;
    }
    if(spi_wakd_status_ok == TRUE){
        UIF_WAKD_Status = MB_WAKD_Status;
        spi_wakd_status_ok = FALSE;
    }
    if(spi_wakd_attitude_ok == TRUE){
        UIF_WAKD_Attitude = MB_WAKD_Attitude;
        spi_wakd_attitude_ok = FALSE;
    }
    if(spi_wakd_operatingmode_ok == TRUE){
        UIF_WAKD_OperatingMode = MB_WAKD_OperatingMode;
      
    }
    if(spi_wakd_bts_data_ok==TRUE){
        UIF_BTS_InletTemperature = MB_WAKD_BTS_InletTemperature;
        UIF_BTS_OutletTemperature = MB_WAKD_BTS_OutletTemperature;
        spi_wakd_bts_data_ok = FALSE;
    }
    if(spi_wakd_bps_data_ok==TRUE){
        UIF_BPS_Pressure = MB_WAKD_BPS_Pressure;
        spi_wakd_bps_data_ok = FALSE;
    }
    if(spi_wakd_fps_data_ok==TRUE){
        UIF_FPS_Pressure = MB_WAKD_FPS_Pressure;
        spi_wakd_fps_data_ok = FALSE;
    }  
    if(spi_wakd_blpump_info_ok==TRUE){
        UIF_BLPump_SpeedCode = MB_WAKD_BLPump_SpeedCode;
        spi_wakd_blpump_info_ok = FALSE;
    }
    if(spi_wakd_flpump_info_ok==TRUE){
        UIF_FLPump_SpeedCode = MB_WAKD_FLPump_SpeedCode;
        spi_wakd_flpump_info_ok = FALSE;
    }
    if(spi_clinks_status_ok == TRUE){
        UIF_MB_CLINKS_Status = MB_CLINKS_Status;
        spi_clinks_status_ok = FALSE;
    }
    if(spi_wakd_dcs_data_ok == TRUE){
        UIF_DCS_Conductance = MB_WAKD_DCS_Conductance;        
        UIF_DCS_Susceptance = MB_WAKD_DCS_Susceptance;  
        spi_wakd_dcs_data_ok = FALSE;
    }
    if(spi_wakd_blcircuit_status_ok == TRUE){
        UIF_WAKD_BLCircuit_Status = MB_WAKD_BLCircuit_Status;
        spi_wakd_blcircuit_status_ok = FALSE;
    }
    if(spi_wakd_flcircuit_status_ok == TRUE){
        UIF_WAKD_FLCircuit_Status = MB_WAKD_FLCircuit_Status;
        spi_wakd_flcircuit_status_ok = FALSE;
    }
    if(spi_wakd_hfd_status_ok == TRUE){
        UIF_WAKD_HFD_Status = MB_WAKD_HFD_Status;
        UIF_WAKD_HFD_PercentOK = MB_WAKD_HFD_PercentOK;
        spi_wakd_hfd_status_ok = FALSE;
    }
    if(spi_wakd_su_status_ok == TRUE){
        UIF_WAKD_SU_Status = MB_WAKD_SU_Status;
        spi_wakd_su_status_ok = FALSE;
    }
    if(spi_wakd_polar_data_ok == TRUE){
        UIF_WAKD_POLAR_Status = MB_WAKD_POLAR_Status;
        UIF_WAKD_POLAR_Voltage = MB_WAKD_POLAR_Voltage;
        spi_wakd_polar_data_ok = FALSE;
    }  
    if(spi_wakd_ecps_status_ok == TRUE){
        UIF_WAKD_ECP1_Status = MB_WAKD_ECP1_Status; 
        UIF_WAKD_ECP2_Status = MB_WAKD_ECP2_Status; 
        spi_wakd_ecps_status_ok = FALSE;
    }
    if(spi_wakd_ps_status_ok == TRUE){
        UIF_WAKD_PS_Status = MB_WAKD_PS_Status; 
        spi_wakd_ps_status_ok = FALSE;
    }
    if(spi_wakd_act_status_ok == TRUE){
        UIF_WAKD_ACT_Status = MB_WAKD_ACT_Status; 
        spi_wakd_act_status_ok = FALSE;
    }
    if(spi_wakd_ecp1_a_data_ok == TRUE){
        UIF_P_DATA_NA_ECP1 = MB_P_DATA_NA_ECP1;                      
        UIF_P_DATA_K_ECP1 = UIF_P_DATA_K_ECP1;                       
        spi_wakd_ecp1_a_data_ok = FALSE;
    }
    if(spi_wakd_ecp1_b_data_ok == TRUE){
        UIF_P_DATA_PH_ECP1 = MB_P_DATA_PH_ECP1;                      
        UIF_P_DATA_UREA_ECP1 = UIF_P_DATA_UREA_ECP1;                       
        spi_wakd_ecp1_b_data_ok = FALSE;
    }
    if(spi_wakd_ecp2_a_data_ok == TRUE){
        UIF_P_DATA_NA_ECP2 = MB_P_DATA_NA_ECP2;                      
        UIF_P_DATA_K_ECP2 = UIF_P_DATA_K_ECP2;                       
        spi_wakd_ecp2_a_data_ok = FALSE;
    }
    if(spi_wakd_ecp2_b_data_ok == TRUE){
        UIF_P_DATA_PH_ECP2 = MB_P_DATA_PH_ECP1;                      
        UIF_P_DATA_UREA_ECP2 = UIF_P_DATA_UREA_ECP1;                       
        spi_wakd_ecp2_b_data_ok = FALSE;
    }
//    if(spi_wakd_errors_status_ok == TRUE){
//        UIF_WAKD_ERRORS_Status = MB_WAKD_ERRORS_Status;     
//        spi_wakd_errors_status_ok = FALSE;
//    }
    if(spi_wakd_pdata_firstname_ok == TRUE){
        for(x=0; x< 4;x++) UIF_P_DATA_INITIALS[x] = MB_P_DATA_INITIALS[x];
        spi_wakd_pdata_firstname_ok = FALSE;
    }
    if(spi_wakd_pdata_lastname_ok == TRUE){
        for(x=0; x< 4;x++) UIF_P_DATA_PATIENTCODE[x] = MB_P_DATA_PATIENTCODE[x];
        spi_wakd_pdata_lastname_ok = FALSE;
    }
    if(spi_wakd_pdata_genderage_ok == TRUE){
        UIF_P_DATA_GENDER[0] = MB_P_DATA_GENDER[0];
        UIF_P_DATA_AGE = MB_P_DATA_AGE;
        spi_wakd_pdata_genderage_ok = FALSE;
    }
    if(spi_wakd_pdata_weight_ok == TRUE){
        UIF_P_DATA_WEIGHT = MB_P_DATA_WEIGHT;
        spi_wakd_pdata_weight_ok = FALSE;
    }
    if(spi_wakd_pdata_hr_ok == TRUE){
        UIF_P_DATA_HEARTRATE = MB_P_DATA_HEARTRATE;                  
        spi_wakd_pdata_hr_ok = FALSE;
    }
    if(spi_wakd_pdata_br_ok == TRUE){
        UIF_P_DATA_BREATHINGRATE = MB_P_DATA_BREATHINGRATE;                  
        spi_wakd_pdata_br_ok = FALSE;
    }
    if(spi_wakd_pdata_activity_ok == TRUE){
        UIF_P_DATA_ACTIVITYCODE = MB_P_DATA_ACTIVITYCODE;
        spi_wakd_pdata_activity_ok = FALSE;
    }
    if(spi_wakd_pdata_bloodpressure_ok == TRUE){
        UIF_P_DATA_SISTOLICBP = MB_P_DATA_SISTOLICBP;
        UIF_P_DATA_DIASTOLICBP = MB_P_DATA_DIASTOLICBP;
        spi_wakd_pdata_bloodpressure_ok = FALSE;
    }

}
// -----------------------------------------------------------------------------------------
//! \brief  MB_SET_COMMAND_FLAG
//!
//! ADDS THE NEW COMMAND FLAG (UIF_CMDS_Register)
//!
//! \param    command_index (index to the command)
//! \return   none
// -----------------------------------------------------------------------------------------
void MB_SET_COMMAND_FLAG(uint16_t command_index){
  
  uint16_t operatingmode0 = 0;
  uint16_t operatingmode1 = 0;
  
  switch(command_index){
  case UIF_COMMAND_SHUTDOWN:
    UIF_CMDS_Register |= MB_COMMAND_SHUTDOWN;
    break;
  case UIF_COMMAND_START_OPERATION:
    UIF_CMDS_Register |= MB_COMMAND_START_OPERATION;
    operatingmode0 = UIF_WAKD_OperatingMode;
    operatingmode1 = (operatingmode0 << 8)& 0xF000;
    UIF_CMDS_Register |= operatingmode1;
    break;
  case UIF_COMMAND_GET_WEIGHT:
    UIF_CMDS_Register |= MB_COMMAND_GET_WEIGHT;
    break;
  case UIF_COMMAND_START_SEWSTREAM:
    UIF_CMDS_Register |= MB_COMMAND_START_SEWSTREAM;
    break;
  case UIF_COMMAND_STOP_SEWSTREAM:
    UIF_CMDS_Register |= MB_COMMAND_STOP_SEWSTREAM;
    break;
  case UIF_COMMAND_START_NIBP:
    UIF_CMDS_Register |= MB_COMMAND_START_NIBP;     
    break;
  case UIF_COMMAND_GOTO_DIALYSIS:
    UIF_CMDS_Register |= MB_COMMAND_CHANGE_OPMODE;
    UIF_CMDS_Register |= MB_COMMAND_GOTO_DIALYSIS;
    break;
  case UIF_COMMAND_GOTO_REGEN1:
    UIF_CMDS_Register |= MB_COMMAND_CHANGE_OPMODE;
    UIF_CMDS_Register |= MB_COMMAND_GOTO_REGEN1;
    break;
  case UIF_COMMAND_GOTO_ULTRA:
    UIF_CMDS_Register |= MB_COMMAND_CHANGE_OPMODE;
    UIF_CMDS_Register |= MB_COMMAND_GOTO_ULTRA;
    break;
  case UIF_COMMAND_GOTO_REGEN2:
    UIF_CMDS_Register |= MB_COMMAND_CHANGE_OPMODE;
    UIF_CMDS_Register |= MB_COMMAND_GOTO_REGEN2;
    break;
  default:
    break;
  }
}
// -----------------------------------------------------------------------------------
