package eu.nephron.nephronbridge;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import eu.nephron.bridge.connection.bt.ConnectionService;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //start the connection service
        if(!ConnectionService.connection) {
    		Intent start = new Intent("nephron.mobile.application.ConnectionService");
    		this.startService(start);      
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
}
