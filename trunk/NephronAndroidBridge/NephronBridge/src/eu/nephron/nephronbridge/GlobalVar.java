package eu.nephron.nephronbridge;

import java.util.concurrent.CopyOnWriteArrayList;

import android.app.Application;
import de.imst.nephron.bt.BluetoothAbstractionLayer;

public class GlobalVar extends Application {

private int messageCounter = 1;
	
	private BluetoothAbstractionLayer u;
	
	private boolean ConnectionServiceStarted = false;
	private boolean UserAuthenticated = false;
	private boolean BackendAuthenticated = false;
	
	private boolean alarmWindowOpen;
	private boolean OperationInProgress = false;

	

	
	public int getMessageCounter() {
		return messageCounter++;
	}

	public BluetoothAbstractionLayer getBluetoothAbstractionLayer() {
		return u;
	}
	
	public void setBluetoothAbstractionLayer(BluetoothAbstractionLayer u) {
		this.u=u;
	}

	public boolean isConnectionServiceStarted() {
		return ConnectionServiceStarted;
	}

	public void setConnectionServiceStarted(boolean connectionServiceStarted) {
		ConnectionServiceStarted = connectionServiceStarted;
	}

	public boolean isUserAuthenticated() {
		return UserAuthenticated;
	}

	public void setUserAuthenticated(boolean userAuthenticated) {
		UserAuthenticated = userAuthenticated;
	}

	public boolean isBackendAuthenticated() {
		return BackendAuthenticated;
	}

	public void setBackendAuthenticated(boolean backendAuthenticated) {
		BackendAuthenticated = backendAuthenticated;
	}

	
	public boolean isAlarmWindowOpen() {
		return alarmWindowOpen;
	}

	public void setAlarmWindowOpen(boolean alarmWindowOpen) {
		this.alarmWindowOpen = alarmWindowOpen;
	}

	
	public boolean isOperationInProgress() {
		return OperationInProgress;
	}

	public void setOperationInProgress(boolean operationInProgress) {
		OperationInProgress = operationInProgress;
	}

}