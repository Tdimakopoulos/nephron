package eu.nephron.bridge.soap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.util.Xml;

public class BackendCommandsXmlWriter {

	boolean exists = false;
	int measurementId = 0;
	String rootDir;
	File xmlFile;
	XmlSerializer serializer;
	StringWriter writer;
	BufferedWriter out;
	String xmlSource;

	//Constructor
	public BackendCommandsXmlWriter() {
		File folder = new File(Environment.getExternalStorageDirectory().toString()+"/Nephron");
		if(!folder.exists())
			folder.mkdirs();

		//Save the path as a string value
		String extStorageDirectory = folder.toString();

		//Create New file and name it Image2.PNG
		this.xmlFile = new File(extStorageDirectory, "BackendData.xml");
//			this.exists = xmlFile.createNewFile();
		this.serializer = Xml.newSerializer();
	}

	public void writeBackendXmlFile(Long heartRate, Long respirationRate, Long activityLevel, String measurementDate) {
		FileOutputStream outstream;
//		if(!exists) {
			this.measurementId++;
			try {
				outstream = new FileOutputStream(xmlFile);
				serializer.setOutput(outstream, "UTF-8");
				serializer.startDocument(null, Boolean.valueOf(true));
				serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

				serializer.startTag(null, "BackendCommands");

				serializer.startTag(null, "BackendCommand");
				serializer.attribute(null, "id", String.valueOf(this.measurementId));

				serializer.startTag(null, "heartRate");
				serializer.attribute(null, "value", heartRate.toString());
				serializer.endTag(null, "heartRate");

				serializer.startTag(null, "respirationRate");
				serializer.attribute(null, "value", respirationRate.toString());
				serializer.endTag(null, "respirationRate");

				serializer.startTag(null, "activityLevel");
				serializer.attribute(null, "value", activityLevel.toString());
				serializer.endTag(null, "activityLevel");

				serializer.startTag(null, "measurementDate");
				serializer.attribute(null, "value", measurementDate);
				serializer.endTag(null, "measurementDate");

				serializer.endTag(null, "BackendCommand");

				serializer.endTag(null, "BackendCommands");

				serializer.endDocument();
				serializer.flush();
				outstream.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}


//		}
//		else {	//	Case when the file is present and
//			//	measurements have been added...
//			try {
//				instream = new FileInputStream(xmlFile);
//				//				instream.
//
//				//TODO Add code here to handle 
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			}	
//		}

	}



	//	case to be used when we save ECG measurements to an array and then
	//	write the corresponding values to XML
	public void writeECGXmlFileArray(List<HashMap<String, Long>> measurements)  {

		FileOutputStream outstream;
		if(!exists) {
			try {
				outstream = new FileOutputStream(xmlFile);
				serializer.setOutput(outstream, "UTF-8");
				serializer.startDocument(null, Boolean.valueOf(true));
				serializer.setFeature("", true);
				serializer.startTag(null, "ECGData");

				for(HashMap<String,Long> measurement : measurements) {

					this.measurementId++;

					serializer.startTag(null, "ECGMeasurement");
					serializer.attribute(null, "id", String.valueOf(measurementId));

					serializer.startTag(null, "heartRate");
					serializer.attribute(null, "value", measurement.get("heartRate").toString());
					serializer.endTag(null, "heartRate");

					serializer.startTag(null, "respirationRate");
					serializer.attribute(null, "value", measurement.get("respirationRate").toString());
					serializer.endTag(null, "respirationRate");

					serializer.startTag(null, "activityLevel");
					serializer.attribute(null, "value", measurement.get("activityLevel").toString());
					serializer.endTag(null, "activityLevel");

					serializer.startTag(null, "measurementDate");
					serializer.attribute(null, "value", measurement.get("measurementDate").toString());
					serializer.endTag(null, "measurementDate");

					serializer.endTag(null, "ECGMeasurement");

				}

				serializer.endTag(null, "ECGData");

				serializer.endDocument();
				serializer.flush();
				outstream.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public boolean ECGXmlFileExists() {
		return exists;
	}

}
