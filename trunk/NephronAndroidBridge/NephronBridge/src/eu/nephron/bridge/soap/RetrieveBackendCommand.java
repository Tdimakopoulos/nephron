package eu.nephron.bridge.soap;

import java.util.TimerTask;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.telephony.TelephonyManager;
import eu.nephron.nephronbridge.GlobalVar;

public class RetrieveBackendCommand extends TimerTask {

	Context context;
	// BackendInteractionHandler bhandler;
	final int BACKEND_SHUTDOWN = 1;

	public RetrieveBackendCommand(Context context) {
		super();
		this.context = context;
		// this.bhandler = new BackendInteractionHandler(this.context);
	}

	@Override
	public void run() {

		NephronSOAPClient soapClient = new NephronSOAPClient(context);
		String commandResponse = soapClient
				.executeRetrieveCommandRequest(((TelephonyManager) context
						.getSystemService(Context.TELEPHONY_SERVICE))
						.getDeviceId());
		// if(commandResponse != null) {
		// Message msg = decodeBackendCommand(commandResponse);
		// msg.what = BACKEND_SHUTDOWN;
		// msg.setTarget(bhandler);
		// if(!((GlobalVar)context.getApplicationContext()).isOperationInProgress())
		// {
		// ((GlobalVar)context.getApplicationContext()).setOperationInProgress(true);
		// msg.sendToTarget();
		// }
	}

	private Message decodeBackendCommand(String retrievedCommand) {

		// int sepIndex = retrievedCommand.indexOf(":")+1;
		// String commandID = retrievedCommand.substring(0,
		// retrievedCommand.indexOf(":"));
		// String command = retrievedCommand.substring(sepIndex,
		// retrievedCommand.length()).substring(6, 8);
		// Bundle b = new Bundle();
		// b.putString("commandID", commandID);
		// b.putString("command", command);
		// Message msg = new Message();
		// msg.setData(b);
		// return msg;

		int sepIndex = retrievedCommand.indexOf(":") + 1;
		String commandID = retrievedCommand.substring(0,
				retrievedCommand.indexOf(":"));
		String command = retrievedCommand.substring(sepIndex,
				retrievedCommand.length());
		Bundle b = new Bundle();
		b.putString("commandID", commandID);
		b.putString("command", command);
		Message msg = new Message();
		msg.setData(b);
		return msg;
	}

}