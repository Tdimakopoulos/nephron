package eu.nephron.bridge.soap;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

public class NephronSOAPClient {

	private Context context;

	public NephronSOAPClient(Context context) {
		this.context = context;
	}


	public String executeWeightRequest(String weightData) {

		String nameSpace = "http://weight.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/WeightManager";
		String soapAction = "http://weight.nephron.eu/WeightManager/WeightManagerNewValueReceived";
		String methodName = "WeightManagerNewValueReceived";

		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

		SoapObject request = new SoapObject(nameSpace, methodName);

		request.addProperty("imei", deviceID);
		request.addProperty("date", String.valueOf(new Date().getTime()));
		request.addProperty("reply", weightData);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				Log.d("INSIDE THE SOAP QUESTION CLIENT", "FINISHED BACKEND QUESTION REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		Log.d("INSIDE THE SOAP QUESTION CLIENT", "FINISHED BACKEND QUESTION REQUEST, RETURN NULL");
		return null;
	}
	
	public String executeQuestionnaireRequest(String questionid, String answer) {

		String nameSpace = "http://questionary.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/QuestionaryManager";
		String soapAction = "http://questionary.nephron.eu/QuestionaryManager/QuestionaryManagerSaveQuestionReply";
		String methodName = "QuestionaryManagerSaveQuestionReply";

		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

		SoapObject request = new SoapObject(nameSpace, methodName);

		request.addProperty("questionid", questionid);
		request.addProperty("imei", deviceID);
		request.addProperty("questionarydate", String.valueOf(new Date().getTime()));
		request.addProperty("questionreply", answer);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				Log.d("INSIDE THE SOAP QUESTION CLIENT", "FINISHED BACKEND QUESTION REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		Log.d("INSIDE THE SOAP QUESTION CLIENT", "FINISHED BACKEND QUESTION REQUEST, RETURN NULL");
		return null;
	}

	
	public String executeACKRequest(Long commandID) {

		//	successful ACK returns "Command Updated"
		//	unsuccessful ACK returns "Command ID does not exist"

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/MarkACK";
		String methodName = "MarkACK";

		SoapObject request = new SoapObject(nameSpace, methodName);
		request.addProperty("commandid", commandID);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				//	Log.d("INSIDE THE SOAP ACK CLIENT", "FINISHED BACKEND ACK REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		//		Log.d("INSIDE THE SOAP ACK CLIENT", "FINISHED BACKEND ACK REQUEST, RETURN NULL");
		return null;
	}


	public String executeUploadFileRequest(String imei, String file) {

		String nameSpace = "http://ws.file.nephron.eu/";
		String URL = "http://178.63.69.149:8787/NephronFileAttachments/NephronECGManagerWS";
		String soapAction = "http://ws.file.nephron.eu/NephronECGManagerWS/ECGTransfer";
		String methodName = "ECGTransfer";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("ECG", file);
		request.addProperty("Filename", "/home/exodus/nephron/wsfile/" + deviceID+"_" + new Date().getTime() + ".ecg");
		request.addProperty("IMEI", deviceID);
		request.addProperty("Date", new Date().getTime());
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				//				Log.d("INSIDE THE SOAP UPLOAD FILE CLIENT", "FINISHED UPLOAD FILE REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		//		Log.d("INSIDE THE SOAP UPLOAD FILE CLIENT", "FINISHED UPLOAD FILE REQUEST, RETURN NULL");
		return null;
	}

	public String executeRetrieveCommandRequest(String imei) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/RetrieveCommand";
		String methodName = "RetrieveCommand";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				//				Log.d("INSIDE THE SOAP RETRIEVE COMMAND CLIENT", "FINISHED RETRIEVE COMMAND REQUEST, RETURN STH");
				return response.toString();
			}
		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
			return null;
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
			return null;
		}
		//		Log.d("INSIDE THE SOAP RETRIEVE COMMAND CLIENT", "FINISHED RETRIEVE COMMAND REQUEST, RETURN NULL");
		return null;
	}



	public String executeBackendLoginRequest(Object param) {

		String nameSpace = "http://webservices.nephron.eu/";
		String URL = "http://178.63.69.149:8787/PKI-Infastructure/KeyManagerService";
		String soapAction = "http://webservices.nephron.eu/KeyManager/GetUserRole";
		String methodName = "GetUserRole";

		SoapObject request = new SoapObject(nameSpace, methodName);
		PropertyInfo attachedParam = new PropertyInfo();
		attachedParam.setName("key");
		attachedParam.setValue(param);
		attachedParam.setType(String.class);
		request.addProperty(attachedParam);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				//				Log.d("INSIDE THE BACKEND LOGIN CLIENT", "FINISHED BACKEND LOGIN REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
		}
		//		Log.d("INSIDE THE BACKEND LOGIN CLIENT", "FINISHED BACKEND LOGIN REQUEST, RETURN NULL");
		return null;
	}

	public String executeStoreCommandRequest(String hexCommand, long date, List<String> restArgs) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/StoreCommand";
		String methodName = "StoreCommand";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		request.addProperty("command", hexCommand);
		request.addProperty("date", date);

		if(restArgs != null) {
			if(!restArgs.isEmpty()) {
				int counter =1;
				for(String arg : restArgs) {
					request.addProperty("param" + counter, arg);
					counter++;
				}
			}
		}
		else{

			//TODO Change the webservice so that it accepts null values as well

			request.addProperty("param1", ".");
			request.addProperty("param2", ".");
			request.addProperty("param3", ".");
			request.addProperty("param4", ".");
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
			if(response!= null) {
				//				Log.d("INSIDE THE SOAP STORE COMMAND CLIENT", "FINISHED STORE COMMAND REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
		}
		//		Log.d("INSIDE THE SOAP STORE COMMAND CLIENT", "FINISHED STORE COMMAND REQUEST, RETURN NULL");
		return null;
	}


	public String executeStoreWAKDStatusRequest(String hexCommand, long date, List<String> restArgs) {

		String nameSpace = "http://webservices.spcm.nephron.eu/";
		String URL = "http://178.63.69.149:8787/SmartPhoneCommunicationManager/CommunicationManager";
		String soapAction = "http://webservices.spcm.nephron.eu/CommunicationManager/StoreWAKD";
		String methodName = "StoreWAKD";

		SoapObject request = new SoapObject(nameSpace, methodName);
		String deviceID  = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		request.addProperty("imei", deviceID);
		request.addProperty("wakdstatus", hexCommand);
		request.addProperty("date", date);

		if(restArgs != null) {
			if(!restArgs.isEmpty()) {
				int counter =1;
				for(String arg : restArgs) {
					request.addProperty("param"+counter, arg);
					counter++;
				}
			}
		}
		else{

			//TODO Change the webservice so that it accepts null values as well

			request.addProperty("param1", ".");
			request.addProperty("param2", ".");
			request.addProperty("param3", ".");
			request.addProperty("param4", ".");
		}

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

		try {
			androidHttpTransport.call(soapAction, envelope);
			SoapPrimitive response = (SoapPrimitive) envelope.getResponse();

			if(response!= null) {
				//				Log.d("INSIDE THE WAKD STATUS CLIENT", "FINISHED STORE WAKD STATUS REQUEST, RETURN STH");
				return response.toString();
			}

		} catch (IOException e) {
			Log.d("SOAP IO EXCEPTION", "SOAP IO EXCEPTION");
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			Log.d("SOAP XML PARSER EXCEPTION", "SOAP XML PARSER EXCEPTION");
			e.printStackTrace();
		}
		//		Log.d("INSIDE THE WAKD STATUS CLIENT", "FINISHED STORE WAKD STATUS REQUEST, RETURN NULL");
		return null;
	}

}
