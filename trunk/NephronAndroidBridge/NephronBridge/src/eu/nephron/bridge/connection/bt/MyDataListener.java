package eu.nephron.bridge.connection.bt;
 
import android.content.Context;
import android.content.Intent;
import de.imst.nephron.bt.BluetoothAbstractionLayer.DataListener;

public class MyDataListener implements DataListener {

	byte[] message,outputBytes,tempOutputBytes,tempMessage;
	
	int i = 0;
	boolean waitForMessage = false;

	Context context;

	public MyDataListener(Context context) {
		this.context = context;
	}

//	@Override
	public void onDataReceived(byte[] data) {

	
		message = new byte[0]; 
		i++;

		message = data;
		
		if(message.length > 5 && unsignedByteToInt(message[0]) == 4){
			Intent i = new Intent("nephron.mobile.application.DataArrived");
			i.putExtra("value", message);
			this.context.sendBroadcast(i);
		}
		 
		
		 
	 
		
		
	}

	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

}
