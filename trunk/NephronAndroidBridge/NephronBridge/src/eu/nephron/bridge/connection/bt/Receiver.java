package eu.nephron.bridge.connection.bt;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;
import eu.nephron.bridge.soap.NephronSOAPClient;
import eu.nephron.bridge.utils.DataTransformationUtils;
import eu.nephron.nephronbridge.GlobalVar;

public class Receiver extends BroadcastReceiver {

	String current_State;
	
	boolean wait_for_update = false;
	

	@SuppressWarnings("unchecked")
	@Override
	public void onReceive(Context context, Intent intent) {
		
		String action = intent.getAction(); 

		if (action.equals("nephron.mobile.application.DataArrived")) {
			byte[] incomingMessage = intent.getByteArrayExtra("value");
			
			
			
			if (incomingMessage != null) {
				
				if (unsignedByteToInt(incomingMessage[3]) == 5) {  // ACK
					Intent i2 = new Intent("nephron.mobile.application.ACK"); 
					i2.putExtra("messageCount", unsignedByteToInt(incomingMessage[4]));
					context.sendBroadcast(i2);
				}
				else if (unsignedByteToInt(incomingMessage[3]) == 4) { // CurrentWakdStatus
					if(incomingMessage.length == 8) {
				
						createClientforWAKDStatus(context, incomingMessage);

				
					}
				}

				else if (unsignedByteToInt(incomingMessage[3]) == 8) { // ALERT
					if(incomingMessage.length == 8){
				

						createClientforStoreCommand(context, incomingMessage);

									}
				} else if (unsignedByteToInt(incomingMessage[3]) == 12) { // Physiological
					if(incomingMessage.length == 70){
				
						createClientforStoreCommand(context, incomingMessage);

				
				
					}

				} 
				else if(unsignedByteToInt(incomingMessage[3]) == 17) {	// Physical Sensor Data
				
					if(incomingMessage.length == 52) {

				
						createClientforStoreCommand(context, incomingMessage);
					}

				}
 


				else if (unsignedByteToInt(incomingMessage[3]) == 22) { // Weight
					if(incomingMessage.length == 19) { 
						createClientforStoreCommand(context, incomingMessage);
					}
				}

				else if (unsignedByteToInt(incomingMessage[3]) == 27) { // EcgData Periodic, Careful, has different Intent action than ECG Stream


						createClientforStoreCommand(context, incomingMessage);


					}
				}
				else if (unsignedByteToInt(incomingMessage[3]) == 25/*19*/) { // Blood Pressure Data


					if(incomingMessage.length == 13) {
						createClientforStoreCommand(context, incomingMessage);
					}
				}
				else {

				}
			}

		else {	// if there are unchecked Alerts display them on screen

		}

	}


	private void createClientforWAKDStatus(final Context context, final byte[] incomingMessage) {

		class SoapWAKDStatusClient implements Runnable {
			@Override
			public void run() {
				NephronSOAPClient soapClient = new NephronSOAPClient(context);
				String strCommand = DataTransformationUtils.convertByteArrayToString(incomingMessage);
				soapClient.executeStoreWAKDStatusRequest(strCommand, new Date().getTime(), null);
			}
		}
		Thread thread = new Thread(new SoapWAKDStatusClient());
		thread.start();
	}


	private void createClientforStoreCommand(final Context context, final byte[] incomingMessage) {

		class SoapWAKDStatusClient implements Runnable {
			@Override
			public void run() {
				NephronSOAPClient soapClient = new NephronSOAPClient(context);
				String strCommand = DataTransformationUtils.convertByteArrayToString(incomingMessage);
				soapClient.executeStoreCommandRequest(strCommand, new Date().getTime(), null);
			}
		}
		Thread thread = new Thread(new SoapWAKDStatusClient());
		thread.start();
	}


	public static int unsignedByteToInt(byte b) {
		return (int) b & 0xFF;
	}

	public static int unsignedByteArrayToInt(byte[] byteArray, int offset) {
		int ret = 0;
		for (int i=0; i<4 && i+offset<byteArray.length; i++) {
			ret <<= 8;
			ret |= (int)byteArray[i] & 0xFF;
		}
		return ret;
	}


}

