package eu.nephron.bridge.connection.bt;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import de.imst.nephron.bt.BluetoothAbstractionLayer;
import eu.nephron.bridge.soap.SPLogin;
import eu.nephron.nephronbridge.GlobalVar;

public class ConnectionService extends Service {
	
	//TODO bluetooth WAS static only, NOW IS public static, there was error in the shutdowntask
	public static BluetoothAbstractionLayer u;
	public static boolean connection = false;

	MyDataListener mylistener;
	Thread alertHandler,currentStateHandler;
	public static boolean alive;
	
	
	
	final int CONNECTION_OK = 0;
	final int CONNECTION_ERROR = 1;
	Runnable loginRunnable;
	Thread backendLogin;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	// Start Service
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("Service Started","Service Started");
		
		loginRunnable = new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				//SPLogin login = new SPLogin(handler, getApplicationContext());
	    		//login.run();
			}
		};
		
		backendLogin = new Thread(loginRunnable);
		
        

        alive = true;
		u = new BluetoothAbstractionLayer();

		
		try {
			u.init(); 
		    connection = true;
			 
			
		  
 			 
		} catch (IOException e) {


			e.printStackTrace();
		}
 	
		
		// if there is no connection try to connect every 2 sec
		if (!connection) {
			final Timer t = new Timer();
			t.schedule(new TimerTask() {
				public void run() {
					Looper.prepare();
					int connectionAttempt = 0;
					while (!connection) {
						Log.d("!!!!NO CONNECTION!!!!!!!!","!NO CONNECTION!!!!!!!!!");
						try {
							Thread.sleep(2000);
							u.init();
							connection = true;
							Intent i = new Intent("nephron.mobile.application.WakStatusEvent");
							i.putExtra("connection","Active");
					    	sendBroadcast(i);
					     
							
							Log.d("!!!!YESSSSSSSSSSSS!!!!!!!!","!!!!!!!!YESSSSSSSSSSSS!!!!!!!!!");
							//Show dialog if connection established
				    	    
				    	    
						} catch (IOException e) {
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						connectionAttempt++;
					}

					// t.cancel();
				}
			}, 2000);
		}else {
			//Show dialog if connection established
//			Intent infoDialog = new Intent(ConnectionService.this,infoDialog.class);
//			infoDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);       
//			infoDialog.putExtra("message","Connection Established with WAKD!!");
//    	    startActivity(infoDialog);
    	    
		}
     
		// Set dataListener from WAKD
		mylistener = new MyDataListener(this);
		u.setDataListener(mylistener);
		
		
		
		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("Service stopped","Service stopped");
		alive = false;
		connection = false;

	}

}
