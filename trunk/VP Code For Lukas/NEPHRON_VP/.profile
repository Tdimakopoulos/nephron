#!/bin/sh.exe
#
# Get the aliases and functions
#
if [ -f ~/.bashrc ]
then
   . ~/.bashrc
fi

export CVS_RSH="ssh"
export EDITOR="/usr/bin/vim"
export HISTSIZE="500"

