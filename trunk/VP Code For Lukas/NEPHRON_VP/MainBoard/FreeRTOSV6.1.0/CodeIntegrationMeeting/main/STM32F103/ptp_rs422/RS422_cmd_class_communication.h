// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   RS422_cmd_class_communication.h
//! \brief  communication class opcodes and parameters
//!
//! ...
//!
//! \author  Dudnik G.
//! \date    17.07.2011
//! \version 1.0 First version (GDU)
//! \version 2.0 
//! \version 2.1 
// -----------------------------------------------------------------------------------
#ifndef RS422_CMD_CLASS_COMMUNICATION_H_
#define RS422_CMD_CLASS_COMMUNICATION_H_

/* Opcodes */		
enum communication_opcode_communication_e {		
	COMMUNICATION_OPCODE_COMMUNICATION_BTRATE= 1,                      // 1
	COMMUNICATION_OPCODE_COMMUNICATION_FLUSHINTERVAL,                 // 2
	COMMUNICATION_OPCODE_COMMUNICATION_CHANNELFILTER,                 // 3
	COMMUNICATION_OPCODE_COMMUNICATION_CONNECT,                       // 4
	COMMUNICATION_OPCODE_COMMUNICATION_DISCONNECT,                    // 5
	COMMUNICATION_OPCODE_COMMUNICATION__LAST        /*Keep as last!*/ // 6
};		
		
/* Parameters */		
enum communication_parameterid_communication_e {		
	COMMUNICATION_PARAMETERID_COMMUNICATION_BAUDRATE=1,               // 1
	COMMUNICATION_PARAMETERID_COMMUNICATION_INTERVAL,                 // 2
	COMMUNICATION_PARAMETERID_COMMUNICATION_NUMBER,                   // 3
	COMMUNICATION_PARAMETERID_COMMUNICATION_ENABLE,                   // 4
	COMMUNICATION_PARAMETERID_COMMUNICATION_NUMBER_RANGE_FROM,        // 5
	COMMUNICATION_PARAMETERID_COMMUNICATION_NUMBER_RANGE_TO,          // 6
	COMMUNICATION_PARAMETERID_COMMUNICATION_PASSWORD,                 // 7
	COMMUNICATION_PARAMETERID_COMMUNICATION_WAKDID,                   // 8
	COMMUNICATION_PARAMETERID_COMMUNICATION__LAST   /*Keep as last!*/ // 9
};		


#endif	//RS422_CMD_CLASS_ACQUISITION_H
