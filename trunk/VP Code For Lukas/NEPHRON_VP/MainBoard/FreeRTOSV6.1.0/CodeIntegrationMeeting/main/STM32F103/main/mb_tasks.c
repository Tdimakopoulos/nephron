// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   mb_tasks.c
//! \brief  Tasks for main
//!
//! Init and Hooks
//!
//! \author  Dudnik G.S.
//! \date    11.06.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported global data
// -----------------------------------------------------------------------------------
#define LED_PERIOD 100
#define SPHORE_PERIOD 100
uint32_t x=0;
uint16_t led_counter = 0;
uint16_t sphore_counter = 0;
uint32_t X1 = 0;
uint32_t X2 = 0;
uint32_t X5 = 0;
uint32_t Taken_OK = 0;
uint32_t Given_OK = 0;
uint32_t GivenISR_OK = 0;
uint32_t CBTaken_OK = 0;
uint32_t Taken_KO = 0;
uint32_t Given_KO = 0;
uint32_t GivenISR_KO = 0;
uint32_t CBTaken_KO = 0;
uint8_t X_CB_Task = 0;

portBASE_TYPE mTaken = pdFAIL;
portBASE_TYPE mGiven = pdFAIL;
portBASE_TYPE mGivenISR = pdFAIL;
portBASE_TYPE mCBTaken = pdFAIL;
// -----------------------------------------------------------------------------------
// Private data
// -----------------------------------------------------------------------------------
/* A variable that is incremented by the idle task hook function. */
static unsigned long ulIdleCycleCount = 0UL;
xSemaphoreHandle xMaintenanceSemaphore;
extern xSemaphoreHandle xCBMBSemaphore;
// -----------------------------------------------------------------------------------
// Function definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
//! \brief  prvSetupHardware
//!
//! Hardware & Variables Initialization
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void prvSetupHardware( void )
{
    __disable_irq();
    // -----------------------------
    // do not allow communication...
    MB_SYSTEM_READY = 0;
    // -----------------------------
    DelayBySoft (10); 
    MB_DATA_INIT();           // Data Initialization: TBD
  
    DelayBySoft (10); 
    MB_HW_INIT();             // Hardware Initialization: ALL 

    SetDate(9, 7, 2011);
    SetTime(15,11,7);
    SetAlarmTime(15,11,15);

    ReadDate();
    x = CalculateTimeG();
    ReadTime();

    DelayBySoft (100);
    
    SUPPLIES_SWITCH_ALL_ON();   // Switch ALL ON SUPPLYES PROGRESSIVELY
    DelayBySoft (100);

    CONFIGURE_SYSTICK();        // ENABLES AFTER SUPPLIES


    // ENABLES INTS AFTER SYSTICK  
    USART_RX_INT_ENABLE(USART1);
    USART_RX_INT_ENABLE(USART2);
    USART_RX_INT_ENABLE(USART3);
#ifndef STM32F_UART4_DMA
    USART_RX_INT_ENABLE(UART4);
#endif
    USART_RX_INT_ENABLE(UART5);


    // UART4 by DMA
    RTMCB_UART4_DMA_Config();
    //RTMCB_UART4_DMA_NVIC_Config();

    // ENABLES INCOMING MESSAGES  
    ENABLE_USART1_RX_TX();       
    ENABLE_USART2_RX_TX();       
    ENABLE_USART3_RX_TX();       
    ENABLE_UART4_RX_TX();         
    ENABLE_UART5_RX_TX();         

    // Enables EXTERNAL INTS at the end...
    MB_EXTI_Config();

    // Configure TIM2 update on overflow & Interrupt
    TIM2_Config();

    __enable_irq();

#ifdef TEST_SDCARD
    LOCK_CRITICAL_INTS = 1;
    // efsl library initialization
    SDCARD_PRESENT = SDCARD_NO_THERE;
    while(SDCARD_PRESENT != SDCARD_INSERTED){
        Update_SDCARD_Presence();
        //if (SDCARD_PRESENT != SDCARD_INSERTED) return -1;
    }
    SD_libInit();
    res = 0x55;
    while(res!=0){
        //if ( ( res = efs_init( &sdcard_efs, 0 ) ) != 0 ) return -2; // if efs_init
        res = efs_init( &sdcard_efs, 0 ); // if efs_init
    }
    sdfilecounter = 1;
    LOCK_CRITICAL_INTS = 0;
#endif
   
    // ALLOW CRITICAL ACTIVITIES
    MB_SYSTEM_READY = DEVICE_READY;

    // SWITCH LED ON
    STM32F_GPIOOff(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        

}
// -----------------------------------------------------------------------------------
//! \brief vApplicationStackOverflowHook
//!
//! Stack Overflow debug
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
	for( ;; );
}

// -----------------------------------------------------------------------------------
//! \brief vApplicationIdleHook
//!
//! Idle hook functions MUST be called vApplicationIdleHook(), take no parameters, and return void
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationIdleHook( void )
{
	/* This hook function does nothing but increment a counter. */
	ulIdleCycleCount++;
}
// -----------------------------------------------------------------------------------
//! \brief vApplicationTickHook
//!
//! Here you build your own Systick
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vApplicationTickHook ( void ){

#ifdef STM32F_SEMAPHORE_INT
    portBASE_TYPE xHighPriorityTaskWoken = pdFALSE;

    sphore_counter++;
    if(sphore_counter == SPHORE_PERIOD){
        sphore_counter = 0;
        mGivenISR = xSemaphoreGiveFromISR ( xMaintenanceSemaphore, &xHighPriorityTaskWoken );
        if(mGivenISR == pdPASS) GivenISR_OK++;
        else GivenISR_KO++;
    }
#endif

// This function decrements a counter to create delays.
  if (TimingDelay != 0x00)
  {
    TimingDelay--;
  }
  // MB TimeStamp
  globalTimestamp++;

//  if (LOCK_CRITICAL_INTS==0){
    ADCDMACounter++;
    if(ADCDMACounter == ADCDMA_SAMPLEPERIOD){
        ADCDMACounter = 0;
        /* Start ADC1 Software Conversion */ 
        ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    }
//  }

#ifdef RTOS_SYSTICK_LED
    led_counter++;
    if(led_counter == LED_PERIOD){
        led_counter = 0;
        STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
    }
#endif

#ifdef STM32F_SPORTS_STINT
        usart_tx_counter++;
        if(usart_tx_counter == USART_TX_PERIOD){
        usart_tx_counter = 0;
#ifndef STM32_USART1_LBACK   // USART1 TX
        SerialPort_SendPacket(USART1);
#endif
#ifndef STM32_USART2_LBACK   // USART2 TX
        SerialPort_SendPacket(USART2);
#endif
#ifndef STM32_USART3_LBACK  // USART3 TX
        SerialPort_SendPacket(USART3);
#endif
#ifndef STM32_UART4_LBACK   // UART4 TX
        SerialPort_SendPacket(UART4);
#endif
#ifndef STM32_UART5_LBACK  // UART5 TX
        SerialPort_SendPacket(UART5);
#endif 
        }
#endif  // STM32F_SPORTS_MAIN

    uif_acc_data_counter++;
    if(uif_acc_data_counter == UIF_ACC_DATA_SIMUL){
        uif_acc_data_counter = 0;
        MB_WAKD_BTS_InletTemperature+=2;
        if(MB_WAKD_BTS_InletTemperature==100) MB_WAKD_BTS_InletTemperature = 0;
        MB_WAKD_BTS_OutletTemperature++;
        if(MB_WAKD_BTS_OutletTemperature==100) MB_WAKD_BTS_OutletTemperature = 0;
    }

#ifdef STM32F_SEMAPHORE_INT
    portEND_SWITCHING_ISR( xHighPriorityTaskWoken );
#endif
}
// -----------------------------------------------------------------------------------
//! \brief vTaskFunction
//!
//! Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskFunction( void *pvParameters )
{
	/* The string to print out is passed in via the parameter.  
        Cast this to a character pointer. */
        
        char *pcTaskName  = ( char * ) pvParameters;

	/* As per most tasks, this task is implemented in an infinite loop. */

	for( ;; )
	{
                X1++;
                #ifdef STM32F_LED_TASK1
                // TOGGLES EVERY 2.25' (250)
                STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
                #endif
                #ifdef STM32F_SEMAPHORE_TASK
                mGiven = xSemaphoreGive( xMaintenanceSemaphore );
                if(mGiven == pdPASS) Given_OK++;
                else Given_KO++;
                #endif
                vTaskDelay( (115*4) / portTICK_RATE_MS );      
	}
}
// -----------------------------------------------------------------------------------
//! \brief vTaskMaintenance
//!
//! Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskMaintenance( void *pvParameters )
{

        char *pcTaskName  = ( char * ) pvParameters;
        portSHORT sError = pdFALSE;

        // Semaphore cannot be used before a call to vSemaphoreCreateBinary ().
        // This is a macro so pass the variable in directly.
        vSemaphoreCreateBinary( xMaintenanceSemaphore );

	/* As per most tasks, this task is implemented in an infinite loop. */
        if(xMaintenanceSemaphore !=  NULL ){
            X2++;   // the task was created 

            for( ;; )
            {
                #ifdef STM32F_LED_TASK_MAINTENANCE_TG
                // TOGGLES EVERY 112ms (115/portTICK_RATE_MS)
                STM32F_GPIOToggle(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
                #endif
                #ifdef STM32F_LED_TASK_MAINTENANCE_PL
                // ON: Starts Waiting Semaphore
                STM32F_GPIOOff(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
                #endif
                X2++;
                /* Try to obtain the semaphore. */
                // But does not wait ... if there is something to process, ok
                // otherwise continue... [0.... 115ms[x/portTICK_RATE_MS]....portMAX_DELAY(wait forever]
                //mTaken = xSemaphoreTake(xMaintenanceSemaphore, 0 );
                mTaken = xSemaphoreTake(xMaintenanceSemaphore, portMAX_DELAY );                                    
                if(mTaken == pdPASS) Taken_OK++;
                else Taken_KO++;
                if( mTaken == pdPASS ) { // semaphore taken successfully
                    /* If we have a block time then we are running at a priority higher                        
                    than the idle priority.  This task takes a long time to complete                        
                    a cycle (deliberately so to test the guarding) so will be starving                        
                    out lower priority tasks.  Block for some time to allow give lower                        
                    priority tasks some processor time. */                        
                    vTaskDelay( 115 / portTICK_RATE_MS );                
                }                
                else  { // pdFALSE, semaphore not available
                    if( (115 / portTICK_RATE_MS) == ( portTickType ) 0 )                        
                    {        
                        X5++;                                                
                        /* We have not got the semaphore yet, so no point using the processor.  
                        We are not blocking when attempting to obtain the semaphore. */                                
                        taskYIELD();                        
                    }                
                }     
                mTaken = pdFAIL;       
                //mGiven = pdFAIL;
                #ifdef STM32F_LED_TASK_MAINTENANCE_PL
                // OFF: Taken Semaphore or TimeOUT
                STM32F_GPIOOn(DEBUG_LED_GPIO_PORT, DEBUG_LED_GPIO_PIN);        
                #endif
            }
        }
}
// -----------------------------------------------------------------------------------
//! \brief vTaskCB
//!
//! Testing Task
//!
//! \param[IN]  void
//!
//! \return     none
// -----------------------------------------------------------------------------------
void vTaskCB( void *pvParameters )
{

        char *pcTaskName  = ( char * ) pvParameters;
        portSHORT sError = pdFALSE;
        uint16_t CB_DATALEN = 0;
        uint16_t w=0;

        vSemaphoreCreateBinary( xCBMBSemaphore );

	/* As per most tasks, this task is implemented in an infinite loop. */
        if(xCBMBSemaphore !=  NULL ){
            X_CB_Task++;   // the task was created 

            for( ;; )
            {
                X_CB_Task++;
                /* Try to obtain the semaphore. */
                // But does not wait ... if there is something to process, ok
                // otherwise continue... [0.... 115ms[x/portTICK_RATE_MS]....portMAX_DELAY(wait forever]
                //mTaken = xSemaphoreTake(xMaintenanceSemaphore, 0 );
                mCBTaken = xSemaphoreTake(xCBMBSemaphore, 115/portTICK_RATE_MS );                                    
                if(mCBTaken == pdPASS) CBTaken_OK++;
                else CBTaken_KO++;
                if( mCBTaken == pdPASS ) { // semaphore taken successfully
                        // ---------------------------------------------------
                        // LoopBack Function
                        // ---------------------------------------------------
                        // SLIP_RX_CB -->[FROM SLIP]--> RAW_RX_CB_0
                        CB_DATALEN = CB_ConvertSliptorawRX(SLIP_RX_CB, out_cb_reclen_0,&uartBufferCBin[0]);
                        // RAW_RX_CB_0 --> RAW_TX_CB_0
                        CB_DATALEN = CB_Make_LoopBack_Packet(uartBufferCBin, CB_DATALEN, &uartBufferCBout[0]);
                        // RAW_TX_CB -->[TO SLIP]--> SLIP_TX_CB
                        CB_DATALEN = CB_ConvertToSlipTX(uartBufferCBout, CB_DATALEN, &SLIP_TX_CB[0]);        
                        // ---------------------------------------------------
                        for(w=0;w<CB_DATALEN;w++) {    
                            USART_SendData(USART2, SLIP_TX_CB[w]);
                            while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET) {}
                        }
                        // ---------------------------------------------------
                        CB_CLR_OUT_COM_BUFFERS();	

                    /* If we have a block time then we are running at a priority higher                        
                    than the idle priority.  This task takes a long time to complete                        
                    a cycle (deliberately so to test the guarding) so will be starving                        
                    out lower priority tasks.  Block for some time to allow give lower                        
                    priority tasks some processor time. */                        
                    vTaskDelay( 115 / portTICK_RATE_MS );                
                }                
                else  { // pdFALSE, semaphore not available
                    if( (115 / portTICK_RATE_MS) == ( portTickType ) 0 )                        
                    {        
                        /* We have not got the semaphore yet, so no point using the processor.  
                        We are not blocking when attempting to obtain the semaphore. */                                
                        taskYIELD();                        
                    }                
                }     
                mCBTaken = pdFAIL;       
            }
        }
}


// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
