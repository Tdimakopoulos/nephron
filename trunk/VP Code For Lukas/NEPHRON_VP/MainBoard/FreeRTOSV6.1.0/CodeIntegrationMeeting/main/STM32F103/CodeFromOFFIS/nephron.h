/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef NEPHRON_H
#define NEPHRON_H

#include "global.h"

// Choose ONLy ONE !! of the following targets to compile to
// Nehpron Virtual Platform
// #define VP_SIMULATION
// Evaluation Board
// #define STM3210B

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "typedefsNephron.h"
#ifdef VP_SIMULATION
	#include "simlink.h"
	#include "lpc210x.h"
#endif
#include "definesActuatorValues.h"
#include "ioHelperFunc.h"
#include "apiWakdTime.h"
#ifndef LINUX
	#include "includes_nephron\\interfaces.h"
#else
	#include "includes_nephron/interfaces.h"
#endif

// This size is defined in CSEM specification document
// http://is.gd/3QRhPN
#define SIZEUARTBUFFERCB  (1006)
#define SIZEUARTBUFFERRTB (1006)

extern tdAllHandles allHandles;

// Pump direction definition 1=(+), 2=(-)
#define PD_FORWARD (1)
#define PD_REVERSE (2)

// if DONT_EXPECT_ACK_FROM_PHONE is defined, the dummy putData function
// will generate it's own ACK message for the message just send. As a
// result it is guaranteed that each message transmitted to the smartphone
// via puData() is always ACKed and the message will never be repeatedly
// transmitted. Normally the message would be send three times in total
// if no ACK is received. Than discarded.
// #define DONT_EXPECT_ACK_FROM_PHONE

// If SLIP_ENABLED is defined, the dummy puData and getData functions will
// add / remove SLIP protocol information to the transmitted message to the
// smartphone.
#define SLIP_ENABLED

// For testing it is handy, if the embedded SW automatically generates dummy
// weight measure data on a received "dataID_weightRequest" message. Handy in
// the case where no physical communication board is attached, but the phone is
// directly attached to the phone. Define the the following to enable this debug
// feature.
 #define GENERATE_WEIGHT_DATA_ON_WEIGHT_REQUEST

// define addresses for VP UART access to smartphone via bluetooth
#define VP_UartBtToPhone_ENABLED
#define UARTBTTOPHONEBASE 0x80020000
#define UARTIN UARTBTTOPHONEBASE
#define UARTOUT (UARTBTTOPHONEBASE+4)
#define UARTIRQ (UARTBTTOPHONEBASE+8)

// define these to see debug messages during simulation
// The Nephron+ Internal Specification Wiki (http://is.gd/HDn7rL) defines several
// sequence charts of interaction. The implementation covers these sequences. Each
// trace output of a sequence can be enabled defining the following. (If more than one
// is defined, the trace will be mixed, reducing readability. But in principle no problem!)
//#define SEQ0	// part of power on and reset sequence
//#define SEQ00	// part of system shutdown request
#define SEQ1	// part of Weight Measure Scenario
#define SEQ2	// part of Weight Measure Scenario
#define SEQ3	// part of Weight Measure Scenario
//#define SEQ4	// part of Change State Scenario
//#define SEQ5	// part of Smartphone Request for Status update
//#define SEQ6	// part of RTB physiological data read
//#define SEQ7	// part of RTB physical data read
//#define SEQ8	// part of RTB actuator data read
//#define SEQ9	// part of configuring RTB states
//
//#define SEQ11	// part of system info/alarms scenario
//#define SEQ12	// part of system configuration scenario: ask for current configuration
//#define SEQ13	// part of system configuration scenario: change current configuration to new values.

#define DEBUG_STACK
// #define DEBUG_MAIN
// #define DEBUG_EG
// #define DEBUG_DISP
// #define DEBUG_UI
#define DEBUG_UI_MSG
// #define DEBUG_DS
// #define DEBUG_SAFETY
// #define DEBUG_SCC
// #define DEBUG_DUP
// #define DEBUG_FC
// #define DEBUG_MTNA
//#define DEBUG_CBPA
// #define DEBUG_REMINDER
// #define DEBUG_RTBPA
// #define DEBUG_PSBPA

// #define DEBUG_ISR_MB
// #define DEBUG_ISR_RTB
// #define DEBUG_ISR_CB
// #define DEBUG_ISR_PB

// #define DEBUG_SHOW_PHYSICAL
// #define DEBUG_SHOW_PHYSIOLOGICAL
// #define DEBUG_SHOW_ACTUATORS	

// Physiological sensor definitions. Values that describe the state of teh patient's health
// we have two sensors for each value. One in front of the adsorbent unit (INLET)
// and one that is behind the adorbent unit (OUTLET)
#define SENSOR_SAMPLETIME_PATI	SENSOR(0)
#define SENSOR_ECPO_NA			SENSOR(1)
#define SENSOR_ECPI_NA			SENSOR(2)
#define SENSOR_ECPO_K			SENSOR(3)
#define SENSOR_ECPI_K			SENSOR(4)
#define SENSOR_ECPO_UREA		SENSOR(5)
#define SENSOR_ECPI_UREA		SENSOR(6)
#define SENSOR_ECPO_CA			SENSOR(7)
#define SENSOR_ECPI_CA			SENSOR(8)
#define SENSOR_ECPO_MG			SENSOR(9)
#define SENSOR_ECPI_MG			SENSOR(10)
#define SENSOR_ECPO_HCO3		SENSOR(11)
#define SENSOR_ECPI_HCO3		SENSOR(12)
#define SENSOR_ECPO_C4H9N3O2	SENSOR(13)
#define SENSOR_ECPI_C4H9N3O2	SENSOR(14) 
#define SENSOR_ECPO_H2PO4		SENSOR(15)
#define SENSOR_ECPI_H2PO4		SENSOR(16) 
#define SENSOR_ECPO_PH			SENSOR(17)
#define SENSOR_ECPI_PH			SENSOR(18) 
#define SENSOR_BTSO_TEMPERATURE	SENSOR(19)
#define SENSOR_BTSI_TEMPERATURE	SENSOR(20) 
#define SENSOR_ECPO_TEMPERATURE	SENSOR(21)
#define SENSOR_ECPI_TEMPERATURE	SENSOR(22)

// Physical sensor definitions. Values that describe the state of the device WAKD
#define SENSOR_SAMPLETIME_WAKD	SENSOR(23)
#define SENSOR_ECPI_STAT		SENSOR(24) 
#define SENSOR_ECPO_STAT		SENSOR(25)
#define SENSOR_DCS_CONDUCT_FCR	SENSOR(26)
#define SENSOR_DCS_CONDUCT_FCQ	SENSOR(27)
#define SENSOR_FLOW_BLOOD		SENSOR(28)
#define SENSOR_FLOW_FLUID		SENSOR(29)
#define SENSOR_BPSI_BLOODPRESI	SENSOR(30)
#define SENSOR_BPSO_BLOODPRESO	SENSOR(31)
#define SENSOR_FPSI_FLUIDPRESSI	SENSOR(32)
#define SENSOR_FPSO_FLUIDPRESSO	SENSOR(33)
#define SENSOR_BABD_STAT_BUBBLE	SENSOR(34)
#define SENSOR_MFSI_STAT_SWITCH SENSOR(35)
#define SENSOR_MFSO_STAT_SWITCH SENSOR(36)
#define SENSOR_MFSU_STAT_SWITCH SENSOR(36) //???????? not there in simulink! therefore duplicated 36
#define SENSOR_FLOWREF_BLOOD	SENSOR(37)
#define SENSOR_FLOWREF_FLUID 	SENSOR(38)
#define SENSOR_BLD_STAT_LEAK	SENSOR(39)
#define SENSOR_FLD_STAT_LEAK	SENSOR(40)
#define SENSOR_STAT_POLARIZ		SENSOR(41)

#define SENSOR_POLARIZ_ONOFF	SENSOR(42)
#define SENSOR_POLARIZ_DIRECT	SENSOR(43)
#define SENSOR_POLARIZ_VOLTAGEREF	SENSOR(44)
#define SENSOR_POLARIZ_VOLTAGE	SENSOR(45)

// Medical device sensors
#define SENSOR_WEIGHTSCALE		SENSOR(53)
#define SENSOR_FROMSTATE        SENSOR(54)
#define SENSOR_TOSTATE			SENSOR(55)
#define SENSOR_FROMOPSTATE		SENSOR(56)
#define SENSOR_TOOPSTATE		SENSOR(57)

// Status "sensors"
#define SENSOR_ECPISTAT			SENSOR(80)
#define SENSOR_ECPOSTAT     	SENSOR(81)
#define SENSOR_BPSISTAT			SENSOR(82)
#define SENSOR_BPSOSTAT			SENSOR(83)
#define SENSOR_FPSISTAT			SENSOR(84)
#define SENSOR_FPSOSTAT			SENSOR(85)
#define SENSOR_BTSSTAT			SENSOR(86)
#define SENSOR_DCSSTAT			SENSOR(87)
#define SENSOR_BLPUMPSTAT		SENSOR(88)
#define SENSOR_FLPUMPSTAT		SENSOR(89)
#define SENSOR_MFSISTAT			SENSOR(90)
#define SENSOR_MFSOSTAT			SENSOR(91)
#define SENSOR_MFSBLSTAT		SENSOR(92)
#define SENSOR_POLARIZSTAT		SENSOR(93)
#define SENSOR_RTMCBSTAT		SENSOR(94)
//#define SENSOR_			SENSOR(95)

// If you change any of the values below, make sure to also change the "gain" attached to
// it in the simulink model. Why is this? Here the story:
// The transfer of values from Simulink to the OVP-Simlink-Interface is Uns32 based. So, if
// we want to receive a double value like 123.45678 we do e.g. a mult 10000 there to
// 1234567.8. After the transfer Simlink gets 1234567. We have to make sure to correct this by
// a division of 10000 resulting to 123.4567
// So 10000 gives us 4 decimal places. 10 would give us one: 123.4
// BE AWARE Uns32 Bit means largest range is (2^32)-1. So your simulation must not generate larger
// numbers than this!
// Physiological Sensors
#define FIXPOINTSHIFT_SAMPLETIME		1
/* Commented parts are moved to "sensors.h" */
// #define FIXPOINTSHIFT_ECPI_NA			10000
// #define FIXPOINTSHIFT_ECPO_NA			10000
// #define FIXPOINTSHIFT_ECPI_K			10000
// #define FIXPOINTSHIFT_ECPO_K			10000
// #define FIXPOINTSHIFT_ECPI_UREA			10000
// #define FIXPOINTSHIFT_ECPO_UREA			10000
#define FIXPOINTSHIFT_ECPI_CA	      	10000
#define FIXPOINTSHIFT_ECPO_CA	     	10000
#define FIXPOINTSHIFT_ECPI_MG	      	10000
#define FIXPOINTSHIFT_ECPO_MG	     	10000
#define FIXPOINTSHIFT_ECPI_HCO3	    	10000
#define FIXPOINTSHIFT_ECPO_HCO3		   	10000
// #define FIXPOINTSHIFT_ECPI_C4H9N3O2		10000
// #define FIXPOINTSHIFT_ECPO_C4H9N3O2		10000
// #define FIXPOINTSHIFT_ECPI_H2PO4		10000
// #define FIXPOINTSHIFT_ECPO_H2PO4		10000
// #define FIXPOINTSHIFT_ECPI_PH			10000
// #define FIXPOINTSHIFT_ECPO_PH			10000
#define FIXPOINTSHIFT_BTSI_TEMPERATURE	10000
#define FIXPOINTSHIFT_BTSO_TEMPERATURE	10000
// #define FIXPOINTSHIFT_ECPI_TEMPERATURE	10000
// #define FIXPOINTSHIFT_ECPO_TEMPERATURE	10000
// Physical Sensors
#define FIXPOINTSHIFT_SAMPLETIME_WAKD	1
#define FIXPOINTSHIFT_ECPI_STAT			1
#define FIXPOINTSHIFT_ECPO_STAT			1 
// #define FIXPOINTSHIFT_DCS_CONDUCT_FCR	10000
// #define FIXPOINTSHIFT_DCS_CONDUCT_FCQ	10000
// #define FIXPOINTSHIFT_FLOW_BLOOD		10000
// #define FIXPOINTSHIFT_FLOW_FLUID		10000
// #define FIXPOINTSHIFT_BPSI_BLOODPRESI	10000
// #define FIXPOINTSHIFT_BPSO_BLOODPRESO	10000
// #define FIXPOINTSHIFT_FPSI_FLUIDPRESSI	10000
// #define FIXPOINTSHIFT_FPSO_FLUIDPRESSO	10000
#define FIXPOINTSHIFT_BABD_STAT_BUBBLE	10000
#define FIXPOINTSHIFT_MFSI_STAT_SWITCH 	10000
#define FIXPOINTSHIFT_MFSU_STAT_SWITCH 	10000
#define FIXPOINTSHIFT_MFSO_STAT_SWITCH 	10000
// #define FIXPOINTSHIFT_PUMPSPEED_BLOOD  	10000
// #define FIXPOINTSHIFT_PUMPSPEED_FLUID  	10000
#define FIXPOINTSHIFT_BLD_STAT_LEAK		10000
#define FIXPOINTSHIFT_FLD_STAT_LEAK		10000
#define FIXPOINTSHIFT_STAT_POLARIZ		1

// These values are part of legacy code and no longer used
//#define ACTUATOR_BLOODPUMP				ACTUATOR(0)
//#define ACTUATOR_FLUIDPUMP				ACTUATOR(1)
//#define ACTUATOR_POLARIZATION			ACTUATOR(2)
//#define ACTUATOR_MFSO					ACTUATOR(3)
//#define ACTUATOR_MFSU					ACTUATOR(4)
//#define ACTUATOR_MFSI					ACTUATOR(5)
//// This following is for a dirty trick to show messages in Simulink GUI.
//// Will have to be implemented correctly once UI is finsihed.
//#define ACTUATOR_MESSAGEUI				ACTUATOR(9)

#define FIXPOINTSHIFT_BLOODPUMP			10000
#define FIXPOINTSHIFT_FLUIDPUMP			10000
//#define FIXPOINTSHIFT_POLARIZ_VOLT      10000
//#define FIXPOINTSHIFT_MFSO				10000
//#define FIXPOINTSHIFT_MFSU				10000
//#define FIXPOINTSHIFT_MFSI				10000

// defining the Nephron tasks' queue lengths.
#define uxQueueLengthDefault 4
static const unsigned portBASE_TYPE uxQueueLengthEG      = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthDISP    = (uxQueueLengthDefault*3);	// everything goes through the dispatcher, give it more room than the others.
static const unsigned portBASE_TYPE uxQueueLengthUI      = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthDS      = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthSAFETY  = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthSCC     = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthDUP     = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthFC      = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthMTNA    = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthCBPA    = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthREMINDER = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthRTBPA   = uxQueueLengthDefault;
static const unsigned portBASE_TYPE uxQueueLengthPSBPA   = uxQueueLengthDefault;


// Defining the Task priorities
#define PRIORITY0	( tskIDLE_PRIORITY )
#define PRIORITY1	( tskIDLE_PRIORITY + 1 )
#define PRIORITY2	( tskIDLE_PRIORITY + 2 )
#define PRIORITY3	( tskIDLE_PRIORITY + 3 )
#define PRIORITY4	( tskIDLE_PRIORITY + 4 )

static const unsigned portBASE_TYPE tskDISP_PRIORITY   	 = PRIORITY2; // maybe higher priority? center of communication
static const unsigned portBASE_TYPE tskUI_PRIORITY     	 = PRIORITY1;
static const unsigned portBASE_TYPE tskDS_PRIORITY       = PRIORITY1;
static const unsigned portBASE_TYPE tskSCC_PRIORITY    	 = PRIORITY1;
static const unsigned portBASE_TYPE tskWD_PRIORITY       = PRIORITY4;
static const unsigned portBASE_TYPE tskFC_PRIORITY     	 = PRIORITY1;
static const unsigned portBASE_TYPE tskCBPA_PRIORITY	 = PRIORITY3;
static const unsigned portBASE_TYPE tskReminder_PRIORITY = PRIORITY1;
static const unsigned portBASE_TYPE tskRTBPA_PRIORITY  	 = PRIORITY3;
static const unsigned portBASE_TYPE tskPMBPA_PRIORITY  	 = PRIORITY3;

// size of Stack for each Task created
static const unsigned short stackSizeEG			= 512;
static const unsigned short stackSizeDISP		= 532;
static const unsigned short stackSizeRTBPA		= 600;
static const unsigned short stackSizeCBPA		= 600;
static const unsigned short stackSizeReminder	= 512;
static const unsigned short stackSizeFC			= 924;
static const unsigned short stackSizeDS			= 1024-100;
static const unsigned short stackSizeSCC		= 1024-100;
static const unsigned short stackSizeWD			= 512+100;
static const unsigned short stackSizeUI			= 562;
static const unsigned short stackSizePMBPA		= 512;

// Defining task execution periods and deadlines
// Dispatcher Task
static const portTickType DISP_PERIOD		= 2000/portTICK_RATE_MS;    	// every 2 Seconds
static const portTickType DISP_PERIOD_RESET	= 5/portTICK_RATE_MS;    		// every 5 ms check if tasks correctly terminated during SW rest (shutdown)
static const portTickType DISP_BLOCKING		= 1000/portTICK_RATE_MS/10;  	// wait tens of second for write to foreign queues
// Real Time Board Protocol Abstraction Task
static const portTickType RTBPA_PERIOD		= 2000/portTICK_RATE_MS;    	// every 2 Seconds
static const portTickType RTBPA_BLOCKING 	= 1000/portTICK_RATE_MS/10;  	// wait tens of second for write to foreign queues
// Communication Board Protocol  Abstraction Task
static const portTickType CBPA_PERIOD		= 2000/portTICK_RATE_MS;		// every 2 Seconds
static const portTickType CBPA_BLOCKING		= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// Reminder Task
// The reminder task does not have a period defined. If nothing has to be reminded it will wait forever.
static const portTickType REMINDER_BLOCKING	= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// Flow Control Task
static const portTickType FC_PERIOD			= 2000/portTICK_RATE_MS;		// every 2 Seconds
static const portTickType FC_BLOCKING		= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// Data Storage Task
static const portTickType DS_PERIOD			= 2000/portTICK_RATE_MS;		// every 2 Seconds
static const portTickType DS_BLOCKING		= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// System Check and Calibration Task
// WARNING: SCC is handling the watchdog. It needs to wake up and reset WD at least every 1 second!!
static const portTickType SCC_PERIOD		= 2000/portTICK_RATE_MS;		// every 2 Seconds
static const portTickType SCC_BLOCKING		= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// User Interface Task
static const portTickType UI_PERIOD			= 2000/portTICK_RATE_MS;		// every 2 Seconds for debugging and not too many messages
static const portTickType UI_BLOCKING		= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// Power Management Board Protocol Abstraction
static const portTickType PMBPA_PERIOD		= 2000/portTICK_RATE_MS;		// every 2 Seconds for debugging and not too many messages
static const portTickType PMBPA_BLOCKING	= 1000/portTICK_RATE_MS/10;		// wait tens of second for write to foreign queues
// ED is outdated and was removed from the Nephron+ Architecture
//static const portTickType EG_PERIOD   = 1000/portTICK_RATE_MS;     // every second
//static const portTickType EG_BLOCKING = 1000/portTICK_RATE_MS/10;  // wait tens of second for write to foreign queues
//static const unsigned short EG_PERIOD_PhysicalSensorRead = 60;     // every minute



// CODE BELOW IS BEFORE THE SW ARCHITECTURE WAS BEAUTIFIED.
// THIS CODE STILL NEEDS BEAUTIFICATION.
// (IF NEEDED AT ALL! We might just as well delete it completely!)



/*-----------------------------------------------------------*/
/*
 * Task execution periods in ms. Still has to be refined!
 */
 



// Activities are triggered by the EG every 60 sec.
// If nothing has happened to the tasks after 70 sec. something has gone bad! 
//#define UI_PERIOD    ((portTickType)70000/portTICK_RATE_MS)
//#define DS_PERIOD    ((portTickType)70000/portTICK_RATE_MS)
//#define RTBPA_PERIOD ((portTickType)70000/portTICK_RATE_MS)
//#define ST_PERIOD    ((portTickType)70000/portTICK_RATE_MS)
//#define CBPA_PERIOD  ((portTickType)70000/portTICK_RATE_MS)
//#define FC_PERIOD    ((portTickType)70000/portTICK_RATE_MS)
//#define SCC_PERIOD   ((portTickType)70000/portTICK_RATE_MS)
//
//#define DU_PERIOD    ((portTickType)60000/portTICK_RATE_MS)
//#define M_PERIOD     ((portTickType)60000/portTICK_RATE_MS)
//#define PSBPA_PERIOD ((portTickType)60000/portTICK_RATE_MS)




/*-----------------------------------------------------------*/
/*
 * Generic definitions and declarations
 */
//#define pollqNO_DELAY ((portTickType) 0 )


/*-----------------------------------------------------------*/
/*
 * Types and data structures for inter task communication 
 */

// struct to contain one set of sensor readouts
//typedef struct strDataBlockAllSensors
//{
//  sensorType sensor0;
//  sensorType sensor1;
//  sensorType sensor2;
//  sensorType sensor3;
//  sensorType sensor4;
//  sensorType sensor5;
//  sensorType sensor6;
//  sensorType sensor7;
//  sensorType sensor8;
//  sensorType sensor9;
//} tdDataBlockAllSensors;

// struct to contain one set of actuator values
//typedef struct strDataBlockAllActuators
//{
//  actuatorType actuator0;
//  actuatorType actuator1;
//  actuatorType actuator2;
//  actuatorType actuator3;
//  actuatorType actuator4;
//  actuatorType actuator5;
//  actuatorType actuator6;
//  actuatorType actuator7;
//  actuatorType actuator8;
//  actuatorType actuator9;
//} tdDataBlockAllActuators;

// struct to transport data from Dispatcher task to User Interface
// typedef struct strDataBlockDISPtoUI
// {
  // tdDataBlockAllSensors *pDataBlockAllSensors;
// } tdDataBlockDISPtoSCC;

// What Safety should do with queue item
//#define UI_doNothing                0
//#define UI_displayNaWarning         1
//#define UI_displayUreaWarning       2
//#define UI_displayKWarning          3

// struct to hold one token of Safety queue entry
//typedef struct strQtokenUIin
//{
//  unsigned short command;
//} tdQtokenUIin;

// struct to transport data from Dispatcher task to Safety
//typedef struct strDataBlockDISPtoSCC
//{
//  tdDataBlockAllSensors *pDataBlockAllSensors;
//} tdDataBlockDISPtoSCC;

// What Safety should do with queue item
//#define SCC_doNothing                0
//#define SCC_newDataFromRTBPA         1

// struct to hold one token of Safety queue entry
//typedef struct strQtokenSCCin
//{
//  unsigned short command;
//  tdDataBlockDISPtoSCC *pDataBlockDISPtoSCC;
//} tdQtokenSCCin;

// struct to transport data from Dispatcher task to Flow Control
//typedef struct strDataBlockDISPtoFC
//{
//  tdDataBlockAllSensors *pDataBlockAllSensors;
//} tdDataBlockDISPtoFC;

//// What Flow Control should do with queue item
//#define FC_doNothing                0
//#define FC_newDataFromRTBPA         1

// struct to hold one token of flow control queue entry
//typedef struct strQtokenFCin
//{
//  unsigned short command;
//  tdDataBlockDISPtoFC *pDataBlockDISPtoFC;
//} tdQtokenFCin;

// struct to transport data from Dispatcher task to data storage
//typedef struct strDataBlockDISPtoDS
//{
//  tdDataBlockAllSensors *pDataBlockAllSensors;
//} tdDataBlockDISPtoDS;

// Commands for Data storage to do
//#define DS_doNothing                0
//#define DS_newDataFromRTBPA         1

// struct to hold one token of data storage queue entry
//typedef struct strQtokenDSin
//{
//  unsigned short command;
//  tdDataBlockDISPtoDS *pDataBlockDISPtoDS;
//} tdQtokenDSin;

// struct to transport data from Flow Control to Dispatcher
//typedef struct strDataBlockFCtoDISP
//{
//  tdDataBlockAllActuators *pDataBlockAllActuators;
//} tdDataBlockFCtoDISP;

// struct to transport data from Realtime board protocoll abstraction task to Dispatcher task
//typedef struct strDataBlockRTBPAtoDISP
//{
//  tdDataBlockAllSensors *pDataBlockAllSensors;
//} tdDataBlockRTBPAtoDISP;

// commands the Dispatcher understands
//#define DISP_doNothing               0
//#define DISP_newSensorDataFromRTBPA  1
//#define DISP_newActuator3ValueFromFC    2
//#define DISP_NaWarning               3
//#define DISP_KWarning                4
//#define DISP_UreaWarning             5

// struct to hold one token of Dispatcher queue entry
//typedef struct strQtokenDISPin
//{
//  unsigned short command;
//  tdDataBlockRTBPAtoDISP *pDataBlockRTBPAtoDISP;
//  tdDataBlockFCtoDISP    *pDataBlockFCtoDISP;
//} tdQtokenDISPin;

// struct to transport data from Dispatcher to RTBPA
//typedef struct strDataBlockDISPtoRTBPA
//{
//  tdDataBlockAllActuators *pDataBlockAllActuators;
//} tdDataBlockDISPtoRTBPA;

//#define RTBPA_doNothing             0
//#define RTBPA_readAllSensors        1
//#define RTBPA_newactuator3ValueFromFC  2

// struct to hold one token of RTBPA queue entry
//typedef struct strQtokenRTBPAin
//{
//  unsigned short command;
//  tdDataBlockDISPtoRTBPA    *pDataBlockDISPtoRTBPA;
//} tdQtokenRTBPAin;
 
#endif
