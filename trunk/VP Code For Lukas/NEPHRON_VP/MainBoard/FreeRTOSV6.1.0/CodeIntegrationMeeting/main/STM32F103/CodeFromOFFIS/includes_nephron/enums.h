/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef ENUMS_H
#define ENUMS_H 

// HW-interface API-functions need to know where data needs to go to or come from.
// By using the following enumeration type, API-functions decide on which UART to
// use to reach correct recipient/sender.
typedef enum enumWhoId
{
	whoId_nd,	// not defined, used whenever whoId is irrelevant (sender does not need Ack/Nack)
	whoId_MB,	// Main Board
	whoId_RTB,	// Real Time Board
	whoId_CB,	// Communication Board
	whoId_SP,	// Smart Phone
	whoId_PMB1,	// Power Management Board 1
	whoId_PMB2,	// Power management Board 2
	// Are some missing?
} tdWhoId; 

// WAKD states/modes as defined in specification document
// Embedded Software: Task Flow Control Version 10:36, 29 June 2011
/*
  IMPORTANT: numbers specified explicitly, to ensure consistency for a written csv-file!
			(otherwise when you change the enum and use an old csv-file the values e.g.
			state Regen1 might be read out as Ultrafiltration)
*/
typedef enum enumWakdStates
{
	wakdStates_UndefinedInitializing, // Internal state after PowerOn, initialize and proceed directly to AllStopped
	
	// NON OPERATIONAL States
	wakdStates_AllStopped,
	wakdStates_Maintenance,
	wakdStates_NoDialysate,			// formerly known as "BloodFlowOn"
	
	// OPERATIONAL States
	wakdStates_Dialysis,
	wakdStates_Regen1,
	wakdStates_Ultrafiltration,
	wakdStates_Regen2,

	// *** WARNING ***
	// Do not define more than 256 states (one byte limit)
	// since this enum is processed as an uint8_t

} tdWakdStates;

// WAKD states/modes as defined in specification document
// tdWakdOperationalState differentiates between 
typedef enum enumWakdOperationalState
{
	wakdStatesOS_UndefinedInitializing, // Internal state after PowerOn, initialize and proceed directly to OperationalState Automatic
	wakdStatesOS_Automatic, 	// OperationalState Automatic
	wakdStatesOS_Semiautomatic, // OperationalState Semi-Automatic (falling back to All Stopped after this)

	// *** WARNING ***
	// Do not define more than 256 states (one byte limit)
	// since this enum is processed as an uint8_t

} tdWakdOperationalState;


// Transmission channels (UART, BlueTooth, TCP/IP) transport data packages without
// interpretation byte for byte. The sender receives a bytewise copy of the transmitted data.
// The following enumaration type defines commands/actions/datatypes
typedef enum enumDataId
{
	dataID_undefined,					// 0:
	/* generic commands/data relevant for all WhoId. */
	dataID_reset,						// 1: Recipient should reset itself no data attached (refer to strMessageOnly)
	dataID_shutdown,					// 2: request to put system in safe state to turn of power in the end.
	dataID_setTimeDate,					// 3: Recipient should set Unix time according to attached data (refer to tdsetTime)
	dataID_currentWAKDstateIs,			// 4: Recipient is informed about the current state of WAKD
	dataID_ack,							// 5:
	dataID_nack,						// 6:
	dataID_statusRequest,				// 7:
	dataID_systemInfo,					// 8:
	
	/* commands/data relevant for RTB */
	// configures actuator values for the states implemented in the RTB
	dataID_configureState,					// 9: For RTB only: set WAKD actuators in state according to attached data (refer to tdActCtrl)
	dataID_configurePeriodPolarizerToggle,	// A: For RTB only: tdMsgPeriodPolarizerToggle
		
	// Command id that triggers to switch into another state. The RTB will automatically apply
	// the preconfigured actuator values (see above) whenever it changes into another state.
	dataID_changeWAKDStateFromTo,		// B: Request a state change from State A to B. (refer to tdWakdStateFromTo)
	
	// Command ids relevant for exchange of sensor data from RTB
	dataID_physiologicalData,			// C: Recipient gets all physioloical data in one set. (refer to tdPhysiologicalData)
	dataID_physiologicalData_K,			// D:
	dataID_physiologicalData_Na,		// E:
	dataID_physiologicalData_pH,		// F:
	dataID_physiologicalData_Ur,		// 10:
	dataID_physicalData,				// 11: Recipient gets all physical data in one set. (refer to tdPhysicalData)
	dataID_actuatorData,				// 12:
	dataID_alarmRTB,					// 13: Recipient is informed about alarm status from RTB. (refer to tdAlarms)
	dataID_statusRTB,					// 14:
	
	/* commands/data relevant for CB */
	dataID_weightRequest,				// 15: MessageOnly command to ask CB to connect to weight scale and receive new value.
	dataID_weightData,					// 16: Recipient gets weight measurement. (refer to tdWeightData)
	dataID_weightDataOK,				// 17: Weight data that was verified (and corrected) by the patient
	dataID_bpRequest,  					// 18: SHOULD BE OIN ONE BLOCK
	dataID_bpData,						// 19:
	dataID_EcgRequest,					// 1A:
	dataID_EcgData,						// 1B:
	
	/*data to configure the WAKD control presets */
	dataID_PatientProfile,			// 1C:
	dataID_WAKDAllStateConfigure, 	// 1D: used for task FC, single state: dataID_configureState
	dataID_StateParametersSCC,	 	// 1E: used for task SCC
	dataID_AllStatesParametersSCC,	// 1F: used for task SCC
	dataID_configureStateDump,		// 20: used to log the configutaion send out from FC to the RTB

	dataID_CurrentParameters,		// 21: maintains IDs of parameters that current values WAKD should send to SP.
	dataID_ActualParameters,		// 22: answer to dataID_CurrentParameters. Maintains name-value pairs of the parameters required.
	dataID_ConfigureParameters,		// 23: Used by Smartphone (Exodus) to send new parameter data to WAKD (minboard, OFFIS)
} tdDataId;


// Mainly for Mainboard internal use. The following commands can be sent in between queues of FreeRTOS tasks
// so that receiving tasks knows what to do.
typedef enum enumCommand
{
	command_DefaultError,
//	command_DataPhysiologicalSensorRead,
//	command_DataPhysicalSensorRead,
//	command_DataActuatorSensorRead,
//	command_DataActuatorValuesFromFC,	// Now obsolete when RTB was decided to have states and MB is not controlling actuators directly
//	command_SemiAutomaticModeFC,		// Flow Control goes into Semi mode (after each state we jump back to state No Dialysate)
//	command_AutomaticModeFC,			// Flow Control goes into auto mode (loops through all four states: dialys, regen, ...)
//	command_ConfigureWakdState,			// Moved to "command_UseAtachedMsgHeader", the header info there contains "dataID_configureState"
	command_InitializeSCC,
	command_InitializeSCCReady,
//	command_DataSCCInitialized,
	command_ButtonEvent,
	command_InitializeFC,
	command_InitializeFCReady,
	command_InformGuiConnectBag,
	command_InformGuiDisconnectBag,
	command_BufferFromRTBavailable,
	command_BufferFromCBavailable,
	command_BufferFromPB1available,
	command_BufferFromPB2available,
//	command_TransmissionErrorRTB,		// e.g. CRC error while RTB tried to transmit data
//	command_TransmissionErrorCB,		// e.g. CRC error while CB tried to transmit data
//	command_TransmissionErrorPB1,		// e.g. CRC error while PB tried to transmit data
//	command_TransmissionErrorPB2,		// e.g. CRC error while PB tried to transmit data
	command_WaitOnMsgCBPA,				// msg to be sent to Reminder TAsk to list msg for reminding later if ACK arrived.
	command_AckOnMsgCBPA,				// msg to be sent to Reminder Task to unlist msg from reminding list.
	command_WakeupCall,					// msg from Reminder Task that ACK for some msg is missing: send again!
	command_UseAtachedMsgHeader,		// look into header for dataID_* to know what to do with this.
	command_RequestDatafromDS,
	command_RequestDatafromDSReady,
	command_getPhysioPatientData,
	command_RtbChangeIntoStateRequest,
	command_ConfigurationChanged,
} tdCommand;

typedef enum enumCParameter {

	// patient profile
	parameter_name,						//  0: e.g. "Frank Poppen" This is a STRING!!!
	// name[50]; is a string does this "fit into" the defined transmission? How to transmit this?
	parameter_gender,					//  1: m/f
	parameter_measureTimeHours,			//  2: daily time for weight & bp measurement, hour
	parameter_measureTimeMinutes,		//  3: daily time for weight & bp measurement, minute
	parameter_wghtCtlTarget,			//  4: weight control target [same unit as weigth measurement on RTB]
	parameter_wghtCtlLastMeasurement,	//  5: weight control last measurement [same unit as weigth measurement on RTB]
	parameter_wghtCtlDefRemPerDay,		//  6: weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
	parameter_kCtlTarget,				//  7: potassium control target use FIXPOINTSHIFT for [mmol/l]
	parameter_kCtlLastMeasurement,		//  8: potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
	parameter_kCtlDefRemPerDay,			//  9: potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
	parameter_urCtlTarget,				//  a: urea control target use FIXPOINTSHIFT for [mmol/l]
	parameter_urCtlLastMeasurement,		//  b: urea control last measurement use FIXPOINTSHIFT for [mmol/l]
	parameter_urCtlDefRemPerDay,		//  c: urea default removal per day use FIXPOINTSHIFT for [mmol/day]
	parameter_minDialPlasFlow,			//  d: minimal flowrate for plasma in Dialysis mode  ml/min
	parameter_maxDialPlasFlow,			//  e: maximal flowrate for plasma in Dialysis mode  ml/min
	parameter_minDialVoltage,			//  f: minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
	parameter_maxDialVoltage,			// 10: maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT

	// To be configured for every state of "enumWakdStates"
	parameter_defSpeedBp_mlPmin,		// 11: setting speed blood pump [ml/min]
	parameter_defSpeedFp_mlPmin,		// 12: setting speed fluidic pump [ml/min]
	parameter_defPol_V,					// 13: setting voltage polarization [V]
	parameter_direction,				// 14: polarity of polarization voltage.
	parameter_duration_sec,				// 15: pre-setting for duration of "enumWakdStates"

	// WAKD parameters
	parameter_NaAbsL,		    // 16: lower Na absolute threshold
	parameter_NaAbsH,     		// 17: upper Na absolute threshold
	parameter_NaTrdLT,			// 18: lower Na time period
	parameter_NaTrdHT,			// 19: upper Na time period
	parameter_NaTrdLPsT,		// 1a: lower Na trend per Specified Time threshold
	parameter_NaTrdHPsT,		// 1b: upper Na trend per Specified Time threshold

	parameter_KAbsL,			// 1c: lower K absolute threshold
	parameter_KAbsH,			// 1d: upper K absolute threshold
	parameter_KAAL,				// 1e: lower K absolute alarm threshold
	parameter_KAAH,				// 1f: upper K absolute alarm threshold
	parameter_KTrdLT,			// 20: lower K trend low time-difference
	parameter_KTrdHT,			// 21: upper K trend low time-difference
	parameter_KTrdLPsT,			// 22: lower K trend per Specified Time threshold
	parameter_KTrdHPsT,			// 23: upper K trend per Specified Time threshold

	parameter_CaAbsL,			// 24: lower Ca absolute threshold
	parameter_CaAbsH,			// 25: upper Ca absolute threshold
	parameter_CaAAbsL,			// 26: lower K absolute alarm threshold
	parameter_CaAAbsH,			// 27: upper K absolute alarm threshold
	parameter_CaTrdLT,			// 28: lower Ca time period
	parameter_CaTrdHT,			// 29: upper Ca time period
	parameter_CaTrdLPsT,		// 2a: lower Ca trend per Specified Time threshold
	parameter_CaTrdHPsT,		// 2b: upper Ca trend per Specified Time threshold

	parameter_UreaAAbsH,		// 2c: upper Urea absolute alarm threshold
	parameter_UreaTrdHT,		// 2d: upper Urea time period
	parameter_UreaTrdHPsT,		// 2e: upper Urea trend per Specified Time

	parameter_CreaAbsL,			// 2f: lower Crea absolute threshold
	parameter_CreaAbsH,			// 30: upper Crea absolute threshold
	parameter_CreaTrdHT,		// 31: upper Crea time period
	parameter_CreaTrdHPsT,		// 32: upper Crea trend per Specified Time

	parameter_PhosAbsL,			// 33: lower Phos absolute threshold
	parameter_PhosAbsH,			// 34: higher Phos absolute threshold
	parameter_PhosAAbsH,		// 35: upper Phos absolute alarm threshold
	parameter_PhosTrdLT,		// 36: lower Phos time period
	parameter_PhosTrdHT,		// 37: upper Phos time period
	parameter_PhosTrdLPsT,		// 38: lower Phos trend per Specified Time threshold
	parameter_PhosTrdHPsT,		// 39: upper Phos trend per Specified Time

	parameter_HCO3AAbsL,		// 3a: lower HCO3 absolute alarm threshold
	parameter_HCO3AbsL,			// 3b: lower HCO3 absolute threshold
	parameter_HCO3AbsH,			// 3c: upper HCO3 absolute threshold
	parameter_HCO3TrdLT,		// 3d: lower HCO3 time period
	parameter_HCO3TrdHT,		// 3e: upper HCO3 time period
	parameter_HCO3TrdLPsT,		// 3f: lower HCO3 trend per Specified Time threshold
	parameter_HCO3TrdHPsT,		// 40: upper HCO3 trend per Specified Time

	parameter_PhAAbsL,			// 41: lower pH absolute alarm threshold
	parameter_PhAAbsH,			// 42: upper pH absolute alarm threshold
	parameter_PhAbsL,			// 43: lower pH absolute threshold
	parameter_PhAbsH,			// 44: upper pH absolute threshold
	parameter_PhTrdLT,			// 45: lower pH time period
	parameter_PhTrdHT,			// 46: upper pH time period
	parameter_PhTrdLPsT,		// 47: lower pH trend per Specified Time threshold
	parameter_PhTrdHPsT,		// 48: upper pH trend per Specified Time threshold

	parameter_BPsysAbsL,		// 49: lower BPsys absolute threshold
	parameter_BPsysAbsH,		// 4a: upper BPsys absolute threshold
	parameter_BPsysAAbsH,		// 4b: upper BPsys absolute alarm threshold
	parameter_BPdiaAbsL,		// 4c: lower BPdia absolute threshold
	parameter_BPdiaAbsH,		// 4d: upper BPdia absolute threshold
	parameter_BPdiaAAbsH,		// 4e: upper BPdia absolute alarm threshold

	parameter_pumpFaccDevi,		// 4f: accepted flow deviation ml/min
	parameter_pumpBaccDevi,		// 50: accepted flow deviation ml/min

	parameter_VertDeflecitonT,	// 51: accepted vertical deflection threshold

	parameter_WghtAbsL,			// 52: lower Weight absolute threshold
	parameter_WghtAbsH,			// 53: upper Weight absolute threshold
	parameter_WghtTrdT,			// 54: Weight thrend threshold
	parameter_FDpTL,			// 55: Fluid Extraction Deviation Percentage Threshold Low
	parameter_FDpTH,			// 56: Fluid Extraction Deviation Percentage Threshold

	parameter_BPSoTH,			// 57: sensor 'BPSo' pressure upper threshold
	parameter_BPSoTL,			// 58: sensor 'BPSo' pressure lower threshold
	parameter_BPSiTH,			// 59: sensor 'BPSi' pressure upper threshold
	parameter_BPSiTL,			// 5a: sensor 'BPSi' pressure lower threshold
	parameter_FPS1TH,			// 5b: sensor 'FPS1' pressure upper threshold
	parameter_FPS1TL,			// 5c: sensor 'FPS1' pressure lower threshold
	parameter_FPS2TH,			// 5d: sensor 'FPS2' pressure upper threshold
	parameter_FPS2TL,			// 5e: sensor 'FPS2' pressure lower threshold

	parameter_BTSoTH,			// 5f: temperature sensor BTSo value upper threshold
	parameter_BTSoTL,			// 60: temperature sensor BTSo value lower threshold
	parameter_BTSiTH,			// 61: temperature sensor BTSi value upper threshold
	parameter_BTSiTL,			// 62: temperature sensor BTSi value lower threshold

	parameter_BatStatT,			// 63: battery status threshold

	// Expected adsorption rate calculation
	// parameters named according to Nanodialysis funciton of the sorbend unit
	parameter_f_K,				// 64:
	parameter_SCAP_K,			// 65:
	parameter_P_K,				// 66:
	parameter_Fref_K,			// 67:
	parameter_cap_K,			// 68:
	parameter_f_Ph,				// 69:
	parameter_SCAP_Ph,			// 6a:
	parameter_P_Ph,				// 6b:
	parameter_Fref_Ph,			// 6c:
	parameter_cap_Ph,			// 6d:
	parameter_A_dm2,			// 6e:
	parameter_CaAdr,			// 6f:
	parameter_CreaAdr,			// 70:
	parameter_HCO3Adr,			// 71:
	parameter_wgtNa,			// 72: the fuzzy importance of the adsorption of this substance
	parameter_wgtK,				// 73:
	parameter_wgtCa,			// 74:
	parameter_wgtUr,			// 75:
	parameter_wgtCrea,			// 76:
	parameter_wgtPhos,			// 77:
	parameter_wgtHCO3,			// 78:
	parameter_mspT,				// 79: set the threshold here!
} tdParameter;

typedef enum enumErrMsg
{
	errmsg_TimeoutOnDispQueue, // #0
	errmsg_TimeoutOnDsQueue,
	errmsg_TimeoutOnFcQueue,
	errmsg_TimeoutOnRtbpaQueue,
	errmsg_TimeoutOnCbpaQueue,
	errmsg_TimeoutOnSccQueue,
	errmsg_TimeoutOnUiQueue,
	errmsg_QueueOfDispatcherTaskFull,
	errmsg_QueueOfDataStorageTaskFull,
	errmsg_QueueOfSystemCheckCalibrationTaskFull,
	errmsg_QueueOfCommunicationBoardAbstractionTaskFull, // #10
	errmsg_QueueOfRealtimeBoardAbstractionTaskFull,
	errmsg_QueueOfFlowControlTaskFull,
	errmsg_QueueOfReminderTaskFull,
	errmsg_QueueOfUserInterfaceTaskFull,
	errmsg_nackFromCB,
	errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask,
	errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask,
	errmsg_IsrMbButtonEventLost,
	errmsg_DispDataWeightSensorReadEventLost,
	errmsg_DispDataPatientProfielWriteEventLost, // #20
	errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask,
	errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask,
	errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask,
	errmsg_DispChangeWAKDStateFromToEventLost,
	errmsg_pvPortMallocFailed,
	errmsg_getDataFailed,
	errmsg_putDataFailed,
	errmsg_ConfiguringWakdStateFailed,
	errmsg_ConfiguredStateTimesMismatch,
	errmsg_ErrFlowControl, // #30
	errmsg_ErrSCC,
	errmsg_ErrSendNack,
	errmsg_ErrSendAck,
	errmsg_command_WakeupCallEventLost,
	errmsg_CommunicationBoardUnreachable,
	errmsg_NotInListForAckOnMsgCBPA,
	errmsg_PutDataFunctionFailed,
	errmsg_UnknownRecipient,
	errmsg_SDcardError,
	errmsg_FlowControlStateIsUnknown, // #40
	errmsg_IsrCbReceivedFaultySlipMsg,
	errmsg_SmartphoneUnreachable,
	errmsg_ReadingSDcardFailed
} tdErrMsg;

typedef enum enumMsg 
{
	// here are messages send by the decision tree, if certain states detected
	// just like violations of thresholds for medical parameters
	// Prefix_ooRangeThresold
	// 'detected' indicates a occurred situation
	// a threshold under/overshoot or a detected anomaly or...
	// this is followed then by the name of the threshold/anomaly
	// afterwards the rest of the system needs to react, like when 'BPdiaAAbsH' exceeds:
	// 1. a msg to patient "contact physician" and
	// 2. a msg to physician "ALARM: BP-diastolic is high" NEED TO GO OFF!
	
	// blood pressure
	decTreeMsg_detectedBPsysAbsL, // #0
	decTreeMsg_detectedBPsysAbsH,
	decTreeMsg_detectedBPsysAAbsH,
	decTreeMsg_detectedBPdiaAbsL,
	decTreeMsg_detectedBPdiaAbsH,
	decTreeMsg_detectedBPdiaAAbsH,
	
	// weight
	dectreeMsg_detectedWghtAbsL, //#6
	dectreeMsg_detectedWghtAbsH,
	dectreeMsg_detectedWghtTrdT,
	
	//actuator data
	dectreeMsg_detectedpumpBaccDevi, //#9
	dectreeMsg_detectedpumpFaccDevi,
	
	// physical
	dectreeMsg_detectedBPorFPhigh, //#11
	dectreeMsg_detectedBPorFPlow,
	dectreeMsg_detectedBTSoTH,
	dectreeMsg_detectedBTSoTL,
	dectreeMsg_detectedBTSiTH,
	dectreeMsg_detectedBTSiTL,
	
	// physiological
	dectreeMsg_detectedSorDys, //#17
	dectreeMsg_detectedPhAAbsL,
	dectreeMsg_detectedPhAAbsH,
	dectreeMsg_detectedPhAbsLorPhAbsH, //#20
	dectreeMsg_detectedPhTrdLPsT,
	dectreeMsg_detectedPhTrdHPsT,
	dectreeMsg_detectedNaAbsLorNaAbsH,
	dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT,
	dectreeMsg_detectedKAbsLorKAbsHandSorDys, //#25
	dectreeMsg_detectedKAbsLorKAbsHnoSorDys,
	dectreeMsg_detectedKAAL,
	dectreeMsg_detectedKAAHandKincrease,
	dectreeMsg_detectedKAAHnoKincrease,
	dectreeMsg_detectedKTrdLPsT, //#30
	dectreeMsg_detectedKTrdHPsT,
	dectreeMsg_detectedKTrdofnriandSorDys,
	dectreeMsg_detectedPhTrdofnriandSorDys,
	dectreeMsg_detectedUreaAAbsHandSorDys,
	dectreeMsg_detectedUreaAAbsHnoSorDys,  //#35
	dectreeMsg_detectedUreaTrdHPsT,
	
	// Messages from Flow Control.
	flowControlMsg_excretionBagNotConnected,

} tdInfoMsg;
#endif
