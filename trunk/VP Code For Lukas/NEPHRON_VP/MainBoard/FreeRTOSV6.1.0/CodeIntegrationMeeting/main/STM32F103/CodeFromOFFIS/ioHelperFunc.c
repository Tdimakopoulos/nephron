/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "nephron.h"
#include <string.h>
#include <stdio.h>

void sFree(void **vp)
{
	vPortFree(*vp);
	*vp = NULL;
}

void printConfigureState(const char *pPrefix, tdMsgConfigureState *pConfigureState)
{
	taskMessage("I", pPrefix, "recipientId:......%s", whoIdToString(	pConfigureState->header.recipientId));
	taskMessage("I", pPrefix, "msgSize:..........%d",					pConfigureState->header.msgSize);
	taskMessage("I", pPrefix, "dataId:...........%s", dataIdToString(	pConfigureState->header.dataId));
	taskMessage("I", pPrefix, "msgCount:.........%d",           		pConfigureState->header.msgCount);
	taskMessage("I", pPrefix, "issuedBy:.........%s", whoIdToString( 	pConfigureState->header.issuedBy));
	taskMessage("I", pPrefix, "state configure:..%s", stateIdToString(	pConfigureState->stateToConfigure));
	taskMessage("I", pPrefix, "time stamp:.......%d ms",				pConfigureState->actCtrl.msOffset_ActCtrl);
	taskMessage("I", pPrefix, "BL pump direction:%d", 					pConfigureState->actCtrl.BLPumpCtrl.direction);
	taskMessage("I", pPrefix, "BL pump flow ref:.%d", 					pConfigureState->actCtrl.BLPumpCtrl.flowReference);
	taskMessage("I", pPrefix, "FL pump direction:%d", 					pConfigureState->actCtrl.FLPumpCtrl.direction);
	taskMessage("I", pPrefix, "FL pump flow ref:.%d", 					pConfigureState->actCtrl.FLPumpCtrl.flowReference);
	taskMessage("I", pPrefix, "Polarizer direct:.%d", 					pConfigureState->actCtrl.PolarizationCtrl.direction);
	taskMessage("I", pPrefix, "Polarizer ref:....%d",	 				pConfigureState->actCtrl.PolarizationCtrl.voltageReference);
}

void printActuatorData(const char *pPrefix, tdActuatorData *pActuators)
{

                #if defined VP_SIMULATION
	printf("*********** Flow: %d\n", pActuators->BLPumpData.Flow);
        #endif

	taskMessage("I", pPrefix, "Received Data Actuator Values.");
	taskMessage("I", pPrefix, "               Switch (on/off): %d", pActuators->BLPumpData.SwitchONnOFF);
	taskMessage("I", pPrefix, "    Bloodpump (forward/revers): %d", pActuators->BLPumpData.Direction);
	taskMessage("I", pPrefix, "      Speed of Bloodpump Motor: %d", pActuators->BLPumpData.Speed);
	taskMessage("I", pPrefix, "           Bloodpump Flow Rate: %d", pActuators->BLPumpData.FlowReference);
	taskMessage("I", pPrefix, "    Flow through the Blumdpump: %d", pActuators->BLPumpData.Flow);
	taskMessage("I", pPrefix, "                       Current: %d", pActuators->BLPumpData.Current);
//	taskMessage("I", pPrefix, "              Bloodpump Status: %d", pActuators->BLPumpData.Status);
	taskMessage("I", pPrefix, "   Fluidicpump Switch (on/off): %d", pActuators->FLPumpData.SwitchONnOFF);
	taskMessage("I", pPrefix, "  Fluidicpump (forward/revers): %d", pActuators->FLPumpData.Direction);
	taskMessage("I", pPrefix, "    Speed of Fluidicpump Motor: %d", pActuators->FLPumpData.Speed);
	taskMessage("I", pPrefix, "         Fluidicpump Flow Rate: %d", pActuators->FLPumpData.FlowReference);
	taskMessage("I", pPrefix, "  Flow through the Fluidicpump: %d", pActuators->FLPumpData.Flow);
	taskMessage("I", pPrefix, "                       Current: %d", pActuators->FLPumpData.Current);
//	taskMessage("I", pPrefix, "            Fluidicpump Status: %d", pActuators->FLPumpData.Status);
	taskMessage("I", pPrefix, "     Polarizer Switch (on/off): %d", pActuators->PolarizationData.SwitchONnOFF);
    taskMessage("I", pPrefix, "      Voltage (forward/revers): %d", pActuators->PolarizationData.Direction);
    taskMessage("I", pPrefix, "             Voltage Flow Rate: %d", pActuators->PolarizationData.VoltageReference);
    taskMessage("I", pPrefix, "             Polarizer Voltage: %d", pActuators->PolarizationData.Voltage);
    //taskMessage("I", pPrefix, "              Polarizer Status: %d", pActuators->PolarizationData.Status);
}

void printPhysiologicalData(const char *pPrefix, tdPhysiologicalData *pPhysiologicalData)
{
	taskMessage("I", pPrefix, "Physiological data package time: %d s", pPhysiologicalData->TimeStamp);
	taskMessage("I", pPrefix, "  inlet  Na+        = %f", (((float) pPhysiologicalData->ECPDataI.Sodium)/FIXPOINTSHIFT_NA));
	taskMessage("I", pPrefix, "  outlet Na+        = %f", (((float) pPhysiologicalData->ECPDataO.Sodium)/FIXPOINTSHIFT_NA));
	taskMessage("I", pPrefix, "  inlet  K+         = %f", (((float) pPhysiologicalData->ECPDataI.Potassium)/FIXPOINTSHIFT_K));
	taskMessage("I", pPrefix, "  outlet K+         = %f", (((float) pPhysiologicalData->ECPDataO.Potassium)/FIXPOINTSHIFT_K));
	taskMessage("I", pPrefix, "  inlet  Urea       = %f", (((float) pPhysiologicalData->ECPDataI.Urea)/FIXPOINTSHIFT_UREA));
	taskMessage("I", pPrefix, "  outlet Urea       = %f", (((float) pPhysiologicalData->ECPDataO.Urea)/FIXPOINTSHIFT_UREA));
//	taskMessage("I", pPrefix, "  inlet  Creatine   = %f", (((float) pPhysiologicalData->ECPDataI.Creatinine)/FIXPOINTSHIFT_CREATININE));
//	taskMessage("I", pPrefix, "  outlet Creatine   = %f", (((float) pPhysiologicalData->ECPDataO.Creatinine)/FIXPOINTSHIFT_CREATININE));
//	taskMessage("I", pPrefix, "  inlet  Phostphate = %f", (((float) pPhysiologicalData->ECPDataI.Phosphate)/FIXPOINTSHIFT_PHOSPHATE));
//	taskMessage("I", pPrefix, "  outlet Phostphate = %f", (((float) pPhysiologicalData->ECPDataO.Phosphate)/FIXPOINTSHIFT_PHOSPHATE));
	taskMessage("I", pPrefix, "  inlet  pH         = %f", (((float) pPhysiologicalData->ECPDataI.pH)/FIXPOINTSHIFT_PH));
	taskMessage("I", pPrefix, "  outlet pH         = %f", (((float) pPhysiologicalData->ECPDataO.pH/FIXPOINTSHIFT_PH)));
	taskMessage("I", pPrefix, "  inlet  ECPi Temp  = %f", (((float) pPhysiologicalData->ECPDataI.Temperature)/FIXPOINTSHIFT_TEMPERATURE));
	taskMessage("I", pPrefix, "  outlet ECPo Temp  = %f", (((float) pPhysiologicalData->ECPDataO.Temperature)/FIXPOINTSHIFT_TEMPERATURE));	
}
 
void printPhysicalData(const char *pPrefix, tdPhysicalsensorData *pPhysicalData)
{
	taskMessage("I", pPrefix, "Physical data package time: %d s", pPhysicalData->TimeStamp);
	//taskMessage("I", pPrefix, "  ECPI status			= %d", ((uint8_t) pPhysicalData->statusECPI));
	//taskMessage("I", pPrefix, "  ECPO status			= %d", ((uint8_t) pPhysicalData->statusECPO));
	taskMessage("I", pPrefix, "  FCI pressure fluid		= %f", (((float) pPhysicalData->PressureFCI.Pressure)						/FIXPOINTSHIFT_PRESSURE));
	taskMessage("I", pPrefix, "  FCO pressure fluid		= %f", (((float) pPhysicalData->PressureFCO.Pressure)						/FIXPOINTSHIFT_PRESSURE));
	taskMessage("I", pPrefix, "  BCI pressure blood		= %f", (((float) pPhysicalData->PressureBCI.Pressure)						/FIXPOINTSHIFT_PRESSURE));
	taskMessage("I", pPrefix, "  BCO pressure blood		= %f", (((float) pPhysicalData->PressureBCO.Pressure)						/FIXPOINTSHIFT_PRESSURE));
	taskMessage("I", pPrefix, "  Temperature Blood I	= %f", (((float) pPhysicalData->TemperatureInOut.TemperatureInlet_Value)	/FIXPOINTSHIFT_TEMPERATURE));
	taskMessage("I", pPrefix, "  Temperature Blood O	= %f", (((float) pPhysicalData->TemperatureInOut.TemperatureOutlet_Value)	/FIXPOINTSHIFT_TEMPERATURE));
	taskMessage("I", pPrefix, "  Conductivity Real		= %f", (((float) pPhysicalData->CSENS1Data.Cond_FCR)						/FIXPOINTSHIFT_DCS_CONDUCT_FCR));
	taskMessage("I", pPrefix, "  Conductivity Imag		= %f", (((float) pPhysicalData->CSENS1Data.Cond_FCQ)						/FIXPOINTSHIFT_DCS_CONDUCT_FCQ));
	taskMessage("I", pPrefix, "  Conductivity PT1000	= %f", (((float) pPhysicalData->CSENS1Data.Cond_PT1000)						/FIXPOINTSHIFT_DCS_CONDUCT_PT1000));
//	taskMessage("I", pPrefix, "  MFSI status switch		= %d", ((uint8_t) pPhysicalData->MultiSwitchBIData.Position));
//	taskMessage("I", pPrefix, "  MFSO status switch		= %d", ((uint8_t) pPhysicalData->MultiSwitchBOData.Position));
//	taskMessage("I", pPrefix, "  MFSU status switch		= %d", ((uint8_t) pPhysicalData->MultiSwitchBUData.Position));
//	taskMessage("I", pPrefix, "  Pump Blood (on/off)	= %d", ((uint8_t) pPhysicalData->BLPumpData.SwitchONnOFF));
//	taskMessage("I", pPrefix, "  Pump Blood (direction)	= %d", ((uint8_t) pPhysicalData->BLPumpData.Direction));
//	taskMessage("I", pPrefix, "  Pump Blood (speed)		= %f", (((float) pPhysicalData->BLPumpData.Speed)							/FIXPOINTSHIFT_PUMP_SPEED));
//	taskMessage("I", pPrefix, "  Pump Blood (reference)	= %f", (((float) pPhysicalData->BLPumpData.FlowReference)					/FIXPOINTSHIFT_PUMP_FLOW));
//	taskMessage("I", pPrefix, "  Pump Blood (flow)		= %f", (((float) pPhysicalData->BLPumpData.Flow)							/FIXPOINTSHIFT_PUMP_FLOW));
//	taskMessage("I", pPrefix, "  Pump Fluid (on/off)	= %d", ((uint8_t) pPhysicalData->FLPumpData.SwitchONnOFF));
//	taskMessage("I", pPrefix, "  Pump Fluid (direction)	= %d", ((uint8_t) pPhysicalData->FLPumpData.Direction));
//	taskMessage("I", pPrefix, "  Pump Fluid (speed)		= %f", (((float) pPhysicalData->FLPumpData.Speed)							/FIXPOINTSHIFT_PUMP_SPEED));
//	taskMessage("I", pPrefix, "  Pump Fluid (reference)	= %f", (((float) pPhysicalData->FLPumpData.FlowReference)					/FIXPOINTSHIFT_PUMP_FLOW));
//	taskMessage("I", pPrefix, "  Pump Fluid (flow)		= %f", (((float) pPhysicalData->FLPumpData.Flow)							/FIXPOINTSHIFT_PUMP_FLOW));
//	taskMessage("I", pPrefix, "  Polarizer (on/off)		= %d", ((uint8_t) pPhysicalData->PolarizationData.SwitchONnOFF));
//	taskMessage("I", pPrefix, "  Polarizer (direction)	= %d", ((uint8_t) pPhysicalData->PolarizationData.Direction));
//	taskMessage("I", pPrefix, "  Polarizer (reference)	= %f", (((float) pPhysicalData->PolarizationData.VoltageReference)			/FIXPOINTSHIFT_POLARIZ_VOLT));
//	taskMessage("I", pPrefix, "  Polarizer (voltage)	= %f", (((float) pPhysicalData->PolarizationData.Voltage)					/FIXPOINTSHIFT_POLARIZ_VOLT));
}

void printMsgWakdStateFromTo(tdMsgWakdStateFromTo *pMsgWakdStateFromTo)
{
                #if defined VP_SIMULATION
	printf("**** recipientId:......"); printfWhoId( pMsgWakdStateFromTo->header.recipientId); printf("\n");
	printf("**** msgSize:..........%d\n",           pMsgWakdStateFromTo->header.msgSize);
	printf("**** dataId:..........."); printfDataId(pMsgWakdStateFromTo->header.dataId);      printf("\n");
	printf("**** msgCount:.........%d\n",           pMsgWakdStateFromTo->header.msgCount);
	printf("**** issuedBy:........."); printfWhoId( pMsgWakdStateFromTo->header.issuedBy);    printf("\n");
	printf("**** fromWakdState:...."); printfStateId(  pMsgWakdStateFromTo->fromWakdState);   printf("\n");
	printf("**** fromWakdOpState:.."); printfOpStateId(pMsgWakdStateFromTo->fromWakdOpState); printf("\n");
	printf("**** toWakdState:......"); printfStateId(  pMsgWakdStateFromTo->toWakdState);     printf("\n");
	printf("**** toWakdOpState:...."); printfOpStateId(pMsgWakdStateFromTo->toWakdOpState);   printf("\n");
        #endif
}

void taskMessage(const char * severity, const char * pref, const char * message, ...)
{
	#ifdef VP_SIMULATION
		// during simulation we make "printf"s on simulation trace xterm window
		va_list vargzeiger;
		va_start(vargzeiger, message);
		if (strcmp(severity, "I") == 0) {
			printf("Info ");
		} else if (strcmp(severity, "W") == 0) {
			printf("Warning ");
		} else if (strcmp(severity, "E") == 0) {
			printf("Error ");
		} else if (strcmp(severity, "F") == 0) {
			printf("Failure ");
		} else {
			printf("N.D. ");
		}
		printf("(%s) ", pref);
		vprintf(message, vargzeiger);
		printf("\n");
		va_end(vargzeiger);
	#endif

	#ifdef STM3210B
		xLCDMessage LCDMessage;
		va_list vargzeiger;
		va_start(vargzeiger, message);
		char msgFormated[MAXNUMCHARONLCD];
		vsprintf(msgFormated, message, vargzeiger);
		va_end(vargzeiger);
		if ( (strlen(severity))+(strlen(pref))+(strlen(msgFormated))+3 > MAXNUMCHARONLCD)
		{
			strcpy(LCDMessage.pcMessage, "Msg too long for LCD!\n");
		} else {
			strcpy(LCDMessage.pcMessage, severity);
			strcat(LCDMessage.pcMessage, ":");
			strcat(LCDMessage.pcMessage, pref);
			strcat(LCDMessage.pcMessage, "-");
			strcat(LCDMessage.pcMessage, message);
			strcat(LCDMessage.pcMessage, "\n");
		}
		xQueueSend( pAllQueueHandles->qhLCDQueue, &LCDMessage, DISP_BLOCKING);
	#endif
}

// Function to be used to inform user (user interface) and logfile (data storage)
// about trouble in the system. MUST NOT BE USED from within interrupt service
// routines. For ISR use errMsgISR()!!
void errMsg(tdErrMsg msg)
{
 	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	// BE AWARE: we are misusing the pointer pData to transport an enumeration type.
	// The recipient has to be aware of this and must not interpret as pointer!
	Qtoken.pData	= (void *) msg;

	// Sent msg to FRONT passing all the other to be really quick about this.
	// Sending to data storage and display. We do not check if the msg arrive since:
	// a) this would be a second err in a row.
	// b) one of the following two should arrive.
	// If both msg are lost to inform about the initial err than this would be three err altogether.
	// In this case too much went wrong to handle it!! Info will be lost.
	xQueueSendToFront( allHandles.allQueueHandles.qhDSin, &Qtoken, DISP_BLOCKING);
	xQueueSendToFront( allHandles.allQueueHandles.qhUIin, &Qtoken, DISP_BLOCKING);
}


void sendSystemInfo(const char *pPrefix, xQueueHandle *pQHDISPin, tdInfoMsg msgEnum)
{
	tdQtoken Qtoken;
	Qtoken.command = command_UseAtachedMsgHeader;

	tdMsgSystemInfo *pDesTree_Msg  = NULL;
	pDesTree_Msg  = (tdMsgSystemInfo *) pvPortMalloc(sizeof(tdMsgSystemInfo)); // Will be freed by receiver task!
	if ( pDesTree_Msg == NULL )
	{	// unable to allocate memory
		taskMessage("E", pPrefix, "Unable to allocate memory. Heap full?");
		errMsg(errmsg_pvPortMallocFailed);
	} else {
		pDesTree_Msg->header.recipientId =whoId_SP;
		pDesTree_Msg->header.msgSize	= sizeof(tdMsgSystemInfo);
		pDesTree_Msg->header.dataId		= dataID_systemInfo;
		pDesTree_Msg->header.msgCount	= 0; // not defined; unused
		pDesTree_Msg->header.issuedBy	= whoId_MB;
		pDesTree_Msg->msgEnum = msgEnum;

		Qtoken.pData   = (void *)  pDesTree_Msg;

		#ifdef DEBUG_SCC
			taskMessage("I", pPrefix, "DecissionTree sends message %s.", infoMsgToString(msgEnum));
		#endif
		if ( xQueueSend( pQHDISPin, &Qtoken, SCC_BLOCKING) != pdPASS )
		{	// Seemingly the queue is full. Do something accordingly!
			taskMessage("E", pPrefix, "Queue of Dispatcher did not accept data. Decision Tree Message is lost!!!");
			errMsg(errmsg_QueueOfDispatcherTaskFull);
			sFree((void *)&pDesTree_Msg);
		}
	}
}

// Function to be called within interrupt service routine to inform user (user interface) and logfile (data storage)
// about trouble in the system. Use errMsg() outside ISR.
void errMsgISR(tdErrMsg msg)
{
	// xQueueSend() will set *pxHigherPriorityTaskWoken to
	// pdTRUE if sending to the queue caused a task to unblock, and the
	// unblocked task has a priority higher than the currently running task. If
	// this value is pdTRUE then a context switch should be performed before the interrupt is exited.
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

 	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	// BE AWARE: we are misusing the pointer pData to transport an enumeration type.
	// The recipient has to be aware of this and must not interpret as pointer!
	Qtoken.pData	= (void *) msg;

	// Sent msg to FRONT passing all the other to be really quick about this.
	// Sending to data storage and display. We do not check if the msg arrive since:
	// a) this would be a second err in a row.
	// b) one of the following two should arrive.
	// If both msg are lost to inform about the err than this would be three err altogether.
	// In this case too much went wrong to handle it!! Info will be lost.
	xQueueSendToFrontFromISR( allHandles.allQueueHandles.qhDSin, &Qtoken, &xHigherPriorityTaskWoken);
	xQueueSendToFrontFromISR( allHandles.allQueueHandles.qhUIin, &Qtoken, &xHigherPriorityTaskWoken);
}

void defaultCommandHandling(char *prefix, tdCommand command)
{
	switch (command) {
	case (command_DefaultError):{
		taskMessage("E", prefix, "Don't know what to do with 'command_DefaultError'!");
		break;
	}
	case (command_InitializeSCC):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InitializeSCC'!");
		break;
	}
	case (command_InitializeSCCReady):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InitializeSCCReady'!");
		break;
	}
	case (command_ButtonEvent):{
		taskMessage("E", prefix, "Don't know what to do with 'command_ButtonEvent'!");
		break;
	}
	case (command_InitializeFC):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InitializeFC'!");
		break;
	}
	// case (command_DataFCInitialized):{
		// taskMessage("E", prefix, "Don't know what to do with 'command_DataFCInitialized'!");
		// break;
	// }
	case (command_InformGuiConnectBag):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InformGuiConnectBag'!");
		break;
	}
	case (command_InformGuiDisconnectBag):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InformGuiDisconnectBag'!");
		break;
	}
	case (command_BufferFromRTBavailable):{
		taskMessage("E", prefix, "Don't know what to do with 'command_BufferFromRTBavailable'!");
		break;
	}
	case (command_BufferFromCBavailable):{
		taskMessage("E", prefix, "Don't know what to do with 'command_BufferFromCBavailable'!");
		break;
	}
	case (command_BufferFromPB1available):{
		taskMessage("E", prefix, "Don't know what to do with 'command_BufferFromPB1available'!");
		break;
	}
	case (command_BufferFromPB2available):{
		taskMessage("E", prefix, "Don't know what to do with 'command_BufferFromPB2available'!");
		break;
	}
//	case (command_TransmissionErrorRTB):{
//		taskMessage("E", prefix, "Don't know what to do with 'command_TransmissionErrorRTB'!");
//		break;
//	}
//	case (command_TransmissionErrorCB):{
//		taskMessage("E", prefix, "Don't know what to do with 'command_TransmissionErrorCB'!");
//		break;
//	}
//	case (command_TransmissionErrorPB1):{
//		taskMessage("E", prefix, "Don't know what to do with 'command_TransmissionErrorPB1'!");
//		break;
//	}
//	case (command_TransmissionErrorPB2):{
//		taskMessage("E", prefix, "Don't know what to do with 'command_TransmissionErrorPB2'!");
//		break;
//	}
	case (command_WaitOnMsgCBPA):{
		taskMessage("E", prefix, "Don't know what to do with 'command_WaitOnMsgCBPA'!");
		break;
	}
	case (command_AckOnMsgCBPA):{
		taskMessage("E", prefix, "Don't know what to do with 'command_AckOnMsgCBPA'!");
		break;
	}
	case (command_WakeupCall):{
		taskMessage("E", prefix, "Don't know what to do with 'command_WakeupCall'!");
		break;
	}
	case (command_UseAtachedMsgHeader):{
		taskMessage("E", prefix, "Don't know what to do with 'command_UseAtachedMsgHeader'!");
		break;
	}
	case (command_RtbChangeIntoStateRequest):{
		taskMessage("E", prefix, "Don't know what to do with 'command_RtbReceivedChangeStateRequest'!");
		break;
	}
	case (command_RequestDatafromDS):{
		taskMessage("E", prefix, "Don't know what to do with 'command_RequestDatafromDS'!");
		break;
	}
	case (command_getPhysioPatientData):{
		taskMessage("E", prefix, "Don't know what to do with 'command_getPhysioPatientData'!");
		break;
	}
	case (command_InitializeFCReady):{
		taskMessage("E", prefix, "Don't know what to do with 'command_InitializeFCReady'!");
		break;
	}
	case (command_ConfigurationChanged):{
		taskMessage("E", prefix, "Don't know what to do with 'command_ConfigurationChanged'!");
		break;
	}	
	case (command_RequestDatafromDSReady):{
		taskMessage("E", prefix, "Don't know what to do with 'command_RequestDatafromDSReady'!");
		break;
	}
	
	}

}

void defaultIdHandling(char *prefix, tdDataId dataId)
{
	switch (dataId) {
	case (dataID_undefined):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_Undefined'!");
		break;
	}
	case (dataID_reset):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_reset'!");
		break;
	}
	case (dataID_shutdown):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_shutdown'!");
		break;
	}
	case (dataID_setTimeDate):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_setTimeDate'!");
		break;
	}
	case (dataID_currentWAKDstateIs):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_currentWAKDstateIs'!");
		break;
	}
	case (dataID_ack):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_ack'!");
		break;
	}
	case (dataID_nack):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_nack'!");
		break;
	}
	case (dataID_statusRequest):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_statusRequest'!");
		break;
	}
	case (dataID_configureState):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_configureState'!");
		break;
	}	
	case (dataID_configureStateDump):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_configureStateDump'!");
		break;
	}
	case (dataID_configurePeriodPolarizerToggle):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_configurePeriodPolarizerToggle'!");
		break;
	}
	case (dataID_changeWAKDStateFromTo):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_changeWAKDStateFromTo'!");
		break;
	}
	case (dataID_physiologicalData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physiologicalData'!");
		break;
	}
	case (dataID_physicalData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physicalData'!");
		break;
	}
	case (dataID_actuatorData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_actuatorData'!");
		break;
	}
	case (dataID_alarmRTB):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_alarmRTB'!");
		break;
	}
	case (dataID_statusRTB):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_statusRTB'!");
		break;
	}
	case (dataID_weightRequest):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_weightRequest'!");
		break;
	}
	case (dataID_weightData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_weightData'!");
		break;
	}
	case (dataID_weightDataOK):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_weightDataOK'!");
		break;
	}
	case (dataID_bpRequest):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_bpRequest'!");
		break;
	}
	case (dataID_bpData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_bpData'!");
		break;
	}
	case (dataID_EcgRequest):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_EcgRequest'!");
		break;
	}
	case (dataID_EcgData):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_EcgData'!");
		break;
	}
	case (dataID_PatientProfile):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_PatientProfile'!");
		break;
	}
	case (dataID_WAKDAllStateConfigure):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_WAKDAllStateConfigure'!");
		break;
	}
	case (dataID_AllStatesParametersSCC):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_AllStatesParametersSCC'!");
		break;
	}
	case (dataID_StateParametersSCC):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_StateParametersSCC'!");
		break;
	}
	case (dataID_systemInfo):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_systemInfo'!");
		break;
	}
	case (dataID_physiologicalData_K):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physiologicalData_K'!");
		break;
	}
	case (dataID_physiologicalData_Na):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physiologicalData_Na'!");
		break;
	}	
	case (dataID_physiologicalData_pH):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physiologicalData_pH'!");
		break;
	}
	case (dataID_physiologicalData_Ur):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_physiologicalData_Ur'!");
		break;
	}	
	case (dataID_ConfigureParameters):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_ConfigureParameters'!");
		break;
	}
	case (dataID_CurrentParameters):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_CurrentParameters'!");
		break;
	}
	case (dataID_ActualParameters):{
		taskMessage("E", prefix, "Don't know what to do with 'dataID_ActualParameters'!");
		break;
	}
	}
}

char *opStateIdToString(tdWakdOperationalState opStateId)
{
	char *string = "UNKNOWN opStateId";
	switch (opStateId){
		case (wakdStatesOS_UndefinedInitializing):{
			string = "wakdStatesOS_UndefinedInitializing";
			break;
		}
		case (wakdStatesOS_Automatic):{
			string = "wakdStatesOS_Automatic";
			break;
		}
		case (wakdStatesOS_Semiautomatic):{
			string = "wakdStatesOS_Semiautomatic";
			break;
		}
	}
	return(string);
}

void printfOpStateId(tdWakdOperationalState opStateId)
{
                #if defined VP_SIMULATION
	printf(opStateIdToString(opStateId));
        #endif
}

char *stateIdToString(tdWakdStates stateId)
{
	char *string = "UNKNOWN stateId";
	switch (stateId) {
		case (wakdStates_UndefinedInitializing):{
			string = "wakdStates_UndefinedInitializing";
			break;
		}
		case (wakdStates_AllStopped):{
			string = "wakdStates_AllStopped";
			break;
		}
		case (wakdStates_Maintenance):{
			string = "wakdStates_Maintenance";
			break;
		}
		case (wakdStates_NoDialysate):{
			string = "wakdStates_NoDialysate";
			break;
		}
		case (wakdStates_Dialysis):{
			string = "wakdStates_Dialysis";
			break;
		}
		case (wakdStates_Regen1):{
			string = "wakdStates_Regen1";
			break;
		}
		case (wakdStates_Ultrafiltration):{
			string = "wakdStates_Ultrafiltration";
			break;
		}
		case (wakdStates_Regen2):{
			string = "wakdStates_Regen2";
			break;
		}
	}
	return(string);
}

void printfStateId(tdWakdStates stateId)
{
                #if defined VP_SIMULATION
	printf(stateIdToString(stateId));
        #endif
}

char *whoIdToString(tdWhoId whoId)
{
	char *string = "UNKNOWN whoId";
	switch (whoId) {
		case (whoId_nd):{
			string = "whoId_nd";
			break;
		}
		case (whoId_MB):{
			string = "whoId_MB";
			break;
		}
		case (whoId_RTB):{
			string = "whoId_RTB";
			break;
		}
		case (whoId_CB):{
			string = "whoId_CB";
			break;
		}
		case (whoId_SP):{
			string = "whoId_SP";
			break;
		}
		case (whoId_PMB1):{
			string = "whoId_PMB1";
			break;
		}
		case (whoId_PMB2):{
			string = "whoId_PMB2";
			break;
		}
	}
	return(string);
}

char *parameterToString(tdParameter parameter)
{
	char *string = "UNKNOWN parameter";
	switch (parameter) {
		case (parameter_name):{
			string = "parameter_name";
			break;
		}
		case (parameter_gender):{
			string = "parameter_gender";
			break;
		}
		case (parameter_measureTimeHours):{
			string = "parameter_measureTimeHours";
			break;
		}
		case (parameter_measureTimeMinutes):{
			string = "parameter_measureTimeMinutes";
			break;
		}
		case (parameter_wghtCtlTarget):{
			string = "parameter_wghtCtlTarget";
			break;
		}
		case (parameter_wghtCtlLastMeasurement):{
			string = "parameter_wghtCtlLastMeasurement";
			break;
		}
		case (parameter_wghtCtlDefRemPerDay):{
			string = "parameter_wghtCtlDefRemPerDay";
			break;
		}
		case (parameter_kCtlTarget):{
			string = "parameter_kCtlTarget";
			break;
		}
		case (parameter_kCtlLastMeasurement):{
			string = "parameter_kCtlLastMeasurement";
			break;
		}
		case (parameter_kCtlDefRemPerDay):{
			string = "parameter_kCtlDefRemPerDay";
			break;
		}
		case (parameter_urCtlTarget):{
			string = "parameter_urCtlTarget";
			break;
		}
		case (parameter_urCtlLastMeasurement):{
			string = "parameter_urCtlLastMeasurement";
			break;
		}
		case (parameter_urCtlDefRemPerDay):{
			string = "parameter_urCtlDefRemPerDay";
			break;
		}
		case (parameter_minDialPlasFlow):{
			string = "parameter_minDialPlasFlow";
			break;
		}
		case (parameter_maxDialPlasFlow):{
			string = "parameter_maxDialPlasFlow";
			break;
		}
		case (parameter_minDialVoltage):{
			string = "parameter_minDialVoltage";
			break;
		}
		case (parameter_maxDialVoltage):{
			string = "parameter_maxDialVoltage";
			break;
		}
		case (parameter_defSpeedBp_mlPmin):{
			string = "parameter_defSpeedBp_mlPmin";
			break;
		}
		case (parameter_defSpeedFp_mlPmin):{
			string = "parameter_defSpeedFp_mlPmin";
			break;
		}
		case (parameter_defPol_V):{
			string = "parameter_defPol_V";
			break;
		}
		case (parameter_direction):{
			string = "parameter_direction";
			break;
		}
		case (parameter_duration_sec):{
			string = "parameter_duration_sec";
			break;
		}
		case (parameter_NaAbsL):{
			string = "parameter_NaAbsL";
			break;
		}
		case (parameter_NaAbsH):{
			string = "parameter_NaAbsH";
			break;
		}
		case (parameter_NaTrdLT):{
			string = "parameter_NaTrdLT";
			break;
		}
		case (parameter_NaTrdHT):{
			string = "parameter_NaTrdHT";
			break;
		}
		case (parameter_NaTrdLPsT):{
			string = "parameter_NaTrdLPsT";
			break;
		}
		case (parameter_NaTrdHPsT):{
			string = "parameter_NaTrdHPsT";
			break;
		}
		case (parameter_KAbsL):{
			string = "parameter_KAbsL";
			break;
		}
		case (parameter_KAbsH):{
			string = "parameter_KAbsH";
			break;
		}
		case (parameter_KAAL):{
			string = "parameter_KAAL";
			break;
		}
		case (parameter_KAAH):{
			string = "parameter_KAAH";
			break;
		}
		case (parameter_KTrdLT):{
			string = "parameter_KTrdLT";
			break;
		}
		case (parameter_KTrdHT):{
			string = "parameter_KTrdHT";
			break;
		}
		case (parameter_KTrdLPsT):{
			string = "parameter_KTrdLPsT";
			break;
		}
		case (parameter_KTrdHPsT):{
			string = "parameter_KTrdHPsT";
			break;
		}
		case (parameter_CaAbsL):{
			string = "parameter_CaAbsL";
			break;
		}
		case (parameter_CaAbsH):{
			string = "parameter_CaAbsH";
			break;
		}
		case (parameter_CaAAbsL):{
			string = "parameter_CaAAbsL";
			break;
		}
		case (parameter_CaAAbsH):{
			string = "parameter_CaAAbsH";
			break;
		}
		case (parameter_CaTrdLT):{
			string = "parameter_CaTrdLT";
			break;
		}
		case (parameter_CaTrdHT):{
			string = "parameter_CaTrdHT";
			break;
		}
		case (parameter_CaTrdLPsT):{
			string = "parameter_CaTrdLPsT";
			break;
		}
		case (parameter_CaTrdHPsT):{
			string = "parameter_CaTrdHPsT";
			break;
		}
		case (parameter_UreaAAbsH):{
			string = "parameter_UreaAAbsH";
			break;
		}
		case (parameter_UreaTrdHT):{
			string = "parameter_UreaTrdHT";
			break;
		}
		case (parameter_UreaTrdHPsT):{
			string = "parameter_UreaTrdHPsT";
			break;
		}
		case (parameter_CreaAbsL):{
			string = "parameter_CreaAbsL";
			break;
		}
		case (parameter_CreaAbsH):{
			string = "parameter_CreaAbsH";
			break;
		}
		case (parameter_CreaTrdHT):{
			string = "parameter_CreaTrdHT";
			break;
		}
		case (parameter_CreaTrdHPsT):{
			string = "parameter_CreaTrdHPsT";
			break;
		}
		case (parameter_PhosAbsL):{
			string = "parameter_PhosAbsL";
			break;
		}
		case (parameter_PhosAbsH):{
			string = "parameter_PhosAbsH";
			break;
		}
		case (parameter_PhosAAbsH):{
			string = "parameter_PhosAAbsH";
			break;
		}
		case (parameter_PhosTrdLT):{
			string = "parameter_PhosTrdLT";
			break;
		}
		case (parameter_PhosTrdHT):{
			string = "parameter_PhosTrdHT";
			break;
		}
		case (parameter_PhosTrdLPsT):{
			string = "parameter_PhosTrdLPsT";
			break;
		}
		case (parameter_PhosTrdHPsT):{
			string = "parameter_PhosTrdHPsT";
			break;
		}
		case (parameter_HCO3AAbsL):{
			string = "parameter_HCO3AAbsL";
			break;
		}
		case (parameter_HCO3AbsL):{
			string = "parameter_HCO3AbsL";
			break;
		}
		case (parameter_HCO3AbsH):{
			string = "parameter_HCO3AbsH";
			break;
		}
		case (parameter_HCO3TrdLT):{
			string = "parameter_HCO3TrdLT";
			break;
		}
		case (parameter_HCO3TrdHT):{
			string = "parameter_HCO3TrdHT";
			break;
		}
		case (parameter_HCO3TrdLPsT):{
			string = "parameter_HCO3TrdLPsT";
			break;
		}
		case (parameter_HCO3TrdHPsT):{
			string = "parameter_HCO3TrdHPsT";
			break;
		}
		case (parameter_PhAAbsL):{
			string = "parameter_PhAAbsL";
			break;
		}
		case (parameter_PhAAbsH):{
			string = "parameter_PhAAbsH";
			break;
		}
		case (parameter_PhAbsL):{
			string = "parameter_PhAbsL";
			break;
		}
		case (parameter_PhAbsH):{
			string = "parameter_PhAbsH";
			break;
		}
		case (parameter_PhTrdLT):{
			string = "parameter_PhTrdLT";
			break;
		}
		case (parameter_PhTrdHT):{
			string = "parameter_PhTrdHT";
			break;
		}
		case (parameter_PhTrdLPsT):{
			string = "parameter_PhTrdLPsT";
			break;
		}
		case (parameter_PhTrdHPsT):{
			string = "parameter_PhTrdHPsT";
			break;
		}
		case (parameter_BPsysAbsL):{
			string = "parameter_BPsysAbsL";
			break;
		}
		case (parameter_BPsysAbsH):{
			string = "parameter_BPsysAbsH";
			break;
		}
		case (parameter_BPsysAAbsH):{
			string = "parameter_BPsysAAbsH";
			break;
		}
		case (parameter_BPdiaAbsL):{
			string = "parameter_BPdiaAbsL";
			break;
		}
		case (parameter_BPdiaAbsH):{
			string = "parameter_BPdiaAbsH";
			break;
		}
		case (parameter_BPdiaAAbsH):{
			string = "parameter_BPdiaAAbsH";
			break;
		}
		case (parameter_pumpFaccDevi):{
			string = "parameter_pumpFaccDevi";
			break;
		}
		case (parameter_pumpBaccDevi):{
			string = "parameter_pumpBaccDevi";
			break;
		}
		case (parameter_VertDeflecitonT):{
			string = "parameter_VertDeflecitonT";
			break;
		}
		case (parameter_WghtAbsL):{
			string = "parameter_WghtAbsL";
			break;
		}
		case (parameter_WghtAbsH):{
			string = "parameter_WghtAbsH";
			break;
		}
		case (parameter_WghtTrdT):{
			string = "parameter_WghtTrdT";
			break;
		}
		case (parameter_FDpTL):{
			string = "parameter_FDpTL";
			break;
		}
		case (parameter_FDpTH):{
			string = "parameter_FDpTH";
			break;
		}
		case (parameter_BPSoTH):{
			string = "parameter_BPSoTH";
			break;
		}
		case (parameter_BPSoTL):{
			string = "parameter_BPSoTL";
			break;
		}
		case (parameter_BPSiTH):{
			string = "parameter_BPSiTH";
			break;
		}
		case (parameter_BPSiTL):{
			string = "parameter_BPSiTL";
			break;
		}
		case (parameter_FPS1TH):{
			string = "parameter_FPS1TH";
			break;
		}
		case (parameter_FPS1TL):{
			string = "parameter_FPS1TL";
			break;
		}
		case (parameter_FPS2TH):{
			string = "parameter_FPS2TH";
			break;
		}
		case (parameter_FPS2TL):{
			string = "parameter_FPS2TL";
			break;
		}
		case (parameter_BTSoTH):{
			string = "parameter_BTSoTH";
			break;
		}
		case (parameter_BTSoTL):{
			string = "parameter_BTSoTL";
			break;
		}
		case (parameter_BTSiTH):{
			string = "parameter_BTSiTH";
			break;
		}
		case (parameter_BTSiTL):{
			string = "parameter_BTSiTL";
			break;
		}
		case (parameter_BatStatT):{
			string = "parameter_BatStatT";
			break;
		}
		case (parameter_f_K):{
			string = "parameter_f_K";
			break;
		}
		case (parameter_SCAP_K):{
			string = "parameter_SCAP_K";
			break;
		}
		case (parameter_P_K):{
			string = "parameter_P_K";
			break;
		}
		case (parameter_Fref_K):{
			string = "parameter_Fref_K";
			break;
		}
		case (parameter_cap_K):{
			string = "parameter_cap_K";
			break;
		}
		case (parameter_f_Ph):{
			string = "parameter_f_Ph";
			break;
		}
		case (parameter_SCAP_Ph):{
			string = "parameter_SCAP_Ph";
			break;
		}
		case (parameter_P_Ph):{
			string = "parameter_P_Ph";
			break;
		}
		case (parameter_Fref_Ph):{
			string = "parameter_Fref_Ph";
			break;
		}
		case (parameter_cap_Ph):{
			string = "parameter_cap_Ph";
			break;
		}
		case (parameter_A_dm2):{
			string = "parameter_A_dm2";
			break;
		}
		case (parameter_CaAdr):{
			string = "parameter_CaAdr";
			break;
		}
		case (parameter_CreaAdr):{
			string = "parameter_CreaAdr";
			break;
		}
		case (parameter_HCO3Adr):{
			string = "parameter_HCO3Adr";
			break;
		}
		case (parameter_wgtNa):{
			string = "parameter_wgtNa";
			break;
		}
		case (parameter_wgtK):{
			string = "parameter_wgtK";
			break;
		}
		case (parameter_wgtCa):{
			string = "parameter_wgtCa";
			break;
		}
		case (parameter_wgtUr):{
			string = "parameter_wgtUr";
			break;
		}
		case (parameter_wgtCrea):{
			string = "parameter_wgtCrea";
			break;
		}
		case (parameter_wgtPhos):{
			string = "parameter_wgtPhos";
			break;
		}
		case (parameter_wgtHCO3):{
			string = "parameter_wgtHCO3";
			break;
		}
		case (parameter_mspT):{
			string = "parameter_mspT";
			break;
		}
	}
	return(string);
}

void printfWhoId(tdWhoId whoId)
{
                #if defined VP_SIMULATION
	printf(whoIdToString(whoId));
        #endif
}

char *dataIdToString(tdDataId dataId)
{
	char *string = "UNKNOWN dataId";
	switch (dataId) {
	case (dataID_undefined):{
		string = "dataID_undefined";
		break;
	}
		case (dataID_reset):{
			string = "dataID_reset";
			break;
		}
		case (dataID_shutdown):{
			string = "dataID_shutdown";
			break;
		}
		case (dataID_setTimeDate):{
			string = "dataID_setTimeDate";
			break;
		}
		case (dataID_currentWAKDstateIs):{
			string = "dataID_currentWAKDstateIs";
			break;
		}
		case (dataID_ack):{
			string = "dataID_ack";
			break;
		}
		case (dataID_nack):{
			string = "dataID_nack";
			break;
		}
		case (dataID_statusRequest):{
			string = "dataID_statusRequest";
			break;
		}
		case (dataID_configureState):{
			string = "dataID_configureState";
			break;
		}
		case (dataID_configureStateDump):{
			string = "dataID_configureStateDump";
			break;
		}
		case (dataID_configurePeriodPolarizerToggle):{
			string = "dataID_configurePeriodPolarizerToggle";
			break;
		}
		case (dataID_changeWAKDStateFromTo):{
			string = "dataID_changeWAKDStateFromTo";
			break;
		}
		case (dataID_physiologicalData):{
			string = "dataID_physiologicalData";
			break;
		}
		case (dataID_physicalData):{
			string = "dataID_physicalData";
			break;
		}
		case (dataID_actuatorData):{
			string = "dataID_actuatorData";
			break;
		}
		case (dataID_alarmRTB):{
			string = "dataID_alarmRTB";
			break;
		}
		case (dataID_statusRTB):{
			string = "dataID_statusRTB";
			break;
		}
		case (dataID_weightRequest):{
			string = "dataID_weightRequest";
			break;
		}
		case (dataID_weightData):{
			string = "dataID_weightData";
			break;
		}
		case (dataID_weightDataOK):{
			string = "dataID_weightDataOK";
			break;
		}
		case (dataID_bpRequest):{
			string = "dataID_bpRequest";
			break;
		}
		case (dataID_bpData):{
			string = "dataID_bpData";
			break;
		}
		case (dataID_EcgRequest):{
			string = "dataID_EcgRequest";
			break;
		}
		case (dataID_EcgData):{
			string = "dataID_EcgData";
			break;
		}
		case (dataID_PatientProfile):{
			string = "dataID_PatientProfile";
			break;
		}
		case (dataID_WAKDAllStateConfigure):{
			string = "dataID_WAKDAllStateConfigure";
			break;
		}
		case (dataID_AllStatesParametersSCC):{
			string = "dataID_AllStatesParametersSCC";
			break;
		}	
		case (dataID_StateParametersSCC):{
			string = "dataID_StateParametersSCC";
			break;
		}
		case (dataID_systemInfo):{
			string = "dataID_systemInfo";
			break;
		}
		case (dataID_physiologicalData_K):{
			string = "dataID_physiologicalData_K";
			break;
		}
		case (dataID_physiologicalData_Na):{
			string = "dataID_physiologicalData_Na";
			break;
		}
		case (dataID_physiologicalData_pH):{
			string = "dataID_physiologicalData_pH";
			break;
		}
		case (dataID_physiologicalData_Ur):{
			string = "dataID_physiologicalData_Ur";
			break;
		}
		case (dataID_ConfigureParameters):{
			string = "dataID_ConfigureParameters";
			break;
		}
		case (dataID_CurrentParameters):{
			string = "dataID_CurrentParameters";
			break;
		}
		case (dataID_ActualParameters):{
			string = "dataID_ActualParameters";
			break;
		}				
	}
	return(string);
}

char *errmsgToString(tdErrMsg erroMsg)
{
	char *string = "UNKNOWN errmsg";
	switch (erroMsg) {
		case (errmsg_TimeoutOnDispQueue):{
			string = "errmsg_TimeoutOnDispQueue";
			break;
		}
		case (errmsg_TimeoutOnDsQueue):{
			string = "errmsg_TimeoutOnDsQueue";
			break;
		}
		case (errmsg_TimeoutOnFcQueue):{
			string = "errmsg_TimeoutOnFcQueue";
			break;
		}
		case (errmsg_TimeoutOnRtbpaQueue):{
			string = "errmsg_TimeoutOnRtbpaQueue";
			break;
		}
		case (errmsg_TimeoutOnCbpaQueue):{
			string = "errmsg_TimeoutOnCbpaQueue";
			break;
		}
		case (errmsg_TimeoutOnSccQueue):{
			string = "errmsg_TimeoutOnSccQueue";
			break;
		}
		case (errmsg_TimeoutOnUiQueue):{
			string = "errmsg_TimeoutOnUiQueue";
			break;
		}
		case (errmsg_QueueOfDispatcherTaskFull):{
			string = "errmsg_QueueOfDispatcherTaskFull";
			break;
		}
		case (errmsg_QueueOfDataStorageTaskFull):{
			string = "errmsg_QueueOfDataStorageTaskFull";
			break;
		}
		case (errmsg_QueueOfSystemCheckCalibrationTaskFull):{
			string = "errmsg_QueueOfSystemCheckCalibraionTaskFull";
			break;
		}
		case (errmsg_QueueOfCommunicationBoardAbstractionTaskFull):{
			string = "errmsg_QueueOfCommunicationBoardAbstractionTaskFull";
			break;
		}
		case (errmsg_QueueOfRealtimeBoardAbstractionTaskFull):{
			string = "errmsg_QueueOfRealtimeBoardAbstractionTaskFull";
			break;
		}
		case (errmsg_QueueOfFlowControlTaskFull):{
			string = "errmsg_QueueOfFlowControlTaskFull";
			break;
		}
		case (errmsg_QueueOfReminderTaskFull):{
			string = "errmsg_QueueOfReminderTaskFull";
			break;
		}
		case (errmsg_QueueOfUserInterfaceTaskFull):{
			string = "errmsg_QueueOfUserInterfaceTaskFull";
			break;
		}
		case (errmsg_nackFromCB):{
			string = "errmsg_nackFromCB";
			break;
		}
		case (errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask):{
			string = "errmsg_IsrRtbCouldNotPutTokenIntoQueueOfDispatcherTask";
			break;
		}
		case (errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask):{
			string = "errmsg_IsrCbCouldNotPutTokenIntoQueueOfDispatcherTask";
			break;
		}
		case (errmsg_IsrMbButtonEventLost):{
			string = "errmsg_IsrMbButtonEventLost";
			break;
		}
		case (errmsg_DispDataWeightSensorReadEventLost):{
			string = "errmsg_DispDataWeightSensorReadEventLost";
			break;
		}
		case (errmsg_DispDataPatientProfielWriteEventLost):{
			string = "errmsg_DispDataPatientProfielWriteEventLost";
			break;
		}
		case (errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask):{
			string = "errmsg_DispCouldNotPutTokenIntoQueueOfDataStorageTask";
			break;
		}
		case (errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask):{
			string = "errmsg_DispCouldNotPutTokenIntoQueueOfSystemCheckCalibrationTask";
			break;
		}
		case (errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask):{
			string = "errmsg_DispCouldNotPutTokenIntoQueueOfFlowControlTask";
			break;
		}
		case (errmsg_DispChangeWAKDStateFromToEventLost):{
			string = "errmsg_DispChangeWAKDStateFromToEventLost";
			break;
		}		
		case (errmsg_pvPortMallocFailed):{
			string = "errmsg_pvPortMallocFailed";
			break;
		}		
		case (errmsg_getDataFailed):{
			string = "errmsg_getDataFailed";
			break;
		}		
		case (errmsg_putDataFailed):{
			string = "errmsg_putDataFailed";
			break;
		}		
		case (errmsg_ConfiguringWakdStateFailed):{
			string = "errmsg_ConfiguringWakdStateFailed";
			break;
		}		
		case (errmsg_ErrFlowControl):{
			string = "errmsg_ErrFlowControl";
			break;
		}		
		case (errmsg_ErrSCC):{
			string = "errmsg_ErrSCC";
			break;
		}				
		case (errmsg_ErrSendNack):{
			string = "errmsg_ErrSendNack";
			break;
		}		
		case (errmsg_ErrSendAck):{
			string = "errmsg_ErrSendAck";
			break;
		}		
		case (errmsg_command_WakeupCallEventLost):{
			string = "errmsg_command_WakeupCallEventLost";
			break;
		}		
		case (errmsg_CommunicationBoardUnreachable):{
			string = "errmsg_CommunicationBoardUnreachable";
			break;
		}
		case (errmsg_SmartphoneUnreachable):{
			string = "errmsg_SmartphoneUnreachable";
			break;
		}
		case (errmsg_NotInListForAckOnMsgCBPA):{
			string = "errmsg_NotInListForAckOnMsgCBPA";
			break;
		}		
		case (errmsg_PutDataFunctionFailed):{
			string = "errmsg_PutDataFunctionFailed";
			break;
		}		
		case (errmsg_UnknownRecipient):{
			string = "errmsg_UnknownRecipient";
			break;
		}		
		case (errmsg_SDcardError):{
			string = "errmsg_SDcardError";
			break;
		}		
		case (errmsg_FlowControlStateIsUnknown):{
			string = "errmsg_FlowControlStateIsUnknown";
			break;
		}		
		case (errmsg_ConfiguredStateTimesMismatch):{
			string = "errmsg_ConfiguredStateTimesMismatch";
			break;
		}
		case (errmsg_IsrCbReceivedFaultySlipMsg):{
			string = "errmsg_IsrCbReceivedFaultySlipMsg";
			break;
		}
		case (errmsg_ReadingSDcardFailed):{
			string = "errmsg_ReadingSDcardFailed";
			break;
		}
	}
	return(string);
}


char *infoMsgToString(tdInfoMsg enuMsg)
{
	char *string = "UNKNOWN errmsg";
	switch (enuMsg) {
		case (dectreeMsg_detectedSorDys):{
			string = "dectreeMsg_detectedSorDys";
			break;
		}
		case (decTreeMsg_detectedBPsysAbsL):{
			string = "decTreeMsg_detectedBPsysAbsL";
			break;
		}
		case (decTreeMsg_detectedBPsysAbsH):{
			string = "decTreeMsg_detectedBPsysAbsH";
			break;
		}
		case (decTreeMsg_detectedBPsysAAbsH):{
			string = "decTreeMsg_detectedBPsysAAbsH";
			break;
		}
		case (decTreeMsg_detectedBPdiaAbsL):{
			string = "decTreeMsg_detectedBPdiaAbsL";
			break;
		}
		case (decTreeMsg_detectedBPdiaAbsH):{
			string = "decTreeMsg_detectedBPdiaAbsH";
			break;
		}
		case (decTreeMsg_detectedBPdiaAAbsH):{
			string = "decTreeMsg_detectedBPdiaAAbsH";
			break;
		}
		case (dectreeMsg_detectedWghtAbsL):{
			string = "dectreeMsg_detectedWghtAbsL";
			break;
		}
		case (dectreeMsg_detectedWghtAbsH):{
			string = "dectreeMsg_detectedWghtAbsH";
			break;
		}
		case (dectreeMsg_detectedWghtTrdT):{
			string = "dectreeMsg_detectedWghtTrdT";
			break;
		}
		case (dectreeMsg_detectedpumpBaccDevi):{
			string = "dectreeMsg_detectedpumpBaccDevi";
			break;
		}
		case (dectreeMsg_detectedpumpFaccDevi):{
			string = "dectreeMsg_detectedpumpFaccDevi";
			break;
		}
		case (dectreeMsg_detectedBPorFPhigh):{
			string = "dectreeMsg_detectedBPorFPhigh";
			break;
		}
		case (dectreeMsg_detectedBPorFPlow):{
			string = "dectreeMsg_detectedBPorFPlow";
			break;
		}
		case (dectreeMsg_detectedBTSoTH):{
			string = "dectreeMsg_detectedBTSoTH";
			break;
		}
		case (dectreeMsg_detectedBTSoTL):{
			string = "dectreeMsg_detectedBTSoTL";
			break;
		}
		case (dectreeMsg_detectedBTSiTH):{
			string = "dectreeMsg_detectedBTSiTH";
			break;
		}
		case (dectreeMsg_detectedBTSiTL):{
			string = "dectreeMsg_detectedBTSiTL";
			break;
		}
		case (dectreeMsg_detectedPhAAbsL):{
			string = "dectreeMsg_detectedPhAAbsL";
			break;
		}
		case (dectreeMsg_detectedPhAAbsH):{
			string = "dectreeMsg_detectedPhAAbsH";
			break;
		}
		case (dectreeMsg_detectedPhAbsLorPhAbsH):{
			string = "dectreeMsg_detectedPhAbsLorPhAbsH";
			break;
		}
		case (dectreeMsg_detectedPhTrdLPsT):{
			string = "dectreeMsg_detectedPhTrdLPsT";
			break;
		}
		case (dectreeMsg_detectedPhTrdHPsT):{
			string = "dectreeMsg_detectedPhTrdHPsT";
			break;
		}
		case (dectreeMsg_detectedNaAbsLorNaAbsH):{
			string = "dectreeMsg_detectedNaAbsLorNaAbsH";
			break;
		}
		case (dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT):{
			string = "dectreeMsg_detectedNaTrdLPsTorNaTrdHPsT";
			break;
		}
		case (dectreeMsg_detectedKAbsLorKAbsHandSorDys):{
			string = "dectreeMsg_detectedKAbsLorKAbsHandSorDys";
			break;
		}
		case (dectreeMsg_detectedKAAL):{
			string = "dectreeMsg_detectedKAAL";
			break;
		}
		case (dectreeMsg_detectedKAAHandKincrease):{
			string = "dectreeMsg_detectedKAAHandKincrease";
			break;
		}
		case (dectreeMsg_detectedKAAHnoKincrease):{
			string = "dectreeMsg_detectedKAAHnoKincrease";
			break;
		}
		case (dectreeMsg_detectedKTrdLPsT):{
			string = "dectreeMsg_detectedKTrdLPsT";
			break;
		}
		case (dectreeMsg_detectedKTrdHPsT):{
			string = "dectreeMsg_detectedKTrdHPsT";
			break;
		}
		case (dectreeMsg_detectedKTrdofnriandSorDys):{
			string = "dectreeMsg_detectedKTrdofnriandSorDys";
			break;
		}
		case (dectreeMsg_detectedPhTrdofnriandSorDys):{
			string = "dectreeMsg_detectedPhTrdofnriandSorDys";
			break;
		}
		case (dectreeMsg_detectedUreaAAbsHandSorDys):{
			string = "dectreeMsg_detectedUreaAAbsHandSorDys";
			break;
		}
		case (dectreeMsg_detectedUreaAAbsHnoSorDys):{
			string = "dectreeMsg_detectedUreaAAbsHnoSorDys";
			break;
		}
		case (dectreeMsg_detectedUreaTrdHPsT):{
			string = "dectreeMsg_detectedUreaTrdHPsT";
			break;
		}
		case (dectreeMsg_detectedKAbsLorKAbsHnoSorDys):{
			string = "dectreeMsg_detectedKAbsLorKAbsHnoSorDys";
			break;
		}
		case (flowControlMsg_excretionBagNotConnected):{
			string = "flowControlMsg_excretionBagNotConnected";
			break;
		}
	}
	return(string);
}

void printfDataId(tdDataId dataId)
{
                #if defined VP_SIMULATION
	printf(dataIdToString(dataId));
        #endif
}
