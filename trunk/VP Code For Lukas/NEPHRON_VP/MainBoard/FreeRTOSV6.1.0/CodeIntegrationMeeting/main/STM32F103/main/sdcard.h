// -----------------------------------------------------------------------------------
// Copyright (C) 2010          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   sdcard.h
//! \brief  Implements SDCARD Macro Functions
//!
//! ....
//!
//! \author  Dudnik G-S
//! \date    26.03.2010
//! \version 1.0
// -----------------------------------------------------------------------------------
#ifndef __SDCARD_H_
#define __SDCARD_H_
// -----------------------------------------------------------------------------------
// Include
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
// Exported constant & macro definitions
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
// testing SD

extern void SD_CARD_TEST(void);

// SD CARD
extern void Update_SDCARD_Presence(void);
extern void SD_libInit ();
extern void SD_OpenReadClose_Block (char *, uint8_t *, uint16_t);
extern void SD_OpenWriteClose_Block (char *, uint8_t *, uint16_t);
extern void SD_OpenAppendClose_Block (char *, uint8_t *, uint16_t);
#if 0
extern int8_t SD_OpenFile (char *, EmbeddedFile, uint8_t);
extern uint16_t SD_WriteFile (EmbeddedFile, uint8_t *, uint16_t);
extern uint16_t SD_ReadFile (EmbeddedFile, uint8_t *, uint16_t);
extern void SD_CloseFile (EmbeddedFile);
#endif
extern void ComposeFileName ( uint8_t*,  uint32_t );
extern int8_t SD_CopyFiles_Block (char* , char* , uint8_t );

extern uint16_t SDCARD_WRITE_TEST(uint16_t filescounter);
extern uint32_t SDCARD_READ_TEST(uint16_t filescounter);
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE __SDCARD_H_
// -----------------------------------------------------------------------------------
