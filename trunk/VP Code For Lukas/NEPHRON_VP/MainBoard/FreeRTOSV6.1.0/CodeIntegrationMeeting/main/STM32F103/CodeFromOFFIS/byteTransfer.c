/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "nephron.h"
#include "byteTransfer.h"

#define PREFIX "Function byteTransfer"

// The function creates an array of char (byte array) and sorts the structure
// of the passed pointer into this buffer of bytes according to the network byte order standard.
// The pointer returned is to this buffer.
uint8_t htonMsg(uint8_t *buffer, void *msg)
{
	uint8_t err = 0;
	tdMsgOnly *pMsgOnly = (tdMsgOnly *) msg;
	tdDataId dataId = pMsgOnly->header.dataId;

	// transcopy the header information. This has to be done every time
	// for some msg we need to copy more than the header. That is taken
	// care for in the succeeding case.
	buffer[0]   = pMsgOnly->header.recipientId;
//	buffer[1]   = unfortunately the internal sizes of structures are larger than the network structure due to alignment
//	buffer[2]   = and padding. So we have to define this manually in a switch below!
	buffer[3]   = pMsgOnly->header.dataId;
	buffer[4]   = pMsgOnly->header.msgCount;
	buffer[5]   = pMsgOnly->header.issuedBy;
	switch (dataId){
		case dataID_currentWAKDstateIs:{
			buffer[1]	= 0;	// Size of dataID_currentWAKDstateIs is 8
			buffer[2]	= 8;
			tdMsgCurrentWAKDstateIs *pCurrentWAKDstateIs = (tdMsgCurrentWAKDstateIs *) msg;
			buffer[6]   = *(((uint8_t *)(&(pCurrentWAKDstateIs->wakdStateIs))));
			buffer[7]   = *(((uint8_t *)(&(pCurrentWAKDstateIs->wakdOpStateIs))));
			break;
		}
		case (dataID_shutdown):{
			buffer[1]	= 0;	// Size of dataID_reset is 6
			buffer[2]	= 6;
			break;
		}
		case (dataID_reset):{
			buffer[1]	= 0;	// Size of dataID_reset is 6
			buffer[2]	= 6;
			break;
		}
		case (dataID_ack):{
			buffer[1]	= 0;	// Size of dataID_ack is 6
			buffer[2]	= 6;
			break;
		}
		case (dataID_nack):{
			buffer[1]	= 0;	// Size of dataID_ack is 6
			buffer[2]	= 6;
			break;
		}
		case (dataID_weightRequest):{
			buffer[1]	= 0;	// Size of dataID_nack is 6
			buffer[2]	= 6;
			break;
		}
		case (dataID_systemInfo):{
			buffer[1]	= 0;	// Size of dataID_systemInfo is 8
			buffer[2]	= 8;
			tdMsgSystemInfo *pMsgSystemInfo = (tdMsgSystemInfo *) msg;
			buffer[6]   = *(((uint8_t *)(&(pMsgSystemInfo->msgEnum)))+1);						// swap bytes for uint16_t
			buffer[7]   = *(((uint8_t *)(&(pMsgSystemInfo->msgEnum))));						// swap bytes for uint16_t
			break;
		}
		case (dataID_weightData):{
			buffer[1]	= 0;	// Size of dataID_weightData is 15
			buffer[2]	= 19;
			tdMsgWeightData *pMsgWeightData = (tdMsgWeightData *) msg;
			buffer[6]   = *(((uint8_t *)(&(pMsgWeightData->weightData.Weight)))+1);				// swap bytes for uint16_t
			buffer[7]   = *(((uint8_t *)(&(pMsgWeightData->weightData.Weight))));				// swap bytes for uint16_t
			buffer[8]   = *(((uint8_t *)(&(pMsgWeightData->weightData.BodyFat))));
			buffer[9]   = *(((uint8_t *)(&(pMsgWeightData->weightData.WSBatteryLevel)))+1);		// swap bytes for uint16_t
			buffer[10]  = *(((uint8_t *)(&(pMsgWeightData->weightData.WSBatteryLevel))));		// swap bytes for uint16_t
			buffer[11]  = *(((uint8_t *)(&(pMsgWeightData->weightData.WSStatus)))+3);			// swap bytes for uint32_t
			buffer[12]  = *(((uint8_t *)(&(pMsgWeightData->weightData.WSStatus)))+2);			// swap bytes for uint32_t
			buffer[13]  = *(((uint8_t *)(&(pMsgWeightData->weightData.WSStatus)))+1);			// swap bytes for uint32_t
			buffer[14]  = *(((uint8_t *)(&(pMsgWeightData->weightData.WSStatus))));				// swap bytes for uint32_t
			buffer[15]  = *(((uint8_t *)(&(pMsgWeightData->weightData.TimeStamp)))+3);			// swap bytes for uint32_t
			buffer[16]  = *(((uint8_t *)(&(pMsgWeightData->weightData.TimeStamp)))+2);			// swap bytes for uint32_t
			buffer[17]  = *(((uint8_t *)(&(pMsgWeightData->weightData.TimeStamp)))+1);			// swap bytes for uint32_t
			buffer[18]  = *(((uint8_t *)(&(pMsgWeightData->weightData.TimeStamp))));				// swap bytes for uint32_t
			break;
		}
		case (dataID_ActualParameters):{
			// This data is of variable size depending on the parameters that are included
			// have a look at the size information. Due to alignment and padding two byte
			// of size have to be removed.
			uint8_t *pData = (uint8_t *) msg;
			uint16_t msgSize =  (*(pData+3)<<8)+(*(pData+2));
			buffer[1]	= *((uint8_t*)(&msgSize)+1);
			buffer[2]	= *((uint8_t*)(&msgSize)+0);
			// Copy the remainder of the message
			int i = 0;
			for (i = 6; i < msgSize; i++)
			{
				buffer[i] = *(pData+i+2);
			}
			break;
		}
		case (dataID_physiologicalData):{
			buffer[1]	= 0;	// Size of dataID_physiologicalData is ??
			buffer[2]	= 70;
			tdMsgPhysiologicalData *pMsgPhysiologicalData = (tdMsgPhysiologicalData *) msg;
			// TimeStamp
			buffer[6]   = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.TimeStamp)))+3);						// unint32_t
			buffer[7]   = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.TimeStamp)))+2);						// unint32_t
			buffer[8]   = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.TimeStamp)))+1);						// unint32_t
			buffer[9]   = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.TimeStamp))));							// unint32_t
			// ECPDataI
			buffer[10]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium)))+1);					// uint16_t
			buffer[11]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Sodium))));					// uint16_t
			buffer[12]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Sodium)))+3);		// unint32_t
			buffer[13]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Sodium)))+2);		// unint32_t
			buffer[14]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Sodium)))+1);		// unint32_t
			buffer[15]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Sodium))));			// unint32_t
			buffer[16]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium)))+1);				// uint16_t
			buffer[17]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Potassium))));				// uint16_t
			buffer[18]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Potassium)))+3);	// unint32_t
			buffer[19]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Potassium)))+2);	// unint32_t
			buffer[20]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Potassium)))+1);	// unint32_t
			buffer[21]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Potassium))));		// unint32_t
			buffer[22]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.pH)))+1);						// uint16_t
			buffer[23]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.pH))));						// uint16_t
			buffer[24]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_pH)))+3);			// unint32_t
			buffer[25]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_pH)))+2);			// unint32_t
			buffer[26]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_pH)))+1);			// unint32_t
			buffer[27]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_pH))));				// unint32_t
			buffer[28]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea)))+1);					// uint16_t
			buffer[29]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Urea))));						// uint16_t
			buffer[30]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Urea)))+3);			// unint32_t
			buffer[31]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Urea)))+2);			// unint32_t
			buffer[32]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Urea)))+1);			// unint32_t
			buffer[33]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Urea))));			// unint32_t
			buffer[34]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Temperature)))+1);			// uint16_t
			buffer[35]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Temperature))));				// uint16_t
			buffer[36]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Temperature)))+3);	// unint32_t
			buffer[37]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Temperature)))+2);	// unint32_t
			buffer[38]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Temperature)))+1);	// unint32_t
			buffer[39]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Temperature))));	// unint32_t
//			buffer[40]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.Status))));					// uint8_t
//			buffer[41]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Status)))+3);		// unint32_t
//			buffer[42]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Status)))+2);		// unint32_t
//			buffer[43]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Status)))+1);		// unint32_t
//			buffer[44]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataI.msOffset_Status))));			// unint32_t
			// ECPDataO
			buffer[40]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Sodium)))+1);					// uint16_t
			buffer[41]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Sodium))));					// uint16_t
			buffer[42]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Sodium)))+3);		// unint32_t
			buffer[43]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Sodium)))+2);		// unint32_t
			buffer[44]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Sodium)))+1);		// unint32_t
			buffer[45]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Sodium))));			// unint32_t
			buffer[46]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium)))+1);				// uint16_t
			buffer[47]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Potassium))));				// uint16_t
			buffer[48]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Potassium)))+3);	// unint32_t
			buffer[49]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Potassium)))+2);	// unint32_t
			buffer[50]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Potassium)))+1);	// unint32_t
			buffer[51]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Potassium))));		// unint32_t
			buffer[52]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.pH)))+1);						// uint16_t
			buffer[53]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.pH))));						// uint16_t
			buffer[54]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_pH)))+3);			// unint32_t
			buffer[55]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_pH)))+2);			// unint32_t
			buffer[56]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_pH)))+1);			// unint32_t
			buffer[57]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_pH))));				// unint32_t
			buffer[58]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Urea)))+1);					// uint16_t
			buffer[59]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Urea))));						// uint16_t
			buffer[60]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Urea)))+3);			// unint32_t
			buffer[61]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Urea)))+2);			// unint32_t
			buffer[62]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Urea)))+1);			// unint32_t
			buffer[63]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Urea))));			// unint32_t
			buffer[64]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Temperature)))+1);			// uint16_t
			buffer[65]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Temperature))));				// uint16_t
			buffer[66]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Temperature)))+3);	// unint32_t
			buffer[67]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Temperature)))+2);	// unint32_t
			buffer[68]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Temperature)))+1);	// unint32_t
			buffer[69]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Temperature))));	// unint32_t
//			buffer[75]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.Status))));					// uint8_t
//			buffer[76]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Status)))+3);		// unint32_t
//			buffer[77]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Status)))+2);		// unint32_t
//			buffer[78]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Status)))+1);		// unint32_t
//			buffer[79]  = *(((uint8_t *)(&(pMsgPhysiologicalData->physiologicalData.ECPDataO.msOffset_Status))));			// unint32_t
			break;
		}
		default:{
			defaultIdHandling(PREFIX, dataId);
			err = 1;
			break;
		}
	}
	return(err);
}

// The function receives a pointer to buffer in the network byte order standard.
// It is assumed, that the first 6 Bytes always create a valid header as defined as follows (e.g.):
// Byte 0: 		0x01,          // header.recipientId = MB
// Byte 1-2:	0x00, 0x0a,    // header.msgSize = 10 bytes
// Byte 3:		0x08,          // header.dataId = dataID_changeWAKDStateFromTo
// Byte 4:		0x01,          // header.msgCount
// Byte 5:		0x04,          // header.whoId = SP
uint8_t ntohMsg(void *msg, uint8_t *buffer)
{
	uint8_t *pMsg = (uint8_t *) msg;
	uint8_t err = 0;
	tdDataId dataId = buffer[3];

	// transcopy the header information. This has to be done every time
	// for some msg we need to copy more than the header. That is taken
	// care for in the succeeding case.
	pMsg[offsetof(tdMsgWakdStateFromTo, header.recipientId)]	= buffer[0];
	pMsg[offsetof(tdMsgWakdStateFromTo, header.msgSize)]		= buffer[2];	// swap bytes for uint16_t
	pMsg[offsetof(tdMsgWakdStateFromTo, header.msgSize)+1]		= buffer[1];	// swap bytes for uint16_t
	pMsg[offsetof(tdMsgWakdStateFromTo, header.dataId)] 		= buffer[3];
	pMsg[offsetof(tdMsgWakdStateFromTo, header.msgCount)] 		= buffer[4];
	pMsg[offsetof(tdMsgWakdStateFromTo, header.issuedBy)] 		= buffer[5];

	switch(dataId){
		case (dataID_changeWAKDStateFromTo):{
			pMsg[offsetof(tdMsgWakdStateFromTo, fromWakdState)] 			= buffer[6];
			pMsg[offsetof(tdMsgWakdStateFromTo, fromWakdOpState)] 			= buffer[7];
			pMsg[offsetof(tdMsgWakdStateFromTo, toWakdState)] 				= buffer[8];
			pMsg[offsetof(tdMsgWakdStateFromTo, toWakdOpState)] 			= buffer[9];
			break;
		}
		case (dataID_ack):{
			// Do nothing. Everything is in the header.
			break;
		}
		case (dataID_weightRequest):{
			// Do nothing. Everything is in the header.
			break;
		}
		case (dataID_shutdown):{
			// Do nothing. Everything is in the header.
			break;
		}
		case (dataID_statusRequest):{
			// Do nothing. Everything is in the header.
			break;
		}
		case (dataID_weightDataOK):{
			// Receive the weight scale data after it was validated by the patient.
			pMsg[offsetof(tdMsgWeightData, weightData.Weight)+1]			= buffer[6];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.Weight)] 				= buffer[7];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.BodyFat)] 			= buffer[8];
			pMsg[offsetof(tdMsgWeightData, weightData.WSBatteryLevel)+1]	= buffer[9];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSBatteryLevel)] 		= buffer[10];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+3] 			= buffer[11];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+2] 			= buffer[12];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+1] 			= buffer[13];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)] 			= buffer[14];	// swap bytes for uint32_t
			break;
		}
		case (dataID_weightData):{
			// get the weight, body fat, battery level and status of scale.
			pMsg[offsetof(tdMsgWeightData, weightData.Weight)+1]			= buffer[6];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.Weight)] 				= buffer[7];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.BodyFat)] 			= buffer[8];
			pMsg[offsetof(tdMsgWeightData, weightData.WSBatteryLevel)+1]	= buffer[9];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSBatteryLevel)] 		= buffer[10];	// swap bytes for uint16_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+3] 			= buffer[11];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+2] 			= buffer[12];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)+1] 			= buffer[13];	// swap bytes for uint32_t
			pMsg[offsetof(tdMsgWeightData, weightData.WSStatus)] 			= buffer[14];	// swap bytes for uint32_t
			break;
		}
		case (dataID_CurrentParameters):		// fall through: do the same as for "dataID_ConfigureParameters"
		case (dataID_ConfigureParameters):{
			// First Bytes of header are already there. Begin after msg header
			uint16_t i = 0;
			// Unfortunately the internal sizes of structures are larger than the network structure
			// due to alignment and padding. So we have to recompute this manually.
			for (i = sizeof(tdMsgOnly); i<((buffer[1]<<8)+buffer[2]+(sizeof(tdMsgOnly)-6)); i++)
			{
//				#if defined SEQ12
//					taskMessage("I", "FUNC_ntohMsg", "Assigning Buff %d: %d", i, buffer[i-(sizeof(tdMsgOnly)-6)]);
//				#endif
				pMsg[i] = buffer[i-(sizeof(tdMsgOnly)-6)];
			}
			break;
		}
		default:{
			defaultIdHandling(PREFIX, dataId);
			err = 1;
			break;
		}
	}
	return(err);
}


//	target+00 = buffer+
//
//
//
//	target->header.msgSize = swapBytes(      *((uint16_t *)(buffer+1))      );
//	return((void *) target);

//	uint8_t		recipientId;	// Byte 0 of all messages
//	uint16_t    msgSize;		// Byte 1 and 2 size
//	uint8_t 	dataId;         // Byte 3
//	uint8_t		msgCount;		// Byte 4: rising number (NOT '0') of messages sent
//	uint8_t		issuedBy;		// Byte 5: instance that issued the id and waits for an Ack/Nack.
//
//	uint8_t		fromWakdState;
//		uint8_t 	fromWakdOpState;
//		uint8_t 	toWakdState;
//		uint8_t		toWakdOpState;



//uint16_t swapBytes(uint16_t twoBytes)
//{
//	char *byte0, *byte1;
//	byte0 = (char *) &twoBytes;
//	byte1 = (byte0+1);
//
//	char swap[2];
//	swap[0] = *byte1;
//	swap[1] = *byte0;
//
//	return(*((uint16_t *)swap));
//}
