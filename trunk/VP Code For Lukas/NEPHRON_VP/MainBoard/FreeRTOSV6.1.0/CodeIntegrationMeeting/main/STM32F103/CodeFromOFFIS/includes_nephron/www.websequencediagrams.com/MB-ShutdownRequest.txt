participant SP
participant CB
participant "MB ISR"

alt Power Down via Smart Phone
	note over SP
		User operates menu on SP
		to select power down in SP
		menu  tree.
	end note
	SP -> CB: dataID_shutdown
	CB -> MB: bytewise forward
	MB -> CB: dataID_ack
	CB -> SP: bytewise forward
	note over SP
		SP informs User:
		"Press OK-Button on WAKD device!"
	end note
else Power Down NOT via Smart Phone
	alt Power Down via WAKD Power Button
		note over "MB ISR"
			The WAKD Power Button is
			pushed shortly. (NOT hold
			for several seconds!)
		end note
		MB ISR -> MB: dataID_shutdown
	else Power Down via WAKD LCD Menu
		note over MB
			User operates buttons on WAKD
			to select power down in WAKD
			menu tree.
		end note
	end
	MB -> CB: dataID_shutdown
	CB -> SP: bytewise forward
	SP -> CB: dataID_ack
	CB -> MB: bytewise forward
	note over SP
		Showing message like:
		"System shutdown!
		Look at WAKD UI!!"
	end note
end
	
	
	
	
note over MB
	WAKD LCD shows message:
	"Are you sure to turn off?"
	OK, "Really?" OK, "Really really?"
	OK
end note

MB -> RTB: dataID_shutdown
RTB -> MB: dataID_ack
note over RTB
	RTB will be in state "AllStopped"
	RTB is in a save state and
	has turned itself down to
	minimal activity (maybe even
	powerless?)
end note

MB -> PMB1: dataID_shutdown
PMB1 -> MB: dataID_ack
MB -> PMB2: dataID_shutdown
PMB2 -> MB: dataID_ack

note over MB
	WAKD LCD shows message:
	"System is down! Hold Pwr button for
	X seconds to turn power off!"
end note
