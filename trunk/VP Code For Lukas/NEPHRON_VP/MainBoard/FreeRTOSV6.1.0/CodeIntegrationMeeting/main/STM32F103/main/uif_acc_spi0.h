// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   uif_acc_spi0.h
//! \brief  spi0 for accelerometer (attitude measurement) and UIF 
//!
//! spi0 initialization, send & receive data routines
//!
//! \author  Dudnik G.S.
//! \date    08.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
#ifndef UIF_ACC_SPI0_H_
#define UIF_ACC_SPI0_H_
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "global.h"
// -----------------------------------------------------------------------------------
// Exported constants & macros
// -----------------------------------------------------------------------------------
#define DEV_UIF       1
#define DEV_ACC       2

#define SPI_TIMEOUT   100// 10000
// -----------------------------------------------------------------------------------
// Exported functions
// -----------------------------------------------------------------------------------
extern void UIF_ACC_SPIConfig(void);
extern void UIF_SendData(uint8_t mode, uint8_t _data);
extern uint8_t UIF_ReceiveData(uint8_t mode);
extern void UIF_SendHWSW(uint8_t mode, uint8_t _dataIN);
extern uint8_t UIF_ReceiveHWSW(uint8_t mode, uint8_t _dataIN);
extern uint8_t UIF_SendReceiveHWSW(uint8_t mode, uint8_t _dataIN);
extern uint8_t UIF_ANSWER_8BYTES(uint8_t nbytes);
extern uint8_t UIF_ACC_SendReceive(uint8_t whichres, uint8_t _dataIN);
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
#endif /* UIF_ACC_SPI0_H_ */
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2012 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
