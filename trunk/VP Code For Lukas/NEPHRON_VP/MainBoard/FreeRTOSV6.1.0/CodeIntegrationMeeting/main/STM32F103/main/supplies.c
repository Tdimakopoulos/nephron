// -----------------------------------------------------------------------------------
// Copyright (C) 2011          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   supplies.c
//! \brief  Power Supplies Control
//!
//! routines for switching ON/OFF supplies 
//!
//! \author  Dudnik G.S.
//! \date    10.07.2011
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"
// -----------------------------------------------------------------------------------
//! \brief  SUPPLIES_SWITCH_ALL_ON
//!
//! Switches ALL supplies ON but progressively
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
void SUPPLIES_SWITCH_ALL_ON(void)
{
  // ------------------------------------------------------------
  // A. [COMMUNICATION BOARD POWER SUPPLY]
  // ------------------------------------------------------------
  // Delay(500);
  DelayBySoft (50000);
  STM32F_GPIOOn(WCM_PWR_EN_GPIO_PORT, WCM_PWR_EN_GPIO_PIN);        
  // ------------------------------------------------------------
  // B [MAINTENANCE PORT - USB/FTDI SUPPLIES, OE, RESET]
  // ------------------------------------------------------------
  // POWER SUPPLY
  STM32F_GPIOOn(USB_PWR_EN_GPIO_PORT, USB_PWR_EN_GPIO_PIN);   
  // Delay(500);
  DelayBySoft (50000);
  // FTDI NO RESET (BEFORE ENABLE OUTPUT)
  STM32F_GPIOOn(FTDI_NRESET_GPIO_PORT, FTDI_NRESET_GPIO_PIN);        
  DelayBySoft (1000);
  // ISOLATOR OUTPUT ENABLE
  STM32F_GPIOOn(PC_OUTEN_GPIO_PORT, PC_OUTEN_GPIO_PIN);              
  DelayBySoft (1000);
  // RESET FTDI
  STM32F_GPIOOff(FTDI_NRESET_GPIO_PORT, FTDI_NRESET_GPIO_PIN);              
  DelayBySoft (50000);
  // FTDI NO RESET (BEFORE ENABLE OUTPUT)
  STM32F_GPIOOn(FTDI_NRESET_GPIO_PORT, FTDI_NRESET_GPIO_PIN);        
  DelayBySoft (1000);
  // ------------------------------------------------------------
  // C [SDCARD POWER SUPPLY]
  // ------------------------------------------------------------
  STM32F_GPIOOn(SD_PWR_EN_GPIO_PORT, SD_PWR_EN_GPIO_PIN);   
  // Delay(500);
  DelayBySoft (50000);
}
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
