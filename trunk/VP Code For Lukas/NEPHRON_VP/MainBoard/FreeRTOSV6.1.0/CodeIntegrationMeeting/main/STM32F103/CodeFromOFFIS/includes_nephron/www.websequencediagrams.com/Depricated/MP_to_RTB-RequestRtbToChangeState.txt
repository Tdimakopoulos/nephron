note left of RTBPA
  RTBPA:    Realtimeboard protocol abstraction task
  MB UART:  Mainboard UART driver stack
  RTB UART: Realtimeboard UART driver stack
  RTB SW:   Realtimeboard Application/Firmware
end note

note left of RTBPA
  /* WAKD is requested to change its state WAKD! */
  /* Reason can either be msg from Backend */
  /* or from user operating SP GUI */
  /* or internal reason (Flow Control Task) */
end note

note left of RTBPA
  /* WAKD should know the current WAKD state */
  tdWakdStates currentState;
  currentState = wakdStates_Dialysis;
  
  /* Define message to sent */
  tdWakdStateFromTo myMsg;
  myMsg.header.dataId = dataID_ChangeWAKDStateFromTo;
  myMsg.header.msgCount = 0; // unused, not defined
  myMsg.header.issuedBy = whoId_RTB;
  myMsg.fromWakdState = currentState;
  myMsg.toWakdState = wakdStates_AllStopped
end note

RTBPA -> MB UART: uint8_t err = putData(whoId_RTB, (void *)&myMsg, sizeof(tdWakdStateFromTo));

activate RTBPA

note left of "MB UART"
  Buffer
  Byte 1
  ...
  Byte n
end note

note over "MB UART" "RTB UART"
  To be detailed by CSEM
  the idea is that an API on each side
  of the UART connection makes the serial
  interface completely transparent.
  The API implements only the two functions
  putData() and getData()
end note

note right of "RTB UART"
  Buffer
  Byte 1
  ...
  Byte n
end note

alt ERROR
  note over "RTB UART" "MB UART"
    Retry 3 times
  end note
  MB UART -> RTBPA: return(1) //error
else CRC OK
  RTB UART -> MB UART: CRC OK
  MB UART -> RTBPA: return(0) //no error
  deactivate RTBPA

  note right of "RTB UART"
    (block buffer)
  end note

  RTB UART -> RTB SW: IRQ
  
  note right of "RTB SW"
    /* ISR */
    /* Do what is needed */
  end note

  note left of "RTB SW"
    (free buffer)
  end note
end  
  