/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#ifdef LINUX
	#include "main.h"
	#include "dispatcher.h"
#else
	// it would be cool to get rid of these paths and just include
	// them all in the search path of the compiler.
	#include "..\\main\\main.h"
	#include "dispatcher.h"
#endif

#define PREFIX "OFFIS FreeRTOS task DISP"

// #define DEBUG_DISP

/*----------------------------------------------------------*/
/* Included in STM3210B-EVAL board.							*/
/*----------------------------------------------------------*/

uint8_t heartBeatDISP = 0;

// On system reset, dispatcher task will forward reset request to the attached HW: CB, RTB, PMB1 and PMB2.
// After sending the msg the task waits for the responsible abstraction tasks to transmit this request and than
// terminate. Dispatcher needs to wait for all these tasks to terminate.
// This flag marks with a value != 0 that the system is going down. This is also an important flag for the
// watchdog task. If system is going down, some tasks might have terminated already while other are still
// busy finishing. Since the already terminated tasks are no longer alive, watch dog would hard reset the system
// on detection. So system shut down is a special case for watch dog to know.
uint8_t resetWaitingForTasksShutdown	= 0;

void tskDISP( void *pvParameters )
{
 //  uint8_t testcount =0;

	taskMessage("I", PREFIX, "Starting Dispatcher task!");

    xQueueHandle *qhDISPin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhDISPin;
    xQueueHandle *qhCBPAin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhCBPAin;
    xQueueHandle *qhRTBPAin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhRTBPAin;
    xQueueHandle *qhUIin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhUIin;
    xQueueHandle *qhDSin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhDSin;
    xQueueHandle *qhFCin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhFCin;
    xQueueHandle *qhSCCin	= ((tdAllHandles *) pvParameters)->allQueueHandles.qhSCCin;
    xQueueHandle *qhPMBPA0in= ((tdAllHandles *) pvParameters)->allQueueHandles.qhPMBPA0in;
    xQueueHandle *qhPMBPA1in= ((tdAllHandles *) pvParameters)->allQueueHandles.qhPMBPA1in;
	
 	tdQtoken Qtoken;
	Qtoken.command	= command_DefaultError;
	Qtoken.pData	= NULL;

	#ifdef DEBUG_STACK
		unsigned short stackHighWaterMark = uxTaskGetStackHighWaterMark(NULL);
		taskMessage("I", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
	#endif

	// system normal operation. If no message comes in for dispatcher we just wait
	// for a very long time. But after too long time we expect that something is wrong.
	// This time is defined here.
	portTickType dispatcherPeriod = DISP_PERIOD;

	for( ;; ) { 
		//The heart beat variable is checked by the watch dog task to see if this
		// task is still alive. This value has to change at least every DISP_PERIOD.
		// Otherwise watch dog might decide to reset the entire system!
		heartBeatDISP++;

		#ifdef DEBUG_STACK
			// Summary
			// Each task maintains its own stack, the total size of which is specified when the task is created.
			// uxTaskGetStackHighWaterMark() is used to query how near a task has come to overflowing the stack
			// space allocated to it. This value is called the stack 'high water mark'.
			// Return Values
			// The value returned is the high water mark in words (one word being 4 bytes on a 32bit architecture).
			// The closer the returned value is to zero the closer the task has come to overflowing its stack. A return
			// value of zero indicates that the task has actually overflowed its stack already.
			unsigned short stackHighWaterMarkNew = uxTaskGetStackHighWaterMark(NULL);
			if (stackHighWaterMark != stackHighWaterMarkNew) {
				stackHighWaterMark = stackHighWaterMarkNew;
				if (stackHighWaterMark == 0)
				{
					taskMessage("E", PREFIX, "OUT OFF STACK!!");
				} else if (stackHighWaterMark < 32)
				{
					taskMessage("W", PREFIX, "Stack left to use is: %d", stackHighWaterMark);
				}
			}
		#endif

		// If the system is going down due to previous command we check here if all
		// necessary tasks did terminate. After they did, kill task watch dog and HW
		// will create a reset.
		if (resetWaitingForTasksShutdown)
		{
			if (((tdAllHandles *) pvParameters)->allTaskHandles.handleDS     != NULL)
				taskMessage("I", PREFIX, "Shutdown waiting for DS.");
			if (((tdAllHandles *) pvParameters)->allTaskHandles.handleCBPA   != NULL)
				taskMessage("I", PREFIX, "Shutdown waiting for CBPA.");
			if (((tdAllHandles *) pvParameters)->allTaskHandles.handleRTBPA  != NULL)
				taskMessage("I", PREFIX, "Shutdown waiting for RTBPA.");
			if (((tdAllHandles *) pvParameters)->allTaskHandles.handlePMBPA0 != NULL)
				taskMessage("I", PREFIX, "Shutdown waiting for PMBPA0.");
			if (((tdAllHandles *) pvParameters)->allTaskHandles.handlePMBPA1 != NULL)
				taskMessage("I", PREFIX, "Shutdown waiting for PMBPA1.");

			if (	(((tdAllHandles *) pvParameters)->allTaskHandles.handleDS		== NULL)
				&&	(((tdAllHandles *) pvParameters)->allTaskHandles.handleCBPA		== NULL)
				&&	(((tdAllHandles *) pvParameters)->allTaskHandles.handleRTBPA	== NULL)
				&&	(((tdAllHandles *) pvParameters)->allTaskHandles.handlePMBPA0	== NULL)
				&&	(((tdAllHandles *) pvParameters)->allTaskHandles.handlePMBPA1	== NULL)
				)
			{	// System is prepared to be restarted/shutdown.
				if (resetWaitingForTasksShutdown == dataID_reset)
				{	// Shut down the task watch dog.
					// This will stop reseting the HW watchdog timer. It will trigger and cause for SW reset.
					#if defined DEBUG_DISP | defined SEQ0
						taskMessage("I", PREFIX, "Tasks are ready for restart. 'Releasing' the HW watchdog.");
					#endif
					vTaskDelete(((tdAllHandles *) pvParameters)->allTaskHandles.handleWD);
					vTaskSuspend(NULL); // sit still and wait for system to restart.
				} else {
					#if defined DEBUG_DISP | defined SEQ00
						taskMessage("I", PREFIX, "Waiting forever for patient to turn off power.");
					#endif
					// CODE to turn of power goes here.
					// For the time lets put an endless loop here, doing nothing.
					*(REG32 wdControl) = 0;	// disable watch dog.
					vTaskSuspendAll();
					taskMessage("I", PREFIX, "Embedded software terminated successfully!");
					taskMessage("W", PREFIX, "All tasks stopped - ISR will not be served: following errors are normal as a consequence!");
					while(1){};
				}
			}
		}

		/*
		 * Watch Task's input queue for commands to come in.
		 * If nothing happens in queue, the task will wake up anyway
		 * after period to give note that it is unusually quiet!
		 */
		if ( xQueueReceive( qhDISPin, &Qtoken, dispatcherPeriod) == pdPASS )
		{
			#ifdef DEBUG_DISP
				taskMessage("I", PREFIX, "id: %d in dispatcher queue!", Qtoken.command);
			#endif
			switch (Qtoken.command) {
				case command_BufferFromCBavailable: {
					// Forward to CBPA Task
					if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
						// Seemingly we were able to write to the queue. Great!
						#if defined DEBUG_DISP || defined SEQ1 || defined SEQ2 || defined SEQ3 || defined SEQ4 || defined SEQ5
							taskMessage("I", PREFIX, "Forwarding 'command_BufferFromCBavailable' to CBPA.");
						#endif
					} else {
						// Seemingly the queue is full for too long time. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of CBPA did not accept 'command_BufferFromCBavailable'!");
						errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
					}
					break;
				}
				case command_BufferFromRTBavailable: {
					// Forward to RTBPA Task
					if ( xQueueSend( qhRTBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
						// Seemingly we were able to write to the queue. Great!
						#if defined DEBUG_DISP || defined SEQ4 || defined SEQ6 || defined SEQ7 || defined SEQ8
							taskMessage("I", PREFIX, "Forwarding 'command_BufferFromRTBavailable' to RTBPA.");
						#endif
					} else {
						// Seemingly the queue is full for too long time. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'command_BufferFromRTBavailable'!");
						errMsg(errmsg_QueueOfRealtimeBoardAbstractionTaskFull);
					}
					break;
				}
				case command_RtbChangeIntoStateRequest: {
					// Forward this message to RTBPA task
					#if defined DEBUG_DISP || defined SEQ0 || defined SEQ4
						taskMessage("I", PREFIX, "Forwarding 'command_RtbChangeIntoStateRequest' to RTBPA.");
					#endif
					if ( xQueueSend( qhRTBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full for too long time. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'command_BufferFromRTBavailable'!");
						errMsg(errmsg_QueueOfRealtimeBoardAbstractionTaskFull);
						sFree(&(Qtoken.pData));
					}
					break;
				}
				case command_RequestDatafromDSReady: {
					// Forward this message to SCC task, if the request responded to
					// was coming from 'whoId_MB', because only SCC is the only task requesting data this way
					if (  ((tdMsgSystemInfo *)Qtoken.pData)->header.recipientId == whoId_MB  ) {
					
						#if defined DEBUG_DISP
							taskMessage("I", PREFIX, "Forwarding 'command_RequestDatafromDSReady' to SCC.");
						#endif
						if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
						{	// Seemingly the queue is full for too long time. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of SCC did not accept 'command_RequestDatafromDSReady'!");
							errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
							sFree(&(Qtoken.pData));
						} // of if ( xQueueSend( qhSCCin...
					} else {
						taskMessage("E", PREFIX, "Request for data was not issued by SCC (whoId_MB). Do not know how to react. If this was the SP requesting, this needs to be implemented");
#warning NEEDS IMPLEMENTATION!
						sFree(&(Qtoken.pData));
					} // of if (  ((tdMsgSystemInfo *)Qtoken.p...
					break;
				}
				case command_ConfigurationChanged: {
					// check for which task the configuration has changed and forward command to this task
					if (  ((tdMsgOnly *)Qtoken.pData)->header.dataId == dataID_PatientProfile  
						||  ((tdMsgOnly *)Qtoken.pData)->header.dataId == dataID_configureState	) {
						// Forward this message to FC task, as it needs to reconfigure itself now
						#if defined DEBUG_DISP || defined SEQ13
							taskMessage("I", PREFIX, "Forwarding 'command_ConfigurationChanged' to FC.");
						#endif
						if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
						{	// Seemingly the queue is full for too long time. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of FC did not accept 'command_ConfigurationChanged'!");
							errMsg(errmsg_QueueOfFlowControlTaskFull);
							sFree(&(Qtoken.pData));
						} //  of if ( xQueueSend( qhFCin,  ...
					} else if (((tdMsgOnly *)Qtoken.pData)->header.dataId == dataID_StateParametersSCC) {
					// Forward this message to SCC task, as it needs to reconfigure itself now
					
						#if defined DEBUG_DISP || defined SEQ13
							taskMessage("I", PREFIX, "Forwarding 'command_ConfigurationChanged' to SCC.");
						#endif
						if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
						{	// Seemingly the queue is full for too long time. Do something accordingly!
							taskMessage("E", PREFIX, "Queue of SCC did not accept 'command_ConfigurationChanged'!");
							errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
							sFree(&(Qtoken.pData));
						} // of if ( xQueueSend( qhSCCin, 	
					} else {
						taskMessage("E", PREFIX, "DISP received command_ConfigurationChanged with wrong header.dataId. Do not know how to react");
						sFree(&(Qtoken.pData));
					} // of if (  ((tdMsgSystemInfo *)Qtoken.p...
					break;
				}	
				case command_UseAtachedMsgHeader: {
					// Look at header for further information.
					tdDataId dataId = ((tdMsgOnly *)(Qtoken.pData))->header.dataId;
					switch (dataId){
						case dataID_systemInfo: {
						// Forward this message User-interface and FC-task
							tdMsgSystemInfo *pMsgSystemInfo_UI = NULL;	// message to be sent to UI
							tdMsgSystemInfo *pMsgSystemInfo_SP = NULL;	// message to be sent to SP
							tdMsgSystemInfo *pMsgSystemInfo_FC = NULL;	// message to be sent to FC
							pMsgSystemInfo_FC = (tdMsgSystemInfo *)(Qtoken.pData); //original to be send to FC
				// *** for UI task
							pMsgSystemInfo_UI   = (tdMsgSystemInfo *) pvPortMalloc(sizeof(tdMsgSystemInfo)); // Will be freed by receiver task!
							if ( pMsgSystemInfo_UI == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgSystemInfo_UI,  Qtoken.pData, sizeof(tdMsgSystemInfo));
								// Forward to UI Task
								#if defined DEBUG_DISP || defined SEQ11
									taskMessage("I", PREFIX, "Forwarding 'dataID_systemInfo' to UI");
								#endif
								Qtoken.pData = (void *) pMsgSystemInfo_UI;
								if ( xQueueSend( qhUIin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of User Interface did not accept 'command_ProcessSCCmessage'!");
									errMsg(errmsg_QueueOfUserInterfaceTaskFull);
									sFree((void *)&pMsgSystemInfo_UI);
								}
							}
				// *** for SP (CBPA task)
							pMsgSystemInfo_SP   = (tdMsgSystemInfo *) pvPortMalloc(sizeof(tdMsgSystemInfo)); // Will be freed by receiver task!
							if ( pMsgSystemInfo_SP == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgSystemInfo_SP,  Qtoken.pData, sizeof(tdMsgSystemInfo));
								// Forward to SP Task
								#if defined DEBUG_DISP || defined SEQ11
									taskMessage("I", PREFIX, "Forwarding 'dataID_systemInfo' to CBPA");
								#endif
								Qtoken.pData = (void *) pMsgSystemInfo_SP;
								if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// RTBPA queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of RealTimeBoard did not accept 'command_ProcessSCCmessage'!");
									errMsg(errmsg_QueueOfUserInterfaceTaskFull);
									sFree((void *)&pMsgSystemInfo_SP);
								}
							}
				// *** for FC task
							#if defined DEBUG_DISP || defined SEQ11
								taskMessage("I", PREFIX, "Forwarding 'dataID_systemInfo' to FC.");
							#endif
							Qtoken.pData = (void *) pMsgSystemInfo_FC;
							if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of FC did not accept 'command_ProcessSCCmessage'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;								
						}
						case (dataID_shutdown): // fall through, continue with reset.
						case (dataID_reset):{
							// The system will now be in shutdown mode which is defined by the following flag.
							// As an effect the period of the Dispatcher task will be increased to check if
							// the other tasks did comply and terminated. Secondly the watch dog task will be
							// set one last time for a max. shutdown duration and terminates.
							resetWaitingForTasksShutdown = dataId;	// Remember if reset or shutdown

							#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
								taskMessage("I", PREFIX, "Reset/shutdown requested. Telling tasks to shutdown.");
							#endif
							// Initiating SW system shutdown
							dispatcherPeriod = DISP_PERIOD_RESET;	// increase frequency to poll termination of the following tasks

							tdMsgOnly *pMsgResetCB   = NULL;	// message to be sent to communication board
							tdMsgOnly *pMsgResetRTB  = NULL;	// message to be sent to real time board
							tdMsgOnly *pMsgResetPMB0 = NULL;	// message to be sent to power management board 1
							tdMsgOnly *pMsgResetPMB1 = NULL;	// message to be sent to power management board 2
							pMsgResetCB   = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
							pMsgResetRTB  = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
							pMsgResetPMB0 = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
							pMsgResetPMB1 = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly)); // Will be freed by receiver task!
				// *** for data storage task
							// simple forward of received message
							#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
								taskMessage("I", PREFIX, "Telling DS to shutdown.");
							#endif
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// DS queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'dataID_reset'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree(&(Qtoken.pData));
							}
				// *** for communication board
							if ( pMsgResetCB == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								pMsgResetCB->header.dataId		= dataId; // either dataID_reset or dataID_shutdown
								pMsgResetCB->header.issuedBy	= whoId_MB;
								pMsgResetCB->header.msgSize		= 6;
								pMsgResetCB->header.recipientId	= whoId_CB;
								Qtoken.command = command_UseAtachedMsgHeader;
								Qtoken.pData   = (void *) pMsgResetCB;
								#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
									taskMessage("I", PREFIX, "Telling CB to shutdown.");
								#endif
								if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_reset'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgResetCB);
								}
							}
				// *** for real time board
							if ( pMsgResetRTB == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								pMsgResetRTB->header.dataId			= dataId; // either dataID_reset or dataID_shutdown
								pMsgResetRTB->header.issuedBy		= whoId_MB;
								pMsgResetRTB->header.msgSize		= 6;
								pMsgResetRTB->header.recipientId	= whoId_RTB;
								Qtoken.pData   = (void *) pMsgResetRTB;
								#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
									taskMessage("I", PREFIX, "Telling RTB to shutdown.");
								#endif
								if ( xQueueSend( qhRTBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_reset'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgResetCB);
								}
							}
				// *** for power management board 1
							if ( pMsgResetPMB0 == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								pMsgResetPMB0->header.dataId		= dataId; // either dataID_reset or dataID_shutdown
								pMsgResetPMB0->header.issuedBy	= whoId_MB;
								pMsgResetPMB0->header.msgSize		= 6;
								pMsgResetPMB0->header.recipientId	= whoId_PMB1;
								Qtoken.pData   = (void *) pMsgResetPMB0;
								#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
									taskMessage("I", PREFIX, "Telling PMB0 to shutdown.");
								#endif
								if ( xQueueSend( qhPMBPA0in, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_reset'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgResetCB);
								}
							}
				// *** for power management board 2
							if ( pMsgResetPMB1 == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								pMsgResetPMB1->header.dataId		= dataId; // either dataID_reset or dataID_shutdown
								pMsgResetPMB1->header.issuedBy	= whoId_MB;
								pMsgResetPMB1->header.msgSize		= 6;
								pMsgResetPMB1->header.recipientId	= whoId_PMB2;
								Qtoken.pData   = (void *) pMsgResetPMB1;
								#if defined DEBUG_DISP | defined SEQ0 | defined SEQ00
									taskMessage("I", PREFIX, "Telling PMB1 to shutdown.");
								#endif
								if ( xQueueSend( qhPMBPA1in, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_reset'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgResetCB);
								}
							}
							break;
						}
						case (dataID_currentWAKDstateIs):{
							// This msg needs to be copied and send to CB, FC and SCC
							tdMsgCurrentWAKDstateIs *pCurrentWAKDstateForCB		= NULL;
							tdMsgCurrentWAKDstateIs *pCurrentWAKDstateForFC		= NULL;
							tdMsgCurrentWAKDstateIs *pCurrentWAKDstateForSCC	= NULL;
							// use original msg for CB
							pCurrentWAKDstateForCB = (tdMsgCurrentWAKDstateIs *)(Qtoken.pData);
				// make a copy for FC
							pCurrentWAKDstateForFC  = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs)); // Will be freed by receiver task!
							if ( pCurrentWAKDstateForFC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pCurrentWAKDstateForFC,  Qtoken.pData, sizeof(tdMsgCurrentWAKDstateIs));
								// Forward to FC Task
								#if defined DEBUG_DISP || defined SEQ0 || defined SEQ4 || defined SEQ5
									taskMessage("I", PREFIX, "Forwarded 'dataID_currentWAKDstateIs' to FC: '%s'|'%s'.", stateIdToString(pCurrentWAKDstateForFC->wakdStateIs), opStateIdToString(pCurrentWAKDstateForFC->wakdOpStateIs));
								#endif
								Qtoken.pData = (void *) pCurrentWAKDstateForFC;
								if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_currentWAKDstateIs'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pCurrentWAKDstateForFC);
								}
							}
				// make a copy for SCC
							pCurrentWAKDstateForSCC  = (tdMsgCurrentWAKDstateIs *) pvPortMalloc(sizeof(tdMsgCurrentWAKDstateIs)); // Will be freed by receiver task!
							if ( pCurrentWAKDstateForSCC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pCurrentWAKDstateForSCC,  Qtoken.pData, sizeof(tdMsgCurrentWAKDstateIs));
								// Forward to SCC Task
								#if defined DEBUG_DISP || defined SEQ0 || defined SEQ4 || defined SEQ5
									taskMessage("I", PREFIX, "Forwarded 'dataID_currentWAKDstateIs' to SCC: '%s'|'%s'.", stateIdToString(pCurrentWAKDstateForSCC->wakdStateIs), opStateIdToString(pCurrentWAKDstateForSCC->wakdOpStateIs));
								#endif
								Qtoken.pData = (void *) pCurrentWAKDstateForSCC;
								if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// SCC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of System Check/Calibration did not accept 'dataID_currentWAKDstateIs'!");
									errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
									sFree((void *)&pCurrentWAKDstateForSCC);
								}
							}
				// Forward to CBPA Task
							#if defined DEBUG_DISP || defined SEQ0 || defined SEQ4 || defined SEQ5
								taskMessage("I", PREFIX, "Forwarded 'dataID_currentWAKDstateIs' to CBPA: '%s'|'%s'.", stateIdToString(pCurrentWAKDstateForCB->wakdStateIs), opStateIdToString(pCurrentWAKDstateForCB->wakdOpStateIs));
							#endif
							Qtoken.pData = (void *) pCurrentWAKDstateForCB;
							// The sender of this msg is the RTB. It was sending this msg to all. Now dispatcher is
							// making an individual msg of this for the Smart Phone. Assign correct recipient ID!
							pCurrentWAKDstateForCB->header.recipientId = whoId_SP;
							pCurrentWAKDstateForCB->header.issuedBy    = whoId_MB;
							if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of CBPA did not accept 'dataID_currentWAKDstateIs'!");
								errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
								sFree((void *)&(pCurrentWAKDstateForCB));
							}
							break;
						}
						case (dataID_nack):{
							// Some sender is receiving an NACK from recipient. Let's see where it needs to go and forward.
							tdMsgOnly *pMsgAck = (tdMsgOnly *)(Qtoken.pData);
							switch (pMsgAck->header.recipientId){
								case whoId_SP :{
									// Forward to CBPA Task
									if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
										// Seemingly we were able to write to the queue. Great!
										#if defined DEBUG_DISP || defined SEQ4
											taskMessage("I", PREFIX, "Forwarded 'dataID_nack' to CBPA.");
										#endif
									} else {
										// Seemingly the queue is full for too long time. Do something accordingly!
										taskMessage("E", PREFIX, "Queue of CBPA did not accept 'dataID_nack'!");
										errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
										sFree((void *)&(Qtoken.pData));
									}
									break;
								}
								default :{
									// For this recipient Dispatcher does not know what to do. Big error!
									taskMessage("E", PREFIX, "Received 'dataID_nack' msg for unknown recipient. Cannot dispatch msg!");
									errMsg(errmsg_UnknownRecipient);
									break;
								}
							}
							break;
						}
						case (dataID_ack):{
							// Some sender is receiving an ACK from recipient. Let's see where it needs to go and forward.
							tdMsgOnly *pMsgAck = (tdMsgOnly *)(Qtoken.pData);
							switch (pMsgAck->header.recipientId){
								case whoId_SP :{
									// Forward to CBPA Task
									if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
										// Seemingly we were able to write to the queue. Great!
										#if defined DEBUG_DISP || defined SEQ4
											taskMessage("I", PREFIX, "Forwarded 'dataID_ack' to CBPA.");
										#endif
									} else {
										// Seemingly the queue is full for too long time. Do something accordingly!
										taskMessage("E", PREFIX, "Queue of CBPA did not accept 'dataID_ack'!");
										errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
										sFree((void *)&(Qtoken.pData));
									}
									break;
								}
								default :{
									// For this recipient Dispatcher does not know what to do. Big error!
									taskMessage("E", PREFIX, "Received 'dataID_ack' msg for unknown recipient. Cannot dispatch msg!");
									errMsg(errmsg_UnknownRecipient);
									break;
								}
							}
							break;
						}
						case (dataID_weightData):{
							// This weight measure needs to be checked by the patient. Forward to Task User Interface.
							// This same measure has also already been forwarded to the communication board to forward
							// to the smartphone GUI. So the checking by the patient can come back either from task UI or SP.
							// Forward to UI Task
							if ( xQueueSend( qhUIin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ1 || defined SEQ2
									taskMessage("I", PREFIX, "Forwarded 'dataID_weightData' to UI.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of UI did not accept 'dataID_weightData'!");
								errMsg(errmsg_DispDataWeightSensorReadEventLost);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_CurrentParameters):{
							// Somebody (probably the phone) wants to know the settings for some current
							// parameters. This needs to be answered by the data storage task. Forward it.
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ12
									taskMessage("I", PREFIX, "Forwarded 'dataID_CurrentParameters' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_CurrentParameters'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_ConfigureParameters):{
							// Somebody (probably the phone) wants to reprogram the configuration of the WAKD.
							// This needs to be forwarded to data storage task. After DS stored the values it
							// will automatically inform the remainder of the system about the change.
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ12
									taskMessage("I", PREFIX, "Forwarded 'dataID_ConfigureParameters' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_ConfigureParameters'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_ActualParameters):{
							// Somebody (probably the phone) wanted to know the settings for some current
							// parameters. This is the answer on this to be forwarded back to the communication board.
							if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ12
									taskMessage("I", PREFIX, "Forwarded 'dataID_ActualParameters' to CBPA.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of CBPA did not accept 'dataID_ActualParameters'!");
								errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_PatientProfile):{
							// There was somthing changed in the PatientProfile Data, this should be stored on the SD card. 
							
		taskMessage("F", PREFIX, "If somthing has changed in the configuration, this needs to be forwarded to SCC and FC, waiting for clarification with EXODUS if this is appropriate!");
							
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ1 || defined SEQ2
									taskMessage("I", PREFIX, "Forwarded 'dataID_PatientProfile' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_PatientProfile'!");
								errMsg(errmsg_DispDataPatientProfielWriteEventLost);
								sFree(&(Qtoken.pData));
							} 
							break;
						}
						case (dataID_changeWAKDStateFromTo):{
							// Changing of state is controlled by the flow control task. Forward msg there.
							if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ4
									taskMessage("I", PREFIX, "Forwarded 'dataID_changeWAKDStateFromTo' to FC.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of FC did not accept 'dataID_changeWAKDStateFromTo'!");
								errMsg(errmsg_DispChangeWAKDStateFromToEventLost);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_weightDataOK):{
							// Weight data that was validated by the patient. Store in data storage and also send to flow control and SCC
							#if defined DEBUG_DISP || defined SEQ3
								taskMessage("I", PREFIX, "Received patient validated weight measure.");
							#endif
							// We got a patient validated weight measure. Forward this to ...
							// ... Data Storage to store the values
							// ... Flow Control to compute new actuator values
							// Every task gets its own copy of the data and will free it later on.
							tdMsgWeightData *pMsgWeightDataOKforDS  = NULL;
							tdMsgWeightData *pMsgWeightDataOKforFC  = NULL;
							tdMsgWeightData *pMsgWeightDataOKforSCC  = NULL;
							// Reuse the data block created by sending task
							pMsgWeightDataOKforDS = (tdMsgWeightData *) (Qtoken.pData);
							// Copy data block for the other receivers.
							pMsgWeightDataOKforFC  = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData)); // Will be freed by receiver task!
							pMsgWeightDataOKforSCC = (tdMsgWeightData *) pvPortMalloc(sizeof(tdMsgWeightData)); // Will be freed by receiver task!
							if ( pMsgWeightDataOKforFC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgWeightDataOKforFC, Qtoken.pData, sizeof(tdMsgWeightData));
							}
							if ( pMsgWeightDataOKforSCC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgWeightDataOKforSCC, Qtoken.pData, sizeof(tdMsgWeightData));
							}

				// *** Data Storage
							// Our queue contains a pointer to sensor data. The memory was allocated by CBPA
							// We forwarded this pointer to Data Storage which will free the memory when it is done with it.
							// For Flow Control we made a copy of the same data. Flow control task will be responsible to free mem.
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP || defined SEQ3
									taskMessage("I", PREFIX, "Forwarded 'dataID_weightDataOK' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_weightDataOK'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
								sFree(&(Qtoken.pData));
							}

				// *** Flow Control
							if (pMsgWeightDataOKforFC != NULL)
							{
								Qtoken.pData = (void *) pMsgWeightDataOKforFC;
								if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
									// Seemingly we were able to write to the queue. Great!
									#if defined DEBUG_DISP || defined SEQ3
										taskMessage("I", PREFIX, "Forwarded 'dataID_weightDataOK' to FC.");
									#endif
								} else {
									// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of FC did not accept 'dataID_weightDataOK'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree(&(Qtoken.pData));
								}
							}
				// *** Systemcheck & Calibration
							if (pMsgWeightDataOKforSCC != NULL)
							{
								Qtoken.pData = (void *) pMsgWeightDataOKforSCC;
								if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
									// Seemingly we were able to write to the queue. Great!
									#if defined DEBUG_DISP || defined SEQ3
										taskMessage("I", PREFIX, "Forwarded 'dataID_weightDataOK' to SCC.");
									#endif
								} else {
									// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of SCC did not accept 'dataID_weightDataOK'!");
									errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
									sFree(&(Qtoken.pData));
								}
							}
							break;
						}
						case (dataID_statusRequest):{
							// Somebody wants to know the current state of the WAKD. Forward this to RTB.
							#if defined DEBUG_DISP || defined SEQ5 || defined SEQ0
								taskMessage("I", PREFIX, "Forwarding 'dataID_statusRequest' to RTB.");
							#endif
							if ( xQueueSend( qhRTBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'dataID_statusRequest'!");
								errMsg(errmsg_QueueOfRealtimeBoardAbstractionTaskFull);
								sFree(&(Qtoken.pData));
							}
							break;
						}
						case (dataID_configureState):{
							// Flow Control configures the states of the RTB: forward to RTBPA Task and store in logfile
							tdMsgConfigureState *pMsgConfigureStateforDS  = NULL;
							pMsgConfigureStateforDS  = (tdMsgConfigureState *) pvPortMalloc(sizeof(tdMsgConfigureState)); // Will be freed by receiver task!
							memcpy(pMsgConfigureStateforDS, Qtoken.pData, sizeof(tdMsgConfigureState));
							
							tdMsgConfigureState *pMsgConfigureStateforRTB  = NULL;
							pMsgConfigureStateforRTB  = Qtoken.pData;
							if (pMsgConfigureStateforRTB != NULL)
							{
								Qtoken.pData = (void *) pMsgConfigureStateforRTB;
								if ( xQueueSend( qhRTBPAin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
									// Seemingly we were able to write to the queue. Great!
									#if defined DEBUG_DISP || defined SEQ9 || defined SEQ0
										taskMessage("I", PREFIX, "Forwarded 'dataID_configureState' to RTBPA.");
									#endif
								} else {
									// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of RTBPA did not accept 'dataID_configureState'!");
									errMsg(errmsg_QueueOfRealtimeBoardAbstractionTaskFull);
									sFree(&(Qtoken.pData));
								}
							}
							
							if (pMsgConfigureStateforDS != NULL)
							{	
								// indicate with the dataID, that this shoud go into the dump-file!
								pMsgConfigureStateforDS->header.dataId=dataID_configureStateDump;
								#ifdef DSTEST // for DSTEST the file should be actually changed, not the logfile only!
								pMsgConfigureStateforDS->header.dataId=dataID_configureState;
								#endif
								Qtoken.pData = (void *) pMsgConfigureStateforDS;
								if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
									// Seemingly we were able to write to the queue. Great!
									#if defined DEBUG_DISP || defined SEQ9 || defined SEQ0
										taskMessage("I", PREFIX, "Forwarded 'dataID_configureStateDump' to DS.");
									#endif
								} else {
									// Seemingly the queue is full for too long time. Do something accordingly!
									taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_configureStateDump'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree(&(Qtoken.pData));
								}
							}
							
							break;
						}
						case dataID_WAKDAllStateConfigure: {
							tdMsgWAKDAllStateConfigure *pMsgWAKDAllStateConfigure  = NULL;
							// Reuse the data block created by sending task
							pMsgWAKDAllStateConfigure  = (tdMsgWAKDAllStateConfigure *) (Qtoken.pData);
							Qtoken.pData = (void *) pMsgWAKDAllStateConfigure;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP
									taskMessage("I", PREFIX, "Forwarded 'dataID_WAKDAllStateConfigure' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_WAKDAllStateConfigure'!");
								sFree(&(Qtoken.pData));
							} 
							break;
						}
						case dataID_physiologicalData:{
							// We got new physiological sensor data from Real Time Board sensors. Forward this to ...
							// ... System Check and Calibration
							// ... Data Storage to store the values
							// ... Flow Control to compute new actuator values
							// ... Smart Phone to store and show values.
							// Every task gets its own copy of the data and will free it later on.
							tdMsgPhysiologicalData *pMsgPhysiologicalSensorDataForFC  = NULL;
							tdMsgPhysiologicalData *pMsgPhysiologicalSensorDataForDS  = NULL;
							tdMsgPhysiologicalData *pMsgPhysiologicalSensorDataForSCC = NULL;
							tdMsgPhysiologicalData *pMsgPhysiologicalSensorDataForSP  = NULL;
							// Reuse the data block created by sending task
							pMsgPhysiologicalSensorDataForFC = (tdMsgPhysiologicalData *) (Qtoken.pData);
							// Copy data block for the other receivers.
							pMsgPhysiologicalSensorDataForDS  = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
							if ( pMsgPhysiologicalSensorDataForDS == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgPhysiologicalSensorDataForDS,  Qtoken.pData, sizeof(tdMsgPhysiologicalData));
							}
							pMsgPhysiologicalSensorDataForSCC = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
							if ( pMsgPhysiologicalSensorDataForSCC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgPhysiologicalSensorDataForSCC, Qtoken.pData, sizeof(tdMsgPhysiologicalData));
							}
							pMsgPhysiologicalSensorDataForSP = (tdMsgPhysiologicalData *) pvPortMalloc(sizeof(tdMsgPhysiologicalData)); // Will be freed by receiver task!
							if ( pMsgPhysiologicalSensorDataForSP == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgPhysiologicalSensorDataForSP, Qtoken.pData, sizeof(tdMsgPhysiologicalData));
							}
				// *** Flow Control
							// Our queue contains a pointer to sensor data. The memory was allocated by RTBPA
							// We just forward this pointer to Flow Control which will free the memory when it is done with it.
							#if defined DEBUG_DISP || defined SEQ6
								taskMessage("I", PREFIX, "Forwarding Physiological Sensor Data from RTBPA to FC.");
							#endif
							Qtoken.pData = (void *) pMsgPhysiologicalSensorDataForFC;
							if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// FC queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_physiologicalData'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
								sFree((void *)&pMsgPhysiologicalSensorDataForFC);
							}
				// *** Data Storage
							// Our queue contains a pointer to sensor data. The memory was allocated by RTBPA
							// We forwarded this pointer to Flow Control which will free the memory when it is done with it.
							// For Data Storage we made a copy of the same data. Data Storage task will be responsible to free mem.
							if (pMsgPhysiologicalSensorDataForDS != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ6
									taskMessage("I", PREFIX, "Forwarding Physiological Sensor Data from RTBPA to DS.");
								#endif
								Qtoken.pData = (void *) pMsgPhysiologicalSensorDataForDS;
								if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// DS queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'dataID_physiologicalData'!");
									errMsg(errmsg_QueueOfDataStorageTaskFull);
									sFree((void *)&pMsgPhysiologicalSensorDataForDS);
								}
							}
				// *** System Check Calibration
							if (pMsgPhysiologicalSensorDataForSCC != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ6
									taskMessage("I", PREFIX, "Forwarding Physiological Sensor Data from RTBPA to SCC.");
								#endif
								Qtoken.pData = (void *) pMsgPhysiologicalSensorDataForSCC;
								if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// SCC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of System Check/Calibration did not accept 'dataID_physiologicalData'!");
									errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
									sFree((void *)&pMsgPhysiologicalSensorDataForSCC);
								}
							}
				// *** Smart Phone (via Communication Board Protocol Abstraction)
							if (pMsgPhysiologicalSensorDataForSP != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ6
									taskMessage("I", PREFIX, "Forwarding Physiological Sensor Data from RTBPA to SP (CBPA).");
								#endif
								pMsgPhysiologicalSensorDataForSP->header.issuedBy    = whoId_MB;
								pMsgPhysiologicalSensorDataForSP->header.recipientId = whoId_SP;
								pMsgPhysiologicalSensorDataForSP->header.msgSize	 = 70;			// refer to 'byteTarnsfer.c'! Don't use 'sizeof()'
								Qtoken.pData = (void *) pMsgPhysiologicalSensorDataForSP;
								if ( xQueueSend( qhCBPAin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// CBPA queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Communication Board Abstraction did not accept 'dataID_physiologicalData'!");
									errMsg(errmsg_QueueOfCommunicationBoardAbstractionTaskFull);
									sFree((void *)&pMsgPhysiologicalSensorDataForSP);
								}
							}
							break;
						}
						case dataID_physicalData:{
							// We got new physical sensor data from Real Time Board sensors. Forward this to ...
							// ... System Check and Calibration
							// ... Data Storage to store the values
							// ... Flow Control
							// Every task gets its own copy of the data and will free it later on.
							tdMsgPhysicalsensorData *pMsgPhysicalsensorDataForSCC = NULL;
							tdMsgPhysicalsensorData *pMsgPhysicalsensorDataForDS  = NULL;
							tdMsgPhysicalsensorData *pMsgPhysicalsensorDataForFC  = NULL;
							// Reuse the data block created by sending task
							pMsgPhysicalsensorDataForDS  = (tdMsgPhysicalsensorData *) (Qtoken.pData);
							// Copy data block for the other receivers
							// SCC
							pMsgPhysicalsensorDataForSCC = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
							if ( pMsgPhysicalsensorDataForSCC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgPhysicalsensorDataForSCC, Qtoken.pData, sizeof(tdMsgPhysicalsensorData));
							}
							// FC
							pMsgPhysicalsensorDataForFC = (tdMsgPhysicalsensorData *) pvPortMalloc(sizeof(tdMsgPhysicalsensorData)); // Will be freed by receiver task!
							if ( pMsgPhysicalsensorDataForFC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgPhysicalsensorDataForFC, Qtoken.pData, sizeof(tdMsgPhysicalsensorData));
							}
				// *** Data Storage
							// Our queue contains a pointer to sensor data. The memory was allocated by RTBPA
							// We just forward the received data to Data Storage
							#if defined DEBUG_DISP || defined SEQ7
								taskMessage("I", PREFIX, "Forwarding Physical Sensor Data from RTBPA to DS.");
							#endif
							Qtoken.pData = (void *) pMsgPhysicalsensorDataForDS;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// DS queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'dataID_physicalData'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree((void *)&pMsgPhysicalsensorDataForDS);
							}
				// *** System Check Calibration
							if (pMsgPhysicalsensorDataForSCC != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ7
									taskMessage("I", PREFIX, "Forwarding Physical Sensor Data from RTBPA to SCC.");
								#endif
								Qtoken.pData = (void *) pMsgPhysicalsensorDataForSCC;
								if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// SCC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of System Check/Calibration did not accept 'dataID_physicalData'!");
									errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
									sFree((void *)&pMsgPhysicalsensorDataForSCC);
								}
							}
				// *** Flow Control
							if (pMsgPhysicalsensorDataForFC != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ7
									taskMessage("I", PREFIX, "Forwarding Physical Sensor Data from RTBPA to FC.");
								#endif
								Qtoken.pData = (void *) pMsgPhysicalsensorDataForFC;
								if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_physicalData'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgPhysicalsensorDataForFC);
								}
							}
							break;
						}
						case (dataID_statusRTB) :{
							// We got new status data from Real Time Board sensors. Forward this to ...
							// ... System Check and Calibration - this is not implemented yet, cause we do not know how to interpret these messages
							// ... Data Storage to store the values
							#if defined DEBUG_DISP 
								taskMessage("F", PREFIX, "The Status Data is not used in any decision yet, it is necessary to interpret if and define what should be done...");
							#endif

						// *** Data Storage
							tdMsgDeviceStatus *pMsgDeviceStatusForDS  = NULL;
							pMsgDeviceStatusForDS  = (tdMsgDeviceStatus *) (Qtoken.pData);
							// Our queue contains a pointer to status data. The memory was allocated by RTBPA
							// We just forward the received data to Data Storage
							#if defined DEBUG_DISP || defined SEQ7
								taskMessage("I", PREFIX, "Forwarding Status Data from RTBPA to DS.");
							#endif
							Qtoken.command	= command_UseAtachedMsgHeader;
							Qtoken.pData = (void *) pMsgDeviceStatusForDS;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// DS queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'dataID_statusRTB'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree((void *)&pMsgDeviceStatusForDS);
							}
							break;
						}
						case dataID_actuatorData:{
							// We got new actuator sensor data from Real Time Board actuators. Forward this to ...
							// ... System Check and Calibration
							// ... Data Storage to store the values
							// ... Flow Control
							// Every task gets its own copy of the data and will free it later on.
							tdMsgActuatorData *pMsgActuatorDataForSCC = NULL;
							tdMsgActuatorData *pMsgActuatorDataForDS  = NULL;
							tdMsgActuatorData *pMsgActuatorDataForFC  = NULL;
							// Reuse the data block created by sending task
							pMsgActuatorDataForDS  = (tdMsgActuatorData *) (Qtoken.pData);
							// Copy data block for the other receivers
							// SCC
							pMsgActuatorDataForSCC = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
							if ( pMsgActuatorDataForSCC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgActuatorDataForSCC, Qtoken.pData, sizeof(tdMsgActuatorData));
							}
							// FC
							pMsgActuatorDataForFC = (tdMsgActuatorData *) pvPortMalloc(sizeof(tdMsgActuatorData)); // Will be freed by receiver task!
							if ( pMsgActuatorDataForFC == NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								memcpy(pMsgActuatorDataForFC, Qtoken.pData, sizeof(tdMsgActuatorData));
							}
				// *** Data Storage
							// Our queue contains a pointer to sensor data. The memory was allocated by RTBPA
							// We just forward the received data to Data Storage
							#if defined DEBUG_DISP || defined SEQ8
								taskMessage("I", PREFIX, "Forwarding Actuator Data from RTBPA to DS.");
							#endif
							Qtoken.pData = (void *) pMsgActuatorDataForDS;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// DS queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'dataID_actuatorData'!");
								errMsg(errmsg_QueueOfDataStorageTaskFull);
								sFree((void *)&pMsgActuatorDataForDS);
							}
				// *** System Check Calibration
							if (pMsgActuatorDataForSCC != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ8
									taskMessage("I", PREFIX, "Forwarding Actuator Data from RTBPA to SCC.");
								#endif
								Qtoken.pData = (void *) pMsgActuatorDataForSCC;
								if ( xQueueSend( qhSCCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// SCC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of System Check/Calibration did not accept 'dataID_actuatorData'!");
									errMsg(errmsg_QueueOfSystemCheckCalibrationTaskFull);
									sFree((void *)&pMsgActuatorDataForSCC);
								}
							}
				// *** Flow Control
							if (pMsgActuatorDataForFC != NULL)
							{
								#if defined DEBUG_DISP || defined SEQ8
									taskMessage("I", PREFIX, "Forwarding Actuator Data from RTBPA to FC.");
								#endif
								Qtoken.pData = (void *) pMsgActuatorDataForFC;
								if ( xQueueSend( qhFCin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// FC queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Flow Control did not accept 'dataID_actuatorData'!");
									errMsg(errmsg_QueueOfFlowControlTaskFull);
									sFree((void *)&pMsgActuatorDataForFC);
								}
							}
							break;
						}
						case dataID_EcgData:
						{
							tdMsgEcgData *pMsgEcgData  = NULL;
							// Reuse the data block created by sending task
							pMsgEcgData  = (tdMsgEcgData *) (Qtoken.pData);
							Qtoken.pData = (void *) pMsgEcgData;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP
									taskMessage("I", PREFIX, "Forwarded 'dataID_EcgData' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_EcgData'!");
								sFree(&(Qtoken.pData));
							} 
							break;
						}						
						case dataID_bpData:
						{
							tdMsgBpData *pMsgBpDataForDS  = NULL;
							// Reuse the data block created by sending task
							pMsgBpDataForDS  = (tdMsgBpData *) (Qtoken.pData);
							Qtoken.pData = (void *) pMsgBpDataForDS;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP
									taskMessage("I", PREFIX, "Forwarded 'dataID_bpData' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_bpData'!");
								sFree(&(Qtoken.pData));
							} 
							break;
						}
						case dataID_StateParametersSCC:
						{
							tdMsgParametersSCC *pMsgParametersSCC  = NULL;
							// Reuse the data block created by sending task
							pMsgParametersSCC  = (tdMsgParametersSCC *) (Qtoken.pData);
							Qtoken.pData = (void *) pMsgParametersSCC;
							if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) == pdPASS ) {
								// Seemingly we were able to write to the queue. Great!
								#if defined DEBUG_DISP
									taskMessage("I", PREFIX, "Forwarded 'dataID_StateParametersSCC' to DS.");
								#endif
							} else {
								// Seemingly the queue is full for too long time. Do something accordingly!
								taskMessage("E", PREFIX, "Queue of DS did not accept 'dataID_StateParametersSCC'!");
								sFree(&(Qtoken.pData));
							}
							break;
						}						
						default:{
							defaultIdHandling(PREFIX, dataId);
							break;
						}
					}
					break;
				}
				case command_InitializeSCC:{
					// SCC (System Check and Calibration) requests to be initialized. Forward this to DS (Data Storage)
					#ifdef DEBUG_DISP
						taskMessage("I", PREFIX, "Forwarding 'command_InitializeSCC' to Data Storage.");
					#endif
					if ( xQueueSend(qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'command_InitializeSCC'.");
						errMsg(errmsg_QueueOfDataStorageTaskFull);
					}
					break;
				}
				case command_InitializeFC:{
					// FC (Flow Control) requests to be initialized. Forward this to DS (Data Storage)
					#if defined DEBUG_DISP || defined SEQ0
						taskMessage("I", PREFIX, "Forwarding 'command_InitializeFC' to DS.");
					#endif
					if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'command_InitializeFC'.");
						errMsg(errmsg_QueueOfDataStorageTaskFull);
					}
					break;
				}
				case command_RequestDatafromDS:{
					// Someone requested Data form DS - > forwarding request to DS!
					#if defined DEBUG_DISP || defined SEQ0
						taskMessage("I", PREFIX, "Forwarding 'command_RequestDatafromDS' to DS.");
					#endif
					if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'command_RequestDatafromDS'.");
						errMsg(errmsg_QueueOfDataStorageTaskFull);
					}
					break;
				}
				case command_getPhysioPatientData:{
					// request for physiological data recieved forwarding it to DS 
					if ( xQueueSend( qhDSin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of Data Storage did not accept 'command_getPhysioPatientData'.");
						errMsg(errmsg_QueueOfDataStorageTaskFull);
					}
					break;
				}
				case command_ButtonEvent: {
					// pointer pData was misused to transport data. Be beware of this!
					tdButtons btn = (tdButtons) Qtoken.pData;
					switch (btn) {
						case (btn_reset):{
							// The reset button is a special case that requires direct action of dispatcher task.
							// All other buttons are to be handled by user interface. In the final version no such hard reset
							// button will be available. We use it here for testing the simulation.
							Qtoken.command = command_UseAtachedMsgHeader;
							tdMsgOnly *pMsgReset = NULL;
							pMsgReset = (tdMsgOnly *) pvPortMalloc(sizeof(tdMsgOnly));
							if (pMsgReset== NULL )
							{	// unable to allocate memory
								taskMessage("E", PREFIX, "Unable to allocate memory. Heap full?");
								errMsg(errmsg_pvPortMallocFailed);
							} else {
								pMsgReset->header.dataId		= dataID_reset;
								pMsgReset->header.issuedBy		= whoId_MB;
								pMsgReset->header.msgSize		= 6;
								pMsgReset->header.recipientId	= whoId_MB;
								Qtoken.command = command_UseAtachedMsgHeader;
								Qtoken.pData   = (void *) pMsgReset;
								// send this to your own (dispatcher task) queue
								if ( xQueueSend( qhDISPin, &Qtoken, DISP_BLOCKING) != pdPASS )
								{	// dispatcher queue did not accept data. Free message and send error.
									taskMessage("E", PREFIX, "Queue of Dispatcher task did not accept 'dataID_reset'!");
									errMsg(errmsg_QueueOfDispatcherTaskFull);
									sFree((void *)&pMsgReset);
								}
							}
							break;
						}
						default:{
							// Inform User Interface that Button was pressed
							if ( xQueueSend( qhUIin, &Qtoken, DISP_BLOCKING) != pdPASS )
							{	// FC queue did not accept data. Free message and send error.
								taskMessage("E", PREFIX, "Queue of User Interface did not accept 'command_ButtonEvent'!");
								errMsg(errmsg_QueueOfFlowControlTaskFull);
							}
							break;
						}
					}
					break;
				}
				case command_InformGuiConnectBag:{
					#ifdef DEBUG_DISP
						taskMessage("I", PREFIX, "Asking GUI to ask patient to connect disposal bags.");
					#endif
					if ( xQueueSend( qhUIin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of user interface task did not accept 'command_InformGuiConnectBag'!");
						errMsg(errmsg_QueueOfUserInterfaceTaskFull);
					}
					break;
				}
				case command_InformGuiDisconnectBag:{
					#ifdef DEBUG_DISP
						taskMessage("I", PREFIX, "Asking GUI to ask patient to DISconnect disposal bags.");
					#endif
					if ( xQueueSend( qhUIin, &Qtoken, DISP_BLOCKING) != pdPASS )
					{	// Seemingly the queue is full. Do something accordingly!
						taskMessage("E", PREFIX, "Queue of user interface task did not accept 'command_InformGuiConnectBag'!");
						errMsg(errmsg_QueueOfUserInterfaceTaskFull);
					}
					break;
				}				
			default:{
				// The following function covers all possible commands with one big switch.
				// The reason for this implementation is as follows. The function switch does
				// not contain a 'default' so that compilation will generate a warning, whenever
				// a new command should have been forgotten to be coded.
				defaultCommandHandling(PREFIX, Qtoken.command);
				break;
			}
			}
		} else {
			// We now waited for "DISP_PERIOD" and nothing arrived in input queue.
			#if defined DEBUG_DISP
				taskMessage("I", PREFIX, "Timeout on Dispatcher Task queue. Keep waiting.");
			#endif
		}
	}
}

