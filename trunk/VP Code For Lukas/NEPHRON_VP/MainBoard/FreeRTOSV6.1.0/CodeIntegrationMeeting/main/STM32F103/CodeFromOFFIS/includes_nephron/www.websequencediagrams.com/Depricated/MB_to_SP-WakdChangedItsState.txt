note left of WAKD
  /* The WAKD changes its state! */
  /* Reason can either be external request */
  /* or internal control or */
  /* critical event (bubble detected) */
end note

note left of WAKD
  /* know current state of WAKD */
  tdWakdStates currentState;
  currentState = wakdStates_AllStopped;
 
  /* Define message to sent */
  tdCurrentWAKDstateIs myMsg;
  myMsg.header.dataId = dataID_CurrentWAKDstateIs;
  myMsg.wakdStateIs = currentState;
end note

alt WAKD internal reason for change in state

note left of WAKD
  // broadcast to all,
  myMsg.header.msgCount = 0;
  myMsg.header.issuedBy = whoId_nd; // undefined
end note

else SP requested change in state

note left of WAKD
  // Ack by sending msgCount back to SP
  myMsg.header.msgCount = msgCountSP;
  myMsg.header.issuedBy = whoId_SP;
end note

end

WAKD -> BT: uint8_t err = putData(whoId_SP, (void *)&myMsg, sizeof(tdCurrentWAKDstateIs));

note left of BT
  Buffer
  Byte 1
  ...
  Byte n
end note

note right of BT
  Buffer
  Byte 1
  ...
  Byte n
end note

BT -> SP: IRQ (new data)

note right of SP
  /* what data did arrive? */
  tdDataId myDataId;
  myDataId = *((tdDataId *)&Buffer);
  if (myDataId == dataID_CurrentWAKDstateIs) {
  __/* Allocate mem for new data */
  __tdCurrentWAKDstateIs *pReceivedMsg;
  __pReceivedMsg = (tdCurrentWAKDstateIs *) malloc(sizeof(tdCurrentWAKDstateIs));
  
  __/* copy Data into that mem */
  __getData(whoId_MB, (void *)pReceivedMsg,sizeof(tdCurrentWAKDstateIs));
  }
end note

SP -> BT: getData(whoId_MB, (void *)pReceivedMsg,sizeof(tdCurrentWAKDstateIs));

note right of SP
  currentState = pReceivedMsg->wakdStateIs;
  if (pReceivedMsg->header.issuedBy==whoId_SP){
  __/* Look at msg with pReceivedMsg->header.msgCount */
  __/* When currentState == toWakdState : ACK */
  __/* else Fail. Try again (or whatever) */
end note