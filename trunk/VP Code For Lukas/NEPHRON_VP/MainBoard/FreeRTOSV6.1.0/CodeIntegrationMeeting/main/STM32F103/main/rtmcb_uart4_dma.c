// -----------------------------------------------------------------------------------
// Copyright (C) 2012          CSEM S.A.            CH-2002 Neuchatel
// -----------------------------------------------------------------------------------
//
//! \file   rtmcb_uart4_dma.C
//! \brief  uart4 incoming data to memory by dma
//!
//! Routines for reading the UART4 and store automatically using DMA 
//!
//! \author  Dudnik G.S.
//! \date    28.01.2012
//! \version 0.001
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Includes
// -----------------------------------------------------------------------------------
#include "main.h"

// -----------------------------------------------------------------------------------
// Exported global variables
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// Private Definitions
// -----------------------------------------------------------------------------------
//#define PERIPH_BASE   ((uint32_t)0x40000000) /*!< Peripheral base address in the alias region */
// APB2PERIPH_BASE     (PERIPH_BASE + 0x10000)
// ADC1_BASE        (APB2PERIPH_BASE + 0x2400)
// ADC_DR OFFSET                         0x4C
// ADC1_DR_Address      ((uint32_t)0x4001244C)

// #define PERIPH_BASE           ((uint32_t)0x40000000) /*!< Peripheral base address in the alias region */
// #define APB1PERIPH_BASE       PERIPH_BASE
// #define UART4_BASE            (APB1PERIPH_BASE + 0x4C00) 
// USART DR OFFSET = 0x04
//#define UART4_DR_ADDRESS ((uint32_t) UART4_BASE + 0x00000004)
#define UART4_DR_ADDRESS ((uint32_t) 0x40004C04)
// -----------------------------------------------------------------------------------
//! \brief  RTMCB_UART4_DMA_Config
//!
//! ....
//!
//! \param      none
//!
//! \return     none
// -----------------------------------------------------------------------------------
//! \brief Initialization of DMA (and eventually the UART) incoming data reading
//!
//! This function initializes DMAx Channely for UART incoming data reading
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
void RTMCB_UART4_DMA_Config(void)
{
    DMA_InitTypeDef DMA_InitStructure;
   
    /* UART4 uses DMA2 channel3 ----------------------------------------------------*/
    //----------------------------------------------------------------------------

    // - DMA2 CLOCK ENABLE
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);

    DMA_DeInit(DMA2_Channel3);
    DMA_InitStructure.DMA_PeripheralBaseAddr = UART4_DR_ADDRESS;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&UART4_RX_DMA_RTMCB[0]; 
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = RX_DMA_RS422_RTMCB_TOTAL; // 256
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High; // Very High
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA2_Channel3, &DMA_InitStructure);
    /* Enable DMA2 channel3 */
    DMA_Cmd(DMA2_Channel3, ENABLE);
    /* Enable DMA1 channel1 Interrupt*/
    // DMA_ITConfig(DMA2_Channel3, DMA_CCR1_TCIE, ENABLE);
    // Erase reception Buffer
    RTMCB_UART4_DMA_EraseData();
    rtmcb_dma_counter_new = DMA_GetCurrDataCounter(DMA2_Channel3);  // will follow dma counter
    rtmcb_dma_counter_old = rtmcb_dma_counter_new;
    rtmcb_dma_pointer_auto = RX_DMA_RS422_RTMCB_TOTAL-rtmcb_dma_counter_new;
    rtmcb_dma_pointer = RX_DMA_RS422_RTMCB_TOTAL-rtmcb_dma_counter_new;                                      // index to the current data to read
}
// -----------------------------------------------------------------------------------
//! \brief RTMCB_UART4_DMA_NVIC_Config
//!
//! DMA Interrupt Enable for UART4 RX
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
// DMA Channel x (event UART4 RX)
void RTMCB_UART4_DMA_NVIC_Config(void){
    DEVICE_IRQ_Config(DMA2_Channel3_IRQn, PreemptivePriority_RTMCB_UART4, SubPriority_RTMCB_UART4);
}

// -----------------------------------------------------------------------------------
//! \brief RTMCB_UART4_DMA_EraseData
//!
//! Clear Reception Buffer in Memory
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
void RTMCB_UART4_DMA_EraseData (void){

    uint16_t k=0;
    for(k=0;k<RX_DMA_RS422_RTMCB_TOTAL;k++) {
        UART4_RX_DMA_RTMCB[k] = 0;
    }
    RTMCB_DMA_RX_NEW = SIGNAL_OFF;
     
}
// -----------------------------------------------------------------------------------
//! \brief RTMCB_UART4_DMA_ReadData
//!
//! Read Recently received data 
//!
//! \param void
//! \return void
//!
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
void RTMCB_UART4_DMA_ReadData (void){
  
  uint16_t k=0;
  uint16_t w=0;
  uint16_t k0=0;
  uint16_t dataread = 0;
  uint16_t localidx = 0;
  uint8_t buffer_ok = SIGNAL_OFF;

  // update pointers
  rtmcb_dma_counter_new = DMA_GetCurrDataCounter(DMA2_Channel3);  // will follow dma counter
  rtmcb_dma_pointer_auto = RX_DMA_RS422_RTMCB_TOTAL-rtmcb_dma_counter_new;
  rtmcb_dma_counter_old = rtmcb_dma_counter_new;
  // no data to read

  // if 1st = 0, searches for data with no 0 [now they are all 0, as I erase when read
  dataread = UART4_RX_DMA_RTMCB[rtmcb_dma_pointer];
  UART4_RX_DMA_RTMCB[rtmcb_dma_pointer] = 0;
  // active pointer is also circular
  rtmcb_dma_pointer++;
  if(rtmcb_dma_pointer>=RX_DMA_RS422_RTMCB_TOTAL)rtmcb_dma_pointer=0;

  if(dataread==0){
      do{
          dataread=UART4_RX_DMA_RTMCB[rtmcb_dma_pointer];
          UART4_RX_DMA_RTMCB[rtmcb_dma_pointer] = 0;
          // active pointer is also circular
          rtmcb_dma_pointer++;
          if(rtmcb_dma_pointer>=RX_DMA_RS422_RTMCB_TOTAL)rtmcb_dma_pointer=0;
          if(dataread != 0) break;
      }while(dataread == 0);
  }
  // stores 1st non 0 byte
  zrx_out_rtmcb_str_x[localidx] = (uint8_t) (dataread & 0x00FF);
  localidx++;

  do{
      dataread=UART4_RX_DMA_RTMCB[rtmcb_dma_pointer];
      UART4_RX_DMA_RTMCB[rtmcb_dma_pointer] = 0;
      // active pointer is also circular
      rtmcb_dma_pointer++;
      if(rtmcb_dma_pointer>=RX_DMA_RS422_RTMCB_TOTAL)rtmcb_dma_pointer=0;
      // stores ist non 0 byte
      zrx_out_rtmcb_str_x[localidx] = (uint8_t) (dataread & 0x00FF);
      localidx++;
      if(localidx>=RX_DMA_RS422_RTMCB_TOTAL) break;
      // read i but 0
      if(dataread == 0) {
          buffer_ok = SIGNAL_ON;
          break;
      }
  }while(dataread !=0);
  if(rtmcb_cmd_streaming == SIGNAL_ON){
      k=0;
  }
  // good message and previous has been read, transfer...
  if((buffer_ok == SIGNAL_ON)&&(RTMCB_DMA_RX_NEW == SIGNAL_OFF)){
      for(k=0;k<localidx;k++){
          zrx_out_rtmcb_str_0[k] = zrx_out_rtmcb_str_x[k];
          zrx_out_rtmcb_str_x[k]=0;
      }
      out_rtmcb_reclen_0 = localidx;
      OUT_COMM_RTMCB_FLAG = 0xFF;
      RTMCB_DMA_RX_NEW = SIGNAL_ON;
  }
}
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------
// (C) COPYRIGHT 2011 CSEM SA
// -----------------------------------------------------------------------------------
// END OF FILE
// -----------------------------------------------------------------------------------
