/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "deriveMsgSizeForAktualParameters.h"

uint16_t deriveMsgSizeForAktualParameters(uint16_t numberParameter, void *pData, char *PREFIX)
{
	// size of msg to send back. 8 Bytes for header
	// one byte for the WAKD state in question
	// and more to figure out below.
	uint16_t msgSize = sizeof(tdMsgOnly)+1;

	int i = 0;
	// Start loop through the parameters. Right after the header + one byte for defining the
	// relevant state the loop begins. And continues from there to the number of listed parameter names.
	for (i=sizeof(tdMsgOnly)+1; i < (uint16_t)sizeof(tdMsgOnly)+1+numberParameter; i++)
	{
		tdParameter parameter = (tdParameter)(*(((uint8_t*)pData)+i));
		tdMsgPatientProfileData *pMsgPatientProfileData = NULL;								// This pointer will never be used. It's only purpose is to use it with "sizeof" to compute required bytes.
		tdMsgWAKDStateConfigurationParameters *pMsgWAKDStateConfigurationParameters = NULL;	// This pointer will never be used. It's only purpose is to use it with "sizeof" to compute required bytes.
		tdParametersSCC *pParametersSCC = NULL;												// This pointer will never be used. It's only purpose is to use it with "sizeof" to compute required bytes.
		switch (parameter) {
			case parameter_name: {
				// char	name[50];
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes + 1 for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.name), parameterToString(parameter));
				#endif
				// 50 bytes will be needed to store the string. One extra byte is needed defining the size of the string (50)
				// msgSize = msgSize + 51;
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.name) + 1;
				break;
			}
			case parameter_gender: {						// char	gender; // m/f
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.gender), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.gender);
				break;
			}
			case parameter_measureTimeHours: {			// uint8_t	measureTimeHours;		// daily time for weight & bp measurement, hour
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.measureTimeHours), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.measureTimeHours);
				break;
			}
			case parameter_measureTimeMinutes: {		// uint8_t 	measureTimeMinutes;		// daily time for weight & bp measurement, minute
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.measureTimeMinutes), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.measureTimeMinutes);
				break;
			}
			case parameter_minDialPlasFlow:	{			//	uint16_t  	minDialPlasFlow;		// minimal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.minDialPlasFlow), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.minDialPlasFlow);
				break;
			}
			case parameter_maxDialPlasFlow:	{			//	uint16_t  	maxDialPlasFlow;		// maximal flowrate for plasma in Dialysis mode  FIXPOINTSHIFT_PUMP_FLOW
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.maxDialPlasFlow), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.maxDialPlasFlow);
				break;
			}
			case parameter_minDialVoltage:	{			//	uint16_t	minDialVoltage;			// minimal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.minDialVoltage), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.minDialVoltage);
				break;
			}
			case parameter_maxDialVoltage: {			//	uint16_t	maxDialVoltage;			// maximal Voltage in Dialysis mode  Volt FIXPOINTSHIFT_POLARIZ_VOLT
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d bytes for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.maxDialVoltage), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.maxDialVoltage);
				break;
			}
			case parameter_wghtCtlTarget: {				// uint32_t	wghtCtlTarget;			// weight control target [same unit as weigth measurement on RTB]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.wghtCtlTarget), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.wghtCtlTarget);
				break;
			}
			case parameter_wghtCtlLastMeasurement: {	// uint32_t	wghtCtlLastMeasurement;	// weight control last measurement [same unit as weigth measurement on RTB]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.wghtCtlLastMeasurement);
				break;
			}
			case parameter_wghtCtlDefRemPerDay:	{		// uint32_t	wghtCtlDefRemPerDay;	// weight default removal per day [ml/day], set it at turn on, then WAKD will maintain it.
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.wghtCtlDefRemPerDay);
				break;
			}
			case parameter_kCtlTarget: {				// uint32_t	kCtlTarget;				// potassium control target use FIXPOINTSHIFT for [mmol/l]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.kCtlTarget), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.kCtlTarget);
				break;
			}
			case parameter_kCtlLastMeasurement: {		// uint32_t	kCtlLastMeasurement;	// potassium control last measurement use FIXPOINTSHIFT for [mmol/l]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.kCtlLastMeasurement);
				break;
			}
			case parameter_kCtlDefRemPerDay: {			// uint32_t	kCtlDefRemPerDay;		// potassium default removal per day use FIXPOINTSHIFT for [mmol/day]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.kCtlDefRemPerDay);
				break;
			}
			case parameter_urCtlTarget:	{				// uint32_t	urCtlTarget;			// urea control target use FIXPOINTSHIFT for [mmol/l]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.urCtlTarget), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.urCtlTarget);
				break;
			}
			case parameter_urCtlLastMeasurement: {		// uint32_t	urCtlLastMeasurement;	// urea control last measurement use FIXPOINTSHIFT for [mmol/l]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.urCtlLastMeasurement);
				break;
			}
			case parameter_urCtlDefRemPerDay:{			// uint32_t	urCtlDefRemPerDay;		// urea default removal per day use FIXPOINTSHIFT for [mmol/day]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgPatientProfileData->patientProfile.urCtlDefRemPerDay);
				break;
			}
			case parameter_defSpeedBp_mlPmin: {			//	uint16_t		defSpeedBp_mlPmin;	// setting speed blood pump [ml/min]
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedBp_mlPmin);
				break;
			}
			case parameter_defSpeedFp_mlPmin: {			//	uint16_t		defSpeedFp_mlPmin;	// setting speed fluidic pump [ml/min]	(=excreation speed in UF-state) use NO! FIXPOINTSHIFT_PUMP_FLOW
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defSpeedFp_mlPmin);
				break;
			}
			case parameter_defPol_V: {					//	uint16_t		defPol_V;			// setting polarization [V]#
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.defPol_V);
				break;
			}
			case parameter_direction: {					//	uint8_t			direction;
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.direction);
				break;
			}
			case parameter_duration_sec: {				//	uint32_t		duration_sec;		// pre-setting for duaration of this operational state
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pMsgWAKDStateConfigurationParameters->WAKDStateConfigurationParameters.duration_sec);
				break;
			}
			case parameter_NaAbsL: {		    		// lower Na absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaAbsL);
				break;
			}
			case parameter_NaAbsH: {     		// upper Na absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaAbsH);
				break;
			}
			case parameter_NaTrdLT: {			// lower Na time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaTrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaTrdLT);
				break;
			}
			case parameter_NaTrdHT: {			// upper Na time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaTrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaTrdHT);
				break;
			}
			case parameter_NaTrdLPsT: {		// lower Na trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaTrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaTrdLPsT);
				break;
			}
			case parameter_NaTrdHPsT: {		// upper Na trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->NaTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->NaTrdHPsT);
				break;
			}
			case parameter_KAbsL: {			// lower K absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KAbsL);
				break;
			}
			case parameter_KAbsH: {			// upper K absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KAbsH);
				break;
			}
			case parameter_KAAL: {				// lower K absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KAAL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KAAL);
				break;
			}
			case parameter_KAAH: {			// upper K absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KAAH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KAAH);
				break;
			}
			case parameter_KTrdLT: {			// lower K trend low time-difference
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KTrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KTrdLT);
				break;
			}
			case parameter_KTrdHT: {			// upper K trend low time-difference
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KTrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KTrdHT);
				break;
			}
			case parameter_KTrdLPsT: {			// lower K trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KTrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KTrdLPsT);
				break;
			}
			case parameter_KTrdHPsT: {			// upper K trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->KTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->KTrdHPsT);
				break;
			}
			case parameter_CaAbsL: {			// lower Ca absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaAbsL);
				break;
			}
			case parameter_CaAbsH: {			// upper Ca absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaAbsH);
				break;
			}
			case parameter_CaAAbsL: {			// lower K absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaAAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaAAbsL);
				break;
			}
			case parameter_CaAAbsH: {			// upper K absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaAAbsH);
				break;
			}
			case parameter_CaTrdLT: {			// lower Ca time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaTrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaTrdLT);
				break;
			}
			case parameter_CaTrdHT: {			// upper Ca time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaTrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaTrdHT);
				break;
			}
			case parameter_CaTrdLPsT: {		// lower Ca trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaTrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaTrdLPsT);
				break;
			}
			case parameter_CaTrdHPsT: {		// upper Ca trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaTrdHPsT);
				break;
			}
			case parameter_UreaAAbsH: {		// upper Urea absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->UreaAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->UreaAAbsH);
				break;
			}
			case parameter_UreaTrdHT: {		// upper Urea time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->UreaTrdT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->UreaTrdT);
				break;
			}
			case parameter_UreaTrdHPsT: {		// upper Urea trend per Specified Time
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->UreaTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->UreaTrdHPsT);
				break;
			}
			case parameter_CreaAbsL: {			// lower Crea absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CreaAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CreaAbsL);
				break;
			}
			case parameter_CreaAbsH: {			// upper Crea absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CreaAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CreaAbsH);
				break;
			}
			case parameter_CreaTrdHT: {		// upper Crea time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CreaTrdT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CreaTrdT);
				break;
			}
			case parameter_CreaTrdHPsT: {		// upper Crea trend per Specified Time
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CreaTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CreaTrdHPsT);
				break;
			}
			case parameter_PhosAbsL: {			// lower Phos absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosAbsL);
				break;
			}
			case parameter_PhosAbsH: {			// higher Phos absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosAbsH);
				break;
			}
			case parameter_PhosAAbsH: {		// upper Phos absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosAAbsH);
				break;
			}
			case parameter_PhosTrdLT: {		// lower Phos time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosTrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosTrdLT);
				break;
			}
			case parameter_PhosTrdHT: {		// upper Phos time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosTrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosTrdHT);
				break;
			}
			case parameter_PhosTrdLPsT: {		// lower Phos trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosTrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosTrdLPsT);
				break;
			}
			case parameter_PhosTrdHPsT: {		// upper Phos trend per Specified Time
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhosTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhosTrdHPsT);
				break;
			}
			case parameter_HCO3AAbsL: {		// lower HCO3 absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3AAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3AAbsL);
				break;
			}
			case parameter_HCO3AbsL: {			// lower HCO3 absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3AbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3AbsL);
				break;
			}
			case parameter_HCO3AbsH: {			// upper HCO3 absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3AbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3AbsH);
				break;
			}
			case parameter_HCO3TrdLT: {		// lower HCO3 time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3TrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3TrdLT);
				break;
			}
			case parameter_HCO3TrdHT: {		// upper HCO3 time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3TrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3TrdHT);
				break;
			}
			case parameter_HCO3TrdLPsT: {		// lower HCO3 trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3TrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3TrdLPsT);
				break;
			}
			case parameter_HCO3TrdHPsT: {		// upper HCO3 trend per Specified Time
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3TrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3TrdHPsT);
				break;
			}
			case parameter_PhAAbsL: {			// lower pH absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhAAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhAAbsL);
				break;
			}
			case parameter_PhAAbsH: {			// upper pH absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhAAbsH);
				break;
			}
			case parameter_PhAbsL: {			// lower pH absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhAbsL);
				break;
			}
			case parameter_PhAbsH: {			// upper pH absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhAbsH);
				break;
			}
			case parameter_PhTrdLT: {			// lower pH time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhTrdLT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhTrdLT);
				break;
			}
			case parameter_PhTrdHT: {			// upper pH time period
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhTrdHT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhTrdHT);
				break;
			}
			case parameter_PhTrdLPsT: {		// lower pH trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhTrdLPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhTrdLPsT);
				break;
			}
			case parameter_PhTrdHPsT: {		// upper pH trend per Specified Time threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->PhTrdHPsT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->PhTrdHPsT);
				break;
			}
			case parameter_BPsysAbsL: {		// lower BPsys absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPsysAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPsysAbsL);
				break;
			}
			case parameter_BPsysAbsH: {		// upper BPsys absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPsysAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPsysAbsH);
				break;
			}
			case parameter_BPsysAAbsH: {		// upper BPsys absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPsysAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPsysAAbsH);
				break;
			}
			case parameter_BPdiaAbsL: {		// lower BPdia absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPdiaAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPdiaAbsL);
				break;
			}
			case parameter_BPdiaAbsH: {		// upper BPdia absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPdiaAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPdiaAAbsH);
				break;
			}
			case parameter_BPdiaAAbsH: {		// upper BPdia absolute alarm threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPdiaAAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPdiaAAbsH);
				break;
			}
			case parameter_pumpFaccDevi: {		// accepted flow deviation ml/min
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->pumpFaccDevi), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->pumpFaccDevi);
				break;
			}
			case parameter_pumpBaccDevi: {		// accepted flow deviation ml/min
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->pumpBaccDevi), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->pumpBaccDevi);
				break;
			}
			case parameter_VertDeflecitonT: {	// accepted vertical deflection threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->VertDeflecitonT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->VertDeflecitonT);
				break;
			}
			case parameter_WghtAbsL: {			// lower Weight absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->WghtAbsL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->WghtAbsL);
				break;
			}
			case parameter_WghtAbsH: {			// upper Weight absolute threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->WghtAbsH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->WghtAbsH);
				break;
			}
			case parameter_WghtTrdT: {			// Weight thrend threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->WghtTrdT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->WghtTrdT);
				break;
			}
			case parameter_FDpTL: {			// Fluid Extraction Deviation Percentage Threshold Low
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FDpTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FDpTL);
				break;
			}
			case parameter_FDpTH: {			// Fluid Extraction Deviation Percentage Threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FDpTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FDpTH);
				break;
			}
			case parameter_BPSoTH: {			// sensor 'BPSo' pressure upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPSoTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPSoTH);
				break;
			}
			case parameter_BPSoTL: {			// sensor 'BPSo' pressure lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPSoTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPSoTL);
				break;
			}
			case parameter_BPSiTH: {			// sensor 'BPSi' pressure upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPSiTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPSiTH);
				break;
			}
			case parameter_BPSiTL: {			// sensor 'BPSi' pressure lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BPSiTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BPSiTL);
				break;
			}
			case parameter_FPS1TH: {			// sensor 'FPS1' pressure upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FPSoTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FPSoTH);
				break;
			}
			case parameter_FPS1TL: {			// sensor 'FPS1' pressure lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FPSoTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FPSoTL);
				break;
			}
			case parameter_FPS2TH: {			// sensor 'FPS2' pressure upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FPSTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FPSTH);
				break;
			}
			case parameter_FPS2TL: {			// sensor 'FPS2' pressure lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->FPSTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->FPSTL);
				break;
			}
			case parameter_BTSoTH: {			// temperature sensor BTSo value upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BTSoTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BTSoTH);
				break;
			}
			case parameter_BTSoTL: {			// temperature sensor BTSo value lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BTSoTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BTSoTL);
				break;
			}
			case parameter_BTSiTH: {			// temperature sensor BTSi value upper threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BTSiTH), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BTSiTH);
				break;
			}
			case parameter_BTSiTL: {			// temperature sensor BTSi value lower threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BTSiTL), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BTSiTL);
				break;
			}
			case parameter_BatStatT: {			//battery status threshold
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->BatStatT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->BatStatT);
				break;
			}
			case parameter_f_K: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->f_K), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->f_K);
				break;
			}
			case parameter_SCAP_K: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->SCAP_K), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->SCAP_K);
				break;
			}
			case parameter_P_K: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->P_K), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->P_K);
				break;
			}
			case parameter_Fref_K: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->Fref_K), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->Fref_K);
				break;
			}
			case parameter_cap_K: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->cap_K), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->cap_K);
				break;
			}
			case parameter_f_Ph: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->f_Ph), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->f_Ph);
				break;
			}
			case parameter_SCAP_Ph: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->SCAP_Ph), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->SCAP_Ph);
				break;
			}
			case parameter_P_Ph: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->P_Ph), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->P_Ph);
				break;
			}
			case parameter_Fref_Ph: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->Fref_Ph), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->Fref_Ph);
				break;
			}
			case parameter_cap_Ph: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->cap_Ph), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->cap_Ph);
				break;
			}
			case parameter_A_dm2: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->A_dm2), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->A_dm2);
				break;
			}
			case parameter_CaAdr: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CaAdr), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CaAdr);
				break;
			}
			case parameter_CreaAdr: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->CreaAdr), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->CreaAdr);
				break;
			}
			case parameter_HCO3Adr: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->HCO3Adr), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->HCO3Adr);
				break;
			}
			case parameter_wgtNa: {			// the fuzzy importance of the adsorption of this substance
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtNa), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtNa);
				break;
			}
			case parameter_wgtK: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtK), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtK);
				break;
			}
			case parameter_wgtCa: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtCa), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtCa);
				break;
			}
			case parameter_wgtUr: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtUr), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtUr);
				break;
			}
			case parameter_wgtCrea: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtCrea), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtCrea);
				break;
			}
			case parameter_wgtPhos: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtPhos), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtPhos);
				break;
			}
			case parameter_wgtHCO3: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->wgtHCO3), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->wgtHCO3);
				break;
			}
			case parameter_mspT: {
				#if defined DEBUG_DS || defined SEQ12
					taskMessage("I", PREFIX, "Allocating %d byte for parameter '%s'.", sizeof(pParametersSCC->mspT), parameterToString(parameter));
				#endif
				msgSize = msgSize + sizeof(pParametersSCC->mspT);
				break;
			}
			default: {
				taskMessage("E", PREFIX, "Unknown parameter inside 'dataID_CurrentParameters'.");
				break;
			}
		}
	}
	// The memory Size needs to be increased some more to also store the 'names' for each value
	msgSize = msgSize + numberParameter;	// dataSize is equivalent to the number of parameters requested.
	return(msgSize);
}
