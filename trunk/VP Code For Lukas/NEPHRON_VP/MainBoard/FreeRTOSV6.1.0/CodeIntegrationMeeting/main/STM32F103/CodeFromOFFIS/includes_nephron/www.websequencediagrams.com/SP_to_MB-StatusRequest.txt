SP -> MB: dataID_statusRequest
note over SP MB
	tdMsgOnly varMsg;
	varMsg.header.recipientId = whoId_MB;
	varMsg.header.msgSize  = sizeof(tdMsgOnly);
	varMsg.header.dataId   = dataID_statusRequest;
	varMsg.header.msgCount = 7;
	varMsg.header.issuedBy = whoId_SP;
	putData(&varMsg);
end note

alt
	MB -> SP: dataID_ack
	note over SP MB
		tdMsgOnly varMsg;
		varMsg.header.recipientId = whoId_SP;
		varMsg.header.msgSize  = sizeof(tdMsgOnly);
		varMsg.header.dataId   = dataID_ack;
		varMsg.header.msgCount = 7; // copy from msg to ack!!
		varMsg.header.issuedBy = whoId_MB;
		putData(&varMsg);
	end note
	
	MB -> RTB: putData();
	note over MB RTB
		dataID_statusRequest
	end note
	RTB -> MB: decodeData();
	
	note over MB
		current state now
		in static variable
	end note
	
	MB -> SP: dataID_currentWAKDstateIs
	note over MB SP
		tdMsgCurrentWAKDstateIs varMsg;
		varMsg.header.recipientId = whoId_SP;
		varMsg.header.msgSize  = sizeof(tdMsgCurrentWAKDstateIs);
		varMsg.header.dataId   = dataID_currentWAKDstateIs;
		varMsg.header.msgCount = 531;
		varMsg.header.issuedBy = whoId_MB;
		varMsg.wakdStateIs = wakdStates_AllStopped;
		putData(&varMsg);
	end note
	
	alt msg received
		SP -> MB: dataID_ack
	else msg error
		SP -> MB: dataID_nack
	end
else
	MB -> SP: dataID_nack
	note over SP
		Request could not be processed
		Act accordingly (out of BT reach?)
	end note
end

