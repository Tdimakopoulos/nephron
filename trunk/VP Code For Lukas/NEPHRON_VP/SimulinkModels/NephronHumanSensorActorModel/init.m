warning('off');
TbaseSim=1;

CUr_Init_mmolPL=5;
CK_exInit_mmolPL=6.3;
CK_inInit_mmolPL=143;
CNa_exInit_mmolPL=145;

real_Cin=CUr_Init_mmolPL;
real_GUr=0.155/4; %initial, overlayed by Transient
real_GNa=0.06944;
%real_k_V=0.4;
real_V_tot=45;
real_V_ex=real_V_tot*0.375;
real_V_drink=1500;

real_GmKNa=0;