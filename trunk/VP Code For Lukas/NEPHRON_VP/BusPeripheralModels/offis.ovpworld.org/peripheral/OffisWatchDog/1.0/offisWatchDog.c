/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include "offisWatchDog.h"

#define PREFIX "OFFIS HW Watchdog"

////////////////////// Declaration of peripheral wide data structures ///////////////////////////////

OFFIS_wd_RegisterT OFFIS_wd_sPort; // structure with all public slave port registers
handles_type handles;              // structure of handles to ports and nets




//////////////////////////////// Bus Slave Ports ///////////////////////////////

static void installSlavePorts(void) {
  handles.OFFIS_wd_sPort = ppmCreateSlaveBusPort("sp", 64);
}

/////////////////////////////////// Net Ports //////////////////////////////////

static void installNetPorts(void) {
// To write to this net, use ppmWriteNet(handles.IRQ*, value);
    handles.reset = ppmOpenNetPort("reset");
}

///////////////////////////// MMR Generic callbacks ////////////////////////////

static PPM_VIEW_CB(view32) {  *(Uns32*)data = *(Uns32*)user; }

PPM_REG_WRITE_CB(wcb) {
	// bhmMessage("I", PREFIX, "Value before: %d and after: %d write", *(Uns32*)user, data);
	*(Uns32*)user = data;
}

PPM_REG_READ_CB(rcb) {
    // YOUR CODE HERE (regRd32)
    return *(Uns32*)user;
}

PPM_CONSTRUCTOR_CB(periphConstructor);

PPM_CONSTRUCTOR_CB(constructor) {
  // YOUR CODE HERE (pre constructor)
  periphConstructor();
  // YOUR CODE HERE (post constructor)
}

PPM_DESTRUCTOR_CB(destructor) {
    // YOUR CODE HERE (destructor)
}

//////////////////////////// Memory mapped registers ///////////////////////////

static void installRegisters(void) {

  ppmCreateRegister("value_register",                          // name
		    "value register",                          // description
		    handles.OFFIS_wd_sPort,                    // WINDOW base
		    0,                                         // port offset in bytes
		    4,                                         // register size in bytes
		    rcb,                                       // Read call back function
		    wcb,                                       // write call back function
		    view32,                                    // debug view ????
		    &(OFFIS_wd_sPort.count.value),             // register in my data structure
		    True
		    );
  ppmCreateRegister("control_register",
		    "control register",
		    handles.OFFIS_wd_sPort,
		    4,
		    4,
		    rcb,
		    wcb,
		    view32,
		    &(OFFIS_wd_sPort.ctrl.value),
		    True
		    );
}

////////////////////////////////// Constructor /////////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor) {
  installSlavePorts();
  installRegisters();
  installNetPorts();
}

////////////////////////////////// Create Threads////////////////////////////////
#define size (32*1024)

char stackA[size]; 

void myThread(void *user)
{
  // struct myThreadcontext *p = user;
  // This is a very simple watchdog dummy
  while(1) {
    bhmWaitDelay(1000); // wait for 1ms (in us)
	// The Watchdog is active if the control register is not "0"
    if (OFFIS_wd_sPort.ctrl.value) {
		if (OFFIS_wd_sPort.count.value > 0)
		{
			// bhmMessage("I", PREFIX, "WD count is %d", OFFIS_wd_sPort.count.value);
			--OFFIS_wd_sPort.count.value;
		} else {
			bhmMessage("W", PREFIX, "Reset of microcontroller!");
			ppmWriteNet(handles.reset, 0);
			bhmMessage("W", PREFIX, "Disabling watchdog!");
			OFFIS_wd_sPort.ctrl.value = 0;
		}
	}
  }
}

void userInit(void)
{
  struct myThreadcontext { Uns32 myThreadData1; Uns32 myThreadData2; } contextA;
  bhmCreateThread(myThread, &contextA, "threadA", &stackA[size]);
}

///////////////////////////////////// Main /////////////////////////////////////

int main(int argc, char *argv[]) {
  constructor();
  userInit();
  bhmWaitEvent(bhmGetSystemEvent(BHM_SE_END_OF_SIMULATION));
  destructor();
  return 0;
}
