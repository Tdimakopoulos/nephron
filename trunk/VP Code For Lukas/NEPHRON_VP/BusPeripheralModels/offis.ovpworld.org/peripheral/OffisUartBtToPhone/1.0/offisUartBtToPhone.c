/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>

#define PSE
#include "offisUartBtToPhone.h"

//define no inline attribute for intercepted functions 
#define NOINLINE __attribute__((noinline)) 

////////////////////// Declaration of peripheral wide data structures ///////////////////////////////

OFFIS_sl_RegisterT OFFIS_sl_sPort; // structure with all public slave port registers
handles_type handles;              // structure of handles to ports and nets

//////////////////////////////// Bus Slave Ports ///////////////////////////////

// Offset | Regsiter
// =============================
// 0x0000 | 
//   ...  |   ...

static void installSlavePorts(void)
{
	handles.OFFIS_sl_sPort = ppmCreateSlaveBusPort("sp", 12);
}

static void installNetPorts(void)
{
	// To write to this net, use ppmWriteNet(handles.IRQ*, value);
	handles.IRQo0  = ppmOpenNetPort("IRQo0"); // (IRQ from UART)
}

///////////////////////////// Interface functions to host native DLL  ////////////////////////////

NOINLINE void writeUartBtToPhone() { 
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: writeUartBtToPhone should be intercepted"); 
} 

NOINLINE void openUartBtToPhone(Uns32 strlenComPort, char * comPort)
{
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: openUartBtToPhone should be intercepted"); 
}

NOINLINE void closeUartBtToPhone()
{ 
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: closeUartBtToPhone should be intercepted"); 
} 

NOINLINE void readUartBtToPhone(Uns32 p_sPort) { 
  // fatal error to reach this point 
  bhmMessage("F", PREFIX, "OFFIS: readUartBtToPhone should be intercepted"); 
}

///////////////////////////// MMR Generic callbacks ////////////////////////////

static PPM_VIEW_CB(view32) {  *(Uns32*)data = *(Uns32*)user; }

PPM_REG_READ_CB(rcb_uartOut) {
	bhmMessage("E", PREFIX, "Reading from output UART address!"); 
	return *(Uns32*)user;
}

PPM_REG_WRITE_CB(wcb_uartOut) {
	// bhmMessage("I", PREFIX, "Writing to output UART address value: %x.", data); 
	writeUartBtToPhone();
}

PPM_REG_READ_CB(rcb_uartIn) {
	// bhmMessage("I", PREFIX, "Reading from input UART address."); 
	return *(Uns32*)user;
}

PPM_REG_WRITE_CB(wcb_uartIn) {
	bhmMessage("E", PREFIX, "Writing to input UART address is not defined!"); 
}

PPM_REG_READ_CB(rcb_uartIRQ) {
	// bhmMessage("W", PREFIX, "Reading from UART IRQ address!"); 
	return *(Uns32*)user;
}

PPM_REG_WRITE_CB(wcb_uartIRQ) {
	// Writing to register uartIRQ does not set the value for future reads from it.
	// Rather, this register should be written near the end of an ISR, to clear the reset.
	// bhmMessage("I", PREFIX, "Clearing UART interrupt."); 
	ppmWriteNet(handles.IRQo0, 0);
}

PPM_CONSTRUCTOR_CB(periphConstructor);

PPM_CONSTRUCTOR_CB(constructor) {
  // YOUR CODE HERE (pre constructor)
  periphConstructor();
  // YOUR CODE HERE (post constructor)
}

PPM_DESTRUCTOR_CB(destructor) {
	// YOUR CODE HERE (destructor)
	closeUartBtToPhone();
}

//////////////////////////// Memory mapped registers ///////////////////////////

static void installRegisters(void) {
	
	// First address on OffisSimLink bus interafce is the IRQ status register
	// whenever this register is not 0 OffisSimLink creates an IRQ on its output
	ppmCreateRegister(	"uartIn",							// name
						"uartIn",							// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						0,									// port offset in bytes
						4,									// register size in bytes
						rcb_uartIn,							// Read call back function
						wcb_uartIn,							// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.uartIn),			// register in my data structure
						True
					);	
	ppmCreateRegister(	"uartOut",							// name
						"uartOut",							// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						4,									// port offset in bytes
						4,									// register size in bytes
						rcb_uartOut,						// Read call back function
						wcb_uartOut,						// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.uartOut),			// register in my data structure
						True
					);
	ppmCreateRegister(	"uartIRQ",							// name
						"uartIRQ",							// description
						handles.OFFIS_sl_sPort,				// WINDOW base
						8,									// port offset in bytes
						4,									// register size in bytes
						rcb_uartIRQ,						// Read call back function
						wcb_uartIRQ,						// write call back function
						view32,								// debug view ????
						&(OFFIS_sl_sPort.uartIRQ),			// register in my data structure
						True
					);
}

////////////////////////////////// Constructor /////////////////////////////////

PPM_CONSTRUCTOR_CB(periphConstructor) {
	installSlavePorts();
	installRegisters();
	installNetPorts();

	Uns32 strlenComPort;
	if (bhmIntegerAttribute("strlenComPort", &strlenComPort)) {
		// bhmMessage("I", PREFIX, "The Com Port Name has #characters: %d", strlenComPort);
	} else {
		bhmMessage("F", PREFIX, "Number characters for \"strlenComPort\" could not be read!"); 
	}
	char *comPort = malloc(strlenComPort);
	if (bhmStringAttribute("comPort", comPort, strlenComPort)) { 
		// bhmMessage("I", PREFIX, "The Com Port Name is %s", comPort);
	} else{
		bhmMessage("F", PREFIX, "No Com Port name defined!"); 
	}
	openUartBtToPhone(strlenComPort, comPort);
}

////////////////////////////////// Create Threads////////////////////////////////
#define size (32*2048)

char stackA[size]; 

void myThread(void *user)
{
	while(1) {
		readUartBtToPhone((Uns32)&OFFIS_sl_sPort);
		if(OFFIS_sl_sPort.uartIRQ != 0) {
			// bhmMessage("I", PREFIX, "Setting UART IRQ for received value %x.", OFFIS_sl_sPort.uartIn);
			ppmWriteNet(handles.IRQo0, 1); // set interrupt
			OFFIS_sl_sPort.uartIRQ = 0;
		}
		// syncMatlabDUMMY(&OFFIS_sl_sPort);
		bhmWaitDelay(900);  // time to wait in micro seconds, -> 1ms
	}
}

void userInit(void)
{
  struct myThreadcontext { Uns32 myThreadData1; Uns32 myThreadData2; } contextA;
  bhmCreateThread(myThread, &contextA, "threadA", &stackA[size]);
}

///////////////////////////////////// Main /////////////////////////////////////

int main(int argc, char *argv[]) {
  constructor();
  userInit();
  bhmWaitEvent(bhmGetSystemEvent(BHM_SE_END_OF_SIMULATION));
  destructor();
  return 0;
}
