/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */
  
#define SEMIHOST_DLL
#include "../offisUartBtToPhone.h"
#include "peripheral_semihost_helperFuncs.h"



// prefix to be used during printf messages
#define PREFIX  "PERIPHERAL_OffisUartBtPhone_DLL"

#define WIN32_LEAN_AND_MEAN            
#include <windows.h>

#include <io.h>
#include <stdio.h> 
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

HANDLE hComm;
DCB RS232Config;
COMMTIMEOUTS commtimeouts;

DWORD commerr;
COMSTAT comstat;

char readChar;

static int errormsg(DWORD dwErrorCode) 
{ 
   LPVOID lpBuffer;

   FormatMessage(
           FORMAT_MESSAGE_ALLOCATE_BUFFER | 
           FORMAT_MESSAGE_FROM_SYSTEM,
           NULL,
           dwErrorCode,
           MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
           (LPTSTR) &lpBuffer,
           0, NULL );

   vmiMessage("E", PREFIX, "Error %d: %s\r\n", (int) dwErrorCode, (char*) lpBuffer);
   LocalFree(lpBuffer);
   return 1;
}
   
   
static VMIOS_INTERCEPT_FN(openUartBtToPhone)
{
	// vp_: pointer in virtual address space of simulation. Need to copy from
	//      there to here on real host (DLL) address space.
	Uns32 vp_comPort;
	Uns32 strlenComPort;

	getArg(processor, object, 0, &strlenComPort);
	getArg(processor, object, 1, &vp_comPort);

	char *comPort = malloc(strlenComPort);
	vmirtReadNByteDomain(object->pseDomain, vp_comPort, comPort, strlenComPort, 0, False);

	char *fixAboveCom9 = malloc(7+strlenComPort);
	// If we want to enable more than just com1 to com9, we need to include
	// the following string before:
	strcpy(fixAboveCom9, "\\\\.\\");
	strcat(fixAboveCom9, comPort);
	vmiMessage("I", PREFIX, "Opening port %s.", fixAboveCom9);
	
	// RS232 stuff from here on
	
	// open port
	hComm = CreateFile( fixAboveCom9,
			GENERIC_READ | GENERIC_WRITE, 
			0, 
			NULL, 
			OPEN_EXISTING, 
			FILE_ATTRIBUTE_NORMAL, 
			NULL); 

	RS232Config.DCBlength = sizeof(RS232Config);
   
	// check if port is open
	if(hComm == INVALID_HANDLE_VALUE)
		errormsg(GetLastError());

	// set IO-buffer
	if(SetupComm(hComm, 2048, 2048) == FALSE)
		errormsg(GetLastError());

	// set com port
	if(GetCommState(hComm, &RS232Config) == FALSE)
		errormsg(GetLastError());

	RS232Config.BaudRate = 9600; 
	RS232Config.ByteSize = 8;
	RS232Config.Parity = NOPARITY; 
	RS232Config.StopBits = ONESTOPBIT; 
	RS232Config.fRtsControl = 0;
	RS232Config.fOutxCtsFlow = 0;
	RS232Config.fAbortOnError = TRUE;
   
	/*printf("Current Settings:\n Baud Rate %d\n Parity %d\n Byte Size %d\n Stop Bits %d\n", 
		RS232Config.BaudRate, RS232Config.Parity, RS232Config.ByteSize, RS232Config.StopBits);
   */
	if(SetCommState(hComm, &RS232Config) == FALSE)
		errormsg(GetLastError());

	// set timeout
	memset(&commtimeouts, 0, sizeof(COMMTIMEOUTS)); 
	
	commtimeouts.ReadIntervalTimeout = 10;
	commtimeouts.ReadTotalTimeoutMultiplier = 10;
	commtimeouts.ReadTotalTimeoutConstant = 10;
	commtimeouts.WriteTotalTimeoutMultiplier = 10;
	commtimeouts.WriteTotalTimeoutConstant = 50;

	if(SetCommTimeouts(hComm, &commtimeouts) == FALSE)
		errormsg(GetLastError());

	// clear IO-buffer
	PurgeComm(hComm, PURGE_RXABORT | PURGE_TXABORT | PURGE_RXCLEAR | PURGE_TXCLEAR);

	//Sleep(1000); // wait some time to make sure the port is open
}

static VMIOS_INTERCEPT_FN(writeUartBtToPhone)
{  
	// Retrieve values from OVP-Platform
	Uns32 p_sPort;
	OFFIS_sl_RegisterT OFFIS_sl_sPort;

	// Get the first argument from stack of virtual x86 (PSE)
	// which contains the pointer in the virtual processor memory space.
	// Then copy the struc from the virtual memory space into the
	// physical domain of this host dll
	getArg(processor, object, 0, &p_sPort);
	vmirtReadNByteDomain(object->pseDomain, p_sPort, (void *)&OFFIS_sl_sPort, sizeof(OFFIS_sl_sPort), 0, False);
	// vmiMessage("I", PREFIX, "Sending to phone: %x.", OFFIS_sl_sPort.uartOut);
	
	DWORD bytesWritten;
	WriteFile(hComm, &OFFIS_sl_sPort.uartOut, 1, &bytesWritten, NULL);
}

// Usually we would not need this function. The code could be copy pasted into
// the destructur below. But for unknown reasons the code hangs if done so.
// I strongly suspect an error in the OVP/VMI interface.
static VMIOS_INTERCEPT_FN(closeUartBtToPhone)
{
	if(hComm != NULL)
		CloseHandle(hComm);
	vmiMessage("I", PREFIX, "Clossing Uart link!\n");
}

static VMIOS_INTERCEPT_FN(readUartBtToPhone)
{
	DWORD bytesRead;
	
	// Retrieve values from OVP-Platform
	Uns32 p_sPort;
	OFFIS_sl_RegisterT OFFIS_sl_sPort;
	
	ClearCommError(hComm, &commerr, &comstat);
	if(comstat.cbInQue != 0)
    {
		// Get the first argument from stack of virtual x86 (PSE)
		// which contains the pointer in the virtual processor memory space.
		// Then copy the struc from the virtual memory space into the
		// physical domain of this host dll
		// this is done because we don't want to overwrite the uartOut register
		getArg(processor, object, 0, &p_sPort);
		vmirtReadNByteDomain(object->pseDomain, p_sPort, (void *)&OFFIS_sl_sPort, sizeof(OFFIS_sl_sPort), 0, False);
		// if bytes are in the uart queue, read them out
		if(ReadFile(hComm, &readChar, 1, &bytesRead, NULL) != 0)
		{
			// write them into OFFIS_sl_sPort.uartIn, set IRQ flag and send it back into the register
			OFFIS_sl_sPort.uartIn = readChar;
			OFFIS_sl_sPort.uartIRQ = 1;
			vmirtWriteNByteDomain(object->pseDomain, p_sPort, (void *)&OFFIS_sl_sPort, sizeof(OFFIS_sl_sPort), 0, False);
			// vmiMessage("I", PREFIX, "Receiving from Phone: %x.", OFFIS_sl_sPort.uartIn);
		}
	}
	else
	{
		;
	}
}

// Constructor
static VMIOS_CONSTRUCTOR_FN(constructor)
{
	object->result       = vmiosGetRegDesc(processor, "eax");
	object->sp           = vmiosGetRegDesc(processor, "esp");
	object->pseDomain    = vmirtGetProcessorDataDomain(processor);
}

// Destructor
static VMIOS_DESTRUCTOR_FN(destructor)
{
}

vmiosAttr modelAttrs = { 

	////////////////////////////////////////////////////////////////////////
	// VERSION
	////////////////////////////////////////////////////////////////////////

	VMI_VERSION,                // version string (THIS MUST BE FIRST)
	VMI_INTERCEPT_LIBRARY,      // type
	"semiHostPeripheral",       // description 
	sizeof(vmiosObject),        // size in bytes of object 

	////////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR/DESTRUCTOR ROUTINES
	////////////////////////////////////////////////////////////////////////
  
	constructor,                // object constructor 
	destructor,                 // object destructor 

	////////////////////////////////////////////////////////////////////////
	// INSTRUCTION INTERCEPT ROUTINES
	////////////////////////////////////////////////////////////////////////

	0,                          // morph callback 
	0,                          // get next instruction address
	0,                          // disassemble instruction 
  
	// ADDRESS INTERCEPT DEFINITIONS 
	// -------------------   ----------- ------ ----------------- 
	// Name                  Address     Opaque Callback 
	// -------------------   ----------- ------ ----------------- 
	{
		{"openUartBtToPhone",	0, True, openUartBtToPhone},
		{"writeUartBtToPhone",	0, True, writeUartBtToPhone},
		{"closeUartBtToPhone",	0, True, closeUartBtToPhone},
		{"readUartBtToPhone",	0, True, readUartBtToPhone},
		{ 0 }
	}
}; 
