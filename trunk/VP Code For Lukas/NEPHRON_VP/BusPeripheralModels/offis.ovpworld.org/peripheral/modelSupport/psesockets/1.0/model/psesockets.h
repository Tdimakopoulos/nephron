/*
 * Copyright (c) 2005-2011 Imperas Software Ltd., www.imperas.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// #include this in the PSE. It defines the intercepted routines
// and the record/replay interface.
// NOTE THAT THIS IS PSE CODE, NOT NATIVE.

#ifndef PSE_SOCKET_H
#define PSE_SOCKET_H

#include "impTypes.h"
#include "bhm.h"

// Open a port, logfile or source file.
// Cannot have a port and source file
// but can have a port and log file.
// Returns 0 >= ch >= MAX     Fatal if it fails
//

static void failedToIntercept(const char *what) {
    bhmMessage("F", "PPM_SER", "Failed to intercept %s. You need to use the psesockets intercept library.", what);
}

Uns32 sockOpenBlocking(Uns32 *port, const char *logfile, const char *sourcefile, Bool verbose)
{
    failedToIntercept("sockOpenBlocking");
    return 0;
}

//
// ch < 0 causes all channels to be closed
//
void sockClose(Uns32 ch) {
    failedToIntercept("sockClose");
}

Uns32 sockRead(Int32 ch, Uns8 *buffer, Uns32 length)
{
    failedToIntercept("sockRead");
    return 0;
}

Uns32 sockWrite(Int32 ch, Uns8 *buffer, Uns32 length)
{
    failedToIntercept("sockWrite");
    return 0;
}

/////////////////////////////////////// UART SERVICES //////////////////////////////////////

typedef enum ktEventTypesE {
    KT_NULLS           = 78,     // random non-zero number
    KT_EVENT,
    KT_FINISH
} ktEventTypes;

static Bool recording = False;
static Bool replaying = False;

static void recordNullEvent(void)
{
#ifdef DEBUG_RECORD
    bhmMessage("I", "PPM_SER", "Record Event: NULL %e", bhmGetCurrentTime());
#endif
    bhmRecordEvent(KT_NULLS, 0, 0);

}

static void recordEvent(Uns8 *buffer, Uns32 length)
{
#ifdef DEBUG_RECORD
    bhmMessage("I", "PPM_SER", "Record Event: KT_EVENT : %e, %u %s", bhmGetCurrentTime(), length, buffer);
#endif
    bhmRecordEvent(KT_EVENT, length, buffer);
}

static void replayEvent(Uns8 *buffer, Uns32 *length)
{
    double      then;
    double      now = bhmGetCurrentTime();
    Uns32       type;
    Int32 bytes = bhmReplayEvent(&then, &type, buffer);
    static Bool warningGiven = False;
    if (bytes < 0) {
        if(!warningGiven) {
            bhmMessage("W", "PPM_SER", "End of replay file at %e no more events to send", now);
            warningGiven = True;
        }
        bytes = 0;
        type = KT_NULLS;
    }
    *length = bytes;

#ifdef DEBUG_RECORD
        bhmMessage("I", "PPM_SER",
            "Replay Event: %s : %e, %u %s",
            (type == KT_NULLS ? "KT_NULLS" : "KT_EVENT"),
            now,
            *length,
            buffer
        );
#endif

    switch(type) {
        case KT_NULLS:
            if(recording) {
                recordNullEvent();
            }
            return;

        case KT_EVENT:
            if(recording){
                recordEvent(buffer, *length);
            }
            now = bhmGetCurrentTime();
            if (now != then) {
                bhmMessage("F", "PPM_SER", "    Replay is out of step with original : now %e then %e", now, then);
            }
            break;

        case KT_FINISH:
            return;

        default:
            bhmMessage("F", "PPM_SER", "Illegal entry in record file");
            break;
    }
}

//
// Interface used by UART models.
// Note the functions are static because this file is inlined in the user's code.
//
static Uns32 serOpenBlocking(Uns32 *portp, const char *logfile, const char *sourcefile, Bool verbose)
{
    // only use record and replay if tryinmg to use an external socket
    if (bhmRecordStart()) {
#ifdef DEBUG_RECORD
        bhmMessage("I", "PPM_SER", "Record Start");
#endif
        recording = True;
    }
    if (bhmReplayStart()) {
#ifdef DEBUG_RECORD
        bhmMessage("I", "PPM_SER", "Replay Start");
#endif
        replaying = True;
    }
    if (replaying) {
        // if in replay mode, do not use the socket
        portp = NULL;
    }
    return sockOpenBlocking(portp, logfile, sourcefile, verbose);
}


static Uns32 serRead(Int32 ch, Uns8 *buffer, Uns32 length)
{
    if(replaying) {
        replayEvent(buffer, &length);
        return length;
    } else {
        Uns32 bytes = sockRead(ch, buffer, length);
        if (recording) {
            if(bytes > 0) {
                recordEvent(buffer, bytes);
            } else {
                recordNullEvent();
            }
        }
        return bytes;
    }
}

static Uns32 serWrite(Int32 ch, Uns8 *buffer, Uns32 length) {
    return sockWrite(ch, buffer, length);
}



static void serClose(Uns32 ch) {
    if(recording) {
        bhmRecordEvent(KT_FINISH, 0, NULL);
        bhmRecordFinish();
    }
    sockClose(ch);
}

#endif /* PSE_SOCKET_H */
