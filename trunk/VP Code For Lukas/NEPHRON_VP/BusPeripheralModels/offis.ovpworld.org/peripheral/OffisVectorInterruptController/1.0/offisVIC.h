/* -----------------------------------------------------------------------
 * Copyright (c) 2010     OFFIS Institute for Information Technology,
 *                        26121 Oldenburg, Germany
 *
 * All rights reserved.
 *
 * This file is directly or indirectly part of the OFFIS Virtual Platform.
 * A simualtion of an ARM7tdmi micro processor with OVP (a), FreeRTOS
 * operating system (b) and user defined tasks.
 *
 * (a) http://www.ovpworld.org/
 * (b) http://www.freertos.org/
 *
 * Created for the European projects: Nephron+ (1)
 *
 * 1) http://www.nephronplus.eu/
 *
 * A list of authors and contributors can be found in the accompanying
 * AUTHORS file.  For detailed copyright information, please refer
 * to the COPYING file.
 *
 * -----------------------------------------------------------------------
 * Contact information:
 *  OFFIS
 *    Institute for Information Technology
 *  Dipl.-Inform. Frank Poppen
 *    Escherweg 2
 *    D-26121 Oldenburg
 *    Germany
 *  www  : http://www.offis.de/
 *  phone: +49 (441) 9722-230
 *  fax  : +49 (441) 9722-128
 * -----------------------------------------------------------------------
 */

#ifndef LPC210XVIC_H
#define LPC210XVIC_H

#include "peripheral/impTypes.h"
#include "peripheral/bhm.h"
#include "peripheral/ppm.h"

#define PREFIX  "PERIPHERAL_VIC"

/////////////////////////// Register data declaration //////////////////////////

// Here I define a data Structure that will contain all the vlaues of my registers.

typedef struct OFFIS_VIC_RegisterS { 
  union { 
    Uns32 value;
  } VICIRQStatus;
  union { 
    Uns32 value;
  } VICFIQStatus;
  union { 
    Uns32 value;
  } VICRawIntr;
  union { 
    Uns32 value;
  } VICIntSelect;
  union { 
    Uns32 value;
  } VICIntEnable;
  union { 
    Uns32 value;
  } VICIntEnClr;
  union { 
    Uns32 value;
  } VICSoftInt;
  union { 
    Uns32 value;
  } VICSoftIntClear;
  union { 
    Uns32 value;
  } VICProtection;
  union { 
    Uns32 value;
  } VICVectAddr;
  union { 
    Uns32 value;
  } VICDefVectAddr;
  union { 
    Uns32 value;
  } VICVectAddr0;
  union { 
    Uns32 value;
  } VICVectAddr1;
  union { 
    Uns32 value;
  } VICVectAddr2;
  union { 
    Uns32 value;
  } VICVectAddr3;
  union { 
    Uns32 value;
  } VICVectAddr4;
  union { 
    Uns32 value;
  } VICVectAddr5;
  union { 
    Uns32 value;
  } VICVectAddr6;
  union { 
    Uns32 value;
  } VICVectAddr7;
  union { 
    Uns32 value;
  } VICVectAddr8;
  union { 
    Uns32 value;
  } VICVectAddr9;
  union { 
    Uns32 value;
  } VICVectAddr10;
  union { 
    Uns32 value;
  } VICVectAddr11;
  union { 
    Uns32 value;
  } VICVectAddr12;
  union { 
    Uns32 value;
  } VICVectAddr13;
  union { 
    Uns32 value;
  } VICVectAddr14;
  union { 
    Uns32 value;
  } VICVectAddr15;
  union { 
    Uns32 value;
  } VICVectCntl0;
  union { 
    Uns32 value;
  } VICVectCntl1;
  union { 
    Uns32 value;
  } VICVectCntl2;
  union { 
    Uns32 value;
  } VICVectCntl3;
  union { 
    Uns32 value;
  } VICVectCntl4;
  union { 
    Uns32 value;
  } VICVectCntl5;
  union { 
    Uns32 value;
  } VICVectCntl6;
  union { 
    Uns32 value;
  } VICVectCntl7;
  union { 
    Uns32 value;
  } VICVectCntl8;
  union { 
    Uns32 value;
  } VICVectCntl9;
  union { 
    Uns32 value;
  } VICVectCntl10;
  union { 
    Uns32 value;
  } VICVectCntl11;
  union { 
    Uns32 value;
  } VICVectCntl12;
  union { 
    Uns32 value;
  } VICVectCntl13;
  union { 
    Uns32 value;
  } VICVectCntl14;
  union { 
    Uns32 value;
  } VICVectCntl15;
} OFFIS_VIC_RegisterT, *OFFIS_VIC_RegisterPT;

///////////////////////////////// Port handles /////////////////////////////////

typedef struct handlesS {
  void          *OFFIS_VIC_SlavePort;
  ppmNetHandle  IRQo;
  ppmNetHandle  IRQi0;
  ppmNetHandle  IRQi1;
  ppmNetHandle  IRQi2;
  ppmNetHandle  IRQi3;
  ppmNetHandle  IRQi4;
  ppmNetHandle  IRQi5;
} handles_type, *handles_pointerType;

#endif
